<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="title" content="Sistema de inventario - VTELCA">
      <title>Siintra - VTELCA</title>
      <!-- Bootstrap Core CSS -->
      <link rel="stylesheet" href="{{asset('css/app.css')}}">
      <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('css/util.css') }}" rel="stylesheet">
   </head>
   <div>
      <div class="limiter" >
         <div class="container-login100">
            <div class="wrap-login100">
               @guest
               <form class="login100-form"  method="POST" action="{{ route('login') }}">
                  <span class="login100-form-title p-b-34">
                  <img src="http://rec.vtelca.gob.ve/img/vtelca-transparente.png" alt="Logo Vtelca" class="img-fluid" />
                  </span>
                  {{ csrf_field() }}
                  <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                     <input id="email" type="email" name="email" value="{{ old('email') }}" class="input100" placeholder="Email" required autofocus>
                  </div>
                  <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type password">
                     <input id="password" type="password" class="input100" name="password" required placeholder="Contraseña">
                  </div>
                  <div class="container-login100-form-btn">
                     <div class="btn-group mx-auto">
                        <button type="submit" class="login100-form-btn">Iniciar Sesión</button>
                     </div>
                  </div>
                  <div class="errors mx-auto pt-3">
                     <span class="focus-input100"></span>
                     @if ($errors->has('email'))
                     <span class="help-block">
                     <strong>{{ $errors->first('email') }}</strong>
                     </span>
                     @endif
                     <span class="focus-input100"></span>
                     @if ($errors->has('password'))
                     <span class="help-block">
                     <strong>{{ $errors->first('password') }}</strong>
                     </span>
                     @endif
                  </div>
                  <div class="w-full text-center p-t-27 p-b-239">
                  </div>
               </form>
               @else
               <form class="login100-form"  method="POST" action="{{ route('login') }}">
                  <span class="login100-form-title p-b-34">
                  <img src="http://rec.vtelca.gob.ve/img/vtelca-transparente.png" alt="Logo Vtelca" class="img-fluid" />
                  </span>
                  <div class="container-login100-form-btn">
                     <div class="btn-group mx-auto">
                        <a href="{{url('home')}}" class="login100-form-btn">Ir al tablero de opciones</a>
                     </div>
                  </div>
               </form>
               @endguest
               <div class="login100-more pt-5 ">
                  <div class="container-fluid">
                     <div class="row pt-5 my-5">
                        <div class="mx-auto pt-5  my-5">
                           <!-- <h1 class=" display-4 text-center" style=" text-shadow: 3px 3px #000">SIINTRA</h1> -->
                        </div>
                        <div class="description text-white">
                           <!-- <h1 class="text-center  text-shadow: 3px 3px #000;">Sistema Integrado de Trabajadores</h1> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="dropDownSelect1"></div>
   </div>
</html>
