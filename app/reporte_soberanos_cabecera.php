<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reporte_soberanos_cabecera extends Model
{
    protected $table="reporte_soberanos_cabeceras";
    protected $connection="web_empleados";
    protected $fillable=[
      'cedula',
      'nombre_completo',
      'cargo' ,
      'departamento' ,
      'tipo_nomina',
      'sueldo_mensual',
      'mes',
      'quincena' ,
      'fecha_ingreso',
      'fecha_corte_nomina',
      'total_asignacion',
      'total_deducion',
      'neto_cobrar'
    ];
}
