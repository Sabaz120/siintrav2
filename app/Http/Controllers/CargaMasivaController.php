<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reporte_soberanos_ingresos;
use App\reporte_soberanos_cabecera;
// use Excel;

use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use File;
use Carbon;
use Session;
class CargaMasivaController extends Controller
{

  public function cargaMasivaNomina(Request $request){
    ini_set('memory_limit','512M');

    $this->validate($request, array(
      'file'      => 'required'
    ));

    if($request->hasFile('file')){
      $extension = File::extension($request->file->getClientOriginalName());
      if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

        $path = $request->file->getRealPath();
        // $data = Excel::load($path, function($reader) {})->get();
        //Hoja operativa
        $data= Excel::selectSheetsByIndex('0')->load($path, function($reader) {})->get();
          if(!empty($data) && $data->count()){
              foreach ($data as $key => $value) {
                if($key!=0){
                  // dd($value);

                  if($value->p1!=null){
                    $insert[]=[
                      'mes' => $value->mes,
                      'quincena' => $value->quincena,
                      'cedula' => $value->p1,
                      'nombre_completo' =>$value->p2,
                      'cargo' => $value->p3,
                      'departamento' => $value->p4,
                      'tipo_nomina' => $value->p5,
                      'fecha_ingreso'=> date("Y/m/d, H:i:s", strtotime($value->p6)),
                      'fecha_corte_nomina'=> date("Y/m/d, H:i:s", strtotime($value->p7)),
                      'sueldo_mensual' => $value->p8,
                      'bono_complementario' => $value->p10,
                      'total_asignacion'=>$value->a11,
                      'total_deducion'=>$value->d11,
                      'neto_cobrar'=>$value->p9
                    ];
                    // deducciones, asignaciones
                    // dd($insert,$value);

                    if ($value->a1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Sueldo Quincenal",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a1,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima de profesión",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a2,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por antiguedad",
                        'monto' => $value->a3,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Encargaduria",
                        'monto' => $value->a4,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Salario 1/3",
                        'monto' => $value->a5,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reintegro en salario bsf",
                        'monto' => $value->a6,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias Adic. Prestaciones Soc. en Bs",
                        'monto' => $value->a7,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Feriados trabajados en Bs",
                        'monto' => $value->a8,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono nocturno en Bs",
                        'monto' => $value->a9,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Diferencia de vaciones",
                        'monto' => $value->a10,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por hijo",
                        'monto' => $value->a12,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Becas",
                        'monto' => $value->a13,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Vacaciones",
                        'monto' => $value->a14,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a15!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias adicionales de vacaciones",
                        'monto' => $value->a15,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a16!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono vacacional",
                        'monto' => $value->a16,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a18!=null) {
                        $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Part. Beneficios",
                          'monto' => $value->a18,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                        ];
                      }if ($value->a17!=null) {
                          $insertReporteIngresosSoberanos[]=[
                            'cedula' => $value->p1,
                            'conceptos' => "Utiles",
                            'monto' => $value->a17,
                            'tipo_ingreso' =>1,
                            'tipo_nomina' => $value->p5,
                            'sueldo_mensual' => $value->p8,
                            'mes' => $value->mes,
                            'quincena' => $value->quincena
                          ];
                        }if ($value->a19!=null) {
                            $insertReporteIngresosSoberanos[]=[
                              'cedula' => $value->p1,
                              'conceptos' => "Tickets Hallacas",
                              'monto' => $value->a19,
                              'tipo_ingreso' =>1,
                              'tipo_nomina' => $value->p5,
                              'sueldo_mensual' => $value->p8,
                              'mes' => $value->mes,
                              'quincena' => $value->quincena
                            ];
                          }
                          if ($value->a20!=null) {
                              $insertReporteIngresosSoberanos[]=[
                                'cedula' => $value->p1,
                                'conceptos' => "Rectroactivo feriado trabajado",
                                'monto' => $value->a20,
                                'tipo_ingreso' =>1,
                                'tipo_nomina' => $value->p5,
                                'sueldo_mensual' => $value->p8,
                                'mes' => $value->mes,
                                'quincena' => $value->quincena
                              ];
                            }
                            if ($value->a21!=null) {
                                $insertReporteIngresosSoberanos[]=[
                                  'cedula' => $value->p1,
                                  'conceptos' => "Rectroactivo bono nocturno",
                                  'monto' => $value->a21,
                                  'tipo_ingreso' =>1,
                                  'tipo_nomina' => $value->p5,
                                  'sueldo_mensual' => $value->p8,
                                  'mes' => $value->mes,
                                  'quincena' => $value->quincena
                                ];
                              }
                            if ($value->a22!=null) {
                                $insertReporteIngresosSoberanos[]=[
                                  'cedula' => $value->p1,
                                  'conceptos' => "Rectroactivo becas",
                                  'monto' => $value->a22,
                                  'tipo_ingreso' =>1,
                                  'tipo_nomina' => $value->p5,
                                  'sueldo_mensual' => $value->p8,
                                  'mes' => $value->mes,
                                  'quincena' => $value->quincena
                                ];
                              }
                    /*****************************Se agregó el 13/05/2019********************************/
                    if ($value->a23!=null) {
                      $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Retroactivo Salario",
                          'monto' => $value->a23,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                      ];
                    }

                    /**************************************************************************************

                    /*** IF DE DEDUCIONES ***/
                    if ($value->d1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Jornada Social",
                        'monto' => $value->d1,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Aporte voluntario",
                        'monto' => $value->d12,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "HCM",
                        'monto' => $value->d2,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prestamos en Bs",
                        'monto' => $value->d3,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inasistencias en Bsf",
                        'monto' => $value->d4,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reposicion Carnet en bs",
                        'monto' => $value->d5,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Descuento salario indebido",
                        'monto' => $value->d13,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Seguro social en bs",
                        'monto' => $value->d6,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Regimen prestacional de empleo en bsf",
                        'monto' => $value->d7,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro oblig para Viv en Bs",
                        'monto' => $value->d8,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Tesoreria de la Seg. Social en Bs",
                        'monto' => $value->d9,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro en Bs",
                        'monto' => $value->d10,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inces",
                        'monto' => $value->d14,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d20!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Prima por antiguedad indebido.",
                        'monto' => $value->d20,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d21!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Becas indebido",
                        'monto' => $value->d21,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                  }
                  // $insertDataModel=reporte_soberanos_cabecera::create([
                  //   'cedula' => $value->p1,
                  //   'nombre_completo' =>$value->p2,
                  //   'cargo' => $value->p3,
                  //   'departamento' => $value->p4,
                  //   'tipo_nomina' => $value->p5,
                  //   'sueldo_mensual' => $value->p8,
                  //   'mes' => $value->mes,
                  //   'quincena' => $value->quincena,
                  //   'fecha_ingreso'=>$value->p6,
                  //   'fecha_corte_nomina'=>$value->p7
                  // ]);
                  // dd($insertDataModel);
                }//key
              }//foreach
          }//foreach hoja 0
          //////Hoja Contratados
        $data= Excel::selectSheetsByIndex('1')->load($path, function($reader) {})->get();
          if(!empty($data) && $data->count()){

              //try
              foreach ($data as $key => $value) {
                if($key!=0){
                  if($value->p1!=null){
                    $insert[]=[
                      'mes' => $value->mes,
                      'quincena' => $value->quincena,
                      'cedula' => $value->p1,
                      'nombre_completo' =>$value->p2,
                      'cargo' => $value->p3,
                      'departamento' => $value->p4,
                      'tipo_nomina' => $value->p5,
                      'fecha_ingreso'=> date("d/m/Y, H:i:s", strtotime($value->p6)),
                      'fecha_corte_nomina'=> date("Y/m/d, H:i:s", strtotime($value->p7)),
                      'sueldo_mensual' => $value->p8,
                      'total_asignacion'=>$value->a11,
                      'total_deducion'=>$value->d11,
                      'neto_cobrar'=>$value->p9
                    ];
                    // ss
                    if ($value->a1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Sueldo Quincenal",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a1,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima de profesión",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a2,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por antiguedad",
                        'monto' => $value->a3,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Encargaduria",
                        'monto' => $value->a4,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Salario 1/3",
                        'monto' => $value->a5,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reintegro en salario bsf",
                        'monto' => $value->a6,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias Adic. Prestaciones Soc. en Bs",
                        'monto' => $value->a7,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Feriados trabajados en Bs",
                        'monto' => $value->a8,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono nocturno en Bs",
                        'monto' => $value->a9,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Diferencia de vaciones",
                        'monto' => $value->a10,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por hijo",
                        'monto' => $value->a12,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Becas",
                        'monto' => $value->a13,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Vacaciones",
                        'monto' => $value->a14,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a15!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias adicionales de vacaciones",
                        'monto' => $value->a15,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a16!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono vacacional",
                        'monto' => $value->a16,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a18!=null) {
                        $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Part. Beneficios",
                          'monto' => $value->a18,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                        ];
                      }if ($value->a17!=null) {
                          $insertReporteIngresosSoberanos[]=[
                            'cedula' => $value->p1,
                            'conceptos' => "Utiles",
                            'monto' => $value->a17,
                            'tipo_ingreso' =>1,
                            'tipo_nomina' => $value->p5,
                            'sueldo_mensual' => $value->p8,
                            'mes' => $value->mes,
                            'quincena' => $value->quincena
                          ];
                        }if ($value->a19!=null) {
                            $insertReporteIngresosSoberanos[]=[
                              'cedula' => $value->p1,
                              'conceptos' => "Tickets Hallacas",
                              'monto' => $value->a19,
                              'tipo_ingreso' =>1,
                              'tipo_nomina' => $value->p5,
                              'sueldo_mensual' => $value->p8,
                              'mes' => $value->mes,
                              'quincena' => $value->quincena
                            ];
                          }
                          if ($value->a20!=null) {
                              $insertReporteIngresosSoberanos[]=[
                                'cedula' => $value->p1,
                                'conceptos' => "Rectroactivo feriado trabajado",
                                'monto' => $value->a20,
                                'tipo_ingreso' =>1,
                                'tipo_nomina' => $value->p5,
                                'sueldo_mensual' => $value->p8,
                                'mes' => $value->mes,
                                'quincena' => $value->quincena
                              ];
                            }
                            if ($value->a21!=null) {
                                $insertReporteIngresosSoberanos[]=[
                                  'cedula' => $value->p1,
                                  'conceptos' => "Rectroactivo bono nocturno",
                                  'monto' => $value->a21,
                                  'tipo_ingreso' =>1,
                                  'tipo_nomina' => $value->p5,
                                  'sueldo_mensual' => $value->p8,
                                  'mes' => $value->mes,
                                  'quincena' => $value->quincena
                                ];
                              }
                    /*** IF DE DEDUCIONES ***/
                    if ($value->d1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Jornada Social",
                        'monto' => $value->d1,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Aporte voluntario",
                        'monto' => $value->d12,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "HCM",
                        'monto' => $value->d2,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prestamos en Bs",
                        'monto' => $value->d3,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inasistencias en Bsf",
                        'monto' => $value->d4,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reposicion Carnet en bs",
                        'monto' => $value->d5,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Descuento salario indebido",
                        'monto' => $value->d13,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Seguro social en bs",
                        'monto' => $value->d6,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Regimen prestacional de empleo en bsf",
                        'monto' => $value->d7,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro oblig para Viv en Bs",
                        'monto' => $value->d8,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Tesoreria de la Seg. Social en Bs",
                        'monto' => $value->d9,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro en Bs",
                        'monto' => $value->d10,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inces",
                        'monto' => $value->d14,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d20!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Prima por antiguedad indebido.",
                        'monto' => $value->d20,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d21!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Becas indebido",
                        'monto' => $value->d21,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                  }
                }//key
              }//foreach hoja 1
          }//end if
          //////Hoja alto nivel
        $data= Excel::selectSheetsByIndex('2')->load($path, function($reader) {})->get();
          if(!empty($data) && $data->count()){
              //try
              foreach ($data as $key => $value) {
                if($key!=0){
                  if($value->p1!=null){
                    $insert[]=[
                      'mes' => $value->mes,
                      'quincena' => $value->quincena,
                      'cedula' => $value->p1,
                      'nombre_completo' =>$value->p2,
                      'cargo' => $value->p3,
                      'departamento' => $value->p4,
                      'tipo_nomina' => $value->p5,
                      'fecha_ingreso'=> date("Y-m-d, H:i:s", strtotime($value->p6)),
                      'fecha_corte_nomina'=> date("Y-m-d, H:i:s", strtotime($value->p7)),
                      'sueldo_mensual' => $value->p8,
                      'bono_complementario' => $value->p10,
                      'total_asignacion'=>$value->a11,
                      'total_deducion'=>$value->d11,
                      'neto_cobrar'=>$value->p9
                    ];
                    // dd($insert[5]);
                    if ($value->a1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Sueldo Quincenal",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a1,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima de profesión",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a2,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por antiguedad",
                        'monto' => $value->a3,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Encargaduria",
                        'monto' => $value->a4,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Salario 1/3",
                        'monto' => $value->a5,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reintegro en salario bsf",
                        'monto' => $value->a6,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias Adic. Prestaciones Soc. en Bs",
                        'monto' => $value->a7,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Feriados trabajados en Bs",
                        'monto' => $value->a8,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono nocturno en Bs",
                        'monto' => $value->a9,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Diferencia de vaciones",
                        'monto' => $value->a10,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por hijo",
                        'monto' => $value->a12,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Becas",
                        'monto' => $value->a13,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Vacaciones",
                        'monto' => $value->a14,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a15!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias adicionales de vacaciones",
                        'monto' => $value->a15,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a16!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono vacacional",
                        'monto' => $value->a16,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a18!=null) {
                        $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Part. Beneficios",
                          'monto' => $value->a18,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                        ];
                      }if ($value->a17!=null) {
                          $insertReporteIngresosSoberanos[]=[
                            'cedula' => $value->p1,
                            'conceptos' => "Utiles",
                            'monto' => $value->a17,
                            'tipo_ingreso' =>1,
                            'tipo_nomina' => $value->p5,
                            'sueldo_mensual' => $value->p8,
                            'mes' => $value->mes,
                            'quincena' => $value->quincena
                          ];
                        }if ($value->a19!=null) {
                            $insertReporteIngresosSoberanos[]=[
                              'cedula' => $value->p1,
                              'conceptos' => "Tickets Hallacas",
                              'monto' => $value->a19,
                              'tipo_ingreso' =>1,
                              'tipo_nomina' => $value->p5,
                              'sueldo_mensual' => $value->p8,
                              'mes' => $value->mes,
                              'quincena' => $value->quincena
                            ];
                          }
                          if ($value->a20!=null) {
                              $insertReporteIngresosSoberanos[]=[
                                'cedula' => $value->p1,
                                'conceptos' => "Rectroactivo feriado trabajado",
                                'monto' => $value->a20,
                                'tipo_ingreso' =>1,
                                'tipo_nomina' => $value->p5,
                                'sueldo_mensual' => $value->p8,
                                'mes' => $value->mes,
                                'quincena' => $value->quincena
                              ];
                            }
                            if ($value->a21!=null) {
                                $insertReporteIngresosSoberanos[]=[
                                  'cedula' => $value->p1,
                                  'conceptos' => "Rectroactivo bono nocturno",
                                  'monto' => $value->a21,
                                  'tipo_ingreso' =>1,
                                  'tipo_nomina' => $value->p5,
                                  'sueldo_mensual' => $value->p8,
                                  'mes' => $value->mes,
                                  'quincena' => $value->quincena
                                ];
                              }
/*****************************Se agregó el 13/05/2019********************************/
                    if ($value->a22!=null) {
                      $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Retroactivo Salario",
                          'monto' => $value->a21,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                      ];
                    }
/**************************************************************************************
                    /*** IF DE DEDUCIONES ***/
                    if ($value->d1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Jornada Social",
                        'monto' => $value->d1,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Aporte voluntario",
                        'monto' => $value->d12,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "HCM",
                        'monto' => $value->d2,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prestamos en Bs",
                        'monto' => $value->d3,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inasistencias en Bsf",
                        'monto' => $value->d4,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reposicion Carnet en bs",
                        'monto' => $value->d5,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Descuento salario indebido",
                        'monto' => $value->d13,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Seguro social en bs",
                        'monto' => $value->d6,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Regimen prestacional de empleo en bsf",
                        'monto' => $value->d7,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro oblig para Viv en Bs",
                        'monto' => $value->d8,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Tesoreria de la Seg. Social en Bs",
                        'monto' => $value->d9,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro en Bs",
                        'monto' => $value->d10,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inces",
                        'monto' => $value->d14,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d20!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Prima por antiguedad indebido.",
                        'monto' => $value->d20,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d21!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Becas indebido",
                        'monto' => $value->d21,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                  // dd($insertDataModel);
                }//key
              }//foreach
          }//end if

          //////Hoja adm
        $data= Excel::selectSheetsByIndex('3')->load($path, function($reader) {})->get();
          if(!empty($data) && $data->count()){
              //try
              foreach ($data as $key => $value) {
                if($key!=0){
                  if($value->p1!=null){
                    $insert[]=[
                      'mes' => $value->mes,
                      'quincena' => $value->quincena,
                      'cedula' => $value->p1,
                      'nombre_completo' =>$value->p2,
                      'cargo' => $value->p3,
                      'departamento' => $value->p4,
                      'tipo_nomina' => $value->p5,
                      'fecha_ingreso'=> date("Y/m/d, H:i:s", strtotime($value->p6)),
                      'fecha_corte_nomina'=> date("Y/m/d, H:i:s", strtotime($value->p7)),
                      'sueldo_mensual' => $value->p8,
                      'bono_complementario' => $value->p10,
                      'total_asignacion'=>$value->a11,
                      'total_deducion'=>$value->d11,
                      'neto_cobrar'=>$value->p9
                    ];
                    if ($value->a1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Sueldo Quincenal",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a1,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima de profesión",
                        // 'unidad' =>$value->a1_y_d1,
                        'monto' => $value->a2,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por antiguedad",
                        'monto' => $value->a3,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Encargaduria",
                        'monto' => $value->a4,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Salario 1/3",
                        'monto' => $value->a5,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  } if ($value->a6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reintegro en salario bsf",
                        'monto' => $value->a6,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias Adic. Prestaciones Soc. en Bs",
                        'monto' => $value->a7,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Feriados trabajados en Bs",
                        'monto' => $value->a8,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono nocturno en Bs",
                        'monto' => $value->a9,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Diferencia de vaciones",
                        'monto' => $value->a10,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prima por hijo",
                        'monto' => $value->a12,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Becas",
                        'monto' => $value->a13,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Vacaciones",
                        'monto' => $value->a14,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a15!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Dias adicionales de vacaciones",
                        'monto' => $value->a15,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                  }if ($value->a16!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Bono vacacional",
                        'monto' => $value->a16,
                        'tipo_ingreso' =>1,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->a18!=null) {
                        $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Part. Beneficios",
                          'monto' => $value->a18,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                        ];
                      }if ($value->a17!=null) {
                          $insertReporteIngresosSoberanos[]=[
                            'cedula' => $value->p1,
                            'conceptos' => "Utiles",
                            'monto' => $value->a17,
                            'tipo_ingreso' =>1,
                            'tipo_nomina' => $value->p5,
                            'sueldo_mensual' => $value->p8,
                            'mes' => $value->mes,
                            'quincena' => $value->quincena
                          ];
                        }if ($value->a19!=null) {
                            $insertReporteIngresosSoberanos[]=[
                              'cedula' => $value->p1,
                              'conceptos' => "Tickets Hallacas",
                              'monto' => $value->a19,
                              'tipo_ingreso' =>1,
                              'tipo_nomina' => $value->p5,
                              'sueldo_mensual' => $value->p8,
                              'mes' => $value->mes,
                              'quincena' => $value->quincena
                            ];
                          }
                          if ($value->a20!=null) {
                              $insertReporteIngresosSoberanos[]=[
                                'cedula' => $value->p1,
                                'conceptos' => "Rectroactivo feriado trabajado",
                                'monto' => $value->a20,
                                'tipo_ingreso' =>1,
                                'tipo_nomina' => $value->p5,
                                'sueldo_mensual' => $value->p8,
                                'mes' => $value->mes,
                                'quincena' => $value->quincena
                              ];
                            }
                            if ($value->a21!=null) {
                                $insertReporteIngresosSoberanos[]=[
                                  'cedula' => $value->p1,
                                  'conceptos' => "Rectroactivo bono nocturno",
                                  'monto' => $value->a21,
                                  'tipo_ingreso' =>1,
                                  'tipo_nomina' => $value->p5,
                                  'sueldo_mensual' => $value->p8,
                                  'mes' => $value->mes,
                                  'quincena' => $value->quincena
                                ];
                              }
                    if ($value->a23!=null) {
                      $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Retroactivo Salario",
                          'monto' => $value->a23,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                      ];
                    }
                    if ($value->a24!=null) {
                      $insertReporteIngresosSoberanos[]=[
                          'cedula' => $value->p1,
                          'conceptos' => "Reintegro Encargaduria",
                          'monto' => $value->a24,
                          'tipo_ingreso' =>1,
                          'tipo_nomina' => $value->p5,
                          'sueldo_mensual' => $value->p8,
                          'mes' => $value->mes,
                          'quincena' => $value->quincena
                      ];
                    }
                    /*** IF DE DEDUCIONES ***/
                    if ($value->d1!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Jornada Social",
                        'monto' => $value->d1,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d12!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Aporte voluntario",
                        'monto' => $value->d12,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d2!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "HCM",
                        'monto' => $value->d2,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d3!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Prestamos en Bs",
                        'monto' => $value->d3,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d4!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inasistencias en Bsf",
                        'monto' => $value->d4,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d5!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Reposicion Carnet en bs",
                        'monto' => $value->d5,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d13!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Descuento salario indebido",
                        'monto' => $value->d13,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d6!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Seguro social en bs",
                        'monto' => $value->d6,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d7!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Regimen prestacional de empleo en bsf",
                        'monto' => $value->d7,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d8!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro oblig para Viv en Bs",
                        'monto' => $value->d8,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d9!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Tesoreria de la Seg. Social en Bs",
                        'monto' => $value->d9,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d10!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Fondo de ahorro en Bs",
                        'monto' => $value->d10,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }if ($value->d14!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Inces",
                        'monto' => $value->d14,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d20!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Prima por antiguedad indebido.",
                        'monto' => $value->d20,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                    if ($value->d21!=null) {
                      $insertReporteIngresosSoberanos[]=[
                        'cedula' => $value->p1,
                        'conceptos' => "Desc Becas indebido",
                        'monto' => $value->d21,
                        'tipo_ingreso' =>2,
                        'tipo_nomina' => $value->p5,
                        'sueldo_mensual' => $value->p8,
                        'mes' => $value->mes,
                        'quincena' => $value->quincena
                      ];
                    }
                  }
                  }
                  // dd($insertDataModel);
                }//key
              }//foreach
          }//end if
          try{
            DB::beginTransaction();
            foreach($insert as $filas){
              $insertDataModel=reporte_soberanos_cabecera::create($filas);

            }//foreach filas
            foreach($insertReporteIngresosSoberanos as $filasIngresos){
              $insertDataModel=reporte_soberanos_ingresos::create($filasIngresos);
            }//foreach filas
            DB::commit();
            return redirect('/CargaMasiva')->with('success',"Registro almacenado correctamente");
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect('/CargaMasiva')->with('error',"Ocurrio un error al hacer el regitro".$e->getMessage());
          }//
        }else {
          Session::flash('error', 'Archivo es una '.$extension.' cargue un archivo xls/csv válido!!');
          return back();
        }
      }
    }
  }
