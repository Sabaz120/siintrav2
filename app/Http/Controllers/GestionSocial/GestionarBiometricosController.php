<?php

namespace App\Http\Controllers\GestionSocial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\Modelos\GestionSocial\GestionarBiometricos;

class GestionarBiometricosController extends Controller
{
  public function registrarBiometricos(Request $request){
    try{
      DB::beginTransaction();
      $biometricos = new GestionarBiometricos;
      $biometricos->ip=$request->ip;
      $biometricos->estado=1;
      $biometricos->save();
      DB::commit();

      return response()->json(['error'=>0,'mensaje'=>'Biometrico registrado con exito.' ]);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
    }
  }

  public function obtenerBiometricos(){
      $biometricos=GestionarBiometricos::all();
      return response()->json(['error'=>0,'biometricos'=>$biometricos]);
    return response()->json(['error'=>1]);
  }

  public function actualizarBiometricos(Request $request){
    try{
      DB::beginTransaction();
      $biometricos = GestionarBiometricos::find($request->biometrico_id);
      $biometricos->ip=$request->ip;
      $biometricos->estado=1;
      $biometricos->update();
      DB::commit();

      return response()->json(['error'=>0,'mensaje'=>'Biometrico actualizado con exito.' ]);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
    }
  }

  public function borrarBiometricos($id){
    
    $biometricos= GestionarBiometricos::findOrFail($id);
    $biometricos->delete();
    return response()->json(['error'=>0,'mensaje'=>'Biometrico eliminado']);
    //return response()->json(['error'=>1]);
  }

}
