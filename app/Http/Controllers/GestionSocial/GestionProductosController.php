<?php

namespace App\Http\Controllers\GestionSocial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\Modelos\GestionSocial\GestionarProductos;
use App\Modelos\GestionSocial\DetalleJornada;

class GestionProductosController extends Controller
{

  public function obtenerProductos(){
      $productos=GestionarProductos::with('clasificacion')->get();
      return response()->json(['error'=>0,'productos'=>$productos]);
    return response()->json(['error'=>1]);
  }

  public function obtenerProductosAsociados(Request $request){
    $productos=GestionarProductos::where('clasificacion_productos_id',$request->clasificacion_id)->with('clasificacion')->get();
    return response()->json(['error'=>0,'productos'=>$productos]);
    return response()->json(['error'=>1]);
  }

  public function obtenerProductosRegistrados(Request $request){
    $productos=DetalleJornada::where('jornada_id',$request->jornada_id)->with('producto.clasificacion','producto')->get();
    return response()->json(['error'=>0,'productos'=>$productos, '']);
    return response()->json(['error'=>1]);
  }

  public function registrarProductos(Request $request){
    try{
      DB::beginTransaction();
      $productos = new GestionarProductos;
      $productos->nombre=ucfirst($request->nombreProducto);
      $productos->clasificacion_productos_id=$request->clasificacion_id;
      $productos->save();
      DB::commit();

      return response()->json(['error'=>0,'mensaje'=>'Producto registrado con exito.' ]);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
    }
  }

  public function actualizacionProductos(Request $request){

   try{
       DB::beginTransaction();
       $productos=GestionarProductos::where('id',$request->id)->first();
       $productos->nombre=ucfirst($request->nombreProducto);
       $productos->clasificacion_productos_id=$request->clasificacion_id;
       $productos->update();
       DB::commit();

       return response()->json(['error'=>0,'mensaje'=>'Actualizacion satisfactoria.' ]);
     }catch (Exception $e){
       DB::rollBack();
       return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
     }
 }


   public function deshabilitarProductos(Request $request){
     try{
       DB::beginTransaction();
       $validator=Validator::make($request->all(), [
           'id' => 'required'
       ]);
       if ($validator->fails()) {
         return response()->json(['error'=>1,'mensajes'=>$validator->errors()]);
       }
       $productos=GestionarProductos::where('id',$request->id)->first();
       $productos->delete();
       DB::commit();
       return response()->json(['error'=>0,'mensaje'=>'Producto eliminado exitosamente.']);
     }catch (Exception $e){
       DB::rollBack();
       return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
     }
   }





}
