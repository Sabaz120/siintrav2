<?php

namespace App\Http\Controllers\GestionSocial\Reportes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon;
use Excel;
use DB;
use App\Modelos\GestionSocial\Trabajador;
use App\Modelos\GestionSocial\EntregaBeneficios;
use App\Modelos\GestionSocial\Jornadas;
use App\Modelos\GestionSocial\DetalleJornada;
use App\Modelos\Sigesp\srh_gerencia;
use App\Modelos\Sigesp\sno_personal;

class entregasBeneficiosController extends Controller
{

    public function index(){

      return view('gestionSocial.reportes.index');

    }

    public function entregasBeneficios($id){
      $jornada = Jornadas::find($id);
      $detalleJornada = DetalleJornada::where('jornada_id', $id)->first();
      $entregas=EntregaBeneficios::where('detalle_jornada_id', $detalleJornada->id)->get();
      $data = ['jornada' => $jornada, 'detalleJornada' => $detalleJornada, 'entregas' => $entregas];

      $mytime = \Carbon\Carbon::now();
      Excel::create($mytime->toDateTimeString().' REPORTE ENTREGA DE BENEFICIOS', function($excel) use($data) {
        $excel->sheet("Personal", function ($sheet) use($data) {
          $sheet->loadView('gestionSocial.jornadas.reportes.entregasBeneficios')->with('data', $data);
        });
      })->export('xls');
    }

    public function estadisticas($id){

      $presidenciaCod = "0000001000";
      $presidenciaCant = 0;
      $consultoriaCod = "0000002000";
      $consultoriaCant = 0;
      $gotCod = "0000003000";
      $gotCant = 0;
      $ensamblajeCod = "0000004000";
      $ensamblajeCant = 0;
      $socioCod = "0000005000";
      $socioCant = 0;
      $asuntosCod = "0000006000";
      $asuntosCant = 0;
      $auditoriaCod = "0000008000";
      $auditoriaCant = 0;
      $adminCod = "0000009000";
      $adminCant = 0;
      $infraCod = "0000010000";
      $infraCant = 0;
      $mercaCod = "0000011000";
      $mercaCant = 0;
      $planCod = "0000012000";
      $planCant = 0;
      $tecCod = "0000013000";
      $tecCant = 0;
      $calidadCod = "0000014000";
      $calidadCant = 0;
      $seguridadCod = "0000015000";
      $seguridadCant = 0;
      $prevencionCod = "0000018000";
      $prevencionCant = 0;
      $investCod = "0000019000";
      $investCant = 0;

      $jornada = Jornadas::find($id);
      $detalleJornadas = DetalleJornada::where('jornada_id', $id)->get();

      foreach($detalleJornadas as $detalleJornada){
        $entregas=EntregaBeneficios::where('detalle_jornada_id', $detalleJornada->id)->get();
        $data = ['jornada' => $jornada, 'detalleJornada' => $detalleJornada, 'entregas' => $entregas];
      }

      foreach($data['entregas'] as $user){

        $personal_codigo_gerencia = "";
        $personal = sno_personal::datosPersonal($user->cedula);
        foreach($personal as $per){
          $personal_codigo_gerencia = $per->codigo_gerencia;
        }

        if($personal_codigo_gerencia == $presidenciaCod){
          $presidenciaCant++;
        }

        else if($personal_codigo_gerencia == $consultoriaCod){
          $consultoriaCant++;
        }
        else if($personal_codigo_gerencia == $gotCod){
          $gotCant++;
        }
        else if($personal_codigo_gerencia == $ensamblajeCod){
          $ensamblajeCant++;
        }
        else if($personal_codigo_gerencia == $socioCod){
          $socioCant++;
        }
        else if($personal_codigo_gerencia == $asuntosCod){
          $asuntosCant++;
        }
        else if($personal_codigo_gerencia == $auditoriaCod){
          $auditoriaCant++;
        }
        else if($personal_codigo_gerencia == $adminCod){
          $adminCant++;
        }
        else if($personal_codigo_gerencia == $infraCod){
          $infraCant++;
        }
        else if($personal_codigo_gerencia == $mercaCod){
          $mercaCant++;
        }
        else if($personal_codigo_gerencia == $planCod){
          $planCant++;
        }
        else if($personal_codigo_gerencia == $tecCod){
          $tecCant++;
        }
        else if($personal_codigo_gerencia == $calidadCod){
          $calidadCant++;
        }

        else if($personal_codigo_gerencia == $seguridadCod){
          $seguridadCant++;
        }

        else if($personal_codigo_gerencia == $prevencionCod){
          $prevencionCant++;
        }

        else if($personal_codigo_gerencia == $investCod){
          $investCant++;
        }

      }

      dd($presidenciaCant, $consultoriaCant, $gotCant, $ensamblajeCant, $socioCant, $asuntosCant, $auditoriaCant, $adminCant, $infraCant, $mercaCant, $planCant, $tecCant, $calidadCant, $seguridadCant, $prevencionCant, $investCant);

    }
}
