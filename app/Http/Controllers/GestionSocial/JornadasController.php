<?php

namespace App\Http\Controllers\GestionSocial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\Modelos\GestionSocial\Jornadas;
use App\Modelos\GestionSocial\GestionarProductos;
use App\Modelos\GestionSocial\DetalleJornada;
use App\Modelos\GestionSocial\Trabajador;
use App\Modelos\GestionSocial\EntregaBeneficios;
class JornadasController extends Controller
{

    public function obtenerJornadas(){
        $jornadas=Jornadas::all();
        $entregas=EntregaBeneficios::where('detalle_jornada_id',16)->get();
        return response()->json(['error'=>0,'jornadas'=>$jornadas,'entregas'=>$entregas]);
      return response()->json(['error'=>1]);
    }

    public function obtenerDetalleJornada(Request $request){
        $detalle=DetalleJornada::where('jornada_id',$request->jornada_id)->with('clasificacion','producto','jornada')->get();
        return response()->json(['error'=>0,'detalle'=>$detalle]);
      return response()->json(['error'=>1]);
    }


    public function registroJornada(Request $request){
      try{
        DB::beginTransaction();
        $validator=Validator::make($request->all(), [
            'fechaInicio'=> 'required',
            'fechaFin'=> 'required',
            'nombreJornada'=> 'required',
            'lugarJornada'=> 'required',
        ],[
          'fechaInicio.unique'=>'Debe ingresar la fecha inicio de la jornada',
          'fechaFin.required'=>'Debe ingresar la fecha fin de la jornada',
          'nombreJornada.required'=>'Debe ingresar el nombre de la jornada',
          'lugarJornada.required'=>'Debe ingresar el lugar de la jornada',
        ]);
        if ($validator->fails()) {
          return response()->json(['error'=>1,'mensaje'=>$validator->errors()]);
        }
        $jornadas = new Jornadas;
        $jornadas->nombre=ucfirst($request->nombreJornada);
        $jornadas->fecha_inicio=$request->fechaInicio;
        $jornadas->fecha_fin=$request->fechaFin;
        $jornadas->lugar=ucfirst($request->lugarJornada);
        $jornadas->hora=$request->horaJornada;
        $jornadas->save();
        DB::commit();

        return response()->json(['error'=>0,'mensaje'=>'Jornada registrada con exito.' ]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['error'=>2,'mensaje'=>$e->getMessage()]);
      }
    }

    public function actualizarJornada(Request $request){
      try{
        DB::beginTransaction();
        $jornadas = Jornadas::find($request->jornada_id);
        $jornadas->nombre=ucfirst($request->nombreJornada);
        $jornadas->fecha_inicio=$request->fechaInicio;
        $jornadas->fecha_fin=$request->fechaFin;
        $jornadas->lugar=ucfirst($request->lugarJornada);
        $jornadas->hora=$request->horaJornada;
        $jornadas->update();
        DB::commit();

        return response()->json(['error'=>0,'mensaje'=>'Registro actualizado con exito.' ]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
      }

    }




    public function asociacionProductos(Request $request){
      try{
        DB::beginTransaction();
        $detalleJornada = new DetalleJornada;
        $detalleJornada->description=ucfirst($request->descripcion);
        $detalleJornada->modalidad=ucfirst($request->modalidad_id);
        $detalleJornada->monto=$request->monto;
        $detalleJornada->jornada_id=$request->jornada_id;
        $detalleJornada->producto_id=$request->producto_id;
        $detalleJornada->save();
        DB::commit();

        return response()->json(['error'=>0,'mensaje'=>'Productos asociados con exito.' ]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
      }
    }

    public function actualizarDetalleJornada(Request $request){

      try{
        DB::beginTransaction();
        $detalleJornada = DetalleJornada::find($request->detalleJornadaId);
        $detalleJornada->description=ucfirst($request->descripcion);
        $detalleJornada->modalidad=ucfirst($request->modalidad_id);
        $detalleJornada->monto=$request->monto;
        $detalleJornada->jornada_id=$request->jornada_id;
        $detalleJornada->producto_id=$request->producto_id;
        $detalleJornada->update();
        DB::commit();

        return response()->json(['error'=>0,'mensaje'=>'Detalle de jornada actualizado.' ]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
      }

    }

}
