<?php

namespace App\Http\Controllers\GestionSocial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\GestionSocial\ClasificacionProducto;

class ClasificacionProductoController extends Controller
{
    public function store(Request $request){

      try {
        ClasificacionProducto::updateOrCreate([
          "nombre"=>$request->nombre
        ],[
          "nombre"=>$request->nombre
        ]);
        return response()->json(['msg'=>'Registro satisfactorio']);
      } catch (\Exception $e) {
        return response()->json(['msg'=>['Ha ocurrido un error en el servidor',],'errorSystem'=>$e->getMessage()],500);
      }//catch
    }
    public function index(){
      return response()->json(['data'=>ClasificacionProducto::orderBy('created_at','ASC')->get()]);
    }//index

    public function update(Request $request){
      try {
        $clasificacion=ClasificacionProducto::where('id',$request->clasificacion_id)->update(['nombre'=>$request->nombre]);
        if($clasificacion)
        return response()->json(['msg'=>'Actualización satisfactoria']);
        else
        throw new \Exception("Ocurrio un error al intentar actualizar la clasificación",401);
      } catch (\Exception $e) {
        return response()->json(['msg'=>['Ha ocurrido un error en el servidor',],'errorSystem'=>$e->getMessage()],500);
      }//catch
    }//update()

    public function destroy($id){
      try {
        $clasificacion=ClasificacionProducto::where('id',$id)->delete();//delete usa softdelete, si usa destroy lo borra completo de db.
        if($clasificacion)
        return response()->json(['msg'=>'Clasificacion eliminada satisfactoriamente.']);
        else
        throw new \Exception("Ocurrio un error al intentar borrar la clasificación",401);
      } catch (\Exception $e) {
        return response()->json(['msg'=>['Ha ocurrido un error en el servidor',],'errorSystem'=>$e->getMessage()],500);
      }//catch
    }
}
