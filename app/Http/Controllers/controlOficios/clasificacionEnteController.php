<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\ControlOficios\Ente;
use DB;
use Validator;

class clasificacionEnteController extends Controller
{
    public function mostrarClasificacionEnte(){
        $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get();
        return view('controlOficios.clasificacionEnte')->with('ClasificacionEnte',$ClasificacionEnte);
    }//public function mostrarClasificacionEnte(Request $request)

    public function registrarClasificacionEnte(Request $request){
          
        try {    
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'nombre' => 'required|min:3|max:50',
                'descripcion' =>'required|min:3|max:200',
            ],[
               'nombre.required'=>'El campo nombre es requerido',
               'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'descripcion.required'=>'El campo descripcion es requerido',
               'descripcion.min'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
               'descripcion.max'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }  
            $Clasificacion=ClasificacionEnte::where('nombre',$request->nombre)->first();
            if(count($Clasificacion)){
                return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo nombre']);
            }//if(count($Clasificacion))      
            $create=ClasificacionEnte::create($request->all()); 
            DB::commit();     
            $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get();  
            return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'ClasificacionEnte'=>$ClasificacionEnte]);
        } catch (\Exception $e) { 
            DB::rollBack();         
            return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
        }//catch()  


        

    }//public function registrarClasificacionEnte(Request $request)

    public function modificarClasificacionEnte(Request $request){

        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required',
                'nombre' => 'required|min:3|max:50',
                'descripcion' =>'required|min:3|max:200',
            ],[
               'id.required'=>'El campo id es requerido',
               'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
               'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'descripcion.required'=>'El campo descripcion es requerido',
               'descripcion.min'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
               'descripcion.max'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }
            $Clasificacion=ClasificacionEnte::where('id','<>',$request->id)->where('nombre',$request->nombre)->first();
            $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get(); 
            $error=array('No se puede duplicar el campo nombre.');
            if(count($Clasificacion)!=0){
                return response()->json(['status'=>'error','mensaje'=>$error]);  
            }// if(count($Clasificacion)!=0)
            $Clasificacion=ClasificacionEnte::where('id',$request->id)->first();
            $Clasificacion->nombre=ucfirst($request->nombre);
            $Clasificacion->descripcion=ucfirst($request->descripcion);
            $Clasificacion->update();
            DB::commit();
            $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria', 'ClasificacionEnte'=>$ClasificacionEnte]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function modificarClasificacionEnte(Request $request, $id)
    
    public function eliminarClasificacionEnte(Request $request){
        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required'
            ],[
                'id.required'=>'El campo id es requerido',]);
            if ($validator->fails()) {
              return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
            }
            $Ente=Ente::where('clasificacion_ente_id',$request->id)->first();
            $error=array('La clasificación a eliminar se encuentra asociada a un ente.');
            if(count($Ente)){
                return response()->json(['status'=>'error','mensaje'=> $error]);
            }
            $Clasificacion=ClasificacionEnte::where('id',$request->id)->first();
            if (count($Clasificacion)>0 ) {
                $Clasificacion->delete();
            } else {
              return response()->json(['status'=>'error','mensaje'=>array('El registro que desea eliminar no se encuentra en base de datos')]);
            }
            DB::commit();
            $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'ClasificacionEnte'=>$ClasificacionEnte]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function eliminarClasificacionEnte(Request $request, $id)
}
