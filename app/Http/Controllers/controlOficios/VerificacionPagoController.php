<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico as responsablesEmpresa;
use App\Modelos\ControlOficios\Control_oficio_productos_aprobacion_parcial;
use App\Modelos\ControlOficios\Departamentos;
use App\Modelos\ControlOficios\Control_oficio_pago as PagosOfertaEconomica;
use App\Modelos\ControlOficios\Control_oficio_oferta_economica as OfertaEconomica;
use App\Modelos\SistemaProduccionV1\Productos;
use App\Modelos\ControlOficios\Requisicion;
use App\Modelos\ControlOficios\DeduccionesRequisicion;
use Excel;
use PDF;
use DB;
use Carbon;
class VerificacionPagoController extends Controller
{
    public function index(){
      $query=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('oferta_economica',
                'oferta_economica.datos_reintegro',
                'oferta_economica.detalle_oferta',
                'oferta_economica.pagos',
                'oferta_economica.detalle_oferta.producto',
                'estado','personal_juridico',
                'usuario_creador',
                'personal_juridico.ente',
                'personal_juridico.ente.clasificacion_ente',
                'aprobacion_parcial_productos',
                'oferta_economica.requisicion.recaudos.recaudo',
                'oferta_economica.requisicion.deducciones.deduccion')
      ->orderBy('created_at','ASC');
      $query->whereIn('control_oficio_registro_oficio.id', function($query) {
        $query->select('control_oficio_registro_oficio_id')
        ->from('control_oficio_oferta_economicas');
      });
      $query->has('oferta_economica.pagos');
      $query->whereHas('oferta_economica',function($query){
        $query->whereHas('pagos',function($query){
          $query->where('tipo_pago','transferencia');
        });
      });
      $pagos=$query->get();
      //Metodo para organizar los datos a mostrar
      $pagos=$this->OrganizarConsulta($pagos);
      return view('controlOficios.VerificacionPago.verificacionPago',compact('pagos'));
    }//public function index()

    public function verificarPagos(Request $request){
     
      $query=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('oferta_economica',
                'oferta_economica.datos_reintegro',
                'oferta_economica.detalle_oferta',
                'oferta_economica.pagos',
                'oferta_economica.detalle_oferta.producto',
                'estado',
                'personal_juridico',
                'personal_natural',
                'usuario_creador',
                'personal_juridico.ente',
                'personal_juridico.ente.clasificacion_ente',
                'aprobacion_parcial_productos',
                'oferta_economica.requisicion.recaudos.recaudo',
                'oferta_economica.requisicion.deducciones.deduccion')
      ->orderBy('created_at','ASC');
      $query->whereIn('control_oficio_registro_oficio.id', function($query) {
        $query->select('control_oficio_registro_oficio_id')
        ->from('control_oficio_oferta_economicas');
      });
      $query->has('oferta_economica.pagos');
      //$query->whereBetween('control_oficio_registro_oficio.created_at',[$request->fecha_inicio,$request->fecha_fin]);
      $query->whereDate('created_at','>=',$request->fecha_inicio);
      $query->whereDate('created_at','<=',$request->fecha_fin);

      //Busqueda dependiendo del tipo de solicitante 1 para persona natural 2 para persona juridica
      if($request->tipo_solicitante==1){
      
        $query->whereHas('personal_natural',function($query) use($request){
          $query->where('cedula',$request->solicitante);
        });
      }//if($request->tipo_solicitante==1)
      else{
      
        $query->whereHas('personal_juridico',function($query) use($request){
          $query->where('rif_cedula_situr',$request->solicitante);
        });

      }//else if($request->tipo_solicitante==1)
      $query->whereHas('oferta_economica',function($query){
        $query->whereHas('pagos',function($query){
          $query->where('tipo_pago','transferencia');
        });
      });
      $pagos=$query->get();
      //Metodo para organizar los datos a mostrar
      $pagos=$this->OrganizarConsulta($pagos);
      return $pagos;

    }//public function verificacionPagos()

    public function verificarPagosPdf($id){
      $query=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('oferta_economica',
                'oferta_economica.datos_reintegro',
                'oferta_economica.detalle_oferta',
                'oferta_economica.pagos',
                'oferta_economica.detalle_oferta.producto',
                'estado','personal_juridico',
                'usuario_creador',
                'personal_juridico.ente',
                'personal_juridico.ente.clasificacion_ente',
                'aprobacion_parcial_productos')
      ->orderBy('created_at','ASC');
      $query->whereIn('control_oficio_registro_oficio.id', function($query) {
        $query->select('control_oficio_registro_oficio_id')
        ->from('control_oficio_oferta_economicas');
      });
      $query->has('oferta_economica.pagos');
      $query->whereHas('oferta_economica',function($query){
        $query->whereHas('pagos',function($query){
          $query->where('tipo_pago','transferencia');
        });
      });
      $pago=$query->where('id',$id)->get();
      $soportes= array();
      foreach($pago as $registro){
        foreach($registro->oferta_economica->pagos as &$soporte){
            if($soporte->soporte_lote!=null){
              if($soporte->tipo_pago=="transferencia")
                array_push($soportes,$soporte->soporte_lote);
            }//if($soporte->soporte_lote!=null)
        }//foreach($registro->oferta_economica->pagos as &$soporte)
      }//foreach($pago as &$registro)


      $pdf = PDF::loadView('controlOficios.VerificacionPago.reporte.verificacionPagoPdf',
      ["Soportes"=>$soportes]);
      $pdf->setPaper('a4', 'landscape');
      $mytime = \Carbon\Carbon::now();
      return $pdf->stream($mytime->toDateTimeString().'Verificación de Pago.pdf');
    }//public function verificarPagosPdf

    public function OrganizarConsulta($pagos){
      foreach($pagos as &$registro){
        $registro['fecha_emision']=voltearFecha($registro->fecha_emision);
        $registro['solicitante']= $registro->personal_natural->nombre.' '.$registro->personal_natural->apellido;
        $registro['fecha_aprobacion']=  voltearFecha(substr($registro->aprobacion_parcial_productos[0]->created_at,0,10));
        $registro['agente_retencion']= 'No';
        if(count($registro->personal_juridico)>0){
          $registro['solicitante']=$registro->personal_juridico->nombre_institucion.' / '. $registro['solicitante'];
          if($registro->personal_juridico->agente_retencion==true)
             $registro['agente_retencion']= 'Si';
        }//if(count($registro->personal_juridico)>0)
        //Agente de retención

        $retencion="";
     
         if($registro->oferta_economica->requisicion!=null)
        foreach($registro->oferta_economica->requisicion->deducciones as $deducciones){
          if($retencion==""){
            $retencion=$deducciones->nombre."(".$deducciones->tipo_deduccion.")";
          }else{
            $retencion=$retencion.','.$deducciones->nombre."(".$deducciones->tipo_deduccion.")";
          }//else  if($retencion=="")
        }// foreach($registro->oferta_economica->requisicion->deducciones as $deducciones)
        
        if($retencion==""){
          $retencion="Sin Información";
        }//if($retencion=="")
        $registro['retencion']= $retencion;
        $modelos_solicitados="";
        $cantidad=0;
        foreach($registro->aprobacion_parcial_productos as $producto){
          $cantidad+=$producto->cantidad;
          $producto=Productos::find($producto->producto_id);
          if($modelos_solicitados=="")
            $modelos_solicitados=$producto->nombre;
          else  
            $modelos_solicitados=$modelos_solicitados.','.$producto->nombre;
        }//foreach($registro->aprobacion_parcial_productos as $producto)
        $registro['modelos_solicitados']=  $modelos_solicitados;   
        $registro['cantidad_total_solicitada']=  $cantidad;
       
      }//foreach($pagos as &$registro)
      return $pagos;
    }//public function OrganizarConsulta($pago)
}