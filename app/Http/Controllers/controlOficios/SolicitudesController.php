<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\SolicitudDetal;
use App\Modelos\ControlOficios\DetalleSolicitudDetal;

//Productos - modalidades - disponibilidad
use DB;
use App\Modelos\SistemaProduccionV1\ExistenciaAlmacenDespacho;
use App\Modelos\SistemaProduccionV1\Productos;
use App\Modelos\ControlOficios\Control_oficio_personal_natural;

class SolicitudesController extends Controller
{
  public $productos=[];
  public $productosSistProd=[];

  public function loadGlobalVars(){

    //////////////////////////Inicio carga modalidad / disponibilidad
    $this->productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
    if(count($this->productosSistProd)==0){
      foreach($this->productos as $producto){
        $arreglo=[];
        $modalidades=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->select('modalidad')->distinct()->get();
        foreach($modalidades as $modalidad){
          $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$producto->nombre)->where('modalidad',$modalidad->modalidad)->sum('cantidadEnvia');
          if($disponibilidad>0){
            $disponibilidad=$disponibilidad-obtenerComprometido($producto->id,$modalidad->modalidad);
          }//Si disponibilidad > 0
          $arreglo[]=[
            'modalidad'=>$modalidad->modalidad,
            'disponibilidad'=>$disponibilidad,
            'nombre'=>$producto->nombre,
            'id'=>$producto->id,
            'descripcion'=>$producto->descripcion,
          ];
        }
        $this->productosSistProd[str_slug($producto->nombre)]=$arreglo;
      }//productos
    }
    ////////////////////////////////Fin carga modalidad / disponibilidad

  }//_construct

    public function index(){
      $this->loadGlobalVars();
      $pn=\App\Modelos\SistemaProduccionV1\Clientes::find(4);
      // dd($pn);
      // $pn=Control_oficio_personal_natural::find(223);

      $solicitudes=[];
      // $solicitudes=SolicitudDetal::with('detalle')->get();
      return view('controlOficios.AgenteAutorizado.solicitudesDetal.index',[
        'pn'=>$pn,
        'solicitudes'=>$solicitudes,
        'productos'=>$this->productos,
        'productosSistProd'=>$this->productosSistProd,
      ]);

    }
}
