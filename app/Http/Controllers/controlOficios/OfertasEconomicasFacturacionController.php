<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_detalle_oferta_economica as detOfertaEconomica;
use App\Modelos\ControlOficios\Control_oficio_oferta_economica as OfertaEconomica;
use Auth;
use DB;
use App\Modelos\ControlOficios\Control_oficio_despacho as Despachos;

class OfertasEconomicasFacturacionController extends Controller
{

    // OBTENER OFICIOS PARA ASGINAR FECHA DE DESPACHO
    public function obtenerOficios(){
      return Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('detalle_evento','despacho','despacho.detalle','despacho.responsable','despacho.chofer','despacho.vehiculo','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.pagos','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
      ->whereIn('estado_aprobacion_id',[6,7])//En proceso de facturacion, en proceso de despacho
      ->where('motivo_solicitud_id',1)//Solo oficios de jornada de telefono
      ->orderBy('created_at','ASC')->get();
      //estado aprobacion 3 = parcial, 2= completa
    }//obtenerOficios()

    // Vista lista de facturación
    public function listaFacturacion(){
      $oficios=$this->obtenerOficios();
      $oficios=$this->transformadorOficios($oficios);
      return view('controlOficios.OfertasEconomicasFacturacion/lista',['oficios'=>$oficios]);
    }//listaFacturacion

    //Almacenar plan de despacho
    public function guardarDatosDespacho(Request $request){
      //Recibe: oficio_id, num_factura, fecha_despacho
      try {
        DB::beginTransaction();
        $despacho=Despachos::where('registro_oficio_id',$request->oficio_id)->first();
        $despacho->fecha_despacho=voltearFecha($request->fecha_despacho);//Voltea la fecha ya que la recibe en formato: dd-mm-yyyy
        $despacho->update();
        $oferta=OfertaEconomica::where('id',$request->oferta_economica_id)->first();
        $oferta->codigo_factura=$request->num_factura;
        $oferta->update();
        $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
        $oficio->estado_aprobacion_id=7;//EN proceso de despacho
        $oficio->update();
        DB::commit();
        $oficios=$this->obtenerOficios();//
        $oficios=$this->transformadorOficios($oficios);
        return response()->json(['error'=>0,'msg'=>'Plan de despacho almacenado exitosamente.','oficios'=>$oficios]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
      }//catch()
    }//guardarDatosDespacho()


    //Confirmar despacho
    public function confirmarDespacho(Request $request){
      //Recibe: oferta_economica_id,observacion
      try {
        DB::beginTransaction();
        if(empty($request->observacion))
          $request->observacion=null;
        $despacho=Despachos::where('registro_oficio_id',$request->oficio_id)->first();
        $despacho->observacion=$request->observacion;//Observación de finalización de despacho
        $despacho->update();
        $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
        $oficio->estado_aprobacion_id=8;//Finalizado
        $oficio->update();
        DB::commit();
        $oficios=$this->obtenerOficios();//
        $oficios=$this->transformadorOficios($oficios);
        return response()->json(['error'=>0,'msg'=>'Oferta económica cerrada exitosamente.','oficios'=>$oficios]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
      }//catch()
    }//confirmarDespacho()

    // TRANSFORMADOR OFICIOS
    public function transformadorOficios($oficios){
      $user=Auth::user();
      $arreglo=[];
      foreach($oficios as $oficio){
        $monto_total=0;
        $personal_juridico=null;
        $cantidad_total=0;
        $ente='';
        if($oficio->personal_juridico!=null){
          $personal_juridico=[
            'rif'=>$oficio->personal_juridico->rif_cedula_situr,
            'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
            'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
            'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
            'ente'=>$oficio->personal_juridico->ente->nombre,
            'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
          ];
          $ente=' / '.$oficio->personal_juridico->ente->nombre;
        }//si existe personal juridico
        $pagos=$oficio->oferta_economica->pagos;
        foreach($pagos as $pago){
          $monto_total=$monto_total+$pago->monto;
        }//pagos
        $productos=$oficio->oferta_economica->detalle_oferta;
        foreach($productos as $producto){
          $cantidad_total=$cantidad_total+$producto->cantidad;
        }//foreach productos oferta economica
        $arreglo[]=[
          'id'=>$oficio->id,
          'oferta_economica_id'=>$oficio->oferta_economica->id,
          'user_id'=>$oficio->user_id,
          'usuario'=>$oficio->usuario_creador->name,
          'numero_oficio'=>$oficio->numero_oficio,
          'estado'=>$oficio->estado->estado,
          'documento'=>$oficio->documento,
          'descripcion'=>$oficio->descripcion,
          'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
          'motivo_solicitud_id'=>$oficio->motivo_solicitud->id,
          'personal_natural'=>[
            'id'=>$oficio->personal_natural_id,
            'cedula'=>$oficio->personal_natural->cedula,
            'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
            'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
            'telefono'=>$oficio->personal_natural->telefono,
            'correo_electronico'=>$oficio->personal_natural->correo_electronico,
            'firma_personal'=>$oficio->personal_natural->firma_personal
          ],
          'personal_juridico'=>$personal_juridico,
          'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
          'cantidad_total'=>$cantidad_total,
          'monto_total'=>$monto_total,
          'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
          'estado_aprobacion_id'=>$oficio->estado_aprobacion_id
        ];
      }//oficios

      return $arreglo;
    }//transformadorOficios($param)
}
