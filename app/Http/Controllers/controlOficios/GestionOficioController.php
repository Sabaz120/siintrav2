<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Territorios\Territorios_estado;
use App\Modelos\ControlOficios\MotivoSolicitud;
use App\Modelos\ControlOficios\Control_oficio_solicitud_productos;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_detalle_evento as de;
use App\Modelos\ControlOficios\Control_oficio_materiales;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material as sm;
use App\Modelos\Sigesp\sno_personal;
use App\Modelos\SalaSituacional\personal;
use Carbon;
use Auth;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use Excel;
use PDF;
use DB;
use Illuminate\Support\Collection;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\SistemaProduccionV1\Productos;

class GestionOficioController extends Controller
{
  public function obtenerOficios(){
    return Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','solicitud_productos','solicitud_productos.producto','personal_juridico')->where('estado_aprobacion_id',1)->orderBy('id', 'asc')->get();
  }

  function cerrarOferta(Request $request){

    try{

      $controlOficio = Control_oficio_registro_oficio::where("id", $request->cerrarOfertaEconomica)->first();
      //dd($controlOficio);
          $controlOficio->estado_aprobacion_id = 8;
          $controlOficio->update();

          return response()->json(["success" => true]);



    }catch(\Exception $e){

      return response()->json(["success" => false, "err" => $e->getMessage(), "ln" => $e->getLine()]);

    }

  }

  public function registrar(){
    $related = new Collection();
    $estados=Territorios_estado::all();
    // $estados=[];
    $motivos_solicitud=MotivoSolicitud::all();
    $oficios=$this->obtenerOficios();
    $materiales = Control_oficio_materiales::all();
    //dd($oficios);
    return view('controlOficios.recepcionOficio.gestionRecepcionOficio',['estados'=>$estados,'motivos_solicitud'=>$motivos_solicitud,'oficios'=>$oficios, 'materiales' => $materiales]);
  }//registrar

  public function trazabilidad(){
    $includes=[
      'personal_juridico',
      'personal_natural',
      'estado_aprobacion',
      'oferta_economica',
      'oferta_economica.requisicion',
      'oferta_economica.requisicion.despacho',

    ];
    $oficios=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with($includes)->whereIn('estado_aprobacion_id',[2,3])->orderBy('id', 'asc')->get();
    return view('controlOficios.recepcionOficio.trazabilidadOficios',['oficios'=>$oficios]);
  }//registrar

  public function existeTrabajador($cedper){
    //retorna true si el trabajador existe en la tabla control_oficio_personal_natural
    $personal = pn::where('cedula',$cedper)->first();
    if($personal != null){
    //if(count($personal) > 0){ //conflictos merge
      return true;
    }else{
      return false;
    }
  }//existeTrabajador()

  public function validarExistenciaPersonaEnOficios($personal_natural_id,$motivo_solicitud_id){
    //Validar que un oficio no se encuentre registrado con el mismo motivo de solicitud y persona.
    //Si retorna true, es porque ya existe un oficio con esta persona

    $oficios=Control_oficio_registro_oficio::where('personal_natural_id',$personal_natural_id)->where('estado_aprobacion_id', "=", 1)->get();
    if(count($oficios)>0)
    return true;
    else
      //conflictos mergee
    /*$oficios=Control_oficio_registro_oficio::where('personal_natural_id',$personal_natural_id)->where('motivo_solicitud_id',$motivo_solicitud_id)->where('estado_aprobacion_id','!=',8)->first();
    if(count($oficios)>0){
      if($oficios->estado_aprobacion_id==4){
        //Estado rechazado
        return false;
      }else{
        return true;
      }
    }else*/
    return false;
  }//validarOficioDuplicado()
  public function registrarPost(Request $request){

    try {
      $empresa=null;

        if(!$this->existeTrabajador($request->cedper)){

          $cedula = str_pad($request->cedper,  10, "0", STR_PAD_LEFT);

          $personal = new pn;
          $personal->cedula = $request->cedper;
          // $personal->nombre = utf8_decode(sno_personal::where('cedper', $request->cedper)->value('nomper'));
          $personal->nombre = sno_personal::where('cedper', $request->cedper)->value('nomper');
          // $personal->apellido = utf8_decode(sno_personal::where('cedper', $request->cedper)->value('apeper'));
          $personal->apellido = sno_personal::where('cedper', $request->cedper)->value('apeper');
          $personal->codigo_carnet_patria = personal::where('id_codper', $cedula)->value('codigo_carnet_patria');;
          $personal->telefono = sno_personal::where('cedper', $request->cedper)->value('telmovper');
          $personal->correo_electronico = sno_personal::where('cedper', $request->cedper)->value('coreleper');
          $personal->firma_personal = false;
          $personal->trabajador_vtelca = true;
          $personal->save();

        }

      $persona_natural=pn::where('cedula',$request->cedper)->first();
      if($persona_natural == null){

        return response()->json(['error'=>1,'msg'=>'La cédula que procede a registrar no esta en base de datos.']);
      }

      if($request->tipo_solicitante_id==2){
        $empresa=pj::where('rif_cedula_situr',$request->rif_empresa)->first();
        if(count($empresa)==0)
        return response()->json(['error'=>1,'msg'=>'Este R.I.F no se encuentra registrado en base de datos.']);
      }//Si el tipo de solicitante es persona juridica
      if($request->motivo_solicitud_id==2){
        $request->cantidad=null;
      }
      if($this->validarExistenciaPersonaEnOficios($persona_natural->id,$request->motivo_solicitud_id))
        return response()->json(['error'=>1,'msg'=>'Esta persona ya esta asociada a un oficio registrado.']);

      $oficio=new Control_oficio_registro_oficio();
      $oficio->user_id=Auth::user()->id;
      $oficio->numero_oficio=$request->n_oficio;
      $oficio->fecha_emision=convertir_fecha_mysql($request->fecha_recepcion);
      $oficio->estado_id=$request->estado_id;
      // $oficio->documento=null;
      $oficio->documento=$request->documento_anexo;
      $oficio->cantidad=null;
      $oficio->descripcion=$request->descripcion;
      $oficio->motivo_solicitud_id=$request->motivo_solicitud_id;
      $oficio->personal_natural_id=$persona_natural->id;
      if($request->tipo_solicitante_id==2){
        $oficio->personal_juridico_id=$empresa->id;
      }//Si es persona juridica
      $oficio->save();
      if($request->motivo_solicitud_id==2){
        //Invitacion a evento
        $detalle_evento=new de();
        $detalle_evento->fecha=convertir_fecha_mysql($request->fecha_evento);
        $detalle_evento->hora=$request->hora_evento;
        $detalle_evento->lugar=$request->lugar_evento;
        $detalle_evento->registro_oficio_id=$oficio->id;
        $detalle_evento->save();
      }//motivo solicitud evento
      $oficios=$this->obtenerOficios();
      return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','oficios'=>$oficios]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//registrarPost
  public function borrarOficio(Request $request){
    try {
      $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
      if($oficio->motivo_solicitud_id==2){
        $detalle_oficio=de::where('registro_oficio_id',$oficio->id)->first();
        $detalle_oficio->delete();
      }//personal_juridico_id
      $productos=Control_oficio_solicitud_productos::where('control_oficio_registro_oficio_id',$oficio->id)->delete();
      $materiales=sm::where('registro_oficio_id',$oficio->id)->delete();
      $oficio->delete();
      return response()->json(['error'=>0,'msg'=>'Registro eliminado satisfactoriamente.']);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//borrarOficio()
  public function actualizarOficio(Request $request){

    //return response()->json($request->all());

    try {
      $motivos_solicitud_antiguo=0;
      $oficio=Control_oficio_registro_oficio::find($request['oficio']['id']);
      $motivos_solicitud_antiguo=$oficio->motivo_solicitud_id;
      $oficio->numero_oficio=$request['oficio']['numero_oficio'];
      $oficio->fecha_emision=convertir_fecha_mysql($request['oficio']['fecha_emision']);
      $oficio->estado_id=$request['oficio']['estado_id'];
      // $oficio->cantidad=$request['oficio']['cantidad'];
      $oficio->cantidad=null;
      $oficio->descripcion=$request['oficio']['descripcion'];
      $oficio->motivo_solicitud_id=$request['oficio']['motivo_solicitud_id'];
      if($oficio->personal_natural_id!=$request['oficio']['personal_natural_id']){
        //Validar que no este como responsable en otro oficio que aun no haya sido cerrado.
        $oficioValidacion=Control_oficio_registro_oficio::where('personal_natural_id',$request['oficio']['personal_natural_id'])->whereNotIn('estado_aprobacion_id',[8,4,5])->get();
        //Esta consulta obtiene todos los oficios donde X personal natural tenga un oficio sin rechazar o sin procesar (Culminado)
        if(count($oficioValidacion)==0)
        $oficio->personal_natural_id=$request['oficio']['personal_natural_id'];
        else
        return response()->json(['error'=>1,'msg'=>'Este responsable se encuentra asociado a un oficio actualmente.']);
      }//if
      if($request['oficio']['personal_juridico_rif']!="" || $request['oficio']['personal_juridico_rif']!=null){
        $oficio->personal_juridico_id=$request['oficio']['personal_juridico_id'];
      }
      if($motivos_solicitud_antiguo!=$request['oficio']['motivo_solicitud_id'] && $request['oficio']['motivo_solicitud_id']==2){
        //Si cambio el motivo de solicitud a tipo evento
        $detalle_evento=new de();
        $detalle_evento->fecha=convertir_fecha_mysql($request['oficio']['fecha_evento']);
        $detalle_evento->hora=$request['oficio']['hora_evento'];
        $detalle_evento->lugar=$request['oficio']['lugar_evento'];
        $detalle_evento->registro_oficio_id=$oficio->id;
        $detalle_evento->save();
        $oficio->cantidad=null;
        $oficio->descripcion=null;
      }else if($motivos_solicitud_antiguo!=$request['oficio']['motivo_solicitud_id'] && $request['oficio']['motivo_solicitud_id']!=2){
        //Si cambio el motivo de solicitud de tipo evento a otro, borrar el registro en detalle.
        $detalle_evento=de::where('registro_oficio_id',$oficio->id)->first();
        if(count($detalle_evento)>0)
        $detalle_evento->delete();
      }
      if($request['documento_anexo']!=""){
        $oficio->documento=$request['documento_anexo'];
      }
      $oficio->update();
      if(isset($request['oficio']['detalle_evento_id'])){
        //Invitacion a evento
        if($request['oficio']['detaller_evento_id'] != 0){

          $detalle_evento=de::find($request['oficio']['detalle_evento_id']);
          if(count($detalle_evento)>0){
            $detalle_evento->fecha=$request['oficio']['fecha_evento'];
            $detalle_evento->hora=$request['oficio']['hora_evento'];
            $detalle_evento->lugar=$request['oficio']['lugar_evento'];
            $detalle_evento->registro_oficio_id=$oficio->id;
            $detalle_evento->update();
          }

        }
      }//motivo solicitud evento
      $oficios=$this->obtenerOficios();
      return response()->json(['error'=>0,'msg'=>'Actualización satisfactoria.','oficios'=>$oficios]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>'Se ha producido un error en el servidor.','msgSystem'=>$e->getMessage()]);
    }
  }//actualizarOficio()
  public function asignarTlf(Request $request){
    try {
      foreach($request->telefonos as $telefono){
        $productos = Control_oficio_solicitud_productos::updateOrCreate(
          ['control_oficio_registro_oficio_id' => $request->oficio_id, 'producto_id' => $telefono['id']],
          ['cantidad' => $telefono['cantidad']]
        );
      }
      return response()->json(['error'=>0,'msg'=>'Cantidad de teléfonos registrados exitosamente.']);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//asignarTlf

  public function borrarTlf(Request $request){
    try {
      $validar=Control_oficio_solicitud_productos::where('control_oficio_registro_oficio_id',$request->oficio_id)->get();
      if(count($validar)==0){

      }else if(count($validar)<2){
        return response()->json(['error'=>1,'msg'=>'No puedes eliminar este equipo,puesto que debe estar al menos uno registrado en base de datos.']);
      }//validacion
      else{
        $tlf=Control_oficio_solicitud_productos::where('control_oficio_registro_oficio_id',$request->oficio_id)
        ->where('producto_id',$request->telefono['id'])
        ->first();
        $tlf->delete();
      }
      return response()->json(['error'=>0,'msg'=>'Teléfono eliminado exitosamente.']);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//eliminarTlf()
  ///////////////////////////////////Reportes
  public function reporteGet(){
    $estados=Territorios_estado::all();
    $motivos_solicitud=MotivoSolicitud::all();
    $clasificacion_entes=ClasificacionEnte::all();
    return view('controlOficios.recepcionOficio.reporte.reporte_seguimiento',compact('estados','motivos_solicitud','clasificacion_entes'));
  }//reporteGet()

  public function reporteAtendidosGet(){
    $estados=Territorios_estado::all();
    $motivos_solicitud=MotivoSolicitud::all();
    $clasificacion_entes=ClasificacionEnte::all();
    return view('controlOficios.recepcionOficio.reporte.reporte_atendidos',compact('estados','motivos_solicitud','clasificacion_entes'));
  }//reportePost()


  public function reporteAtendidosPost(Request $request){

    $dateInicio = \Carbon\Carbon::parse($request->fecha_inicio);
    $fechaInicio = $dateInicio->format('Y-m-d');

    $dateFin = \Carbon\Carbon::parse($request->fecha_fin);
    $fechaFin = $dateFin->format('Y-m-d');

    $result = DB::select(DB::raw("SELECT
    ro.created_at, 
    pj.nombre_institucion,
    pj.nombre_autoridad,
    ro.descripcion,
    estados.nombre as estado,
    
    sum(doe.cantidad)
      FROM control_oficios.control_oficio_detalle_oferta_economicas doe
      left join control_oficios.control_oficio_oferta_economicas oe on control_oficio_oferta_economica_id=oe.id
      join control_oficios.control_oficio_registro_oficio ro on ro.id=oe.control_oficio_registro_oficio_id
     join control_oficios.control_oficio_personal_juridico pj on pj.id=ro.personal_juridico_id
     join public.estados on estados.id=ro.estado_aprobacion_id
      where doe.created_at::text between  '".$fechaInicio."' and  '".$fechaFin."' 
      and ro.estado_aprobacion_id!=4
      group by 
      doe.control_oficio_oferta_economica_id,
      ro.id,
      oe.id,
     ro.descripcion,
     pj.nombre_institucion,
    pj.nombre_autoridad,
    estados.nombre"));

    

    $mytime = \Carbon\Carbon::now();
    Excel::create($mytime->toDateTimeString().'-Oficios atendidos', function($excel) use($result, $fechaInicio, $fechaFin) {
          $excel->sheet("Oficios atendidos", function ($sheet) use($result, $fechaInicio, $fechaFin) {
              $sheet->loadView('controlOficios.recepcionOficio.reporte.pdf.oficio_atendidos')->with('result',$result)->with('fechaInicio', $fechaInicio)->with('fechaFin', $fechaFin);
        $sheet->setColumnFormat(array(
          'I' => '0.00',
          'J' => '0.00',
          'L' => '0.00',
          'M' => '0.00',
          'N' => '0.00',
          'O' => '0.00',
        ));
      });
    })->export('xls');
  }

  public function reporteDisponibilidadProduct(){
    $productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
    return view('controlOficios.recepcionOficio.reporte.reporte_disponibilidad',compact('productos'));
  }//reporteGet()

  public function iconoFontAwesome($motivos_solicitud){
    //Si mas adelante da problemas por hace esto por nombre específico,aplicarle SLUG o ser cargado por DB los iconos y fondos.
    //tipo==1 return icon , tipo==2 return bg
    $icon_class="fa fa-user";
    $bg_class="bg-info";
    if($motivos_solicitud=="Jornada de Venta de Telefonos"){
      $icon_class="fa fa-shopping-cart";
      $bg_class="bg-info";
    }else if($motivos_solicitud=="Donativos"){
      $icon_class="fa fa-envelope";
      $bg_class="bg-danger";
    }else if($motivos_solicitud=="Invitación a Eventos"){
      $icon_class="fa fa-gift";
      $bg_class="bg-success";
    }else if($motivos_solicitud=="Intercambio de Bienes y Servicios"){
      $icon_class="fa fa-handshake-o";
      $bg_class="bg-primary";
    }
    return ['icon'=>$icon_class,'background'=>$bg_class];
  }
  public function transformadorContadorOficios($oficios){
    $arreglo=array();
    $b=0;//Bandera motivo de solicitud no existe en arreglo
    foreach($oficios as $oficio){
      $b=0;
      if(count($arreglo)>0){
        for($i=0;$i<count($arreglo);$i++){
          if($arreglo[$i]['nombre']==$oficio->motivo_solicitud->nombre){
            $b=1;
            $arreglo[$i]['cantidad']++;
            $arreglo[$i]['oficios']=array_merge($arreglo[$i]['oficios'],[$oficio]);
          }//if
        }//for
        $iconBg=$this->iconoFontAwesome($oficio->motivo_solicitud->nombre);
        if($b==0){
          $arreglo=array_merge($arreglo,[['nombre'=>$oficio->motivo_solicitud->nombre,'cantidad'=>1,'estado_id'=>$oficio->estado_id,'motivo_solicitud_id'=>$oficio->motivo_solicitud->id,'class'=>$iconBg['background'],'icon_card'=>$iconBg['icon'],'oficios'=>[$oficio]]]);
        }//if b==0
      }else{
        $iconBg=$this->iconoFontAwesome($oficio->motivo_solicitud->nombre);
        $arreglo=array_merge($arreglo,[['nombre'=>$oficio->motivo_solicitud->nombre,'cantidad'=>1,'estado_id'=>$oficio->estado_id,'motivo_solicitud_id'=>$oficio->motivo_solicitud->id,'class'=>$iconBg['background'],'icon_card'=>$iconBg['icon'],'oficios'=>[$oficio]]]);
      }
    }//foreach
    return $arreglo;
  }

  public function reportePost(Request $request){
    try {
      $raw="date(created_at) BETWEEN '$request->fecha_inicio' AND '$request->fecha_fin'";
      if($request->motivo_solicitud_id!=0){
        $raw.=" and motivo_solicitud_id=".$request->motivo_solicitud_id;
      }
      if($request->estado_id!=0){
        $raw.=" and estado_id=".$request->estado_id;
      }
      $query=Control_oficio_registro_oficio::query();
      $query->whereRaw($raw)->with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }]);
      $query->with('personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente');//Includes
      $query->orderBy('created_at','ASC');
      if($request->nombre_solicitante!="" && $request->tipo_solicitante=="natural"){
        $query->whereHas('personal_natural', function ($query) use($request) {
          $query->where('nombre', 'ilike', '%' . $request->nombre_solicitante . '%')
          ->orWhere('apellido', 'ilike', '%' . $request->nombre_solicitante . '%');
        });
      }//Nombre solicitante
      if($request->tipo_solicitante!="" && $request->tipo_solicitante=="natural"){
        //Si el tipo de solicitante es natural
        $query->whereHas('personal_natural', function ($query)  {
          $query->where('trabajador_vtelca', false);
        });
        $query->where('personal_juridico_id',null);
      }else if($request->tipo_solicitante!="" && $request->tipo_solicitante=="juridico"){
        //Si el tipo de solicitante es juridico
        $query->has('personal_juridico');
      }else if($request->tipo_solicitante!="" && $request->tipo_solicitante=="trabajador"){
        //Si el tipo de solicitante es trabajor
        $query->whereHas('personal_natural', function ($query)  {
          $query->where('trabajador_vtelca', true);
        });
      }//trabajador
      $query->where('estado_aprobacion_id', '<>', 4);
      if($request->ente_id!=0){
        $query->whereHas('personal_juridico', function ($query) use($request) {
          $query->where('ente_id', $request->ente_id);
        });
      }//ente_id
      if($request->estado_oficio_id=="Pendientes")
      $query->where('estado_aprobacion_id', 1);
      else if($request->estado_oficio_id=="Atendidas")
      $query->where('estado_aprobacion_id', 8);
      else if($request->estado_oficio_id=="EnProceso")
      $query->whereIn('estado_aprobacion_id', [3,2,6, 7, 9]);
      $oficios=$query->get();
      $solicitudesTotal = Control_oficio_registro_oficio::whereRaw($raw)->where('estado_aprobacion_id', '<>','4')->count();
      $solicitudesPendientes = Control_oficio_registro_oficio::whereRaw($raw)->where('estado_aprobacion_id', 1)->count();
      $solicitudesAtendidas = Control_oficio_registro_oficio::whereRaw($raw)->where('estado_aprobacion_id', 8)->count();
      $solicitudesProceso = Control_oficio_registro_oficio::whereRaw($raw)->whereIn('estado_aprobacion_id', [3,2,6, 7, 9])->count();

      // $oficios=Control_oficio_registro_oficio::whereRaw($raw)->with(['personal_natural'=>function($q){
      //   $q->withTrashed();
      // }])->with(['motivo_solicitud'=>function($q){
      //   $q->withTrashed();
      // }])->with('personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')->orderBy('created_at','ASC')->get();
      // $oficiosTemp=[];
      // if($request->ente_id!=0){
      //   // foreach($oficios as $oficio){
      //   for($i=0;$i<count($oficios);$i++){
      //     if($oficios[$i]->personal_juridico!=null){
      //       if($oficios[$i]->personal_juridico->ente_id==$request->ente_id){
      //         $oficiosTemp=array_merge($oficiosTemp,[$oficios[$i]]);
      //       }
      //     }
      //   }//for
      //   $oficios=$oficiosTemp;
      // }//if request->ente_id!=0
      if(count($oficios)==0)
        return response()->json(['error'=>1,'msg'=>'No existe información asociada a la búsqueda.']);
      return response()->json(['error'=>0,'msg'=>'','oficios'=>$this->transformadorContadorOficios($oficios), 'solicitudesPendientes' => $solicitudesPendientes, 'solicitudesAtendidas' => $solicitudesAtendidas, 'solicitudesProceso' => $solicitudesProceso, 'solicitudesTotal' => $solicitudesTotal]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//reportePost

  public function invertirFecha($fecha){
    //Reciba fecha: dd-mm-yyyy o yyyy-mm--dd
    $fecha_new=explode("-",$fecha);
    if(strlen($fecha_new[2])==4)
    $fecha_new=$fecha_new[2].'-'.$fecha_new[1].'-'.$fecha_new[0];
    else
    $fecha_new=$fecha_new[0].'-'.$fecha_new[1].'-'.$fecha_new[2];
    return $fecha_new;
  }//invertirFecha

  public function transformadorExcelPaginaEstadistica($oficios){
    // dd($oficios);
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $arreglo=[];//Estructura del arreglo: [Mes1=>[],Mes2=>[]]
    $c=0;
    foreach($oficios as $oficio){
      $c++;
      $b=0;//Variable bandera por si el mes no existe en el arreglo
      $b2=0;//Variable bandera por si el ente no existe en el arreglo
      if($oficio->personal_juridico!=null){
        $mes=$meses[$oficio->created_at->format("n")-1];
        $clasificacion_ente=$oficio->personal_juridico->ente->clasificacion_ente->nombre;
        $ente=$oficio->personal_juridico->ente->nombre;
        $nombre_completo_ente=$clasificacion_ente.'/'.$ente;
        for($i=0;$i<count($arreglo);$i++){
          if($arreglo[$i]['mes']==$mes){
            $b=1;
            for($l=0;$l<count($arreglo[$i]['oficios']);$l++){
              if($arreglo[$i]['oficios'][$l]['ente']==$nombre_completo_ente){
                $b2=1;
                $arreglo[$i]['oficios'][$l]['cantidad']+=1;
                break;
              }
            }//for recorre arreglo para saber si el ente ya esta.
            if($b2!=1)
            $arreglo[$i]['oficios']=array_merge($arreglo[$i]['oficios'],[['ente'=>$nombre_completo_ente,'cantidad'=>1]]);
            break;
          }
        }//for
        if($b!=1){
          $arreglo=array_merge($arreglo,[['mes'=>$mes,'oficios'=>[['ente'=>$nombre_completo_ente,'cantidad'=>1]]]]);
        }//$b1=1
        // dd($mes,$clasificacion_ente,$ente);
      }//Si es personal juridico
    }//foreach
    return json_decode(json_encode($arreglo));
  }//transformadorExcelPaginaEstadistica()

  public function generarPDF(Request $request){
    $mytime = \Carbon\Carbon::now();
    $fecha_inicio=$this->invertirFecha($request->fecha_inicio);
    $fecha_fin=$this->invertirFecha($request->fecha_fin);
    $raw="date(created_at) BETWEEN '$fecha_inicio' AND '$fecha_fin'";
    if($request->motivo_solicitud_id!=0){
      $raw.=" and motivo_solicitud_id=".$request->motivo_solicitud_id;
    }
    if($request->estado_id!=0){
      $raw.=" and estado_id=".$request->estado_id;
    }
    $query=Control_oficio_registro_oficio::query();
    $query->whereRaw($raw)->with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }]);
    $query->with('personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente');//Includes
    $query->orderBy('created_at','ASC');
    if($request->nombre_solicitante!="" && $request->tipo_solicitante=="natural"){
      //FIltro de búsqueda por nombre de solicitante solo habilitado para tipo de solicitante natural
      $query->whereHas('personal_natural', function ($query) use($request) {
        $query->where('nombre', 'ilike', '%' . $request->nombre_solicitante . '%')
        ->orWhere('apellido', 'ilike', '%' . $request->nombre_solicitante . '%');
      });
    }//Nombre solicitante
    if($request->tipo_solicitante!="" && $request->tipo_solicitante=="natural"){
      //Si el tipo de solicitante es natural
      $query->whereHas('personal_natural', function ($query)  {
        $query->where('trabajador_vtelca', false);
      });
      $query->where('personal_juridico_id',null);
    }else if($request->tipo_solicitante!="" && $request->tipo_solicitante=="juridico"){
      //Si el tipo de solicitante es juridico
      $query->has('personal_juridico');
    }else if($request->tipo_solicitante!="" && $request->tipo_solicitante=="trabajador"){
      //Si el tipo de solicitante es trabajor
      $query->whereHas('personal_natural', function ($query)  {
        $query->where('trabajador_vtelca', true);
      });
    }//trabajador
    if($request->ente_id!=0){
      $query->whereHas('personal_juridico', function ($query) use($request) {
        $query->where('ente_id', $request->ente_id);
      });
    }//ente_id
    $oficios=$query->get();

    if($request->tipo_reporte==1){
      $pdf = PDF::loadView('controlOficios.recepcionOficio.reporte.pdf.oficios',
      ["titulo"=>"REPORTE SEGUIMIENTO DE OFICIO",
      "oficios"=>$oficios,
      "fecha_inicio"=>$request->fecha_inicio,
      "fecha_fin"=>$request->fecha_fin]);
      $pdf->setPaper('a4', 'landscape');
      return $pdf->download($mytime->toDateTimeString().' REPORTE SEGUIMIENTO DE OFICIO.pdf');
      return view('controlOficios.recepcionOficio.reporte.pdf.oficios', ["titulo"=>"REPORTE SEGUIMIENTO DE OFICIO",
      "oficios"=>$oficios,
      "fecha_inicio"=>$request->fecha_inicio,
      "fecha_fin"=>$request->fecha_fin]);
    }else if($request->tipo_reporte==2){

      //dd($oficios);

      $meses=$this->transformadorExcelPaginaEstadistica($oficios);
      Excel::create($mytime->toDateTimeString().' REPORTE SEGUIMIENTO DE OFICIO', function($excel) use($oficios,$meses) {
        $excel->sheet("Personal Natural", function ($sheet) use($oficios) {
          $sheet->loadView('controlOficios.recepcionOficio.reporte.excel.personal_natural')->with('oficios',$oficios);
        });
        $excel->sheet("Personal Jurídico", function ($sheet) use($oficios) {
          $sheet->loadView('controlOficios.recepcionOficio.reporte.excel.personal_juridico')->with('oficios',$oficios);
        });
        if($meses){
          $excel->sheet("Estadística_Comunicaciones", function ($sheet) use($meses) {
            $sheet->loadView('controlOficios.recepcionOficio.reporte.excel.estadistica')->with('meses',$meses);
          });
        }//$meses!=null || > 0
      })->export('xls');
    }else{
      return redirect('/');
    }//else
  }//generarPDF()

  public function registrarMateriales(Request $request){
    $personal_id = Control_oficio_registro_oficio::where('id', $request->oficio_id)->value('personal_natural_id');
    if(!$personal_id)
    return response()->json(['error'=>1,'msg'=>'No se encontro la persona asociada a este oficio']);
    $materiales = $request->materiales;
    $now = Carbon\Carbon::now();
    foreach($materiales as $material){
      $fecha=null;
      //Ultimo oficio con aprobación parcial
      $oficioConAprobParcial=Control_oficio_registro_oficio::where('personal_natural_id', $personal_id)
      ->orderBy('control_oficio_registro_oficio.id', 'DESC')
      ->where('id','!=',$request->oficio_id)
      ->whereHas('aprobacion_parcial_materiales',function($query) use($material){
        $query->where('materiales_id',$material['id']);
      })->first();
      //Ultimo oficio con aprob completa
      $oficioConAprobCompleta=Control_oficio_registro_oficio::where('personal_natural_id', $personal_id)
      ->orderBy('control_oficio_registro_oficio.id', 'DESC')
      ->where('id','!=',$request->oficio_id)
      ->whereHas('solicitud_materiales',function($query) use($material){
        $query->where('materiales_id',$material['id']);
      })->first();
      // return response()->json(['pn'=>$personal_id,'oficio_id'=>$request->oficio_id,'parcial'=>$oficioConAprobParcial,'completa'=>$oficioConAprobCompleta]);
      if($oficioConAprobParcial && $oficioConAprobCompleta){
        //Compara quien es el mayor y luego compara fecha
        if($oficioConAprobParcial->id>$oficioConAprobCompleta->id)
          $fecha = new Carbon\Carbon($oficioConAprobParcial->created_at);
        else
          $fecha = new Carbon\Carbon($oficioConAprobCompleta->created_at);
      }else if($oficioConAprobParcial){
        //COmpara fecha de la aprob parcial
        $fecha = new Carbon\Carbon($oficioConAprobParcial->created_at);
      }else if($oficioConAprobCompleta){
        //Compara fecha de la aprob completa
        $fecha = new Carbon\Carbon($oficioConAprobCompleta->created_at);
      }else{
        //Registra normal
        //fecha =  null;
      }
      if($fecha==null){
        $material = sm::updateOrCreate(
          ['registro_oficio_id' => $request->oficio_id, 'materiales_id' => $material['id']],
          ['cantidad' => $material['cantidad']]
        );
      }else{
        //Dias desde la última vez que hizo la solicitud de X material.
        $dias = ($fecha->diff($now)->days < 1) ? 1 : $fecha->diffForHumans($now);
        if (str_slug($material['nombre']) == "madera" || str_slug($material['nombre']) == "maderas") {
          if($dias>180){
            $material = sm::updateOrCreate(
              ['registro_oficio_id' => $request->oficio_id, 'materiales_id' => $material['id']],
              ['cantidad' => $material['cantidad']]
            );
          }else
            return response()->json(['error'=>1,'msg'=>'No se puede registrar el material '.$material['nombre'].' porque no ha cumplido los 180 días desde su ultima solicitud.']);
        }else{
          //Si es un material distinto a madera, son 90 dias antes de poder volver a solicitar.
          if($dias>90){
            $material = sm::updateOrCreate(
              ['registro_oficio_id' => $request->oficio_id, 'materiales_id' => $material['id']],
              ['cantidad' => $material['cantidad']]
            );
          }else
            return response()->json(['error'=>1,'msg'=>'No se puede registrar el material '.$material['nombre'].' porque no ha cumplido los 90 días desde su ultima solicitud.']);
        }//else
      }//else
    }//materiales
    return response()->json(['error'=>0,'msg'=>'Materiales agregados exitosamente.']);

  }//registrarMateriales

  public function obtenerMateriales(Request $request){

    $oficio_id = $request->oficio_id;
    $materiales = sm::where('registro_oficio_id', $oficio_id)->get();

    foreach($materiales as $material){

      $material->nombre = Control_oficio_materiales::where('id', $material->materiales_id)->value('nombre');
      $material->id = $material->materiales_id;

    }

    return response()->json($materiales);

  }

  public function eliminarMaterial(Request $request){

    $count = sm::where('registro_oficio_id', $request->oficio_id)->where('materiales_id', $request->id)->count();
    sm::where('registro_oficio_id', $request->oficio_id)->where('materiales_id', $request->id)->delete();

    if($count > 0){
      return response()->json(true);
    }else{
      return response()->json(false);
    }

  }//eliminarMaterial()

  public function expedienteSolicitudes(){
    $oficios=[];
    $query=Control_oficio_registro_oficio::query();
    $query->where('estado_aprobacion_id',8);//Procesados
    $includes=[
      'personal_juridico','usuario_creador','personal_juridico.ente',
      'personal_juridico.ente.clasificacion_ente','personal_natural',
      'oferta_economica.requisicion','oferta_economica.pagos'
    ];
    $query->with($includes);//Includes
    $query->orderBy('updated_at','ASC');
    $query=$query->get();
    foreach($query as $oficio){
      $solicitante="";
      if($oficio->personal_juridico){
        $solicitante=$oficio->personal_juridico->ente->nombre." / ".$oficio->personal_juridico->nombre_institucion;
      }else{
        $solicitante=$oficio->personal_natural->nombre." ".$oficio->personal_natural->apellido;
      }

      try {
        if(!$oficio->oferta_economica->requisicion->numero_requisicion){
          dd($oficio);
          throw new \Exception('Acción odsadaond.',401);
        }
      } catch (\Exception $e) {
        dd("Se ha encontrado un error",$e->getMessage(),$oficio);
      }

      $oficios[]=[
        "numero_requisicion"=>$oficio->oferta_economica->requisicion->numero_requisicion,
        "solicitante"=>$solicitante,
        "id"=>$oficio->id,
        "documento"=>$oficio->documento,
        "createdAt"=>$oficio->created_at->format('d-m-Y'),
        "updatedAt"=>$oficio->updated_at->format('d-m-Y')
      ];
    }
    return view('controlOficios.recepcionOficio.reporte.expediente_solicitudes', [
      "oficios"=>$oficios
    ]);
  }//expedienteSolicitudes

  public function generarPdfPagos(Request $request){
    $mytime = \Carbon\Carbon::now();
    $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
    $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.Pagos',
    [
      "oficio"=>$oficio
    ]);
    $pdf->setPaper('a4', 'landscape');
    return $pdf->stream($mytime->toDateTimeString().' REPORTE DE PAGOS.pdf');

  }//generarPDFPagos

}
