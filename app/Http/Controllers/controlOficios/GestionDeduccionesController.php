<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;//Manejo de transacciones
use App\Modelos\ControlOficios\Deduccion;

class GestionDeduccionesController extends Controller
{
  public function index(){
    $deducciones=Deduccion::all();
    return view('controlOficios.GestionDeducciones.index',['deducciones'=>$deducciones]);
  }//gestionMaterial()

  public function store(Request $request){
    try {
      DB::beginTransaction();
      $recaudo=Deduccion::where('nombre',$request->nombre)->first();
      if(count($recaudo)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe una deducción con este nombre registrado.']);
      $recaudo = Deduccion::firstOrCreate(
        ['nombre' => $request->nombre], ['cantidad' => $request->cantidad,'tipo_deduccion'=>$request->tipo_deduccion]
      );
      DB::commit();
      $deducciones=Deduccion::all();
      return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','deducciones'=>$deducciones]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//registrarMaterial()

  public function update(Request $request){
    try {
      DB::beginTransaction();
      $deduccion=Deduccion::where('nombre',$request->nombre)->where('id','!=',$request->id)->first();
      if(count($deduccion)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe una deducción con este nombre registrado.']);
      $deduccion = Deduccion::where('id',$request->id)->update(['nombre'=>$request->nombre,'cantidad'=>$request->cantidad,'tipo_deduccion'=>$request->tipo_deduccion]);
      DB::commit();
      $deducciones=Deduccion::all();
      return response()->json(['error'=>0,'msg'=>'Actualización satisfactoria.','deducciones'=>$deducciones]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//actualizarMaterial

  public function delete(Request $request){
    try {
      DB::beginTransaction();
      $deduccion = Deduccion::where('id',$request->id)->delete();
      DB::commit();
      $deducciones=Deduccion::all();
      return response()->json(['error'=>0,'msg'=>'Deducción eliminada correctamente.','deducciones'=>$deducciones]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//eliminarMaterial()
}
