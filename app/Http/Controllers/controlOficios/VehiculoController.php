<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_vehiculo as Vehiculo;
use DB;
use Validator;
class VehiculoController extends Controller
{
    public function mostrarVehiculo(){
        $Vehiculos=Vehiculo::orderBy('modelo')->get();
        return view('controlOficios.registrarVehiculo')->with('Vehiculos',$Vehiculos);
    }//public function mostrarVehiculo()

    public function registrarVehiculo(Request $request){

        try {    
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'modelo' => 'required|min:3|max:45',
                'placa' => 'required|min:6|max:7',
            ],[
               'modelo.required'=>'Existen campos por llenar:<br>*Modelo.',
               'modelo.min'=>'El campo modelo acepta un minimo de 3 caracteres y un máximo de 45 caracteres.',
               'modelo.max'=>'El campo modelo acepta un minimo de 3 caracteres y un máximo de 45 caracteres.',

               'placa.required'=>'Existen campos por llenar:<br>*Placa.',
               'placa.min'=>'El campo placa acepta un minimo de 6 caracteres y un máximo de 7 caracteres.',
               'placa.max'=>'El campo placa acepta un minimo de 6 caracteres y un máximo de 7 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }  
            $Vehiculos=Vehiculo::where('placa',$request->placa)->first();
            if(count($Vehiculos)){
                return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo placa']);
            }//if(count($Vehiculos))      
            $create=Vehiculo::create($request->all()); 
            DB::commit();     
            $Vehiculos=Vehiculo::orderBy('modelo')->get();  
            return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'Vehiculos'=>$Vehiculos]);
        } catch (\Exception $e) { 
            DB::rollBack();         
            return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
        }//catch()  


        

    }//public function registrarVehiculo(Request $request)

    public function modificarVehiculo(Request $request){

        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'modelo' => 'required|min:3|max:45',
                'placa' => 'required|min:6|max:7',
            ],[
               'modelo.required'=>'Existen campos por llenar:<br>*Modelo.',
               'modelo.min'=>'El campo modelo acepta un minimo de 3 caracteres y un máximo de 45 caracteres.',
               'modelo.max'=>'El campo modelo acepta un minimo de 3 caracteres y un máximo de 45 caracteres.',

               'placa.required'=>'Existen campos por llenar:<br>*Placa.',
               'placa.min'=>'El campo placa acepta un minimo de 6 caracteres y un máximo de 7 caracteres.',
               'placa.max'=>'El campo placa acepta un minimo de 6 caracteres y un máximo de 7 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }  
            $Vehiculos=Vehiculo::where('id','<>',$request->id)->where('placa',$request->placa)->first();
            $error=array('No se puede duplicar el campo placa.');
            if(count($Vehiculos)!=0){
                return response()->json(['status'=>'error','mensaje'=>$error]);  
            }// if(count($Vehiculos)!=0)
            $Vehiculos=Vehiculo::where('id',$request->id)->first();
            $Vehiculos->modelo=ucfirst($request->modelo);
            $Vehiculos->placa=strtoupper($request->placa);
            $Vehiculos->descripcion=$request->descripcion;
            $Vehiculos->update();
            DB::commit();
            $Vehiculos=Vehiculo::orderBy('modelo')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'Vehiculos'=>$Vehiculos]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function modificarVehiculo(Request $request, $id)
    
}//class Vehiculo extends Controller
