<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico as responsablesEmpresa;
use App\Modelos\ControlOficios\Departamentos;
use DB;
class AsignacionResponsableController extends Controller
{

    public function obtenerResponsables($responsables){
      $array=[];
      foreach($responsables as $responsable){
        $array[]=[
          'id'=>$responsable->id,
          'personal_juridico_id'=>$responsable->personal_juridico_id,
          'personal_natural_id'=>$responsable->personal_natural_id,
          'departamento_id'=>$responsable->departamento_id,
          'responsable'=>$responsable->personal_natural->nombre.' '.$responsable->personal_natural->apellido,
          'cedula'=>$responsable->personal_natural->cedula,
          'empresa'=>$responsable->personal_juridico->nombre_institucion,
          'departamento'=>$responsable->departamento->nombre
        ];
      }
      return json_decode(json_encode($array));
    }

    public function GestionResponsables(){
      $empresas=pj::all();
      $departamentos=Departamentos::all();
      $responsables=responsablesEmpresa::with('personal_natural','personal_juridico','departamento')->get();
      $responsables=json_encode($this->obtenerResponsables($responsables));//Enviarlo como Json al vuejs
      return view('controlOficios.GestionResponsables.gestionResponsables',['Empresas'=>$empresas,'Departamentos'=>$departamentos,'Responsables'=>$responsables]);
    }//gestionResponsables()

    public function AsignarResponsable(Request $request){
      try {
        DB::beginTransaction();
        $pn=pn::where('cedula',$request->cedula)->first();
        if($pn==null)
          return response()->json(['error'=>1,'msg'=>'Esta cédula no se encuentra registrada en sistema.']);
        $e=responsablesEmpresa::where('personal_natural_id',$pn->id)
        // ->where('personal_juridico_id',$request->empresa_id)
        // ->where('departamento_id',$request->departamento_id)
        ->first();
        if($e != null)
          return response()->json(['error'=>1,'msg'=>'Este responsable ya se encuentra registrado en sistema.']);
        $empresa=new responsablesEmpresa();
        $empresa->personal_juridico_id=$request->empresa_id;
        $empresa->departamento_id=$request->departamento_id;
        $empresa->personal_natural_id=$pn->id;
        $empresa->save();
        DB::commit();
        $responsables=responsablesEmpresa::all();
        $responsables=$this->obtenerResponsables($responsables);
        return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','Responsables'=>$responsables]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
      }
    }//AsignacionResponsable()
    public function actualizarResponsable(Request $request){
      try {
        DB::beginTransaction();
        $pn=pn::where('cedula',$request->cedula)->first();
        if(count($pn)==0)
          return response()->json(['error'=>1,'msg'=>'Esta cédula no se encuentra registrada en sistema.']);
        $responsable=responsablesEmpresa::where('id',$request->id)->first();
        $e=responsablesEmpresa::where('personal_natural_id',$pn->id)
        ->where('id','!=',$responsable->id)
        // ->where('personal_juridico_id',$request->empresa_id)
        // ->where('departamento_id',$request->departamento_id)
        ->first();
        if(count($e)>0)
          return response()->json(['error'=>1,'msg'=>'Este responsable ya se encuentra registrado en sistema.']);
        $responsable->personal_juridico_id=$request->empresa_id;
        $responsable->departamento_id=$request->departamento_id;
        $responsable->personal_natural_id=$pn->id;
        $responsable->update();
        DB::commit();
        $responsables=responsablesEmpresa::all();
        $responsables=$this->obtenerResponsables($responsables);
        return response()->json(['error'=>0,'msg'=>'Registro actualizado satisfactoriamente.','Responsables'=>$responsables]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
      }
    }//AsignacionResponsable()

    public function borrarResponsable(Request $request){
      try {
        DB::beginTransaction();
        $material = responsablesEmpresa::where('id',$request->id)->delete();
        $responsables=responsablesEmpresa::all();
        $responsables=$this->obtenerResponsables($responsables);
        DB::commit();
        return response()->json(['error'=>0,'msg'=>'Responsable eliminado correctamente.','Responsables'=>$responsables]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
      }//catch
    }//borrarResponsable()
}
