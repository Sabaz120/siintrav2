<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\TiempoAtencion;
use DB;
use Validator;
class tiempoAtencionController extends Controller
{
    public function mostrarTiempoAtencion(){
        $TiempoAtencion=TiempoAtencion::orderBy('tiempo')->get();
        return view('controlOficios.configuracionTiempoAtencion')->with('TiempoAtencion',$TiempoAtencion);
    }//public function mostrarTiempoAtencion(Request $request)

    public function registrarTiempoAtencion(Request $request){

        try {    
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'tiempo' => 'required|max:5',
            ],[
               'tiempo.required'=>'Existen campos por llenar:<br>*Tiempo.',
               'tiempo.max'=>'El campo tiempo acepta máximo de 5 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }  
            $Tiempo=TiempoAtencion::where('tiempo',$request->tiempo)->first();
            if(count($Tiempo)){
                return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo tiempo']);
            }//if(count($Tiempo))      
            $create=TiempoAtencion::create($request->all()); 
            DB::commit();     
            $TiempoAtencion=TiempoAtencion::orderBy('tiempo')->get();  
            return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'TiempoAtencion'=>$TiempoAtencion]);
        } catch (\Exception $e) { 
            DB::rollBack();         
            return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
        }//catch()       

    }//public function registrarTiempoAtencion(Request $request)

    public function modificarTiempoAtencion(Request $request){

        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required',
                'tiempo' => 'required|max:5',
            ],[
               'id.required'=>'Existen campos por llenar:<br>*Id.',  
               'tiempo.required'=>'Existen campos por llenar:<br>*Tiempo.',
               'tiempo.max'=>'El campo tiempo acepta máximo de 5 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }
            $Tiempo=TiempoAtencion::where('id','<>',$request->id)->where('tiempo',$request->tiempo)->first();
            $TiempoAtencion=TiempoAtencion::orderBy('tiempo')->get(); 
            $error=array('No se puede duplicar el campo tiempo.');
            if(count($Tiempo)!=0){
                return response()->json(['status'=>'error','mensaje'=>$error]);  
            }// if(count($Tiempo)!=0)
            $Tiempo=TiempoAtencion::where('id',$request->id)->first();
            $Tiempo->tiempo=ucfirst($request->tiempo);
            $Tiempo->update();
            DB::commit();
            $TiempoAtencion=TiempoAtencion::orderBy('tiempo')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'TiempoAtencion'=>$TiempoAtencion]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function modificarTiempoAtencion(Request $request, $id)
    
    public function eliminarTiempoAtencion(Request $request){
        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required'
            ],[
                'id.required'=>'Existen campos por llenar:<br>*Id.', ]);
            if ($validator->fails()) {
              return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
            }
      
            $Tiempo=TiempoAtencion::where('id',$request->id)->first();
            if (count($Tiempo)>0 ) {
                $Tiempo->delete();
            } else {
              return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
            }
            DB::commit();
            $TiempoAtencion=TiempoAtencion::orderBy('tiempo')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'TiempoAtencion'=>$TiempoAtencion]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function eliminarTiempoAtencion(Request $request, $id)   
}//class tiempoAtencionController extends Controller
