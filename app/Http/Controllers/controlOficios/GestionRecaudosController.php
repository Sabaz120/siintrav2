<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;//Manejo de transacciones
use App\Modelos\ControlOficios\Recaudo;

class GestionRecaudosController extends Controller
{
  public function index(){
    $recaudos=Recaudo::all();
    return view('controlOficios.GestionRecaudos.index',['recaudos'=>$recaudos]);
  }//gestionMaterial()

  public function store(Request $request){
    try {
      DB::beginTransaction();
      $recaudo=Recaudo::where('nombre',$request->nombre)->first();
      if(count($recaudo)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe un recaudo con este nombre registrado.']);
      $recaudo = Recaudo::firstOrCreate(
        ['nombre' => $request->nombre], ['descripcion' => $request->descripcion]
      );
      DB::commit();
      $recaudos=Recaudo::all();
      return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','recaudos'=>$recaudos]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//registrarMaterial()

  public function update(Request $request){
    try {
      DB::beginTransaction();
      $recaudo=Recaudo::where('nombre',$request->nombre)->where('id','!=',$request->id)->first();
      if(count($recaudo)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe un recaudo con este nombre registrado.']);
      $recaudo = Recaudo::where('id',$request->id)->update(['nombre'=>$request->nombre,'descripcion'=>$request->descripcion]);
      DB::commit();
      $recaudos=Recaudo::all();
      return response()->json(['error'=>0,'msg'=>'Actualización satisfactoria.','recaudos'=>$recaudos]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//actualizarMaterial

  public function delete(Request $request){
    try {
      DB::beginTransaction();
      $recaudo = Recaudo::where('id',$request->id)->delete();
      DB::commit();
      $recaudos=Recaudo::all();
      return response()->json(['error'=>0,'msg'=>'Recaudo eliminado correctamente.','recaudos'=>$recaudos]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//eliminarMaterial()
}
