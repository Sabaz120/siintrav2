<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\NivelAtencion;
use DB;
use Validator;
class nivelAtencionController extends Controller
{
    public function mostrarNivelAtencion(){
        $NivelAtencion=NivelAtencion::orderBy('ponderacion')->get();
        return view('controlOficios.configuracionPonderacion')->with('NivelAtencion',$NivelAtencion);
    }//public function mostrarNivelAtencion(Request $request)

    public function registrarNivelAtencion(Request $request){

        try {    
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'ponderacion' => 'required|max:2',
            ],[
               'ponderacion.required'=>'Existen campos por llenar:<br>*Nivel.',
               'ponderacion.max'=>'El campo nivel acepta máximo de 2 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }  
            $Nivel=NivelAtencion::where('ponderacion',$request->ponderacion)->first();
            if(count($Nivel)){
                return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo nivel']);
            }//if(count($Nivel))      
            $create=NivelAtencion::create($request->all()); 
            DB::commit();     
            $NivelAtencion=NivelAtencion::orderBy('ponderacion')->get();  
            return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'NivelAtencion'=>$NivelAtencion]);
        } catch (\Exception $e) { 
            DB::rollBack();         
            return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
        }//catch()       

    }//public function registrarNivelAtencion(Request $request)

    public function modificarNivelAtencion(Request $request){

        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required',
                'ponderacion' => 'required|max:2',
            ],[
               'id.required'=>'El campo id es requerido',
               'ponderacion.required'=>'Existen campos por llenar:<br>*Nivel.',
               'ponderacion.max'=>'El campo nivel acepta máximo de 2 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }
            
            $Nivel=NivelAtencion::where('id','<>',$request->id)->where('ponderacion',$request->ponderacion)->first();
          
            $NivelAtencion=NivelAtencion::orderBy('ponderacion')->get(); 
          
            $error=array('No se puede duplicar el campo nivel.');
            
            if(count($Nivel)!=0){
                return response()->json(['status'=>'error','mensaje'=>$error]);  
            }// if(count($Nivel)!=0)
            $Nivel=NivelAtencion::where('id',$request->id)->first();
            $Nivel->ponderacion=$request->ponderacion;
            $Nivel->update();
            DB::commit();
            $NivelAtencion=NivelAtencion::orderBy('ponderacion')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'NivelAtencion'=>$NivelAtencion]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function modificarNivelAtencion(Request $request, $id)
    
    public function eliminarNivelAtencion(Request $request){
        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required'
            ]);
            if ($validator->fails()) {
              return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
            }
      
            $Nivel=NivelAtencion::where('id',$request->id)->first();
            if (count($Nivel)>0 ) {
                $Nivel->delete();
            } else {
              return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
            }
            DB::commit();
            $NivelAtencion=NivelAtencion::orderBy('ponderacion')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'NivelAtencion'=>$NivelAtencion]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function eliminarNivelAtencion(Request $request, $id)
}//class nivelAtencionController extends Controller
