<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\MotivoSolicitud;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico;

class personalJuridicoController extends Controller
{
    public function obtenerTodoPersonalJuridico(){
      $personalJuridico=Control_oficio_personal_juridico::with(['ente'=>function($q){
        $q->withTrashed();
      }])->get();
      $transformer=[];
      foreach($personalJuridico as $empresa){
        $transformer=array_merge($transformer,[['id'=>$empresa->id,'ente'=>$empresa->ente->nombre,'rif_empresa'=>$empresa->rif_cedula_situr,'nombre_empresa'=>$empresa->nombre_institucion,'nombre_autoridad'=>$empresa->nombre_autoridad]]);
      }
      return json_decode(json_encode($transformer));
    }//obtenerTodoPersonalJuridico()

    public function mostrarPersonalJuridico(){
      $motivos_solicitud=MotivoSolicitud::all();
      $clasificacion_entes=ClasificacionEnte::all();
      $personalJuridico=json_encode($this->obtenerTodoPersonalJuridico());
      return view('controlOficios.registrarPersonalJuridico',compact('motivos_solicitud','clasificacion_entes','personalJuridico'));
    }//mostrarPersonalJuridica()

    public function registrarPersonalJuridico(Request $request){
      try {
        $duplicado=Control_oficio_personal_juridico::where('rif_cedula_situr',$request->rif_empresa)->first();
        if(count($duplicado)>0){
          return response()->json(['error'=>1,'msg'=>'Ya existe una empresa registrada con este R.I.F. Empresa: '.$duplicado->nombre_institucion]);
        }
        $p=new Control_oficio_personal_juridico();
        $p->rif_cedula_situr=$request->rif_empresa;
        $p->nombre_institucion=$request->nombre_empresa;
        $p->nombre_autoridad=$request->nombre_autoridad;
        $p->direccion_fiscal=$request->direccion_fiscal;
        $p->agente_retencion=$request->agente_retencion;
        $p->ente_id=$request->ente_id;
        $p->save();
        $personalJuridico=$this->obtenerTodoPersonalJuridico();
        return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','personalJuridico'=>$personalJuridico]);
      } catch (\Exception $e) {
        return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
      }
    }//registrarPersonalJuridico()

    public function modificarPersonalJuridico(Request $request){
      try {
        $empresa=json_decode(json_encode($request->empresa));
        $p=Control_oficio_personal_juridico::find($empresa->id);
        if($p->rif_cedula_situr!=$empresa->rif_empresa){
          $duplicado=Control_oficio_personal_juridico::where('rif_cedula_situr',$empresa->rif_empresa)->first();
          if(count($duplicado)>0){
            return response()->json(['error'=>1,'msg'=>'Ya existe una empresa registrada con este R.I.F. Empresa: '.$duplicado->nombre_institucion]);
          }
        }//
        $p->rif_cedula_situr=$empresa->rif_empresa;
        $p->nombre_institucion=$empresa->nombreEmpresa;
        $p->nombre_autoridad=$empresa->nombre_autoridad;
        $p->direccion_fiscal=$empresa->direccion_fiscal;
        $p->agente_retencion=$empresa->agente_retencion;
        $p->ente_id=$empresa->ente_id;
        $p->update();
        $personalJuridico=$this->obtenerTodoPersonalJuridico();
        return response()->json(['error'=>0,'msg'=>'Actualización Satisfactoria.','personalJuridico'=>$personalJuridico]);
      } catch (\Exception $e) {
        return response()->json(['error'=>500,'msg'=>$e->getMessage()]);

      }
    }//modificarPersonalJuridico()

}
