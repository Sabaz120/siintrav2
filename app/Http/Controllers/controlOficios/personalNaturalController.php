<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_personal_natural;
use App\Modelos\Sigesp\sno_cargo;
use App\Modelos\Sigesp\sno_ubicacionfisica as sno_ubicacion;
use App\Modelos\Sigesp\sno_personalnomina as sno_personalnomina;
use App\Modelos\Sigesp\srh_gerencia;
use App\Modelos\Sigesp\sno_personal;
use App\Modelos\SalaSituacional\personal;
use DB;
use Validator;
class personalNaturalController extends Controller
{
  public function mostrarPersonalNatural(){
    $PersonalNatural=Control_oficio_personal_natural::orderBy('cedula')->get();
    return view('controlOficios.registrarPersonaNatural',array('PersonalNatural'=>$PersonalNatural));
  }//public function mostrarPersonalNatural()

  public function buscarPersona(Request $request){

    try{

      $cedula = str_pad($request->cedula,  10, "0", STR_PAD_LEFT);
      $personal = sno_personalnomina::where('codper', $cedula)->first();

      if($personal != null){
        $personal->cargo = sno_cargo::where('codcar', $personal->codcar)->value('descar');
        $personal->nombre = sno_personal::where('cedper', $request->cedula)->value('nomper');
        $personal->apellido = sno_personal::where('cedper', $request->cedula)->value('apeper');
        $personal->codigoCarnetPatria = personal::where('id_codper', $cedula)->value('codigo_carnet_patria');
        $personal->telefono = sno_personal::where('cedper', $request->cedula)->value('telmovper');
        $personal->correo_electronico = sno_personal::where('cedper', $request->cedula)->value('coreleper');
        $personal->codger = sno_personal::where('cedper', $request->cedula)->value('codger');
        $personal->gerencia = srh_gerencia::where('codger', $personal->codger)->value('denger');
      }

      return response()->json(['personal' => $personal]);

    }catch(\Exception $e){

      return response()->json($e->getMessage());
    }

  }

  public function registrarPersonalNatural(Request $request){
    try {
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'cedula' => 'required|min:7|max:8',
        'nombre' => 'required|min:3|max:50',
        'apellido' =>'required|min:3|max:50',
        'codigo_carnet_patria' => 'max:10',
        'telefono' => 'required|min:10|max:11',
        'correo_electronico' => 'required|email|min:10|max:50',
        'firma_personal' => 'required|boolean',
      ],[
        'cedula.required'=>'Existen campos por llenar:<br>*Cédula.',
        'cedula.unique'=>'No se pueden duplicar el campo cédula para un personal natural.',
        'cedula.min'=>'El campo cédula acepta un minimo de 7 caracteres y un máximo de 8 caracteres.',
        'cedula.min'=>'El campo cédula acepta un minimo de 7 caracteres y un máximo de 8 caracteres.',
        'nombre.required'=>'Existen campos por llenar:<br>*Nombres.',
        'nombre.min'=>'El campo nombres acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'nombre.max'=>'El campo nombres acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'apellido.required'=>'Existen campos por llenar:<br>*Apellidos.',
        'apellido.min'=>'El campo apellidos acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'apellido.max'=>'El campo apellidos acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',

        'codigo_carnet_patria.required'=>'Existen campos por llenar:<br>*Código carnet de la patria.',
        'codigo_carnet_patria.unique'=>'No se pueden duplicar el campo código de carnet de la patria para un personal natural.',
        'codigo_carnet_patria.min'=>'El campo código carnet de la patria debe contener 10 caracteres incluyendo los ceros.',
        // 'codigo_carnet_patria.max'=>'El campo código carnet de la patria debe contener 10 caracteres incluyendo los ceros.',

        'telefono.required'=>'Existen campos por llenar:<br>*Teléfono.',
        'telefono.min'=>'El campo teléfono debe contener 11 caracteres incluyendo el código de area.',
        'telefono.max'=>'El campo teléfono debe contener 11 caracteres incluyendo el código de area.',

        'correo_electronico.required'=>'Existen campos por llenar:<br>*Correo electrónico.',
        'correo_electronico.email'=>'El campo correo electrónico es Invalido.',
        'correo_electronico.min'=>'El campo correo electrónico acepta un minimo de 10 caracteres y un máximo de 50 caracteres.',
        'correo_electronico.max'=>'El campo correo electrónico acepta un minimo de 10 caracteres y un máximo de 50 caracteres.',

        'firma_personal.required'=>'Existen campos por llenar:<br>*Firma Personal.',
        'firma_personal.boolean'=>'El campo firma personal debe ser de tipo boolean',

      ]);
      if ($validator->fails()) {
        return response()->json(['statu'=>'errors','mensaje'=>$validator->errors()]);
      }
      if(!$request->codigo_carnet_patria)
        $request['codigo_carnet_patria']="0000000000";
      $PersonalNatural=Control_oficio_personal_natural::query();
      $PersonalNatural->where('cedula',$request->cedula);
      if($request->codigo_carnet_patria!="0000000000"){
        $PersonalNatural->orWhere('codigo_carnet_patria',$request->codigo_carnet_patria);
      }
      $PersonalNatural=$PersonalNatural->first();
      if($PersonalNatural != null){
        return response()->json(['status'=>'error','mensaje'=>['No se pueden duplicar el campo cédula o código de carnet de la patria para un personal natural']]);
      }//if(count($PersonalNatural))


      $create=Control_oficio_personal_natural::create($request->all());

      /*$personal = new Control_oficio_personal_natural;
      $personal->cedula = $request->cedula;
      $persona->nombre = $request->nombre;
      $persona->apellido = $request->apellido;
      $persona->codigo_carnet_patria = $request->codigo_carnet_patria;*/

      DB::commit();
      $PersonalNatural=Control_oficio_personal_natural::orderBy('cedula')->get();
      return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'PersonalNatural'=>$PersonalNatural]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
    }//catch()




  }//public function registrarPersonalNatural(Request $request)

  public function modificarPersonalNatural(Request $request){

    try{
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'id' => 'required',
        'cedula' => 'required|min:7|max:8',
        'nombre' => 'required|min:3|max:50',
        'apellido' =>'required|min:3|max:50',
        'codigo_carnet_patria' => 'required|min:10|max:10',
        'telefono' => 'required|min:11|max:11',
        'correo_electronico' => 'required|email|min:10|max:50',
        'firma_personal' => 'required|boolean',
      ],[
        'id.required'=>'El campo id es requerido',
        'cedula.required'=>'Existen campos por llenar:<br>*Cédula.',
        'cedula.min'=>'El campo cédula acepta un minimo de 7 caracteres y un máximo de 8 caracteres.',
        'cedula.min'=>'El campo cédula acepta un minimo de 7 caracteres y un máximo de 8 caracteres.',
        'nombre.required'=>'Existen campos por llenar:<br>*Nombres.',
        'nombre.min'=>'El campo nombres acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'nombre.max'=>'El campo nombres acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'apellido.required'=>'Existen campos por llenar:<br>*Apellidos.',
        'apellido.min'=>'El campo apellidos acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'apellido.max'=>'El campo apellidos acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',

        'codigo_carnet_patria.required'=>'Existen campos por llenar:<br>*Código carnet de la patria.',
        'codigo_carnet_patria.min'=>'El campo código carnet de la patria debe contener 10 caracteres incluyendo los ceros.',
        'codigo_carnet_patria.max'=>'El campo código carnet de la patria debe contener 10 caracteres incluyendo los ceros.',

        'telefono.required'=>'Existen campos por llenar:<br>*Teléfono.',
        'telefono.min'=>'El campo teléfono debe contener 11 caracteres incluyendo el código de area.',
        'telefono.max'=>'El campo teléfono debe contener 11 caracteres incluyendo el código de area.',

        'correo_electronico.required'=>'Existen campos por llenar:<br>*Correo electrónico.',
        'correo_electronico.email'=>'El campo correo electrónico es Invalido.',
        'correo_electronico.min'=>'El campo correo electrónico acepta un minimo de 10 caracteres y un máximo de 50 caracteres.',
        'correo_electronico.max'=>'El campo correo electrónico acepta un minimo de 10 caracteres y un máximo de 50 caracteres.',

        'firma_personal.required'=>'Existen campos por llenar:<br>*Firma Personal.',
        'firma_personal.boolean'=>'El campo firma personal debe ser de tipo boolean',

      ]);
      if ($validator->fails()) {
        return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
      }
      $PersonalNatural=Control_oficio_personal_natural::where('cedula',$request->cedula)->Where('id','<>',$request->id)->first();
      $error=array('No se pueden duplicar el campo cédula para un personal natural.');
      if(count($PersonalNatural)){
        return response()->json(['status'=>'error','mensaje'=>$error]);
      }//if(count($PersonalNatural))

      $PersonalNatural=Control_oficio_personal_natural::where('codigo_carnet_patria',$request->codigo_carnet_patria)->Where('id','<>',$request->id)->first();
      $error=array('No se pueden duplicar el campo código de carnet de la patria para un personal natural.');
      if(count($PersonalNatural)){
        return response()->json(['status'=>'error','mensaje'=>$error]);
      }//if(count($PersonalNatural))

      $PersonalNatural=Control_oficio_personal_natural::where('id',$request->id)->first();
      $PersonalNatural->cedula=$request->cedula;
      $PersonalNatural->nombre=$request->nombre;
      $PersonalNatural->apellido=$request->apellido;
      $PersonalNatural->codigo_carnet_patria=$request->codigo_carnet_patria;
      $PersonalNatural->telefono=$request->telefono;
      $PersonalNatural->correo_electronico=$request->correo_electronico;
      $PersonalNatural->firma_personal=$request->firma_personal;
      $PersonalNatural->update();
      DB::commit();
      $PersonalNatural=Control_oficio_personal_natural::orderBy('cedula')->get();
      return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'PersonalNatural'=>$PersonalNatural]);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
    }

  }//public function modificarPersonalNatural(Request $request, $id)

  public function eliminarPersonalNatural(Request $request){
    try{
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'id' => 'required'
      ],[
        'id.required'=>'El campo id es requerido', ]);
        if ($validator->fails()) {
          return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
        }

        $PersonalNatural=Control_oficio_personal_natural::where('id',$request->id)->first();
        if (count($PersonalNatural)>0 ) {
          $PersonalNatural->delete();
        } else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
        DB::commit();
        $PersonalNatural=Control_oficio_personal_natural::orderBy('nombre')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'PersonalNatural'=>$PersonalNatural]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
      }

    }//public function eliminarPersonalNatural(Request $request, $id)

  }//class personaNaturalController extends Controller
