<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_pago as PagosOfertaEconomica;
use App\Modelos\ControlOficios\Control_oficio_oferta_economica as OfertaEconomica;
use DB;
class ListadoReintegrosController extends Controller
{

  public function obtenerOficios(){
    $query=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('oferta_economica','oferta_economica.datos_reintegro','oferta_economica.detalle_oferta','oferta_economica.pagos','oferta_economica.detalle_oferta.producto','estado','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
    ->where('estado_aprobacion_id',4)//estado 4 denegado el oficio
    ->orderBy('created_at','ASC');
    //Solo retornar oficios que tengan reintegro tipo true, es decir que tienen un reintegro por hacerse
    $query->whereIn('control_oficio_registro_oficio.id', function($query) {
      $query->select('control_oficio_registro_oficio_id')
      ->from('control_oficio_oferta_economicas')
      ->where('reintegro',true)//se le debe hacer re-integro
      ->where('estado_reintegro_id',1);//Estado de re-integro pendiente
    });
    //Solo retornar oficios que tengan reintegro tipo true, es decir que tienen un reintegro por hacerse
    return $query->get();
  }//obtenerOficios()

  public function index(){
    $oficios=$this->obtenerOficios();
    $oficios=$this->transformadorOficios($oficios);
    $oficios=json_encode($oficios);
    return view('controlOficios.ListadoReintegro.listado',compact('oficios'));
  }//listadoReintegro(Request $request)

  public function guardarReembolso(Request $request){
    try {
      $oferta_economica=OfertaEconomica::where('id',$request->oferta_economica_id)->first();
      if(count($oferta_economica)==0)
        return response()->json(['error'=>1,'msg'=>'Oferta económica no encontrada.']);
      $pagos=PagosOfertaEconomica::where('control_oficio_oferta_economica_id',$oferta_economica->id)->get();

      $reembolsado=0;
      $pendiente_reembolsar=0;
      foreach($pagos as $pago){
        if($pago->tipo_pago!="reintegro")
          $pendiente_reembolsar=floatval($pendiente_reembolsar)+floatval($pago->monto);
        else
          $reembolsado=floatval($reembolsado)+floatval($pago->monto);
      }//foreach pagos
      if($reembolsado>=$pendiente_reembolsar || ($reembolsado+floatval($request->monto))>$pendiente_reembolsar)
        return response()->json(['error'=>1,'msg'=>'No se pueden registrar mas pagos en este oficio.']);
      $p=PagosOfertaEconomica::where('referencia',$request->referencia_pago)->first();
      if(count($p)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe un pago con este número de referencia.']);
      DB::beginTransaction();
      $pago=PagosOfertaEconomica::create([
        'control_oficio_oferta_economica_id'=>$oferta_economica->id,
        'tipo_pago'=>'reintegro',
        'referencia'=>$request->referencia_pago,
        'monto'=>floatval($request->monto),
        'soporte_lote'=>$request->soporte_pago,
        'validacion'=>true
      ]);
      $reembolsado=floatval($reembolsado)+floatval($request->monto);
      $completado=false;//Falso si aun falta por pagar
      if(floatval($pendiente_reembolsar)-floatval($reembolsado)<=0){
        $oferta_economica->estado_reintegro_id=8;
        $oferta_economica->update();
        $completado=true;
      }
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Reembolso almacenado correctamente.','pago'=>$pago,'reembolsado'=>$reembolsado,'completado'=>$completado]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//guardarReembolso

  public function transformadorOficios($oficios){
    $array=[];
    foreach($oficios as $oficio){
      $personal_juridico=null;
      $ente='';
      $oferta_economica=null;//.
      if($oficio->personal_juridico!=null){
        $personal_juridico=[
          'rif'=>$oficio->personal_juridico->rif_cedula_situr,
          'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
          'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
          'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
          'ente'=>$oficio->personal_juridico->ente->nombre,
          'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
        ];
        $ente=' / '.$oficio->personal_juridico->ente->nombre;
      }//si existe personal juridico
      $pagos=$oficio->oferta_economica->pagos;
      $pago_reembolso=0;
      $pago_cancelado=0;
      foreach($oficio->oferta_economica->pagos as $pago){
        if($pago->tipo_pago!="reintegro")
          $pago_reembolso=floatval($pago_reembolso)+floatval($pago->monto);
        else
          $pago_cancelado=floatval($pago_cancelado)+floatval($pago->monto);
      }//foreach pagos
      $array[]=[
        'id'=>$oficio->id,
        'oferta_economica_id'=>$oficio->oferta_economica->id,
        'personal_natural'=>[
          'id'=>$oficio->personal_natural_id,
          'cedula'=>$oficio->personal_natural->cedula,
          'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
          'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
          'telefono'=>$oficio->personal_natural->telefono,
          'correo_electronico'=>$oficio->personal_natural->correo_electronico,
          'firma_personal'=>$oficio->personal_natural->firma_personal
        ],
        'personal_juridico'=>$personal_juridico,
        'banco'=>$oficio->oferta_economica->datos_reintegro->banco,
        'num_cuenta'=>$oficio->oferta_economica->datos_reintegro->numero_cuenta,
        'tipo_cuenta'=>$oficio->oferta_economica->datos_reintegro->tipo_cuenta,
        'identificacion'=>$oficio->oferta_economica->datos_reintegro->identificacion,
        'nombre'=>$oficio->oferta_economica->datos_reintegro->nombre,
        'correo_electronico'=>$oficio->oferta_economica->datos_reintegro->correo_electronico,
        'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
        'pagos'=>$pagos,
        'pago_reembolso'=>$pago_reembolso,
        'pago_cancelado'=>$pago_cancelado
      ];
    }//foreach oficios
    return $array;
  }//transformadorOficios()
}
