<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico as responsablesEmpresa;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\MotivoSolicitud;
use App\Modelos\Territorios\Territorios_estado;
use App\Modelos\ControlOficios\Departamentos;
use App\Modelos\Sigesp\sno_personal;
use App\Modelos\ControlOficios\Ente;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material_aprobacion_parcial;
use App\Modelos\ControlOficios\Control_oficio_materiales;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\ControlOficios\Control_oficio_despacho;
use App\Modelos\ControlOficios\Control_oficio_vehiculo;
use DB;
use Validator;
class GestionSolicitudesDonativosAprobadasController extends Controller
{
  public function index(){
    $estados=Territorios_estado::all();
    $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get();
    $oficios=$this->buscarSolicitudesDonativosAprobadasSinFiltro();
    return view('controlOficios.GestionSolicitudesDonativosAprobadas.gestionSolicitudesDonativosAprobadas')->with(['estados'=>$estados,'oficios'=>json_encode($oficios),'ClasificacionEnte'=>json_encode($ClasificacionEnte)]);
  }//public function index()

  public function buscarSolicitudesDonativosAprobadasSinFiltro(){
    $oficiosConsulta=Control_oficio_registro_oficio::join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
    ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
    ->where('motivo_solicitud_id','3')
    ->whereIn('control_oficio_registro_oficio.estado_aprobacion_id',array(2,3))
    ->select('control_oficio_registro_oficio.created_at','control_oficio_registro_oficio.fecha_estado',
              DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
              'control_oficio_personal_natural.trabajador_vtelca', 'control_oficio_registro_oficio.personal_natural_id',
              'control_oficio_registro_oficio.personal_juridico_id','control_oficio_registro_oficio.id as id_oficio',
              'control_oficio_registro_oficio.estado_aprobacion_id')->get();
    return $this->TransformarConsulta($oficiosConsulta);
  }//public function buscarSinfiltroOficio()

  public function TransformarConsulta($oficiosConsulta){
    $oficios=[];
    foreach($oficiosConsulta as &$oficio){
      $tipo_solicitante="";
      if($oficio->trabajador_vtelca==true){
        $tipo_solicitante="Trabajador";
      }else if($oficio->personal_juridico_id!=null){
        $tipo_solicitante="Persona Juridica";
      }else{
        $tipo_solicitante="Persona Natural";
      }
      if($oficio->estado_aprobacion_id!=7){
        $materiales=$this->buscarMaterialesAprobados($oficio->id_oficio,$oficio->estado_aprobacion_id);
      }else{
        //Agregado 01 febrero
        //Si es un oficio con fecha de despacho asignada.
        $materiales=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')
        ->where('registro_oficio_id',$oficio->id_oficio)
        ->select('control_oficio_materiales.nombre as nombre','control_oficio_material_aprobacion_parcial.cantidad as cantidad','control_oficio_material_aprobacion_parcial.materiales_id as id_materiales')
        ->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
        if(count($materiales)>0){
          //Aprobación parcial
          $id_materiales="";
          $nombres_materiales="";
          foreach($materiales as $material){
            $nombres_materiales=$nombres_materiales.$material->nombre.", ";
            $id_materiales=$id_materiales.$material->id_materiales.", ";
          }//foreach
          $nombres_materiales =  trim($nombres_materiales, ', ');
          $id_materiales =  trim($id_materiales, ', ');//Agregado 24 enero
          $cantidad=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')->where('registro_oficio_id',$oficio->id_oficio)->sum('cantidad');
          $materiales=['materiales'=>$nombres_materiales,'cantidad'=>$cantidad,'idMateriales'=>$id_materiales];
        }else{
          //Aprobación completa
          $materiales=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')
          ->where('registro_oficio_id',$oficio->id_oficio)
          ->select('control_oficio_materiales.nombre as nombre','control_oficio_solicitud_material.cantidad as cantidad','control_oficio_solicitud_material.materiales_id as id_materiales')
          ->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
          $nombres_materiales="";
          $id_materiales="";
          foreach($materiales as $material){
            $nombres_materiales=$nombres_materiales.$material->nombre.", ";
            $id_materiales=$id_materiales.$material->id_materiales.", ";
          }
          $nombres_materiales =  trim($nombres_materiales, ', ');
          $id_materiales =  trim($id_materiales, ', ');
          $cantidad=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')->where('registro_oficio_id',$oficio->id_oficio)->sum('cantidad');
          $materiales=['materiales'=>$nombres_materiales,'cantidad'=>$cantidad,'idMateriales'=>$id_materiales];
        }
      }//else
      $oficios[]=[
        'id_oficio'=>$oficio->id_oficio,
        'fecha_solicitud'=>$oficio->created_at->format('d-m-Y'),
        'fecha_estado'=> substr($oficio->fecha_estado,8,2)."-".substr($oficio->fecha_estado,5,2)."-".substr($oficio->fecha_estado,0,4),
        'nombre_responsable'=>$oficio->nombre_responsable,
        'tipo_solicitante'=> $tipo_solicitante,
        'nombres_materiales'=>$materiales['materiales'],
        'id_materiales'=>$materiales['idMateriales'],
        'materiales_cantidad'=>$materiales['cantidad']
      ];
    }
    return $oficios;
  }// public function TransformarConsulta($consulta)


  public function buscarMaterialesAprobados($idOficio,$tipoEstadoid){

    if($tipoEstadoid==2)
    {
      $materiales=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')
      ->where('registro_oficio_id',$idOficio)
      ->select('control_oficio_materiales.nombre as nombre','control_oficio_solicitud_material.cantidad as cantidad','control_oficio_solicitud_material.materiales_id as id_materiales')
      ->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
      $nombres_materiales="";
      $id_materiales="";

      foreach($materiales as $material){
        $nombres_materiales=$nombres_materiales.$material->nombre.", ";
        $id_materiales=$id_materiales.$material->id_materiales.", ";
      }

      $nombres_materiales =  trim($nombres_materiales, ', ');
      $id_materiales =  trim($id_materiales, ', ');

      $cantidad=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')->where('registro_oficio_id',$idOficio)->sum('cantidad');
      $materiales_cantidad=['materiales'=>$nombres_materiales,'cantidad'=>$cantidad,'idMateriales'=>$id_materiales];

      return ($materiales_cantidad);
    }// if($tipoEstadoid==2)
    else{
      $id_materiales="";//Agregado 24 enero

      $materiales=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')
      ->where('registro_oficio_id',$idOficio)
      ->select('control_oficio_materiales.nombre as nombre','control_oficio_material_aprobacion_parcial.cantidad as cantidad','control_oficio_material_aprobacion_parcial.materiales_id as id_materiales')
      ->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
      $nombres_materiales="";
      foreach($materiales as $material){
        $nombres_materiales=$nombres_materiales.$material->nombre.", ";
        $id_materiales=$id_materiales.$material->id_materiales.", ";//Agregado 24 enero
      }
      $nombres_materiales =  trim($nombres_materiales, ', ');
      $id_materiales =  trim($id_materiales, ', ');//Agregado 24 enero
      $cantidad=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')->where('registro_oficio_id',$idOficio)->sum('cantidad');
      $materiales_cantidad=['materiales'=>$nombres_materiales,'cantidad'=>$cantidad,'idMateriales'=>$id_materiales];
      return ($materiales_cantidad);
    }// else if($tipoEstadoid==2)
  }//public function buscarSolicitudesDonativosAprobadas(Request $request)

  public function buscarSolicitudesDonativosAprobadasConFiltro(Request $request){
    if($request->estado_id==0){
      $condicional="1=1";
    }else{
      $condicional="control_oficio_registro_oficio.estado_id=".$request->estado_id;
    }//if($request->estado_id==0)

    /***********************************Sin Filtros****************************************/
    if($request->tipo_solicitante_id==0 && $request->Ente_id==0){
      $oficiosConsulta=Control_oficio_registro_oficio::query();
      $oficiosConsulta->join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
      ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
      ->where('motivo_solicitud_id','3');
      if($request->con_fecha_despacho==1){//Traer oficios que ya tengan una fecha de despacho asignada.
        $oficiosConsulta->where('control_oficio_registro_oficio.estado_aprobacion_id',7);
        $oficiosConsulta->has('despacho');
      }//if despacho==1
      else
      $oficiosConsulta->whereIn('control_oficio_registro_oficio.estado_aprobacion_id',array(2,3));
      $oficiosConsulta->whereRaw($condicional)
      ->select('control_oficio_registro_oficio.created_at','control_oficio_registro_oficio.fecha_estado',
      DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
      'control_oficio_personal_natural.trabajador_vtelca', 'control_oficio_registro_oficio.personal_natural_id',
      'control_oficio_registro_oficio.personal_juridico_id','control_oficio_registro_oficio.id as id_oficio',
      'control_oficio_registro_oficio.estado_aprobacion_id');
      $oficiosConsulta=$oficiosConsulta->get();
      $oficios= $this->TransformarConsulta($oficiosConsulta);
      if(count($oficios)==0)
      return response()->json(['error'=>1,'msg'=>'No se encontradon registros asociados a los filtros seleccionados.']);
      return response()->json(['error'=>0,'oficios'=>$oficios]);
    }//if($request->tipo_solicitante_id==0 && $request->Ente_id==0)

    /***********************************Persona Natural****************************************/
    else if ($request->tipo_solicitante_id==1){
      $oficiosConsulta=Control_oficio_registro_oficio::query();
      $oficiosConsulta->join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
      ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
      ->where('motivo_solicitud_id','3');
      if($request->con_fecha_despacho==1){//Traer oficios que ya tengan una fecha de despacho asignada.
        $oficiosConsulta->where('control_oficio_registro_oficio.estado_aprobacion_id',7);
        $oficiosConsulta->has('despacho');
      }//if despacho==1
      else
      $oficiosConsulta->whereIn('control_oficio_registro_oficio.estado_aprobacion_id',array(2,3));
      $oficiosConsulta->where('control_oficio_registro_oficio.personal_juridico_id',null)
      ->where('control_oficio_personal_natural.trabajador_vtelca',false)
      ->whereRaw($condicional)
      ->select('control_oficio_registro_oficio.created_at','control_oficio_registro_oficio.fecha_estado',
      DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
      'control_oficio_personal_natural.trabajador_vtelca', 'control_oficio_registro_oficio.personal_natural_id',
      'control_oficio_registro_oficio.personal_juridico_id','control_oficio_registro_oficio.id as id_oficio',
      'control_oficio_registro_oficio.estado_aprobacion_id');
      $oficiosConsulta=$oficiosConsulta->get();
      if(count($oficiosConsulta)==0)
      return response()->json(['error'=>1,'msg'=>'No se encontradon registros asociados a los filtros seleccionados.']);
      return response()->json(['error'=>0,'oficios'=>$this->TransformarConsulta($oficiosConsulta)]);
    }// else if ($request->tipo_solicitante_id==1)

    /***********************************Trabajador****************************************/
    else if($request->tipo_solicitante_id==3){
      $oficiosConsulta=Control_oficio_registro_oficio::query();
      $oficiosConsulta->join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
      ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
      ->where('motivo_solicitud_id','3');
      if($request->con_fecha_despacho==1){//Traer oficios que ya tengan una fecha de despacho asignada.
        $oficiosConsulta->where('control_oficio_registro_oficio.estado_aprobacion_id',7);
        $oficiosConsulta->has('despacho');
      }//if despacho==1
      else
      $oficiosConsulta->whereIn('control_oficio_registro_oficio.estado_aprobacion_id',array(2,3));
      $oficiosConsulta->where('control_oficio_registro_oficio.personal_juridico_id',null)
      ->where('control_oficio_personal_natural.trabajador_vtelca',true)
      ->whereRaw($condicional)
      ->select('control_oficio_registro_oficio.created_at','control_oficio_registro_oficio.fecha_estado',
      DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
      'control_oficio_personal_natural.trabajador_vtelca', 'control_oficio_registro_oficio.personal_natural_id',
      'control_oficio_registro_oficio.personal_juridico_id','control_oficio_registro_oficio.id as id_oficio',
      'control_oficio_registro_oficio.estado_aprobacion_id');
      $oficiosConsulta=$oficiosConsulta->get();
      if(count($oficiosConsulta)==0)
      return response()->json(['error'=>1,'msg'=>'No se encontradon registros asociados a los filtros seleccionados.']);
      return response()->json(['error'=>0,'oficios'=>$this->TransformarConsulta($oficiosConsulta)]);
    }//else if($request->tipo_solicitante_id==3)

    /***********************************Perona Juridica****************************************/
    else if ($request->tipo_solicitante_id==2 && $request->Ente_id==0){
      $oficiosConsulta=Control_oficio_registro_oficio::query();
      $oficiosConsulta->join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
      ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
      ->where('motivo_solicitud_id','3');
      if($request->con_fecha_despacho==1){//Traer oficios que ya tengan una fecha de despacho asignada.
        $oficiosConsulta->where('control_oficio_registro_oficio.estado_aprobacion_id',7);
        $oficiosConsulta->has('despacho');
      }//if despacho==1
      else
      $oficiosConsulta->whereIn('control_oficio_registro_oficio.estado_aprobacion_id',array(2,3));
      $oficiosConsulta->where('control_oficio_registro_oficio.personal_juridico_id',null)
      ->where('control_oficio_personal_natural.trabajador_vtelca',false)
      ->whereRaw($condicional)
      ->select('control_oficio_registro_oficio.created_at','control_oficio_registro_oficio.fecha_estado',
      DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
      'control_oficio_personal_natural.trabajador_vtelca', 'control_oficio_registro_oficio.personal_natural_id',
      'control_oficio_registro_oficio.personal_juridico_id','control_oficio_registro_oficio.id as id_oficio',
      'control_oficio_registro_oficio.estado_aprobacion_id');
      $oficiosConsulta=$oficiosConsulta->get();
      if(count($oficiosConsulta)==0)
      return response()->json(['error'=>1,'msg'=>'No se encontradon registros asociados a los filtros seleccionados.']);
      return response()->json(['error'=>0,'oficios'=>$this->TransformarConsulta($oficiosConsulta)]);
    }// else if ($request->tipo_solicitante_id==1)



    else if ($request->tipo_solicitante_id==2 && $request->Ente_id!=0 && $request->estado_id==0){
      $oficiosConsulta=Control_oficio_registro_oficio::query();
      $oficiosConsulta->join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
      ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
      ->join('control_oficio_personal_juridico','control_oficio_personal_juridico.id','=','control_oficio_registro_oficio.personal_juridico_id')
      ->where('motivo_solicitud_id','3');
      if($request->con_fecha_despacho==1){//Traer oficios que ya tengan una fecha de despacho asignada.
        $oficiosConsulta->where('control_oficio_registro_oficio.estado_aprobacion_id',7);
        $oficiosConsulta->has('despacho');
      }//if despacho==1
      else
      $oficiosConsulta->whereIn('control_oficio_registro_oficio.estado_aprobacion_id',array(2,3));
      $oficiosConsulta->where('control_oficio_registro_oficio.personal_juridico_id',null)
      ->where('control_oficio_personal_natural.trabajador_vtelca',false)
      ->whereRaw($condicional)
      ->select('control_oficio_registro_oficio.created_at','control_oficio_registro_oficio.fecha_estado',
      DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
      'control_oficio_personal_natural.trabajador_vtelca', 'control_oficio_registro_oficio.personal_natural_id',
      'control_oficio_registro_oficio.personal_juridico_id','control_oficio_registro_oficio.id as id_oficio',
      'control_oficio_registro_oficio.estado_aprobacion_id');
      $oficiosConsulta=$oficiosConsulta->get();
      if(count($oficiosConsulta)==0)
      return response()->json(['error'=>1,'msg'=>'No se encontradon registros asociados a los filtros seleccionados.']);
      return response()->json(['error'=>0,'oficios'=>$this->TransformarConsulta($oficiosConsulta)]);
    }// else if ($request->tipo_solicitante_id==1)

    else{
      return response()->json(['error'=>1,'msg'=>'No se encontradon registros asociados a los filtros seleccionados.']);
    }
  }//public function buscarSolicitudesDonativosAprobadas(Request $request)

  public function registrarDatosChofer(Request $request){
    try {
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'responsable_id' => 'required',
        'chofer_id' => 'required',
        'vehiculo_id' => 'required',
        'registro_oficio_id' => 'required',
      ],[
        'responsable_id.required'=>'Existen campos por llenar:<br>*Datos del responsable.',
        'chofer_id.required'=>'Existen campos por llenar:<br>*Datos del Chofer.',
        'vehiculo_id.required'=>'Existen campos por llenar:<br>*Datos del Vehículo.',
        'registro_oficio_id.required'=>'Existen campos por llenar:<br>*Datos del oficio.',
      ]);

      if ($validator->fails()) {
        return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
      }
      $Chofer=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
      $Chofer->responsable_id = $request->responsable_id;
      $Chofer->chofer_id = $request->chofer_id;
      $Chofer->vehiculo_id = $request->vehiculo_id;
      $Chofer->registro_oficio_id = $request->registro_oficio_id;
      $Chofer->update();
      DB::commit();
      // $despachos=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
      return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
    }//catch()
  }//public function registarDatosChofer()

  public function BuscarDatosDespacho(Request $request){
    $validator=Validator::make($request->all(), [
      'registro_oficio_id' => 'required',
    ],[
      'registro_oficio_id.required'=>'Existen campos por llenar:<br>*Datos del oficio.',
    ]);
    if ($validator->fails()) {
      return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
    }
    $despachos=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
    if(count($despachos)==0)
    return response()->json(['status'=>'error']);
    if($despachos->responsable_id==null){
      $responsable_id="";
      $cedulaResponsable="";
      $nombresResponsable="";
      $telefonoResponsable="";
      $chofer_id="";
      $cedulaChofer="";
      $nombresChofer="";
      $telefonoChofer="";
      $vehiculo_id="";
      $idVehiculo="";
      $modeloVehiculo="";
      $placaVehiculo="";
      $fecha_despacho="";
      $registro_oficio_id="";
    }
    else{
      $DatosChofer=   $persona_natural=pn::where('id',$despachos->chofer_id)->first();
      $DatosResponsable=   $persona_natural=pn::where('id',$despachos->responsable_id)->first();
      $DatosVehiculo=Control_oficio_vehiculo::where('id',$despachos->vehiculo_id)->first();
      $despacho=[];
      $responsable_id=$despachos->responsable_id;
      $cedulaResponsable=$DatosResponsable->cedula;
      $nombresResponsable=$DatosResponsable->apellido." ".$DatosResponsable->nombre;
      $telefonoResponsable=$DatosResponsable->telefono;
      $chofer_id=$despachos->chofer_id;
      $cedulaChofer=$DatosChofer->cedula;
      $nombresChofer=$DatosChofer->apellido." ".$DatosChofer->nombre;
      $telefonoChofer=$DatosChofer->telefono;
      $vehiculo_id=$despachos->vehiculo_id;
      $idVehiculo=$despachos->vehiculo_id;
      $modeloVehiculo=$DatosVehiculo->modelo;
      $placaVehiculo=$DatosVehiculo->placa;
      $registro_oficio_id=$despachos->registro_oficio_id;
    }
    $despacho[]=[
      'responsable_id' =>  $responsable_id,
      'cedulaResponsable'=>$cedulaResponsable,
      'nombresResponsable'=>$nombresResponsable,
      'telefonoResponsable'=>$telefonoResponsable,
      'chofer_id' => $chofer_id,
      'cedulaChofer'=>$cedulaChofer,
      'nombresChofer'=>$nombresChofer,
      'telefonoChofer'=>$telefonoChofer,
      'vehiculo_id' => $vehiculo_id,
      'registro_oficio_id' => $registro_oficio_id,
      'idVehiculo'=> $idVehiculo,
      'modeloVehiculo'=>$modeloVehiculo,
      'placaVehiculo'=>$placaVehiculo,
      'fecha_despacho' =>$despachos->fecha_despacho
    ];
    return response()->json(['status'=>'success', 'despacho'=>$despacho]);
  }//public function BuscarDatosDespacho(Request $request)


  public function ObtenerDatosChoferDespacho(Request $request){
    $validator=Validator::make($request->all(), [
      'registro_oficio_id' => 'required',
    ],[
      'registro_oficio_id.required'=>'Existen campos por llenar:<br>*Datos del oficio.',
    ]);
    if ($validator->fails()) {
      return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
    }

    $datosChofer = DB::table('control_oficios.control_oficio_despachos')
    ->select('control_oficio_despachos.responsable_id','control_oficio_despachos.chofer_id','control_oficio_despachos.vehiculo_id',
    'control_oficio_despachos.registro_oficio_id','control_oficio_registro_oficio.personal_natural_id','control_oficio_personal_natural.nombre as nombresChofer',
    'control_oficio_personal_natural.apellido as apellidoChofer','control_oficio_personal_natural.cedula as cedulaChofer','control_oficio_personal_natural.telefono','control_oficio_vehiculos.placa','control_oficio_vehiculos.modelo')
    ->join('control_oficios.control_oficio_registro_oficio', 'control_oficio_despachos.registro_oficio_id', '=', 'control_oficio_registro_oficio.id')
    ->join('control_oficios.control_oficio_personal_natural', 'control_oficio_despachos.chofer_id', '=', 'control_oficio_personal_natural.id')
    ->join('control_oficios.control_oficio_vehiculos', 'control_oficio_despachos.vehiculo_id', '=', 'control_oficio_vehiculos.id')
    ->where('control_oficio_despachos.registro_oficio_id',$request->registro_oficio_id)
    ->first();

    return response()->json(['status'=>'success', 'datosChofer'=>$datosChofer]);

  }

  public function actualizarDatosChofer(Request $request){
    try {
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'responsable_id' => 'required',
        'chofer_id' => 'required',
        'vehiculo_id' => 'required',
        'registro_oficio_id' => 'required',
      ],[
        'responsable_id.required'=>'Existen campos por llenar:<br>*Datos del responsable.',
        'chofer_id.required'=>'Existen campos por llenar:<br>*Datos del Chofer.',
        'vehiculo_id.required'=>'Existen campos por llenar:<br>*Datos del Vehículo.',
        'registro_oficio_id.required'=>'Existen campos por llenar:<br>*Datos del oficio.',
      ]);

      if ($validator->fails()) {
        return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
      }

      $control_oficio_despachos=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
      $control_oficio_despachos->responsable_id     =$request->responsable_id;
      $control_oficio_despachos->chofer_id          =$request->chofer_id;
      $control_oficio_despachos->vehiculo_id        =$request->vehiculo_id;
      $control_oficio_despachos->update();
      if($control_oficio_despachos->fecha_despacho!=null){
        $Control_oficio_registro_oficio=Control_oficio_registro_oficio::where('id',$request->registro_oficio_id)->first();
        $Control_oficio_registro_oficio->estado_aprobacion_id  =7;
        $Control_oficio_registro_oficio->update();
      }//if($control_oficio_despachos->responsable_id!=null)
      DB::commit();
      // $despachos=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
      // $oficios=$this->buscarSolicitudesDonativosAprobadasSinFiltro();
      return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactoria.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
    }//catch()
  }//public function registarDatosChofer()



  public function cambiarEstado(Request $request){
    $hoy = getdate();
    $hoy = $hoy['year'].'-'.$hoy['mon'].'-'.$hoy['mday'];
    try{
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'id' => 'required'
      ],[
        'id.required'=>'Existen campos por llenar:<br>*Id.', ]);
        if ($validator->fails()) {
          return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
        }

        $Control_oficio_registro_oficio=Control_oficio_registro_oficio::where('id',$request->id)->first();
        if (count($Control_oficio_registro_oficio)>0 ) {

          $Control_oficio_registro_oficio->estado_aprobacion_id  =4;
          $Control_oficio_registro_oficio->fecha_estado          =$hoy ;
          $Control_oficio_registro_oficio->update();
        } else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
        DB::commit();
        $oficios=$this->buscarSolicitudesDonativosAprobadasSinFiltro();
        return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'oficios'=>$oficios]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
      }

    }//public  public function eliminar(Request $request)

    public function AsignarFechaDesapacho(Request $request){
      try {
        DB::beginTransaction();
        $validator=Validator::make($request->all(), [
          'fecha_despacho' => 'required|date',

        ],[
          'fecha_despacho.required'=>'Existen campos por llenar:<br>*Datos del responsable.',
          'fecha_despacho.date'=>'La fecha ingresada no es valida.',
        ]);

        if ($validator->fails()) {
          return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
        }

        $control_oficio_despachos=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
        $Control_oficio_registro_oficio=Control_oficio_registro_oficio::where('id',$request->registro_oficio_id)->first();
        if(count($control_oficio_despachos)==0){
          $create=Control_oficio_despacho::create($request->all());
          /*$create= new Control_oficio_despacho;
          $create->registro_oficio_id=$request->registro_oficio_id;
          $create->fecha_despacho=$request->fecha_despacho;*/
          // $create->save();
          $Control_oficio_registro_oficio->estado_aprobacion_id  =7;
          $Control_oficio_registro_oficio->update();

        }//if(count($control_oficio_despachos)==0) Se crea si no existe
        else{
          $control_oficio_despachos->fecha_despacho =$request->fecha_despacho;
          $control_oficio_despachos->update();
          $Control_oficio_registro_oficio->estado_aprobacion_id  =7;
          $Control_oficio_registro_oficio->update();
          /*VALIDACION DE CHOFER*/
          /*if($control_oficio_despachos->responsable_id!=null){
          $Control_oficio_registro_oficio=Control_oficio_registro_oficio::where('id',$request->registro_oficio_id)->first();
          $Control_oficio_registro_oficio->estado_aprobacion_id  =7;
          $Control_oficio_registro_oficio->update();
        }//if($control_oficio_despachos->responsable_id!=null)*/


      }//else if(count($control_oficio_despachos)==0)
      $oficios=$this->buscarSolicitudesDonativosAprobadasSinFiltro();
      DB::commit();
      $despachos=Control_oficio_despacho::where('registro_oficio_id',$request->registro_oficio_id)->first();
      return response()->json(['status'=>'success','mensaje'=>'Actuzlización Satisfactoria.', 'despachos'=>$despachos, 'oficios'=>$oficios]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
    }//catch()
  }//public function registarDatosChofer()

}//class motivoSolicitud extends Controller
