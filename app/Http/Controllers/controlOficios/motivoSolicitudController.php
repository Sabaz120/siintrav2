<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\MotivoSolicitud;
use DB;
use Validator;
class motivoSolicitudController extends Controller
{
    public function mostrarMotivoSolicitud(){
        $MotivoSolicitud=MotivoSolicitud::orderBy('nombre')->get();
        return view('controlOficios.gestionSolicitud')->with('MotivosSolicitud',$MotivoSolicitud);
    }//public function mostrarMotivoSolicitud(Request $request)

    public function registrarMotivoSolicitud(Request $request){

        try {    
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'nombre' => 'required|min:3|max:50',
                'descripcion' =>'required|min:3|max:200',
            ],[
               'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
               'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'descripcion.required'=>'Existen campos por llenar:<br>*Descripción.',
               'descripcion.min'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
               'descripcion.max'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }  
            $Motivo=MotivoSolicitud::where('nombre',$request->nombre)->first();
            if(count($Motivo)){
                return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo nombre']);
            }//if(count($Motivo))      
            $create=MotivoSolicitud::create($request->all()); 
            DB::commit();     
            $MotivoSolicitud=MotivoSolicitud::orderBy('nombre')->get();  
            return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'MotivosSolicitudes'=>$MotivoSolicitud]);
        } catch (\Exception $e) { 
            DB::rollBack();         
            return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
        }//catch()  


        

    }//public function registrarMotivoSolicitud(Request $request)

    public function modificarMotivoSolicitud(Request $request){

        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required',
                'nombre' => 'required|min:3|max:50',
                'descripcion' =>'required|min:3|max:200',
            ],[
               'id.required'=>'El campo id es requerido', 
               'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
               'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'descripcion.required'=>'El campo descripcion es requerido',
               'descripcion.min'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
               'descripcion.max'=>'El campo descripción acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }
            $Motivo=MotivoSolicitud::where('id','<>',$request->id)->where('nombre',$request->nombre)->first();
            $MotivoSolicitud=MotivoSolicitud::orderBy('nombre')->get(); 
            $error=array('No se puede duplicar el campo nombre.');
            if(count($Motivo)!=0){
                return response()->json(['status'=>'error','mensaje'=>$error]);  
            }// if(count($Motivo)!=0)
            $Motivo=MotivoSolicitud::where('id',$request->id)->first();
            $Motivo->nombre=ucfirst($request->nombre);
            $Motivo->descripcion=ucfirst($request->descripcion);
            $Motivo->update();
            DB::commit();
            $MotivoSolicitud=MotivoSolicitud::orderBy('nombre')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'MotivosSolicitudes'=>$MotivoSolicitud]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function modificarMotivoSolicitud(Request $request, $id)
    
    public function eliminarMotivoSolicitud(Request $request){
        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required'
            ],[
                'id.required'=>'El campo id es requerido', ]);
            if ($validator->fails()) {
              return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
            }
      
            $Motivo=MotivoSolicitud::where('id',$request->id)->first();
            if (count($Motivo)>0 ) {
                $Motivo->delete();
            } else {
              return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
            }
            DB::commit();
            $MotivoSolicitud=MotivoSolicitud::orderBy('nombre')->get();   
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'MotivosSolicitudes'=>$MotivoSolicitud]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }
      
    }//public function eliminarMotivoSolicitud(Request $request, $id)

}//class motivoSolicitud extends Controller
