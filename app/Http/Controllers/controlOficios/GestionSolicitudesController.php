<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_detalle_oferta_economica as detOfertaEconomica;
use App\Modelos\ControlOficios\Control_oficio_oferta_economica as OfertaEconomica;
use App\Modelos\ControlOficios\Control_oficio_pago as PagosOfertaEconomica;
use App\Modelos\PremiumSoft\Articulo;
use App\Modelos\PremiumSoft\Invcodalternativo;
use App\Modelos\PremiumSoft\Moneda;
use App\Modelos\PremiumSoft\PrecioPetro;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use PDF;
use DB;
use Auth;
use App\Modelos\Territorios\Territorios_estado;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use App\Modelos\ControlOficios\Control_oficio_vehiculo as Vehiculos;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_despacho as Despachos;
use App\Modelos\ControlOficios\Control_oficio_despacho_jornadas as DespachosJornadas;
use App\Modelos\ControlOficios\Control_oficio_reintegro_cuenta as ReintegroCuenta;
use App\Modelos\ControlOficios\Control_oficio_observaciones_generales as observaciones_generales;

//Productos - modalidades - disponibilidad
use App\Modelos\SistemaProduccionV1\ExistenciaAlmacenDespacho;
use App\Modelos\SistemaProduccionV1\Productos;



class GestionSolicitudesController extends Controller
{
  public $productos=[];
  public $productosSistProd=[];

  public function loadGlobalVars(){
    /////////Carga de precios por articulo de premiunsoft en variable global
    /*
    Esto debido que tarda mucho en hacer una simple consulta a ese servidor,
    es preferible hacer una solo consulta, envés de que hiciera una consulta por cada oficio.
    */
    $articulos=DB::connection('premiumsoft')->select(DB::raw("
    SELECT
    a.codigo,a.nombre, b.codalternativo,a.precio1,impuesto1,a.precio2,a.precio3,c.precio1 as precio1p,c.precio2 as precio2p,c.precio3 as precio3p
    FROM
    admin001000.articulo a
    JOIN
    admin001000.invcodalternativo b  on a.codigo=b.codigo
    JOIN
    admin001000.precio_petro c  on a.codigo=c.codigo
    "));
    if(count($articulos)==0){
      abort(404);
      \Log::error("No se han cargado los modelos de teléfonos en premiumsoft");
      // return ['error'=>1,'msg'=>'No se han cargado los modelos de teléfonos en premiumsoft'];
    }//articulos

    foreach($articulos as $articulo){

      $invcod = Invcodalternativo::where('codalternativo', $articulo->codalternativo)->first();
      $precioPetro = PrecioPetro::where('codigo', trim($invcod->codigo))->first();

      $this->productos[str_slug($articulo->codalternativo)]=[
        'precio1'=>$articulo->precio1,
        'precio2'=>$articulo->precio2,
        'precio3'=>$articulo->precio3,
        'precio1p'=>$precioPetro->precio1,
        'precio2p'=>$precioPetro->precio2,
        'precio3p'=>$precioPetro->precio3
        // 'precio1p'=>$precioPetro->preciofin1,
        // 'precio2p'=>$precioPetro->preciofin2,
        // 'precio3p'=>$precioPetro->preciofin3
      ];
    }//foreach articulos

    /////////Carga de precios por articulo de premiunsoft en variable global
    //dd($this->productos);
    //////////////////////////Inicio carga modalidad / disponibilidad
    $productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
    if(count($this->productosSistProd)==0){
      foreach($productos as $producto){
        $arreglo=[];
        $modalidades=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->select('modalidad')->distinct()->get();
        foreach($modalidades as $modalidad){
          $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$producto->nombre)->where('modalidad',$modalidad->modalidad)->where('idAlmacen',4)->sum('cantidadEnvia');
          if($disponibilidad>0){
            $disponibilidad=$disponibilidad-obtenerComprometido($producto->id,$modalidad->modalidad);
          }//Si disponibilidad > 0
          $arreglo[]=[
            'modalidad'=>$modalidad->modalidad,
            'disponibilidad'=>$disponibilidad,
            'nombre'=>$producto->nombre,
            'id'=>$producto->id,
            'descripcion'=>$producto->descripcion,
          ];
        }
        $this->productosSistProd[str_slug($producto->nombre)]=$arreglo;
      }//productos
    }
    ////////////////////////////////Fin carga modalidad / disponibilidad

  }//_construct

  //Obtener oficios
  public function obtenerOficios(){
    return Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','oferta_economica.requisicion.despacho.detalle',
    'oferta_economica.requisicion.despacho.responsable','oferta_economica.requisicion.despacho.chofer','oferta_economica.requisicion.despacho.vehiculo',
    'oferta_economica','oferta_economica.detalle_oferta','oferta_economica.pagos',
    'oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos',
    'solicitud_productos','personal_juridico','usuario_creador','personal_juridico.ente',
    'personal_juridico.ente.clasificacion_ente')
    ->whereIn('estado_aprobacion_id',[2,3])
    ->where('motivo_solicitud_id',1)//Solo oficios de jornada de telefono
    ->orderBy('created_at','ASC')->get();
    //estado aprobacion 3 = parcial, 2= completa
  }//obtenerOficios()

  public function obtenerOficioEspecifico($oficio_id){
    return Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','oferta_economica',
    'oferta_economica.requisicion.despacho.detalle',
    'oferta_economica.requisicion.despacho.responsable',
    'oferta_economica.requisicion.despacho.chofer','oferta_economica.requisicion.despacho.vehiculo',
    'oferta_economica.pagos','oferta_economica.detalle_oferta.producto',
    'estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto',
    'solicitud_productos','solicitud_productos.producto','personal_juridico',
    'usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
    ->where('id',$oficio_id)
    ->orderBy('created_at','ASC')->first();
    //estado aprobacion 3 = parcial, 2= completa
  }

  public function transformadorOficios($oficios){
    $user=Auth::user();
    $arreglo=[];
    $pagos=[];
    if($user->hasPermissionTo('siintra_menu_control_oficios_gestion_solicitudes_validar_pago')){
      /*Es usuario de administración que verifica los pagos en la cuenta bancaria.
      Solo cargar oficios que tengan pagos pendientes por validar.
      */
      foreach($oficios as $oficio){
        $pagos=[];
        $personal_juridico=null;
        $productos=null;
        $modelos='';
        $cantidad_solicitada=0;
        $cont=0;
        $ente='';
        $oferta_economica=null;//null si no ha configurado oferta economica, con datos si ya posee oferta economica.
        if($oficio->personal_juridico!=null){
          $personal_juridico=[
            'rif'=>$oficio->personal_juridico->rif_cedula_situr,
            'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
            'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
            'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
            'ente'=>$oficio->personal_juridico->ente->nombre,
            'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
          ];
          //$ente=' / '.$oficio->personal_juridico->ente->nombre;
          $ente=' / '.$oficio->personal_juridico->nombre_institucion;
        }//si existe personal juridico
        if($oficio->oferta_economica!=null){
          //Oficio con oferta economica
          $b=0;//sin Pagos que validar, 1 con Pagos pendiente por validar.
          $pagos=$oficio->oferta_economica->pagos;
          foreach($pagos as &$pago){
            if(!$pago->validacion){
              $b=1;
              break;
            }
          }//Validar si hay algun pago pendiente por validar
          foreach($pagos as $pago)
          $pago['galeria']=$pago->galeria;
          if($b==1){
            /////////////////////////////////Despacho
            $despacho=null;
            // if($oficio->despacho!=null){
            //   $despacho=[
            //     'id'=>$oficio->despacho->id,
            //     'fecha_despacho'=>$oficio->despacho->fecha_despacho,
            //     'responsable'=>[
            //       'id'=>$oficio->despacho->responsable->id,
            //       'nombres_apellidos'=>$oficio->despacho->responsable->nombre.' '.$oficio->despacho->responsable->apellido,
            //       'cedula'=>$oficio->despacho->responsable->cedula,
            //       'telefono'=>$oficio->despacho->responsable->telefono,
            //       'correo_electronico'=>$oficio->despacho->responsable->correo_electronico,
            //       'firma_personal'=>$oficio->despacho->responsable->firma_personal,
            //       'codigo_carnet_patria'=>$oficio->despacho->responsable->codigo_carnet_patria
            //     ],
            //     'chofer'=>[
            //       'id'=>$oficio->despacho->chofer->id,
            //       'nombres_apellidos'=>$oficio->despacho->chofer->nombre.' '.$oficio->despacho->chofer->apellido,
            //       'cedula'=>$oficio->despacho->chofer->cedula,
            //       'telefono'=>$oficio->despacho->chofer->telefono,
            //       'correo_electronico'=>$oficio->despacho->chofer->correo_electronico,
            //       'firma_personal'=>$oficio->despacho->chofer->firma_personal,
            //       'codigo_carnet_patria'=>$oficio->despacho->chofer->codigo_carnet_patria
            //     ],
            //     'vehiculo'=>[
            //       'id'=>$oficio->despacho->vehiculo->id,
            //       'placa'=>$oficio->despacho->vehiculo->placa,
            //       'modelo'=>$oficio->despacho->vehiculo->modelo
            //     ],
            //     // 'destino'=>$oficio->despacho->detalle->destino,
            //     // 'beneficiario'=>$oficio->despacho->detalle->beneficiario,
            //     // 'created_at'=>$oficio->despacho->created_at->format('d-m-Y')
            //   ];
            // }//despacho
            /////////////////////////////////Despacho
            $oferta_economica=[
              'id'=>$oficio->oferta_economica->id,
              'precio_total'=>floatval($oficio->oferta_economica->precio_total),
              'despacho'=>$despacho,
              "requisicion"=>$oficio->oferta_economica->requisicion
            ];
            foreach($oficio->oferta_economica->detalle_oferta as $producto){
              if($cont==0)
              $modelos=$producto->producto->descripcion;
              else
              $modelos=$modelos.', '.$producto->producto->descripcion;
              $precios=[];
              $cantidad_aprobada=0;
              if($oficio->estado_aprobacion_id==3){
                foreach($oficio->aprobacion_parcial_productos as $productoAprobado){
                  if($productoAprobado->producto_id==$producto->producto_id){
                    $cantidad_aprobada=$productoAprobado->cantidad;
                    break;
                  }
                }
              }else if($oficio->estado_aprobacion_id==2){
                foreach($oficio->solicitud_productos as $productoAprobado){
                  if($productoAprobado->producto_id==$producto->producto_id){
                    $cantidad_aprobada=$productoAprobado->cantidad;
                    break;
                  }
                }
              }
              $precios=[
                'precio1'=>0,
                'precio2'=>0,
                'precio3'=>0,
                'precio1p'=>0,
                'precio2p'=>0,
                'precio3p'=>0
              ];
              $precio_desactualizado=0;//Actualizado

              $productos[]=[
                'detalle_oferta_economica_id'=>$producto->id,
                'producto_id'=>$producto->producto_id,
                'cantidad'=>$producto->cantidad,
                'cantidad_max'=>$cantidad_aprobada,
                'modelo'=>$producto->producto->nombre,
                'descripcion'=>$producto->producto->descripcion,
                'modalidad'=>$producto->modalidad,
                'precio'=>$producto->precio_unitario,
                //'invcod' => $invcod,
                'preciop'=>$producto->precio_unitario_petro,
                // 'preciop'=>$precios[$producto->nivel_precio+3],
                // 'nivel_precio'=>1,
                'nivel_precio'=>$producto->nivel_precio,
                'precios'=>$precios,
                'precio_desactualizado'=>$precio_desactualizado
              ];
              $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
              $cont++;
            }//foreach productos oferta economica
            $arreglo[]=[
              'id'=>$oficio->id,
              'num_oferta'=>$oficio->oferta_economica ? $oficio->oferta_economica->id : null,
              'verificacion_pago'=>$oficio->verificacion_pago,
              'user_id'=>$oficio->user_id,
              //'usuario'=>$oficio->usuario_creador->name,
              'usuario'=>$oficio->usuario_creador ? $oficio->usuario_creador->name : "",
              'tipo_aprobacion'=>'parcial',
              'numero_oficio'=>$oficio->numero_oficio,
              'estado'=>$oficio->estado->estado,
              'documento'=>$oficio->documento,
              'descripcion'=>$oficio->descripcion,
              'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
              'personal_natural'=>[
                'id'=>$oficio->personal_natural_id,
                'cedula'=>$oficio->personal_natural->cedula,
                'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
                'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
                'telefono'=>$oficio->personal_natural->telefono,
                'correo_electronico'=>$oficio->personal_natural->correo_electronico,
                'firma_personal'=>$oficio->personal_natural->firma_personal
              ],
              'personal_juridico'=>$personal_juridico,
              'productos'=>$productos,
              'fecha_aprobacion'=>$oficio->updated_at->format('d-m-Y'),
              'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
              'modelos'=>$modelos,
              'cantidad_solicitada'=>$cantidad_solicitada,
              'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido .''.$ente,
              'oferta_economica'=>$oferta_economica,
              'oferta_economica_pagos'=>$pagos
            ];

          }//b==1
        }//if oferta economica
      }//oficios
      //dd("entre");
    }else{
      //Sigue todo el proceso normal
       $contg=0;
      foreach($oficios as $oficio){


        $pagos=[];
        $personal_juridico=null;
        $productos=null;
        $modelos='';
        $cantidad_solicitada=0;
        $cont=0;
        $ente='';
        $oferta_economica=null;//null si no ha configurado oferta economica, con datos si ya posee oferta economica.
        if($oficio->personal_juridico!=null){
          $personal_juridico=[
            'rif'=>$oficio->personal_juridico->rif_cedula_situr,
            'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
            'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
            'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
            'ente'=>$oficio->personal_juridico->ente->nombre,
            'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre,
            'agente_retencion'=>$oficio->personal_juridico->agente_retencion
          ];
          //$ente=' / '.$oficio->personal_juridico->ente->nombre;
          $ente=' / '.$oficio->personal_juridico->nombre_institucion;
        }//si existe personal juridico
        if($oficio->oferta_economica!=null){

          //Oficio con oferta economica
          $pagos=$oficio->oferta_economica->pagos;
         //   if($contg==17)
         // dd($oficio);
          foreach($pagos as $pago)
          $pago['galeria']=$pago->galeria;

          $despacho=null;
          // if($oficio->oferta_economica->despacho!=null){
          //   $despacho=[
          //     'id'=>$oficio->despacho->id,
          //     'fecha_despacho'=>$oficio->despacho->fecha_despacho,
          //     'responsable'=>[
          //       'id'=>$oficio->despacho->responsable->id,
          //       'nombres_apellidos'=>$oficio->despacho->responsable->nombre.' '.$oficio->despacho->responsable->apellido,
          //       'cedula'=>$oficio->despacho->responsable->cedula,
          //       'telefono'=>$oficio->despacho->responsable->telefono,
          //       'correo_electronico'=>$oficio->despacho->responsable->correo_electronico,
          //       'firma_personal'=>$oficio->despacho->responsable->firma_personal,
          //       'codigo_carnet_patria'=>$oficio->despacho->responsable->codigo_carnet_patria
          //     ],
          //     'chofer'=>[
          //       'id'=>$oficio->despacho->chofer->id,
          //       'nombres_apellidos'=>$oficio->despacho->chofer->nombre.' '.$oficio->despacho->chofer->apellido,
          //       'cedula'=>$oficio->despacho->chofer->cedula,
          //       'telefono'=>$oficio->despacho->chofer->telefono,
          //       'correo_electronico'=>$oficio->despacho->chofer->correo_electronico,
          //       'firma_personal'=>$oficio->despacho->chofer->firma_personal,
          //       'codigo_carnet_patria'=>$oficio->despacho->chofer->codigo_carnet_patria
          //     ],
          //     'vehiculo'=>[
          //       'id'=>$oficio->despacho->vehiculo->id,
          //       'placa'=>$oficio->despacho->vehiculo->placa,
          //       'modelo'=>$oficio->despacho->vehiculo->modelo
          //     ],
          //     /*'destino'=>$oficio->despacho->detalle->destino,
          //     'beneficiario'=>$oficio->despacho->detalle->beneficiario,
          //     'created_at'=>$oficio->despacho->created_at->format('d-m-Y')*/
          //   ];
          // }
          $oferta_economica=[
            'id'=>$oficio->oferta_economica->id,
            'precio_total'=>floatval($oficio->oferta_economica->precio_total),
            'despacho'=>$despacho,
            "requisicion"=>$oficio->oferta_economica->requisicion
          ];
          foreach($oficio->oferta_economica->detalle_oferta as $producto){
            if($cont==0)
            $modelos=$producto->producto->descripcion;
            else
            $modelos=$modelos.', '.$producto->producto->descripcion;
            $precios=[];
            if(isset($this->productos[str_slug($producto->producto->nombre.$producto->modalidad)])){
              $invcod = Invcodalternativo::where('codalternativo', $producto->producto->nombre.$producto->modalidad)->first();
              $precioPetro = PrecioPetro::where('codigo', trim($invcod->codigo))->first();
              /*$precios=[
                'precio1'=>$this-f>productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio1'],
                'precio2'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio2'],
                'precio3'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio3'],
                'precio1p'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio1p'],
                'precio2p'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio2p'],
                'precio3p'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio3p']
              ];*/
              $precios=[
                'precio1'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio1'],
                'precio2'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio2'],
                'precio3'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio3'],
                'precio1p'=>$precioPetro->precio1,
                // 'precio1p'=>$precioPetro->preciofin1,
                'precio2p'=>$precioPetro->precio2,
                // 'precio2p'=>$precioPetro->preciofin2,
                'precio3p'=>$precioPetro->precio3
                // 'precio3p'=>$precioPetro->preciofin3
              ];

            }else{
              //dd(str_slug($producto->producto->nombre.$producto->modalidad));
              return ['error'=>1,'msg'=>'Este modelo no se encuentra configurado en premiumsoft: '.$producto->producto->descripcion];
            }
            $precio_desactualizado=0;//Actualizado
            if($producto->nivel_precio==1){
              if($producto->precio_unitario!=$precios['precio1'])
              $precio_desactualizado=1;
            }
            else if($producto->nivel_precio==2){
              if($producto->precio_unitario!=$precios['precio2'])
              $precio_desactualizado=1;
            }else if($producto->nivel_precio==3){
              if($producto->precio_unitario!=$precios['precio3'])
              $precio_desactualizado=1;
            }
            $cantidad_aprobada=0;
            if($oficio->estado_aprobacion_id==3){
              foreach($oficio->aprobacion_parcial_productos as $productoAprobado){
                if($productoAprobado->producto_id==$producto->producto_id){
                  $cantidad_aprobada=$productoAprobado->cantidad;
                  break;
                }
              }
            }else if($oficio->estado_aprobacion_id==2){
              foreach($oficio->solicitud_productos as $productoAprobado){
                if($productoAprobado->producto_id==$producto->producto_id){
                  $cantidad_aprobada=$productoAprobado->cantidad;
                  break;
                }
              }
            }
            $cantidad_almacen=0;
            foreach($this->productosSistProd[str_slug($producto->producto->nombre)] as $dpa){
              if($dpa['modalidad']==$producto->modalidad){
                $cantidad_almacen=$dpa['disponibilidad'];
                break;
              }
            }
            $asociacion_nombre="precio".$producto->nivel_precio."p";
            $invcod = Invcodalternativo::where('codalternativo', $producto->producto->nombre.$producto->modalidad)->first();
            $precioPetro = PrecioPetro::where('codigo', trim($invcod->codigo))->first();
            $productos[]=[
              'detalle_oferta_economica_id'=>$producto->id,
              'producto_id'=>$producto->producto_id,
              'cantidad'=>$producto->cantidad,
              'cantidad_max'=>$cantidad_aprobada,
              'cantidad_almacen'=>$cantidad_almacen,
              'modelo'=>$producto->producto->nombre,
              'descripcion'=>$producto->producto->descripcion,
              'modalidad'=>$producto->modalidad,
              'precio'=>$producto->precio_unitario,
              //'codigo' => $invcod['codigo'],
              //'preciop'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)][$asociacion_nombre],
              'preciop' => $precioPetro->precio_unitario_petro,
              'nivel_precio'=>$producto->nivel_precio,
              'precios'=>$precios,
              'precio_desactualizado'=>$precio_desactualizado
            ];
            $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
            $cont++;
          }//foreach productos oferta economica
        }else{
          if($oficio->estado_aprobacion_id==3){
            //Aprobación parcial
            foreach($oficio->aprobacion_parcial_productos as $producto){
              if($cont==0)
              $modelos=$producto->producto->descripcion;
              else
              $modelos=$modelos.', '.$producto->producto->descripcion;
              $precios=[];
              if(isset($this->productos[str_slug($producto->producto->nombre.$producto->modalidad)])){
                $precios=[
                  'precio1'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio1'],
                  'precio2'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio2'],
                  'precio3'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio3'],
                  'precio1p'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio1p'],
                  'precio2p'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio2p'],
                  'precio3p'=>$this->productos[str_slug($producto->producto->nombre.$producto->modalidad)]['precio3p']
                ];
              }else{
                return ['error'=>1,'msg'=>'Este modelo no se encuentra configurado en premiumsoft: '.$producto->producto->descripcion];
              }
              // $nivel_precio=0;
              //Cambio solicitado el 5 de junio 2019 que siempre sea precio 1 y ya no distinga por cantidad.
              $nivel_precio=1;
              $precio=$precios['precio1'];
              $preciop=$precios['precio1p'];
              // $precio=0;
              // if($producto->cantidad>49){
              //   $precio=$precios['precio2'];
              //   $nivel_precio=2;
              // }
              // else{
              //   $precio=$precios['precio1'];
              //   $nivel_precio=1;
              // }
              $cantidad_almacen=0;
              foreach($this->productosSistProd[str_slug($producto->producto->nombre)] as $dpa){
                if($dpa['modalidad']==$producto->modalidad){
                  $cantidad_almacen=$dpa['disponibilidad'];
                  break;
                }
              }
              $invcod = Invcodalternativo::where('codalternativo', 'like', $producto->producto->descripcion)->first();
              $productos[]=[
                'producto_id'=>$producto->producto_id,
                'cantidad'=>$producto->cantidad,
                'cantidad_max'=>$producto->cantidad,
                'cantidad_almacen'=>$cantidad_almacen,
                'modelo'=>$producto->producto->nombre,
                'descripcion'=>$producto->producto->descripcion,
                'modalidad'=>$producto->modalidad,
                'precio'=>$precio,
                'invcod' => $invcod,
                'preciop'=>$preciop,
                //'preciop' => Invcodalternativo::where('codalternativo', 'like', $producto->precio_unitario)->first()->preciofin1,
                'nivel_precio'=>$nivel_precio,
                'precios'=>$precios
              ];
              $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
              $cont++;
            }//foreach productos
          }//si es aprobacion parcial
          else if($oficio->estado_aprobacion_id==2){
            //Aprobacion total
            foreach($oficio->solicitud_productos as $producto){
              if($cont==0)
              $modelos=$producto->producto->descripcion;
              else
              $modelos=$modelos.', '.$producto->producto->descripcion;
              $precios=[];
              if(isset($this->productos[str_slug($producto->producto->nombre)])){
                $precios=[
                  'precio1'=>$this->productos[str_slug($producto->producto->nombre)]['precio1'],
                  'precio2'=>$this->productos[str_slug($producto->producto->nombre)]['precio2'],
                  'precio3'=>$this->productos[str_slug($producto->producto->nombre)]['precio3'],
                  'precio1p'=>$this->productos[str_slug($producto->producto->nombre)]['precio1p'],
                  'precio2p'=>$this->productos[str_slug($producto->producto->nombre)]['precio2p'],
                  'precio3p'=>$this->productos[str_slug($producto->producto->nombre)]['precio3p']
                ];
              }else{
                return ['error'=>1,'msg'=>'Este modelo no se encuentra configurado en premiumsoft: '.$producto->producto->descripcion];
              }
              $precio=0;
              $preciop=0;
              $nivel_precio=0;
              if($producto->cantidad>49)
              $nivel_precio=2;
              else
              $nivel_precio=1;
              $cantidad_almacen=0;
              foreach($this->productosSistProd[str_slug($producto->producto->nombre)] as $dpa){
                if($dpa['modalidad']==$producto->modalidad){
                  $cantidad_almacen=$dpa['disponibilidad'];
                  break;
                }
              }
              $invcod = Invcodalternativo::where('codalternativo', 'like', $producto->producto->descripcion)->first();
              $productos[]=[
                'producto_id'=>$producto->producto_id,
                'cantidad'=>$producto->cantidad,
                'cantidad_max'=>$producto->cantidad,
                'modelo'=>$producto->producto->nombre,
                'cantidad_almacen'=>$cantidad_almacen,
                'descripcion'=>$producto->producto->descripcion,
                'precio'=>0,
                'invcod' => $invcod,
                'preciop'=>0,
                //'preciop' => Invcodalternativo::where('codalternativo', 'like', $producto->precio_unitario)->first()->preciofin1,
                'nivel_precio'=>$nivel_precio,
                'precios'=>$precios
              ];
              $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
              $cont++;
            }//foreach productos
          }//estado aprobacion
        }//else no posee oferta economica.
        $arreglo[]=[
          'id'=>$oficio->id,
          'num_oferta'=>$oficio->oferta_economica ? $oficio->oferta_economica->id : null,
          'user_id'=>$oficio->user_id,
          'verificacion_pago'=>$oficio->verificacion_pago,
          //'usuario'=>$oficio->usuario_creador->name,
          'usuario'=>$oficio->usuario_creador ? $oficio->usuario_creador->name : "",
          'tipo_aprobacion'=>'parcial',
          'numero_oficio'=>$oficio->numero_oficio,
          'estado'=>$oficio->estado->estado,
          'documento'=>$oficio->documento,
          'descripcion'=>$oficio->descripcion,
          'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
          'personal_natural'=>[
            'id'=>$oficio->personal_natural_id,
            'cedula'=>$oficio->personal_natural->cedula,
            'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
            'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
            'telefono'=>$oficio->personal_natural->telefono,
            'correo_electronico'=>$oficio->personal_natural->correo_electronico,
            'firma_personal'=>$oficio->personal_natural->firma_personal
          ],
          'personal_juridico'=>$personal_juridico,
          'productos'=>$productos,
          'fecha_aprobacion'=>$oficio->updated_at->format('d-m-Y'),
          'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
          'modelos'=>$modelos,
          'cantidad_solicitada'=>$cantidad_solicitada,
          'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
          'solicitante_ente_nombre'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
          'solicitante_ente_ente'=>$ente,
          'oferta_economica'=>$oferta_economica,
          'oferta_economica_pagos'=>$pagos
        ];


        $contg++;
      }//oficios
    }//else proceso normal, agente autorizado/comercialización - ejemplo de usuario: maria de los angeles
    return ['error'=>0,'oficios'=>$arreglo];
  }//transformadorOficios($param)

  public function index(){
    $this->loadGlobalVars();
    $oficios=$this->obtenerOficios();//
    $oficios=$this->transformadorOficios($oficios);
    //dd($oficios);
    if($oficios['error']==1)
    return redirect('/home')->with('error',$oficios['msg']);
    $oficios=$oficios['oficios'];
    $estados=Territorios_estado::all();
    $clasificacion_entes=ClasificacionEnte::all();
    $preciosPremium=[];
    foreach($this->productos as $key => $val){
      foreach($val as $key2=>$val2){
        if($key2=="precio1"){
          $preciosPremium[$key]["1"]=$val2;
        }else if($key2=="precio2"){
          $preciosPremium[$key]["2"]=$val2;
        }else if($key2=="precio3"){
          $preciosPremium[$key]["3"]=$val2;
        }else if($key2=="precio1p"){
          $preciosPremium[$key]["4"]=$val2;
        }else if($key2=="precio2p"){
          $preciosPremium[$key]["5"]=$val2;
        }else if($key2=="precio3p"){
          $preciosPremium[$key]["6"]=$val2;
        }
      }
    }

    //dd($this->productos);

    $productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
    return view('controlOficios.GestionSolicitudes.GestionSolicitudes',['oficios'=>json_encode($oficios),'preciosPremium'=>$preciosPremium,'productos'=>$productos,'productosSistProd'=>$this->productosSistProd,'estados'=>$estados,'clasificacion_entes'=>$clasificacion_entes]);
  }//index()

  public function crearOfertaEconomica(Request $request){
    //Recibe oficio,total
    try {
      DB::beginTransaction();
      $moneda = Moneda::where('sigular', "Petro")->first();
      $oficio=json_decode(json_encode($request->oficio));
      $total=$request->total;
      $total_petro=$request->total_petro;
      $oferta=new OfertaEconomica();
      $oferta->control_oficio_registro_oficio_id=$oficio->id;
      $oferta->precio_total=$total;
      $oferta->precio_total_petro=$total_petro;
      $oferta->tasa_dia_petro = $moneda->ultimacotizacion;
      $oferta->save();
      foreach($oficio->productos as $producto){
        $detalleOferta=new detOfertaEconomica();
        $detalleOferta->control_oficio_oferta_economica_id=$oferta->id;
        $detalleOferta->precio_unitario=$producto->precio;
        $detalleOferta->precio_unitario_petro=$producto->preciop;
        $detalleOferta->producto_id=$producto->producto_id;
        $detalleOferta->modalidad=$producto->modalidad;
        // $detalleOferta->producto_nombre=$producto->modelo;
        // $detalleOferta->producto_descripcion=$producto->descripcion;
        $detalleOferta->cantidad=$producto->cantidad;
        $detalleOferta->nivel_precio=$producto->nivel_precio;
        $detalleOferta->save();
      }//foreach productos
      DB::commit();
      $this->loadGlobalVars();
      $oficios=$this->obtenerOficios();//
      $oficios=$this->transformadorOficios($oficios);
      if($oficios['error']==1)
      return redirect('/home')->with('error',$oficios['msg']);
      $oficios=$oficios['oficios'];
      return response()->json(['error'=>0,'msg'=>'Cotización generada exitosamente.','oficios'=>$oficios]);
      // return response()->json(['error'=>0,'msg'=>'Cotización generada exitosamente.','oficios'=>$oficios);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage(),'lineSystem'=>$e->getLine()]);
    }//catch()
  }//crearOfertaEconomica()

  public function actualizarProductoOfertaEconomica(Request $request){
    //Recibe Producto,Total
    try {
      DB::beginTransaction();
      $moneda = Moneda::where('singular', "Petro")->first();
      $producto=json_decode(json_encode($request->Producto));
      //Get precio petro
      // $detProducto=detOfertaEconomica::where('id', $producto->detalle_oferta_economica_id)->first();
      // $invcod = Invcodalternativo::where('codalternativo', $detProducto->producto->nombre.$detProducto->modalidad)->first();
      // $precioPetro = PrecioPetro::where('codigo', trim($invcod->codigo))->first();
      // $precioUnitarioPetro=$precioPetro->precio1;
      // if((int)$producto->nivel_precio==2){
      //   $precioUnitarioPetro=$precioPetro->precio2;
      //
      // }else if((int)$producto->nivel_precio==3){
      //   $precioUnitarioPetro=$precioPetro->precio3;
      //
      // }
      //
      $detalleOferta=detOfertaEconomica::where('id', $producto->detalle_oferta_economica_id)
      ->update(['cantidad' => $producto->cantidad,'precio_unitario'=>$producto->precio,'precio_unitario_petro'=>$producto->precio_petro,'nivel_precio'=>$producto->nivel_precio]);
      $detalleOferta=detOfertaEconomica::where('id', $producto->detalle_oferta_economica_id)->first();
      $oferta=OfertaEconomica::where('id',$detalleOferta->control_oficio_oferta_economica_id)->first();
      $oferta->tasa_dia_petro = $moneda->ultimacotizacion;
      $oferta->precio_total=$request->Total;
      $oferta->precio_total_petro=$request->total_petro;
      $oferta->update();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Datos del producto actualizados correctamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//actualizarProductoOfertaEconomica()

  public function agregarProductoOfertaEconomica(Request $request){
    try {
      DB::beginTransaction();
      $this->loadGlobalVars();
      $precios=$this->productos[str_slug($request->nombre.$request->modalidad)];
      $productos=Productos::where('id',$request->producto_id)->get();
      if(count($this->productosSistProd)==0){
        foreach($productos as $producto){
          $arreglo=[];
          $modalidades=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->select('modalidad')->distinct()->get();
          foreach($modalidades as $modalidad){
            $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$producto->nombre)->where('modalidad',$modalidad->modalidad)->where('idAlmacen',4)->sum('cantidadEnvia');
            if($disponibilidad>0){
              $disponibilidad=$disponibilidad-obtenerComprometido($producto->id,$modalidad->modalidad);
            }//Si disponibilidad > 0
            $arreglo[]=[
              'modalidad'=>$modalidad->modalidad,
              'disponibilidad'=>$disponibilidad,
              'nombre'=>$producto->nombre,
              'id'=>$producto->id,
              'descripcion'=>$producto->descripcion,
            ];
          }
          $this->productosSistProd[str_slug($producto->nombre)]=$arreglo;
        }//productos
      }
      $cantidad_almacen=0;
      foreach($this->productosSistProd[str_slug($productos[0]->nombre)] as $modalidad){
        if($modalidad['modalidad']==$request->modalidad){
          $cantidad_almacen=$modalidad['disponibilidad'];
          break;
        }
      }
      $data=$request->all();
      $data['precio_unitario']=$precios['precio1'];
      $data['precio_unitario_petro']=$precios['precio1p'];
      $detOferta=detOfertaEconomica::create($data);
      $total=0;
      $total_petro=0;
      $oferta=OfertaEconomica::where('id',$detOferta->control_oficio_oferta_economica_id)->first();
      foreach($oferta->detalle_oferta as $det){
        $total=(float)$total+((float)$det->cantidad*(float)$det->precio_unitario);
        $total_petro=(float)$total_petro+((float)$det->cantidad*(float)$det->precio_unitario_petro);
      }//foreach($oferta->detalle_oferta as $det)

      $oferta->precio_total=$total;
      $oferta->precio_total_petro=$total_petro;
      $oferta->update();
      $transformerData=(object)[
        "cantidad"=>$detOferta->cantidad,
        "cantidad_almacen"=>$cantidad_almacen,
        "cantidad_max"=>50000,
        "descripcion"=>$detOferta->producto->descripcion,
        "modelo"=>$detOferta->producto->nombre,
        "modalidad"=>$detOferta->modalidad,
        "detalle_oferta_economica_id"=>$detOferta->id,
        "nivel_precio"=>$detOferta->nivel_precio,
        "precio"=>$detOferta->precio_unitario,
        "precio_desactualizado"=>0,
        "precios"=>$precios,
        "producto_id"=>$detOferta->producto_id
      ];
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Producto agregado correctamente.','data'=>$transformerData]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//agregarProductoOfertaEconomica

  public function eliminarProductoOfertaEconomica(Request $request){
    try {
      DB::beginTransaction();
      $detOferta=detOfertaEconomica::where('id',$request->detalle_oferta_economica_id)->first();
      detOfertaEconomica::where('id',$request->detalle_oferta_economica_id)->delete();
      $total=0;
      $total_petro=0;
      $oferta=OfertaEconomica::where('id',$detOferta->control_oficio_oferta_economica_id)->first();
      foreach($oferta->detalle_oferta as $det){
        $total=(float)$total+((float)$det->cantidad*(float)$det->precio_unitario);
        $total_petro=(float)$total_petro+((float)$det->cantidad*(float)$det->precio_unitario_petro);
      }//foreach($oferta->detalle_oferta as $det)

      $oferta->precio_total=$total;
      $oferta->precio_total_petro=$total_petro;
      $oferta->update();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Producto eliminado correctamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//eliminarProductoOfertaEconomica()

  public function actualizarProductosOfertaEconomica(Request $request){
    //Recibe Productos,Total
    try {
      DB::beginTransaction();
      $moneda = Moneda::where('sigular', 'Petro')->first();
      $productos=json_decode(json_encode($request->Productos));
      foreach($productos as $producto){
        $detalleOferta=detOfertaEconomica::where('id', $producto->detalle_oferta_economica_id)
        ->update(
          [
          'cantidad' => $producto->cantidad,
          'precio_unitario'=>$producto->precio,
          'precio_unitario_petro'=>$producto->preciop,
          'nivel_precio'=>$producto->nivel_precio,
          'producto_id'=>$producto->producto_id,
          'modalidad'=>$producto->modalidad
        ]);
        $detalleOferta=detOfertaEconomica::where('id', $producto->detalle_oferta_economica_id)->first();

      }//foreach productos
      $oferta=OfertaEconomica::where('id',$request->Oferta_economica_id)->first();
      $oferta->precio_total=$request->Total;
      $oferta->precio_total_petro=$request->total_petro;
      $oferta->tasa_dia_petro = $moneda->ultimacotizacion;
      $oferta->update();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Datos de productos actualizados correctamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//actualizarProductoOfertaEconomica()

  public function obtenerOficiosFiltro(Request $request){
    $oficios=[];
    if($request->tipo_solicitante==0 && $request->estado_id==0){
      $oficios=$this->obtenerOficios();//
    }else if($request->tipo_solicitante==0 && $request->estado_id!=0){
      //Búsqueda solo por estado
      $oficios=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
      ->whereIn('estado_aprobacion_id',[2,3])
      ->where('motivo_solicitud_id',1)//Solo oficios de jornada de telefono
      ->whereBetween('plan_venta', [$request->fechaInicio,$request->fechaInicio])
      ->orderBy('created_at','ASC')->where('estado_id',$request->estado_id)->get();
    }else if($request->tipo_solicitante!=0){
      //Tipo de solicitante especifico y sin seleccionar estado
      $oficiosTemp=[];
      $oficios=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
      ->whereIn('estado_aprobacion_id',[2,3])
      ->where('motivo_solicitud_id',1)//Solo oficios de jornada de telefono
      ->whereBetween('plan_venta', [$request->fechaInicio,$request->fechaInicio])
      ->orderBy('created_at','ASC')->get();
      if($request->tipo_solicitante==1){
        if($request->estado_id==0){
          //Oficios de personal natural sin filtro de estado
          foreach($oficios as $oficio){
            if($oficio->personal_juridico_id==null){
              $oficiosTemp[]=$oficio;
            }//oficio que apunta a personal juridico
          }//foreach oficios
          $oficios=$oficiosTemp;
        }else{
          //Oficios de personal natural con filtro de estado
          $oficios=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
            $q->withTrashed();
          }])->with(['motivo_solicitud'=>function($q){
            $q->withTrashed();
          }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
          ->whereIn('estado_aprobacion_id',[2,3])
          ->where('motivo_solicitud_id',1)//Solo oficios de jornada de telefono
          ->whereBetween('plan_venta', [$request->fechaInicio,$request->fechaInicio])
          ->orderBy('created_at','ASC')->where('estado_id',$request->estado_id)->get();
          foreach($oficios as $oficio){
            if($oficio->personal_juridico_id==null){
              $oficiosTemp[]=$oficio;
            }//oficio que apunta a personal juridico
          }//foreach oficios
          $oficios=$oficiosTemp;
        }
      }//tipo_solicitante==1
      else if($request->tipo_solicitante==2){
        //Búsqueda por personal jurídico
        if($request->clasificacion_ente_id==0 && $request->ente_id==0){
          //Todos los oficios que sean personal jurídico sin filtros de ente.
          foreach($oficios as $oficio){
            if($oficio->personal_juridico_id!=null){
              $oficiosTemp[]=$oficio;
            }//oficio que apunta a personal juridico
          }//foreach oficios
          $oficios=$oficiosTemp;
        }else if($request->clasificacion_ente_id!=0 && $request->ente_id==0){
          return response()->json(['error'=>1,'msg'=>'Debes seleccionar un ente para el filtrado.']);
        }else{
          if($request->estado_id==0){
            //Todos los oficios por ente sin filtro de estado
            foreach($oficios as $oficio){
              if($oficio->personal_juridico_id!=null){
                // dd($oficio);
                if($oficio->personal_juridico->ente_id==$request->ente_id)
                $oficiosTemp[]=$oficio;
              }//oficio que apunta a personal juridico
            }//foreach oficios
            $oficios=$oficiosTemp;
          }else{
            //Todos los oficios por ente con filtro de estado
            $oficios=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
              $q->withTrashed();
            }])->with(['motivo_solicitud'=>function($q){
              $q->withTrashed();
            }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
            ->whereIn('estado_aprobacion_id',[2,3])
            ->where('motivo_solicitud_id',1)//Solo oficios de jornada de telefono
            ->whereBetween('plan_venta', [$request->fechaInicio,$request->fechaInicio])
            ->orderBy('created_at','ASC')->where('estado_id',$request->estado_id)->get();
            foreach($oficios as $oficio){
              if($oficio->personal_juridico_id!=null){
                // dd($oficio);
                if($oficio->personal_juridico->ente_id==$request->ente_id)
                $oficiosTemp[]=$oficio;
              }//oficio que apunta a personal juridico
            }//foreach oficios
            $oficios=$oficiosTemp;
          }//filtro de ente con estado
        }//else
      }//tipo_solicitante==2 juridico
    }
    $oficios=$this->transformadorOficios($oficios);
    if($oficios['error']==1)
    return redirect('/home')->with('error',$oficios['msg']);
    $oficios=$oficios['oficios'];
    return response()->json(['error'=>0,'oficios'=>$oficios]);
  }//obtenerOficiosFiltro()

  public function generarPDFOferta(Request $request){

    $observaciones=observaciones_generales::orderBy('posicion','ASC')->get();
    $oficio=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
    ->orderBy('created_at','ASC')->where('id',$request->oficio_id)->first();
    // dd($oficio);
    $personal_juridico=null;
    $productos=null;
    $modelos='';
    $cantidad_solicitada=0;
    $ente='';
    $cont=0;
    if($oficio->personal_juridico!=null){
      $personal_juridico=[
        'rif'=>$oficio->personal_juridico->rif_cedula_situr,
        'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
        'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
        'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
        'ente'=>$oficio->personal_juridico->ente->nombre,
        'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
      ];
      //$ente=' / '.$oficio->personal_juridico->ente->nombre;
      $ente=' / '.$oficio->personal_juridico->nombre_institucion;
    }//si existe personal juridico
    foreach($oficio->oferta_economica->detalle_oferta as $producto){
      if($cont==0)
      $modelos=$producto->producto_descripcion;
      else
      $modelos=$modelos.', '.$producto->producto_descripcion;
      $precios=[];
      $productos[]=[
        'detalle_oferta_economica_id'=>$producto->id,
        'producto_id'=>$producto->producto_id,
        'codigo'=>$producto->producto_nombre,
        'descripcion'=>$producto->producto_descripcion,
        'modalidad'=>$producto->modalidad,
        'cantidad'=>$producto->cantidad,
        'modelo'=>$producto->producto->nombre,
        'descripcion'=>$producto->producto->descripcion,
        'precio'=>$producto->precio_unitario,
        'preciop'=>$producto->precio_unitario_petro,
        'nivel_precio'=>$producto->nivel_precio,
      ];
      $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
      $cont++;
    }//foreach productos oferta economica
    $arreglo=[
      'id'=>$oficio->id,
      'user_id'=>$oficio->user_id,
      'oferta_id'=>$oficio->oferta_economica->id,
      //'usuario'=>$oficio->usuario_creador->name,
      'usuario'=>$oficio->usuario_creador ? $oficio->usuario_creador->name : "",
      'tipo_aprobacion'=>'parcial',
      'numero_oficio'=>$oficio->numero_oficio,
      'estado'=>$oficio->estado->estado,
      'documento'=>$oficio->documento,
      'descripcion'=>$oficio->descripcion,
      'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
      'personal_natural'=>[
        'id'=>$oficio->personal_natural_id,
        'cedula'=>$oficio->personal_natural->cedula,
        'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
        'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
        'telefono'=>$oficio->personal_natural->telefono,
        'correo_electronico'=>$oficio->personal_natural->correo_electronico,
        'firma_personal'=>$oficio->personal_natural->firma_personal
      ],
      'personal_juridico'=>$personal_juridico,
      'productos'=>$productos,
      'fecha_aprobacion'=>$oficio->updated_at->format('d-m-Y'),
      'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
      'modelos'=>$modelos,
      'cantidad_solicitada'=>$cantidad_solicitada,
      'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
    ];
    $arreglo=json_decode(json_encode($arreglo));
    // dd($arreglo);
    $mytime = \Carbon\Carbon::now();
    if($oficio->personal_juridico!=null){
      //personal juridico
      $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.OfertaEconomicaPersonalJuridico',
      [
        "titulo"=>"OFERTA ECONOMICA",
        "oficio"=>$arreglo,
        'observaciones'=>$observaciones
      ]);
    }else{
      //Personal natural
      $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.OfertaEconomicaPersonaNatural',
      [
        "titulo"=>"OFERTA ECONOMICA",
        "oficio"=>$arreglo,
        'observaciones'=>$observaciones
      ]);
    }
    return $pdf->stream($mytime->toDateTimeString().' OFERTA ECONOMICA.pdf');
  }//generarPDFOferta()

  public function generarPDFOfertaPetro(Request $request){

    $observaciones=observaciones_generales::orderBy('posicion','ASC')->get();
    $oficio=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
    ->orderBy('created_at','ASC')->where('id',$request->oficio_id)->first();
    // dd($oficio);
    $personal_juridico=null;
    $productos=null;
    $modelos='';
    $cantidad_solicitada=0;
    $ente='';
    $cont=0;

    //dd($oficio->oferta_economica->detalle_oferta);

    if($oficio->personal_juridico!=null){
      $personal_juridico=[
        'rif'=>$oficio->personal_juridico->rif_cedula_situr,
        'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
        'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
        'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
        'ente'=>$oficio->personal_juridico->ente->nombre,
        'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
      ];
      //$ente=' / '.$oficio->personal_juridico->ente->nombre;
      $ente=' / '.$oficio->personal_juridico->nombre_institucion;
    }//si existe personal juridico
    foreach($oficio->oferta_economica->detalle_oferta as $producto){
      if($cont==0)
      $modelos=$producto->producto_descripcion;
      else
      $modelos=$modelos.', '.$producto->producto_descripcion;
      $precios=[];
      $productos[]=[
        'detalle_oferta_economica_id'=>$producto->id,
        'producto_id'=>$producto->producto_id,
        'codigo'=>$producto->producto_nombre,
        'descripcion'=>$producto->producto_descripcion,
        'modalidad'=>$producto->modalidad,
        'cantidad'=>$producto->cantidad,
        'modelo'=>$producto->producto->nombre,
        'descripcion'=>$producto->producto->descripcion,
        'precio'=>$producto->precio_unitario,
        'preciop'=>$producto->precio_unitario_petro,
        'nivel_precio'=>$producto->nivel_precio,
      ];
      $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
      $cont++;
    }//foreach productos oferta economica

    $arreglo=[
      'id'=>$oficio->id,
      'user_id'=>$oficio->user_id,
      'oferta_id'=>$oficio->oferta_economica->id,
      //'usuario'=>$oficio->usuario_creador->name,
      'usuario'=>$oficio->usuario_creador ? $oficio->usuario_creador->name : "",
      'tipo_aprobacion'=>'parcial',
      'numero_oficio'=>$oficio->numero_oficio,
      'estado'=>$oficio->estado->estado,
      'documento'=>$oficio->documento,
      'descripcion'=>$oficio->descripcion,
      'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
      'personal_natural'=>[
        'id'=>$oficio->personal_natural_id,
        'cedula'=>$oficio->personal_natural->cedula,
        'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
        'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
        'telefono'=>$oficio->personal_natural->telefono,
        'correo_electronico'=>$oficio->personal_natural->correo_electronico,
        'firma_personal'=>$oficio->personal_natural->firma_personal
      ],
      'personal_juridico'=>$personal_juridico,
      'productos'=>$productos,
      'fecha_aprobacion'=>$oficio->updated_at->format('d-m-Y'),
      'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
      'modelos'=>$modelos,
      'tasa_dia_petro' => $oficio->oferta_economica->tasa_dia_petro,
      'cantidad_solicitada'=>$cantidad_solicitada,
      'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
    ];
    $arreglo=json_decode(json_encode($arreglo));
    // dd($arreglo);
    $mytime = \Carbon\Carbon::now();
    if($oficio->personal_juridico!=null){
      //personal juridico
      $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.OfertaEconomicaPersonalJuridicoPetro',
      [
        "titulo"=>"OFERTA ECONOMICA",
        "oficio"=>$arreglo,
        'observaciones'=>$observaciones
      ]);
    }else{
      //Personal natural
      $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.OfertaEconomicaPersonaNaturalPetro',
      [
        "titulo"=>"OFERTA ECONOMICA",
        "oficio"=>$arreglo,
        'observaciones'=>$observaciones
      ]);
    }
    return $pdf->stream($mytime->toDateTimeString().' OFERTA ECONOMICA.pdf');
  }//generarPDFOfertaPetro()

  public function generarPDFOfertaHibrido(Request $request){

    $observaciones=observaciones_generales::orderBy('posicion','ASC')->get();
    $oficio=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','oferta_economica','oferta_economica.detalle_oferta','oferta_economica.detalle_oferta.producto','estado','aprobacion_parcial_productos','aprobacion_parcial_productos.producto','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
    ->orderBy('created_at','ASC')->where('id',$request->oficio_id)->first();
    // dd($oficio);
    $personal_juridico=null;
    $productos=null;
    $modelos='';
    $cantidad_solicitada=0;
    $ente='';
    $cont=0;

    //dd($oficio->oferta_economica->detalle_oferta);

    if($oficio->personal_juridico!=null){
      $personal_juridico=[
        'rif'=>$oficio->personal_juridico->rif_cedula_situr,
        'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
        'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
        'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
        'ente'=>$oficio->personal_juridico->ente->nombre,
        'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
      ];
      //$ente=' / '.$oficio->personal_juridico->ente->nombre;
      $ente=' / '.$oficio->personal_juridico->nombre_institucion;
    }//si existe personal juridico
    foreach($oficio->oferta_economica->detalle_oferta as $producto){
      if($cont==0)
      $modelos=$producto->producto_descripcion;
      else
      $modelos=$modelos.', '.$producto->producto_descripcion;
      $precios=[];

      $invcod = Invcodalternativo::where('codalternativo', $producto->producto->nombre.$producto->modalidad)->first();
      $precioPetro = PrecioPetro::where('codigo', trim($invcod->codigo))->first();

      $productos[]=[
        'detalle_oferta_economica_id'=>$producto->id,
        'producto_id'=>$producto->producto_id,
        'codigo'=>$producto->producto_nombre,
        'descripcion'=>$producto->producto_descripcion,
        'modalidad'=>$producto->modalidad,
        'cantidad'=>$producto->cantidad,
        'modelo'=>$producto->producto->nombre,
        'descripcion'=>$producto->producto->descripcion,
        'precio'=>$producto->precio_unitario,
        'preciop'=>$producto->precio_unitario_petro,
        //"preciop" => $precioPetro->precio1,
        'nivel_precio'=>$producto->nivel_precio,
      ];
      $cantidad_solicitada=$cantidad_solicitada+$producto->cantidad;
      $cont++;
    }//foreach productos oferta economica
    $arreglo=[
      'id'=>$oficio->id,
      'user_id'=>$oficio->user_id,
      'oferta_id'=>$oficio->oferta_economica->id,
      'tasa_dia_petro' => $oficio->oferta_economica->tasa_dia_petro,
      //'usuario'=>$oficio->usuario_creador->name,
      'usuario'=>$oficio->usuario_creador ? $oficio->usuario_creador->name : "",
      'tipo_aprobacion'=>'parcial',
      'numero_oficio'=>$oficio->numero_oficio,
      'estado'=>$oficio->estado->estado,
      'documento'=>$oficio->documento,
      'descripcion'=>$oficio->descripcion,
      'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
      'personal_natural'=>[
        'id'=>$oficio->personal_natural_id,
        'cedula'=>$oficio->personal_natural->cedula,
        'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
        'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
        'telefono'=>$oficio->personal_natural->telefono,
        'correo_electronico'=>$oficio->personal_natural->correo_electronico,
        'firma_personal'=>$oficio->personal_natural->firma_personal
      ],
      'personal_juridico'=>$personal_juridico,
      'productos'=>$productos,
      'fecha_aprobacion'=>$oficio->updated_at->format('d-m-Y'),
      'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
      'modelos'=>$modelos,
      'cantidad_solicitada'=>$cantidad_solicitada,
      'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
    ];
    $arreglo=json_decode(json_encode($arreglo));
    // dd($arreglo);
    $mytime = \Carbon\Carbon::now();
    if($oficio->personal_juridico!=null){
      //personal juridico
      $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.OfertaEconomicaPersonalJuridicoHibrido',
      [
        "titulo"=>"OFERTA ECONOMICA",
        "oficio"=>$arreglo,
        'observaciones'=>$observaciones
      ]);
    }else{
      //Personal natural
      $pdf = PDF::loadView('controlOficios.GestionSolicitudes.Reporte.OfertaEconomicaPersonaNaturalHibrido',
      [
        "titulo"=>"OFERTA ECONOMICA",
        "oficio"=>$arreglo,
        'observaciones'=>$observaciones
      ]);
    }
    return $pdf->stream($mytime->toDateTimeString().' OFERTA ECONOMICA.pdf');
  }

  //Asociar Pagos a oferta economica

  public function asociarPago(Request $request){
    //Recibe oficio_id, tipo_pago,referencia,monto,soporte,validacion
    try {
      $oferta_economica=OfertaEconomica::where('control_oficio_registro_oficio_id',$request->oficio_id)->first();
      if(count($oferta_economica)==0)
      return response()->json(['error'=>1,'msg'=>'Cotización no encontrada.']);
      $precio_total=floatval($oferta_economica->precio_total);
      $pagos=PagosOfertaEconomica::where('control_oficio_oferta_economica_id',$oferta_economica->id)->get();
      // $cancelado=0;
      // foreach($pagos as $pago){
      //   $cancelado=floatval($cancelado)+floatval($pago->monto);
      // }//pagos
      // if($cancelado>=$precio_total)
      //   return response()->json(['error'=>1,'msg'=>'No se pueden registrar mas pagos en este oficio.']);
      // if(($cancelado+floatval($request->monto_pago))>$precio_total)
      //   return response()->json(['error'=>1,'msg'=>'El monto a registrar supera el saldo deudor.']);
      $p=PagosOfertaEconomica::where('referencia',$request->referencia_pago)->first();
      if(count($p)>0)
      return response()->json(['error'=>1,'msg'=>'Ya existe un pago con este número de referencia.']);
      DB::beginTransaction();
      $soporte_pago=null;
      if($request->soporte_pago!="" || $request->soporte_pago!="")
      $soporte_pago=$request->soporte_pago;
      $validado=false;
      if($request->tipo_pago=="punto_venta"){
        $validado=true;
        if(strlen($request->soporte_pago)<3)
        return response()->json(['error'=>1,'msg'=>'El número de lote debe ser mayor a 2 dígitos.']);
      }else if($request->tipo_pago=="efectivo")
      $validado=true;
      $pago=PagosOfertaEconomica::create([
        'control_oficio_oferta_economica_id'=>$oferta_economica->id,
        'tipo_pago'=>$request->tipo_pago,
        'referencia'=>$request->referencia_pago,
        'monto'=>number_format($request->monto_pago, 2,".",""),
        'soporte_lote'=>$soporte_pago,
        'validacion'=>$validado
      ]);
      if($request->soporte_pago!="" || $request->soporte_pago!=""){
        $soporte_pago=$request->soporte_pago;
        if(!is_numeric($soporte_pago)){
          $soporte_pago=saveImage($soporte_pago,'pagos/'.$pago->id.'.jpg');
          $pago->soporte_lote=$soporte_pago;
          $pago->update();
        }
      }
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Pago asociado correctamente.','pago'=>$pago]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//asociarPago
  public function asociarReferenciaAdicionalPago(Request $request){
    //Recibe pago_id, imagen
    try {

      $soporte_pago=saveImage($request->imagen,'referenciasPago/'.$request->pago_id.'/'.\Illuminate\Support\Str::random(15).'.jpg');
      $pago=PagosOfertaEconomica::where('id',$request->pago_id)->first();
      $imagenes=$pago->galeria;
      return response()->json(['error'=>0,'msg'=>'Anexo registrado correctamente.','imagenes'=>$imagenes]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//asociarPago
  public function actualizarPago(Request $request){
    //Recibe array pago
    try {
      $pago=json_decode(json_encode($request->pago));
      $pagoOfertaEconomica=PagosOfertaEconomica::where('id',$pago->id)->first();
      if(count($pagoOfertaEconomica)==0)
      return response()->json(['error'=>1,'msg'=>'Este pago no fue encontrado en base de datos.']);
      $p=PagosOfertaEconomica::where('referencia',$pago->referencia)->where('id','!=',$pago->id)->first();
      if(count($p)>0)
      return response()->json(['error'=>1,'msg'=>'Ya existe un pago con este número de referencia.']);
      DB::beginTransaction();
      $soporte_pago=null;
      if($pago->soporte_lote!="" || $pago->soporte_lote!="")
      $soporte_pago=$pago->soporte_lote;
      $pagoActualizado=PagosOfertaEconomica::where('id', $pago->id)
      ->update([
        'tipo_pago' => $pago->tipo_pago,
        'referencia' => $pago->referencia,
        'monto'       =>$pago->monto,
        'soporte_lote'=>$soporte_pago
      ]);
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Pago actualizado correctamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//actualizarPago
  public function borrarPago(Request $request){
    //Recibe pago_id
    try {
      DB::beginTransaction();
      $pagoOfertaEconomica=PagosOfertaEconomica::where('id',$request->pago_id)->first();
      if(count($pagoOfertaEconomica)==0)
      return response()->json(['error'=>1,'msg'=>'Este pago no fue encontrado en base de datos.']);
      $pagoOfertaEconomica->delete();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Pago eliminado correctamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//borrarPago
  public function confirmarPago(Request $request){
    //Recibe pago_id - Ventana de confirmación de pago de marianly cabreras (Administración verifica si el pago de tipo transferencia cayo en la cuenta bancaria.)
    try {
      DB::beginTransaction();
      $pagoOfertaEconomica=PagosOfertaEconomica::where('id',$request->pago_id)->first();
      if(count($pagoOfertaEconomica)==0)
      return response()->json(['error'=>1,'msg'=>'Este pago no fue encontrado en base de datos.']);
      $pagoOfertaEconomica->validacion=true;
      $pagoOfertaEconomica->update();
      $pagos=PagosOfertaEconomica::where('control_oficio_oferta_economica_id',$pagoOfertaEconomica->control_oficio_oferta_economica_id)->get();
      $oferta_economica=OfertaEconomica::where('id',$pagoOfertaEconomica->control_oficio_oferta_economica_id)->first();
      $oficio=Control_oficio_registro_oficio::where('id',$oferta_economica->control_oficio_registro_oficio_id)->first();
      $total_pago=floatval($oferta_economica->precio_total);
      $pagos_realizados=0;
      foreach($pagos as $pago){
        if($pago->tipo_pago!="reintegro" && $pago->validacion==true)
        $pagos_realizados=floatval($pagos_realizados)+floatval($pago->monto);
      }//pagos
      $pago_completado=false;
      if(floatval($pagos_realizados)==floatval($total_pago)){
        // if($oficio->despacho!=null){
        //   $oficio->estado_aprobacion_id=6;
        //   $oficio->update();
        // }//despacho
        $pago_completado=true;
      }
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Pago Validado correctamente.','pago_completado'=>$pago_completado]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//borrarPago

  public function revertirPago(Request $request){
    try {
      DB::beginTransaction();
      $pagoOfertaEconomica=PagosOfertaEconomica::where('id',$request->pago_id)->first();
      if ($pagoOfertaEconomica !=null) {
        $pagoActualizado=PagosOfertaEconomica::where('id', $pagoOfertaEconomica->id)
        ->update([
          'validacion' => False
        ]);
      }

      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Pago Revertido correctamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//borrarPago


  public function registrarDatosChofer(Request $request){

    try {
      DB::beginTransaction();
      $persona_natural=pn::where('cedula',$request->cedula_chofer)->first();

      if ($persona_natural != null) {

          $vehiculo=Vehiculos::firstOrCreate(
            ['placa'=>$request->vehiculo_placa],
            [
            'modelo'=>$request->vehiculo_modelo,
            'descripcion'=>$request->vehiculo_descripcion
          ]);

        $despacho=Despachos::create([
          'requisicion_id'=>$request->requisicion_id,
          // 'registro_oficio_id'=>$request->oficio_id,
          'vehiculo_id'=>$vehiculo->id,
          'chofer_id'=>$persona_natural->id,
          'responsable_id'=>$persona_natural->id
        ]);

      }else {

        $chofer=pn::create([
          'cedula'=>$request->cedula_chofer,
          'nombre'=>$request->chofer_nombres,
          'apellido'=>$request->chofer_apellidos,
          'codigo_carnet_patria'=>$request->codigo_carnet_patria,
          'telefono'=>$request->chofer_numero,
          'correo_electronico'=>$request->chofer_correo,
          'firma_personal'=>false
        ]);

        if(!Vehiculos::where('placa', $request->vehiculo_placa)->first()){
          $vehiculo=Vehiculos::create([
            'placa'=>$request->vehiculo_placa,
            'modelo'=>$request->vehiculo_modelo,
            'descripcion'=>$request->vehiculo_descripcion
          ]);
        }

        $despacho=Despachos::create([
          'requisicion_id'=>$request->requisicion_id,
          // 'registro_oficio_id'=>$request->oficio_id,
          'vehiculo_id'=>$vehiculo->id,
          'chofer_id'=>$chofer->id,
          'responsable_id'=>$request->responsable_id
        ]);

      }

      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Datos registrado exitosamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }

  public function actualizarDatosChofer2(Request $request){
    //Recibe

    try {
      DB::beginTransaction();

      $chofer=pn::where('cedula',$request->cedula_chofer)->first();

      if ($chofer==null) {
        $chofer=pn::create([
          'cedula'=>$request->cedula_chofer,
          'nombre'=>$request->nombre_chofer,
          'apellido'=>$request->apellidos_chofer,
          'codigo_carnet_patria'=>'0000000000',
          'telefono'=>$request->telefono_chofer,
          'correo_electronico'=>$request->correo_chofer,
          'firma_personal'=>false
        ]);

        $chofer=pn::where('cedula',$request->cedula_chofer)->first();

        $chofer_id=$chofer->id;

      }else {

        $chofer=pn::where('cedula',$request->cedula_chofer)->update([
          'cedula'=>$request->cedula_chofer,
          'nombre'=>$request->nombre_chofer,
          'apellido'=>$request->apellidos_chofer,
          'telefono'=>$request->telefono_chofer,
          'correo_electronico'=>$request->correo_chofer,
        ]);

        $chofer=pn::where('cedula',$request->cedula_chofer)->first();

        $chofer_id=$chofer->id;


      }//else if ($chofer== null)

      $vehiculo=Vehiculos::where('placa',$request->vehiculo_placa)->first();

      if ($vehiculo==null) {

        $vehiculo=Vehiculos::firstOrCreate(
          ['placa'=>$request->vehiculo_placa],
          [
          'modelo'=>$request->vehiculo_modelo,
          'descripcion'=>$request->vehiculo_descripcion
        ]);
        $vehiculo=Vehiculos::where('placa',$request->vehiculo_placa)->first();
        $vehiculo_id=$vehiculo->id;

      }else {

        $vehiculo=Vehiculos::where('placa',$request->vehiculo_placa)->update([
          'placa'=>$request->vehiculo_placa,
          'modelo'=>$request->vehiculo_modelo,
          'descripcion'=>$request->vehiculo_descripcion
        ]);
        $vehiculo=Vehiculos::where('placa',$request->vehiculo_placa)->first();
        $vehiculo_id=$vehiculo->id;
      }// else if ($vehiculo== null)

      $despacho=Despachos::where('id',$request->despacho_id)->update([
        'vehiculo_id'=>$vehiculo_id,
        'chofer_id'=>$chofer_id,
      ]);

      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Datos de chofer actualizados exitosamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//actualizarChofer()


  public function registrarChofer(Request $request){
    //Recibe
    try {
      DB::beginTransaction();
      // Vehiculos, pn
      $responsable=pn::where('cedula',$request->cedula_responsable)->first();
      if(count($responsable)==0)
      return response()->json(['error'=>1,'msg'=>'La cédula de este responsable no se encuentra en base de datos.']);
      $chofer=pn::where('cedula',$request->cedula_chofer)->first();
      if(count($chofer)==0)
      return response()->json(['error'=>1,'msg'=>'La cédula de este chofer no se encuentra en base de datos.']);
      $vehiculo=Vehiculos::where('placa',$request->vehiculo_placa)->first();
      if(count($vehiculo)==0)
      return response()->json(['error'=>1,'msg'=>'La placa de este vehículo no se encuentra en base de datos.']);
      $despacho=Despachos::create([
        'registro_oficio_id'=>$request->oficio_id,
        'vehiculo_id'=>$vehiculo->id,
        'chofer_id'=>$chofer->id,
        'responsable_id'=>$responsable->id
      ]);
      $despachoJornada=DespachosJornadas::create([
        'destino'=>$request->destino_despacho,
        'beneficiario'=>$request->beneficiarios,
        'despacho_id'=>$despacho->id
      ]);
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Chofer registrado exitosamente.','despacho_id'=>$despacho->id,'despacho_detalle'=>$despachoJornada->id]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//registrarChofer()



  public function actualizarChofer(Request $request){
    //Recibe
    try {
      DB::beginTransaction();
      // Vehiculos, pn
      $responsable=pn::where('cedula',$request->cedula_responsable)->first();
      if(count($responsable)==0)
      return response()->json(['error'=>1,'msg'=>'La cédula de este responsable no se encuentra en base de datos.']);
      $chofer=pn::where('cedula',$request->cedula_chofer)->first();
      if(count($chofer)==0)
      return response()->json(['error'=>1,'msg'=>'La cédula de este chofer no se encuentra en base de datos.']);
      $vehiculo=Vehiculos::where('placa',$request->vehiculo_placa)->first();
      if(count($vehiculo)==0)
      return response()->json(['error'=>1,'msg'=>'La placa de este vehículo no se encuentra en base de datos.']);
      $despacho=Despachos::where('id',$request->despacho_id)->update([
        'vehiculo_id'=>$vehiculo->id,
        'chofer_id'=>$chofer->id,
        'responsable_id'=>$responsable->id
      ]);
      $despachoJornada=DespachosJornadas::where('despacho_id',$request->despacho_id)->update([
        'destino'=>$request->destino_despacho,
        'beneficiario'=>$request->beneficiarios
      ]);
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Datos de chofer actualizados exitosamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//actualizarChofer()

  public function denegarCompra(Request $request){
    try {
      DB::beginTransaction();
      if($request->reintegro){
        //Si debe hacerse re-integro
        if(strlen($request->num_cuenta)!=24)
        return response()->json(['error'=>1,'msg'=>'Número de cuenta inválido.']);
        if($request->tipo_personal!="natural"){
          //Juridico
          if(strlen($request->identificacion)!=11)
          return response()->json(['error'=>1,'msg'=>'R.I.F inválido.']);
        }//juridico
        $oferta_economica=OfertaEconomica::find($request->oferta_economica_id);
        $reintegro=ReintegroCuenta::create([
          'numero_cuenta'=>$request->num_cuenta,
          'tipo_cuenta'=>$request->tipo_cuenta,
          'banco'=>$request->banco,
          'identificacion'=>$request->identificacion,
          'nombre'=>$request->nombre,
          'correo_electronico'=>$request->correo,
          'control_oficio_oferta_economica_id'=>$request->oferta_economica_id
        ]);
        $oferta_economica->reintegro=true;
        $oferta_economica->update();
      }//Si debe hacerse re-integro
      $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
      $oficio->estado_aprobacion_id=4;//Denegado la Cotización.
      $oficio->update();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Cotización denegada exitosamente.']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//denegarCompra()

  public function actualizarEstadoOficio(Request $request){
    //recibe oficio_id, estado
    try {
      DB::beginTransaction();
      $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
      $oficio->estado_aprobacion_id=$request->estado;
      $oficio->update();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Estado de la Cotización actualizado exitosamente..']);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }
}
