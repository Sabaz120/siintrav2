<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_materiales as Materiales;
use DB;//Manejo de transacciones
class GestionMaterialesController extends Controller
{
  public function gestionMaterial(){
    //Esta función envía el listado de materiales registrados
    $materiales=Materiales::all();
    return view('controlOficios.GestionMateriales.crearMateriales',['materiales'=>$materiales]);
  }//gestionMaterial()

  public function registrarMaterial(Request $request){
    try {
      DB::beginTransaction();
      $material=Materiales::where('nombre',strtolower($request->nombre))->first();
      if(count($material)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe un material con este nombre registrado.']);
      $material = Materiales::firstOrCreate(
        ['nombre' => strtolower($request->nombre)], ['descripcion' => $request->descripcion]
      );
      $materiales=Materiales::all();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','materiales'=>$materiales]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//registrarMaterial()

  public function actualizarMaterial(Request $request){
    try {
      DB::beginTransaction();
      $material=Materiales::where('nombre',strtolower($request->nombre))->where('id','!=',$request->id)->first();
      if(count($material)>0)
        return response()->json(['error'=>1,'msg'=>'Ya existe un material con este nombre registrado.']);
      $material = Materiales::where('id',$request->id)->update(['nombre'=>strtolower($request->nombre),'descripcion'=>$request->descripcion]);
      $materiales=Materiales::all();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Actualización satisfactoria.','materiales'=>$materiales]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//actualizarMaterial

  public function eliminarMaterial(Request $request){
    try {
      DB::beginTransaction();
      $material = Materiales::where('id',$request->id)->delete();
      $materiales=Materiales::all();
      DB::commit();
      return response()->json(['error'=>0,'msg'=>'Material eliminado correctamente.','materiales'=>$materiales]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }//catch
  }//eliminarMaterial()
}
