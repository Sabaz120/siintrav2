<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico as responsablesEmpresa;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\MotivoSolicitud;
use App\Modelos\Territorios\Territorios_estado;
use App\Modelos\ControlOficios\Departamentos;
use App\Modelos\Sigesp\sno_personal;
use App\Modelos\ControlOficios\Ente;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material_aprobacion_parcial;
use App\Modelos\ControlOficios\Control_oficio_materiales;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\ControlOficios\Control_oficio_despacho;
use App\Modelos\ControlOficios\Control_oficio_vehiculo;
use PDF;
use DB;
use Validator;
class ListadoDonacionesController extends Controller
{

  public function index(){
    // $oficios=$this->BuscarDatosDespacho(0,'','');
    $oficios=[];
    $materiales=Control_oficio_materiales::all();
    return view('controlOficios.GestionSolicitudesDonativosAprobadas.listadoDonaciones')->with([
      'oficios'=>json_encode($oficios),
      'materiales'=>$materiales
    ]);
  }//public function index()

  public function obtenerDatosChofer(Request $request){
    $condicional="";
    $parametros_estatus=[7,8,9];
    $estatus = 2;
    if($estatus==2){
      $parametros_estatus=[7];
    }//if($estatus==2)
    $oficiosConsulta=Control_oficio_registro_oficio::join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
    ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
    ->join('control_oficio_despachos','control_oficio_despachos.registro_oficio_id','=','control_oficio_registro_oficio.id')
    ->join('control_oficio_vehiculos','control_oficio_vehiculos.id','=','control_oficio_despachos.vehiculo_id')
    ->join('public.estados','public.estados.id','=','control_oficio_registro_oficio.estado_aprobacion_id')
    ->where('motivo_solicitud_id','3')
    ->where('registro_oficio_id',$request->id_oficio)
    ->whereIn('control_oficio_registro_oficio.estado_aprobacion_id', $parametros_estatus)
    // ->whereRaw($condicional)
    ->select('control_oficio_registro_oficio.created_at',
    'control_oficio_registro_oficio.fecha_estado',
    'control_oficio_personal_natural.cedula',
    DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
    'control_oficio_personal_natural.trabajador_vtelca',
    'control_oficio_registro_oficio.personal_natural_id',
    'control_oficio_registro_oficio.personal_juridico_id',
    'control_oficio_registro_oficio.id as id_oficio',
    'control_oficio_registro_oficio.estado_aprobacion_id',
    'control_oficio_despachos.responsable_id',
    'control_oficio_despachos.chofer_id',
    'control_oficio_despachos.vehiculo_id',
    'control_oficio_despachos.registro_oficio_id',
    'control_oficio_vehiculos.modelo',
    'control_oficio_vehiculos.placa',
    'control_oficio_despachos.fecha_despacho',
    'public.estados.nombre as nombre_estado'
    )->get();

    foreach ($oficiosConsulta as &$key) {
      $DatosChofer=   $persona_natural=pn::where('id',$key->chofer_id)->first();
      $key['nombre_chofer']=$DatosChofer->nombre;
    }
    return response()->json(['error'=>0,'DatosChofer'=>$oficiosConsulta]);


  }

  public function BuscarDatosDespacho($estatus,$fechaDespacho,$fechaDespachofin){
    //Esta la modifico luis para que le trajera los registros en listado de donaciones por despachar
    $oficios=[];
    $condicional="";
    $parametros_estatus=[7,8,9];
    if($estatus==2){
      $parametros_estatus=[7];
    }//if($estatus==2)
    if($fechaDespacho=="")
    $condicional="1=1";
    else
    $condicional="control_oficio_despachos.fecha_despacho BETWEEN '".$fechaDespacho."' and '".$fechaDespachofin."'";
    /*$oficiosConsulta=Control_oficio_registro_oficio::join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
    ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
    ->join('control_oficio_despachos','control_oficio_despachos.registro_oficio_id','=','control_oficio_registro_oficio.id')
    ->join('control_oficio_vehiculos','control_oficio_vehiculos.id','=','control_oficio_despachos.vehiculo_id')
    ->join('public.estados','public.estados.id','=','control_oficio_registro_oficio.estado_aprobacion_id')
    ->where('motivo_solicitud_id','3')
    ->whereIn('control_oficio_registro_oficio.estado_aprobacion_id', $parametros_estatus)
    ->whereRaw($condicional)
    ->select('control_oficio_registro_oficio.created_at',
    'control_oficio_registro_oficio.fecha_estado',
    'control_oficio_personal_natural.cedula',
    DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
    'control_oficio_personal_natural.trabajador_vtelca',
    'control_oficio_registro_oficio.personal_natural_id',
    'control_oficio_registro_oficio.personal_juridico_id',
    'control_oficio_registro_oficio.id as id_oficio',
    'control_oficio_registro_oficio.estado_aprobacion_id',
    'control_oficio_despachos.responsable_id',
    'control_oficio_despachos.chofer_id',
    'control_oficio_despachos.vehiculo_id',
    'control_oficio_despachos.registro_oficio_id',
    'control_oficio_vehiculos.modelo',
    'control_oficio_vehiculos.placa',
    'control_oficio_despachos.fecha_despacho',
    'public.estados.nombre as nombre_estado'
    )->get();
    */
    $oficiosConsulta=Control_oficio_registro_oficio::join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
    ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
    ->join('control_oficio_despachos','control_oficio_despachos.registro_oficio_id','=','control_oficio_registro_oficio.id')

    ->join('public.estados','public.estados.id','=','control_oficio_registro_oficio.estado_aprobacion_id')
    ->where('motivo_solicitud_id','3')
    ->whereIn('control_oficio_registro_oficio.estado_aprobacion_id', $parametros_estatus)
    ->whereRaw($condicional)
    ->select('control_oficio_registro_oficio.created_at',
    'control_oficio_registro_oficio.fecha_estado',
    'control_oficio_personal_natural.cedula',
    DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
    'control_oficio_personal_natural.trabajador_vtelca',
    'control_oficio_registro_oficio.personal_natural_id',
    'control_oficio_registro_oficio.personal_juridico_id',
    'control_oficio_registro_oficio.id as id_oficio',
    'control_oficio_registro_oficio.estado_aprobacion_id',
    'control_oficio_despachos.responsable_id',
    'control_oficio_despachos.registro_oficio_id',
    'control_oficio_despachos.fecha_despacho',
    'public.estados.nombre as nombre_estado'
    )->OrderBy('control_oficio_despachos.fecha_despacho','DESC')->get();
    foreach($oficiosConsulta as &$oficio){
      $tipo_solicitante="";
      if($oficio->trabajador_vtelca==true){
        $tipo_solicitante="Trabajador";
      }//if($oficio->trabajador_vtelca==true)
      else if($oficio->personal_juridico_id!=null){
        $tipo_solicitante="Persona Juridica";
      }//else if($oficio->personal_juridico_id!=null)
      else{
        $tipo_solicitante="Persona Natural";
      }//else
      $materiales=$this->buscarMaterialesAprobados($oficio->id_oficio,$oficio->estado_aprobacion_id);
      $DatosChofer=   $persona_natural=pn::where('id',$oficio->chofer_id)->first();
      $DatosResponsable=   $persona_natural=pn::where('id',$oficio->responsable_id)->first();
      $oficios[]=[
        'id_oficio'=>$oficio->id_oficio,
        'fecha_solicitud'=>$oficio->created_at->format('d-m-Y'),
        'fecha_estado'=> substr($oficio->fecha_estado,8,2)."-".substr($oficio->fecha_estado,5,2)."-".substr($oficio->fecha_estado,0,4),
        'nombre_responsable'=>$oficio->nombre_responsable,
        'cedulaResponsable'=>$oficio->cedula,
        'tipo_solicitante'=> $tipo_solicitante,
        'registro_oficio_id'=>$oficio->registro_oficio_id,
        'fecha_despacho'=>$oficio->fecha_despacho,
        'nombres_materiales'=>$materiales['materiales'],
        'materiales_cantidad'=>$materiales['cantidad'],
        'nombre_estado'=>$oficio->nombre_estado,
      ];
    }//foreach($oficiosConsulta as &$oficio)
    return($oficios);
  }//public function BuscarDatosDespachoPorFecha

  public function BuscarDatosDespachov2($estatus,$fechaDespacho,$fechaDespachofin){
    //Esta era la original, que actualmente solo funciona para el reporte pdf de listado de donaciones
    $oficios=[];
    $condicional="";
    $parametros_estatus=[7,8,9];
    if($estatus==2){
      $parametros_estatus=[7];
    }//if($estatus==2)
    if($fechaDespacho=="")
    $condicional="1=1";
    else
    $condicional="control_oficio_despachos.fecha_despacho BETWEEN '".$fechaDespacho."' and '".$fechaDespachofin."'";
    /*$oficiosConsulta=Control_oficio_registro_oficio::join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
    ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
    ->join('control_oficio_despachos','control_oficio_despachos.registro_oficio_id','=','control_oficio_registro_oficio.id')
    ->join('control_oficio_vehiculos','control_oficio_vehiculos.id','=','control_oficio_despachos.vehiculo_id')
    ->join('public.estados','public.estados.id','=','control_oficio_registro_oficio.estado_aprobacion_id')
    ->where('motivo_solicitud_id','3')
    ->whereIn('control_oficio_registro_oficio.estado_aprobacion_id', $parametros_estatus)
    ->whereRaw($condicional)
    ->select('control_oficio_registro_oficio.created_at',
    'control_oficio_registro_oficio.fecha_estado',
    'control_oficio_personal_natural.cedula',
    DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
    'control_oficio_personal_natural.trabajador_vtelca',
    'control_oficio_registro_oficio.personal_natural_id',
    'control_oficio_registro_oficio.personal_juridico_id',
    'control_oficio_registro_oficio.id as id_oficio',
    'control_oficio_registro_oficio.estado_aprobacion_id',
    'control_oficio_despachos.responsable_id',
    'control_oficio_despachos.chofer_id',
    'control_oficio_despachos.vehiculo_id',
    'control_oficio_despachos.registro_oficio_id',
    'control_oficio_vehiculos.modelo',
    'control_oficio_vehiculos.placa',
    'control_oficio_despachos.fecha_despacho',
    'public.estados.nombre as nombre_estado'
    )->get();
    */
    $oficiosConsulta=Control_oficio_registro_oficio::join('control_oficio_motivo_solicitud', 'control_oficio_motivo_solicitud.id', '=', 'control_oficio_registro_oficio.motivo_solicitud_id')
    ->join('control_oficio_personal_natural', 'control_oficio_personal_natural.id', '=', 'control_oficio_registro_oficio.personal_natural_id')
    ->join('control_oficio_despachos','control_oficio_despachos.registro_oficio_id','=','control_oficio_registro_oficio.id')
    ->join('control_oficio_vehiculos','control_oficio_vehiculos.id','=','control_oficio_despachos.vehiculo_id')
    ->join('public.estados','public.estados.id','=','control_oficio_registro_oficio.estado_aprobacion_id')
    ->where('motivo_solicitud_id','3')
    ->whereIn('control_oficio_registro_oficio.estado_aprobacion_id', $parametros_estatus)
    ->whereRaw($condicional)
    ->select('control_oficio_registro_oficio.created_at',
    'control_oficio_registro_oficio.fecha_estado',
    'control_oficio_personal_natural.cedula',
    DB::raw("CONCAT(control_oficio_personal_natural.apellido,' ',control_oficio_personal_natural.nombre) as nombre_responsable"),
    'control_oficio_personal_natural.trabajador_vtelca',
    'control_oficio_registro_oficio.personal_natural_id',
    'control_oficio_registro_oficio.personal_juridico_id',
    'control_oficio_registro_oficio.id as id_oficio',
    'control_oficio_registro_oficio.estado_aprobacion_id',
    'control_oficio_despachos.responsable_id',
    'control_oficio_despachos.chofer_id',
    'control_oficio_despachos.vehiculo_id',
    'control_oficio_despachos.registro_oficio_id',
    'control_oficio_vehiculos.modelo',
    'control_oficio_vehiculos.placa',
    'control_oficio_despachos.fecha_despacho',
    'public.estados.nombre as nombre_estado'
    )->OrderBy('control_oficio_despachos.fecha_despacho','DESC')->get();
    foreach($oficiosConsulta as &$oficio){
      $tipo_solicitante="";
      if($oficio->trabajador_vtelca==true){
        $tipo_solicitante="Trabajador";
      }//if($oficio->trabajador_vtelca==true)
      else if($oficio->personal_juridico_id!=null){
        $tipo_solicitante="Persona Juridica";
      }//else if($oficio->personal_juridico_id!=null)
      else{
        $tipo_solicitante="Persona Natural";
      }//else
      $materiales=$this->buscarMaterialesAprobados($oficio->id_oficio,$oficio->estado_aprobacion_id);
      $DatosChofer=   $persona_natural=pn::where('id',$oficio->chofer_id)->first();
      $DatosResponsable=   $persona_natural=pn::where('id',$oficio->responsable_id)->first();
      $oficios[]=[
        'id_oficio'=>$oficio->id_oficio,
        'fecha_solicitud'=>$oficio->created_at->format('d-m-Y'),
        'fecha_estado'=> substr($oficio->fecha_estado,8,2)."-".substr($oficio->fecha_estado,5,2)."-".substr($oficio->fecha_estado,0,4),
        'nombre_responsable'=>$oficio->nombre_responsable,
        'tipo_solicitante'=> $tipo_solicitante,
        'responsable_id'=>$oficio->responsable_id,
        'cedulaResponsable'=>$oficio->cedula,
        'nombresResponsableDespacho'=>$DatosResponsable->apellido." ".$DatosResponsable->nombre,
        'telefonoResponsableDespacho'=>$DatosResponsable->telefono,
        'chofer_id'=>$oficio->chofer_id,
        'cedulaChofer'=>$DatosChofer->cedula,
        'nombresChofer'=>$DatosChofer->apellido." ".$DatosChofer->nombre,
        'telefonoChofer'=>$DatosChofer->telefono,
        'vehiculo_id'=>$oficio->vehiculo_id,
        'registro_oficio_id'=>$oficio->registro_oficio_id,
        'modelo'=>$oficio->modelo,
        'placa'=>$oficio->placa,
        'fecha_despacho'=>$oficio->fecha_despacho,
        'nombres_materiales'=>$materiales['materiales'],
        'materiales_cantidad'=>$materiales['cantidad'],
        'nombre_estado'=>$oficio->nombre_estado,
      ];
    }//foreach($oficiosConsulta as &$oficio)
    return($oficios);
  }//public function BuscarDatosDespachoPorFecha

  public function buscarMaterialesAprobados($idOficio,$tipoEstadoid){

    $materiales=Control_oficio_solicitud_material_aprobacion_parcial::
    join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')
    ->where('registro_oficio_id',$idOficio)
    ->select('control_oficio_materiales.nombre as nombre','control_oficio_material_aprobacion_parcial.cantidad as cantidad')
    ->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
    if(count($materiales)!=0)
    {
      //Materiales en aprobación parcial
      $nombres_materiales="";
      foreach($materiales as $material)
      $nombres_materiales=$nombres_materiales.$material->nombre.", ";
      $nombres_materiales =  trim($nombres_materiales, ', ');
      $cantidad=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')
      ->where('registro_oficio_id',$idOficio)->sum('cantidad');
      $materiales_cantidad=['materiales'=>$nombres_materiales,'cantidad'=>$cantidad];
      return ($materiales_cantidad);
    }// if(count($materiales)==0)
    else{
      //Materiales aprobación completa
      $materiales=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')
      ->where('registro_oficio_id',$idOficio)
      ->select('control_oficio_materiales.nombre as nombre','control_oficio_solicitud_material.cantidad as cantidad')
      ->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
      $nombres_materiales="";
      foreach($materiales as $material)
      $nombres_materiales=$nombres_materiales.$material->nombre.", ";
      $nombres_materiales =  trim($nombres_materiales, ', ');
      $cantidad=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')
      ->where('registro_oficio_id',$idOficio)->sum('cantidad');
      $materiales_cantidad=['materiales'=>$nombres_materiales,'cantidad'=>$cantidad];
      return ($materiales_cantidad);
    }// else if($tipoEstadoid==2)
  }//public function buscarSolicitudesDonativosAprobadas(Request $request)

  ////////////////////////Módulo listado donaciones pdf

  public function buscarOficiosPorFiltros($request){
    $includes=[
      'usuario_creador','personal_natural',
      'personal_juridico','motivo_solicitud',
      'estado','despacho.detalle',
      'solicitud_materiales.material','aprobacion_parcial_materiales.material',
      'despacho.chofer','despacho.responsable','despacho.vehiculo',
      'estado_aprobacion'
    ];
    $tipoSolicitante="Natural";
    $oficios=Control_oficio_registro_oficio::query();
    $oficios->where('motivo_solicitud_id','3');
    $oficios->with($includes);
    if(isset($request->fechaInicio) && isset($request->fechaFin)){
      $oficios->whereBetween('fecha_emision',[$request->fechaInicio,$request->fechaFin]);
      // $oficios->whereIn('control_oficio_registro_oficio.id', function($oficios) use($request){
      //   $oficios->select('id')
      //   ->from('control_oficio_registro_oficio')
      //   ->whereBetween('fecha_emision',[$request->fechaInicio,$request->fechaFin]);
      // });
    }//Intervalo de fechas
    if(isset($request->nombreSolicitante)){
      $oficios->whereHas('personal_natural', function ($query) use($request) {
        $query->where('nombre', 'ilike', '%' . $request->nombreSolicitante . '%')
        ->orWhere('apellido', 'ilike', '%' . $request->nombreSolicitante . '%');
      });
    }//
    if(isset($request->estado_aprobacion_id)){
      $oficios->where('estado_aprobacion_id',$request->estado_aprobacion_id);
    }//isset estado aprobacion id
    if(isset($request->tipoSolicitante) && $request->tipoSolicitante!=0){
      //Si es != 0, es que selecciono personal natural,juridico o trabajador.
      if($request->tipoSolicitante==1){
        //Personal Natural
        $oficios->whereHas('personal_natural', function ($query)  {
          $query->where('trabajador_vtelca', false);
        });
        $oficios->where('personal_juridico_id',null);
      }else if($request->tipoSolicitante==2){
        //Personal Jurídico
        $tipoSolicitante="Jurídico";
        $oficios->has('personal_juridico');
      }else if($request->tipoSolicitante==3){
        //Personal Trabajador
        $tipoSolicitante="Trabajador";
        $oficios->whereHas('personal_natural', function ($query)  {
          $query->where('trabajador_vtelca', true);
        });
        // $oficios->where('personal_juridico_id',null);//Esto por si quieren que no venga personal juridico de los trabajadores de vtelca. Ya sea por un error de configuración de responsables de una empresa, agreguen un trabajador de vtelca.
      }//tipoSOlicitante==3
    }//tipoSolicitante != 0
    $oficios->orderBy('id','ASC');
    $oficios=$oficios->get();
    $oficiosTemp=[];
    if(isset($request->materiales_id)){
      if(is_array($request->materiales_id)==false){
        $request->materiales_id=explode(",",$request->materiales_id);
      }
      if(count($request->materiales_id)>0){
        //Recibe arreglo de id's de materiales
        foreach($oficios as $oficio){
          $encontrado=0;
          if(count($oficio->aprobacion_parcial_materiales)>0){
            foreach($oficio->aprobacion_parcial_materiales as $material){
              foreach($request->materiales_id as $materialRequest){
                if($material->materiales_id==(int)$materialRequest){
                  $oficiosTemp[]=$oficio;
                  $encontrado=1;
                  break;
                }//if
              }//foreach materiales
              if($encontrado==1){
                break;
              }
            }//material
          }else if(count($oficio->solicitud_materiales)>0){
            $encontrado=0;
            foreach($oficio->solicitud_materiales as $material){
              foreach($request->materiales_id as $materialRequest){
                if($material->materiales_id==(int)$materialRequest){
                  $oficiosTemp[]=$oficio;
                  $encontrado=1;
                  break;
                }//if
              }//foreach materiales
              if($encontrado==1){
                break;
              }
            }//material
          }//else if solicitud materiales
        }//oficios
        // $oficios->whereHas('aprobacion_parcial_materiales', function ($query) use($materiales_ids) {
        //   $query->whereIn('materiales_id', $materiales_ids);
        // })->orwhereHas('solicitud_materiales', function ($query) use($materiales_ids) {
        //   $query->whereIn('materiales_id', $materiales_ids);
        // });
      }//count>0
      else
      $oficiosTemp=$oficios;
    }//materiales_id
    else
    $oficiosTemp=$oficios;
    return $oficiosTemp;
  }//buscarOficiosPorFiltros()

  public function BuscarDatosDespachoPorFechaOld(Request $request){
    //Usado en listad ode donaciones por despachar
    $oficios=$this->BuscarDatosDespacho($request->estatus_oficio,$request->fecha_despacho,$request->fechaDespachoFin);
    return response()->json(['error'=>0,'oficios'=>$oficios]);
  }//

  public function BuscarDatosDespachoPorFecha(Request $request){
    $oficios=$this->buscarOficiosPorFiltros($request);
    $datosTabla=$this->transformadorOficiosTabla($oficios);//Aplica conteo de oficios aprobados/rechazados/despachados/verificadas/procesadas
    $materialesSeleccionados=$request->materiales_id;
    $nombres_materiales="";
    if($materialesSeleccionados){
      $materiales=Control_oficio_materiales::find($materialesSeleccionados);//Obtiene los materiales seleccionados por id.
      foreach($materiales as $material){
        $nombres_materiales=$nombres_materiales.$material->nombre.", ";
      }//foreach
      $nombres_materiales =  trim($nombres_materiales, ', ');
    }//materiales
    else{
      $nombres_materiales="Todos";
    }
    $tipoSolicitante="Todos";
    if(isset($request->tipoSolicitante) && $request->tipoSolicitante!=0){
      //Si es != 0, es que selecciono personal natural,juridico o trabajador.
      if($request->tipoSolicitante==1){
        //Personal Jurídico
        $tipoSolicitante="Natural";
      }else if($request->tipoSolicitante==2){
        //Personal Jurídico
        $tipoSolicitante="Jurídico";
      }else if($request->tipoSolicitante==3){
        //Personal Trabajador
        $tipoSolicitante="Trabajador";
      }//tipoSOlicitante==3
    }//tipoSolicitante != 0
    if(count($oficios)==0)
    return response()->json([
      'error'=>1,
      'msg'=>'No se encontradon registros asociados a los filtros seleccionados.',
      'oficios'=>$oficios,
      'contenidoTabla'=>$datosTabla,
      'tipoSolicitante'=>$tipoSolicitante,
      'ValortipoSolicitante'=>$request->tipoSolicitante,
      'fechasConsultadas'=>[
        'inicio'=>$request->fechaInicio,
        'fin'=>$request->fechaFin
      ],
      'materialesConsultados'=>$materialesSeleccionados,
      'nombresMaterialesConsultados'=>$nombres_materiales,
      'nombreSolicitante'=>$request->nombreSolicitante
    ]);
    else
    return response()->json([
      'error'=>0,
      'oficios'=>$oficios,
      'contenidoTabla'=>$datosTabla,
      'tipoSolicitante'=>$tipoSolicitante,
      'ValortipoSolicitante'=>$request->tipoSolicitante,
      'fechasConsultadas'=>[
        'inicio'=>$request->fechaInicio,
        'fin'=>$request->fechaFin
      ],
      'materialesConsultados'=>$materialesSeleccionados,
      'nombresMaterialesConsultados'=>$nombres_materiales,
      'nombreSolicitante'=>$request->nombreSolicitante
    ]);
  }//public function BuscarDatosDespachoPorFecha(Request $request);

  public function transformadorOficiosTabla($oficios){
    $array=[];
    foreach($oficios as $oficio){
      $b=0;
      // $cantidad_total=0;
      // if($oficio->aprobacion_parcial_materiales!=null && count($oficio->aprobacion_parcial_materiales)>0){
      //   foreach($oficio->aprobacion_parcial_materiales as $material){
      //     $cantidad_total=$cantidad_total+(int)$material->cantidad;
      //   }//foreach
      // }else{
      //   foreach($oficio->solicitud_materiales as $material){
      //     $cantidad_total=$cantidad_total+(int)$material->cantidad;
      //   }//foreach
      // }//else
      foreach($array as &$ar){
        if($ar['id']==$oficio->estado_aprobacion_id){
          $b=1;
          // $ar['cantidadTotal']=(int)$ar['cantidadTotal']+(int)$cantidad_total;
          $ar['cantidadTotal']=(int)$ar['cantidadTotal']+(int)1;
        }//exist in array
      }//foreach $array estados aprobacion
      if($b==0){
        $array[]=[
          'id'=>$oficio->estado_aprobacion_id,
          'nombre'=>$oficio->estado_aprobacion->nombre,
          'cantidadTotal'=>1
          // 'cantidadTotal'=>$cantidad_total
        ];
      }
    }//oficios
    return $array;
  }//transformadorOficiosTabla()

  public function pdfIndividual(Request $request){
    $oficios=$this->buscarOficiosPorFiltros($request);
    if(count( $oficios)==0)
    return redirect()->back()->with('error','No hay oficios que mostrar');
    $pdf = PDF::loadView('controlOficios.GestionSolicitudesDonativosAprobadas.Reporte.listado_donaciones_individual',
    [
      "titulo"=>"Reporte de Material de Donación",
      "oficios"=>$oficios,
      'fechaInicio'=>(\Carbon\Carbon::parse($request->fechaInicio))->format('d-m-Y'),
      'fechaFin'=>(\Carbon\Carbon::parse($request->fechaFin))->format('d-m-Y')
    ]
  );
  $mytime = \Carbon\Carbon::now();
  return $pdf->download($mytime->toDateTimeString().' Reporte de Salida Material de Donacion.pdf');
}//public function pdfIndividual(Request $request)
public function pdfGlobal(Request $request){
  $oficios=$this->buscarOficiosPorFiltros($request);
  if(count( $oficios)==0)
  return redirect()->back()->with('error','No hay oficios que mostrar');

  $pdf = PDF::loadView('controlOficios.GestionSolicitudesDonativosAprobadas.Reporte.listado_donaciones_individual',
  [
    "titulo"=>"Reporte de Material de Donación",
    "oficios"=>$oficios,
    'fechaInicio'=>(\Carbon\Carbon::parse($request->fechaInicio))->format('d-m-Y'),
    'fechaFin'=>(\Carbon\Carbon::parse($request->fechaFin))->format('d-m-Y')
  ]);
  $mytime = \Carbon\Carbon::now();
  return $pdf->download($mytime->toDateTimeString().' Reporte de Salida Material de Donacion.pdf');

}//public function pdfGlobal(Request $request)

public function pdf(Request $request){
  $fechaDespacho=json_decode($request->fechaDespacho);
  $fechaDespachoFin=json_decode($request->fechaDespachoFin);
  if($fechaDespacho=="")
  $oficios=$this->BuscarDatosDespachov2(0,'','');
  else
  $oficios=$this->BuscarDatosDespachov2(0,$fechaDespacho,$fechaDespachoFin);
  if(count( $oficios)==0)
  return response()->json(['status'=>'error','mensaje'=>'No hay información para la generación del archivo pdf']);

  $pdf = PDF::loadView('controlOficios.GestionSolicitudesDonativosAprobadas.Reporte.oficios',
  [
    "titulo"=>"Reporte de Salida Material de Donación",
    "oficio"=>$oficios,
  ]);
  $mytime = \Carbon\Carbon::now();
  return $pdf->download($mytime->toDateTimeString().' Reporte de Salida Material de Donacion.pdf');

}//public function pdf(Request $request)

////////Módulo Reporte listado donaciones

public function listadoDonacionesDespachar(){
  $oficios=$this->BuscarDatosDespacho(2,\Carbon\Carbon::now(),\Carbon\Carbon::now());
  return view('controlOficios.GestionSolicitudesDonativosAprobadas.listadoDonacionesADespachar')->with(['oficios'=>json_encode($oficios)]);
}// public function listadoDoanacionesDespachar()

public function transformadorOficios($oficios){
  $array=[];

  foreach($oficios as $oficio){
    $materialesDB=[];
    $materiales=[];
    $nombres_materiales="";
    $cantidad_solicitada=0;
    $condicional_material=0;

    if(count($oficio->aprobacion_parcial_materiales)!=0)
    $materiales=$oficio->aprobacion_parcial_materiales;//Aprobación parcial
    else
    $materiales=$oficio->solicitud_materiales;//Aprobación completa

    foreach($materiales as $material){
      if (str_slug($material->material->nombre) == "madera" || str_slug($material->material->nombre) == "maderas") {
        //Si tiene el material madera/maderas
        $condicional_material =1;
      }
      $materialesDB[]=[
        'id'=>$material->materiales_id,
        'nombre'=>$material->material->nombre,
        'slug'=>str_slug($material->material->nombre),
        'cantidad_solicitada'=>$material->cantidad,
        'despachado'=>$material->despachado
      ];
    }//materiales
    $materialesDB=json_decode(json_encode($materialesDB));
    foreach($materialesDB as $material){
      $nombres_materiales=$nombres_materiales.$material->nombre.", ";
      $cantidad_solicitada+=$material->cantidad_solicitada;
    }//foreach materialesDB
    $nombres_materiales =  trim($nombres_materiales, ', ');
    //new - Validación para módulo listado de donaciones por verificar, si falta al menos uno por despachar, no podrá verificarlo.
    $verificar=true;
    if(count($materialesDB)>0){
      foreach($materialesDB as $material){
        if(!$material->despachado)
        $verificar=false;
      }//foreach material
    }
    //new
    $array[]=[
      'id'=>$oficio->id,
      'responsable'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
      'cedula'=>$oficio->personal_natural->cedula,
      'materiales'=>$materialesDB,
      'nombres_materiales'=>$nombres_materiales,
      'cantidad_solicitada'=>$cantidad_solicitada,
      'condicional_material'=>$condicional_material,
      'chofer'=>$oficio->despacho->chofer,
      'verificar'=>$verificar
    ];
  }//
  return $array;
}//transformadorOficios

public function listadoDonacionesVerificar(){
  //Trae todos los oficios de tipo donativos con estado en proceso de verificación
  $oficios=Control_oficio_registro_oficio::where('motivo_solicitud_id',3)
  ->where('estado_aprobacion_id',9)
  ->with('despacho.chofer')
  ->whereHas('despacho', function ($query) {
    // $query->where('fecha_despacho',\Carbon\Carbon::now());
  })->get();
  $oficios=$this->transformadorOficios($oficios);
  return view('controlOficios.GestionSolicitudesDonativosAprobadas.listadoDonacionesVerificar')->with(['oficios'=>json_encode($oficios)]);
}//listadoDonacionesVerificar()

public function buscarOficiosPorVerificar(Request $request){
  $oficios=Control_oficio_registro_oficio::where('motivo_solicitud_id',3)
  ->where('estado_aprobacion_id',9)
  ->with('despacho.chofer')
  ->whereHas('despacho', function ($query) use($request) {
    $query->whereBetween('fecha_despacho',[$request->fecha_inicio,$request->fecha_fin]);
  })->get();
  $oficios=$this->transformadorOficios($oficios);
  return response()->json(['error'=>0,'oficios'=>$oficios]);
}//buscarOficiosPorVerificar()

public function listadoDonacionesVerificarPost(Request $request){
  try {
    DB::beginTransaction();
    $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
    $oficio->estado_aprobacion_id=8;//Procesado / Verificado por PCP
    $oficio->update();
    DB::commit();
    $oficios=Control_oficio_registro_oficio::where('motivo_solicitud_id',3)->where('estado_aprobacion_id',9)->where('fecha_emision',\Carbon\Carbon::now())->get();
    $oficios=$this->transformadorOficios($oficios);
    return response()->json(['error'=>0,'msg'=>'Oficio verificado correctamente.','oficios'=>$oficios]);
  } catch (\Exception $e) {
    DB::rollBack();
    return response()->json(['error'=>1,'msg'=>'Ha ocurrido un error en el sistema.','msgSystem'=>$e->getMessage()]);
  }
}//listadoDonacionesVerificarPost()
public function buscarMaterialesAsociados(Request $request){
  $idOficio=$request->oficio_id;
  $materiales=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')
  ->where('registro_oficio_id',$idOficio)->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
  if(count($materiales)==0)
  $materiales=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')->where('registro_oficio_id',$idOficio)->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
  return response()->json(['materiales'=>$materiales]);
}//public function buscarMaterialesAsociados()

public function entregarMaterial(Request $request){
  //Recibe oficio_id y material_id
  try {
    DB::beginTransaction();
    $idOficio=$request->registro_oficio_id;
    $idMaterial=$request->id;
    $aprobacion_completa=0;
    $material=Control_oficio_solicitud_material_aprobacion_parcial::where('registro_oficio_id',$idOficio)
    ->where('materiales_id',$idMaterial)
    ->first();
    if(count($material)==0){
      //Si no consigue nada en aprobación parcial, es porque fue una aprobación completa (Tabla solicitudes_material)
      $material=Control_oficio_solicitud_material::where('registro_oficio_id',$idOficio)
      ->where('materiales_id',$idMaterial)
      ->first();
      $aprobacion_completa=1;
    }
    $material->despachado =true;
    $material->update();
    DB::commit();
    $materialesDespachados=[];
    $materiales=[];
    $b=0;
    if($aprobacion_completa==1){
      $materiales=Control_oficio_solicitud_material::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_solicitud_material.materiales_id')
      ->where('registro_oficio_id',$idOficio)->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
      $materialesDespachados=Control_oficio_solicitud_material::where('registro_oficio_id',$idOficio)->where('despachado',false)->get();
    }else{
      $materiales=Control_oficio_solicitud_material_aprobacion_parcial::join('control_oficio_materiales','control_oficio_materiales.id','control_oficio_material_aprobacion_parcial.materiales_id')
      ->where('registro_oficio_id',$idOficio)->orderBy('control_oficio_materiales.nombre')->getQuery()->get();
      $materialesDespachados=Control_oficio_solicitud_material_aprobacion_parcial::where('registro_oficio_id',$idOficio)->where('despachado',false)->get();
    }
    if(count($materialesDespachados)==0){
      $Control_oficio_registro_oficio=Control_oficio_registro_oficio::where('id',$idOficio)->first();
      $Control_oficio_registro_oficio->estado_aprobacion_id=9;//Cambia a en proceso de verificación por PCP
      $Control_oficio_registro_oficio->update();
      $b=1;
    }//if(count($materiales)==0)
    if(isset($request->fecha_inicio) && $request->fecha_inicio!=null  && isset($request->fecha_fin) && $request->fecha_fin!=null)
    $oficios=$this->BuscarDatosDespacho(2,$request->fecha_inicio,$request->fecha_fin);
    else
    $oficios=$this->BuscarDatosDespacho(2,\Carbon\Carbon::now(),\Carbon\Carbon::now());
    return response()->json(['error'=>0,'msg'=>'Material despachado correctamente.','materiales'=>$materiales,'oficios'=>$oficios,'despacho_completo'=>$b,'fecha_inicio'=>$request->fecha_inicio,'fecha_fin'=>$request->fecha_fin]);
  } catch (\Exception $e) {
    DB::rollBack();
    return response()->json(['error'=>1,'msg'=>'Ha ocurrido un error en el sistema.','msgSystem'=>$e->getMessage()]);
  }
}//public function entregarMaterial(Request $request)

}//class motivoSolicitud extends Controller
