<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Departamentos;
use DB;
use Validator;

class departamentosController extends Controller
{
    public function mostrarDepartamentos(){
        $Departamentos=Departamentos::orderBy('nombre')->get();
        return view('controlOficios.gestionDepartamentos')->with('Departamentos',$Departamentos);
    }//public function mostrarDepartamentos(Request $request)

    public function registrarDepartamentos(Request $request){

        try {
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'nombre' => 'required|min:3|max:200',
            ],[
               'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
               'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
               'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 200 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }
            $Departamento=Departamentos::where('nombre',$request->nombre)->first();
            if(count($Departamento)){
                return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo nombre']);
            }//if(count($Departamento))
            // $create=Departamentos::create($request->all());
            $dep=new Departamentos();
            $dep->nombre=$request->nombre;
            $dep->descripcion=$request->descripcion;
            $dep->save();
            DB::commit();
            $Departamentos=Departamentos::orderBy('nombre')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'Departamentos'=>$Departamentos]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
        }//catch()




    }//public function registrarDepartamentos(Request $request)

    public function modificarDepartamentos(Request $request){
        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required',
                'nombre' => 'required|min:3|max:50',
            ],[
               'id.required'=>'El campo id es requerido',
               'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
               'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
               'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
            ]);
            if ($validator->fails()) {
               return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
            }
            $Departamento=Departamentos::where('id','<>',$request->id)->where('nombre',$request->nombre)->first();
            $Departamentos=Departamentos::orderBy('nombre')->get();
            $error=array('No se puede duplicar el campo nombre.');
            if(count($Departamento)!=0){
                return response()->json(['status'=>'error','mensaje'=>$error]);
            }// if(count($Departamento)!=0)
            $Departamento=Departamentos::where('id',$request->id)->first();
            $Departamento->nombre=$request->nombre;
            $Departamento->descripcion=$request->descripcion;
            $Departamento->update();
            DB::commit();
            $Departamentos=Departamentos::orderBy('nombre')->get();
            return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'Departamentos'=>$Departamentos]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }

    }//public function modificarDepartamentos(Request $request, $id)

    public function eliminarDepartamentos(Request $request){
        try{
            DB::beginTransaction();
            $validator=Validator::make($request->all(), [
                'id' => 'required'
            ],[
                'id.required'=>'El campo id es requerido',] );
            if ($validator->fails()) {
              return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
            }

            $Departamento=Departamentos::where('id',$request->id)->first();
            if (count($Departamento)>0 ) {
                $Departamento->delete();
            } else {
              return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
            }
            DB::commit();
            $Departamentos=Departamentos::orderBy('nombre')->get();
            return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'Departamentos'=>$Departamentos]);
          }catch (Exception $e){
            DB::rollBack();
            return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
          }

    }//public function eliminarDepartamentos(Request $request, $id)
}//class departamentosController extends Controller
