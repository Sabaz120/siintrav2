<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_observaciones_generales as observaciones_generales;
use DB;
class ObservacionesCondicionesController extends Controller
{
    public function index(){
      $observaciones=observaciones_generales::withTrashed()->orderBy('deleted_at','DESC')->get();
      return view('controlOficios.ObservacionesGenerales.index',['Observaciones'=>$observaciones]);
    }//

    public function store(Request $request){
      try {
        DB::beginTransaction();
        //Verificar si la posición seleccionada la tiene otra observación.
        $posicionExiste=observaciones_generales::withTrashed()->where('posicion',(int)$request->posicion)->first();
        //Count
        $count=observaciones_generales::withTrashed()->count();
        $count=$count+1;
        //Crear observación
        $observacion=observaciones_generales::create($request->all());
        //Si existe una observación con la posición seleccionada, será cambiada por la ultima ṕosición generada.
        if($posicionExiste){
          $posicionExiste->posicion=$count;
          $posicionExiste->update();
        }
        DB::commit();
        $Observaciones=observaciones_generales::withTrashed()->orderBy('deleted_at','DESC')->get();
        return response()->json(['error'=>0,'msg'=>'Registro satisfactorio.','Observaciones'=>$Observaciones]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>1,'msg'=>'Ha ocurrido un error en el servidor.','msgSystem'=>$e->getMessage()]);
      }//catch
    }//store()

    public function update(Request $request){
      try {
        DB::beginTransaction();
        $observacion=observaciones_generales::find($request->id);
        $posicionVieja=$observacion->posicion;
        //Verificar si la posición seleccionada la tiene otra observación.
        $posicionExiste=observaciones_generales::withTrashed()->where('posicion',$request->posicion)->first();
        //Actualizar observación
        $observacion->posicion=$request->posicion;
        $observacion->descripcion=$request->descripcion;
        $observacion->update();
        //Si existe una observación con la posición seleccionada, será cambiada por la ultima ṕosición generada.
        if($posicionExiste){
          $posicionExiste->posicion=$posicionVieja;
          $posicionExiste->update();
        }
        DB::commit();
        $Observaciones=observaciones_generales::withTrashed()->orderBy('deleted_at','DESC')->get();
        return response()->json(['error'=>0,'msg'=>'Actualización satisfactoria.','Observaciones'=>$Observaciones]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>1,'msg'=>'Ha ocurrido un error en el servidor.','msgSystem'=>$e->getMessage()]);
      }//catch
    }//update

    public function destroy(Request $request){
      try {
        DB::beginTransaction();
        $observacion=observaciones_generales::find($request->id);
        $observacion->delete();
        DB::commit();
        $Observaciones=observaciones_generales::withTrashed()->orderBy('deleted_at','DESC')->get();
        return response()->json(['error'=>0,'msg'=>'Deshabilitado satisfactoriamente.','Observaciones'=>$Observaciones]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>1,'msg'=>'Ha ocurrido un error en el servidor.','msgSystem'=>$e->getMessage()]);
      }//catch
    }//
    public function restore(Request $request){
      try {
        DB::beginTransaction();
        $observacion=observaciones_generales::withTrashed()->where('id',$request->id)->first();
        $observacion->restore();
        DB::commit();
        $Observaciones=observaciones_generales::withTrashed()->orderBy('deleted_at','DESC')->get();
        return response()->json(['error'=>0,'msg'=>'Restaurado satisfactoriamente.','Observaciones'=>$Observaciones]);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['error'=>1,'msg'=>'Ha ocurrido un error en el servidor.','msgSystem'=>$e->getMessage()]);
      }//catch
    }//
}
