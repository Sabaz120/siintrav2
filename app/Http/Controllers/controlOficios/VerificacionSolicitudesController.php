<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Territorios\Territorios_estado;
use App\Modelos\ControlOficios\MotivoSolicitud;
use App\Modelos\ControlOficios\ClasificacionEnte;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\SistemaProduccionV1\ExistenciaAlmacenDespacho;
// use App\Modelos\Estados;
use App\Modelos\SistemaProduccionV1\Productos;
use App\Modelos\ControlOficios\Control_oficio_productos_aprobacion_parcial;
class VerificacionSolicitudesController extends Controller
{
  public $productos=[];
  public function obtenerOficios(){
    return Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
      $q->withTrashed();
    }])->with(['motivo_solicitud'=>function($q){
      $q->withTrashed();
    }])->with('detalle_evento','estado','solicitud_productos','solicitud_productos.producto','personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente')
    ->where('estado_aprobacion_id',1)
    ->where('motivo_solicitud_id',1)
    ->orderBy('created_at','ASC')->get();
  }//obtenerOficios()

  public function obtenerDisponibilidadProducto(Request $request){
    //Sin filtro de modalidad
    $Producto=Productos::where('id',$request->producto_id)->first();
    if(!$Producto)
    return response()->json(['error'=>1,'msg'=>'Modelo de equipo no encontrado para consultar su disponibilidad.'],404);
    $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$Producto->nombre)->where('idAlmacen',4)->sum('cantidadEnvia');
    if($disponibilidad>0){
      $disponibilidad=$disponibilidad-obtenerComprometido($request->producto_id);
    }//disponibilidad
    return response()->json(['disponibilidad'=>$disponibilidad]);
  }//obtenerDisponibilidadProducto()
  public function obtenerModalidadesYDisponibilidadProducto(Request $request){
    //Sin filtro de modalidad
    $Producto=Productos::where('id',$request->producto_id)->first();
    if(!$Producto)
    return response()->json(['error'=>1,'msg'=>'Modelo de equipo no encontrado para consultar su disponibilidad.'],404);
    $modalidades=$this->obtenerModalidadProductoPhp($request->producto_id);
    return response()->json(['modalidades'=>$modalidades,'modalidad_id'=>$modalidades[0]['modalidad'],'disponibilidad'=>$modalidades[0]['disponibilidad']]);
  }//obtenerDisponibilidadProducto()

  public function obtenerDisponibilidadProductoPhp($producto_id){
    $Producto=Productos::where('id',$producto_id)->first();
    $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$Producto->nombre)->where('idAlmacen',4)->sum('cantidadEnvia');
    if($disponibilidad>0){
      //Valida contra cantidad comprometida (oficios ya aprobados),
      $oficios=Control_oficio_registro_oficio::with('solicitud_productos','aprobacion_parcial_productos')->whereIn('estado_aprobacion_id',[2,3])->get();
      foreach($oficios as $oficio){
        if($oficio->estado_aprobacion_id==2){
          foreach($oficio->solicitud_productos as $producto){
            if((int)$producto->producto_id==(int)$producto_id){
              $disponibilidad=(int)$disponibilidad-(int)$producto->cantidad;
            }//
          }//productos
        }else if($oficio->estado_aprobacion_id==3){
          foreach($oficio->aprobacion_parcial_productos as $producto){
            if((int)$producto->producto_id==(int)$producto_id){
              $disponibilidad=(int)$disponibilidad-(int)$producto->cantidad;
            }//
          }//productos
        }//else if
      }//oficios
    }//
    return $disponibilidad;
  }//obtenerDisponibilidadProducto()
  public function obtenerModalidadProductoPhp($producto_id){
    $Producto=Productos::where('id',$producto_id)->first();
    // $modalidades=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$Producto->nombre)->select('modalidad')->distinct()->get();
    $modalidades=ExistenciaAlmacenDespacho::where('modeloPhone',$Producto->nombre)->select('modalidad')->distinct()->get();
    // if($producto_id==40)
    // dd($Producto,$modalidades);
    $arr=[];
    foreach($modalidades as $modalidad){
      $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$Producto->nombre)->where('modalidad',$modalidad->modalidad)->where('idAlmacen',4)->sum('cantidadEnvia');
      if($disponibilidad>0){
        $disponibilidad=$disponibilidad-obtenerComprometido($producto_id);
      }//Si disponibilidad > 0
      $arr[]=['modalidad'=>$modalidad->modalidad,'disponibilidad'=>$disponibilidad];
    }//foreach modalidades
    // dd($modalidades);
    // return $modalidades;
    return $arr;
  }//obtenerDisponibilidadProducto()
  public function transformadorOficios($oficios){
    $oficiosPersonalNatural=[];
    $oficiosPersonalJuridico=[];
    $oficiosPersonalTrabajador = [];
    // dd($oficios);
    //////////////////////////Inicio carga modalidad / disponibilidad
    $productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
    foreach($productos as $producto){
      $arreglo=[];
      $modalidades=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->select('modalidad')->distinct()->get();
      foreach($modalidades as $modalidad){
        $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$producto->nombre)->where('modalidad',$modalidad->modalidad)->where('idAlmacen',4)->sum('cantidadEnvia');
        if($disponibilidad>0){
          $disponibilidad=$disponibilidad-obtenerComprometido($producto->id,$modalidad->modalidad);
        }//Si disponibilidad > 0
        $arreglo[]=['modalidad'=>$modalidad->modalidad,'disponibilidad'=>$disponibilidad];
      }
      $this->productos[str_slug($producto->nombre)]=$arreglo;
    }//productos
    ////////////////////////////////Fin carga modalidad / disponibilidad
    foreach($oficios as $oficio){
      $cantidad=0;
      $icon_class="fa fa-user";
      $bg_class="bg-info";
      $motivos_solicitud=$oficio->motivo_solicitud->nombre;
      if($motivos_solicitud=="Jornada de Venta de Telefonos"){
        $bg_class="bg-info";
        $icon_class="fa fa-shopping-cart";
      }else if($motivos_solicitud=="Donativos"){
        $bg_class="bg-danger";
        $icon_class="fa fa-envelope";
      }else if($motivos_solicitud=="Invitación a Eventos"){
        $bg_class="bg-success";
        $icon_class="fa fa-gift";
      }else if($motivos_solicitud=="Intercambio de Bienes y Servicios"){
        $bg_class="bg-primary";
        $icon_class="fa fa-handshake-o";
      }
      if($oficio->solicitud_productos==null || count($oficio->solicitud_productos)==0)
        $cantidad=0;
      else{
        foreach($oficio->solicitud_productos as $producto){
          $cantidad=$cantidad+$producto->cantidad;
          // $producto['disponibilidad']=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$producto->producto->nombre)->sum('cantidadEnvia');
          // $producto['disponibilidad']=$this->obtenerDisponibilidadProductoPhp($producto->producto_id);
          $producto['modalidades']=$this->productos[str_slug($producto->producto->nombre)];
          // $producto['modalidades']=$this->obtenerModalidadProductoPhp($producto->producto_id);
          if(count($producto['modalidades'])==0){
            $producto['modalidad_id']='No disponible.';
            $producto['disponibilidad']=0;
          }else{
            // dd($producto['modalidades'],$producto->producto_id,$oficio->solicitud_productos);
            $producto['modalidad_id']=$producto['modalidades'][0]['modalidad'];
            $producto['disponibilidad']=$producto['modalidades'][0]['disponibilidad'];
          }//else si tiene modalidades disponibles
          // dd($producto);
        }//foreach productos
      }//else
      if($cantidad>0){
        if($oficio->personal_juridico==null){
          //Es persona natural

          $oficiosPersonalNatural=array_merge($oficiosPersonalNatural,[
            [
              'fecha_registro'=>$oficio->created_at->format('d-m-Y'),
              'fecha_recepcion'=>$oficio->fecha_recepcion,
              'solicitante'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
              'cantidad'=>$cantidad,
              'detalle_productos'=>$oficio->solicitud_productos,
              'estado'=>$oficio->estado->estado,
              'ver_detalle'=>0,
              'oficio_id'=>$oficio->id,
              'aprobacion_parcial'=>0,
              'motivo_solicitud_id'=>$oficio->motivo_solicitud_id,
              'estado_id'=>$oficio->estado_id,
              'atencion_estado'=>Control_oficio_registro_oficio::where('estado_id',$oficio->estado_id)->where('estado_aprobacion_id',8)->count(),
              'icon_class'=>$icon_class,
              'bg_class'=>$bg_class,
              'num_atencion'=>Control_oficio_registro_oficio::where('personal_natural_id',$oficio->personal_natural->id)->where('estado_aprobacion_id',8)->count()
            ]
          ]);

        }else{
          //Es persona juridica
          $oficiosPersonalJuridico=array_merge($oficiosPersonalJuridico,[
            [
              'fecha_registro'=>$oficio->created_at->format('d-m-Y'),
              'fecha_recepcion'=>$oficio->fecha_recepcion,
              'solicitante'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
              'ente'=>$oficio->personal_juridico->ente->nombre,
              'cantidad'=>$cantidad,
              'detalle_productos'=>$oficio->solicitud_productos,
              'estado'=>$oficio->estado->estado,
              'empresa' => $oficio->personal_juridico->nombre_institucion,
              'ver_detalle'=>0,
              'oficio_id'=>$oficio->id,
              'aprobacion_parcial'=>0,
              'motivo_solicitud_id'=>$oficio->motivo_solicitud_id,
              'estado_id'=>$oficio->estado_id,
              'ente_id'=>$oficio->personal_juridico->ente->id,
              'icon_class'=>$icon_class,
              'bg_class'=>$bg_class,
              'agente_retencion'=>$oficio->personal_juridico->agente_retencion,
              'verificacion_pago'=>$oficio->verificacion_pago,
              'atencion_estado'=>Control_oficio_registro_oficio::where('estado_id',$oficio->estado_id)->where('estado_aprobacion_id',8)->count(),
              'num_atencion'=>Control_oficio_registro_oficio::where('personal_natural_id',$oficio->personal_natural->id)->where('estado_aprobacion_id',8)->count()
            ]
          ]);
        }
      }//if posee productos asociados
    }//foreach oficios
    return ['personalNatural'=>$oficiosPersonalNatural,'personalJuridico'=>$oficiosPersonalJuridico, 'personalTrabajador' => $oficiosPersonalTrabajador];
  }//transformadorOficios
  public function index(){
    // dd(Control_oficio_productos_aprobacion_parcial::all());
    $estados=Territorios_estado::all();
    $motivos_solicitud=MotivoSolicitud::all();
    $clasificacion_entes=ClasificacionEnte::all();
    $oficios=json_encode($this->transformadorOficios($this->obtenerOficios()));

    $productosArr=[];
    $productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();

    foreach($productos as $producto){
      // $disponibilidad=ExistenciaAlmacenDespacho::where('despachado',0)->where('modeloPhone',$producto->nombre)->sum('cantidadEnvia');
      // if($disponibilidad && $disponibilidad>0)
      $productosArr[]=$producto;
    }//productos
    $productosArr=json_encode($productosArr);

    return view('controlOficios.verificacionSolicitudes.index',['productos'=>$productosArr,'estados'=>$estados,'motivos_solicitud'=>$motivos_solicitud,'clasificacion_entes'=>$clasificacion_entes,'oficios'=>$oficios]);
  }//index()

  public function agruparProductosDuplicados($productos){
    $arreglo=[];
    foreach($productos as $producto){
      $b=0;
      foreach($arreglo as &$arr){
        if($arr['producto_id']==$producto->producto_id && $arr['modalidad_id']==$producto->modalidad_id){
          $arr['cantidad']=$arr['cantidad']+$producto->cantidad;
          $b=1;
          break;
        }//if array
      }//foreach arreglo
      if($b==0){
        $arreglo[]=[
          'producto_id'=>$producto->producto_id,
          // 'producto_nombre'=>$producto->producto->nombre,
          // 'producto_descripcion'=>$producto->producto->descripcion,
          'modalidad_id'=>$producto->modalidad_id,
          'cantidad'=>$producto->cantidad,
          // 'descripcion'=>$producto->producto->descripcion
        ];
      }
    }//productos
    return json_decode(json_encode($arreglo));
  }//validarProductosDuplicados()

  public function aprobarOficio(Request $request){
    $oficio=json_decode(json_encode($request->oficio));
    $aprobacion_parcial=false;
    $productos=[];
    $productos=$this->agruparProductosDuplicados($oficio->detalle_productos);
    //Validación disponibilidad de cada producto
    // foreach($productos as $producto){
    //   $disponibilidad=$this->obtenerDisponibilidadProductoPhp($producto->producto_id);
    //   if($producto->cantidad>$disponibilidad){
    //     return response()->json(['error'=>1,'msg'=>'No puedes aprobar este oficio, el producto: '.$producto->descripcion.' no tiene la suficiente cantidad disponible en almacen.','oficios'=>$this->transformadorOficios($this->obtenerOficios())]);
    //   }//Cantidad solicitada es > a disponibilidad.
    // }//foreach validacion productos
    //Validación para saber si fue una aprobación total o parcial.
    $OficioOriginal=Control_oficio_registro_oficio::find($oficio->oficio_id);//Oficio original
    //En vista del último cambio realizado 18/02/2019, que ahora se agrego el campo modalidad, siempre será aprobación parcial.
    $aprobacion_parcial=true;
    // if(count($OficioOriginal->solicitud_productos)!=count($productos)){
    //   //Si la cantidad aprobada tiene mas o menos productos, es una aprobación parcial.
    //   $aprobacion_parcial=true;
    // }else{
    //   //Si tienen la misma cantidad de productos aprobada,
    //   //toca validar si hay diferencias en la cantidad aprobada de cada producto.
    //   foreach($OficioOriginal->solicitud_productos as $producto_solicitud){
    //     $b=0;//Cambia a 1 si consigue uno con Diferente cantidad y seria una aprobacion parcial
    //     $encontrado=0;//Si no lo consigue, quedara en 0 y por lo tanto sería una aprobación parcial.
    //     foreach($productos as $producto){
    //       if((int)$producto->producto_id==(int)$producto_solicitud->producto_id){
    //         $encontrado=1;
    //         //Si consigue este material en la solicitud
    //         if((int)$producto->cantidad!=(int)$producto_solicitud->cantidad){
    //           //Si la cantidad aprobada es diferente a la solicitada.
    //           $b=1;
    //           break;
    //         }//if
    //       }//encuentra
    //     }//foreach materiales aprobados
    //     if($b==1 || $encontrado==0){
    //       $aprobacion_parcial=true;
    //       break;
    //     }//if
    //   }//foreach material inicial hecho en la solicitud
    // }//else (count($OficioOriginal->solicitud_productos)==count($productos))
    // dd($productos);
    if($aprobacion_parcial){
      //Aprobación parcial
      foreach($productos as $producto){
        $aprobacion=new Control_oficio_productos_aprobacion_parcial();
        $aprobacion->control_oficio_registro_oficio_id=$oficio->oficio_id;
        $aprobacion->producto_id=$producto->producto_id;
        // $aprobacion->producto_nombre=$producto->producto_nombre;
        // $aprobacion->producto_descripcion=$producto->producto_descripcion;
        $aprobacion->modalidad=$producto->modalidad_id;
        $aprobacion->cantidad=$producto->cantidad;
        $aprobacion->save();
      }//productos
      $estado_aprobacion=3;//Aprobado parcialmente
    }else{
      //Aprobación total
      foreach($OficioOriginal->solicitud_productos as $producto){
        $disponibilidad=$this->obtenerDisponibilidadProductoPhp($producto->producto_id);
        if($producto->cantidad>$disponibilidad){
          return response()->json(['error'=>1,'msg'=>'No puedes aprobar el oficio totalmente, el producto: '.$producto->producto->descripcion.' no tiene la suficiente cantidad disponible en almacen.','oficios'=>$this->transformadorOficios($this->obtenerOficios())]);
        }//
      }//foreach
      $estado_aprobacion=2;//Aprobado totalmente
    }//else
    $oficio=Control_oficio_registro_oficio::find($oficio->oficio_id);
    $oficio->estado_aprobacion_id=$estado_aprobacion;
    $oficio->fecha_estado = now();
    if(isset($request->verificacion))
    $oficio->verificacion_pago=$request->verificacion;
    else if(isset($request->oficio['verificacion_pago']))
    $oficio->verificacion_pago=$request->oficio['verificacion_pago'];
    $oficio->plan_venta=$request->plan_venta;
    $oficio->update();
    // $oficios=$this->transformadorOficios($this->obtenerOficios());
    $oficios=[];
    return response()->json(['error'=>0,'oficios'=>$oficios]);
  }//aprobarOficio
  // public function aprobarOficioOLd(Request $request){
  //   $oficio=json_decode(json_encode($request->oficio));
  //   return response()->json(['oficio'=>$oficio]);
  //   $estado_aprobacion=1;
  //   $productos=[];
  //   if($oficio->aprobacion_parcial==1){
  //     //Es aprobación parcial
  //     $productos=$this->agruparProductosDuplicados($oficio->detalle_productos);
  //     //////Validar disponibilidad
  //     foreach($productos as $producto){
  //       $disponibilidad=$this->obtenerDisponibilidadProductoPhp($producto->producto_id);
  //       if($producto->cantidad>$disponibilidad){
  //         return response()->json(['error'=>1,'msg'=>'No puedes aprobar este oficio, el producto: '.$producto->descripcion.' no tiene la suficiente cantidad disponible en almacen.','oficios'=>$this->transformadorOficios($this->obtenerOficios())]);
  //       }
  //     }//foreach validacion productos
  //     /////
  //     foreach($productos as $producto){
  //       $aprobacion=new Control_oficio_productos_aprobacion_parcial();
  //       $aprobacion->control_oficio_registro_oficio_id=$oficio->oficio_id;
  //       $aprobacion->producto_id=$producto->producto_id;
  //       $aprobacion->cantidad=$producto->cantidad;
  //       $aprobacion->save();
  //     }//productos
  //     $estado_aprobacion=3;//Aprobado parcialmente
  //   }else{
  //     //Es aprobación total
  //     $oficioP=Control_oficio_registro_oficio::find($oficio->oficio_id);
  //     foreach($oficioP->solicitud_productos as $producto){
  //       $disponibilidad=$this->obtenerDisponibilidadProductoPhp($producto->producto_id);
  //       if($producto->cantidad>$disponibilidad){
  //         return response()->json(['error'=>1,'msg'=>'No puedes aprobar el oficio totalmente, el producto: '.$producto->producto->descripcion.' no tiene la suficiente cantidad disponible en almacen.','oficios'=>$this->transformadorOficios($this->obtenerOficios())]);
  //       }
  //     }
  //     $estado_aprobacion=2;//Aprobado totalmente
  //   }
  //   $oficio=Control_oficio_registro_oficio::find($oficio->oficio_id);
  //   $oficio->estado_aprobacion_id=$estado_aprobacion;
  //   $oficio->fecha_estado = now();
  //   $oficio->update();
  //   $oficios=$this->transformadorOficios($this->obtenerOficios());
  //   return response()->json(['error'=>0,'oficios'=>$oficios]);
  // }//aprobarOficio

  public function rechazarOficio(Request $request){
    $oficio=json_decode(json_encode($request->oficio));
    $o=Control_oficio_registro_oficio::find($oficio->oficio_id);
    $o->estado_aprobacion_id=4;//Rechazado
    $o->update();
    $oficios=$this->transformadorOficios($this->obtenerOficios());
    return response()->json(['error'=>0,'oficios'=>$oficios]);
  }//rechazarOficio()

  public function obtenerOficiosConFiltro(Request $request){
    //Filtros de búsqueda en módulo verificación de solicitudes de jornada de teléfonos;
    //Método construido 21 febrero 2019 - sabas vega
    try {
      $query=Control_oficio_registro_oficio::query();
      $query->where('estado_aprobacion_id',1);
      $query->where('motivo_solicitud_id',1);//Jornada de tlf
      $query->orderBy('id', 'asc');//DEl mas nuevo al mas viejo.
      $query->with(['personal_natural'=>function($q){
        $q->withTrashed();
      }]);//With trashed natural
      $query->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }]);//With trashed motivo solicitud
      if(isset($request->filtros)){
        $filtros=json_decode(json_encode($request->filtros));
        //Filtro por nombre de Solicitante
        if(isset($filtros->solicitante) && $filtros->tipoOficio=='natural'){
          $query->whereHas('personal_natural', function ($query) use($filtros) {
            $query->where('nombre', 'ilike', '%' . $filtros->solicitante . '%')
            ->orWhere('apellido', 'ilike', '%' . $filtros->solicitante . '%');
          });
        }//Nombre solicitante
        //Filtros de fecha
        if(isset($filtros->fecha_inicio) && isset($filtros->fecha_fin))
          $query->whereBetween('created_at',[$filtros->fecha_inicio.' 00:00:00',$filtros->fecha_fin.' 23:59:59']);
        //Filtros de estado
        // dd($filtros);
        if(isset($filtros->estado_id))
        $query->where('estado_id',$filtros->estado_id);
        //Filtros tipo oficio natural o juridico
        if($filtros->tipoOficio=='natural')
        $query->where('personal_juridico_id',null);
        else
        $query->where('personal_juridico_id','!=',null);
        //Filtro por ente_id
        if(isset($filtros->ente_id) && $filtros->tipoOficio=='juridico'){
          $query->whereHas('personal_juridico', function ($query) use($filtros) {
            $query->where('ente_id', $filtros->ente_id);
          });
        }//ente_id
      }//filtros query
      $oficios=$query->get();
      $oficiosTemp=collect();
      if(isset($request->filtros['num_atenciones']) && $request->filtros['tipoOficio']=='natural'){
        $num_atenciones=$request->filtros['num_atenciones'];
        foreach($oficios as $oficio){
          $t=0;//Si cumple con las condiciones.
          $oficioValid=Control_oficio_registro_oficio::query();
          $oficioValid->where('estado_aprobacion_id',8)->where('personal_natural_id',$oficio->personal_natural_id);
          if(isset($request->filtros->fecha_inicio) && isset($request->filtros->fecha_fin))
          $query->whereBetween('created_at',[$request->filtros->fecha_inicio.' 00:00:00',$request->filtros->fecha_fin.' 23:59:59']);
          $oficioValid=$oficioValid->count();
          if($num_atenciones=="0"){
            //Ningún número de atención
            if($oficioValid==0)
            $t=1;
          }
          if($num_atenciones=="1y5"){
            //Mayor a 0 y Menor o igual a 5 números de atención, es decir entre 1 y 5 números de atención
            if($oficioValid>0 && $oficioValid<=5)
            $t=1;
          }
          if($num_atenciones=="M5"){
            //Mayor a 5 números de atención
            if($oficioValid>5)
              $t=1;
          }
          if($t==1){
            $oficiosTemp->push($oficio);
          }
        }//oficios
        $oficios=$oficiosTemp;
      }//filtros
      $oficios=json_decode(json_encode($this->transformadorOficios($oficios)));
      return response()->json(['error'=>0,'oficios'=>$oficios]);
    } catch (\Exception $e) {
      return response()->json(['error'=>1,'msg'=>'Error en el servidor','msgSystem'=>$e->getMessage()]);
    }
  }//obtenerOficiosConFiltro()

  public function obtenerOficiosConFiltro2(Request $request){
    //Recibe $motivos_solicitud,$estado_id,$ente_id
    $oficios=json_decode(json_encode($this->transformadorOficios($this->obtenerOficios())));
    $oficiosTmp=['personalNatural'=>[],'personalJuridico'=>[]];
    if($request->motivo_solicitud_id!=0 || $request->estado_id!=0 || $request->ente_id!=0){
      foreach($oficios->personalNatural as $oficio){
        if($request->motivo_solicitud_id!=0 && $request->estado_id!=0){
          if($oficio->motivo_solicitud_id==$request->motivo_solicitud_id && $oficio->estado_id==$request->estado_id)
          $oficiosTmp['personalNatural']=array_merge($oficiosTmp['personalNatural'],[$oficio]);
        }else if($request->motivo_solicitud_id!=0)
        if($oficio->motivo_solicitud_id==$request->motivo_solicitud_id){
          $oficiosTmp['personalNatural']=array_merge($oficiosTmp['personalNatural'],[$oficio]);
        }else if($request->estado_id!=0){
          if($request->estado_id==$oficio->estado_id)
          $oficiosTmp['personalNatural']=array_merge($oficiosTmp['personalNatural'],[$oficio]);
        }//estado_id!=0
      }//oficios personal natural
      foreach($oficios->personalJuridico as $oficio){
        if($request->motivo_solicitud_id!=0 && $request->estado_id!=0 && $request->ente_id!=0){
          if($oficio->motivo_solicitud_id==$request->motivo_solicitud_id && $oficio->estado_id==$request->estado_id && $oficio->ente_id==$request->ente_id)
          $oficiosTmp['personalJuridico']=array_merge($oficiosTmp['personalJuridico'],[$oficio]);
        }else if($request->motivo_solicitud_id!=0 && $request->ente_id!=0){
          if($oficio->motivo_solicitud_id==$request->motivo_solicitud_id && $oficio->ente_id==$request->ente_id)
          $oficiosTmp['personalJuridico']=array_merge($oficiosTmp['personalJuridico'],[$oficio]);
        }else if($request->motivo_solicitud_id!=0 && $request->estado_id!=0){
          if($oficio->motivo_solicitud_id==$request->motivo_solicitud_id && $oficio->estado_id==$request->estado_id)
          $oficiosTmp['personalJuridico']=array_merge($oficiosTmp['personalJuridico'],[$oficio]);
        }else if($request->motivo_solicitud_id!=0){
          if($oficio->motivo_solicitud_id==$request->motivo_solicitud_id)
          $oficiosTmp['personalJuridico']=array_merge($oficiosTmp['personalJuridico'],[$oficio]);
        }else if($request->estado_id!=0){
          if($request->estado_id==$oficio->estado_id)
          $oficiosTmp['personalJuridico']=array_merge($oficiosTmp['personalJuridico'],[$oficio]);
        }else if($request->ente_id!=0){
          if($request->ente_id==$oficio->ente_id)
          $oficiosTmp['personalJuridico']=array_merge($oficiosTmp['personalJuridico'],[$oficio]);
        }//estado_id!=0
      }//oficios personal natural
      $oficios=$oficiosTmp;
    }
    return response()->json(['error'=>0,'oficios'=>$oficios]);
  }//obtenerOficiosConFiltro()
}
