<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Ente;
use App\Modelos\ControlOficios\ClasificacionEnte;//
use DB;
use Validator;
class GestionEnteController extends Controller
{
  public function mostrarEntes(){
    $Ente=Ente::orderBy('nombre')->get();
    $ClasificacionEnte=ClasificacionEnte::orderBy('nombre')->get();
    return view('controlOficios.gestionEntes',array('Entes'=>$Ente,'ClasificacionEnte'=>$ClasificacionEnte));
  }//public function mostrarEntes()

  public function registrarEnte(Request $request){
    try {
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'clasificacion_ente_id' => 'required',
        'nombre' => 'required|min:3|max:50',
        'ponderacion' => 'required',
      ],[
        'clasificacion_ente_id.required'=>'Existen campos por llenar:<br>*Clasificación de Ente.',
        'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
        'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'ponderacion.required'=>'Existen campos por llenar:<br>*Nivel de Atención.',
      ]);
      if ($validator->fails()) {
        return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
      }
      $Ente=Ente::where('clasificacion_ente_id',$request->clasificacion_ente_id)->where('nombre',$request->nombre)->first();
      if(count($Ente)){
        return response()->json(['status'=>'error','mensaje'=>'No se pueden duplicar el campo nombre para una misma clasificacion']);
      }//if(count($Ente))
      $create=Ente::create($request->all());
      DB::commit();
      $Ente=Ente::orderBy('nombre')->get();
      return response()->json(['status'=>'success','mensaje'=>'Registro Satisfactorio.', 'Entes'=>$Ente]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>'Ocurrió un error: '.$e->getMessage()]);
    }//catch()




  }//public function registrarEnte(Request $request)

  public function modificarEnte(Request $request){

    try{
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'id' => 'required',
        'clasificacion_ente_id' => 'required',
        'nombre' => 'required|min:3|max:50',
        'ponderacion' => 'required',
      ],[
        'id.required'=>'El campo id es requerido',
        'clasificacion_ente_id.required'=>'Existen campos por llenar:<br>*Clasificación de Ente.',
        'nombre.required'=>'Existen campos por llenar:<br>*Nombre.',
        'nombre.min'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'nombre.max'=>'El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.',
        'ponderacion.required'=>'Existen campos por llenar:<br>*Nivel de Atención.',
      ]);
      if ($validator->fails()) {
        return response()->json(['status'=>'error','mensaje'=>$validator->errors()]);
      }
      $Ente=Ente::where('clasificacion_ente_id',$request->clasificacion_ente_id)->where('nombre',$request->nombre)->first();
      $error=array('No se puede duplicar el campo nombre para una misma clasificación de ente.');
      if(count($Ente)!=0 && $Ente->clasificacion_ente_id!=$request->clasificacion_ente_id){
        return response()->json(['status'=>'error','mensaje'=>$error]);
      }// if(count($Ente)!=0)
      $Ente=Ente::where('id',$request->id)->first();
      $Ente->clasificacion_ente_id=$request->clasificacion_ente_id;
      $Ente->nombre=$request->nombre;
      $Ente->descripcion=$request->descripcion;
      $Ente->ponderacion=$request->ponderacion;
      $Ente->update();
      DB::commit();
      $Ente=Ente::orderBy('nombre')->get();
      return response()->json(['status'=>'success','mensaje'=>'Actualización Satisfactorio.', 'Entes'=>$Ente]);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
    }

  }//public function modificarEnte(Request $request, $id)

  public function eliminarEnte(Request $request){
    try{
      DB::beginTransaction();
      $validator=Validator::make($request->all(), [
        'id' => 'required'
      ],[
        'id.required'=>'El campo id es requerido', ]);
        if ($validator->fails()) {
          return response()->json(['status'=>'error','mensajes'=>$validator->errors()]);
        }

        $Ente=Ente::where('id',$request->id)->first();
        if (count($Ente)>0 ) {
          $Ente->delete();
        } else {
          return response()->json(['status'=>'error','mensaje'=>'El registro que desea eliminar no se encuentra en base de datos']);
        }
        DB::commit();
        $Ente=Ente::orderBy('nombre')->get();
        return response()->json(['status'=>'success','mensaje'=>'Registro eliminado satisfactoriamente', 'Entes'=>$Ente]);
      }catch (Exception $e){
        DB::rollBack();
        return response()->json(['status'=>'error','mensaje'=>$e->getMessage()]);
      }

    }//public function eliminarEnte(Request $request, $id)
  }
