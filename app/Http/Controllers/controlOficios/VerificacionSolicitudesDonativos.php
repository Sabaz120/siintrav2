<?php

namespace App\Http\Controllers\controlOficios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_personal_natural;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material_aprobacion_parcial as aprobacion_parcial;
use App\Modelos\ControlOficios\Control_oficio_materiales;
use Carbon;
use DB;

class VerificacionSolicitudesDonativos extends Controller
{
  function index(){

    $personalNatural = $this->obtenerOficiosPersonalNatural();
    $personalTrabajador = $this->obtenerOficiosPersonalTrabajador();
    $personalJuridico = $this->obtenerOficiosPersonalJuridico();
    $materiales = Control_oficio_materiales::all();

    return view('controlOficios.verificacionSolicitudesDonativos.index', ['personalNatural' => $personalNatural, 'personalTrabajador' => $personalTrabajador, 'personalJuridico' => $personalJuridico, 'materiales' => $materiales]);

  }

  function obtenerOficiosPersonalNatural(){

    $oficiosPersonalNatural = Control_oficio_registro_oficio::join('control_oficio_personal_natural', 'control_oficio_registro_oficio.personal_natural_id', '=', 'control_oficio_personal_natural.id')->where('control_oficio_registro_oficio.motivo_solicitud_id', 3)->where('control_oficio_registro_oficio.estado_aprobacion_id', 1)->where('control_oficio_personal_natural.trabajador_vtelca', false)->where('control_oficio_registro_oficio.personal_juridico_id', null)->select("control_oficio_registro_oficio.id as oficios_id", 'control_oficio_registro_oficio.created_at', 'control_oficio_registro_oficio.personal_natural_id')
    ->with('solicitud_materiales')->has('solicitud_materiales')
    ->orderBy('created_at', 'ASC')
    ->get();

    foreach($oficiosPersonalNatural as $oficios){

      $oficios->nombre = Control_oficio_personal_natural::where('id', $oficios->personal_natural_id)->value('nombre')." ".Control_oficio_personal_natural::where('id', $oficios->personal_natural_id)->value('apellido');
      $oficios->cantidad_materiales = Control_oficio_solicitud_material::where('registro_oficio_id', $oficios->oficios_id)->sum('cantidad');
      $oficios->materiales = Control_oficio_solicitud_material::where('registro_oficio_id', $oficios->oficios_id)->get();
      $oficios->fecha_creacion = $oficios->created_at->format('d-m-Y');

      foreach($oficios->materiales as $material){

        $material->nombre_material = Control_oficio_materiales::where('id', $material->materiales_id)->value('nombre');

      }

    }

    return $oficiosPersonalNatural;

  }

  function obtenerOficiosPersonalTrabajador(){

    $oficiosPersonalTrabajador = Control_oficio_registro_oficio::join('control_oficio_personal_natural', 'control_oficio_registro_oficio.personal_natural_id', '=', 'control_oficio_personal_natural.id')->where('control_oficio_registro_oficio.motivo_solicitud_id', 3)->where('control_oficio_personal_natural.trabajador_vtelca', true)->where('control_oficio_registro_oficio.estado_aprobacion_id', 1)->select("control_oficio_registro_oficio.id as oficios_id", 'control_oficio_registro_oficio.created_at', 'control_oficio_registro_oficio.personal_natural_id')->with('solicitud_materiales')->has('solicitud_materiales')
      ->orderBy('created_at', 'ASC')
      ->get();

    foreach($oficiosPersonalTrabajador as $oficios){

      $oficios->nombre = Control_oficio_personal_natural::where('id', $oficios->personal_natural_id)->value('nombre')." ".Control_oficio_personal_natural::where('id', $oficios->personal_natural_id)->value('apellido');
      $oficios->cedula = Control_oficio_personal_natural::where('id', $oficios->personal_natural_id)->value('cedula');
      $datosPersona=\App\Modelos\Sigesp\sno_personal::datosPersonal($oficios->cedula);
      if($datosPersona){
        $oficios->cargo=$datosPersona[0]->cargo;
        $oficios->gerencia=$datosPersona[0]->area;
      }else{
        $oficios->cargo='Sin cargo';
        $oficios->gerencia='Sin gerencia';
      }
      $oficios->cantidad_materiales = Control_oficio_solicitud_material::where('registro_oficio_id', $oficios->oficios_id)->sum('cantidad');
      $oficios->materiales = Control_oficio_solicitud_material::where('registro_oficio_id', $oficios->oficios_id)->get();
      $oficios->fecha_creacion = $oficios->created_at->format('d-m-Y');

      foreach($oficios->materiales as $material){

        $material->nombre_material = Control_oficio_materiales::where('id', $material->materiales_id)->value('nombre');

      }

    }

    return $oficiosPersonalTrabajador;

  }

  function obtenerOficiosPersonalJuridico(){

    $oficiosPersonalJuridico = Control_oficio_registro_oficio::
    join('control_oficio_personal_natural', 'control_oficio_registro_oficio.personal_natural_id', '=', 'control_oficio_personal_natural.id')
    ->where('control_oficio_registro_oficio.motivo_solicitud_id', 3)
    ->where('control_oficio_registro_oficio.personal_juridico_id', '!=', null)
    ->where('control_oficio_personal_natural.trabajador_vtelca', false)
    ->where('control_oficio_registro_oficio.estado_aprobacion_id', 1)
    ->select("control_oficio_registro_oficio.id as oficios_id", 'control_oficio_registro_oficio.created_at',
    'control_oficio_registro_oficio.personal_juridico_id', 'control_oficio_registro_oficio.personal_natural_id'
    ,'control_oficio_personal_natural.nombre as nombre_responsable','control_oficio_personal_natural.apellido as apellido_responsable')
    ->with('solicitud_materiales')->has('solicitud_materiales')
    ->orderBy('created_at', 'ASC')
    ->get();
    foreach($oficiosPersonalJuridico as $oficios){
      $oficios->nombre = $oficios->nombre_responsable.' '.$oficios->apellido_responsable;
      $oficios->cantidad_materiales = Control_oficio_solicitud_material::where('registro_oficio_id', $oficios->oficios_id)->sum('cantidad');
      $oficios->materiales = Control_oficio_solicitud_material::where('registro_oficio_id', $oficios->oficios_id)->get();
      $oficios->fecha_creacion = $oficios->created_at->format('d-m-Y');
      foreach($oficios->materiales as $material){
        $material->nombre_material = Control_oficio_materiales::where('id', $material->materiales_id)->value('nombre');
      }//foreach
    }//oficios
    return $oficiosPersonalJuridico;
  }//obtenerOficiosPersonalJuridico()

  public function aprobarSolicitudv2(Request $request){
    try {
      DB::beginTransaction();
      // dd('this');
      //Este aprobar es solo para solicitudes de donativos.
      $oficio=json_decode(json_encode($request->oficio));
      // dd($oficio->materiales);
      $oficio_id=$oficio->oficios_id;
      $registro_oficio = Control_oficio_registro_oficio::find($oficio_id);
      if(!$registro_oficio)
      return response()->json(['error' => 1, 'msg'=>'Oficio no encontrado.']);
      // dd($registro_oficio->solicitud_materiales);
      $materialesSinDuplicado=[];
      //Validar materiales duplicados
      foreach($oficio->materiales as $material){
        $b=0;
        if($material->cantidad<=0)
        return response()->json(['error' => 1, 'msg'=>'No puede haber una solicitud de material con cantidad 0']);
        foreach($materialesSinDuplicado as &$m){
          if($m['materiales_id']==$material->materiales_id){
            $m['cantidad']=$m['cantidad']+$material->cantidad;
            $b=1;
            break;
          }//if
        }//foreach
        if($b==0){
          $materialesSinDuplicado[]=[
            'materiales_id'=>$material->materiales_id,
            'cantidad'=>$material->cantidad
          ];
        }
      }//foreach
      $materialesSinDuplicado=json_decode(json_encode($materialesSinDuplicado));
      /////Validar si hubo algún cambio en la solicitud de materiales
      $tipo_aprobacion="completa";
      /*
        -Validar:
        *Sí hay nuevos materiales en la solicitud o si fue cambiado su cantidad.
        *
      */
      if(count($registro_oficio->solicitud_materiales)!=count($materialesSinDuplicado)){
        //Automaticamente es parcial
        $tipo_aprobacion="parcial";
      }else{
        //SI tienen la misma cantidad de materiales, validar que sean los mismos y las cantidades solicitadas por cada uno no sea diferente.
        foreach($registro_oficio->solicitud_materiales as $material_solicitud){
          $b=0;//Cambia a 1 si consigue uno con Diferente cantidad y seria una aprobacion parcial
          $encontrado=0;//Si no lo consigue, quedara en 0 y por lo tanto sería una aprobación parcial.
          foreach($materialesSinDuplicado as $material){
            if((int)$material->materiales_id==(int)$material_solicitud->materiales_id){
              $encontrado=1;
              //Si consigue este material en la solicitud
              if((int)$material->cantidad!=(int)$material_solicitud->cantidad){
                //Si la cantidad aprobada es diferente a la solicitada.
                $b=1;
                break;
              }//if
            }//encuentra
          }//foreach materiales aprobados
          if($b==1 || $encontrado==0){
            $tipo_aprobacion="parcial";
            break;
          }
        }//foreach material inicial hecho en la solicitud
      }//else
      //////////Crear aprobación total o parcial
      if($tipo_aprobacion=="parcial"){
        foreach($materialesSinDuplicado as $material){
          $aprobacion_parcial = new aprobacion_parcial;
          $aprobacion_parcial->registro_oficio_id = $oficio_id;
          $aprobacion_parcial->materiales_id = $material->materiales_id;
          $aprobacion_parcial->cantidad = $material->cantidad;
          $aprobacion_parcial->save();
        }//foreach materiales sin duplicado
        $registro_oficio->estado_aprobacion_id = 3;
        $registro_oficio->fecha_estado = Carbon\Carbon::now();
        $registro_oficio->update();
      }else{
        //Aprobación completa.
        $registro_oficio->estado_aprobacion_id = 2;
        $registro_oficio->fecha_estado = Carbon\Carbon::now();
        $registro_oficio->update();
      }
      DB::commit();
      $personalNatural = $this->obtenerOficiosPersonalNatural();
      $personalTrabajador = $this->obtenerOficiosPersonalTrabajador();
      $personalJuridico = $this->obtenerOficiosPersonalJuridico();
      return response()->json(['error' => 0, 'personalNatural' => $personalNatural, 'personalTrabajador' => $personalTrabajador, 'personalJuridico' => $personalJuridico]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error' => 1, 'msg' => 'Ocurrió un error en el servidor.','msgSystem'=>$e->getMessage()]);
    }
  }//aprobarSolicitudv2()

  function aprobarSolicitud(Request $request){
    $oficio_id = $request->oficio['oficios_id'];
    try {
      DB::beginTransaction();
      if($request->aprobacion == "parcial"){
        $materiales = $request->oficio['materiales'];
        $materialesSinDuplicado=[];
        //Validar materiales duplicados
        foreach($materiales as $material){
          $b=0;
          if((int)$material['cantidad']<=0)
            return response()->json(['error' => 1, 'msg'=>'No puede haber una solicitud de material con cantidad 0 o menor.']);
          foreach($materialesSinDuplicado as &$m){
            if($m['materiales_id']==$material['materiales_id']){
              $m['cantidad']=$m['cantidad']+$material['cantidad'];
              $b=1;
              break;
            }//if
          }//foreach
          if($b==0){
            $materialesSinDuplicado[]=[
              'materiales_id'=>$material['materiales_id'],
              'cantidad'=>$material['cantidad']
            ];//material sin duplicado
          }//$b==0
        }//foreach
        // $materialesSinDuplicado=json_decode(json_encode($materialesSinDuplicado));
        //Validar materiales duplicados
        $materialesSinDuplicado=json_decode(json_encode($materialesSinDuplicado));
        foreach($materialesSinDuplicado as $material){
          $aprobacion_parcial = new aprobacion_parcial;
          $aprobacion_parcial->registro_oficio_id = $oficio_id;
          $aprobacion_parcial->materiales_id = $material->materiales_id;
          $aprobacion_parcial->cantidad = $material->cantidad;
          $aprobacion_parcial->save();
        }//foreach materiales sin duplicado
        $registro_oficio = Control_oficio_registro_oficio::find($oficio_id);
        $registro_oficio->estado_aprobacion_id = 3;
        $registro_oficio->fecha_estado = Carbon\Carbon::now();
        $registro_oficio->update();
      }else{
        //Aprobación completa
        $registro_oficio = Control_oficio_registro_oficio::find($oficio_id);
        $registro_oficio->estado_aprobacion_id = 2;
        $registro_oficio->fecha_estado = Carbon\Carbon::now();
        $registro_oficio->update();
      }
      DB::commit();
      $personalNatural = $this->obtenerOficiosPersonalNatural();
      $personalTrabajador = $this->obtenerOficiosPersonalTrabajador();
      $personalJuridico = $this->obtenerOficiosPersonalJuridico();
      return response()->json(['error' => 0, 'personalNatural' => $personalNatural, 'personalTrabajador' => $personalTrabajador, 'personalJuridico' => $personalJuridico]);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error' => 1, 'msg' => 'Ocurrió un error en el servidor.','msgSystem'=>$e->getMessage()]);
    }
  }//aprobarSolicitud()

  function rechazarSolicitud(Request $request){

    $registro_oficio = Control_oficio_registro_oficio::find($request->id);
    $registro_oficio->estado_aprobacion_id = 4;
    $registro_oficio->fecha_estado = Carbon\Carbon::now();
    $registro_oficio->update();

    return response()->json(true);

  }

  function eliminarMaterial(Request $request){

    try {

      DB::beginTransaction();
      Control_oficio_solicitud_material::find($request->id)->delete();
      DB::commit();

      return response()->json(['error' => 0, 'msg' => "Material eliminado"]);

    }catch (\Exception $e) {

      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>"Problema de conexión con el servidor"]);
    }

  }

}
