<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\SolicitudDetal;
use App\Modelos\ControlOficios\DetalleSolicitudDetal;
use DB;
use App\Modelos\SistemaProduccionV1\Clientes;
use App\Modelos\ControlOficios\Control_oficio_personal_natural;
use App\Modelos\SistemaProduccionV1\AptOrderCompra;
use App\Modelos\SistemaProduccionV1\AptOrderCompraDet;
use Auth;
class SolicitudDetalController extends Controller
{
    public function store(Request $request){
      try {
        DB::beginTransaction();
        $lastRequest=SolicitudDetal::latest()->first();
        $numRequest=1;
        if($lastRequest){
          $numRequest=$lastRequest->id;
        }
        $numRequest='S'.str_pad($numRequest.date('Y'),7,"0",STR_PAD_LEFT);
        $entity=SolicitudDetal::create([
          'user_id'=>Auth::user()->id,
          'numero_solicitud'=>$numRequest
        ]);
        foreach($request->products as $product){
          DetalleSolicitudDetal::create([
            'producto_id'=>$product['product_id'],
            'cantidad'=>$product['cantidad'],
            'modalidad'=>$product['modalidad'],
            'solicitud_detal_id'=>$entity->id
          ]);
        }//foreach products
        //Sara atienza
        // $pn=Control_oficio_personal_natural::find(223);
        // if($pn){
        //   $nombreCliente=$pn->nombre.' '.$pn->apellido;
        //   $codigoCliente=$pn->cedula;
        //   $telefono=$pn->telefono;
        //   $email=$pn->correo_electronico;
        // }else{
        //   abort(404);
        // }
        // $dataClient=[
        //   "codigoCliente"=>$codigoCliente,
        //   "nombre"=>$nombreCliente,
        //   "representante"=>"COMERCIALIZACION",
        //   "telefonoRepresentante"=>$telefono,
        //   "correoElectronico"=>$email,
        //   "direccionCliente"=>"----",
        //   "estatus"=>1
        // ];
        // $cliente=Clientes::where("codigoCliente",$codigoCliente)->first();
        // if(!$cliente){
        //   $cliente=Clientes::create($dataClient);
        // }
        $cliente=Clientes::where("id",4)->first();
        if(!$cliente)
        abort(404);

        $concepto="Solicitud de agente autorizado";
        foreach($request->products as $product){
          $concepto=$concepto." ".$product['cantidad']." equipos de ".$product['modelo'];
        }

        $ordenCompra=AptOrderCompra::create([
          "codigo_orden_compra"=>$numRequest,
          "fecha_registro"=>$entity->created_at,
          "fecha_estimada_salida"=>$entity->created_at->addDays(7),
          "concepto"=>$concepto,
          "moneda_id"=>3,
          "cliente_id"=>$cliente->id,
          "estado_od_id"=>1,
          "sistema_oficio"=>2
        ]);
        foreach($request->products as $product){
          AptOrderCompraDet::create([
            "producto_id"=>$product['nombre'],
            "color_id"=>"Blanco",
            "cantidad_telefonos"=>$product['cantidad'],
            "costo_unitario"=>0,
            "descuento"=>0,
            "modalidad"=>$product['modalidad'],
            "recargo"=>0,
            "seed_stock"=>2,
            "unidad_medida_id"=>1,
            "orden_compra_pt_id"=>$ordenCompra->id,
          ]);
        }

        DB::commit();
        return response()->json(['msg'=>"Solicitud generada correctamente bajo el número: ".$numRequest],200);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['msg'=>'Ocurrió un error en el servidor.','msgSystem'=>$e->getMessage(),'lineError'=>$e->getLine(),'traceError'=>$e->getTrace()],500);

      }



    }//
}
