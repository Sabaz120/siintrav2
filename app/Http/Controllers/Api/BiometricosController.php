<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TADPHP\TAD;
use TADPHP\TADFactory;
use Carbon\Carbon;
use App\Modelos\Sigesp\sno_personal;
use App\Modelos\GestionSocial\EntregaBeneficios;
use App\Modelos\GestionSocial\Trabajador;
class BiometricosController extends Controller
{
  /*
  -Config biometric
  https://www.youtube.com/watch?v=Gi5j1dtvqLI
  -Documentation:
  https://github.com/cobisja/tad-php

  */
  public $personal=[
    [
      "cedula"=>20257577,
      "nombres"=>"García Leon, Hugo Jose",
      "area"=>"Presidencia" //
    ],
    [
      "cedula"=>13662103,
      "nombres"=>"Diaz Rosales Dalia del Valle",
      "area"=>"Departamento de Mantenimiento y Conservación"
    ],
    [
      "cedula"=>16987055,
      "nombres"=>"De la Hoz Maldonado, Teddy Junior",
      "area"=>"Operaciones internas y externas-oficina central de procesamiento"
    ],
    [
      "cedula"=>15097262,
      "nombres"=>"Garcia Romero, Almari",
      "area"=>"Departamento de costos e inventario"
    ],
    [
      "cedula"=>15980146,
      "nombres"=>"Sanchez Leal, Neywin Maria",
      "area"=>"Asuntos públicos"
    ],
    [
      "cedula"=>18449117,
      "nombres"=>"Gomez Molina, Juan Manuel",
      "area"=>"Operaciones internas y externas-oficina central de procesamiento"
    ],
    [
      "cedula"=>19442781,
      "nombres"=>"Cordero Diaz, Oriana De los Angeles",
      "area"=>"Asuntos públicos"
    ],
    [
      "cedula"=>20254032,
      "nombres"=>"Castro Ulacio, Eduyin David",
      "area"=>"Oficina central de procesamiento" //
    ],
    [
      "cedula"=>11771453,
      "nombres"=>"Zavala Mahly Zuriely",
      "area"=>"Unidad de mercadeo y comercialización"
    ],
    [
      "cedula"=>26057784,
      "nombres"=>"Rojas Sanchez, Alexander Jose",
      "area"=>"Operaciones internas y externas de la oficina central de procesamiento" //
    ],
    [
      "cedula"=>26813232,
      "nombres"=>"Sevilla Arzaga, Rogelio",
      "area"=>"Operaciones internas-externas"
    ],
    [
      "cedula"=>26986293,
      "nombres"=>"Avila Fernandez, José David",
      "area"=>"Operaciones internas y externas-oficina central de procesamiento"
    ],
    [
      "cedula"=>19647408,
      "nombres"=>"Haryannys Nazaret Querales Amaya",
      "area"=>"Departamento de administracion del trabajo"
    ],
    [
      "cedula"=>16684304,
      "nombres"=>"Atienza Perez, Sara Guadalupe",
      "area"=>"Presidencia" //
    ],
    [
      "cedula"=>16438578,
      "nombres"=>"Gomez Milano, Sol",
      "area"=>"Presidencia"
    ],
    [
      "cedula"=>12138313,
      "nombres"=>"Vivas Leon, Luis Yanoski",
      "area"=>"Prevención y Control de Pérdidas"
    ],
    [
      "cedula"=>17177477,
      "nombres"=>"Medina Medina, Luis Fernando",
      "area"=>"Almacen y Logistica"
    ],
    [
      "cedula"=>20213112,
      "nombres"=>"Aular Leal, Luis Gerardo",
      "area"=>"Presidencia"
    ],
    [
      "cedula"=>18481689,
      "nombres"=>"Romero Lopez, Jhanny Virginia",
      "area"=>"Presidencia"
    ],
    [
      "cedula"=>14028830,
      "nombres"=>"García Romero, Abiezer",
      "area"=>"Organización del Trabajo"
    ],
    [
      "cedula"=>23851465,
      "nombres"=>"Ortega Perez, Oriana Yulianlis",
      "area"=>"Departamento de Mantenimiento y Conservación"
    ],
    [
      "cedula"=>20369179,
      "nombres"=>"Silva Benites, Hanzony Ramon",
      "area"=>"Presidencia"
    ],
    [
      "cedula"=>9808669,
      "nombres"=>"Milano Colina, Sandra María",
      "area"=>"Departamento de Agente Autorizado"
    ],
    [
      "cedula"=>20570726,
      "nombres"=>"Arape Fernandez Darwin Alejandro",
      "area"=>"Gerencia de prevencion y control de perdidas"
    ],
    [
      "cedula"=>11765008,
      "nombres"=>"Davalillo De Martinez, Maria Gabriela ",
      "area"=>"Presidencia"
    ],
    [
      "cedula"=>17518392,
      "nombres"=>"Sierra Penso, Ivan Jose ",
      "area"=>"Consultoria juridica"
    ],
    [
      "cedula"=>13706175,
      "nombres"=>"Martinez Arteaga, Nadia Juledda",
      "area"=>"Gerencia de organización del trabajo"
    ]
  ];
  function establecerFechayHoraActual($ip){
    /* Documentation :
    // Getting current time and date
    $dt = $tad->get_date();

    // Setting device's date to '2014-01-01' (time will be set to now!)
    $response = $tad->set_date(['date'=>'2014-01-01']);

    // Setting device's time to '12:30:15' (date will be set to today!)
    $response = $tad->set_date(['time'=>'12:30:15']);

    // Setting device's date & time
    $response = $tad->set_date(['date'=>'2014-01-01', 'time'=>'12:30:15']);

    // Setting device's date & time to now.
    $response = $tad->set_date();

    */
    $comands = TAD::commands_available();
    $tad = (new TADFactory(['ip'=>$ip, 'com_key'=>0]))->get_instance();
    $response = $tad->set_date();
    return $response;
  }

  function limpiarRegistrosBiometrico($ip){
    /*
    ###Clearing out data from device You can clear information from device according a code you specify.

    Code meaning:

    1: clear all device data, 2: clear all users templates and 3: clear all attendance logs.

    // Delete all attendance logs stored.
    $tad->delete_data(['value'=>3]);
    */
    $comands = TAD::commands_available();
    $tad = (new TADFactory(['ip'=>$ip, 'com_key'=>0]))->get_instance();
    $p=$tad->delete_data(['value'=>3]);
    return $p;
  }

  function reiniciarBiometrico($ip){
    $comands = TAD::commands_available();
    $tad = (new TADFactory(['ip'=>$ip, 'com_key'=>0]))->get_instance();
    $response=$tad->restart();
    return $response;
  }

  function insertarUsuario($ip){
    $comands = TAD::commands_available();
    $tad = (new TADFactory(['ip'=>$ip, 'com_key'=>0]))->get_instance();
    $r = $tad->set_user_info([
      'pin' => 123,
      'name'=> 'Admin Test Bar',
      'privilege'=> 14,
      'password' => 123456
    ]);
    return $r;
  }

  function obtenerMarcas($ip){

    $comands = TAD::commands_available();
    $tad = (new TADFactory(['ip'=>$ip, 'com_key'=>0]))->get_instance();
    $marcas=$tad->get_att_log();
    $array_marcas=$marcas->to_array();
    if(isset($array_marcas['Row'])){
      if(isset($array_marcas['Row']["PIN"]))
      return [$array_marcas['Row']];
      foreach($array_marcas as $datos){
        foreach($datos as $llave => $datos_proceso ){

        }
      }
      return $datos;
    }else
    return [];

  }//OBtener marcas

  public function obtenerPersonalConst($cedula){
    foreach($this->personal as $persona){
      if((int)$persona['cedula']==(int)$cedula)
      return $persona;
    }
    return null;
  }

  public function transformador($data,$sinDuplicados=false,$sinLimite=0){
    $array=collect();
    $data=json_decode(json_encode($data));
    // dd($data);
    foreach($data as $marca){
      $fecha=Carbon::parse($marca->DateTime);
      $persona=sno_personal::datosPersonal($marca->PIN);
      $personal=$this->obtenerPersonalConst($marca->PIN);
      if(count($persona)>0 || $personal!=null){
        $b=1;
        if($sinDuplicados){
          foreach($array as $key){
            if((int)$key['cedula']==(int)$marca->PIN){
              $b=0;
              break;
            }//
          }//foreach array person
        }//sin duplicados
        $nombres=$personal!=null ? $personal['nombres'] : $persona[0]->nombres;
        $area=$personal!=null ? strtolower($personal['area']) : strtolower($persona[0]->area);
        $cargo=$personal!=null ? "" : $persona[0]->cargo;
        if($b==1){
          $array->push([
            'nombres'=>$nombres,
            'nombre'=>$personal ? $personal['nombres'] : $persona[0]->nombre,
            'apellido'=>$personal ? "" : $persona[0]->apellido,
            'area'=>$area,
            'cargo'=>$cargo,
            'cedula'=>$marca->PIN,
            'fecha'=>$fecha->format('d-m-Y'),
            'hora'=>$fecha->format('H:i:s'),
          ]);
        }
      }//!$persona
    }//$marca

    $duplicado=0;
    $nombre_duplicado="";
    $cedula_duplicado="";
    if(count($data)>0){
      //Si tiene al menos una marca.
      $valor=$data[count($data)-1];
      $cedula=$valor->PIN;
      $conteo=0;
      foreach($data as $marca){
        if((int)$cedula==(int)$marca->PIN){
          $conteo++;
          $cedula_duplicado=$cedula;
        }//
      }//foreach array person
      if($conteo>1){
        $duplicado=1;
        foreach($array as $key){
          if((int)$key['cedula']==(int)$cedula)
          $nombre_duplicado=$key['nombres'];
        }
      }
    }
    //Solo enviar las ultimas 4 marcas

    $arrayTemp=[];
    if($sinLimite==1)
      $arrayTemp=$array;
    else{
      if(count($array)<8)
      $arrayTemp=$array;
      else{
        $cont=0;
        $array=$array->sortKeysDesc();
        $array=$array->slice(0, 8);
        foreach($array as $key){
          $arrayTemp[]=$key;
        }
      }
    }
    return ['marcas'=>$arrayTemp,'duplicado'=>$duplicado,'nombre_duplicado'=>$nombre_duplicado,'cedula_duplicado'=>$cedula_duplicado];
  }//transformador

  function obtenerMarcaje($ip){
    $response="";
    $status=200;
    try {
      //192.168.100.208
      //Limpiar registros de X biometrico
      // $marcas=$this->limpiarRegistrosBiometrico($ip);
      // $marcas=$this->establecerFechayHoraActual($ip);
      // $marcas=$this->reiniciarBiometrico($ip);
      // $marcas=$this->insertarUsuario($ip);
      $marcas=$this->obtenerMarcas($ip);
      //dd($marcas);
      // dd($marcas);
      $data=$this->transformador($marcas,true);

      //dd($data);
      $response=["data"=>$data];
    } catch (\Exception $e) {
      $status=500;
      $response=["msg"=>$e->getMessage()];
    }
    return response()->json($response,$status);
  }

  function obtenerMarcajePrueba($ip){
    $response="";
    $status=200;
    try {
      //192.168.100.208
      //Limpiar registros de X biometrico
      // $marcas=$this->limpiarRegistrosBiometrico($ip);
      // $marcas=$this->establecerFechayHoraActual($ip);
      // $marcas=$this->reiniciarBiometrico($ip);
      // $marcas=$this->insertarUsuario($ip);
      $marcas=$this->obtenerMarcas($ip);
      //dd($marcas);
      // dd($marcas);
      $data=$this->transformador($marcas,true);

      dd($marcas);
      $response=["data"=>$data];
    } catch (\Exception $e) {
      $status=500;
      $response=["msg"=>$e->getMessage()];
    }
    return response()->json($response,$status);
  }

  function insertar(Request $request){
    $marcas=$this->obtenerMarcas($request->biometrico_id);
    // $marcas=$this->obtenerMarcas("192.168.100.208");

    $data=$this->transformador($marcas,true,1);
    //$marca['fecha'], $marca['hora']
    foreach($data['marcas'] as $marca){

      $trabajador = Trabajador::firstOrCreate(
        [
          'cedula' => $marca['cedula']
        ], [
          'nombre' => $marca['nombre']!="" ? $marca['nombre'] : $marca['nombres'],
          'apellido' => $marca['apellido']!="" ? $marca['apellido'] : "",
          'cedula' => $marca['cedula'],
          'cargo' => $marca['cargo'],
          'departamento' => $marca['area']
        ]
      );
      $entrega=EntregaBeneficios::updateOrCreate(
        [
          'cedula'=>$marca['cedula'],
          'detalle_jornada_id'=>$request->detalle_jornada_id
        ]
        ,[
          'cedula'=>$marca['cedula'],
          'entregado'=>1,
          'detalle_jornada_id'=>$request->detalle_jornada_id,
          //'marcaje'=>$marca['fecha']." ".$marca['hora']
          'marcaje' => $this->formatearFecha($marca['fecha'])." ".$marca['hora']
        ]);
      }
      return response()->json(['data'=>$data]);
    }//insertar()

    function formatearFecha($fecha){

      $day = substr($fecha, 0, 2);
      $month = substr($fecha, 3, 2);
      $year = substr($fecha, 6, 4);

      $string = $year."-".$month."-".$day;

      return $string;

    }


  }
