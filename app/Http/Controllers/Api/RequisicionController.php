<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Requisicion;
use App\Modelos\ControlOficios\DeduccionesRequisicion;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use PDF;
use App\Modelos\ControlOficios\Control_oficio_despacho as Despachos;
use App\Modelos\ControlOficios\Control_oficio_oferta_economica as OfertaEconomica;
use DB;
use App\Modelos\ControlOficios\Recaudo;
use App\Modelos\ControlOficios\RecaudosEntregados;
use App\Modelos\SistemaProduccionV1\AptOrderCompra;
use App\Modelos\SistemaProduccionV1\AptOrderCompraDet;

use App\Modelos\PremiumSoft\Serialmv;
use App\Modelos\PremiumSoft\SerialmvControlOficio;
use App\Modelos\PremiumSoft\Operti;
use App\Modelos\PremiumSoft\Opermv;
use App\Modelos\SistemaProduccionV1\Clientes;
class RequisicionController extends Controller
{
  public function getData($params=[]){
    /*
    Obtener oficios que tengan cotización generada (Relación oferta_economica de la entidad oficio.)
    -Se debe tener en cuenta 2 cosas.
    -Si requiere verificación de pago (Campo verificacion_pago con valor true en entidad oficio)
    Debe tener todos los pagos validados.
    -Si no requiere verificacion de pago (Campo verificacion_pago con valor false en entidad oficio)
    Debe tener cotización generada (Mediante eloquent con el has de la relación oferta economica ya hace esto automaticamente)
    */
    //Iniciar
    $oficios=Control_oficio_registro_oficio::query();
    //Relaciones
    $relations=['personal_juridico.responsables.personal_natural',
    'oferta_economica.detalle_oferta.producto',
    'oferta_economica.pagos','oferta_economica.requisicion.despacho',
    'oferta_economica.requisicion.recaudos.recaudo',
    'oferta_economica.requisicion.deducciones.deduccion'
    ];
    $oficios->with($relations);

    //Condicional
    $oficios->has('oferta_economica');
    $oficios->whereNotIn('estado_aprobacion_id',[4,8]);//Estado 8 cerrado , Estado 4 rechazado en gestion de solicitudes (oferta economica)999999999999999999999999999999
    //Filtros
    if(isset($params['verificacion_pago'])){
      if($params['verificacion_pago']!=null){
        $oficios->where("verificacion_pago",$params['verificacion_pago']);
      }
    }//verificacion pago
    if(isset($params['fecha_inicio']) && isset($params['fecha_fin'])){
      if($params['fecha_fin']!=null && $params['fecha_inicio']!=null){
        $oficios->whereHas('oferta_economica', function ($query) use($params) {
          $fecha_inicio=$params['fecha_inicio'];
          $fecha_fin=$params['fecha_fin'];
          $raw="date(created_at) BETWEEN '$fecha_inicio' AND '$fecha_fin'";
          $query->whereRaw($raw);
        });
      }
    }else if(isset($params['fecha_fin'])){
      if($params['fecha_fin']!=null){
        $oficios->whereHas('oferta_economica', function ($query) use($params) {
          $query->whereDate('created_at',"<=",$params["fecha_fin"]);
        });
      }
    }else if(isset($params['fecha_inicio'])){
      if($params['fecha_inicio']!=null){
        $oficios->whereHas('oferta_economica', function ($query) use($params) {
          $query->whereDate('created_at',">=",$params["fecha_inicio"]);
        });
      }
    }//fechas
    if(isset($params['num_requisicion'])){
      $oficios->whereHas('oferta_economica', function ($query) use($params) {
        $query->whereHas('requisicion', function ($query) use($params) {
          $query->where('numero_requisicion',$params["num_requisicion"]);
        });
      });
    }
    //Orden
    $oficios->orderBy('created_at','ASC');
    //Obtener
    $oficios=$oficios->get();

    $arreglo=[];
    foreach($oficios as $oficio){
      $b=1;
      if($oficio->verificacion_pago){
        //Requiere validación de pago
        $pagos=$oficio->oferta_economica->pagos;
        foreach($pagos as $pago){
          if($pago->validacion==false || $pago->validacion==0){
            $b=0;
            break;//Tiene un pago pendiente por verificar.
          }//if validacion==false
        }//foreach pagos
      }//Si el oficio requiere verificación de pago
      if($b==1){
        $num_identificacion="";
        $cliente="";
        $agente_retencion=false;
        $personal_juridico=false;
        if($oficio->personal_juridico){
          $personal_juridico=true;
          $num_identificacion=$oficio->personal_juridico->rif_cedula_situr;
          $cliente=$oficio->personal_juridico->nombre_institucion;
          $agente_retencion=$oficio->personal_juridico->agente_retencion;
        }else{
          //Persona natural
          $num_identificacion=$oficio->personal_natural->cedula;
          $cliente=$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido;
        }
        //Cantidad de equipos
        $equipos=$oficio->oferta_economica->detalle_oferta;
        $cantidad_equipos=0;
        $totalCancelar=0;
        foreach($equipos as $equipo){
          $cantidad_equipos+=$equipo->cantidad;
          $totalCancelar+=$equipo->cantidad*$equipo->precio_unitario;
        }
        $despacho=null;
        $num_factura="";
        $num_requisicion=$oficio->oferta_economica->id.date('Y');
        if($oficio->oferta_economica->requisicion){
          $num_requisicion=str_pad($num_requisicion,8,"0",STR_PAD_LEFT);
          $opermvP=DB::connection('premiumsoft')->select(DB::raw("SELECT documento as factura, SUBSTR(desdeimpor, 4) as nota
          FROM opermv WHERE desdeimpor <>'' and desdeimpor like 'NOT%' and SUBSTR(desdeimpor, 4) ='".$num_requisicion."'
          GROUP BY documento,desdeimpor; "));
          if(count($opermvP)>0)
            $num_factura=$opermvP[0]->factura;

          if($oficio->oferta_economica->requisicion->despacho){
            $despacho=[
              'id'=>$oficio->oferta_economica->requisicion->despacho->id,
              'fecha_despacho'=>$oficio->oferta_economica->requisicion->despacho->fecha_despacho,
              'responsable'=>[
                'id'=>$oficio->oferta_economica->requisicion->despacho->responsable->id,
                'nombres_apellidos'=>$oficio->oferta_economica->requisicion->despacho->responsable->nombre.' '.$oficio->oferta_economica->requisicion->despacho->responsable->apellido,
                'cedula'=>$oficio->oferta_economica->requisicion->despacho->responsable->cedula,
                'telefono'=>$oficio->oferta_economica->requisicion->despacho->responsable->telefono,
                'correo_electronico'=>$oficio->oferta_economica->requisicion->despacho->responsable->correo_electronico,
                'firma_personal'=>$oficio->oferta_economica->requisicion->despacho->responsable->firma_personal,
                'codigo_carnet_patria'=>$oficio->oferta_economica->requisicion->despacho->responsable->codigo_carnet_patria
              ],
              'chofer'=>[
                'id'=>$oficio->oferta_economica->requisicion->despacho->chofer->id,
                'nombres'=>$oficio->oferta_economica->requisicion->despacho->chofer->nombre,
                'apellidos'=>$oficio->oferta_economica->requisicion->despacho->chofer->apellido,
                'nombres_apellidos'=>$oficio->oferta_economica->requisicion->despacho->chofer->nombre.' '.$oficio->oferta_economica->requisicion->despacho->chofer->apellido,
                'cedula'=>$oficio->oferta_economica->requisicion->despacho->chofer->cedula,
                'telefono'=>$oficio->oferta_economica->requisicion->despacho->chofer->telefono,
                'correo_electronico'=>$oficio->oferta_economica->requisicion->despacho->chofer->correo_electronico,
                'firma_personal'=>$oficio->oferta_economica->requisicion->despacho->chofer->firma_personal,
                'codigo_carnet_patria'=>$oficio->oferta_economica->requisicion->despacho->chofer->codigo_carnet_patria
              ],
              'vehiculo'=>[
                'id'=>$oficio->oferta_economica->requisicion->despacho->vehiculo->id,
                'placa'=>$oficio->oferta_economica->requisicion->despacho->vehiculo->placa,
                'modelo'=>$oficio->oferta_economica->requisicion->despacho->vehiculo->modelo,
                'descripcion'=>$oficio->oferta_economica->requisicion->despacho->vehiculo->descripcion
              ]
              //'destino'=>$oficio->despacho->detalle->destino,
              //'beneficiario'=>$oficio->despacho->detalle->beneficiario,
              //'created_at'=>$oficio->despacho->created_at->format('d-m-Y')
            ];
          }//if despacho
        }//if requisicion
        ///// NUm requsicionID + EL AÑO EN CURSO
        $recaudos=null;
        if($oficio->oferta_economica->requisicion){
          $recaudos=$oficio->oferta_economica->requisicion->recaudos;
          $num_requisicion=$oficio->oferta_economica->requisicion->numero_requisicion;
        }
        $arreglo[]=[
          'id'=>$oficio->id,
          'oferta_economica_id'=>$oficio->oferta_economica->id,
          'oferta_economica'=>$oficio->oferta_economica,
          'num_cotizacion'=>str_pad($oficio->oferta_economica->id,8,"0",STR_PAD_LEFT),
          'num_requisicion'=>str_pad($num_requisicion,8,"0",STR_PAD_LEFT),
          'num_identificacion'=>$num_identificacion,
          'requisicion'=>$oficio->oferta_economica->requisicion,
          'cliente'=>$cliente,
          'cantidad_equipos'=>$cantidad_equipos,
          'equipos'=>$equipos,
          'despacho'=>$despacho,
          'recaudos'=>$recaudos,
          'agente_retencion'=>$agente_retencion,
          'es_personal_juridico'=>$personal_juridico,
          'personal_juridico'=>$oficio->personal_juridico,
          'personal_natural'=>$oficio->personal_natural,
          'personal_natural_id'=>$oficio->personal_natural_id,
          'totalCancelar'=>number_format($totalCancelar,2,",","."),
          "num_factura"=>$num_factura
        ];
      }//
    }//foreach oficios
    return $arreglo;
  }

  public function index(Request $request){
    try {
      $oficios=$this->getData($request->all());
      return response()->json(['error'=>0,'oficios'=>$oficios]);
    } catch (\Exception $e) {
      return response()->json(['error'=>1,
      'msg'=>'Ocurrió un error en el servidor.',
      'msgSystem'=>$e->getMessage(),
      'lineSystem'=>$e->getLine(),
      'traceSystem'=>$e->getTrace()
    ]);
    }
  }//gestionDespacho

  public function store(Request $request){
    try {
      DB::beginTransaction();
      /*
      Crear requisición
      'numero_requisicion',
      'user_id',
      'despachado',
      'oferta_economica_id',
      'tipo_venta_id'

      */
      $req=Requisicion::firstOrCreate([
        'oferta_economica_id'=>$request->oferta_economica_id,
      ],[
        'user_id'=>\Auth::user()->id,
        'numero_requisicion'=>$request->numero_requisicion,
        'oferta_economica_id'=>$request->oferta_economica_id,
        "seed_stock"=>$request->seedStock,
        'tipo_venta_id'=>$request->tipo_venta_id,
      ]);

      //Crea recaudos al oficio en pivot
      $recaudos=Recaudo::all();
      foreach($recaudos as $recaudo){
        RecaudosEntregados::create([
          "recaudos_id"=>$recaudo->id,
          "requisicion_id"=>$req->id,
          "entregado"=>false
        ]);
      }//foreach recaudos
      $oferta_economica=OfertaEconomica::with('oficio.personal_natural','oficio.personal_juridico')->where('id',$request->oferta_economica_id)->first();
      $codigoCliente="";
      $nombreCliente="";
      $direccion="---";
      if($oferta_economica->oficio->personal_juridico){
        $codigoCliente=$oferta_economica->oficio->personal_juridico->rif_cedula_situr;
        $nombreCliente=$oferta_economica->oficio->personal_juridico->nombre_institucion;
        $direccion=$oferta_economica->oficio->personal_juridico->direccion_fiscal;
        if(!$direccion){
          $direccion="----";
        }
      }else{
        $codigoCliente=$oferta_economica->oficio->personal_natural->cedula;
        $nombreCliente=$oferta_economica->oficio->personal_natural->nombre." ".$oferta_economica->oficio->personal_natural->apellido;
      }//else
      $dataClient=[
        "codigoCliente"=>$codigoCliente,
        "nombre"=>$nombreCliente,
        "representante"=>"COMERCIALIZACION",
        "telefonoRepresentante"=>$oferta_economica->oficio->personal_natural->telefono,
        "correoElectronico"=>$oferta_economica->oficio->personal_natural->correo_electronico,
        "direccionCliente"=>$direccion,
        "estatus"=>1
      ];
      $cliente=Clientes::where("codigoCliente",$codigoCliente)->first();
      if(!$cliente)
        $cliente=Clientes::create($dataClient);
      $equipos=$oferta_economica->detalle_oferta;
      //Inserta en sistema produccion v1
      $concepto="Requisición de ".$req->tipo_venta->nombre;
      foreach($equipos as $equipo)
        $concepto=$concepto." ".$equipo->cantidad." equipos de ".$equipo->producto->descripcion;

      $ordenCompra=AptOrderCompra::create([
        "codigo_orden_compra"=>$request->numero_requisicion,
        "fecha_registro"=>$req->created_at,
        "fecha_estimada_salida"=>$req->created_at->addDays(7),
        "concepto"=>$concepto,
        "moneda_id"=>3,
        "cliente_id"=>$cliente->id,
        "estado_od_id"=>1,
        "sistema_oficio"=>1
      ]);

      if($request->seedStock == true){
        $concepto="Seedstock de {$request->seedStockPorcentaje} % de Requisición de ".$req->tipo_venta->nombre;//Aqui el cambio

        $ordenCompraSeedStock=AptOrderCompra::create([
          "codigo_orden_compra"=>$request->numero_requisicion."SeedSTock",
          "fecha_registro"=>$req->created_at,
          "fecha_estimada_salida"=>$req->created_at->addDays(7),
          "concepto"=>$concepto,
          "moneda_id"=>3,
          "cliente_id"=>$cliente->id,
          // "cliente_id"=>$oferta_economica->control_oficio_registro_oficio_id,
          "estado_od_id"=>1,
          "sistema_oficio"=>1,
        ]);

      }

      foreach($equipos as $equipo){
          AptOrderCompraDet::create([
            "producto_id"=>$equipo->producto->nombre,
            "color_id"=>"Blanco",
            "cantidad_telefonos"=>$equipo->cantidad,
            "descuento"=>0,
            "modalidad"=>$equipo->modalidad,
            "recargo"=>0,
            "seed_stock"=>0,
            "unidad_medida_id"=>1,
            "orden_compra_pt_id"=>$ordenCompra->id,
            "costo_unitario"=>$equipo->precio_unitario,
            "nivel_precio"=>$equipo->nivel_precio,
          ]);

          if($request->seedStock == true){

            $cantidad = $equipo->cantidad*($request->seedStockPorcentaje/100);

            AptOrderCompraDet::create([
              "producto_id"=>$equipo->producto->nombre,
              "color_id"=>"Blanco",
              "cantidad_telefonos"=> $cantidad,
              "descuento"=>0,
              "modalidad"=>$equipo->modalidad,
              "recargo"=>0,
              "seed_stock"=>$request->seedStockPorcentaje,
              "unidad_medida_id"=>1,
              "orden_compra_pt_id"=>$ordenCompraSeedStock->id,
              "costo_unitario"=>$equipo->precio_unitario,
              "nivel_precio"=>$equipo->nivel_precio,
            ]);

          }

        }

      DB::commit();

      $oficios=$this->getData();
      return response()->json(['msg'=>"Requisición generada correctamente.","oficios"=>$oficios],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor.','msgSystem'=>$e->getMessage(),'lineError'=>$e->getLine(),'traceError'=>$e->getTrace()],500);
    }
  }//store()

  public function reportePdf(Request $request){
    // dd($request->all());
    $mytime = \Carbon\Carbon::now();
    $oficio_id=$request->oficio_id;
    $oficio=Control_oficio_registro_oficio::where('id',$oficio_id)
    ->with(['oferta_economica.detalle_oferta.producto','oferta_economica.requisicion', 'personal_juridico'])
    ->has('oferta_economica')
    ->orderBy('created_at','ASC')
    ->first();

    //Cantidad de equipos
    $equipos=$oficio->oferta_economica->detalle_oferta;
    $cantidad_equipos=0;
    foreach($equipos as $equipo)
    $cantidad_equipos+=$equipo->cantidad;
    $arreglo=[];
    $arreglo=[
      'id'=>$oficio->id,
      'oferta_economica_id'=>$oficio->oferta_economica->id,
      'num_cotizacion'=>str_pad($oficio->oferta_economica->id,8,"0",STR_PAD_LEFT),
      'num_requisicion'=>$oficio->oferta_economica->requisicion->numero_requisicion,
      'requisicion'=>$oficio->oferta_economica->requisicion,
      'cantidad_equipos'=>$cantidad_equipos,
      'equipos'=>$equipos
    ];
    $arreglo=json_decode(json_encode($arreglo));
    $pdf = PDF::loadView('controlOficios.GestionDespacho.reporte.pdf.requisicion_despacho',
    [
      "oficio"=>$arreglo,
      'nombre_institucion' => $oficio->personal_juridico->nombre_institucion
    ]);
    // $pdf->setPaper('a4', 'landscape');
    return $pdf->stream($mytime->toDateTimeString().' REPORTE REQUISICIÓN DE DESPACHO.pdf');

  }//generarReporte

  //Almacenar Fecha de despacho
  public function guardarDatosDespacho(Request $request){
    //Recibe: oficio_id, num_factura, fecha_despacho
    try {
      DB::beginTransaction();
      // dd($request->all());
      $despacho=Despachos::where('requisicion_id',$request->requisicion_id)->first();
      $despacho->fecha_despacho=$request->fecha_despacho;//Voltea la fecha ya que la recibe en formato: dd-mm-yyyy
      $despacho->update();
      // $oferta=OfertaEconomica::where('id',$request->oferta_economica_id)->first();
      // $oferta->codigo_factura=$request->num_factura;
      // $oferta->update();
      $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
      $oficio->estado_aprobacion_id=7;//EN proceso de despacho
      $oficio->update();
      DB::commit();
      $oficios=$this->getData();
      return response()->json(['error'=>0,'msg'=>'Plan de despacho almacenado exitosamente.','oficios'=>$oficios],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }//catch()
  }//guardarDatosDespacho()

  public function actualizarDatosDespacho(Request $request){
    try {
      DB::beginTransaction();
      // dd($request->all());
      $despacho=Despachos::where('requisicion_id',$request->requisicion_id)->first();
      $despacho->fecha_despacho=$request->fecha_despacho;//Voltea la fecha ya que la recibe en formato: dd-mm-yyyy
      $despacho->update();
      DB::commit();
      $oficios=$this->getData();
      return response()->json(['error'=>0,'msg'=>'Plan de despacho actualizado exitosamente.','oficios'=>$oficios],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()]);
    }//catch()
  }//actualizarDatosDespacho()

  //Actualizar recaudos
  public function actualizarRecaudos(Request $request){
    try {
      DB::beginTransaction();
      foreach($request->recaudos as $recaudo)
      RecaudosEntregados::where('id',$recaudo['id'])->update(["entregado"=>$recaudo['entregado']]);
      DB::commit();
      $oficios=$this->getData();
      return response()->json(['error'=>0,'msg'=>'Recaudos actualizados exitosamente.','oficios'=>$oficios],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage(),500]);
    }//catch()
  }//guardarDatosDespacho()

  public function asociarDeducciones(Request $request){
    try {
      DB::beginTransaction();
      $requisicion=Requisicion::find($request->requisicion_id);
      DeduccionesRequisicion::where('requisicion_id',$requisicion->id)->delete();
      foreach($request->deducciones as $deduccion){
        DeduccionesRequisicion::create([
          'requisicion_id'=>$requisicion->id,
          'deduccion_id'=>$deduccion
        ]);
      }
      DB::commit();
      $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
      $deducciones=$oficio->oferta_economica->requisicion->deducciones;
      return response()->json(['error'=>0,'msg'=>'Deducciones configuradas exitosamente.','deducciones'=>$deducciones],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['error'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage(),500]);
    }//catch()
  }//asociarDeducciones

  public function correccionRequisicion(Request $request){
    try {
      DB::beginTransaction();
      if(isset($request->num_requisicion)){
        $serialmvco=SerialmvControlOficio::where('documento',$request->num_requisicion)->get();
        if(count($serialmvco)==0)
        throw new \Exception("No existen registros en la tabla SerialmvControlOficio para el número de requisicion ".$request->num_requisicion,500);
        foreach($serialmvco as $serial){
          Serialmv::firstOrCreate([
            "id_empresa"=>$serial->id_empresa,
            "codigo"=>$serial->codigo,
            "serial"=>$serial->serial,
            "almacen"=>$serial->almacen,
            "estatus"=>$serial->estatus,
            "agencia"=>$serial->agencia,
            "tipodoc"=>$serial->tipodoc,
            "documento"=>$serial->documento,
            "sumaresta"=>$serial->sumaresta,
            "doc_id"=>$serial->doc_id,
            "fecha_doc"=>$serial->fecha_doc,
            "destino"=>$serial->destino,
            "cliente"=>$serial->cliente,
          ]);
        }//foreach seriales
        Operti::where('documento',$request->num_requisicion)->update(['seimporto'=>0]);
        Opermv::where('documento',$request->num_requisicion)->update([
          'seimporto'=>0,
          'cntdevuelt'=>0,
        ]);
        return response()->json([
          'status'=>200,
          // 'msg'=>'En construcción ',
          'msg'=>'Corrección de requisición realizada exitosamente'
        ],200);
      }else
      throw new \Exception("Debes enviar el número de requisición",500);
      DB::commit();
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['status'=>500,'msg'=>'Ocurrió un error en el servidor','msgSystem'=>$e->getMessage()],200);
    }

  }//correccionRequisicion


  public function eliminarRequisicion(Request $request){
    try{
      DB::beginTransaction();
      $requisicion=Requisicion::where('numero_requisicion',$request->numero_requisicion)->first();
      if ($requisicion !=null) {
        $aptOrderCompra=AptOrderCompra::where('codigo_orden_compra',$requisicion->numero_requisicion)->first();
        $aptOrderCompraSeed=AptOrderCompra::where('codigo_orden_compra',$requisicion->numero_requisicion."SeedSTock")->first();
        if ($aptOrderCompra !=null) {
          $aptOrderCompraDet=AptOrderCompraDet::where('orden_compra_pt_id',$aptOrderCompra->id)->delete();
          $aptOrderCompra->delete();
        }
        if ($aptOrderCompraSeed !=null) {
          $aptOrderCompraDet=AptOrderCompraDet::where('orden_compra_pt_id',$aptOrderCompraSeed->id)->delete();
          $aptOrderCompraSeed->delete();
        }
        $requisicion->delete();
      }
      DB::commit();
      return response()->json(['error'=>0,'mensaje'=>'Registro eliminado exitosamente.']);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
    }
  }

  public function cierreRequisicion(Request $request){
    try{
      DB::beginTransaction();
      $oficio=Control_oficio_registro_oficio::find($request->oficio_id);
      $oficio->estado_aprobacion_id=8;//Procesado
      $oficio->update();
      $requisicion=Requisicion::find($request->requisicion_id);
      $requisicion->observacion_cierre=$request->observacion;
      $requisicion->update();
      DB::commit();
      return response()->json(['error'=>0,'mensaje'=>'Requisición cerrada exitosamente.']);
    }catch (Exception $e){
      DB::rollBack();
      return response()->json(['error'=>1,'mensaje'=>$e->getMessage()]);
    }
  }
}
