<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\ControlOficios\Control_oficio_personal_natural as pn;
use App\Modelos\ControlOficios\Control_oficio_vehiculo as Vehiculos;
use App\Modelos\ControlOficios\Control_oficio_personal_juridico as pj;
use App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico as responsablesEmpresa;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_detalle_evento as de;
use App\Modelos\ControlOficios\Control_oficio_solicitud_material as solicitud_materiales;
use App\Modelos\ControlOficios\Control_oficio_materiales as materiales;
use App\Modelos\Territorios\Territorios_estado;
use App\Modelos\ControlOficios\Departamentos;
use App\Modelos\Sigesp\sno_personal;
use App\Modelos\ControlOficios\Ente;
use DB;
use App\Modelos\ControlOficios\Requisicion;

class ControlOficioController extends Controller
{

  public function obtenerDatosTrabajadorSigesp($cedper){
    try {
      $trabajador = DB::connection('sigesp')->select(DB::raw("select
      sno_personal.cedper as cedula,
      sno_personal.nomper || ' ' ||  sno_personal.apeper  as nombres,
      CASE WHEN sno_personal.sexper='M' THEN 'Masculino'
      ELSE 'Femenino'
      END AS genero,
      sno_personal.dirper as direccion,
      sno_personal.telhabper as telefonohabitacion,
      sno_personal.coreleper,
      sno_personal.telmovper as telefonomovil,
      sno_cargo.descar  as cargo,
      srh_departamento.dendep as departamento,
      srh_gerencia.denger as gerencia,
      sno_personalnomina.codcueban as numerocuenta


      from sno_personal
      INNER JOIN sno_personalnomina ON sno_personal.codemp = sno_personalnomina.codemp
      and sno_personal.codper = sno_personalnomina.codper
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN srh_gerencia ON sno_personal.codemp = srh_gerencia.codemp
      and sno_personal.codger = srh_gerencia.codger

      LEFT JOIN srh_departamento ON sno_personalnomina.codemp = srh_departamento.codemp
      and sno_personalnomina.coddep = srh_departamento.coddep
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_cargo ON sno_personalnomina.codemp = sno_cargo.codemp
      and sno_personalnomina.codcar = sno_cargo.codcar
      and sno_personalnomina.codnom = sno_cargo.codnom
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_unidadadmin ON sno_unidadadmin.codemp=sno_personalnomina.codemp
      and sno_unidadadmin.minorguniadm=sno_personalnomina.minorguniadm
      and sno_unidadadmin.ofiuniadm=sno_personalnomina.ofiuniadm
      and sno_unidadadmin.uniuniadm=sno_personalnomina.uniuniadm
      and sno_unidadadmin.depuniadm=sno_personalnomina.depuniadm
      and sno_unidadadmin.prouniadm=sno_personalnomina.prouniadm
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_asignacioncargo ON sno_personalnomina.codemp = sno_asignacioncargo.codemp
      and sno_personalnomina.codasicar = sno_asignacioncargo.codasicar
      and sno_personalnomina.codnom = sno_asignacioncargo.codnom
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_dedicacion ON sno_dedicacion.codemp = sno_personalnomina.codemp
      and sno_dedicacion.codded = sno_personalnomina.codded
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004') where sno_personalnomina.staper IN ('1','2','4')
      AND sno_personal.cedper = '".$cedper."'"));
      if(count($trabajador) > 0)
      return response()->json(['error' => 0, 'trabajador' => $trabajador]);
      else
      return response()->json(['error' => 1, 'msg' => 'Trabajador no encontrado en SIGESP','msgSystem'=>'']);
    } catch (\Exception $e) {
      return response()->json(['error' => 1, 'msg' => $e>getMessage(),'msgSystem'=>'']);
    }
  }//obtenerDatosTrabajadorSigesp
  public function obtenerDatosPersonalNatural(Request $request){
    try {

      $data = DB::connection('sigesp')->select(DB::raw("select
      sno_personal.cedper as cedula,
      sno_personal.nomper || ' ' ||  sno_personal.apeper  as nombres,
      CASE WHEN sno_personal.sexper='M' THEN 'Masculino'
      ELSE 'Femenino'
      END AS genero,
      sno_personal.dirper as direccion,
      sno_personal.telhabper as telefonohabitacion,
      sno_personal.coreleper,
      sno_personal.telmovper as telefonomovil,
      sno_cargo.descar  as cargo,
      srh_departamento.dendep as departamento,
      srh_gerencia.denger as gerencia,
      sno_personalnomina.codcueban as numerocuenta


      from sno_personal
      INNER JOIN sno_personalnomina ON sno_personal.codemp = sno_personalnomina.codemp
      and sno_personal.codper = sno_personalnomina.codper
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN srh_gerencia ON sno_personal.codemp = srh_gerencia.codemp
      and sno_personal.codger = srh_gerencia.codger

      LEFT JOIN srh_departamento ON sno_personalnomina.codemp = srh_departamento.codemp
      and sno_personalnomina.coddep = srh_departamento.coddep
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_cargo ON sno_personalnomina.codemp = sno_cargo.codemp
      and sno_personalnomina.codcar = sno_cargo.codcar
      and sno_personalnomina.codnom = sno_cargo.codnom
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_unidadadmin ON sno_unidadadmin.codemp=sno_personalnomina.codemp
      and sno_unidadadmin.minorguniadm=sno_personalnomina.minorguniadm
      and sno_unidadadmin.ofiuniadm=sno_personalnomina.ofiuniadm
      and sno_unidadadmin.uniuniadm=sno_personalnomina.uniuniadm
      and sno_unidadadmin.depuniadm=sno_personalnomina.depuniadm
      and sno_unidadadmin.prouniadm=sno_personalnomina.prouniadm
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_asignacioncargo ON sno_personalnomina.codemp = sno_asignacioncargo.codemp
      and sno_personalnomina.codasicar = sno_asignacioncargo.codasicar
      and sno_personalnomina.codnom = sno_asignacioncargo.codnom
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

      INNER JOIN sno_dedicacion ON sno_dedicacion.codemp = sno_personalnomina.codemp
      and sno_dedicacion.codded = sno_personalnomina.codded
      and sno_personalnomina.codnom IN ('0001','0002','0003','0004') where sno_personalnomina.staper IN ('1','2','4') AND sno_personal.cedper = '".$request->cedper."'"));
      // $data[0]->nombres=utf8_decode($data[0]->nombres);
      if(count($data) > 0)
      return response()->json(['error' => 0, 'datos_trabajador' => $data, 'es_trabajador' => true]);
      //return response()->json(['error' => 0, 'datos_trabajador' => $data, 'es_trabajador' => true]);

      $persona_natural=pn::where('cedula',$request->cedper)->first();
      if(count($persona_natural)==0){
        return response()->json(['error'=>1,'msg'=>'Cédula no encontrada.']);
      }
      return response()->json(['error'=>0,'persona_natural'=>$persona_natural]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//obtenerDatosPersonalNatural
  public function obtenerDatosPersonalNatural2(Request $request){
    try {
      $persona_natural=pn::where('cedula',$request->cedper)->first();
      if($persona_natural==null){
        return response()->json(['error'=>1,'msg'=>'Cédula no encontrada.']);
      }
      return response()->json(['error'=>0,'persona_natural'=>$persona_natural]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//obtenerDatosPersonalNatural2
  public function obtenerDatosPersonalJuridico(Request $request){
    //$request->rif
    try {
      $empresa=pj::with('ente')->where('rif_cedula_situr',$request->rif)->first();
      if(count($empresa)==0)
      return response()->json(['error'=>1,'msg'=>'Esta empresa no se encuentra registrada en base de datos.','Rif'=>$request->rif]);
      $responsables=responsablesEmpresa::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with('departamento')->where('personal_juridico_id',$empresa->id)->get();
      if(count($responsables)==0)
      return response()->json(['error'=>1,'msg'=>'Esta empresa no posee responsables registrados.']);
      return response()->json(['error'=>0,'empresa'=>$empresa,'responsables'=>$responsables]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//obtenerDatosPersonalNatural
  public function obtenerDatosPersonalJuridicoSinResponsables(Request $request){
    //$request->rif
    try {
      $empresa=pj::with('ente')->where('rif_cedula_situr',$request->rif)->first();
      if(count($empresa)==0)
      return response()->json(['error'=>1,'msg'=>'Esta empresa no se encuentra registrada en base de datos.','Rif'=>$request->rif]);
      return response()->json(['error'=>0,'empresa'=>$empresa]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//obtenerDatosPersonalNatural
  public function obtenerDatosOficio(Request $request){
    //$request->oficio_id;
    try{
      $oficio = null;
      $departamento=null;
      $oficio=Control_oficio_registro_oficio::with(['personal_natural'=>function($q){
        $q->withTrashed();
      }])->with(['motivo_solicitud'=>function($q){
        $q->withTrashed();
      }])->with('personal_juridico','usuario_creador','personal_juridico.ente','personal_juridico.ente.clasificacion_ente','solicitud_productos','solicitud_productos.producto')->where('id',$request->oficio_id)->first();

      if($oficio==null)
      return response()->json(['error'=>1,'msg'=>'Este oficio no se encuentra en base de datos.','Oficio_id'=>$request->oficio_id]);

      $materiales = solicitud_materiales::where('registro_oficio_id', $request->oficio_id)->get();

      foreach($materiales as $material){
        $material->nombre_material = materiales::where('id', $material->materiales_id)->value('nombre');
      }

      $detalle_oficio=de::where('registro_oficio_id',$oficio->id)->get();
      if(count($detalle_oficio)>0){
        $detalle_oficio=de::where('registro_oficio_id',$oficio->id)->first();
        $detalle_oficio->fecha=voltearFecha($detalle_oficio->fecha);
      }
      if($oficio->personal_juridico_id!=null){
        $responsable=responsablesEmpresa::where('personal_natural_id',$oficio->personal_natural_id)->first();
        $departamento=Departamentos::where('id',$responsable->departamento_id)->first();
      }//personal_juridico_id
      $estado=Territorios_estado::where('idEstado',$oficio->estado_id)->first();
      // $estado=[];
      $responsable=responsablesEmpresa::with('departamento')->where('personal_natural_id',$oficio->personal_natural_id)->first();
      return response()->json(['error'=>0,'oficio'=>$oficio,'detalle_oficio'=>$detalle_oficio,'estado'=>$estado,'departamento'=>$departamento,'responsable'=>$responsable, 'materiales' => $materiales]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//obtenerDatosOficio()

  public function obtenerEntes(Request $request){
    //$request->clasificacion_ente_id
    try {
      $entes=Ente::where('clasificacion_ente_id',$request->clasificacion_ente_id)->get();
      if(count($entes)==0)
      return response()->json(['error'=>1,'msg'=>'Esta clasificación de ente,no posee entes asociados.','clasificacion_ente_id'=>$request->clasificacion_ente_id]);
      return response()->json(['error'=>0,'entes'=>$entes]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
    }
  }//obtenerEntes()

  public function obtenerDatosVehiculo(Request $request){
    try {
      $vehiculo=Vehiculos::where('placa', $request->placa)->first();
      if(!$vehiculo)
      return response()->json(['error'=>1,'msg'=>'Este vehículo no se encuentra registrado en base de datos.']);
      return response()->json(['error'=>0,'vehiculo'=>$vehiculo]);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msgSystem'=>$e->getMessage(),'msg'=>'Ha ocurrido un error en el servidor.']);
    }
  }//obtenerDatosVehiculo


  ////////////////////////////////Api v2 sabas was here

  public function obtenerDespachosPendientesPorFecha(Request $request){
    //Recibe: fecha , consultar oficios con estado 7 (En proceso de despacho)
    try {
      $includes=['despacho','oferta_economica','usuario_creador','personal_juridico','personal_juridico.ente'];
      $oficios=Control_oficio_registro_oficio::where('estado_aprobacion_id',7)->with($includes)->whereHas('despacho',function($query) use($request){
        $query->where('fecha_despacho',voltearFecha($request->fecha_despacho));
      })->get();
      $despachos=[];
      foreach($oficios as $oficio){
        $ente='';
        $cantidad_total=0;
        $modelos='';
        $productos=$oficio->oferta_economica->detalle_oferta;
        foreach($productos as $producto){
          $cantidad_total=$cantidad_total+$producto->cantidad;
          if($modelos!="")
          $modelos=$modelos.", ".$producto->producto->nombre;
          else
          $modelos=$producto->producto->nombre;
        }//foreach productos oferta economica

        if($oficio->personal_juridico!=null){
          $ente=' / '.$oficio->personal_juridico->ente->nombre;
        }//si existe personal juridico
        // Obtener gerencia del trabajador
        // $cedper=$oficio->usuario_creador->cedper;
        $trabajador = DB::connection('sigesp')->select(DB::raw("select
        sno_personal.cedper as cedula,
        sno_personal.nomper || ' ' ||  sno_personal.apeper  as nombres,
        CASE WHEN sno_personal.sexper='M' THEN 'Masculino'
        ELSE 'Femenino'
        END AS genero,
        sno_personal.dirper as direccion,
        sno_personal.telhabper as telefonohabitacion,
        sno_personal.coreleper,
        sno_personal.telmovper as telefonomovil,
        sno_cargo.descar  as cargo,
        srh_departamento.dendep as departamento,
        srh_gerencia.denger as gerencia,
        sno_personalnomina.codcueban as numerocuenta


        from sno_personal
        INNER JOIN sno_personalnomina ON sno_personal.codemp = sno_personalnomina.codemp
        and sno_personal.codper = sno_personalnomina.codper
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN srh_gerencia ON sno_personal.codemp = srh_gerencia.codemp
        and sno_personal.codger = srh_gerencia.codger

        LEFT JOIN srh_departamento ON sno_personalnomina.codemp = srh_departamento.codemp
        and sno_personalnomina.coddep = srh_departamento.coddep
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_cargo ON sno_personalnomina.codemp = sno_cargo.codemp
        and sno_personalnomina.codcar = sno_cargo.codcar
        and sno_personalnomina.codnom = sno_cargo.codnom
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_unidadadmin ON sno_unidadadmin.codemp=sno_personalnomina.codemp
        and sno_unidadadmin.minorguniadm=sno_personalnomina.minorguniadm
        and sno_unidadadmin.ofiuniadm=sno_personalnomina.ofiuniadm
        and sno_unidadadmin.uniuniadm=sno_personalnomina.uniuniadm
        and sno_unidadadmin.depuniadm=sno_personalnomina.depuniadm
        and sno_unidadadmin.prouniadm=sno_personalnomina.prouniadm
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_asignacioncargo ON sno_personalnomina.codemp = sno_asignacioncargo.codemp
        and sno_personalnomina.codasicar = sno_asignacioncargo.codasicar
        and sno_personalnomina.codnom = sno_asignacioncargo.codnom
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_dedicacion ON sno_dedicacion.codemp = sno_personalnomina.codemp
        and sno_dedicacion.codded = sno_personalnomina.codded
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004') where sno_personalnomina.staper IN ('1','2','4')
        AND sno_personal.cedper = '".$oficio->usuario_creador->cedper."'"));
        $gerencia=$trabajador[0]->gerencia;
        // Obtener gerencia del trabajador
        $despachos[]=[
          'fecha_despacho'=>$oficio->despacho->fecha_despacho,
          'solicitante_ente'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido.$ente,
          'cantidad_total'=>$cantidad_total,
          'modelos'=>$modelos,
          'gerencia_creador_oficio'=>$gerencia
        ];
      }//oficios
      return response()->json(['error'=>0,'despachos'=>$despachos,'msgSystem'=>'','msg'=>'']);
    } catch (\Exception $e) {
      return response()->json(['error'=>500,'msgSystem'=>$e->getMessage(),'msg'=>'Ha ocurrido un error en el servidor.']);
    }
  }//obtenerDespachosPendientesPorFecha

  public function resumenDespacho(){
    /*
    Cargar oficios con estado 2 y 3 que ya tengan oferta economica generada
    */
    try {
     
      $oficios=Control_oficio_registro_oficio::whereIn('estado_aprobacion_id',[2,3,6,7])->has('oferta_economica')->orderBy('created_at','ASC')->get();

      $arreglo=[];
      foreach($oficios as $oficio){
        $ente='';
        $personal_juridico=null;
        $cantidad_total=0;
        $productos_modelos="";
        $despacho=null;
        if($oficio->personal_juridico!=null){
          $personal_juridico=[
            'rif'=>$oficio->personal_juridico->rif_cedula_situr,
            'nombre_institucion'=>$oficio->personal_juridico->nombre_institucion,
            'nombre_autoridad'=>$oficio->personal_juridico->nombre_autoridad,
            'direccion_fiscal'=>$oficio->personal_juridico->direccion_fiscal,
            'ente'=>$oficio->personal_juridico->ente->nombre,
            'clasificacion_ente'=>$oficio->personal_juridico->ente->clasificacion_ente->nombre
          ];
          //$ente=$oficio->personal_juridico->ente->nombre.' / ';
          $ente=$oficio->personal_juridico->nombre_institucion.' / ';
        }//si existe personal juridico
        
        $productos=$oficio->oferta_economica->detalle_oferta;
       
        foreach($productos as $producto){
          $cantidad_total=$cantidad_total+$producto->cantidad;
          if($productos_modelos=="")
          $productos_modelos=$producto->producto->nombre;
          else
          $productos_modelos=$productos_modelos.', '.$producto->producto->nombre;
        }//foreach productos oferta economica
        if($oficio->despacho){
          $despacho=[
            'id'=>$oficio->despacho->id,
            'fecha_despacho'=>$oficio->despacho->fecha_despacho,
            'chofer_nombre'=>$oficio->despacho->chofer->nombre.' '.$oficio->despacho->chofer->apellido,
            'chofer_cedula'=>$oficio->despacho->chofer->cedula,
            'vehiculo_placa'=>$oficio->despacho->vehiculo->placa,
            'vehiculo_modelo'=>$oficio->despacho->vehiculo->modelo
          ];
        }
       

        if($this->existe_requesicion($oficio->oferta_economica->id)!=0)
        $arreglo[]=[
          'id'=>$oficio->id,
          'oferta_economica_id'=>$oficio->oferta_economica->id,
          'user_id'=>$oficio->user_id,
          'usuario'=>$oficio->usuario_creador->name,
          'numero_oficio'=>$oficio->numero_oficio,
          'estado'=>$oficio->estado->estado,
          'documento'=>$oficio->documento,
          'descripcion'=>$oficio->descripcion,
          'motivo_solicitud'=>$oficio->motivo_solicitud->nombre,
          'motivo_solicitud_id'=>$oficio->motivo_solicitud->id,
          'personal_natural'=>[
            'id'=>$oficio->personal_natural_id,
            'cedula'=>$oficio->personal_natural->cedula,
            'nombre_completo'=>$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
            'codigo_carnet_patria'=>$oficio->personal_natural->codigo_carnet_patria,
            'telefono'=>$oficio->personal_natural->telefono,
            'correo_electronico'=>$oficio->personal_natural->correo_electronico,
            'firma_personal'=>$oficio->personal_natural->firma_personal
          ],
          'personal_juridico'=>$personal_juridico,
          'fecha_solicitud'=>voltearFecha($oficio->fecha_emision),
          'solicitante_ente'=>$ente.$oficio->personal_natural->nombre.' '.$oficio->personal_natural->apellido,
          'estado_aprobacion_id'=>$oficio->estado_aprobacion_id,
          'cantidad_total'=>$cantidad_total,
          'modelos_productos'=>$productos_modelos,
          'despacho'=>$despacho,
          'productos'=>$productos
        ];
      }//foreach oficios
     
      return response()->json(['error'=>0,'oficios'=>$arreglo]);
    } catch (\Exception $e) {
      return response()->json(['error'=>1,'msg'=>'Ocurrió un error en el servidor.','msgSystem'=>$e->getMessage()]);
    }

  }//resumenDespacho
  public function existe_requesicion($oferta_economica_id){
    $existe_requesicion = Requisicion::where('oferta_economica_id', $oferta_economica_id)->get();
    return(count($existe_requesicion));
  }
}
