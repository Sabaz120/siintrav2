<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\SistemaProduccionV1\Productos;
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;
use App\Modelos\ControlOficios\Control_oficio_detalle_oferta_economica;
use App\Modelos\ControlOficios\Requisicion;
use PDF;
use App\Modelos\SistemaProduccionV1\ExistenciaAlmacenDespacho;
use Excel;
use Carbon;
use DB;
class ProductosController extends Controller
{
    public function obtenerProductos(){
      try {
        return response()->json(['error'=>0,'productos'=>Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get()]);
      } catch (\Exception $e) {
        return response()->json(['error'=>500,'msg'=>$e->getMessage()]);
      }
    }//obtenerProductos

    public function obtenerModalidad(Request $request){
      try {
        $producto=Productos::find($request->producto_id);
        $modalidades=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->select('modalidad')->distinct()->get();
        $response=[
          "modalidades"=>$modalidades
        ];
        return response()->json($response,200);
      } catch (\Exception $e) {
        return response()->json(['msg'=>$e->getMessage()],500);
      }

    }//obtenerModalidad()
    public function obtenerColor(Request $request){
      try {
        $producto=Productos::find($request->producto_id);
        $colores=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->where('modalidad',$request->modalidad)->select('modeloColor')->distinct()->get();
        $response=[
          "colores"=>$colores
        ];
        return response()->json($response,200);
      } catch (\Exception $e) {
        return response()->json(['msg'=>$e->getMessage()],500);
      }
    }//obtenerModalidad()

    public function obtenerDisponibilidadProductosRecepcionOficio(Request $request){

      $productos_disponibilidad=[];
      // $producto=Productos::find($request->producto_id);
      $productos=[];
      if($request->producto_id==0){
        //Todos los productos
        $productos=Productos::where('categoria_producto_id',1)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
      }else{
        //Producto especifico
        $productos=Productos::where('categoria_producto_id',1)->where('id',$request->producto_id)->where('edoRegistro',0)->orderBy('descripcion','ASC')->get();
      }
      //dd($productos,$request->all());
      foreach($productos as $producto){
        if(is_numeric($request->modalidad_id))
          $modalidades=ExistenciaAlmacenDespacho::where("idAlmacen", 4)->where('modeloPhone',$producto->nombre)->select('modalidad')->distinct()->get();
        else{
          $modalidades=[];
          $modalidades[]=[
            "modalidad"=>$request->modalidad_id
          ];
          $modalidades=json_decode(json_encode($modalidades));
        }

        $comprometidos = DB::select(DB::raw("SELECT
        sum (doe.cantidad), ap.producto_id,ap.modalidad
        FROM
          control_oficios.control_oficio_registro_oficio ro
          left join control_oficios.control_oficio_oferta_economicas oe
          on ro.id = oe.control_oficio_registro_oficio_id
          left join control_oficios.control_oficio_detalle_oferta_economicas doe
          on doe.control_oficio_oferta_economica_id = oe.id
          left join  control_oficios.control_oficio_requisicion r
          on r.oferta_economica_id = oe.id
          left join control_oficios.control_oficio_productos_aprobacion_parcial ap
          on ap.control_oficio_registro_oficio_id = ro.id
          where
          estado_aprobacion_id=3
          and r.despachado ='f' or  r.despachado is null
         group by ap.producto_id,ap.modalidad"));


        foreach($modalidades as $modalidad){
          $disponible=ExistenciaAlmacenDespacho::where('modeloPhone',$producto->nombre)->where('modalidad',$modalidad->modalidad)->where('despachado',0)->where('idAlmacen',4)->sum('cantidadEnvia');
          $resultado=0;


            /*$registrosComprometidos = Control_oficio_detalle_oferta_economica::join('control_oficio_oferta_economicas', 'control_oficio_detalle_oferta_economicas.control_oficio_oferta_economica_id', '=', 'control_oficio_oferta_economicas.id')->join('control_oficio_registro_oficio', 'control_oficio_oferta_economicas.control_oficio_registro_oficio_id', '=', 'control_oficio_registro_oficio.id')->join('control_oficio_requisicion', 'control_oficio_requisicion.oferta_economica_id', '=', 'control_oficio_oferta_economicas.id')->where('control_oficio_requisicion.despachado', false)->orWhere('control_oficio_requisicion.despachado', null)->where('control_oficio_registro_oficio.estado_aprobacion_id', 3)->where('producto_id', $producto->id)->where('modalidad', $modalidad->modalidad)->get();*/



            foreach($comprometidos as $comprometido){

              if($producto->id == $comprometido->producto_id && $comprometido->modalidad == $modalidad->modalidad){
                $resultado = $comprometido->sum;
              }

            }


             //dd($result);

                    //dd($registrosComprometidos);

          //foreach($registrosComprometidos as $registro){

            //$comprometido = $compro $registro->cantidad;

          //}

          //echo $registrosComprometidos." ".$producto->id."<br>";
          //Solo Consulta oficios parciales (Ya que debido al cambio, que mandaron a agregar modalidad, todas las aprobaciones se iban a aprobación parcial.)
          /*$oficiosParciales=Control_oficio_registro_oficio::where('estado_aprobacion_id',7)
          ->with('aprobacion_parcial_productos')
          ->whereHas('aprobacion_parcial_productos', function ($query) use($producto,$modalidad)  {
            $query->where('producto_id',$producto->id)->where('modalidad',$modalidad->modalidad);
          })->get();
          foreach($oficiosParcales as $oficio){
            foreach($oficio->aprobacion_parcial_productos as $productoOficio){
              $comprometido=$comprometido+(int)$productoOficio->cantidad;
            }//productos
          }//oficiosParciales*/
          //dd($comprometido[0]->sum);


          $productos_disponibilidad[]=[
            "nombre"=>$producto->nombre,
            "descripcion"=>$producto->descripcion,
            "disponible"=>$disponible,
            "modalidad"=>$modalidad->modalidad,
            'comprometido'=> $resultado,
            "total_disponible"=>$disponible-$resultado
          ];
        }
      }//foreach productos

      //dd($productos_disponibilidad);


      if($request->type == 'PDF'){

        $pdf = PDF::loadView('controlOficios.recepcionOficio.reporte.pdf.disponibilidad_productos',
        [
          "titulo"=>"REPORTE DISPONIBILIDAD PRODUCTOS",
          "productos_disponibilidad"=>json_decode(json_encode($productos_disponibilidad))
        ]);
        return $pdf->stream('REPORTE DISPONIBILIDAD DE PRODUCTOS.pdf');

      }else{

        $mytime = \Carbon\Carbon::now();

        Excel::create($mytime->toDateTimeString().'-REPORTE DISPONIBILIDAD PRODUCTOS', function($excel) use($productos_disponibilidad) {

              $excel->sheet("Disponibilidad", function ($sheet) use($productos_disponibilidad) {
                  $sheet->loadView('controlOficios.recepcionOficio.reporte.excel.disponibilidad_producto')->with('productos_disponibilidad', $productos_disponibilidad);
              });

            })->export('xls');

          }


    }//obtenerDisponibilidadProductosRecepcionOficio()
}
