<?php

namespace App\Http\Controllers\Vidamed;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modelos\Vidamed\afiliados;
use App\Modelos\Vidamed\carga_familiar;
use DB;
use App\Modelos\Sigesp\sno_personalnomina;
use App\Modelos\Sigesp\sno_personal;
class VidamedController extends Controller
{
  public function index(){
    $afiliados=afiliados::where('estatus','')->get();//estatus en blanco es activo, si tiene I es inactivo
    $personal=DB::connection('sigesp')->select(DB::raw("SELECT b.codger as codger,b.codper as codper,b.fecingper as FECHA_INGRESO,b.fecnacper as FECHA_NACIMIENTO, a.coddep as coddep,d.desubifis AS UBICACION_FISICA,
      CONCAT(b.apeper,', ',b.nomper) as APELLIDOS_Y_NOMBRES,
      CASE b.sexper WHEN 'M' THEN 'M' ELSE 'F' END AS GENERO,
      b.cedper AS CEDULA,
      b.dirper as DIRECCION,
      CASE b.nivacaper WHEN '0' THEN 'NINGUNO'
      WHEN '1' THEN 'PRIMARIA'
      WHEN '2' THEN 'BACHILLER'
      WHEN '3' THEN 'TECNICO SUPERIOR'
      WHEN '4' THEN 'UNIVERSITARIO'
      WHEN '5' THEN 'MAESTRIA'
      WHEN '6' THEN 'POSTGRADO'
      WHEN '7' THEN 'DOCTORADO' END NIVEL_ACADEMICO,
      f.despro as PROFESION,
      c.descar as CARGO,
      e.desded as DEDICACION,
      b.telhabper as TELEFONO_HABITACION,
      b.telmovper as TELEFONO_MOVIL,
      b.coreleper as CORREO_ELECTRONICO,
      g.desuniadm as UNIDAD_ADMINISTRATIVA,
      (SUBSTRING(g.codestpro1,19,7)||SUBSTRING(g.codestpro2,19,7)||SUBSTRING(g.codestpro3,22,8)) as ESTRUCTURA_PRESUPUESTARIA,
      CASE h.codnom
      WHEN '0001' THEN 'ADMINISTRATIVA'
      WHEN '0002' THEN 'OPERATIVA'
      WHEN '0003' THEN 'CONTRATADO' WHEN '0004'
      THEN 'ALTO NIVEL'
      ELSE 'N/A' END as NOMINA
      FROM sno_personalnomina a, sno_personal b, sno_cargo c,
      sno_ubicacionfisica d, sno_dedicacion e,sno_profesion f, sno_unidadadmin g, sno_nomina h
      WHERE
      a.staper ='1'
      AND b.estper='1'
      -- a.staper IN('1','2','4')
      -- AND b.estper IN('1','2','4')
      AND a.codnom in ('0001','0002','0003','0004')
      AND a.codper=b.codper
      AND a.codcar=c.codcar
      AND a.codnom=c.codnom
      AND a.codubifis=d.codubifis
      AND e.codded=a.codded
      AND f.codpro=b.codpro
      AND a.minorguniadm=g.minorguniadm
      AND a.ofiuniadm=g.ofiuniadm
      AND a.uniuniadm=g.uniuniadm
      AND a.depuniadm=g.depuniadm
      AND a.prouniadm=g.prouniadm
      AND h.codnom=a.codnom
      "));
      //2 vacaciones, 4 suspendido,
      $PersonasNoAfiliadas=array();
      foreach($personal as $persona){
        $b=0;
        foreach($afiliados as $afiliado){
          if($persona->cedula==$afiliado->CEDULA){
            $b=1;
            break;
          }//if
        }//foreach afiliados
        if($b==0){
          $PersonasNoAfiliadas=array_merge($PersonasNoAfiliadas,[['cedula'=>$persona->cedula,'nomina'=>$persona->nomina,'nombres'=>$persona->apellidos_y_nombres,'estructura_presupuestaria'=>$persona->estructura_presupuestaria,'cargo'=>$persona->cargo,'fecha_ingreso'=>$persona->fecha_ingreso,'fecha_nacimiento'=>$persona->fecha_nacimiento,'sexo'=>$persona->genero]]);
        }//b==0
      }//foreach personal
      return view('Vidamed.sincronizar',['afiliadosActivos'=>$afiliados,'sigespActivos'=>$personal,'PersonasNoAfiliadas'=>$PersonasNoAfiliadas]);
    }//index()

    public function afiliarPersonalHcm(Request $request){
      try {
        $afiliado=new afiliados();
        $afiliado->cedula=$request->personal['cedula'];
        $afiliado->nombres=$request->personal['nombres'];
        $afiliado->cargo=$request->personal['cargo'];
        $afiliado->tipo_nomina=$request->personal['nomina'];
        $afiliado->estructura=$request->personal['estructura_presupuestaria'];
        $afiliado->fec_nac=$request->personal['fecha_nacimiento'];
        //Calcular edad aca
        $edad=0;
        $afiliado->edad=$edad;//Calcular edad
        $afiliado->sexo=$request->personal['sexo'];
        $afiliado->id_organis=1;
        $afiliado->organismo="VTELCA";
        $afiliado->estado="FALCON";
        $afiliado->borrado=0;
        $afiliado->estatus="";
        $afiliado->fec_ing=$request->personal['fecha_ingreso'];
        $afiliado->fec_sist=$request->personal['fecha_ingreso'];
        $afiliado->id_plan=3;
        $afiliado->observ_ina="";
        $afiliado->observ_rea="";
        $afiliado->examen="";
        $afiliado->id_convenio=3;
        $afiliado->obs_general="";
        $afiliado->marca=0;
        $afiliado->fec_ina="2017-03-27";
        $afiliado->fec_rea="2017-03-27";
        $afiliado->codtrabajador=0;
        $afiliado->codtra_rea=0;
        $afiliado->mon_plan=0;
        $afiliado->mon_plan_exc=0;
        $afiliado->estr_2018="S";
        $afiliado->save();
        return response()->json(['error'=>0]);
      } catch (\Exception $e) {
        return response()->json(['error'=>1,'msg'=>$e->getMessage()]);
      }

    }//afiliarPersonalHcm()

    public function sincronizarFamiliaresGet(){
      $familiares=DB::connection('sigesp')->select(DB::raw("SELECT codper,count(*)  as cantidad
      FROM sno_familiar where
      codper in (select
        sno_personalnomina.codper
        from sno_personal

        INNER JOIN sno_personalnomina ON sno_personal.codemp = sno_personalnomina.codemp
        and sno_personal.codper = sno_personalnomina.codper
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN srh_gerencia ON sno_personal.codemp = srh_gerencia.codemp
        and sno_personal.codger = srh_gerencia.codger

        LEFT JOIN srh_departamento ON sno_personalnomina.codemp = srh_departamento.codemp
        and sno_personalnomina.coddep = srh_departamento.coddep
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_cargo ON sno_personalnomina.codemp = sno_cargo.codemp
        and sno_personalnomina.codcar = sno_cargo.codcar
        and sno_personalnomina.codnom = sno_cargo.codnom
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_unidadadmin ON sno_unidadadmin.codemp=sno_personalnomina.codemp
        and sno_unidadadmin.minorguniadm=sno_personalnomina.minorguniadm
        and sno_unidadadmin.ofiuniadm=sno_personalnomina.ofiuniadm
        and sno_unidadadmin.uniuniadm=sno_personalnomina.uniuniadm
        and sno_unidadadmin.depuniadm=sno_personalnomina.depuniadm
        and sno_unidadadmin.prouniadm=sno_personalnomina.prouniadm
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_asignacioncargo ON sno_personalnomina.codemp = sno_asignacioncargo.codemp
        and sno_personalnomina.codasicar = sno_asignacioncargo.codasicar
        and sno_personalnomina.codnom = sno_asignacioncargo.codnom
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

        INNER JOIN sno_dedicacion ON sno_dedicacion.codemp = sno_personalnomina.codemp
        and sno_dedicacion.codded = sno_personalnomina.codded
        and sno_personalnomina.codnom IN ('0001','0002','0003','0004') where sno_personalnomina.staper in ('1','2')
      ) GROUP by codper;
      "));
      $carga_familiar=DB::connection('vidamedDB')->select(DB::raw("SELECT count(*) as cantidad,cedu_titu FROM seguro_vtelca.carga_familiar group by cedu_titu;"));
      $diferentes=[];
      foreach($carga_familiar as $familiarVidamed){
        foreach($familiares as $familiarSigesp){
          if(str_pad(trim($familiarVidamed->cedu_titu),10,"0",STR_PAD_LEFT)==$familiarSigesp->codper){
            if((int)$familiarVidamed->cantidad!=(int)$familiarSigesp->cantidad){
              $sigesp=DB::connection('sigesp')->select(DB::raw("SELECT cedula, cedfam, apefam || ', ' || nomfam  as nombre_familiar, nexfam, fecnacfam, sexfam
                FROM sno_familiar where
                codper in (select
                  sno_personalnomina.codper
                  from sno_personal

                  INNER JOIN sno_personalnomina ON sno_personal.codemp = sno_personalnomina.codemp
                  and sno_personal.codper = sno_personalnomina.codper
                  and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

                  INNER JOIN srh_gerencia ON sno_personal.codemp = srh_gerencia.codemp
                  and sno_personal.codger = srh_gerencia.codger

                  LEFT JOIN srh_departamento ON sno_personalnomina.codemp = srh_departamento.codemp
                  and sno_personalnomina.coddep = srh_departamento.coddep
                  and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

                  INNER JOIN sno_cargo ON sno_personalnomina.codemp = sno_cargo.codemp
                  and sno_personalnomina.codcar = sno_cargo.codcar
                  and sno_personalnomina.codnom = sno_cargo.codnom
                  and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

                  INNER JOIN sno_unidadadmin ON sno_unidadadmin.codemp=sno_personalnomina.codemp
                  and sno_unidadadmin.minorguniadm=sno_personalnomina.minorguniadm
                  and sno_unidadadmin.ofiuniadm=sno_personalnomina.ofiuniadm
                  and sno_unidadadmin.uniuniadm=sno_personalnomina.uniuniadm
                  and sno_unidadadmin.depuniadm=sno_personalnomina.depuniadm
                  and sno_unidadadmin.prouniadm=sno_personalnomina.prouniadm
                  and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

                  INNER JOIN sno_asignacioncargo ON sno_personalnomina.codemp = sno_asignacioncargo.codemp
                  and sno_personalnomina.codasicar = sno_asignacioncargo.codasicar
                  and sno_personalnomina.codnom = sno_asignacioncargo.codnom
                  and sno_personalnomina.codnom IN ('0001','0002','0003','0004')

                  INNER JOIN sno_dedicacion ON sno_dedicacion.codemp = sno_personalnomina.codemp
                  and sno_dedicacion.codded = sno_personalnomina.codded
                  and sno_personalnomina.codnom IN ('0001','0002','0003','0004')  where sno_personalnomina.staper in ('1','2') and sno_personal.codper='$familiarSigesp->codper'  ORDER BY sno_personal.cedper
                ) order by codper;
                "));
                $diferentes=array_merge($diferentes,[['cedula'=>$familiarVidamed->cedu_titu,'sigesp'=>$sigesp,'vidamed'=>carga_familiar::where('cedu_titu',$familiarVidamed->cedu_titu)->get()]]);
              }//if cantidad de familiares es distinto en ambas DB
            }//if codper== cedu_titu
          }//foreach familiar SIGESP
        }//foreach carga familiar Vidamed
        return view('Vidamed.sincronizar_familiares',['personalFamiliar'=>$diferentes]);
      }//sincronizarFamiliaresGet
    }
