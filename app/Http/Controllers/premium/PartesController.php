<?php

namespace App\Http\Controllers\premium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PartesController extends Controller
{

    function index(){
        return view("premium.preciosPartes");
    }

    function search(Request $request){

        $response = DB::connection("premiumsoft")->select(DB::raw("SELECT CODIGO,FACTOR,CFOB,COSTO,PRECIO1,PRECIOFIN1 FROM `articulo` where codigo ='".$request->codigo."'"));
        return response()->json(["response" => $response, "msg" => "Búsqueda finalizada", "success" => true]);
    }
}
