<?php
namespace App\Http\Controllers\premium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Modelos\PremiumSoft\PrecioPetro;

class PetroController extends Controller
{
    
    function index(){

        $petro = DB::connection('premiumsoft')->table('monedas')->where('codigo', '002')->first();

        return view('premium.precioPetro', ["petro" => $petro]);

    }

    function update(Request $request){

        try{

            DB::connection('premiumsoft')->table('monedas')->where('codigo', '002')->update([
                'ultimacotizacion' => $request->precio
            ]);
    
            $articulosPetro = DB::connection('premiumsoft')->table('precio_petro')->get();
    
            foreach($articulosPetro as $articulo){

                /*$precio_petro_sin_iva = $articulo->precio1 / 1.16;
                $precio_petro_iva = $articulo->precio1 - $precio_petro_sin_iva;
                $iva = $precio_petro_iva * $request->precio;*/

                $precio_con_iva = $articulo->preciofin1 * $request->precio;
                $precio_sin_iva = $articulo->precio1 * $request->precio;
                
                $precio_con_iva2 = $articulo->preciofin2 * $request->precio;
                $precio_sin_iva2 = $articulo->precio2 * $request->precio;

                $precio_con_iva3 = $articulo->preciofin3 * $request->precio;
                $precio_sin_iva3 = $articulo->precio3 * $request->precio;
    
                
                //DB::connection('premiumsoft')->table('articulo')->where('codigo', $articulo->codigo)->update(["preciofin1" => $precio_con_iva, "precio1" => $precio_sin_iva,"factor" => $request->precio]);
                DB::connection('premiumsoft')->table('articulo')->where('codigo', $articulo->codigo)->update(["preciofin1" => $precio_con_iva, "precio1" => $precio_sin_iva, "preciofin2" => $precio_con_iva2, "precio2" => $precio_sin_iva2, "preciofin3" => $precio_con_iva3, "precio3" => $precio_sin_iva3, "factor" => $request->precio]);
            }
    
            $response = ["msg" => "Valor actualizado", "success" => true, "value" => $request->all()];

        }catch(\Exception $e){
            $response = ["msg" => $e->getMessage(), "success" => false, "line" => $e->getLine()];
        }

        return response()->json($response);

    }

    function factorPP(){
        $petro   = DB::connection('premiumsoft')->table('articulo')->whereIn("grupo", ["005", "006"])->first();
        return view("premium.factorpp", ["petro" => $petro->factor]);
    }

    function gestionPrecios(){

        return view('premium.gestionPrecioPetro');
    }

    function buscarPrecios(Request $request){

        try{

            $precios = DB::connection('premiumsoft')->table('precio_petro')->where('codigo', $request->codigo)->get();
            return response()->json(['success' => true, 'data' => $precios, "msg" => "Búsqueda finalizada"]);

        }catch(\Exception $e){

            return response()->json(['success' => false, 'msg' => 'Error en el servidor']);

        }

    }

    function actualizarPrecios(Request $request){

        try{

            PrecioPetro::updateOrCreate(
                ['codigo' => $request->codigo],
                ['precio1' => $request->precio1/1.16, 'precio2' => $request->precio2/1.16, 'precio3' => $request->precio3/1.16, 'preciofin1' => $request->precio1, 'preciofin2' => $request->precio2, 'preciofin3' => $request->precio3]
            );

            return response()->json(["success" => true, "msg" => "Precios actualizados"]);

        }catch(\Exception $e){

            return response()->json(["success" => false, "msg" => "Error en el servidor", "error" => $e->getMessage()]);

        }


    }

    function factorPPupdate(Request $request){

        try{
            
            $articulos = DB::connection('premiumsoft')->table('articulo')->whereIn("grupo", ["005", "006"])->get();
         
            foreach($articulos as $articulo){

                $baseImponible = $articulo->cfob / 1.16;
                $factor = $request->precio;
                $precio1 = $baseImponible * $factor;

                $precioFin1 = $articulo->cfob * $factor;

                DB::connection('premiumsoft')->table('articulo')->where("codigo", $articulo->codigo)->update([
                    "precio1" => $precio1,
                    "preciofin1" => $precioFin1,
                    "factor" => $factor
                ]);

            }

            //DB::connection('premiumsoft')->table('articulo')->whereIn("grupo", ["005", "006"])->update(["factor" => $request->precio]);
            
    
            $response = ["msg" => "Valor actualizado", "success" => true, "value" => $request->all()];

        }catch(\Exception $e){
            $response = ["msg" => $e->getMessage(), "success" => false, "line" => $e->getLine()];
        }

        return response()->json($response);


    }

    function obtenerModelos(){
        try{

            $modelos = DB::connection('premiumsoft')->table('articulo')->where('grupo', 001)->orderBy('codigo', 'asc')->get();
            $modelos = $this->convert_from_latin1_to_utf8_recursively($modelos);
            return response()->json($modelos);

        }catch(\Exception $e){

            return response()->json(["error" => $e->getMessage(), "line" => $e->getLine()]);

        }

    }

    public function convert_from_latin1_to_utf8_recursively($dat)
    {
          if (is_string($dat)) {
             return utf8_encode($dat);
          } elseif (is_array($dat)) {
             $ret = [];
             foreach ($dat as $i => $d) $ret[ $i ] = self::convert_from_latin1_to_utf8_recursively($d);

             return $ret;
          } elseif (is_object($dat)) {
             foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

             return $dat;
          } else {
             return $dat;
        }
    }

}
