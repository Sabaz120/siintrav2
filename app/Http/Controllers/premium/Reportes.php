<?php

namespace App\Http\Controllers\premium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon;
use DB;
use Excel;

class Reportes extends Controller
{
    
	function libroVentas(){

		$grupos = DB::connection('premiumsoft')->select(DB::raw($this->gruposQuery()));
		return view('premium.libroVentas', ['grupos' => $grupos]);
	}

	function reportes(Request $request){
	
		$busqueda = "";
		
		
		if($request->tipo == 1){
			
			$data = DB::connection('premiumsoft')->select(DB::raw($this->agenteAutorizadoQuery($request->fecha_inicio, $request->fecha_fin, $busqueda)));
			
			$this->excel($data, $request->fecha_inicio, $request->fecha_fin, $request->tipoReporte);
		}else if($request->tipo == 2){
			
			$data = DB::connection('premiumsoft')->select(DB::raw($this->postVentaQuery($request->fecha_inicio, $request->fecha_fin, $busqueda)));
			$this->excelPostVenta($data, $request->fecha_inicio, $request->fecha_fin, $request->tipoReporte);
			
		}else if($request->tipo == 3){
			
			$data = DB::connection('premiumsoft')->select(DB::raw($this->ventaDirectaQuery($request->fecha_inicio, $request->fecha_fin, $busqueda)));
			$this->excelVentaDirecta($data, $request->fecha_inicio, $request->fecha_fin, $request->tipoReporte);
		}else{
			$data1 = DB::connection('premiumsoft')->select(DB::raw($this->agenteAutorizadoQuery($request->fecha_inicio, $request->fecha_fin, $busqueda)));
			$data2 = DB::connection('premiumsoft')->select(DB::raw($this->postVentaQuery($request->fecha_inicio, $request->fecha_fin, $busqueda)));
			$data3 = DB::connection('premiumsoft')->select(DB::raw($this->ventaDirectaQuery($request->fecha_inicio, $request->fecha_fin, $busqueda)));
		
			$this->excelTodo($data1, $data2, $data3, $request->fecha_inicio, $request->fecha_fin);
		}
		
		
		

	}

	function excelTodo($data1, $data2, $data3, $fechaInicio, $fechaFin){
	
		$mytime = \Carbon\Carbon::now();
		Excel::create($mytime->toDateTimeString().'-LIBRO VENTAS', function($excel) use($data1, $data2, $data3, $fechaInicio, $fechaFin) {
	        $excel->sheet("Libro Ventas", function ($sheet) use($data1, $data2, $data3, $fechaInicio, $fechaFin) {
          		$sheet->loadView('premium.excel.todosReportes')->with('data1',$data1)->with('data2',$data2)->with('data3',$data3)->with('fechaInicio', $fechaInicio)->with('fechaFin', $fechaFin);
				$sheet->setColumnFormat(array(
					'I' => '0.00',
					'J' => '0.00',
					'L' => '0.00',
					'M' => '0.00',
					'N' => '0.00',
					'O' => '0.00',
				));
			});
      	})->export('xls');
	}

	function agenteAutorizadoQuery($fecha_inicio, $fecha_fin, $busqueda){
		$string ="";
		if($busqueda != ""){
			$string = "AND op.grupo IN ($busqueda)";
		}

		return "SELECT
		o.emision AS 'fecha_de_documento',
		o.codcliente AS 'rif',
		o.nombrecli AS 'nombre_o_razon_Social',
		CONCAT(o.tipodoc, '-', o.documento) AS 'numero_factura',
		o.orden AS 'numero_control_factura',
		o.referencia AS 'numero_referencia',
		IF(o.tipodoc='N/C', dv.cantidad  * -1 ,CAST(op.cantidad AS SIGNED)) AS 'cantidad',
		IF(o.tipodoc='N/C', dv.preciounit * -1 , op.preciounit) AS 'precio_unitario',
		IF(o.tipodoc='N/C', dv.tipoprecio, o.tipoprecio) as tipo_precio,
		IF(o.tipodoc='N/C', dv.codigo , op.codigo) AS 'modelo_telefono',
		IF(o.tipodoc='N/C','N/C','01-REG') AS 'tipo_transaccion',
		IF(o.tipodoc='N/C',dv.montoneto * -1 ,op.montoneto) AS 'base_imponible',
		IF(o.tipodoc='N/C',dv.timpueprc * -1,op.timpueprc)as 'alicuota',
		IF(o.tipodoc='N/C',(dv.montoneto * dv.timpueprc/100)*-1,op.montoneto * op.timpueprc/100 ) as 'impuesto_iva',
		IF(o.tipodoc='N/C',(dv.montoneto+(dv.montoneto * dv.timpueprc/100))*-1,op.montoneto+(op.montoneto * op.timpueprc/100)) as 'total'
		FROM
		operti o
		LEFT JOIN opermv op ON o.documento = op.documento and op.grupo='001'
		LEFT join devolmv dv on dv.documento=(SUBSTR(o.notas,43,8))
		WHERE
		   o.emision BETWEEN '$fecha_inicio' AND '$fecha_fin' AND op.grupo='001' AND o.documento LIKE '00%'  AND (INSTR('N/C', o.tipodoc) OR INSTR('FAC', o.tipodoc))
				GROUP BY o.documento , o.orden, o.codcliente ASC
		ORDER BY o.emision , o.horadocum , o.documento  ASC";
	}

	function postVentaQuery($fecha_inicio, $fecha_fin, $busqueda){
		$string ="";
		if($busqueda != ""){
			$string = "AND op.grupo IN ($busqueda)";
		}

		return "SELECT 
		o.emision AS 'fecha_de_documento',
		o.codcliente AS 'rif',
		o.nombrecli AS 'nombre_o_razon_Social',
		CONCAT(o.tipodoc, '-', o.documento) AS 'numero_factura',
		o.orden AS 'numero_control_factura',
		o.referencia AS 'numero_referencia',
		IF(o.tipodoc = 'N/C',
			dv.cantidad * - 1,
			CAST(op.cantidad AS SIGNED)) AS 'cantidad',
	 
	
		IF(o.tipodoc = 'N/C', 'N/C', '01-REG') AS 'tipo_transaccion',
		IF(o.tipodoc = 'N/C',
			dv.montoneto * - 1,
			op.montoneto) AS 'base_imponible',
		IF(o.tipodoc = 'N/C',
			dv.timpueprc * - 1,
			op.timpueprc) AS 'alicuota',
		IF(o.tipodoc = 'N/C',
			(dv.montoneto * dv.timpueprc / 100) * - 1,
			op.montoneto * op.timpueprc / 100) AS 'impuesto_iva',
		IF(o.tipodoc = 'N/C',
			(dv.montoneto + (dv.montoneto * dv.timpueprc / 100)) * - 1,
			op.montoneto + (op.montoneto * op.timpueprc / 100)) AS 'total'
	FROM
		operti o
			LEFT JOIN
		opermv op ON o.documento = op.documento AND op.grupo in ('005','006') and o.documento like 'C00%'
			LEFT JOIN
		devolmv dv ON dv.documento = (SUBSTR(o.notas, 43, 8))
	WHERE
		o.emision BETWEEN '$fecha_inicio' AND '$fecha_fin'
			AND (o.documento LIKE 'C00%'    OR op.grupo in('005','006') OR (o.tipodoc = 'N/C' AND o.referencia LIKE 'FACC%'))
			#AND op.grupo IN ('005','006')
			AND (INSTR('N/C', o.tipodoc) OR INSTR('FAC-C', o.tipodoc))
			GROUP BY o.documento , o.orden, o.codcliente ASC
	ORDER BY o.emision , o.horadocum , o.documento  ASC;";
	}

	function ventaDirectaQuery($fecha_inicio, $fecha_fin, $busqueda){
		$string ="";
		if($busqueda != ""){
			$string = "AND op.grupo IN ($busqueda)";
		}

		return "SELECT
		o.emision AS 'fecha_de_documento',
		o.codcliente AS 'rif',
		o.nombrecli AS 'nombre_o_razon_Social',
		CONCAT(o.tipodoc, '-', o.documento) AS 'numero_factura',
		o.orden AS 'numero_control_factura',
		o.referencia AS 'numero_referencia',
		IF(o.tipodoc='N/C', dv.cantidad  * -1 ,CAST(op.cantidad AS SIGNED)) AS 'cantidad',
		IF(o.tipodoc='N/C', dv.preciounit * -1 , op.preciounit) AS 'precio_unitario',
		IF(o.tipodoc='N/C', dv.tipoprecio, o.tipoprecio) as tipo_precio,
		IF(o.tipodoc='N/C', dv.codigo , op.codigo) AS 'modelo_telefono',
		IF(o.tipodoc='N/C','N/C','01-REG') AS 'tipo_transaccion',
		IF(o.tipodoc='N/C',dv.montoneto * -1 ,op.montoneto) AS 'base_imponible',
		IF(o.tipodoc='N/C',dv.timpueprc * -1,op.timpueprc)as 'alicuota',
		IF(o.tipodoc='N/C',(dv.montoneto * dv.timpueprc/100)*-1,op.montoneto * op.timpueprc/100 ) as 'impuesto_iva',
		IF(o.tipodoc='N/C',(dv.montoneto+(dv.montoneto * dv.timpueprc/100))*-1,op.montoneto+(op.montoneto * op.timpueprc/100)) as 'total'
		FROM
		operti o
		LEFT JOIN opermv op ON o.documento = op.documento and op.grupo='001'
		LEFT join devolmv dv on dv.documento=(SUBSTR(o.notas,43,8))
		WHERE
		o.emision BETWEEN '$fecha_inicio' AND '$fecha_fin'
		AND (o.documento LIKE 'B00%'OR (o.tipodoc='N/C' AND o.documento LIKE '000%' AND o.referencia LIKE 'FACB%' ))
		AND(INSTR('N/C', o.tipodoc) OR INSTR('FAC', o.tipodoc) )
		ORDER BY o.emision, o.horadocum,o.documento, modelo_telefono asc;";
	}

	function gruposQuery(){
		return "SELECT codigo,nombre FROM admin001000.grupos WHERE codigo IN ('001','005','006')";
	}

	function excel($data, $fechaInicio, $fechaFin, $tipoReporte){
	
		$mytime = \Carbon\Carbon::now();
		Excel::create($mytime->toDateTimeString().'-LIBRO VENTAS', function($excel) use($data, $fechaInicio, $fechaFin, $tipoReporte) {
	        $excel->sheet("Libro Ventas", function ($sheet) use($data, $fechaInicio, $fechaFin, $tipoReporte) {
          		$sheet->loadView('premium.excel.libroVentas')->with('data',$data)->with('fechaInicio', $fechaInicio)->with('fechaFin', $fechaFin)->with("tipoReporte", $tipoReporte);
				$sheet->setColumnFormat(array(
					'I' => '0.00',
					'J' => '0.00',
					'L' => '0.00',
					'M' => '0.00',
					'N' => '0.00',
					'O' => '0.00',
				));
			});
      	})->export('xls');
	}

	function excelPostVenta($data, $fechaInicio, $fechaFin, $tipoReporte){

		$mytime = \Carbon\Carbon::now();

		Excel::create($mytime->toDateTimeString().'-LIBRO VENTAS', function($excel) use($data, $fechaInicio, $fechaFin, $tipoReporte) {
	        $excel->sheet("Libro Ventas", function ($sheet) use($data, $fechaInicio, $fechaFin, $tipoReporte) {
				  $sheet->loadView('premium.excel.libroVentasPostVenta')->with('data',$data)->with('fechaInicio', $fechaInicio)->with('fechaFin', $fechaFin)->with("tipoReporte", $tipoReporte);
				  $sheet->setColumnFormat(array(
					'I' => '0.00',
					'J' => '0.00',
					'L' => '0.00',
					'M' => '0.00',
					'N' => '0.00',
					'O' => '0.00',
				));
	        });
      	})->export('xls');
	}

	function excelVentaDirecta($data, $fechaInicio, $fechaFin, $tipoReporte){

		$mytime = \Carbon\Carbon::now();
		Excel::create($mytime->toDateTimeString().'-LIBRO VENTAS', function($excel) use($data, $fechaInicio, $fechaFin, $tipoReporte) {
	        $excel->sheet("Libro Ventas", function ($sheet) use($data, $fechaInicio, $fechaFin, $tipoReporte) {
				  $sheet->loadView('premium.excel.ventaDirecta')->with('data',$data)->with('fechaInicio', $fechaInicio)->with('fechaFin', $fechaFin)->with("tipoReporte", $tipoReporte);
				  $sheet->setColumnFormat(array(
					'I' => '0.00',
					'J' => '0.00',
					'L' => '0.00',
					'M' => '0.00',
					'N' => '0.00',
					'O' => '0.00',
				));
	        });
      	})->export('xls');
	}

}

