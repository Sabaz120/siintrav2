<?php

namespace App\Modelos\PremiumSoft;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
  public $timestamps = false;
  protected $primaryKey="codigo";
  protected $connection="premiumsoft";
  protected $table="articulo";
}
