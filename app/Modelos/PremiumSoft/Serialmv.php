<?php

namespace App\Modelos\PremiumSoft;

use Illuminate\Database\Eloquent\Model;

class Serialmv extends Model
{
  public $timestamps = false;
  // protected $primaryKey="codigo";
  protected $connection="premiumsoft";
  protected $table="serialmv";
  protected $fillable = [
      'id_empresa',
      'codigo',
      'serial',
      'almacen',
      'estatus',
      'agencia',
      'tipodoc',
      'documento',
      'sumaresta',
      'doc_id',
      'fecha_doc',
      'destino',
      'cliente',
  ];
}
