<?php

namespace App\Modelos\PremiumSoft;

use Illuminate\Database\Eloquent\Model;

class PrecioPetro extends Model
{
    public $timestamps = false;
    // protected $primaryKey="codigo";
    protected $connection="premiumsoft";
    protected $table="precio_petro";

    protected $fillable=["precio1", "precio2", "precio3", "preciofin1", "preciofin2", "preciofin3", "codigo"];
}
