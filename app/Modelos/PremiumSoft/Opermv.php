<?php

namespace App\Modelos\PremiumSoft;

use Illuminate\Database\Eloquent\Model;

class Opermv extends Model
{
  public $timestamps = false;
  // protected $primaryKey="codigo";
  protected $connection="premiumsoft";
  protected $table="opermv";

}
