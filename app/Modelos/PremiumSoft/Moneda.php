<?php

namespace App\Modelos\PremiumSoft;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    public $timestamps = false;
    protected $connection="premiumsoft";
    protected $table="monedas";

    protected $fillable = ["ultimacotizacion", "sigular"];

}
