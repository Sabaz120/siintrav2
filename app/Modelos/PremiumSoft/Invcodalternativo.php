<?php

namespace App\Modelos\PremiumSoft;

use Illuminate\Database\Eloquent\Model;

class Invcodalternativo extends Model
{
  public $timestamps = false;
  protected $primaryKey="codigo";
  protected $connection="premiumsoft";
  protected $table="invcodalternativo";

  public function articulo()
  {
      return $this->hasOne('App\Modelos\PremiumSoft\Articulo','codigo','codigo');
  }

  protected $casts=[
    "codigo"=>"string"
  ];
}
