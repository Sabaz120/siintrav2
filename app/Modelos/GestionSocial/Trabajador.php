<?php

namespace App\Modelos\GestionSocial;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_trabajador';
  protected $fillable = [
      'nombre','cedula','apellido','departamento','cargo'
  ];

  
}
