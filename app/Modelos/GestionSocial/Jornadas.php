<?php

namespace App\Modelos\GestionSocial;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Jornadas extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_jornada';
  protected $fillable = [
      'nombre', 'fecha_inicio','fecha_fin','lugar','hora'
  ];
}
