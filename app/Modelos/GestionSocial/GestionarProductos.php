<?php

namespace App\Modelos\GestionSocial;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class GestionarProductos extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_producto';
  protected $fillable = [
      'nombre', 'clasificacion_productos_id'
  ];
  public function clasificacion(){
    return $this->belongsTo('App\Modelos\GestionSocial\ClasificacionProducto','clasificacion_productos_id');
  }

}
