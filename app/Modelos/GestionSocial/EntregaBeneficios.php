<?php

namespace App\Modelos\GestionSocial;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modelos\GestionSocial\Trabajador;
use Illuminate\Database\Eloquent\Model;
// use App\Trabajador;

class EntregaBeneficios extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_entrega_beneficios';
  protected $fillable = [
      'cedula', 'entregado','detalle_jornada_id','marcaje'
  ];

  protected $appends=['trabajador'];

  public function datosUsers(){
    return $this->belongsTo('App\Modelos\GestionSocial\Trabajador','cedula');
  }

  public function getTrabajadorAttribute(){
    $trabajador=Trabajador::where('cedula',$this->cedula)->first();
    return $trabajador;
  }
}
