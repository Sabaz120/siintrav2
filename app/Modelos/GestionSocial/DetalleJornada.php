<?php

namespace App\Modelos\GestionSocial;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class DetalleJornada extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_detalle_jornada';
  protected $fillable = [
      'modalidad', 'monto','description','jornada_id','producto_id'
  ];


  public function producto(){
    return $this->belongsTo('App\Modelos\GestionSocial\GestionarProductos','producto_id');
  }

  public function jornada(){
    return $this->belongsTo('App\Modelos\GestionSocial\Jornadas','jornada_id');
  }

  public function clasificacion(){
    return $this->belongsTo('App\Modelos\GestionSocial\ClasificacionProducto','producto_id');
  }

}
