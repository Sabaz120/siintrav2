<?php

namespace App\Modelos\GestionSocial;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GestionarBiometricos extends Model
{
     use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_biometricos';
  protected $fillable = [
      'ip','estado'
  ];
}
