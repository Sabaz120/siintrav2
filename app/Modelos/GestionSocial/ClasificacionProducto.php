<?php

namespace App\Modelos\GestionSocial;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class ClasificacionProducto extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_gestion_social';
  protected $table = 'gestion_social.gestion_social_clasificacion_producto';
  protected $fillable = [
      'nombre'
  ];
}
