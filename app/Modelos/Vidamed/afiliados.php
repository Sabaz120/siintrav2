<?php

namespace App\Modelos\Vidamed;

use Illuminate\Database\Eloquent\Model;

class afiliados extends Model
{
    protected $connection="vidamedDB";
    protected $table="afiliados";
    public $timestamps = false;

}
