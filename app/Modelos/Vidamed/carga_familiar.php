<?php

namespace App\Modelos\Vidamed;

use Illuminate\Database\Eloquent\Model;

class carga_familiar extends Model
{
  protected $connection="vidamedDB";
  protected $table="carga_familiar";
  public $timestamps = false;
}
