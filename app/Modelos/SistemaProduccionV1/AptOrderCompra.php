<?php

namespace App\Modelos\SistemaProduccionV1;

use Illuminate\Database\Eloquent\Model;

class AptOrderCompra extends Model
{
  public $timestamps = false;
  protected $primaryKey="id";
  protected $connection="sistemaproduccionv1";
  protected $table="apt_orden_compra";
  protected $fillable=[
    'id',
    'codigo_orden_compra',
    'fecha_estimada_salida',
    'concepto',
    'moneda_id',
    'cliente_id',
    'fecha_registro',
    'estado_oc_id',
    'activo',
    'sistema_oficio',
  ];
}
