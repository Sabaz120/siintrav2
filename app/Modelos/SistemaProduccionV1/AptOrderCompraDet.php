<?php

namespace App\Modelos\SistemaProduccionV1;

use Illuminate\Database\Eloquent\Model;

class AptOrderCompraDet extends Model
{
  public $timestamps = false;
  protected $primaryKey="id";
  protected $connection="sistemaproduccionv1";
  protected $table="apt_orden_compra_det";
  protected $fillable=[
    'id',
    'producto_id',
    'color_id',
    'cantidad_telefonos',
    'costo_unitario',
    'nivel_precio',
    'descuento',
    'recargo',
    'seed_stock',
    'unidad_medida_id',
    'modalidad',
    'orden_compra_pt_id',
  ];
}
