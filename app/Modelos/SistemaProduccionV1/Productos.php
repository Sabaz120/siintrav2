<?php

namespace App\Modelos\SistemaProduccionV1;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
  protected $primaryKey="id";
  protected $connection="sistemaproduccionv1";
  protected $table="productos";
}
