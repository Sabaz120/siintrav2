<?php

namespace App\Modelos\SistemaProduccionV1;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
  protected $primaryKey="id";
  public $timestamps = false;
  protected $connection="sistemaproduccionv1";
  protected $table="clientes";
  protected $fillable=[
    'codigoCliente',
    'nombre',
    'representante',
    'telefonoRepresentante',
    'correoElectronico',
    'direccionCliente',
    'estatus'
  ];//asdadad
}
