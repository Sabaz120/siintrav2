<?php

namespace App\Modelos\Territorios;

use Illuminate\Database\Eloquent\Model;

class Territorios_estado extends Model
{
  protected $primaryKey="idEstado";
  protected $connection="territorios";
  protected $table="estado";
}
