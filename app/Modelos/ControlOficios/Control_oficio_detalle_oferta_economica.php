<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_detalle_oferta_economica extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_detalle_oferta_economicas";

  protected $fillable = [
    'control_oficio_oferta_economica_id',
     'precio_unitario',
     'precio_unitario_petro',
     'cantidad',
     'producto_id',
     'modalidad',
     // 'producto_descripcion',
     // 'producto_nombre',
     'nivel_precio',
     'codigo_factura'
  ];

  public function producto(){
    return $this->belongsTo('App\Modelos\SistemaProduccionV1\Productos','producto_id');
  }

}
