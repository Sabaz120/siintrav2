<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_vehiculo extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_vehiculos";
  protected $fillable = [
    'placa', 'modelo','descripcion'
  ];
}
