<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control_oficio_registro_oficio extends Model
{
    use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table="control_oficio_registro_oficio";
    public function usuario_creador(){
      return $this->belongsTo('App\User','user_id');
    }
    public function personal_natural(){
      return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_natural','personal_natural_id');
    }
    public function personal_juridico(){
      return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_juridico','personal_juridico_id');
    }
    public function motivo_solicitud(){
      return $this->belongsTo('App\Modelos\ControlOficios\MotivoSolicitud','motivo_solicitud_id');
    }
    public function detalle_evento(){
      return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_detalle_evento');
    }
    public function estado(){
      return $this->belongsTo('App\Modelos\Territorios\Territorios_estado','estado_id');
    }
    public function estado_aprobacion(){
      return $this->belongsTo('App\Modelos\Estados','estado_aprobacion_id');
    }
    public function solicitud_productos(){
      return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_solicitud_productos');
    }
    public function aprobacion_parcial_productos(){
      return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_productos_aprobacion_parcial');
    }
    public function solicitud_materiales(){
      return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_solicitud_material','registro_oficio_id');
    }
    public function aprobacion_parcial_materiales(){
      return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_solicitud_material_aprobacion_parcial','registro_oficio_id');
    }
    public function oferta_economica(){
      return $this->hasOne('App\Modelos\ControlOficios\Control_oficio_oferta_economica');
    }

}
