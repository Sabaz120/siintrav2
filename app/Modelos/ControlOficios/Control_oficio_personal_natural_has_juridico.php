<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_personal_natural_has_juridico extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_personal_natural_has_personal_juridico";
  protected $fillable = [
    'personal_natural_id', 'personal_juridico_id','departamento_id'
  ];
  public function personal_natural(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_natural','personal_natural_id');
  }
  public function personal_juridico(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_juridico','personal_juridico_id');
  }
  public function departamento(){
    return $this->belongsTo('App\Modelos\ControlOficios\Departamentos','departamento_id');
  }
}
