<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TiempoAtencion extends Model
{
  use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table = 'control_oficios.control_oficio_tiempo_atencion';
    protected $fillable = [
        'tiempo',
    ];
}
