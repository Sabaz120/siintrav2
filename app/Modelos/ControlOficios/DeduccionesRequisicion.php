<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class DeduccionesRequisicion extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_deducciones_requisiciones";
  protected $fillable = [
      'deduccion_id',
      'requisicion_id',
  ];

  public function deduccion(){
    return $this->belongsTo('App\Modelos\ControlOficios\Deduccion','deduccion_id');
  }
}
