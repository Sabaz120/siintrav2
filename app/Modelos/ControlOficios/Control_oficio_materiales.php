<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_materiales extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_materiales";
  protected $fillable = [
    'nombre', 'descripcion'
  ];

	public function solicitudes(){
		return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_solicitud_material','materiales_id');
	}

}
