<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_solicitud_productos extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_solicitud_productos";
  protected $fillable = [
    'control_oficio_registro_oficio_id', 'producto_id', 'cantidad',
  ];
  public function producto(){
    return $this->belongsTo('App\Modelos\SistemaProduccionV1\Productos','producto_id');
  }
}
