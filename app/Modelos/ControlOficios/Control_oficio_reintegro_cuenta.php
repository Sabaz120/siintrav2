<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_reintegro_cuenta extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_reintegro_cuenta";
  protected $fillable = [
    'numero_cuenta', 'tipo_cuenta','banco','identificacion','nombre','correo_electronico','control_oficio_oferta_economica_id'
  ];
}
