<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivoSolicitud extends Model
{
    use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table = 'control_oficio_motivo_solicitud';
    protected $fillable = [
        'nombre', 'descripcion',
    ];

}
