<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ente extends Model
{
    use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table = 'control_oficios.control_oficio_ente';
    protected $fillable = [
        'nombre', 'descripcion', 'ponderacion', 'clasificacion_ente_id',
    ];
    public function clasificacion_ente(){
      return $this->belongsTo('App\Modelos\ControlOficios\ClasificacionEnte','clasificacion_ente_id');
    }
}
