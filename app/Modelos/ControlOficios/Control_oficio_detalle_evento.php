<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control_oficio_detalle_evento extends Model
{
    use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table="control_oficio_detalle_evento";

    public function oficio()
    {
        return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_registro_oficio');
    }
}
