<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_pago extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_pago";

  protected $fillable = [
    'control_oficio_oferta_economica_id', 'tipo_pago','referencia','monto','soporte_lote','validacion'
  ];

  protected $casts = [
      'monto' => 'float',
  ];
  protected $appends = [
      'galeria'
  ];

  public function getGaleriaAttribute()
  {

      $images = \Storage::disk('publicmedia')->files('referenciasPago/' . $this->id);
      if (count($images)) {
          return $images;
      }
      return null;
  }
}
