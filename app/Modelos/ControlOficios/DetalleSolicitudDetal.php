<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class DetalleSolicitudDetal extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table = 'control_oficio_detalle_solicitud_detal';

  protected $fillable = [
      'producto_id',
      'cantidad',
      'modalidad',
      'solicitud_detal_id',
  ];

  public function solicitudDetal(){
    return $this->belongsTo('App\Modelos\SolicitudDetal','solicitud_detal_id');
  }

  public function producto(){
    return $this->belongsTo('App\Modelos\SistemaProduccionV1\Productos','producto_id');
  }

}
