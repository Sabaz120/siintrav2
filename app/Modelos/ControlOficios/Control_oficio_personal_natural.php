<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control_oficio_personal_natural extends Model
{

  use SoftDeletes;
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_personal_natural";
  protected $fillable = [
    'cedula', 'nombre','apellido','codigo_carnet_patria','telefono','correo_electronico','firma_personal','trabajador_vtelca'
  ];

  public function relation(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico');
  }

  public function getNombreCompletoAttribute(){
    return $this->nombre." ".$this->apellido;
  }
}
