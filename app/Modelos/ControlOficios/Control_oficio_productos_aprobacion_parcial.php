<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_productos_aprobacion_parcial extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table = 'control_oficio_productos_aprobacion_parcial';
  public function producto(){
    return $this->belongsTo('App\Modelos\SistemaProduccionV1\Productos','producto_id');
  }
}
