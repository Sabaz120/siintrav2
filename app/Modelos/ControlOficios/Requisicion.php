<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Requisicion extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table = 'control_oficio_requisicion';
  protected $fillable = [
      'numero_requisicion',
      'user_id',
      'despachado',
      'oferta_economica_id',
      'tipo_venta_id',
      'seed_stock',
      'observacion_cierre'
  ];

  public function recaudos(){
    return $this->hasMany('App\Modelos\ControlOficios\RecaudosEntregados',"requisicion_id");
  }

  public function deducciones(){
    return $this->hasMany('App\Modelos\ControlOficios\DeduccionesRequisicion',"requisicion_id");
  }

  public function tipo_venta(){
    return $this->belongsTo('App\Modelos\ControlOficios\TipoVenta','tipo_venta_id');
  }

  public function despacho(){
    return $this->hasOne('App\Modelos\ControlOficios\Control_oficio_despacho','requisicion_id');
  }
}
