<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_oferta_economica extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_oferta_economicas";

  protected $fillable = [
    'control_oficio_registro_oficio_id', 'precio_total','reintegro','estado_reintegro_id','codigo_factura','precio_total_petro', 'tasa_dia_petro'
  ];

  public function detalle_oferta(){
    return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_detalle_oferta_economica');
  }
  public function pagos(){
    return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_pago');
  }
  public function datos_reintegro(){
    return $this->hasOne('App\Modelos\ControlOficios\Control_oficio_reintegro_cuenta');
  }
  public function requisicion(){
    return $this->hasOne('App\Modelos\ControlOficios\Requisicion','oferta_economica_id');
  }
  public function oficio(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_registro_oficio','control_oficio_registro_oficio_id');
  }
}
