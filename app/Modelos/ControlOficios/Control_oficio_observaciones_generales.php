<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control_oficio_observaciones_generales extends Model
{
  use SoftDeletes;

  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_observaciones_generales";

  protected $fillable = [
    'descripcion', 'posicion'
  ];


}
