<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recaudo extends Model
{
    use SoftDeletes;

    protected $connection = 'siintra_control_oficios';
    protected $table="control_oficio_recaudos";
    protected $fillable = [
        'nombre',
        'descripcion'
    ];
}
