<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control_oficio_personal_juridico extends Model
{

    use SoftDeletes;

  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_personal_juridico";

  protected $fillable = [
    'rif_cedula_situr', 'nombre_institucion','nombre','nombre_autoridad','direccion_fiscal','ente_id',
  ];
  public function ente(){
    return $this->belongsTo('App\Modelos\ControlOficios\Ente','ente_id');
  }

  public function responsables(){
    return $this->hasMany('App\Modelos\ControlOficios\Control_oficio_personal_natural_has_juridico','personal_juridico_id');
  }


}
