<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class SolicitudDetal extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table = 'control_oficio_solicitud_detal';
  protected $fillable = [
      'numero_solicitud',
      'user_id',
      'estado_aprobacion_id',
  ];
  public function estado_aprobacion(){
    return $this->belongsTo('App\Modelos\Estados','estado_aprobacion_id');
  }
  public function detalle(){
    return $this->hasMany('App\Modelos\DetalleSolicitudDetal','solicitud_detal_id');
  }

}
