<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_solicitud_material extends Model
{
	protected $connection = 'siintra_control_oficios';
    protected $table = "control_oficio_solicitud_material";
    protected $fillable = [
    	'registro_oficio_id', 'cantidad', 'materiales_id', 'despachado'
    ];

    public function material(){
    	return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_materiales','materiales_id');
    }

}
