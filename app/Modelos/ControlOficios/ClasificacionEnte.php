<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ClasificacionEnte extends Model
{
    use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table = 'control_oficios.control_oficio_clasificacion_ente';
    protected $fillable = [
        'nombre', 'descripcion',
    ];
}
