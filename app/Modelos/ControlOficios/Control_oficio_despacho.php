<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_despacho extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_despachos";
  protected $fillable = [
    'fecha_despacho', 'responsable_id','chofer_id','vehiculo_id','requisicion_id'
  ];
  // protected $fillable = [
  //   'fecha_despacho', 'responsable_id','chofer_id','vehiculo_id','registro_oficio_id'
  // ];
  public function requisicion()
  {
      return $this->belongsTo('App\Modelos\ControlOficios\Requisicion','requisicion_id');
  }
  public function detalle()
  {
      return $this->hasOne('App\Modelos\ControlOficios\Control_oficio_despacho_jornadas','despacho_id');
  }
  public function chofer(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_natural','chofer_id');
  }
  public function responsable(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_personal_natural','responsable_id');
  }
  public function vehiculo(){
    return $this->belongsTo('App\Modelos\ControlOficios\Control_oficio_vehiculo','vehiculo_id');
  }
}
