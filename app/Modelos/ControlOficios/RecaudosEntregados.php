<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class RecaudosEntregados extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_recaudos_entregados";
  protected $fillable = [
      'recaudos_id',
      'requisicion_id',
      'entregado',
  ];

  public function recaudo(){
    return $this->belongsTo('App\Modelos\ControlOficios\Recaudo','recaudos_id');
  }
}
