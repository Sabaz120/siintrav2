<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;

class Control_oficio_despacho_jornadas extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table="control_oficio_despacho_jornadas";
  protected $fillable = [
    'destino','beneficiario','despacho_id'
  ];


}
