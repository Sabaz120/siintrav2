<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NivelAtencion extends Model
{
  use SoftDeletes;
    protected $connection = 'siintra_control_oficios';
    protected $table = 'control_oficios.control_oficio_nivel_atencion';
    protected $fillable = [
        'ponderacion',
    ];
}
