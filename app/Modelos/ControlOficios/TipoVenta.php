<?php

namespace App\Modelos\ControlOficios;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoVenta extends Model
{
  use SoftDeletes;
  protected $connection = 'siintra_control_oficios';
  protected $table = 'control_oficio_tipo_venta';
  protected $fillable = [
      'nombre'
  ];
}
