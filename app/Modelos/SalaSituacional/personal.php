<?php

namespace App\Modelos\SalaSituacional;

use Illuminate\Database\Eloquent\Model;

class personal extends Model
{
    protected $primaryKey="id_codper";
  	protected $connection="sala_situacional";
  	protected $table="personal";
}

