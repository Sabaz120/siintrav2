<?php

namespace App\Modelos\Sigesp;

use Illuminate\Database\Eloquent\Model;
use DB;
class sno_personal extends Model
{
  protected $primaryKey="codper";
  protected $connection="sigesp";
  protected $table="sno_personal";

  protected function datosPersonal($cedula){
    //Retorna: nombres, cedula, cargo, area, estatus, estatus_nomina, codigo, est, est_nom
    $personal=DB::connection('sigesp')->select(DB::raw("SELECT
      (dp.nomper||' '||dp.apeper) as nombres,
      dp.nomper as nombre,
      dp.apeper as apellido,
      dp.cedper as cedula,
      ger.codger as codigo_gerencia,
      cargo.descar as cargo,
      (ger.denger||' / '||dep.dendep) as area,
      CASE
      dp.estper
      WHEN '0' THEN 'PREINGRESO'
      WHEN '1' THEN 'ACTIVO'
      WHEN '2' THEN 'NO APLICA'
      WHEN '3' THEN 'EGRESADO'
      ELSE 'N/A' END as estatus,
      CASE
      pr.staper
      WHEN '1' THEN 'ACTIVO'
      WHEN '2' THEN 'VACACIONES'
      WHEN '3' THEN 'EGRESADOS'
      WHEN '4' THEN 'SUSPENDIDO'
      ELSE 'N/A' END as estatus_nomina,
      dp.codper as codigo,
      dp.estper AS est,
      pr.staper AS est_nom
      FROM sno_personal as dp
      JOIN sno_personalnomina as pr on (dp.codper=pr.codper)
      JOIN srh_departamento AS dep ON (dep.coddep=pr.coddep)
      JOIN srh_gerencia AS ger ON (ger.codger = dep.codger)
      JOIN sno_cargo AS cargo ON (pr.codnom = cargo.codnom) and (pr.codcar = cargo.codcar)
      JOIN sno_nomina as nominas ON (pr.codnom=nominas.codnom) and (pr.codnom=nominas.codnom)
      WHERE dp.estper='1' AND pr.staper!='3' and nominas.espnom!='1' and dp.cedper='$cedula'
      GROUP BY dp.estper,dp.cedper,pr.staper,dp.codper,dp.nomper,dp.apeper,pr.codper,pr.sueper,pr.codnom,ger.denger,dep.dendep,cargo.descar, pr.codcar, nominas.desnom, nominas.espnom, nominas.peractnom, ger.codger
      ORDER BY cedula"));
      return $personal;
    }
  }
