<?php

namespace App\Modelos\Sigesp;

use Illuminate\Database\Eloquent\Model;

class sno_personalnomina extends Model
{
    protected $connection="sigesp";
    protected $table="sno_personalnomina";
}
