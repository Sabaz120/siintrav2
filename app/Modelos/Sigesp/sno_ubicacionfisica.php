<?php

namespace App\Modelos\Sigesp;

use Illuminate\Database\Eloquent\Model;

class sno_ubicacionfisica extends Model
{
  protected $connection="sigesp";
  protected $table="sno_ubicacionfisica";
}
