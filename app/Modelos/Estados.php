<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
  protected $connection = 'siintra_control_oficios';
  protected $table = 'public.estados';
  protected $fillable = [
      'nombre', 'descripcion',
  ];
}
