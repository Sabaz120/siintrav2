<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class UpatePPPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:pp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualización de PP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();

        $request_param = [
            'coins' => ['PTR'],
            'fiats' => ['BS']
        ];
        $request_data = json_encode($request_param);

        $response = $client->request('POST', 'https://petroapp-price.petro.gob.ve/price/', [
            'headers' => [
                'content-type' => 'application/json',
                'Host' => 'petroapp-price.petro.gob.ve'
            ],
            'body' => $request_data
        ]);

        $content = json_decode($response->getBody()->getContents());
        $PTR_BS = $content->data->PTR->BS;

        $articulos = DB::connection('premiumsoft')->table('articulo')->whereIn("grupo", ["005", "006"])->get();
         
        foreach($articulos as $articulo){

            $baseImponible = $articulo->cfob / 1.16;
            $factor = $PTR_BS;
            $precio1 = $baseImponible * $factor;

            $precioFin1 = $articulo->cfob * $factor;

            DB::connection('premiumsoft')->table('articulo')->where("codigo", $articulo->codigo)->update([
                "precio1" => $precio1,
                "preciofin1" => $precioFin1,
                "factor" => $factor
            ]);

        }

    }
}
