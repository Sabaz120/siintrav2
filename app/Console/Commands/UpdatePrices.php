<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class UpdatePrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza precios en petro';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $client = new \GuzzleHttp\Client();

        $request_param = [
            'coins' => ['PTR'],
            'fiats' => ['BS']
        ];
        $request_data = json_encode($request_param);

        $response = $client->request('POST', 'https://petroapp-price.petro.gob.ve/price/', [
            'headers' => [
                'content-type' => 'application/json',
                'Host' => 'petroapp-price.petro.gob.ve'
            ],
            'body' => $request_data
        ]);

        $content = json_decode($response->getBody()->getContents());
        $PTR_BS = $content->data->PTR->BS;
        
        
        DB::connection('premiumsoft')->table('monedas')->where('codigo', '002')->update([
            'ultimacotizacion' => $PTR_BS
        ]);

        $monedas = DB::connection("premiumsoft")->table("monedas")->where("codigo", "002")->orWhere("codigo")->first();
        $PTR_BS = $monedas->ultimacotizacion;

        $articulosPetro = DB::connection('premiumsoft')->table('precio_petro')->get();

        foreach($articulosPetro as $articulo){

            $precio_con_iva = $articulo->preciofin1 * $PTR_BS;
            $precio_sin_iva = $articulo->precio1 * $PTR_BS;

            $precio_con_iva2 = $articulo->preciofin2 * $PTR_BS;
            $precio_sin_iva2 = $articulo->precio2 * $PTR_BS;

            $precio_con_iva3 = $articulo->preciofin3 * $PTR_BS;
            $precio_sin_iva3 = $articulo->precio3 * $PTR_BS;

            DB::connection('premiumsoft')->table('articulo')->where('codigo', $articulo->codigo)->update(["preciofin1" => $precio_con_iva, "precio1" => $precio_sin_iva, "preciofin2" => $precio_con_iva2, "precio2" => $precio_sin_iva2, "preciofin3" => $precio_con_iva3, "precio3" => $precio_sin_iva3, "factor" => $PTR_BS]);
            Log::debug(["PTR" => $PTR_BS, "articulo" => $articulo->codigo, "preciofin1" => $articulo->preciofin1, "precio1" => $articulo->precio1, "precio_con_iva" => $precio_con_iva, "precio_sin_iva" => $precio_sin_iva]);

        }

    }
}
