<?php
use App\Modelos\ControlOficios\Control_oficio_registro_oficio;

  function obtenerComprometido($producto_id,$modalidad=null){
    $cantidadComprometida=0;
    $oficios=Control_oficio_registro_oficio::whereHas('oferta_economica.requisicion',function($query){
      $query->where('despachado',false);
    })->get();
    foreach($oficios as $oficio){
      foreach($oficio->oferta_economica->detalle_oferta as $producto){
        if($producto->producto_id == $producto_id){
          if($modalidad){
            if($producto->modalidad==$modalidad){
              $cantidadComprometida=$cantidadComprometida+$producto->cantidad;
            }
          }else
          $cantidadComprometida=$cantidadComprometida+$producto->cantidad;
          //ACA SE PODRIA AGREGAR UN BREAK
        }
      }//foreach detalle oferta economica
    }//foreach oficios
    return $cantidadComprometida;
  }//obtenerComprometido()
  function voltearFecha($fecha){
    //Reciba fecha: yyyy-mm-dd y la convierte a dd-mm-yyyy
    $fecha=explode('-',$fecha);
    return $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
  }//voltearFecha()
  function convertir_fecha_mysql($fecha){
    $fecha_new=explode('-',$fecha);
    if(strlen($fecha_new[2])==4)
    return $fecha_new[2].'-'.$fecha_new[1].'-'.$fecha_new[0];
    return $fecha;
  }//

  function convertir_fecha_mysql_a_normal($fecha){
    $fecha_new=explode('-',$fecha);
    if(strlen($fecha_new[0])==4)
    return $fecha_new[0].'-'.$fecha_new[1].'-'.$fecha_new[2];
    return $fecha;
  }

  function saveImage($value, $destination_path, $disk='publicmedia', $size = array(), $watermark = array())
  {

    $default_size = [
      'imagesize' => [
        'width' => 1024,
        'height' => 768,
        'quality'=>80
      ],
      'mediumthumbsize' => [
        'width' => 400,
        'height' => 300,
        'quality'=>80
      ],
      'smallthumbsize' => [
        'width' => 100,
        'height' => 80,
        'quality'=>80
      ],
    ];
    $size = json_decode(json_encode(array_merge($default_size, $size)));
    //Defined return.
    if (ends_with($value, '.jpg')) {
      return $value;
    }

    // if a base64 was sent, store it in the db
    if (starts_with($value, 'data:image')) {
      // 0. Make the image
      $image = \Image::make($value);
      // resize and prevent possible upsizing

      $image->resize($size->imagesize->width, $size->imagesize->height, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      });
      // 2. Store the image on disk.
      \Storage::disk($disk)->put($destination_path, $image->stream('jpg', $size->imagesize->quality));

      // 3. Return the path
      return $destination_path;
    }

    // if the image was erased
    if ($value == null) {
      // delete the image from disk
      \Storage::disk($disk)->delete($destination_path);

      // set null in the database column
      return null;
    }


  }

?>
