<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reporte_soberanos_ingresos extends Model
{
  protected $table="reporte_soberanos_ingresos";
  protected $connection="web_empleados";
  protected $fillable=[
    'cedula',
    'conceptos',
    'unidad' ,
    'monto' ,
    'tipo_ingreso',
    'mes',
    'quincena'
  ];



}
