<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="title" content="Sistema de inventario - VTELCA">
  <title>Siintra - VTELCA</title>
  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('css/util.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">

</head>
<style media="screen">
body{
  margin:0;
  color:#6a6f8c;
  background:#c8c8c8;
  background: url('http://192.168.200.93/siintra/img/fondos/bg-1.jpg')  fixed center center no-repeat;
  /* background: url(../images/fondo-pagina-web-2018.jpg)  fixed center center no-repeat; */
  background-size:cover;

  font:600 16px/18px 'Open Sans',sans-serif;
}
*,:after,:before{box-sizing:border-box}
.clearfix:after,.clearfix:before{content:'';display:table}
.clearfix:after{clear:both;display:block}
a{color:inherit;text-decoration:none}

.login-wrap{
  width:100%;
  margin:auto;
  max-width:505px;
  /* min-height:670px; */
  min-height:458px;
  position:relative;
  top:13rem;
  /* background:url(../images/1.jpg) no-repeat center; */
  box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
  left: 2%;
}
.login-html{
  width:100%;
  height:100%;
  position:absolute;
  padding:90px 70px 50px 70px;
  /* background:rgba(40,57,101,.9); */
  background: rgba(255, 255, 255, 0.8)
}
.login-html .sign-in-htm,
.login-html .sign-up-htm{
  top:0;
  left:0;
  right:0;
  bottom:0;
  position:absolute;
  transform:rotateY(180deg);
  backface-visibility:hidden;
  transition:all .4s linear;
}
.login-html .sign-in,
.login-html .sign-up,
.login-form .group .check{
  display:none;
}
.login-html .tab,
.login-form .group .label,
.login-form .group .button{
  text-transform:uppercase;
}
.login-html .tab{
  font-size:22px;
  margin-right:15px;
  padding-bottom:5px;
  margin:0 15px 10px 0;
  display:inline-block;
  border-bottom:2px solid transparent;
}
.login-html .sign-in:checked + .tab,
.login-html .sign-up:checked + .tab{
  color:#fff;
  border-color:#1161ee;
}
.login-form{
  min-height:345px;
  position:relative;
  perspective:1000px;
  transform-style:preserve-3d;
}
.login-form .group{
  margin-bottom:15px;
}
.login-form .group .label,
.login-form .group .input,
.login-form .group .button{
  width:100%;
  color:#000;
  display:block;
}
.login-form .group .input,
.login-form .group .button{
  border:none;
  padding:15px 20px;
  border-radius:25px;
  /* background:rgba(255,255,255,.1); */
  background: rgba(29, 28, 28, 0.1);
}
.login-form .group input[data-type="password"]{
  text-security:circle;
  -webkit-text-security:circle;
}
.login-form .group .label{
  color:#aaa;
  font-size:12px;
}
.login-form .group .button{
  /* background:#87B1EC; */
  background: rgba(5, 55, 84, 1);
}
.login-form .group label .icon{
  width:15px;
  height:15px;
  border-radius:2px;
  position:relative;
  display:inline-block;
  background:rgba(255,255,255,.1);
}
.login-form .group label .icon:before,
.login-form .group label .icon:after{
  content:'';
  width:10px;
  height:2px;
  background:#fff;
  position:absolute;
  transition:all .2s ease-in-out 0s;
}
.login-form .group label .icon:before{
  left:3px;
  width:5px;
  bottom:6px;
  transform:scale(0) rotate(0);
}
.login-form .group label .icon:after{
  top:6px;
  right:0;
  transform:scale(0) rotate(0);
}
.login-form .group .check:checked + label{
  color:#fff;
}
.login-form .group .check:checked + label .icon{
  background:#1161ee;
}
.login-form .group .check:checked + label .icon:before{
  transform:scale(1) rotate(45deg);
}
.login-form .group .check:checked + label .icon:after{
  transform:scale(1) rotate(-45deg);
}
.login-html .sign-in:checked + .tab + .sign-up + .tab + .login-form .sign-in-htm{
  transform:rotate(0);
}
.login-html .sign-up:checked + .tab + .login-form .sign-up-htm{
  transform:rotate(0);
}

.hr{
  height:2px;
  margin:60px 0 50px 0;
  background:rgba(255,255,255,.2);
}
.foot-lnk{
  text-align:center;
}


.pt-login {
    padding-top: 6rem !important;
}

</style>
<body>
  @guest
  <div class="login-wrap">
    <div class="login-html">
      <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab" style="display:none;">Sign In</label>
      <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab" style="display:none">Sign Up</label>
      <div class="login-form">
        <img src="http://rec.vtelca.gob.ve/img/cintillo-c.png" alt="Logo Vtelca" class="img-fluid" style="position:relative;bottom:50px;width:100%;" />
        <div class="sign-in-htm pt-login">
          <form  method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <!-- <h2 class="text-center element" style="position:relative;bottom:2rem;">Siintra</h3> -->
          <div class="group">
            <!-- <label for="user" class="label">Correo Electrónico</label> -->
            <input id="email" type="email" name="email" value="{{ old('email') }}" class="input" placeholder="Email" required autofocus>

          </div>
          <div class="group">
            <!-- <label for="pass" class="label">Contraseña</label> -->
            <input id="password" type="password" class="input" name="password" required placeholder="Contraseña">

          </div>
          <div class="group">
            <input type="submit" class="button text-white" value="Acceder"  >
          </div>
          <div class="errors mx-auto pt-3">
             <span class="focus-input100"></span>
             @if ($errors->has('email'))
             <span class="help-block">
             <strong>{{ $errors->first('email') }}</strong>
             </span>
             @endif
             <span class="focus-input100"></span>
             @if ($errors->has('password'))
             <span class="help-block">
             <strong>{{ $errors->first('password') }}</strong>
             </span>
             @endif
          </div>
        </form>
        </div>
        </div>
      </div>
    </div>
    @else

      <form class="login-html"  method="POST" action="{{ route('login') }}">
         <div class="" >
           <span class="login100-form-title p-b-34">
           <img src="http://rec.vtelca.gob.ve/img/cintillo-c.png" alt="Logo Vtelca" class="img-fluid" />
           </span>
           <div class="container-login100-form-btn">
              <div class="btn-group mx-auto">
                 <a href="{{url('home')}}" class="login100-form-btn">Ir al tablero de opciones</a>
              </div>
           </div>
         </div>
      </form>
    </div>
    @endguest
  </body>
  </html>
