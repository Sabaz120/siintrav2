@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE RECAUDOS</h5>
      <div class="row justify-content-center">
        <div class="col-12 col-md-6 col-lg-6">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Nombre:</strong>
            <input id="nombre" class="form-control" minlength="3" maxlength="255"  type="text" v-model="nombreMaterial"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese nombre del material" required >
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
          <div class="form-group text-center">
            <strong>Descripción:</strong>
            <input id="descripcion" class="form-control" minlength="3" maxlength="50"  type="text" v-model="descripcion" onkeypress="return soloLetras(event)"  pattern="[a-zA-Z]*" placeholder="Ingrese descripción" required>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
        <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
        <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show='Recaudos.length'>
          <div class="row">
            <div class="header mx-auto">
              <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE RECAUDOS</h4>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <table  class="table table-bordered text-center">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th scope="col" style="cursor:pointer; width:33%;" class="text-center" @click="sort('nombre')">Nombre</th>
                      <th scope="col" style="cursor:pointer; width:33%;" class="text-center" @click="sort('descripcion')">Descripción</th>
                      <th scope="col" class="text-center width:33%;">Acción</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr v-for="(material,index) in materiales_registrados" v-show='statusFiltro' >
                      <td class="text-center text-capitalize">@{{material.nombre}}</td>
                      <td  class="text-center text-capitalize">@{{material.descripcion}}</td>
                      <td class="text-center align-middle">
                        <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarMaterial(index)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="indexMaterial=index"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button>
                      </td>
                    </tr>
                    <tr v-show='!statusFiltro'>
                      <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Recaudos.length}}</strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">EDICIÓN DE RECAUDO</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col">
            <div class="form-group">
              <i class="fas fa-asterisk"></i>
              <label for="nombre">Nombre:</label>
              <input class="form-control" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="nombreEdit" required>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="descripcion">Descripción:</label>
              <input  class="form-control" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" placeholder="Ingrese descripción" v-model="descripcionEdit" required>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success text-white font-weight-bold" @click="editarMaterial()">Actualizar</button>
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- Eliminar -->
<div class="modal" id="myModalEliminar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title mx-auto  text-white">
          ELIMINAR RECAUDO</h4>
          <!-- <button type="button" class="close" data-dismiss="modal" >&times;</button> -->
        </div>
        <div class="modal-body">
          <div class="mx-auto text-center">
            ¿Esta seguro de eliminar este recaudo?
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminarMaterial">Sí, estoy seguro.</button>
          <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    Recaudos:{!! $recaudos ? $recaudos : "''"!!},
    indexMaterial:0,
    nombreEdit:'',
    descripcionEdit:'',
    nombreMaterial:'',
    descripcion:'',
    search:'',
    currentSort:'nombre',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0
  },
  computed:{
    materiales_registrados:function() {
      this.rows=0;
      return this.Recaudos.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    limpiar(){
      this.nombreMaterial='',
      this.descripcion='',
      this.indexMaterial=0
    },
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.Recaudos){
          if(this.Recaudos[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
            this.statusFiltro=1;
            filtrardo.push(this.Recaudos[i]);
          }//if(this.Materiales[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.MotivosSolicitud)
        if(filtrardo.length)
        this.Recaudos=filtrardo;
        else{
          this.statusFiltro=0;
          this.Recaudos={!! $recaudos ? $recaudos : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.Recaudos={!! $recaudos ? $recaudos : "''"!!};
      }//if(this.search$materiales
      },// filtrar:function()
      registrar() {
        if(this.nombreMaterial==""){
          alertify.error('Debes ingresar un nombre de recaudo');
        }else if(this.nombreMaterial.length<4){
          alertify.error('El nombre del recaudo debe tener al menos 4 caracteres.');
        }else{
          axios.post('{{ route("RegistrarRecaudos") }}', {
            nombre: this.nombreMaterial,
            descripcion: this.descripcion,
          })
          .then(response => {
            if(response.data.error==0){
              this.limpiar();
              this.Recaudos=response.data.recaudos;
              alertify.success(response.data.msg);
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
        }//else no errors
      },
      capturarMaterial(index){
        this.indexMaterial=index;
        this.nombreEdit=this.Recaudos[index].nombre;
        this.descripcionEdit=this.Recaudos[index].descripcion;
      },
      editarMaterial(){
        if(this.nombreEdit==""){
          alertify.error('Debes ingresar un nombre de recaudo');
        }else if(this.nombreEdit.length<4){
          alertify.error('El nombre del recaudo debe tener al menos 4 caracteres.');
        }else{
          axios.post('{{ route("ActualizarRecaudo") }}', {
            id:this.Recaudos[this.indexMaterial].id,
            nombre: this.nombreEdit,
            descripcion: this.descripcionEdit,
          })
          .then(response => {
            if(response.data.error==0){
              this.limpiar();
              this.Recaudos=response.data.recaudos;
              alertify.success(response.data.msg);
              $('#myModal').modal('toggle');
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
        }//else no errors
      },
      eliminarMaterial(){
        axios.post('{{ route("EliminarRecaudo") }}', {
          id:this.Recaudos[this.indexMaterial].id
        })
        .then(response => {
          if(response.data.error==0){
            this.limpiar();
            this.Recaudos=response.data.recaudos;
            alertify.success(response.data.msg);
            $('#myModalEliminar').modal('toggle');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      },//eliminarMaterial
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.Recaudos.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      }
    },//methods
  });//const app= new Vue
</script>
@endpush
