<div class="modal" id="myModal" v-if="Oficios.length>0">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">PREVISUALIZAR COTIZACIÓN</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row" v-if="!Oficios[indexOficio].oferta_economica">
          <div class="container-fluid table-responsive">
            <table  class="table table-bordered text-center">
              <thead class="bg-primary text-white">
                <tr>
                  <th scope="col" class="text-center" >Descripción</th>
                  <th scope="col" class="text-center" >Modelo</th>
                  <th scope="col" class="text-center" >Modalidad</th>
                  <th scope="col" class="text-center" style="width: 90px;">Cantidad</th>
                  <th scope="col" class="text-center" >Costo Unitario</th>
                  <th scope="col" class="text-center" >Costo en Petro</th>
                  <th scope="col" class="text-center" >Costo Total</th>
                  {{--
                  <th scope="col" class="text-center" v-if="Oficios[indexOficio].oferta_economica!=null">Acción</th>
                  --}}
                </tr>
              </thead>
              <tbody >
                <tr v-for="(producto,index) in Oficios[indexOficio].productos">
                  <td class="text-center text-capitalize">@{{producto.descripcion}}</td>
                  <td class="text-center text-capitalize">@{{producto.modelo}}</td> <!-- producto.modelo -->
                  <td class="text-center text-capitalize">@{{producto.modalidad}}</td>
                  <td class="text-center text-capitalize"><input type="number" class="form-control" v-model="producto.cantidad"></td>
                  {{--<td class="text-center text-capitalize"><input type="number" class="form-control" v-bind:max="producto.cantidad_max" v-model="producto.cantidad"></td>--}}
                  <td class="text-center text-capitalize">
                    <span v-if="'precio_desactualizado' in producto && producto.precio_desactualizado==1" class="badge badge-danger">Precio desactualizado,<br>selecciona el nivel de precio y actualiza.</span>
                    <select class="form-control" v-model="producto.nivel_precio" v-on:change="cambiarPrecio(index)">
                      <!-- <option value="0">Seleccione Precio</option> -->
                      <option v-bind:value="1" v-bind:title="formatearNumero(producto.precios.precio1p)+' Pt'" v-bind:alt="formatearNumero(producto.precios.precio1p)+' Pt'" data-toggle="tooltip" data-placement="right">P1-@{{formatearNumero(producto.precios.precio1)}}</option>
                      <option v-bind:value="2" v-bind:title="formatearNumero(producto.precios.precio2p)+' Pt'" v-bind:alt="formatearNumero(producto.precios.precio2p)+' Pt'" data-toggle="tooltip" data-placement="right">P2-@{{formatearNumero(producto.precios.precio2)}}</option>
                      <option v-bind:value="3" v-bind:title="formatearNumero(producto.precios.precio3p)+' Pt'" v-bind:alt="formatearNumero(producto.precios.precio3p)+' Pt'" data-toggle="tooltip" data-placement="right">P3-@{{formatearNumero(producto.precios.precio3)}}</option>
                    </select>
                  </td>
                  <td class="text-center text-capitalize">Pt @{{formatearNumero(parseFloat((producto.preciop)*producto.cantidad).toFixed(4))}}</td>
                  <td class="text-center text-capitalize">Bs @{{formatearNumero(parseFloat(producto.precio*producto.cantidad).toFixed(2))}}</td>
                  {{--
                  <td class="text-center align-middle" v-if="Oficios[indexOficio].oferta_economica!=null">
                      <button type="button" class="btn btn-success text-white text-center" title="Actualizar" @click="guardarCambiosProducto(index)"><i class="fa fa-save fa-1x" aria-hidden="true"></i></button>
                  </td>
                  --}}
                </tr>
                <tr>
                  {{--
                  <td colspan="6" v-if="Oficios[indexOficio].oferta_economica!=null" class="text-right"><strong>Sub-Total</strong></td>
                  --}}
                  <td colspan="6"  class="text-right"><strong>Sub-Total</strong></td>
                  {{--<td class="text-center"><strong>@{{formatearNumero(calcular_subtotal_petro)}} Pt</strong></td> --}}
                  <td class="text-center"><strong>@{{formatearNumero(calcular_subtotal)}} Bs</strong></td>
                </tr>
                <tr>
                  {{--
                  <td colspan="6" v-if="Oficios[indexOficio].oferta_economica!=null" class="text-right"><strong>I.V.A @{{iva}} %</strong></td>
                  --}}
                  <td colspan="6"  class="text-right"><strong>I.V.A @{{iva}} %</strong></td>
                  {{--<td class="text-center"><strong>@{{formatearNumero(((calcular_subtotal_petro*(iva/100)).toFixed(4)))}} Pt</strong></td>--}}
                  <td class="text-center"><strong>@{{formatearNumero(((calcular_subtotal*(iva/100)).toFixed(2)))}} Bs</strong></td>
                </tr>
                <tr>
                  {{--
                  <td colspan="6" v-if="Oficios[indexOficio].oferta_economica!=null" class="text-right"><strong>Total</strong></td>
                  --}}
                  <td colspan="6"  class="text-right"><strong>Total</strong></td>
                  {{--<td class="text-center"><strong>@{{formatearNumero(calcular_total_petro)}} Pt</strong></td> --}}
                  <td class="text-center"><strong>@{{formatearNumero(calcular_total)}} BS</strong></td>
                </tr>
                <tr>
                  <td colspan="6"  class="text-right"><strong>Total en Petro</strong></td>
                  <td class="text-center"><strong>@{{formatearNumero(calcular_total_petro)}} Pt</strong></td>
                </tr>
            </tbody>
          </table>
          </div>
        </div>

        <div class="row" v-else>
          <div class="container-fluid table-responsive">
            <table  class="table table-bordered text-center">
              <thead class="bg-primary text-white">
                <tr>
                  <th scope="col" class="text-center" >Modelo</th>
                  <th scope="col" class="text-center" >Modalidad</th>
                  <th scope="col" class="text-center" style="width: 90px;">Cantidad Almacen</th>
                  <th scope="col" class="text-center" style="width: 90px;">Cantidad</th>
                  <th scope="col" class="text-center" >Costo Unitario</th>
                  <th scope="col" class="text-center" >Costo en Petro</th>
                  <th scope="col" class="text-center" >Costo Total</th>
                  <th scope="col" class="text-center" >Acción</th>
                </tr>
              </thead>
              <tbody >
                <tr v-for="(producto,index) in Oficios[indexOficio].productos">
                  <td class="text-center text-capitalize">
                    <select class="form-control" v-model="producto.producto_id" @change="onChangeProduct(index)">
                      <option :value="product.id" v-for="product in productos">@{{product.descripcion}}</option>
                    </select>
                  </td> <!-- producto.modelo -->
                  <td class="text-center text-capitalize">
                    <select class="form-control" v-model="producto.modalidad" @change="onChangeModalidad(index)">
                      <option v-for="mod in productosSistProd[string_to_slug(producto.modelo)]" :value="mod.modalidad" >@{{mod.modalidad}}</option>
                    </select>
                  </td>
                  <td class="text-center text-capitalize">@{{producto.cantidad_almacen}}</td>
                  <td class="text-center text-capitalize"><input type="number" class="form-control" v-model="producto.cantidad" :max="producto.cantidad_almacen"></td>
                  <td class="text-center text-capitalize">
                    <span v-if="'precio_desactualizado' in producto && producto.precio_desactualizado==1" class="badge badge-danger">Precio desactualizado,<br>selecciona el nivel de precio y actualiza.</span>
                    <select class="form-control" v-model="producto.nivel_precio">
                      <option :value="indexP" v-for="(precio,indexP) in preciosPremium[string_to_slug(producto.modelo)+string_to_slug(producto.modalidad)]" v-bind:title="preciosPremium[string_to_slug(producto.modelo)+string_to_slug(producto.modalidad)][(parseInt(indexP)+parseInt(3))]+' Pt'" v-if="indexP<4" >@{{formatearNumero(precio)}}</option>
                      <!-- <option value="1">P1-@{{formatearNumero(producto.precios.precio1)}}</option> -->
                      <!-- <option value="2">P2-@{{formatearNumero(producto.precios.precio2)}}</option> -->
                      <!-- <option value="3">P3-@{{formatearNumero(producto.precios.precio3)}}</option> -->
                    </select>
                  </td>
                  <td class="text-center text-capitalize">Pt @{{formatearNumero(parseFloat(preciosPremium[string_to_slug(producto.modelo)+string_to_slug(producto.modalidad)][(parseInt(producto.nivel_precio)+parseInt(3))]*producto.cantidad).toFixed(4))}}</td>
                  <td class="text-center text-capitalize">Bs @{{formatearNumero(parseFloat(preciosPremium[string_to_slug(producto.modelo)+string_to_slug(producto.modalidad)][producto.nivel_precio]*producto.cantidad).toFixed(2))}}</td>
                  <td>
                    <button type="button" v-show="!borrandoProducto && !Oficios[indexOficio].oferta_economica.requisicion" @click="borrarProducto(index)" class="btn btn-danger" name="button">
                      <i class="fa fa-trash"></i>
                    </button>
                    <button type="button" v-show="borrandoProducto" class="btn btn-danger text-white font-weight-bold rounded-0 border-r-g" name="button">
                      <i class="fa fa-spinner fa-pulse"></i> Borrando producto..
                    </button>
                  </td>
                </tr>

                <tr class="text-center">
                  <td colspan="8" style="text-align:center;">
                    <button type="button" class="btn btn-success" v-show="!agregandoProducto && !Oficios[indexOficio].oferta_economica.requisicion" @click="agregarNuevoProducto()" name="button">Agregar</button>
                    <button type="button" class="btn btn-success text-white font-weight-bold rounded-0 border-r-g" v-show="agregandoProducto" name="button">
                      <i class="fa fa-spinner fa-pulse"></i> Agregando producto..
                    </button>
                  </td>
                </tr>

                <tr>
                  <td class="text-center"><strong>Sub-Total</strong></td>
                  <td class="text-center"><strong>@{{formatearNumero(calcular_subtotal)}} Bs</strong></td>
                  <td class="text-center"><strong>I.V.A @{{iva}} %</strong></td>
                  <td class="text-center"><strong>@{{formatearNumero(((calcular_subtotal*(iva/100)).toFixed(2)))}} Bs</strong></td>
                  <td class="text-center"><strong>Total</strong></td>
                  <td colspan="3" class="text-center"><strong>@{{formatearNumero(calcular_total)}} BS</strong></td>
                </tr>
                <tr>
                  <td colspan="5"class="text-right"><strong>Total en Petro</strong></td>
                  <td colspan="3" class="text-center"><strong>@{{formatearNumero(calcular_total_petro)}} Pt</strong></td>
                </tr>

            </tbody>
          </table>
          </div>
        </div>
        <!-- DIV V-ELSE ACTUALIZAR COTIZACION -->

      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto">
        <button type="button" v-if="Oficios[indexOficio].oferta_economica==null" v-show="!generandoOferta" class="btn btn-success text-white font-weight-bold" @click="GenerarOfertaEconomica()">Guardar</button>
        <button type="button" v-if="Oficios[indexOficio].oferta_economica==null" class="btn btn-success text-white font-weight-bold rounded-0 border-r-g" v-show="generandoOferta">
          <i class="fa fa-spinner fa-pulse"></i> Generando Cotización..
        </button>
        <button type="button" v-if="Oficios[indexOficio].oferta_economica && !Oficios[indexOficio].oferta_economica.requisicion" v-show="!actualizandoOferta" class="btn btn-success text-white font-weight-bold" @click="ActualizarOfertaEconomica()">Actualizar</button>
        <button type="button" v-if="Oficios[indexOficio].oferta_economica" v-show="actualizandoOferta" class="btn btn-success text-white font-weight-bold rounded-0 border-r-g" >
          <i class="fa fa-spinner fa-pulse"></i> Actualizando Cotización..
        </button>
        <!-- <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button> -->
      </div>
    </div>
  </div>
</div>
