<div class="modal" id="masSoportes" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">AGREGAR MAS SOPORTES DE PAGO</h4>
        <button type="button" class="close" data-dismiss="modal" @click="agregarMasSoportes">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12" >
            <div class="form-group text-center">
              <label class="font-weight-bold">Agregar referencia de pago adicional</label>
              <input class="form-control" name="referenciaAdicionalPago" @change="convertirBase64PagoAdicional" type="file" accept="image/jpeg">
            </div>
            <div class="form-group text-center col-12 row" v-if="pago_index!=null && Oficios[indexOficio].oferta_economica_pagos[pago_index].galeria">
              <div class="col-md-12">
                <hr>
                <h4 style="text-align:center;" class="font-weight-bold">Anexos</h4>
              </div>
              <div class="col-md-4 text-center" v-for="anexo in Oficios[indexOficio].oferta_economica_pagos[pago_index].galeria">
                <a :href="anexo" download class="btn btn-success">
                  <img class="img-fluid" style="height:250px;width:250px;" :src="anexo" style="height:300px;width:auto;">
                </a>
              </div>
{{--
              <div class="owl-carousel text-center">
                <div v-for="anexo in Oficios[indexOficio].oferta_economica_pagos[pago_index].galeria">
                  <a :href="anexo" download class="btn btn-success">
                    <img style="height:250px;width:250px;" :src="anexo" style="height:300px;width:auto;">
                  </a>
                 </div>
              </div>
              --}}

            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">
        <button type="button" class="btn btn-success" @click="agregarPagoAdicional" name="button">Agregar Imagen</button>

      </div>
    </div>
  </div>
</div>
