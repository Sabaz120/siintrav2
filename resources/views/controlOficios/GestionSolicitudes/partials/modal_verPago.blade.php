<div class="modal modalPago" id="verPago" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg modal-verPago">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">DETALLES DEL PAGO</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row text-center" v-if="pago_index!=null && pago_editar==false">
          <div class="col-md-6 mb-4">
            <strong>
              <i class="fa fa-asterisk"></i>
              Tipo de Pago:
            </strong>
            <select class="form-control" disabled v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago">
              <option value="0">Seleccione el tipo de pago</option>
              <option value="transferencia">Transferencia</option>
              <option value="punto_venta">Punto de Venta</option>
            </select>
          </div>
          <div class="col-md-6 mb-4" v-show="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='punto_venta'">
            <strong >
              <i class="fa fa-asterisk"></i>
              Número de Lote:
            </strong>
            <input type="text" @keypress="onlyNumber" :maxlength="10" v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" disabled class="form-control" >
          </div>
          <div class="col-md-6 mb-4">
            <strong>
              <i class="fa fa-asterisk"></i>
              Referencia de Pago:
            </strong>
            <input type="text" @keypress="onlyNumberWithOutDecimalPoint" :maxlength="12" disabled v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].referencia" class="form-control" >
          </div>
          <div class="col-md-6 mb-4">
            <strong>
              <i class="fa fa-asterisk"></i>
              Monto:
            </strong>
            <!-- <money v-model="monto_pago" class="form-control"></money> -->
            <input type="text" @keypress="onlyNumber" v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].monto" disabled :maxlength="15" class="form-control" >
          </div>
          <div class="col-md-6" v-show="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='transferencia'">
            <strong >
              <i class="fa fa-asterisk"></i>
              Soporte de Pago:
            </strong>
            <br>
            <a class="btn btn-success" v-if="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote!=null" download="soporte.jpg" :href="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" title="Soporte">
              <!-- <img  :src="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" class="img-fluid img-responsive"> -->
              Descargar soporte
            </a>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">

        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal" @click="limpiar()">Cerrar</button>
      </div>
    </div>
  </div>
</div>
