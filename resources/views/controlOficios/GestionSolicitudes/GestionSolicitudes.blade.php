@extends('layouts.app')
@section('contenido')

<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">Gestión de Solicitudes</h5>
      <div class="row justify-content-center">
        <div class="col-12 col-md-3 col-lg-3" >
          <div class="form-group text-center">
            <strong>Tipo de solicitante:</strong>
            <select class="form-control" v-model="tipo_solicitante">
              <option value="0">Seleccione un tipo de solicitante</option>
              <option value="1">Personal Natural</option>
              <option value="2">Personal Jurídico</option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3" v-if="tipo_solicitante==2">
          <div class="form-group text-center">
            <strong>Clasificación de Ente:</strong>
            <select class="form-control" v-on:change="cargarEntes()"  v-model="clasificacion_ente_id">
              <option value="0">Seleccione una Clasificación</option>
              <option v-for="clasificacion in clasificacion_entes" v-bind:value="clasificacion.id">
                @{{ clasificacion.nombre }}
              </option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3" v-if="tipo_solicitante==2 && clasificacion_ente_id!=0 && entes.length>0">
          <div class="form-group text-center">
            <strong>Ente:</strong>
            <select class="form-control" v-model="ente_id">
              <option value="0">Seleccione un Ente</option>
              <option v-for="ente in entes" v-bind:value="ente.id">
                @{{ ente.nombre }}
              </option>
            </select>
          </div>
        </div>


        <div class="col-12 col-md-2 col-lg-2">
          <div class="form-group text-center">
            <strong>Estado:</strong>
            <select class="form-control" v-model="estado_id">
              <option value="0">Seleccione un Estado</option>
              <option v-for="estado in estados" v-bind:value="estado.idEstado">
                @{{ estado.estado }}
              </option>
            </select>
          </div>
        </div>

        <div class="col-12 col-md-2 col-lg-2">
          <div class="form-group text-center">
            <strong>Fecha inicio:</strong>
            <input type="date" v-model="fechaInicio" class="form-control">
          </div>
        </div>

        <div class="col-12 col-md-2 col-lg-2">
          <div class="form-group text-center">
            <strong>Fecha Fin:</strong>
            <input type="date" v-model="fechaFin" class="form-control">
          </div>
        </div>

        <div class="col-12 col-md-1 col-lg-1">
          <div class="form-group text-center align-center">
            <br>
            <button type="button" class="btn btn-info" name="button" @click="buscarOficios()">Buscar</button>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div v-if="!Oficios.length" class="text-center">
          <hr>
          <h4>
            <p>Sin oficios que mostrar</p>
          </h4>
        </div>
        <div id="tabla" v-else-if="Oficios.length>0">
          <div class="row">
            <div class="header mx-auto">
              <!-- <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE OFICIOS</h4> -->
            </div>
          </div>
          <div class="row">
            <div class="container-fluid table-responsive">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <table  class="table table-bordered text-center">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th scope="col" class="text-center" @click="sort('id')">N° de oficio</th>
                      <th scope="col" class="text-center" @click="sort('num_oferta')">N° de Oferta E.</th>
                      <th scope="col" class="text-center" @click="sort('fecha_solicitud')">Fecha de Solicitud</th>
                      <th scope="col" class="text-center" @click="sort('fecha_aprobacion')">Fecha de Aprobación</th>
                      <th scope="col" class="text-center" @click="sort('solicitante_ente')">Solicitante</th>
                      <th>Agente de Retención</th>
                      <th scope="col" class="text-center" @click="sort('modelos')">Modelos solicitados</th>
                      <th scope="col" class="text-center" @click="sort('cantidad_solicitada')">Cantidad Total Solicitada</th>
                      <th scope="col"  class="text-center">Acción</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr v-for="(oficio,index) in listado_oficios" v-show='statusFiltro' >
                      <td class="text-center text-capitalize">@{{oficio.id}}</td>
                      <td class="text-center text-capitalize">@{{oficio.num_oferta}}</td>
                      <td  class="text-center text-capitalize">@{{oficio.fecha_solicitud}}</td>
                      <td  class="text-center text-capitalize">@{{oficio.fecha_aprobacion}}</td>
                      <td  class="text-center text-capitalize">@{{oficio.solicitante_ente}}</td>
                      <!-- <td  class="text-center text-capitalize" >@{{oficio.solicitante_ente_ente}}  @{{ oficio.solicitante_ente_nombre }}</td> -->
                      <td  class="text-center text-capitalize" v-if="oficio.personal_juridico && oficio.personal_juridico.agente_retencion === true">Si</td>
                      <td  class="text-center text-capitalize" v-else>No</td>

                      <td  class="text-center text-capitalize">@{{oficio.modelos}}</td>
                      <td  class="text-center text-capitalize">@{{oficio.cantidad_solicitada}}</td>
                      <td class="text-center">
                        <div class="form-inline">
                          @if(!Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_solicitudes_validar_pago'))
                          <button v-if="!oficio.oferta_economica.requisicion" type="button" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Previsualizar cotización" @click="capturarOficio(oficio.id)"><i class="fa fa-handshake" aria-hidden="true"></i></button>
                          <form action="{{route('generarPDFOferta')}}" method="post" target="_balnk">
                            {{ csrf_field() }}
                            <input type="hidden" name="oficio_id" v-model="oficio.id">
                            <button v-if="oficio.oferta_economica!=null" type="submit" name="button" class="btn btn-primary text-white mr-2" title="Generar PDF de Cotización"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                          </form>
                          <form action="{{route('generarPDFOfertaPetro')}}" method="post" target="_balnk">
                            {{ csrf_field() }}
                            <input type="hidden" name="oficio_id" v-model="oficio.id">
                            <button v-if="oficio.oferta_economica!=null" type="submit" name="button" class="btn btn-warning text-white mr-2" title="Generar PDF de Cotización en  Petros"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                          </form>
                          <form action="{{route('generarPDFOfertaHibrido')}}" method="post" target="_balnk">
                            {{ csrf_field() }}
                            <input type="hidden" name="oficio_id" v-model="oficio.id">
                            <button v-if="oficio.oferta_economica!=null" type="submit" name="button" class="btn btn-warning text-white mr-2" title="Generar PDF de Cotización en  Petros"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                          </form>
                          <button type="button" v-if="oficio.oferta_economica!=null && oficio.verificacion_pago==true && oficio.personal_juridico && oficio.personal_juridico.agente_retencion === false" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#asociarPago" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Asociar pagos" @click="capturarOficio(oficio.id)"><i class="fa fa-money-bill-wave" aria-hidden="true"></i></button>
                          <!-- <button type="button" v-if="oficio.oferta_economica!=null" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#asignarChofer" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Asignar chofer" @click="capturarOficio(oficio.id)"><i class="fa fa-car" aria-hidden="true"></i></button> -->
                          <button type="button" v-if="oficio.oferta_economica!=null" class="btn btn-danger text-white text-center mr-2" data-toggle="modal" data-target="#denegarCompra" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Denegar compra" @click="capturarOficio(oficio.id,'pagosAprobados')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                          @endif
                          @can('siintra_menu_control_oficios_gestion_solicitudes_validar_pago')
                          <!-- VALIDAR PAGO PERMISO -->
                          <button type="button" v-if="oficio.oferta_economica!=null" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#validarPago" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Validar pagos" @click="capturarOficio(oficio.id,'cargaCarousel');"><i class="fa fa-money-check" aria-hidden="true"></i></button>
                          <!-- VALIDAR PAGO PERMISO -->
                          @endcan
                        </div>
                        <!-- <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="indexMaterial=index"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button> -->
                      </td>
                    </tr>
                    <tr v-show='!statusFiltro'>
                      <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Oficios.length}}</strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('controlOficios.GestionSolicitudes.partials.modal_oferta_economica')
  @include('controlOficios.GestionSolicitudes.partials.modal_asociarPago')
  @include('controlOficios.GestionSolicitudes.partials.modal_soportesAdicionalesPagos')
  @include('controlOficios.GestionSolicitudes.partials.modal_verPago')
  @include('controlOficios.GestionSolicitudes.partials.modal_validar_pago')
  @include('controlOficios.GestionSolicitudes.partials.modal_asignar_chofer')
  @include('controlOficios.GestionSolicitudes.partials.modal_denegar_compra')
</section>
@endsection
@push('scripts')
<script>
Vue.use(VueHZoom);
// $(".owl-carousel").owlCarousel({
//   loop:true,
//   margin:10
// });
const app= new Vue({
  el:'#register',
  data:{
    Oficios:{!! $oficios ? $oficios : "''"!!},
    estados:{!! $estados ? $estados : "''"!!},
    clasificacion_entes:{!! $clasificacion_entes ? $clasificacion_entes : "''"!!},
    productosSistProd:{!! $productosSistProd ? json_encode($productosSistProd) : "''"!!},
    productos:{!! $productos ? json_encode($productos) : "''"!!},
    preciosPremium:{!! $preciosPremium ? json_encode($preciosPremium) : "''"!!},
    indexProducto:0,
    entes:[],
    /* Filtros de búsqueda*/
    tipo_solicitante:0,
    clasificacion_ente_id:0,
    ente_id:0,
    estado_id:0,
    /* Filtros de búsqueda*/
    /* Variables modal asociar pago*/
    tipo_pago:0,
    referencia_pago:'',
    monto_pago:0,
    soporte_pago:'',
    pago_id:0,//Id del pago a editar
    pago_index:null,
    pago_editar:false,
    /* Variables modal asociar pago*/
    indexOficio:0,
    iva:16,
    total:0,
    total_petro:0,
    search:'',
    currentSort:'id',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0,
    /* Variables modal chofer*/
    cedula_chofer:'',
    cedula_responsable:'',
    vehiculo_placa:'',
    vehiculo_modelo:'',
    chofer_nombres_apellidos:'',
    chofer_numero:'',
    responsable_nombres_apellidos:'',
    responsable_numero:'',
    destino_despacho:'',
    beneficiarios:'',//base 64 pdf
    /* Variables modal chofer*/
    /* Variables modal denegar compra*/
    reintegro_num_cuenta:'',
    reintegro_nombre:'',
    reintegro_cedula:'',
    reintegro_rif:'',
    reintegro_banco:0,
    reintegro_correo:'',
    reintegro_tipo_cuenta:1,
    reintegro_reintegrar:0,
    /* Variables modal denegar compra*/
    generandoOferta:false,
    actualizandoOferta:false,
    agregandoProducto:false,
    borrandoProducto:false,
    soporteAdicional:'',
    fechaInicio:'',
    fechaFin:'',
  },
  methods:{
    agregarNuevoProducto(){

      this.agregandoProducto=true;
      var producto=this.productosSistProd.l111[1];
      axios.post('{{ route("agregarProductoOfertaEconomica") }}', {
        control_oficio_oferta_economica_id:this.Oficios[this.indexOficio].oferta_economica.id,
        cantidad:1,
        producto_id:producto.id,
        modalidad:producto.modalidad,
        nombre:producto.nombre,
        nivel_precio:1
      })
      .then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          //Aca añade el producto
          this.Oficios[this.indexOficio].productos.push(response.data.data);
          var lengthProducts=this.Oficios[this.indexOficio].productos.length;
          var cantidadEquipos=0;
          var modelos="";
          for(var i=0;i<lengthProducts;i++){
            if(i==0)
            modelos=this.Oficios[this.indexOficio].productos[i].descripcion;
            else
            modelos=modelos+", "+this.Oficios[this.indexOficio].productos[i].descripcion;
            cantidadEquipos=cantidadEquipos+parseInt(this.Oficios[this.indexOficio].productos[i].cantidad);
          }//for
          this.Oficios[this.indexOficio].modelos=modelos;
          this.Oficios[this.indexOficio].cantidad_solicitada=cantidadEquipos;
        }//if(response.data.status=="success")
        else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
        this.agregandoProducto=false;

      })
      .catch(function (error) {
        console.log(error);
      });
    },
    borrarProducto(indexProducto){
      var lengthProducts=this.Oficios[this.indexOficio].productos.length;
      if(lengthProducts>1){
        this.borrandoProducto=true;
        axios.post('{{ route("eliminarProductoOfertaEconomica") }}', {
          detalle_oferta_economica_id:this.Oficios[this.indexOficio].productos[indexProducto].detalle_oferta_economica_id
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Oficios[this.indexOficio].productos.splice(indexProducto,1);
            var cantidadEquipos=0;
            var modelos="";
            for(var i=0;i<this.Oficios[this.indexOficio].productos.length;i++){
              if(i==0)
              modelos=this.Oficios[this.indexOficio].productos[i].modelo;
              else
              modelos=modelos+", "+this.Oficios[this.indexOficio].productos[i].modelo;
              cantidadEquipos=cantidadEquipos+parseInt(this.Oficios[this.indexOficio].productos[i].cantidad);
            }//for
            this.Oficios[this.indexOficio].modelos=modelos;
            this.Oficios[this.indexOficio].cantidad_solicitada=cantidadEquipos;
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
          this.borrandoProducto=false;
        })
        .catch(function (error) {
          console.log(error);
        });
      }else{
        alertify.error("Debes tener al menos un producto en la cotización.");
      }
    },
    onChangeProduct(indexProducto){
      var producto=this.Oficios[this.indexOficio].productos[indexProducto];
      for(var i=0;i<this.productos.length;i++){
        if(this.productos[i].id==producto.producto_id){
          this.Oficios[this.indexOficio].productos[indexProducto].modelo=this.productos[i].nombre;
          this.Oficios[this.indexOficio].productos[indexProducto].descripcion=this.productos[i].descripcion;
          break;
        }
      }
      var modalidades=this.productosSistProd[this.string_to_slug(this.Oficios[this.indexOficio].productos[indexProducto].modelo)];
      this.Oficios[this.indexOficio].productos[indexProducto].modalidad=modalidades[0].modalidad;
      this.Oficios[this.indexOficio].productos[indexProducto].cantidad_almacen=modalidades[0].disponibilidad;
    },
    onChangeModalidad(indexProducto){
      /*
      this.indexOficio
      indexProducto
      */
      var producto=this.Oficios[this.indexOficio].productos[indexProducto];
      var modalidades=this.productosSistProd[this.string_to_slug(this.Oficios[this.indexOficio].productos[indexProducto].modelo)];
      var disponibilidad=0;
      for(var i=0;i<modalidades.length;i++){
        if(modalidades[i].modalidad==producto.modalidad){
          disponibilidad=modalidades[i].disponibilidad;
          break;
        }//if
      }//for
      this.Oficios[this.indexOficio].productos[indexProducto].cantidad_almacen=disponibilidad;
    },
    string_to_slug (str) {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();

      // remove accents, swap ñ for n, etc
      var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
      var to   = "aaaaeeeeiiiioooouuuunc------";
      for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-'); // collapse dashes

      return str;
    },
    installOwlCarousel: function(){
      $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        items: 4,
        autoplay: true,
        autoplayHoverPause: true,
      })
    },
    installOwlCarousel2: function(){
      $('.owl-carousel2').owlCarousel({
        loop:true,
        margin:10,
        items: 4,
        autoplay: true,
        autoplayHoverPause: true,
      })
    },
    limpiar(){
      this.tipo_solicitante=0,
      this.clasificacion_ente_id=0,
      this.ente_id=0,
      this.estado_id=0,
      this.tipo_pago=0,
      this.referencia_pago='',
      this.monto_pago='',
      this.soporte_pago='',
      this.pago_id=0,
      this.pago_index=null,
      this.pago_editar=false,
      this.cedula_chofer='',
      this.cedula_responsable='',
      this.chofer_nombres_apellidos='',
      this.chofer_numero,
      this.responsable_nombres_apellidos='',
      this.responsable_numero='',
      this.vehiculo_placa='',
      this.vehiculo_modelo='',
      this.destino_despacho='',
      this.beneficiarios='',
      this.reintegro_num_cuenta='',
      this.reintegro_nombre='',
      this.reintegro_cedula='',
      this.reintegro_rif='',
      this.reintegro_banco='',
      this.reintegro_correo='',
      this.reintegro_tipo_cuenta=1,
      this.reintegro_reintegrar=0,
      this.soporteAdicional=''
    },
    agregarMasSoportes(){

      $('#asociarPago').modal('toggle');
      var vm = this;
      Vue.nextTick(function(){
        setTimeout(function(){
          vm.installOwlCarousel();
        }, 1000);
      }.bind(vm));
    },
    agregarPagoAdicional(){
      /*
      Variables:
      Pago id: this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].id
      Imagen: this.soporteAdicional
      */
      var pago_id=this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].id;
      if(this.soporteAdicional && this.soporteAdicional!=''){
        axios.post('{{ route("asociarReferenciaAdicionalPago") }}', {
          pago_id:pago_id,
          imagen:this.soporteAdicional,
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            // this.limpiar();
            this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].galeria=response.data.imagenes;
            $('#masSoportes').modal('toggle');
            $('#asociarPago').modal('toggle');
            this.soporteAdicional=null;
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }else
      alertify.error('Debes cargar una imagen.');
    },
    denegarCompra(){
      var tipo_personal='natural';
      var identificacion=this.reintegro_cedula;
      if(this.Oficios[this.indexOficio].personal_juridico!=null){
        tipo_personal='juridico';
        identificacion=this.reintegro_rif;
      }
      axios.post('{{ route("denegarCompra") }}', {
        oficio_id:this.Oficios[this.indexOficio].id,
        oferta_economica_id:this.Oficios[this.indexOficio].oferta_economica.id,
        reintegro:this.reintegro_reintegrar,
        num_cuenta:this.reintegro_num_cuenta,
        nombre:this.reintegro_nombre,
        identificacion:identificacion,
        banco:this.reintegro_banco,
        correo:this.reintegro_correo,
        tipo_cuenta:this.reintegro_tipo_cuenta,
        tipo_personal:tipo_personal
      }).then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          $('#denegarCompra').modal('toggle');
          if(this.Oficios.length<2){
            setTimeout(function(){ location.reload(); }, 2500);
          }else{
            this.Oficios.splice(this.indexOficio,1);
          }
          // this.Oficios[this.indexOficio].oferta_economica_pagos[indexPago].validacion=true;
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        // this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    revertirPago(indexPago){
      axios.post('{{ route("revertirValidacionPago") }}', {
        pago_id:this.Oficios[this.indexOficio].oferta_economica_pagos[indexPago].id
      }).then(response => {
        alertify.success(response.data.msg);
        $('#validarPago').modal('toggle');
          this.Oficios[this.indexOficio].oferta_economica_pagos[indexPago].validacion=false;
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    confirmarPago(indexPago){
      // console.log(indexPago);
      axios.post('{{ route("confirmarPago") }}', {
        pago_id:this.Oficios[this.indexOficio].oferta_economica_pagos[indexPago].id
      }).then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          this.Oficios[this.indexOficio].oferta_economica_pagos[indexPago].validacion=true;
          if(response.data.pago_completado){
            if(this.Oficios.length<2){
              setTimeout(function(){ location.reload(); }, 2500);
            }else{
              // this.Oficios.splice(this.indexOficio,1);
            }
            $('#validarPago').modal('toggle');
          }
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    registrarChofer(){
      var b=0;
      if(this.cedula_chofer==""){
        b=1;
        alertify.error('Debe ingresar una cédula de chofer');
      }else if(this.destino_despacho==""){
        b=1;
        alertify.error('Debe ingresar el destino del despacho');
      }else if(this.vehiculo_placa==""){
        b=1;
        alertify.error('Debe ingresar una placa de vehículo');
      }else if(this.cedula_responsable==""){
        b=1;
        alertify.error('Debe ingresar una cédula de responsable');
      }else if(this.beneficiarios==""){
        b=1;
        alertify.error('Debe adjuntar un pdf de beneficiarios');
      }
      if(b==0){
        //no errors
        axios.post('{{ route("registrarChofer") }}', {
          cedula_chofer:this.cedula_chofer,
          cedula_responsable:this.cedula_responsable,
          chofer_nombres_apellidos:this.chofer_nombres_apellidos,
          chofer_numero:this.chofer_numero,
          responsable_nombres_apellidos:this.responsable_nombres_apellidos,
          responsable_numero:this.responsable_numero,
          vehiculo_placa:this.vehiculo_placa,
          vehiculo_modelo:this.vehiculo_modelo,
          destino_despacho:this.destino_despacho,
          beneficiarios:this.beneficiarios,
          oficio_id:this.Oficios[this.indexOficio].id
        }).then(response => {
          if(response.data.error==0){
            this.Oficios[this.indexOficio].oferta_economica.despacho={'prueba':1234};
            this.Oficios[this.indexOficio].oferta_economica.despacho.beneficiario=this.beneficiarios;
            this.Oficios[this.indexOficio].oferta_economica.despacho.destino=this.destino_despacho;
            this.Oficios[this.indexOficio].oferta_economica.despacho.fecha_despacho=null;
            this.Oficios[this.indexOficio].oferta_economica.despacho.id=response.data.despacho_id;
            this.Oficios[this.indexOficio].oferta_economica.despacho.chofer={'cedula':this.cedula_chofer,'nombres_apellidos':this.chofer_nombres_apellidos,'telefono':this.chofer_numero};
            this.Oficios[this.indexOficio].oferta_economica.despacho.responsable={'cedula':this.cedula_responsable,'nombres_apellidos':this.responsable_nombres_apellidos,'telefono':this.responsable_numero};
            this.Oficios[this.indexOficio].oferta_economica.despacho.vehiculo={'placa':this.vehiculo_placa,'modelo':this.vehiculo_modelo};
            $('#asignarChofer').modal('toggle');
            alertify.success(response.data.msg);
            this.limpiar();
            var cancelado=0;
            for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica_pagos.length;i++){
              if(this.Oficios[this.indexOficio].oferta_economica_pagos[i].validacion)
              cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica_pagos[i].monto);
            }
            cancelado=parseFloat(cancelado);
            if(parseFloat(cancelado)==this.total){
              //Si ya actualiza los datos del chofer y el pago del oficio estaba completo, de una vez lo quita del listado y actualiza su estado a facturacion
              axios.post('{{ route("actualizarEstadoOficio") }}', {
                oficio_id:this.Oficios[this.indexOficio].id,
                estado:6
              })
              .then(response => {
                if(response.data.error==0){
                  alertify.success(response.data.msg);
                  if(this.Oficios.length<2){
                    setTimeout(function(){ location.reload(); }, 2500);
                  }else{
                    this.Oficios.splice(this.indexOficio,1);
                  }
                  this.limpiar();
                }//if(response.data.status=="success")
                else if(response.data.error==1){
                  alertify.error(response.data.msg);
                }
              })
              .catch(function (error) {
                console.log(error);
              });
            }//Si todo fue cancelado
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      }//b==0 no errors
    },
    buscarPersonaNatural(cedula,tipo_persona,tipo_operacion){
      //tipo_persona == chofer || responsable
      // console.log(cedula,tipo_persona);
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:cedula}).then(response => {
        if(response.data.error==0){
          console.log(response.data);
          if(tipo_persona=="chofer"){
            //Si es chofer
            if(tipo_operacion=='registrar'){
              this.chofer_nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.chofer_numero=response.data.persona_natural.telefono;
            }else{
              this.Oficios[this.indexOficio].oferta_economica.despacho.chofer.nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.Oficios[this.indexOficio].oferta_economica.despacho.chofer.telefono=response.data.persona_natural.telefono;
            }
          }else{
            //Si es responsable
            if(tipo_operacion=='registrar'){
              this.responsable_nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.responsable_numero=response.data.persona_natural.telefono;
            }else{
              this.Oficios[this.indexOficio].oferta_economica.despacho.responsable.nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.Oficios[this.indexOficio].oferta_economica.despacho.responsable.telefono=response.data.persona_natural.telefono;
            }
          }
        }else if(response.data.error==1){
          this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          // console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          // console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    buscarVehiculo(placa,tipo_operacion){
      axios.post('{{ route("api.controloficio.datos.vehiculo") }}', {placa:placa}).then(response => {
        if(response.data.error==0){
          // console.log(response.data);
          if(tipo_operacion=='registrar')
          this.vehiculo_modelo=response.data.vehiculo.modelo;
          else if(tipo_operacion=='editar')
          this.Oficios[this.indexOficio].oferta_economica.despacho.vehiculo.modelo=response.data.vehiculo.modelo;
        }else if(response.data.error==1){
          this.vehiculo_modelo="";
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        // console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    onlyNumber ($event) {
      //console.log($event.keyCode); //keyCodes value
      let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if (keyCode > 31 && (keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
        $event.preventDefault();
      }
    },
    onlyNumberWithOutDecimalPoint ($event) {
      //console.log($event.keyCode); //keyCodes value
      let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) { // 46 is dot (,)
        $event.preventDefault();
      }
    },
    convertirBase64Beneficiarios: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.beneficiarios = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    convertirBase64PagoAdicional: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.soporteAdicional = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    actualizarChofer(){
      // console.log(this.Oficios[this.indexOficio].oferta_economica.despacho);
      var b=0;
      if(this.Oficios[this.indexOficio].oferta_economica.despacho.chofer.cedula==""){
        b=1;
        alertify.error('Debe ingresar una cédula de chofer');
      }else if(this.Oficios[this.indexOficio].oferta_economica.despacho.vehiculo.placa==""){
        b=1;
        alertify.error('Debe ingresar una placa de vehículo');
      }else if(this.Oficios[this.indexOficio].oferta_economica.despacho.destino==""){
        b=1;
        alertify.error('Debe ingresar el destino del despacho');
      }else if(this.Oficios[this.indexOficio].oferta_economica.despacho.responsable.cedula==""){
        b=1;
        alertify.error('Debe ingresar una cédula de responsable');
      }else if(this.Oficios[this.indexOficio].oferta_economica.despacho.beneficiario==""){
        b=1;
        alertify.error('Debe adjuntar un pdf de beneficiarios');
      }
      if(b==0){
        //no errors
        axios.post('{{ route("actualizarChofer") }}', {
          cedula_chofer:this.Oficios[this.indexOficio].oferta_economica.despacho.chofer.cedula,
          cedula_responsable:this.Oficios[this.indexOficio].oferta_economica.despacho.responsable.cedula,
          vehiculo_placa:this.Oficios[this.indexOficio].oferta_economica.despacho.vehiculo.placa,
          destino_despacho:this.Oficios[this.indexOficio].oferta_economica.despacho.destino,
          beneficiarios:this.Oficios[this.indexOficio].oferta_economica.despacho.beneficiario,
          despacho_id:this.Oficios[this.indexOficio].oferta_economica.despacho.id
        }).then(response => {
          if(response.data.error==0){
            this.limpiar();
            $('#asignarChofer').modal('toggle');
            alertify.success(response.data.msg);
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          alertify.error('Error en el servidor.');
          console.log('Axios api datos persona natural catch');
          console.log(error);
        });
      }//b==0 no errors
    },
    convertirBase64BeneficiariosEditar: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.Oficios[this.indexOficio].oferta_economica.despacho.beneficiario = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    convertirBase64SoportePago: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.soporte_pago = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    convertirBase64SoportePagoEditar: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].soporte_lote = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    eliminarPago(pago_id){
      axios.post('{{ route("borrarPago") }}', {
        pago_id:pago_id
      })
      .then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica_pagos.length;i++){
            if(this.Oficios[this.indexOficio].oferta_economica_pagos[i].id==pago_id){
              this.Oficios[this.indexOficio].oferta_economica_pagos.splice(i,1);
              break;
            }//pago==id
          }//for
          // $('#myModal').modal('toggle');
        }//if(response.data.status=="success")
        else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    actualizarPago(){
      var b=0;
      if(this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].tipo_pago==0){
        b++;
        alertify.error('Debes seleccionar un tipo de pago');
      }else if(this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].referencia==""){
        b++;
        alertify.error('Debes ingresar el número de referencia del pago');
      }else if(this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].monto==""){
        b++;
        alertify.error('Debes ingresar el monto del pago realizado');
      }else if(this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].tipo_pago=="transferencia" && this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index].soporte==""){
        b++;
        alertify.error('Debes ingresar el soporte de pago.');
      }
      if(b==0){
        axios.post('{{ route("actualizarPago") }}', {
          pago:this.Oficios[this.indexOficio].oferta_economica_pagos[this.pago_index]
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.limpiar();
            // $('#myModal').modal('toggle');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }//b==0 no errors
    },
    asociarPago(){
      var b=0;
      if(this.tipo_pago==0){
        b++;
        alertify.error('Debes seleccionar un tipo de pago');
      }else if(this.tipo_pago!="efectivo" && this.referencia_pago==""){
        b++;
        alertify.error('Debes ingresar el número de referencia del pago');
      }else if(this.monto==""){
        b++;
        alertify.error('Debes ingresar el monto del pago realizado');
      }else if(this.tipo_pago=="transferencia" && this.soporte_pago==""){
        b++;
        alertify.error('Debes ingresar el soporte de pago.');
      }
      if(b==0){
        axios.post('{{ route("asociarPago") }}', {
          oficio_id:this.Oficios[this.indexOficio].id,
          tipo_pago:this.tipo_pago,
          referencia_pago:this.referencia_pago,
          monto_pago:this.monto_pago,
          soporte_pago:this.soporte_pago
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Oficios[this.indexOficio].oferta_economica_pagos.push(response.data.pago);
            var cancelado=0;
            for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica_pagos.length;i++){
              if(this.Oficios[this.indexOficio].oferta_economica_pagos[i].validacion)
              cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica_pagos[i].monto);
            }
            cancelado=parseFloat(cancelado);
            if(parseFloat(cancelado)==this.total){
              //Muestra alerta que ya todo el dinero fue cancelado,
              //si aun no ha cargado datos de chofer, muestra alerta que debe cargar dichos datos.
              //sino, lo quita del listado.
              $('#asociarPago').modal('toggle');
              if(this.Oficios[this.indexOficio].oferta_economica.despacho==null){
                alertify.warning('Ya se pagó el monto total de la cotización. Debes ingresar los datos del chofer,para proceder al despacho.');
                $('#asignarChofer').modal('toggle');
              }else{
                //Si ya posee datos del chofer registrados, lo quita de la lista de los oficios y cambia su estado a en proceso de facturacion
                //Consulta api axios cambiar estado a facturacion
                axios.post('{{ route("actualizarEstadoOficio") }}', {
                  oficio_id:this.Oficios[this.indexOficio].id,
                  estado:6
                })
                .then(response => {
                  if(response.data.error==0){
                    alertify.success(response.data.msg);
                    if(this.Oficios.length<2){
                      setTimeout(function(){ location.reload(); }, 2500);
                    }else{
                      this.Oficios.splice(this.indexOficio,1);
                    }
                    this.limpiar();
                  }//if(response.data.status=="success")
                  else if(response.data.error==1){
                    alertify.error(response.data.msg);
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
              }//else
            }//Si todo fue cancelado
            this.limpiar();
            // $('#myModal').modal('toggle');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }//b==0 no errors
    },
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.Oficios){
          if(this.Oficios[i].solicitante_ente.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
            this.statusFiltro=1;
            filtrardo.push(this.Oficios[i]);
          }//
        }//
        if(filtrardo.length)
        this.Oficios=filtrardo;
        else{
          this.statusFiltro=0;
          this.Oficios={!! $oficios ? $oficios : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.Oficios={!! $oficios ? $oficios : "''"!!};
      }//if(this.search$materiales
      },// filtrar:function()
      capturarOficio:function(id,tipo_operacion=null){
        for(var i=0;i<this.Oficios.length;i++){
          if(this.Oficios[i].id==id){
            this.indexOficio=i;
          }//if this.oficio
        }//for
        if(tipo_operacion=="pagosAprobados"){
          //Ver si ya este oficio contiene al menos un pago aprobado.
          if(this.Oficios[this.indexOficio].oferta_economica_pagos.length==0){
            //No tiene pagos relacionados
            this.reintegro_reintegrar=0;
          }else{
            var b=0;
            for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica_pagos.length;i++){
              if(this.Oficios[this.indexOficio].oferta_economica_pagos[i].validacion){
                //Si tiene pagos relacionados
                b++;
              }//if pago validado
            }//for
            if(b>0){
              //Tiene pago relacionado
              this.reintegro_reintegrar=1;
            }else{
              //No tiene pagos relacionados
              this.reintegro_reintegrar=0;
            }
          }//else posee pagos
        }//if tipo_operacion
        else if(tipo_operacion=="cargaCarousel"){
          this.installOwlCarousel2();
        }
      },
      cambiarPrecio:function(indexProducto){
        // console.log(this.Oficios[this.indexOficio].productos[indexProducto]);

        if(this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio==0){

          this.Oficios[this.indexOficio].productos[indexProducto].precio=0;
          this.Oficios[this.indexOficio].productos[indexProducto].preciop=0;
        }else if(this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio==1 ){

          // if(this.Oficios[this.indexOficio].productos[indexProducto].cantidad<50)
          this.Oficios[this.indexOficio].productos[indexProducto].precio=this.Oficios[this.indexOficio].productos[indexProducto].precios.precio1;
          this.Oficios[this.indexOficio].productos[indexProducto].preciop=this.Oficios[this.indexOficio].productos[indexProducto].precios.precio1p;
          // else{
          //   alertify.error('Solo puedes seleccionar este nivel si la cantidad de productos es menor a 50.');
          //   this.Oficios[this.indexOficio].productos[indexProducto].precio=0;
          //   this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio=0;
          // }
        }else if(this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio==2){

          // if(this.Oficios[this.indexOficio].productos[indexProducto].cantidad>49)
          this.Oficios[this.indexOficio].productos[indexProducto].precio=this.Oficios[this.indexOficio].productos[indexProducto].precios.precio2;
          this.Oficios[this.indexOficio].productos[indexProducto].preciop=this.Oficios[this.indexOficio].productos[indexProducto].precios.precio2p;
          // else{
          //   alertify.error('Solo puedes seleccionar este nivel si la cantidad de productos es mayor a 49.');
          //   this.Oficios[this.indexOficio].productos[indexProducto].precio=0;
          //   this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio=0;
          // }
        }else if(this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio==3 || this.Oficios[this.indexOficio].productos[indexProducto].nivel_precio==6){


          this.Oficios[this.indexOficio].productos[indexProducto].precio=this.Oficios[this.indexOficio].productos[indexProducto].precios.precio3;
          this.Oficios[this.indexOficio].productos[indexProducto].preciop=this.Oficios[this.indexOficio].productos[indexProducto].precios.precio3p;

        }

      },
      GenerarOfertaEconomica(){
        var oficio=this.Oficios[this.indexOficio];
        var error=0;
        //Validar cantidad de productos no sea mayor a la cantidad aprobada.
        for(var i=0;i<oficio.productos.length;i++){
          // if(parseInt(oficio.productos[i].cantidad)>parseInt(oficio.productos[i].cantidad_max)){
          //   alertify.error('La cantidad ingresada del modelo '+oficio.productos[i].descripcion+' es mayor a la aprobada ('+oficio.productos[i].cantidad_max+')');
          //   error=1;
          //   break;
          // }//if
          // else
          if(parseInt(oficio.productos[i].cantidad)==0){
            alertify.error('La cantidad ingresada del modelo '+oficio.productos[i].descripcion+' debe ser mayor a 0.');
            error=1;
            break;
          }else if(oficio.productos[i].cantidad==""){
            alertify.error('Debes ingresar una cantidad para el modelo '+oficio.productos[i].descripcion);
            error=1;
            break;
          }
          if(oficio.productos[i].nivel_precio==0 || oficio.productos[i].nivel_precio=="0"){
            alertify.error('Debe seleccionar un precio para el modelo '+oficio.productos[i].descripcion);
            error=1;
            break;
          }
          // else if(oficio.productos[i].nivel_precio==1 || oficio.productos[i].nivel_precio=="1"){
          //   if(parseInt(oficio.productos[i].cantidad)>49){
          //     alertify.error('No puedes seleccionar el nivel de precio 1 para el modelo '+oficio.productos[i].descripcion+', su cantidad es mayor a 49');
          //     error=1;
          //     break;
          //   }
          // }else if(oficio.productos[i].nivel_precio==2 || oficio.productos[i].nivel_precio=="2"){
          //   if(parseInt(oficio.productos[i].cantidad)<50){
          //     alertify.error('No puedes seleccionar el nivel de precio 2 para el modelo '+oficio.productos[i].descripcion+', su cantidad es menor a 50');
          //     error=1;
          //     break;
          //   }
          // }//validacion precio 2
        }//for
        if(error==0){
          this.generandoOferta=true;
          axios.post('{{ route("GuardarOfertaEconomica") }}', {
            oficio:oficio,
            total: this.total,
            total_petro: this.total_petro
          })
          .then(response => {
            if(response.data.error==0){
              alertify.success(response.data.msg);
              $('#myModal').modal('toggle');
              // this.Oficios[this.indexOficio]=response.data.oficio;
              this.Oficios=response.data.oficios;
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
              console.log(response.data.msgSystem);
            }
            this.generandoOferta=false;
          })
          .catch(function (error) {
            console.log(error);
          });
        }//
      },
      ActualizarOfertaEconomica(){
        this.actualizandoOferta=true;
        // console.log('Actualizar precios de oficio:');
        // console.log(this.Oficios[this.indexOficio]);
        var productos=this.Oficios[this.indexOficio].productos;
        var error=0;
        for (var i=0;i<productos.length;i++){
          var producto=productos[i];
          var nivel_precio=this.Oficios[this.indexOficio].productos[i].nivel_precio;
          var precio=0;
          if(parseInt(nivel_precio)==1){
            precio=this.Oficios[this.indexOficio].productos[i].precios.precio1p;
            nivel_precio=4;
          }
          if(parseInt(nivel_precio)==2){
            precio=this.Oficios[this.indexOficio].productos[i].precios.precio2p;

            nivel_precio=5;
          }
          if(parseInt(nivel_precio)==3){
            precio=this.Oficios[this.indexOficio].productos[i].precios.precio3p;
            nivel_precio=6;
          }
          // var precio=this.preciosPremium[this.string_to_slug(modelo)+this.string_to_slug(modalidad)][(parseInt(nivel_precio))];
          this.Oficios[this.indexOficio].productos[i].precio_petro=precio;
          this.Oficios[this.indexOficio].productos[i].preciop=precio;
          console.log(this.Oficios[this.indexOficio].productos[i]);
          console.log(precio);
          if(parseInt(producto.cantidad)>parseInt(producto.cantidad_almacen)){
            alertify.error('La cantidad ingresada del modelo '+producto.descripcion+' es mayor a la disponible ('+producto.cantidad_almacen+')');
            error=1;
          }//if
          else if(parseInt(producto.cantidad)==0){
            alertify.error('La cantidad ingresada del modelo '+producto.descripcion+' debe ser mayor a 0.');
            error=1;
          }else if(producto.cantidad==""){
            alertify.error('Debes ingresar una cantidad para el modelo '+producto.descripcion);
            error=1;
          }
          if(producto.nivel_precio==0 || producto.nivel_precio=="0"){
            alertify.error('Debe seleccionar un precio para el modelo '+producto.descripcion);
            error=1;
          }
        }//for productos
        if(error==0){
          //Actualizar precio
          axios.post('{{ route("ActualizarProductosOfertaEconomica") }}', {
            Productos:productos,
            Oferta_economica_id:this.Oficios[this.indexOficio].oferta_economica.id,
            Total:this.total,
            total_petro: this.total_petro
          })
          .then(response => {
            if(response.data.error==0){
              alertify.success(response.data.msg);

              var lengthProducts=this.Oficios[this.indexOficio].productos.length;
              var cantidadEquipos=0;
              var modelos="";
              for(var i=0;i<lengthProducts;i++){
                if(i==0)
                modelos=this.Oficios[this.indexOficio].productos[i].descripcion;
                else
                modelos=modelos+", "+this.Oficios[this.indexOficio].productos[i].descripcion;
                cantidadEquipos=cantidadEquipos+parseInt(this.Oficios[this.indexOficio].productos[i].cantidad);
              }//for
              this.Oficios[this.indexOficio].modelos=modelos;
              this.Oficios[this.indexOficio].cantidad_solicitada=cantidadEquipos;

              // $('#myModal').modal('toggle');
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
              console.log(response.data.msgSystem);
            }
            this.actualizandoOferta=false;
          })
          .catch(function (error) {
            console.log(error);
          });
        }//error==0s
      },
      guardarCambiosProducto(indexProducto){
        // console.log('Producto:');
        // console.log(this.Oficios[this.indexOficio].productos[indexProducto]);
        var producto=this.Oficios[this.indexOficio].productos[indexProducto];
        var error=0;
        // if(parseInt(producto.cantidad)>parseInt(producto.cantidad_max)){
        //   alertify.error('La cantidad ingresada del modelo '+producto.descripcion+' es mayor a la aprobada ('+producto.cantidad_max+')');
        //   error=1;
        // }//if
        // else
        if(parseInt(producto.cantidad)==0){
          alertify.error('La cantidad ingresada del modelo '+producto.descripcion+' debe ser mayor a 0.');
          error=1;
        }else if(producto.cantidad==""){
          alertify.error('Debes ingresar una cantidad para el modelo '+producto.descripcion);
          error=1;
        }
        if(producto.nivel_precio==0 || producto.nivel_precio=="0"){
          alertify.error('Debe seleccionar un precio para el modelo '+producto.descripcion);
          error=1;
        }
        // else if(producto.nivel_precio==1 || producto.nivel_precio=="1"){
        //   if(parseInt(producto.cantidad)>49){
        //     alertify.error('No puedes seleccionar el nivel de precio 1 para el modelo '+producto.descripcion+', su cantidad es mayor a 49');
        //     error=1;
        //   }
        // }else if(producto.nivel_precio==2 || producto.nivel_precio=="2"){
        //   if(parseInt(producto.cantidad)<50){
        //     alertify.error('No puedes seleccionar el nivel de precio 2 para el modelo '+producto.descripcion+', su cantidad es menor a 50');
        //     error=1;
        //   }
        // }//validacion precio 2
        if(error==0){
          //Actualizar precio
          axios.post('{{ route("ActualizarProductoOfertaEconomica") }}', {
            Producto:producto,
            Total:this.total,
            total_petro: this.total_petro
          })
          .then(response => {
            if(response.data.error==0){
              alertify.success(response.data.msg);
              var cantidad_solicitada=0;
              for(var i=0;i<this.Oficios[this.indexOficio].productos.length;i++)
              cantidad_solicitada=cantidad_solicitada+parseInt(this.Oficios[this.indexOficio].productos[i].cantidad);
              this.Oficios[this.indexOficio].cantidad_solicitada=cantidad_solicitada;
              // $('#myModal').modal('toggle');
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
              console.log(response.data.msgSystem);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
        }//error==0s
      },
      cargarEntes(){
        this.ente_id=0;
        this.entes=[];
        axios.post('{{ route("api.controloficio.obtenerentes") }}', {clasificacion_ente_id:this.clasificacion_ente_id}).then(response => {
          if(response.data.error==0){
            this.entes=response.data.entes
          }else if(response.data.error==1){
            // this.limpiar();
            this.clasificacion_ente_id=0;
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          this.clasificacion_ente_id=0;
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      },
      buscarOficios(){
        axios.post('{{ route("FiltroObtenerOficios") }}', {tipo_solicitante:this.tipo_solicitante,clasificacion_ente_id:this.clasificacion_ente_id,ente_id:this.ente_id,estado_id:this.estado_id,fechaInicio:this.fechaInicio,fechaFin:this.fechaFin}).then(response => {
          if(response.data.error==0){
            if(response.data.oficios.length==0){
              alertify.error('No se encontraron oficios con este filtro.');
              this.tipo_solicitante=0;
              this.estado_id=0;
              // this.buscarOficios();
            }
            else
            this.Oficios=response.data.oficios;
            // alertify.error("No existe solicitudes Aprobadas para Gestionar");
          }else if(response.data.error==1){
            // this.limpiar();
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      },
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.Oficios.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      },
      formatearNumero:function(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
      }
    },//methods
    computed:{
      listado_oficios:function() {
        this.rows=0;
        if(this.Oficios.length>0){
          return this.Oficios.sort((a,b) => {
            let modifier = 1;
            if(this.currentSortDir === 'desc')
            modifier = -1;
            if(a[this.currentSort] < b[this.currentSort])
            return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort])
            return 1 * modifier;
            return 0;
          }).filter((row, index) => {
            let start = (this.currentPage-1)*this.pageSize;
            let end = this.currentPage*this.pageSize;
            if(index >= start && index < end){
              this.rows+=1;
              return true;
            }
          });
        }else{
          return [];
        }
      },
      calcular_subtotal_petro:function(){
        if(this.Oficios.length>0){
          var subtotal=0;
          for(var i=0;i<this.Oficios[this.indexOficio].productos.length;i++){
            var nivel_precio=this.Oficios[this.indexOficio].productos[i].nivel_precio;
            var modelo=this.Oficios[this.indexOficio].productos[i].modelo;
            var modalidad=this.Oficios[this.indexOficio].productos[i].modalidad;
            var precio=0;
            if(parseInt(nivel_precio)==1){
              precio=this.Oficios[this.indexOficio].productos[i].precios.precio1p;
              nivel_precio=4;
            }
            if(parseInt(nivel_precio)==2){
              precio=this.Oficios[this.indexOficio].productos[i].precios.precio2p;

              nivel_precio=5;
            }
            if(parseInt(nivel_precio)==3){
              precio=this.Oficios[this.indexOficio].productos[i].precios.precio3p;
              nivel_precio=6;
            }
            // var precio=this.preciosPremium[this.string_to_slug(modelo)+this.string_to_slug(modalidad)][(parseInt(nivel_precio))];
            this.Oficios[this.indexOficio].productos[i].precio_petro=precio;
            this.Oficios[this.indexOficio].productos[i].preciop=precio;
            console.log(this.Oficios[this.indexOficio].productos[i]);
            console.log(precio);
            subtotal=subtotal+this.Oficios[this.indexOficio].productos[i].cantidad*precio;
          }
          return parseFloat(subtotal).toFixed(4);
        }else{
          return 0;
        }
      },
      calcular_subtotal:function(){
        if(this.Oficios.length>0){
          var subtotal=0;
          for(var i=0;i<this.Oficios[this.indexOficio].productos.length;i++){
            var nivel_precio=this.Oficios[this.indexOficio].productos[i].nivel_precio;
            var modelo=this.Oficios[this.indexOficio].productos[i].modelo;
            var modalidad=this.Oficios[this.indexOficio].productos[i].modalidad;
            if(parseInt(nivel_precio)==1)
              nivel_precio=1;
            if(parseInt(nivel_precio)==2)
              nivel_precio=2;
            if(parseInt(nivel_precio)==3)
              nivel_precio=3;
            var precio=this.preciosPremium[this.string_to_slug(modelo)+this.string_to_slug(modalidad)][nivel_precio];
            this.Oficios[this.indexOficio].productos[i].precio=precio;
            subtotal=subtotal+this.Oficios[this.indexOficio].productos[i].cantidad*precio;
          }
          return parseFloat(subtotal).toFixed(2);
        }else{
          return 0;
        }
      },
      calcular_total_petro:function(){
        if(this.Oficios.length>0){
          var total=0;
          var subtotal=0;
          var precio=0;
          for(var i=0;i<this.Oficios[this.indexOficio].productos.length;i++){
            var nivel_precio=this.Oficios[this.indexOficio].productos[i].nivel_precio;
            var modelo=this.Oficios[this.indexOficio].productos[i].modelo;
            var modalidad=this.Oficios[this.indexOficio].productos[i].modalidad;
            if(parseInt(nivel_precio)==1){
              precio=this.Oficios[this.indexOficio].productos[i].precios.precio1p;
              nivel_precio=4;
            }
            if(parseInt(nivel_precio)==2){
              precio=this.Oficios[this.indexOficio].productos[i].precios.precio2p;
              nivel_precio=5;
            }
            if(parseInt(nivel_precio)==3){
              precio=this.Oficios[this.indexOficio].productos[i].precios.precio3p;
              nivel_precio=6;
            }
            // var precio=this.preciosPremium[this.string_to_slug(modelo)+this.string_to_slug(modalidad)][nivel_precio];
            subtotal=subtotal+this.Oficios[this.indexOficio].productos[i].cantidad*precio;
          }
         total=subtotal+((this.iva*subtotal)/100);
          // total=subtotal;
          total=parseFloat(total).toFixed(4);
          this.total_petro=total;
          return total;
        }else{
          return 0;
        }
      },
      calcular_total:function(){
        if(this.Oficios.length>0){
          var total=0;
          var subtotal=0;
          for(var i=0;i<this.Oficios[this.indexOficio].productos.length;i++){
            var nivel_precio=this.Oficios[this.indexOficio].productos[i].nivel_precio;
            var modelo=this.Oficios[this.indexOficio].productos[i].modelo;
            var modalidad=this.Oficios[this.indexOficio].productos[i].modalidad;
            var precio=this.preciosPremium[this.string_to_slug(modelo)+this.string_to_slug(modalidad)][nivel_precio];
            subtotal=subtotal+this.Oficios[this.indexOficio].productos[i].cantidad*precio;
          }
          total=subtotal+((this.iva*subtotal)/100);
          total=parseFloat(total).toFixed(2);
          this.total=total;
          return total;
        }else{
          return 0;
        }
      },
      calcular_restante:function(){
        if(this.Oficios.length>0){
          var cancelado=0;
          for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica_pagos.length;i++){
            // if(this.Oficios[this.indexOficio].oferta_economica_pagos[i].validacion)
            cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica_pagos[i].monto);
          }
          cancelado=this.total-cancelado;
          cancelado=parseFloat(cancelado).toFixed(2);
          return cancelado;//restante
        }else{
          return 0;
        }
      },
      calcular_cancelado:function(){
        if(this.Oficios.length>0){
          var cancelado=0;
          for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica_pagos.length;i++){
            // if(this.Oficios[this.indexOficio].oferta_economica_pagos[i].validacion)
            cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica_pagos[i].monto);
          }
          cancelado=parseFloat(cancelado).toFixed(2);
          return cancelado;
        }else{
          return 0;
        }
      },
      showNivelPrecio(nivel){
        console.log(nivel)
      }
    },mounted(){
      console.log(this.preciosPremium);
      if(this.Oficios.length>0){
        this.indexOficio=0;
      }
      $('#verPago').on('show.bs.modal', function () {
        $('#asociarPago').modal('hide')
      })

      $('#verPago').on('hidden.bs.modal', function () {
        $('#asociarPago').modal('show')
      })
    }

  });//const app= new Vue
</script>
@endpush
