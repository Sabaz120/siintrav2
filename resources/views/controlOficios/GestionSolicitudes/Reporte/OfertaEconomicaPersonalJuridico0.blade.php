<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="screen">
  body{
    margin: 0mm 8mm 2mm 8mm;
  }
  /* table{
    table-layout: fixed;
  } */

  table, th, td {
    /*Agregado */
    width:100%;
    height:auto;
    margin:10px 0 10px 0;
    border-collapse:collapse;
    /*Agregado */
    border: 1px solid black;
    text-align: center;
    font-size:12px;
  }
  table td,th{
    border:1px solid black;
  }
  .tabla_std2 {
    width: 100%;
    margin: 0 auto;
    border: solid 1px black;
    border-collapse: collapse;
  }

  .tabla_std2 th, td, tr {
    border: solid 1px black;
    border-collapse: collapse;
  }

  .tabla_std2 td, th {
    /*padding: 3px 8px;*/
    text-align: center;
  }

  .tabla_std2 th {
    font-weight: bold;
    text-align: center;
  }

  .tabla_std2 td {
    /* width: 20%; */
  }
  .text-center{
    text-align:center;
  }
  .text-left{
    text-align:left;
  }
  .text-right{
    text-align:right;
  }
  .badge{
    font-weight:600;
    padding:3px 5px;
    font-size:12px;
    margin-top:1px;
    display:inline-block;
    line-height: 1;
    text-align:center;
    white-space:nowrap;
    vertical-align: baseline;
    border-radius:.25rem;
  }
  .container-fluid {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
  }
  .text-capitalize{
    text-transform:capitalize;
  }
  .text-justify{
    text-align:justify;
  }
  .badge-primary{
    background-color: #039cfd;
    color:#fff;
  }
  @page { margin: 100px 15px; }
  #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 60px;
    background-size: auto 20px;
    margin: 5px;
  }
  #footer { position: fixed; left: 0px; bottom: -115px; right: 0px; height: 108px;
  }
  #footer .page:after { content: counter(page, upper-roman); }
  </style>
</head>
<body>
  <?php
  $mytime = Carbon\Carbon::now();
  $mytime = $mytime->format('d-m-Y h:m:s');

  // echo $mytime->toDateTimeString();
  ?>
  <div id="header">
    <img style="float:left;height:60px;" src="http://rec.vtelca.gob.ve/img/cintillo-i.png" />
    <img style="float:right;height:60px;" src="http://rec.vtelca.gob.ve/img/cintillo-c.png" />
    <br><br><br>
    <hr style="color:red">
    <!-- <div class="text-left" style="float:left">
      <strong >Reporte emitido por:</strong> {{Auth::user()->name}}
    </div>
    <div class="text-right" style="float:right">
      <strong>Fecha del reporte:</strong>{{$mytime}}
    </div> -->
  </div>
  <div id="footer">
    <hr style="color:red">
    <p style="font-size:12px;text-align:center;">Av. Bolivar, calle de servicio 4, Meseta de Guaranao, galpones 5-5, 5-6, Zona Franca Industrial, Comercial y de Servicio de Paraguaná, C.A. Estado Falcón</p>
    <p class="page" style="text-align:center;font-size:12px"><strong>Teléfonos: </strong>0269-2502511 / 0269-25025250. Página: </p>
  </div>
  <div class="container-fluid">
    <!-- CONTENIDO -->
    <table>
      <tr>
        <th colspan="6" class="text-right">FECHA: {{$mytime}}</th>
      </tr>
      <tr>
        <th style="color:red;" colspan="3">
          <strong>OFERTA ECONÓMICA</strong>
        </th>
        <th style="color:red;" colspan="2">ASUNTOS PÚBLICOS</th>
        <th style="color:red;">N° OFERTA: {{str_pad($oficio->oferta_id,8,"0")}}</th>
      </tr>
      <tr>
        <th colspan="2">
          <strong>NOMBRE DE LA EMPRESA:</strong>
        </th>
        <th colspan="4" class="text-capitalize text-justify">
          {{$oficio->personal_juridico->nombre_institucion}}
        </th>
      </tr>
      <tr>
        <th colspan="2">
          <strong>REGISTRO DE INFORMACIÓN FISCAL</strong>
        </th>
        <th colspan="4" class="text-capitalize text-justify">{{$oficio->personal_juridico->rif}}</th>
      </tr>
      <tr>
        <th colspan="2">
          <strong>DIRECCIÓN FISCAL</strong>
        </th>
        <th colspan="2" class="text-capitalize text-justify">
          {{$oficio->personal_juridico->direccion_fiscal}}
        </th>
        <th colspan="1">
          <strong>TELÉFONOS:</strong>
        </th>
        <th colspan="1">
          <strong>{{$oficio->personal_natural->telefono}}</strong>
        </th>
      </tr>
      <tr>
        <th colspan="2">
          <strong>ATENCIÓN:</strong>
        </th>
        <th colspan="4" class="text-capitalize text-justify">
          {{$oficio->personal_natural->nombre_completo}}
        </th>
      </tr>
      <!-- CABECERA CUADO DE PRODUCTOS -->
      <tr>
        <th style="background:red;color:black;">
          <strong>ITEMS</strong>
        </th>
        <th style="background:red;color:black;">
          <strong>DESCRIPCIÓN</strong>
        </th>
        <th style="background:red;color:black;">
          <strong>CÓDIGO - MODALIDAD</strong>
        </th>
        <th style="background:red;color:black;">
          <strong>CANTIDAD DE EQUIPOS</strong>
        </th>
        <th style="background:red;color:black;">
          <strong>COSTO UNITARIO</strong>
        </th>
        <th style="background:red;color:black;">
          <strong>COSTO TOTAL</strong>
        </th>
      </tr>
      @php
      $subtotal=0;
      @endphp
      @foreach($oficio->productos as $producto)
      <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$producto->descripcion}}</td>
        <td>{{$producto->codigo}} - {{$producto->modalidad}}</td>
        <td>{{$producto->cantidad}}</td>
        <td>{{$producto->precio}}</td>
        <td>{{$producto->precio*$producto->cantidad}}</td>
      </tr>
      @php
      $subtotal=$subtotal+$producto->precio*$producto->cantidad;
      @endphp
      @endforeach
      <tr>
        <td colspan="5" style="text-align:right">
          <strong>SUB-TOTAL</strong>
        </td>
        <td>{{$subtotal}}</td>
      </tr>
      @php
      $iva=($subtotal*16)/100;
      @endphp
      <tr>
        <td colspan="5" style="text-align:right">
          <strong>I.V.A 16%</strong>
        </td>
        <td>{{$iva}}</td>
      </tr>
      <tr>
        <td colspan="5" style="text-align:right">
          <strong>TOTAL</strong>
        </td>
        <td>{{$subtotal+$iva}}</td>
      </tr>
      <tr>
        <th colspan="6" style="background:red;color:black;">
          <strong>CONDICIONES GENERALES DE NEGOCIACIÓN</strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            1. Estos precios incluyen I.V.A.
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            2. A nombre de Venezolana de Telecomunicaciones C.A.
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            3. Nº de cuenta bancaria para transferencias o depósitos: Banco de Tesoro 0163-0310-113103004995
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            4. Rif. G20008334-4
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            5. Documentos requeridos para la venta: ORDEN DE COMPRA / DATOS FISCALES. (RIF)
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            6. Información requerida para la venta:
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            * Datos del Responsable de la Negociación (Nombre, Apellido, Número de Contacto, y dirrección de correo electrónico)
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            * Chofer de Vehículo que retira los equipos (Nombre, Apellido, copia de cédula, y número telefónico)
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            * Listado de Trabajadores firmado y sellado por la máxima autoridad (Cédula y Carnet de la Institución de los trabajadores a Beneficiar)
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            * Carnet de la Patria Escaneado.
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            Esta Oferta Económica esta sujeta a cambios.
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            Es importante recalcar que el éxito de la venta dependerá del cumplimiento de cada uno de los requerimientos antes descritos.
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" class="text-justify">
          <strong>
            Esperamos y deseamos cumplir con sus expectativas de la calidad y satisfacción. Cualquier otra información que requieran favor comunicarse con nuestro departamento de Comercialización enviando un correo electronico a: comercialización.vtelca.@gmail.com
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="6" style="text-align:center;">
          <strong>
            Atentamente;
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="4" style="text-align:center;">
          <p style="text-align: center; margin-bottom: -30px;">
            <img style="width: 60px; height: 60px;" src="{{ public_path('/images/firma_hugo.png') }}">
          </p>
          <br><br>
          <strong>
            HUGO GARCÍA.
          </strong>
          <br>
          <strong>
            PRESIDENCIA
          </strong>
          <br>
          <strong>
            VENEZOLANA DE TELECOMUNICACIONES C.A.
          </strong>
        </th>
      </tr>
    </table>
    <!-- CONTENIDO -->
  </div>
</body>
</html>
