<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="screen">
  body{
    margin: 0mm 8mm 2mm 8mm;
  }
  /* table{
    table-layout: fixed;
  } */

  table, th, td {
    /*Agregado */
    width:100%;
    height:auto;
    margin:10px 0 10px 0;
    border-collapse:collapse;
    /*Agregado */
    border: 1px solid black;
    text-align: center;
    font-size:12px;
  }
  table td,th{
    border:1px solid black;
  }
  .tabla_std2 {
    width: 100%;
    margin: 0 auto;
    border: solid 1px black;
    border-collapse: collapse;
  }

  .tabla_std2 th, td, tr {
    border: solid 1px black;
    border-collapse: collapse;
  }

  .tabla_std2 td, th {
    /*padding: 3px 8px;*/
    text-align: center;
  }

  .tabla_std2 th {
    font-weight: bold;
    text-align: center;
  }

  .tabla_std2 td {
    /* width: 20%; */
  }
  .text-center{
    text-align:center;
  }
  .text-left{
    text-align:left;
  }
  .text-right{
    text-align:right;
  }
  .badge{
    font-weight:600;
    padding:3px 5px;
    font-size:12px;
    margin-top:1px;
    display:inline-block;
    line-height: 1;
    text-align:center;
    white-space:nowrap;
    vertical-align: baseline;
    border-radius:.25rem;
  }
  .container-fluid {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
  }
  .text-capitalize{
    text-transform:capitalize;
  }
  .text-justify{
    text-align:justify;
  }
  .text-right{
    text-align:right;
  }
  .text-left{
    text-align:left;
  }
  .badge-primary{
    background-color: #039cfd;
    color:#fff;
  }
  @page { margin: 100px 15px; }
  #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 60px;
    background-size: auto 20px;
    margin: 5px;
  }
  #footer { position: fixed; left: 0px; bottom: -115px; right: 0px; height: 108px;
  }
  #footer .page:after { content: counter(page, upper-roman); }
  </style>
</head>
<body>
  <?php
  $mytime = Carbon\Carbon::now();
  // $mytime = $mytime->format('d-m-Y h:m:s');
  $mytime = $mytime->format('d-m-Y');

  // echo $mytime->toDateTimeString();
  ?>
  <div id="header">
    <img style="float:left;height:60px;" src="http://rec.vtelca.gob.ve/img/cintillo-i.png" />
    <img style="float:right;height:60px;" src="{{url('/images/logo/logo-vtelca.jpg')}}" />
    <!-- <img style="float:right;height:60px;" src="http://rec.vtelca.gob.ve/img/cintillo-c.png" /> -->
    <br><br><br>
    <hr style="color:red">
    <!-- <div class="text-left" style="float:left">
      <strong >Reporte emitido por:</strong> {{Auth::user()->name}}
    </div>
    <div class="text-right" style="float:right">
      <strong>Fecha del reporte:</strong>{{$mytime}}
    </div> -->
  </div>
  <div id="footer">
    <hr style="color:red">
    <p style="font-size:12px;text-align:center;">Av. Bolivar, calle de servicio 4, Meseta de Guaranao, galpones 5-5, 5-6, Zona Franca Industrial, Comercial y de Servicio de Paraguaná, C.A. Estado Falcón</p>
    <p class="page" style="text-align:center;font-size:12px"><strong>Teléfonos: </strong>0269-2482907. Página: </p>
  </div>
  <div class="container-fluid">
    <!-- CONTENIDO -->
    <table>
      <tr>
        <th style="color:black;" class="text-right" colspan="4">
          <strong>COTIZACIÓN N°: {{str_pad($oficio->oferta_id,8,"0",STR_PAD_LEFT)}}</strong>
        </th>
      </tr>
      <!-- 1 FILA CONTENIDO -->
      <tr>
        <th colspan="1" style="background-color:gray;">
          <strong>TELÉFONO:</strong>
        </th>
        <th colspan="1" class="text-capitalize text-left">
          {{$oficio->personal_natural->telefono}}
        </th>
        <th colspan="1" style="background-color:gray;">
          <strong>CIUDAD Y FECHA:</strong>
        </th>
        <th class="text-left">
          PUNTO FIJO {{$mytime}}
        </th>
      </tr>
      <!-- 2 FILA CONTENIDO: CEDULA, SOLICITADO POR: -->
      <tr>
        <th colspan="1" style="background-color:gray;">
          <strong>CÉDULA:</strong>
        </th>
        <th colspan="1" class="text-capitalize text-left">
          {{$oficio->personal_natural->cedula}}
        </th>
        <th colspan="1" style="background-color:gray;">
          <strong>SOLICITADO POR:</strong>
        </th>
        <th colspan="1" class="text-capitalize text-justify">
          {{$oficio->personal_natural->nombre_completo}}
        </th>
      </tr>
      <!-- 3 fila contenido: correo -->
      <tr>
        <th colspan="1" style="background-color:gray;">
          <strong>CORREO:</strong>
        </th>
        <th colspan="3" class="text-justify">
          {{$oficio->personal_natural->correo_electronico}}
        </th>
      </tr>
      <!-- CABECERA CUADO DE PRODUCTOS -->
      <tr>
        <th colspan="3">CON GUSTO COTIZAMOS A USTEDES LOS SIGUIENTES ARTÍCULOS.</th>
      <th colspan="1">Tasa petro: {{ $oficio->tasa_dia_petro }} Bs.</th>
      </tr>
      <tr>
        <th style="background:gray;color:black;">

          <strong>ARTÍCULOS</strong>

        </th>
        <th style="background:gray;color:black;">
          <strong>CANTIDAD</strong>
        </th>
        <th style="background:gray;color:black;">
          <strong>PRECIO UNITARIO</strong>
        </th>
        <th style="background:gray;color:black;">
          <strong>VALOR EN PETRO</strong>
        </th>
      </tr>
      @php
      $subtotal=0;
      $subtotal_petro=0;
      @endphp
      @foreach($oficio->productos as $producto)
      <tr>
        <td>{{$producto->descripcion}} - {{$producto->modalidad}}</td>
        <td>{{$producto->cantidad}}</td>

        <!-- <td>{{$producto->codigo}} - {{$producto->modalidad}}</td> -->
        <!-- <td>{{$producto->descripcion}}</td> -->
        <td>{{$producto->preciop}}</td>
        <td>{{$producto->preciop*$producto->cantidad}}</td>
      </tr>
      @php
      $subtotal=$subtotal+$producto->precio*$producto->cantidad;
      $subtotal_petro=$subtotal_petro+$producto->preciop*$producto->cantidad;
      @endphp
      @endforeach
      <tr>
        <td colspan="4" style="text-align:right">
          <strong>SUB-TOTAL</strong>
        </td>
        <td>{{$subtotal_petro}}</td>
      </tr>
      @php
      $iva=($subtotal*16)/100;
      $iva_petro=($subtotal_petro*16)/100;
      @endphp
      <tr>
        <td colspan="4" style="text-align:right">
          <strong>I.V.A 16 %</strong>
        </td>
        <td>{{$iva_petro}}</td>
      </tr>
      <tr>
        <td colspan="4" style="text-align:right">
          <strong>TOTAL</strong>
        </td>
        <td>{{$subtotal_petro+$iva_petro}}</td>
      </tr>
      <!-- OBSERVACIONES  -->
      <tr>
        <th colspan="4" style="color:black;text-align:left">
          <strong>CONDICIONES GENERALES DE NEGOCIACIÓN:</strong>
          <br>
          <ul style="text-align:justify">
            @foreach(App\Modelos\ControlOficios\Recaudo as $recaudo)
              <li>{{$recaudo->nombre}}</li>
            @endforeach
          </ul>
        </th>
      </tr>
      <tr>

        <th colspan="4" style="color:black;text-align:left">
          <strong>OBSERVACIONES:</strong>
          <br>
          <ul style="text-align:justify">
            @foreach($observaciones as $observacion)
              <li>{{$observacion->descripcion}}</li>
            @endforeach
          </ul>
        </th>
      </tr>
      <tr>
        <th colspan="4" style="text-align:center;">
          <strong>
            ATENTAMENTE;
          </strong>
        </th>
      </tr>
      <tr>
        <th colspan="4" style="text-align:center;">
          <p style="text-align: center; margin-bottom: -30px;">
            <img style="width: 60px; height: 60px;" src="{{ public_path('/images/firma_hugo.png') }}">
          </p>
          <br><br>
          <strong>
            HUGO GARCÍA.
          </strong>
          <br>
          <strong>
            PRESIDENCIA
          </strong>
          <br>
          <strong>
            VENEZOLANA DE TELECOMUNICACIONES C.A.
          </strong>
        </th>
      </tr>
    </table>
    <!-- CONTENIDO -->
  </div>
</body>
</html>
