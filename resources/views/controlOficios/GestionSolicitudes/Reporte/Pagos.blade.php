<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style media="screen">
  body{
    margin: 0mm 8mm 2mm 8mm;
  }
  /* table{
    table-layout: fixed;
  } */

  table, th, td {
    /*Agregado */
    width:100%;
    height:auto;
    margin:10px 0 10px 0;
    border-collapse:collapse;
    /*Agregado */
    border: 1px solid black;
    text-align: center;
    font-size:12px;
  }
  table td,th{
    border:1px solid black;
  }
  .tabla_std2 {
    width: 100%;
    margin: 0 auto;
    border: solid 1px black;
    border-collapse: collapse;
  }

  .tabla_std2 th, td, tr {
    border: solid 1px black;
    border-collapse: collapse;
  }

  .tabla_std2 td, th {
    /*padding: 3px 8px;*/
    text-align: center;
  }

  .tabla_std2 th {
    font-weight: bold;
    text-align: center;
  }

  .tabla_std2 td {
    /* width: 20%; */
  }
  .text-center{
    text-align:center;
  }
  .text-left{
    text-align:left;
  }
  .text-right{
    text-align:right;
  }
  .badge{
    font-weight:600;
    padding:3px 5px;
    font-size:12px;
    margin-top:1px;
    display:inline-block;
    line-height: 1;
    text-align:center;
    white-space:nowrap;
    vertical-align: baseline;
    border-radius:.25rem;
  }
  .container-fluid {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
  }
  .text-capitalize{
    text-transform:capitalize;
  }
  .text-justify{
    text-align:justify;
  }
  .text-right{
    text-align:right;
  }
  .text-left{
    text-align:left;
  }
  .badge-primary{
    background-color: #039cfd;
    color:#fff;
  }
  @page { margin: 100px 15px; }
  #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 60px;
    background-size: auto 20px;
    margin: 5px;
  }
  #footer { position: fixed; left: 0px; bottom: -115px; right: 0px; height: 108px;
  }
  #footer .page:after { content: counter(page, upper-roman); }
  </style>
</head>
<body>
  <?php
  $mytime = Carbon\Carbon::now();
  // $mytime = $mytime->format('d-m-Y h:m:s');
  $mytime = $mytime->format('d-m-Y');

  // echo $mytime->toDateTimeString();
  ?>
  <div id="header">
    <img style="float:left;height:60px;" src="http://rec.vtelca.gob.ve/img/cintillo-i.png" />
    <img style="float:right;height:60px;" src="{{url('/images/logo/logo-vtelca.jpg')}}" />
    <br><br><br>
    <hr style="color:red">
    <div class="text-left" style="float:left">
      <strong >Reporte emitido por:</strong> {{Auth::user()->name}}
    </div>
    <div class="text-right" style="float:right">
      <strong>Fecha del reporte:</strong>{{$mytime}}
    </div>
  </div>
  <div id="footer">
    <hr style="color:red">
    <p style="font-size:12px;text-align:center;">Av. Bolivar, calle de servicio 4, Meseta de Guaranao, galpones 5-5, 5-6, Zona Franca Industrial, Comercial y de Servicio de Paraguaná, C.A. Estado Falcón</p>
    <p class="page" style="text-align:center;font-size:12px"><strong>Teléfonos: </strong>0269-2482907. Página: </p>
  </div>
  <div class="container-fluid">
    <!-- CONTENIDO -->
    <table>
      <tr>
        <th style="color:black;" class="text-right" colspan="4">
          <strong>COTIZACIÓN N°: {{str_pad($oficio->oferta_economica->id,8,"0",STR_PAD_LEFT)}}</strong>
        </th>
      </tr>
      <!-- 1 FILA CONTENIDO -->
      <tr>
        <th colspan="1" style="background-color:gray;">
          <strong>TELÉFONO:</strong>
        </th>
        <th colspan="1" class="text-capitalize text-left">
          {{$oficio->personal_natural->telefono}}
        </th>
        <th colspan="1" style="background-color:gray;">
          <strong>CIUDAD Y FECHA:</strong>
        </th>
        <th class="text-left">
          PUNTO FIJO {{$oficio->created_at->format('d-m-Y')}}
        </th>
      </tr>
      <!-- 2 FILA CONTENIDO: CEDULA, SOLICITADO POR: -->
      <tr>
        <th colspan="1" style="background-color:gray;">
          <strong>Documento de Identificación:</strong>
        </th>
        <th colspan="1" class="text-capitalize text-left">
          @if($oficio->personal_juridico)
            {{$oficio->personal_juridico->rif_cedula_situr}} - Empresa : {{$oficio->personal_juridico->nombre_institucion}}
          @else
            {{$oficio->personal_natural->cedula}}
          @endif
        </th>
        <th colspan="1" style="background-color:gray;">
          <strong>SOLICITADO POR:</strong>
        </th>
        <th colspan="1" class="text-capitalize text-justify">
          {{$oficio->personal_natural->nombre}} {{$oficio->personal_natural->apellido}}
        </th>
      </tr>
      <!-- CABECERA CUADO DE PRODUCTOS -->
      <tr>
        <th colspan="4">SOPORTES DE PAGO</th>
      </tr>

    </table>
    <!-- CONTENIDO -->

    <table>
      <tr>
        <th colspan="3">PAGOS POR PUNTO DE VENTA</th>
      </tr>
        <tr>
          <td>
            <strong>
              Referencia de pago</td>
            </strong>
          <td>
            <strong>
              Monto
            </strong>
          </td>
          <td>
            <strong>
              Soporte / Número de lote
            </strong>
          </td>
        </tr>
      <tbody>
        @foreach($oficio->oferta_economica->pagos as $pago)
        @if($pago->tipo_pago!="transferencia")
        <tr>
          <td>{{$pago->referencia}}</td>
          <td>{{$pago->monto}}</td>
          <td>{{$pago->soporte_lote}}</td>
        </tr>
        @endif
        @endforeach
      </tbody>
    </table>

    <hr>

    <table>
      <tr>
        <th colspan="3">PAGOS POR TRANSFERENCIA</th>
      </tr>
    </table>
    @foreach($oficio->oferta_economica->pagos as $pago)
      @if($pago->tipo_pago=="transferencia")
      <table>
        <thead>
          <tr>
            <td>Referencia de pago</td>
            <td>Monto</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{{$pago->referencia}}</td>
            <td>{{$pago->monto}}</td>
          </tr>
        </tbody>
      </table>
      <div style="text-align:center;">
        <h3>Soporte:</h3>
        <img style="max-width:100%;max-height:500px;" src="{{url($pago->soporte_lote)}}" alt="">
      </div>
      @endif
    @endforeach

  </div>


</body>
</html>
