@extends('layouts.app')
@section('contenido')
<section id="register">
<div class="card" >
   <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">CONFIGURACIÓN DE TIEMPO DE ATENCIÓN</h5>
      <div class="row justify-content-center">
         <div class="col-12 col-md-3 col-lg-3">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="tiempo">Tiempo:</label>
               <input id="tiempo" class="form-control" v-bind:class="{ 'is-invalid': tiempoRequerido }"  maxlength="5"  type="text" v-model="data.tiempo"  onkeypress="return soloNumeros(event)" placeholder="Ingrese Tiempo"required >
            </div>
         </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
         <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
         <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
         <div id="tabla" v-show='TiempoAtencion.length'>
            <div class="row">
               <div class="header mx-auto">
                  <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE TIEMPO DE ATENCIÓN</h4>
               </div>
            </div>
            <div class="row">
               <div class="container-fluid">
                  <div  class="mx-auto">
                    <div class="row">
                      <div class="col-12 col-md-9 col-lg-9">
                      <br>
                      <div class="mt-2 form-inline font-weight-bold">
                        Mostrar
                        <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                           <option v-for="option in optionspageSize" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
                         registros
                         </div>
                      </div>
                      <div class="col-12 col-md-3 col-lg-3">

                        <div class="form-group">
                         <label for="Buscar" class="font-weight-bold">Buscar:</label>
                         <input id="search" class="form-control form-control-sm"   maxlength="5"  type="text" v-model="search"  onkeypress="return soloNumeros(event)"  v-on:keyup="filtrar" required>
                       </div>
                      </div>
                    </div>
                     <table  class="table table-bordered">
                        <thead class="bg-primary text-white">
                           <tr>
                              <th scope="col" style="cursor:pointer;" class="text-center" @click="sort('tiempo')">Tiempo</th>
                              <th scope="col" class="text-center">Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr v-for="(tiempos,index) in tiempo_atencion_registradas" v-show='statusFiltro'>
                              <td class="text-center">@{{tiempos.tiempo}}</td>
                              <td class="text-center align-middle">
                                 <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarData(tiempos)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                                 <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="datoBorrar(tiempos.id)"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button>
                              </td>
                           </tr>
                           <tr v-show='!statusFiltro'>
                              <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="">
                        <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{TiempoAtencion.length}}</strong>
                        <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                        <div style="float:right">
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                           <div class="row ml-2">
                              <strong>Página:  @{{currentPage}}</strong>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

 <!-- The Modal Editar -->
 <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">EDICIÓN TIEMPO DE ATENCIÓN</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">

              <div class="row">
                <div class="col">
                <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="tiempo">Tiempo:</label>
               <input class="form-control" v-bind:class="{ 'is-invalid': tiempoRequerido }" onkeypress="return soloNumeros(event)"  maxlength="5"  type="text" placeholder="Ingrese tiempo" v-model="tiempo" required>
            </div>
                </div>

              </div>
          </div>
          <!-- Modal footers -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="editarTiempo()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>

        </div>
      </div>
    </div>

<!-- The Modal Eliminar -->
<div class="modal" id="myModalEliminar">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header bg-primary">
            <h5 class="modal-title mx-auto  text-white">
            ELIMINAR MOTIVO</h4>
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <div class="mx-auto text-center">
               ¿Esta seguro de eliminar este registro?
            </div>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminarTiempo()">Sí, estoy seguro.</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </div>
</div>
</section>
@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              errors: [],
              data:{
                  tiempo:'',
              },
              id:'',
              tiempo:'',
              tiempoRequerido:false,
              TiempoAtencion:{!! $TiempoAtencion ? $TiempoAtencion : "''"!!},
              search:'',
              currentSort:'tiempo',//campo por defecto que tomara para ordenar
              currentSortDir:'asc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,

        },
        computed:{

                tiempo_atencion_registradas:function() {
                     this.rows=0;
                     return this.TiempoAtencion.sort((a,b) => {
                        let modifier = 1;
                        if(this.currentSortDir === 'desc')
                           modifier = -1;
                        if(a[this.currentSort] < b[this.currentSort])
                           return -1 * modifier;
                        if(a[this.currentSort] > b[this.currentSort])
                           return 1 * modifier;
                    return 0;
                 }).filter((row, index) => {
                    let start = (this.currentPage-1)*this.pageSize;
                    let end = this.currentPage*this.pageSize;
                    if(index >= start && index < end){
                        this.rows+=1;
                        return true;
                    }
                });
                },
        },
        methods:{
                 filtrar:function(){
                   let filtrardo=[];
                   if(this.search){
                     for(let i in this.TiempoAtencion){
                       if(this.TiempoAtencion[i].tiempo.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                        this.statusFiltro=1;
                        filtrardo.push(this.TiempoAtencion[i]);
                       }//if(this.TiempoAtencion[i].tiempo.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
                     }//for(let i in this.TiempoAtencion)
                     if(filtrardo.length)
                      this.TiempoAtencion=filtrardo;
                     else{
                        this.statusFiltro=0;
                        this.TiempoAtencion={!! $TiempoAtencion ? $TiempoAtencion : "''"!!};
                      }// if(filtrardo.length)
                   }else{
                     this.statusFiltro=1;
                     this.TiempoAtencion={!! $TiempoAtencion ? $TiempoAtencion : "''"!!};
                   }//if(this.search)
                 },// filtrar:function()

                 limpiar:function(){
                     this.error=[];
                     this.data={
                      tiempo:'',
                    };
                    this.tiempoRequerido=false;
                 },//limpiar:function()

                 buscar:function(){
                     for (let i in this.TiempoAtencion){
                         if(this.TiempoAtencion[i].tiempo==this.data.tiempo){
                            return(1);
                         }//if(this.TiempoAtencion[i].tiempo==this.data.tiempo)
                     }//for (let i in this.TiempoAtencion)
                     return(0);
                 },//buscar:function()

                 registrar:function(){
                     this.errors=[];
                     this.tiempoRequerido=false;
                     if (!this.data.tiempo) {
                         this.errors.push("Existen campos por llenar:<br>*Tiempo.");
                         this.tiempoRequerido=true;
                     } else if (this.buscar()==1) {
                         this.errors.push('No se puede duplicar el campo tiempo.');
                         this.tiempoRequerido=true;
                     } else if (this.data.tiempo.toString().length>5) {
                         this.errors.push('El campo tiempo acepta un máximo de 5 caracteres.');
                         this.tiempoRequerido=true;
                     }// if (!this.data.tiempo)


                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       /*Envio de datos a la base de datos*/
                       let self = this;
                       let tiempo=this.data.tiempo;
                       axios.post('{{ url("registrar_tiempo_atencion") }}', {
                        tiempo: tiempo,
                       })
                       .then(function (response) {
                         if(response.data.status=="success"){
                            self.TiempoAtencion=response.data.TiempoAtencion;
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success('Registro Satisfactorio.');
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                         }//if(response.data.status=="error")

                       })
                       .catch(function (error) {
                         console.log(error);
                       });
                       /*Fin envio de datos a la base de datos*/


                     }//if(this.error.length)
                 },
                 capturarData: function(tiempo){
                    this.id=tiempo.id;
                    this.tiempo=tiempo.tiempo;
                    this.tiemponRequerido=false;
                 },
                 editarTiempo:function (){
                    let self = this;
                    let tiempo=this.tiempo;
                    this.errors=[];
                    this.tiemponRequerido=false;
                     if (!tiempo) {
                         this.errors.push("Existen campos por llenar:<br>*Tiempo.");
                         this.tiempoRequerido=true;
                     } else if (tiempo.toString().length>5) {
                      this.tiempoRequerido=true;
                         this.errors.push('El campo tiempo acepta un máximo de 5 caracteres.');
                     }// if (!tiempo)

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       axios.post('{{ url("modificar_tiempo_atencion") }}', {tiempo:tiempo,id:this.id}).then(response => {
                        if(response.data.status=="success"){
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModal').modal('toggle');
                            self.TiempoAtencion=response.data.TiempoAtencion;
                        }//if(response.data.status=="success")
                        if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                        }//if(response.data.status=="error")
                       }).catch(error => {
                        alertify.error(""+error);
                       });
                     }//if(this.errors.length>0)
                 },
                 datoBorrar:function(id){
                    this.id=id;
                 },
                 eliminarTiempo: function(){
                    let self = this;
                      axios.post('{{ url("eliminar_tiempo_atencion") }}', {id:this.id}).then(response => {
                        if(response.data.status=="success"){
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModalEliminar').modal('toggle');
                            self.TiempoAtencion=response.data.TiempoAtencion;
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            $('#myModalEliminar').modal('toggle');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                         }//if(response.data.status=="error")
                      }).catch(error => {
                         console.log(error);
                      });
                 },
                 nextPage:function() {
                    if((this.currentPage*this.pageSize) < this.TiempoAtencion.length) this.currentPage++;
                 },
                 prevPage:function() {
                     if(this.currentPage > 1) this.currentPage--;
                 },
                 sort:function(s) {
                  //if s == current sort, reverse
                     if(s === this.currentSort) {
                        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
                     }
                  this.currentSort = s;
                 }

        },//methods
   });//const app= new Vue
</script>
@endpush
