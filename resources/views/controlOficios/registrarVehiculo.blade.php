@extends('layouts.app')
@section('contenido')
<section id="register">
<div class="card" >
   <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">REGISTRO DE VEHÍCULOS</h5>
      <div class="row justify-content-center text-center">
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
               <i class="fas fa-asterisk"></i>
               <label for="modelo">Vehículo:</label>
               <input id="modelo" class="form-control" v-bind:class="{ 'is-invalid': modeloRequerido }" minlength="6" maxlength="45"  type="text" v-model="data.modelo"  onkeypress="return soloLetrasNumerosSinEspacios(event)" placeholder="Ingrese vehículo" required >
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
               <i class="fas fa-asterisk"></i>
               <label for="placa">Placa:</label>
               <input id="placa" class="form-control" v-bind:class="{ 'is-invalid': placaRequerido }" minlength="6" maxlength="7"  type="text" v-model="data.placa"   onkeypress="return soloLetrasNumerosConEspacios(event)" placeholder="Ingrese placa" required >
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
               <label for="descripcion">Descripción:</label>
               <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" minlength="3" maxlength="200"  type="text" v-model="data.descripcion" onkeypress="return soloLetras(event)"  pattern="[a-zA-Z]*" placeholder="Ingrese descripción" required>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
         <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
         <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
         <div id="tabla" v-show='Vehiculos.length'>
            <div class="row">
               <div class="header mx-auto">
                  <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE VEHÍCULOS</h4>
               </div>
            </div>
            <div class="row">
               <div class="container-fluid">
                  <div  class="mx-auto">
                    <div class="row">
                      <div class="col-12 col-md-9 col-lg-9">
                      <br>
                      <div class="mt-2 form-inline font-weight-bold">
                        Mostrar
                        <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                           <option v-for="option in optionspageSize" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
                         registros
                         </div>
                      </div>
                      <div class="col-12 col-md-3 col-lg-3">

                        <div class="form-group">
                         <label for="Buscar" class="font-weight-bold">Buscar:</label>
                         <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                       </div>
                      </div>
                    </div>
                    <div id="no-more-tables">
                           <table  class="table table-bordered table-condensed cf table-striped">
                        <thead class="bg-primary text-white">
                           <tr>
                              <th scope="col" style="cursor:pointer; width:25%;" class="text-center" @click="sort('modelo')">Vehículo</th>
                              <th scope="col" style="cursor:pointer; width:25%;" class="text-center" @click="sort('placa')">Placa</th>
                              <th scope="col" style="cursor:pointer; width:25%;" class="text-center" @click="sort('descripcion')">Descripción</th>
                              <th scope="col" class="text-center width:25%;">Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr v-for="(registro,index) in registros" v-show='statusFiltro'>
                              <td class="text-justify"  data-title="Modelo"><p class="anchoparrafo">@{{registro.modelo}}</p></td>
                              <td  class="text-justify" data-title="Placa"><p class="anchoparrafo">@{{registro.placa}}</p></td>
                              <td  class="text-justify" data-title="Descripción"><p class="anchoparrafo">@{{registro.descripcion | placeholder('Sin información') }}</p></td>
                              <td class="text-center align-middle" data-title="Acción">
                                 <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarData(registro)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                              </td>
                           </tr>
                           <tr v-show='!statusFiltro'>
                              <td class="text-justify font-weight-bold" colspan="4">No se encontraron registros</td>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                    </div>
                     <div class="">
                        <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Vehiculos.length}}</strong>
                        <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                        <div style="float:right">
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                           <div class="row ml-2">
                              <strong>Página:  @{{currentPage}}</strong>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

 <!-- The Modal Editar -->
 <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">EDICIÓN DE VEHÍCULO</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">

             <div class="row justify-content-center text-center">
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
               <i class="fas fa-asterisk"></i>
               <label for="modelo">Vehículo:</label>
               <input id="modelo" class="form-control" v-bind:class="{ 'is-invalid': modeloRequerido }" minlength="6" maxlength="45"  type="text" v-model="modelo"  onkeypress="return soloLetrasNumerosSinEspacios(event)" placeholder="Ingrese vehículo" required >
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
               <i class="fas fa-asterisk"></i>
               <label for="placa">Placa:</label>
               <input id="placa" class="form-control" v-bind:class="{ 'is-invalid': placaRequerido }" minlength="3" maxlength="45"  type="text" v-model="placa"   onkeypress="return soloLetrasNumerosConEspacios(event)" placeholder="Ingrese placa" required >
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
               <label for="descripcion">Descripción:</label>
               <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" minlength="3" maxlength="200"  type="text" v-model="descripcion" onkeypress="return soloLetras(event)"  pattern="[a-zA-Z]*" placeholder="Ingrese descripción" required>
            </div>
         </div>
      </div>
          </div>
          <!-- Modal footers -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="editar()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>

        </div>
      </div>
    </div>

<!-- The Modal Eliminar -->
<div class="modal" id="myModalEliminar">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header bg-primary">
            <h5 class="modal-title mx-auto  text-white">
            ELIMINAR VEHÍCULO</h4>
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <div class="mx-auto text-center">
               ¿Esta seguro de eliminar este registro?
            </div>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminar()">Sí, estoy seguro.</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </div>
</div>
</section>

@endsection

@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              errors: [],
              data:{
                  modelo:'',
                  placa:'',
                  descripcion:'',
              },
              id:'',
              modelo:'',
              placa:'',
              descripcion:'',
              modeloRequerido:false,
              placaRequerido:false,
              descripcionRequerido:false,
              Vehiculos:{!! $Vehiculos ? $Vehiculos : "''"!!},
              search:'',
              currentSort:'modelo',//campo por defecto que tomara para ordenar
              currentSortDir:'asc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,

        },
        computed:{

                registros:function() {
                     this.rows=0;
                     return this.Vehiculos.sort((a,b) => {
                        let modifier = 1;
                        if(this.currentSortDir === 'desc')
                           modifier = -1;
                        if(a[this.currentSort] < b[this.currentSort])
                           return -1 * modifier;
                        if(a[this.currentSort] > b[this.currentSort])
                           return 1 * modifier;
                    return 0;
                 }).filter((row, index) => {
                    let start = (this.currentPage-1)*this.pageSize;
                    let end = this.currentPage*this.pageSize;
                    if(index >= start && index < end){
                        this.rows+=1;
                        return true;
                    }
                });
                },
        },
        methods:{
                 filtrar:function(){
                   let filtrardo=[];
                   if(this.search){
                     for(let i in this.Vehiculos){
                        if(this.Vehiculos[i].placa!=null)
                         var descripcion="";
                       if(this.Vehiculos[i].modelo.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Vehiculos[i].placa.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                        this.statusFiltro=1;
                        filtrardo.push(this.Vehiculos[i]);
                       }//  if(this.Vehiculos[i].modelo.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Vehiculos[i].placa.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Vehiculos[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
                     }//for(let i in this.Vehiculos)
                     if(filtrardo.length)
                      this.Vehiculos=filtrardo;
                     else{
                        this.statusFiltro=0;
                        this.Vehiculos={!! $Vehiculos ? $Vehiculos : "''"!!};
                      }// if(filtrardo.length)
                   }else{
                     this.statusFiltro=1;
                     this.Vehiculos={!! $Vehiculos ? $Vehiculos : "''"!!};
                   }//if(this.search)
                 },// filtrar:function()

                 limpiar:function(){
                     this.error=[];
                     this.data={
                      modelo:'',
                      placa:'',
                      descripcion:'',
                    };
                    this.modeloRequerido=false;
                    this.placaRequerido=false;
                    this.descripcionRequerido=false;
                 },//limpiar:function()

                 buscar:function(){
                     for (let i in this.Vehiculos){
                         if(this.Vehiculos[i].placa==this.data.placa){
                            return(1);
                         }// if(this.Vehiculos[i].placa==this.data.placa)
                     }//for (let i in this.Vehiculos)
                     return(0);
                 },//buscar:function()

                 registrar:function(){
                     this.errors=[];
                     this.modeloRequerido=false;
                     this.placaRequerido=false;
                     this.descripcionRequerido=false;

                     if (!this.data.placa) {
                         this.errors.push("Existen campos por llenar:<br>*Placa.");
                         this.placaRequerido=true;
                     } else if (this.buscar()==1) {
                         this.errors.push('No se puede duplicar el campo placa.');
                         this.placaRequerido=true;
                     } else if (this.data.placa.length<6 || this.data.placa>7) {
                         this.errors.push('El campo placa acepta un minimo de 6 caracteres y un máximo de 7 caracteres.');
                         this.placaRequerido=true;
                     }// if (!this.data.placa)

                     if (!this.data.modelo) {
                         this.errors.push("Existen campos por llenar:<br>*Modelo.");
                         this.modeloRequerido=true;
                     } else if (this.data.modelo.length<3 || this.data.modelo>45) {
                         this.errors.push('El campo modelo acepta un minimo de 3 caracteres y un máximo de 45 caracteres.');
                         this.modeloRequerido=true;
                     }// if (!this.data.modelo)                   

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       /*Envio de datos a la base de datos*/
                       let self = this;
                       let modelo=this.data.modelo.toLowerCase();                       
                       modelo=modelo.charAt(0).toUpperCase()+modelo.slice(1);
                       let placa=this.data.placa.toUpperCase();
                       let descripcion=this.data.descripcion.toLowerCase();
                       descripcion=descripcion.charAt(0).toUpperCase()+descripcion.slice(1);
                       axios.post('{{ url("registrar_vehiculo") }}', {
                        modelo: modelo,
                        placa: placa,
                        descripcion: descripcion,
                       })
                       .then(function (response) {
                         if(response.data.status=="success"){
                            self.Vehiculos=response.data.Vehiculos;
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success('Registro Satisfactorio.');
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                         }//if(response.data.status=="error")

                       })
                       .catch(function (error) {
                         console.log(error);
                       });
                       /*Fin envio de datos a la base de datos*/
                     }//if(this.error.length)
                 },
                 capturarData: function(registro){
                    this.id=registro.id;
                    this.modelo=registro.modelo;
                    this.placa=registro.placa;
                    this.descripcion=registro.descripcion;
                    this.modeloRequerido=false;
                    this.placaRequerido=false;
                    this.descripcionRequerido=false;
                 },
                 editar:function (){
                    let self = this;
                    let modelo=this.modelo.toLowerCase();
                    modelo=modelo.charAt(0).toUpperCase()+modelo.slice(1);
                    let placa=this.placa.toUpperCase();
                    if(this.descripcion==null)
                    this.descripcion="";
                    console.log(this.descripcion);
                    let descripcion=this.descripcion.toLowerCase();
                    descripcion=descripcion.charAt(0).toUpperCase()+descripcion.slice(1);
                    this.errors=[];
                    this.modeloRequerido=false;
                    this.descripcionRequerido=false;

                     if (!placa) {
                         this.errors.push("Existen campos por llenar:<br>*Placa.");
                         this.placaRequerido=true;
                     } else if (placa.length<6 || placa>7) {
                      this.placaRequerido=true;
                         this.errors.push('El campo placa acepta un minimo de 6 caracteres y un máximo de 7 caracteres.');
                     }// if (!modelo)

                     if (!modelo) {
                         this.errors.push("Existen campos por llenar:<br>*Modelo.");
                         this.modeloRequerido=true;
                     } else if (modelo.length<3 || modelo>45) {
                      this.modeloRequerido=true;
                         this.errors.push('El campo modelo acepta un minimo de 3 caracteres y un máximo de 45 caracteres.');
                     }// if (!modelo)                     

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       axios.post('{{ url("modificar_vehiculo") }}', {modelo:modelo,placa:placa,descripcion:descripcion,id:this.id}).then(response => {
                        if(response.data.status=="success"){
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModal').modal('toggle');
                            self.Vehiculos=response.data.Vehiculos;
                        }//if(response.data.status=="success")
                        if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                        }//if(response.data.status=="error")
                       }).catch(error => {
                        alertify.error(""+error);
                       });
                     }//if(this.errors.length>0)
                 },
                 datoBorrar:function(id){
                    this.id=id;
                 },
                 nextPage:function() {
                    if((this.currentPage*this.pageSize) < this.Vehiculos.length) this.currentPage++;
                 },
                 prevPage:function() {
                     if(this.currentPage > 1) this.currentPage--;
                 },
                 sort:function(s) {
                  //if s == current sort, reverse
                     if(s === this.currentSort) {
                        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
                     }
                  this.currentSort = s;
                 }

        },//methods
   });//const app= new Vue
</script>
@endpush
