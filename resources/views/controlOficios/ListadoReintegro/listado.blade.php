@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">Listado de Reintegro</h5>
      <div class="row justify-content-center">
        <div class="col-12 table-responsive" v-if="Oficios.length>0" >
          <div class="row">
            <div class="col-12 col-md-9 col-lg-9">
              <br>
              <div class="mt-2 form-inline font-weight-bold">
                Mostrar
                <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                  <option v-for="option in optionspageSize" v-bind:value="option.value">
                    @{{ option.text }}
                  </option>
                </select>
                registros
              </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <label for="Buscar" class="font-weight-bold">Buscar:</label>
                <input id="search" class="form-control form-control-sm" type="text" v-model="search" v-on:keyup="filtrar" required>
              </div>
            </div>
          </div>
          <table  class="table table-bordered text-center" >
            <thead class="bg-primary text-white">
              <tr>
                <td>N°</td>
                <td>Persona/Empresa</td>
                <td>Monto</td>
                <td>Banco</td>
                <td>Acción</td>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(oficio,index) in oficios_registrados" v-show="statusFiltro==1">
                <td>@{{index+1}}</td>
                <td>@{{oficio.solicitante_ente}}</td>
                <td>@{{oficio.pago_reembolso}}</td>
                <td>@{{oficio.banco}}</td>
                <td>
                  <button type="button" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#modal_reintegro" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Reembolso" @click="capturarOficio(oficio.id)"><i class="fa fa-money-bill-wave" aria-hidden="true"></i></button>
                </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="5">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Oficios.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 text-center jumbotron" v-else>
            <h5>Sin re-integros que realizar</h5>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal_reintegro" v-if="Oficios.length>0">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header bg-primary text-center">
          <h4 class="modal-title mx-auto text-white">REINTEGRO</h4>
          <button type="button" title="Cerrar" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body row justify-content-center">
          <div class="col-12 text-center">
            <h4>Datos para el reembolso</h4>
          </div>
          <div class="col-md-4 text-center">
            <i class="fa fa-asterisk"></i>
            <strong>Número de cuenta:</strong>
            <input type="text" style="text-align:center;" class="form-control" v-model="Oficios[indexOficio].num_cuenta" readonly>
          </div>
          <div class="col-md-4 text-center">
            <i class="fa fa-asterisk"></i>
            <strong>Tipo de cuenta:</strong>
            <input type="text" style="text-align:center;" v-if="Oficios[indexOficio].tipo_cuenta==1" class="form-control text-capitalize" :value="'Corriente'" readonly maxlength="20">
            <input type="text" style="text-align:center;" v-if="Oficios[indexOficio].tipo_cuenta==2" class="form-control text-capitalize" :value="'Ahorro'" readonly maxlength="20">
          </div>
          <div class="col-md-4 text-center">
            <i class="fa fa-asterisk"></i>
            <strong>Identificación:</strong>
            <input type="text" style="text-align:center;" class="form-control" v-model="Oficios[indexOficio].identificacion" readonly maxlength="20">
          </div>
          <div class="col-md-6 text-center">
            <i class="fa fa-asterisk"></i>
            <strong>Nombre:</strong>
            <input type="text" style="text-align:center;" class="form-control text-capitalize" v-model="Oficios[indexOficio].nombre" readonly maxlength="20">
          </div>
          <div class="col-md-6 text-center">
            <i class="fa fa-asterisk"></i>
            <strong>Correo electrónico:</strong>
            <input type="text" style="text-align:center;" class="form-control" v-model="Oficios[indexOficio].correo_electronico" readonly maxlength="20">
          </div>
          <div class="col-12 text-center">
            <br>
            <h4>Datos de reembolso</h4>
          </div>
          <div class="col-md-4 text-center">
            <i class="fa fa-asterisk"></i>
            <strong>Referencia de Pago:</strong>
            <input type="text" v-model="referencia_pago" @keypress="onlyNumber" :maxlength="12" class="form-control" value="">
          </div>
          <div class="col-md-4 text-center">
            <strong>
              <i class="fa fa-asterisk"></i>
              Monto:
            </strong>
            <input type="text" v-model="monto" @keypress="onlyNumber" :maxlength="15" class="form-control" >
          </div>
          <div class="col-md-4 text-center">
            <strong>
              <i class="fa fa-asterisk"></i>
              Soporte de Pago:
            </strong>
            <input type="file" @change="convertirBase64SoportePago" accept="image/*" class="form-control" >
          </div>
          <div class="col-12 text-center">
            <br>
            <button type="button" class="btn btn-success" name="button" @click="reembolsar()" >Registrar</button>
          </div>
          <div class="col-12 text-center table-responsive">
            <br>
            <h4>Listado de reembolsos</h4>
            <br>
            <p>
              <strong>Monto Total a Reembolsar:</strong>@{{Oficios[indexOficio].pago_reembolso}}
              <strong>Monto Reembolsado:</strong>@{{Oficios[indexOficio].pago_cancelado}}
              <strong>Monto Pendiente por reembolsar:</strong>@{{Oficios[indexOficio].pago_reembolso-Oficios[indexOficio].pago_cancelado}}
            </p>
            <table  class="table table-bordered text-center" >
              <thead class="bg-primary text-white">
                <tr>
                  <th>Referencia</th>
                  <th>Monto</th>
                  <th>Soporte</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="(pago,index) in Oficios[indexOficio].pagos" v-if="pago.tipo_pago=='reintegro'">
                  <td>@{{pago.referencia}}</td>
                  <td>@{{pago.monto}}</td>
                  <td>
                    <a :href="pago.soporte_lote" download>
                      <img :src="pago.soporte_lote" class="img-fluid img-responsive" alt="">
                    </a>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- Modal footers -->
        <!-- <div class="modal-footer mx-auto">
          <button type="button" class="btn btn-success" name="button" >Registrar.</button>
        </div> -->
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    Oficios:{!! $oficios ? $oficios : "''"!!},
    indexOficio:0,
    soporte_pago:'',
    monto:0,
    referencia_pago:0,
    search:'',
    currentSort:'solicitante_ente',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0,
  },
  computed:{
    oficios_registrados:function() {
      this.rows=0;
      return this.Oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    // Paginación
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        // console.log('entre');
        // console.log(this.search);
        for(let i in this.Oficios){
          if(this.Oficios[i].solicitante_ente.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.Oficios[i]);
          }//if(this.NivelAtencion[i].nivel.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.NivelAtencion)
        if(filtrardo.length>0){
          this.Oficios=filtrardo;
          this.statusFiltro=1;
        }
        else{
          this.statusFiltro=0;
          this.Oficios={!! $oficios ? $oficios : "''"!!};
        }// if(filtrardo.length)
      }else{
        // console.log('Search vacio');
        this.statusFiltro=1;
        this.Oficios={!! $oficios ? $oficios : "''"!!};
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.Oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      // console.log(s);
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
    // Paginación
    limpiar(){
      this.referencia_pago='',
      this.monto='',
      this.soporte_pago=''
    },
    capturarOficio:function(id,tipo_operacion=null){
      for(var i=0;i<this.Oficios.length;i++){
        if(this.Oficios[i].id==id){
          this.indexOficio=i;
        }//if this.oficio
      }//for
    },
    reembolsar(){
      var b=0;
      if(this.referencia_pago==""){
        b++;
        alertify.error('Debes ingresar el número de referencia del pago');
      }else if(this.monto==""){
        b++;
        alertify.error('Debes ingresar el monto del pago realizado');
      }else if(this.soporte_pago==""){
        b++;
        alertify.error('Debes ingresar el soporte de pago.');
      }
      if(b==0){
        axios.post('{{ route("guardarReembolso") }}', {
          oferta_economica_id:this.Oficios[this.indexOficio].oferta_economica_id,
          referencia_pago:this.referencia_pago,
          monto:this.monto,
          soporte_pago:this.soporte_pago
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Oficios[this.indexOficio].pagos.push(response.data.pago);
            this.Oficios[this.indexOficio].pago_cancelado=response.data.reembolsado;
            if(response.data.completado){
              this.Oficios.splice(this.indexOficio,1);
              $('#modal_reintegro').modal('toggle');
            }
            this.limpiar();
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }//b==0 no errors
    },
    convertirBase64SoportePago: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.soporte_pago = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    onlyNumber ($event) {
      //console.log($event.keyCode); //keyCodes value
      let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if (keyCode > 31 && (keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
        $event.preventDefault();
      }
    },
  },//methods
  mounted(){
    console.log('adsd');
  }
});//const app= new Vue
</script>
@endpush
