<!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#aprobarOficio">Open Modal</button> -->
<!-- Modal -->
<div class="modal fade" id="aprobarOficio" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title mx-auto text-white">Aprobación</h4>
      </div>
      <div class="modal-body text-center">
        <input type="radio" id="one" value="total" v-model="picked">
        <label for="one">Aprobación Total</label>
        <br>
        <input type="radio" id="two" value="parcial" v-model="picked">
        <label for="two">Aprobación Parcial</label>
        <br>
        <div class="row" v-if="picked=='parcial'">
          <div class="col-md-12 text-center">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead class="bg-primary text-white">
                  <th>Trabajador</th>
                  <th>Material</th>
                  <th>Cantidad de Solicitud</th>
                  <th>Acción</th>
                </thead>
                  <tbody v-if="tipoOficio=='juridico'">
                    <tr v-for="(materialesDonar, index) in personalJuridico[indexOficio].materiales">
                      <td>@{{nombreSolicitante}}</td>
                      <td>
                        <select class="form-control" :id="'select'+index" v-model="materialesDonar.materiales_id" @change="verificarDuplicados(materialesDonar.materiales_id, index)">
                          <option v-for="material in materiales" v-bind:value="material.id"> @{{material.nombre}}</option>
                        </select>
                        <td>
                          <input type="number" min="1" class="form-control" v-model="materialesDonar.cantidad" @keypress="onlyNumber">
                        </td>
                        <td>
                          {{--<button type="button" title="Eliminar" v-show="personalJuridico[indexOficio].materiales.length>1" class="btn btn-danger" name="button" @click="eliminarMaterial(index)"><i class="fa fa-trash"></i></button>--}}
                          <button type="button" title="Eliminar" v-show="personalJuridico[indexOficio].materiales.length>1" class="btn btn-danger" name="button" @click="personalJuridico[indexOficio].materiales.splice(index,1)"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                      <tr >
                        <td colspan="3"></td>
                        <td class="text-center"><button type="button" @click="personalJuridico[indexOficio].materiales.push({'cantidad':1,'registro_oficio_id':personalJuridico[indexOficio].oficios_id,'created_at':'2018-10-18 08:50:40','despachado':false,'id':0,'materiales_id':38,'updated_at':'2018-10-18 08:50:40','nombre_material':'goma espuma blanca'})" class="btn btn-info" name="button"><i class="fa fa-plus"></i></button></td>
                      </tr>
                    </tbody>
                    <tbody v-if="tipoOficio=='natural'">
                      <tr v-for="(materialesDonar, index) in personalNatural[indexOficio].materiales">
                        <td>@{{nombreSolicitante}}</td>
                        <td>
                          <select class="form-control" :id="'select'+index" v-model="materialesDonar.materiales_id">
                            <option v-for="material in materiales" v-bind:value="material.id"> @{{material.nombre}}</option>
                          </select>
                          <td>
                            <input type="number" min="1" class="form-control" v-model="materialesDonar.cantidad" @keypress="onlyNumber" >
                          </td>
                          <td>
                            <button type="button" title="Eliminar" v-show="personalNatural[indexOficio].materiales.length>1" class="btn btn-danger" name="button" @click="personalNatural[indexOficio].materiales.splice(index,1)"><i class="fa fa-trash"></i></button>
                          </td>
                        </tr>
                        <tr >
                          <td colspan="3"></td>
                          <td class="text-center"><button type="button" @click="personalNatural[indexOficio].materiales.push({'cantidad':1,'registro_oficio_id':personalNatural[indexOficio].oficios_id,'created_at':'2018-10-18 08:50:40','despachado':false,'id':0,'materiales_id':38,'updated_at':'2018-10-18 08:50:40','nombre_material':'goma espuma blanca'})" class="btn btn-info" name="button"><i class="fa fa-plus"></i></button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" @click="aprobarSolicitud()" data-dismiss="modal">Enviar.</button>
              <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> -->
            </div>
          </div>
        </div>
      </div>
