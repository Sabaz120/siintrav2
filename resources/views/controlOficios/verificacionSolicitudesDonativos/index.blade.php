@extends('layouts.app')
@section('contenido')
<style type="text/css">

  .red-collapse{
    background-color: #ef9a9a;
    margin-bottom: 20px;
    padding-top: 20px;

  }

  .fs-20{
    font-size: 20px;
  }

  .gift{
    font-size: 80px !important;
  }

  .custom-collapse{
    margin-left: -11px !important;
    margin-right: -11px !important;
  }

  .card-body{
    padding: 0px !important;
  }

  .material-row:nth-child(even) {
    border-left: 1px solid;
  }

  .material-row{
    padding: 1.5rem;
    border-bottom: 1px solid;
  }

  .offset-md-8{
    margin-top: -30px;
  }

</style>
<section id="gestion">

@include('controlOficios.verificacionSolicitudesDonativos.modal_aprobacion')
@include('controlOficios.verificacionSolicitudes.partials.modalRechazarOficio')

  <div class="card" >
    <div class="card-header text-white">
      <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#personalNatural" role="tab" aria-controls="home" aria-selected="true">Personal Natural <span class="badge badge-danger">@{{personalNatural.length}}</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#personalJuridico" role="tab" aria-controls="contact" aria-selected="false">Personal Jurídico <span class="badge badge-danger">@{{personalJuridico.length}}</span> </a>
        </li>
        <li class="nav-item">
          <a @click="tipoOficio='trabajador'" class="nav-link" id="contact-tab" data-toggle="tab" href="#personalTrabajador" role="tab" aria-controls="contact" aria-selected="false">Personal Trabajador <span class="badge badge-danger">@{{personalTrabajador.length}}</span> </a>
        </li>
      </ul>
    </div>
    <div class="card-body">

      <div class="tab-content" id="myTabContent">
        <!-- PESTAÑA PERSONAL NATURAL -->
        <div class="tab-pane fade show active" id="personalNatural" role="tabpanel" aria-labelledby="home-tab">

          <div class="container" v-if="personalNatural.length > 0">

            <div class="row red-collapse" v-for="(natural, index) in personalNatural" :id="'card'+natural.oficios_id">

              <div class="col-md-2">
                <p class="text-center">
                  <i class="fa fa-gift gift text-white" aria-hidden="true"></i>
                </p>
              </div>
              <div class="col-md-5">
                <p class="text-white fs-20">Fecha de Registro: @{{ natural.fecha_creacion }}</p>
                <p class="text-white fs-20">Cantidad: @{{ natural.cantidad_materiales }}</p>
              </div>
              <div class="col-md-5 text-white fs-20 text-capitalize">
                <p>Solicitante: @{{ natural.nombre }}</p>

              </div>
              <div class="col-md-4 offset-md-8">
                <button class="btn btn-success" @click="obtenerOficio(index, 'natural', natural.oficios_id)" data-toggle="modal" data-target="#aprobarOficio"><i class="fa fa-check" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-danger" name="button" data-toggle="modal" data-target="#rechazarOficio" @click="oficio_id=natural.oficios_id"><i class="fa fa-close"></i></button>
                <!-- <button class="btn btn-danger" @click="rechazar(natural.oficios_id)"><i class="fa fa-times" aria-hidden="true"></i></button> -->
              </div>
              <div class="col-md-12">
                <p class="text-center">
                  <a class="btn btn-primary" data-toggle="collapse" v-bind:href="'#collapseExample'+index" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                  </a>
                </p>
                <div class="collapse custom-collapse" v-bind:id="'collapseExample'+index">
                    <div class="card card-body">
                      <div class="container-fluid" style="max-width: 100% !important">
                        <div class="row">
                          <div class="col-md-6 material-row" v-for="(material, index) in natural.materiales">
                            <div style="float: left;">
                              Material: @{{ material.nombre_material }}
                            </div>
                            <div style="float: right;">
                              Cantidad: @{{ material.cantidad }}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>

            </div>

          </div>
          <div v-else class="row justify-content-center">
            <h3>No existe solicitudes pendientes por aprobar.</h3>
            <br><br><br><br><br>
          </div>

        </div> <!-- oficios persona natural -->
        <div class="tab-pane fade" id="personalJuridico" role="tabpanel" aria-labelledby="contact-tab">
          <!-- PESTAÑA PERSONAL JURIDICO -->

          <div class="container" v-if="personalJuridico.length > 0">

            <div class="row red-collapse" v-for="(juridico, index) in personalJuridico" :id="'card'+juridico.oficios_id">

              <div class="col-md-2">
                <p class="text-center">
                  <i class="fa fa-gift gift text-white" aria-hidden="true"></i>
                </p>
              </div>
              <div class="col-md-5">
                <p class="text-white fs-20">Fecha de Registro: @{{ juridico.fecha_creacion }}</p>
                <p class="text-white fs-20">Cantidad: @{{ juridico.cantidad_materiales }}</p>
              </div>
              <div class="col-md-5 text-white fs-20 text-capitalize">
                <p>Solicitante: @{{ juridico.nombre }}</p>

              </div>
              <div class="col-md-4 offset-md-8">
                <button class="btn btn-success" @click="obtenerOficio(index, 'juridico', juridico.oficios_id)" data-toggle="modal" data-target="#aprobarOficio"><i class="fa fa-check" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-danger" name="button" data-toggle="modal" data-target="#rechazarOficio" @click="oficio_id=juridico.oficios_id"><i class="fa fa-close"></i></button>
                <!-- <button class="btn btn-danger" @click="rechazar(juridico.oficios_id)"><i class="fa fa-times" aria-hidden="true"></i></button> -->
              </div>
              <div class="col-md-12">
                <p class="text-center">
                  <a class="btn btn-primary" data-toggle="collapse" v-bind:href="'#collapseExample'+index" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                  </a>
                </p>
                <div class="collapse custom-collapse" v-bind:id="'collapseExample'+index">
                    <div class="card card-body">
                      <div class="container-fluid" style="max-width: 100% !important">
                        <div class="row">
                          <div class="col-md-6 material-row" v-for="(material, index) in juridico.materiales">
                            <div style="float: left;">
                              Material: @{{ material.nombre_material }}
                            </div>
                            <div style="float: right;">
                              Cantidad: @{{ material.cantidad }}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>

            </div>

          </div>
          <div v-else class="row justify-content-center">
            <h3>No existe solicitudes pendientes por aprobar.</h3>
            <br><br><br><br><br>
          </div>


        </div>
        <div class="tab-pane fade" id="personalTrabajador" role="tabpanel" aria-labelledby="contact-tab">
          <!-- PESTAÑA PERSONAL TRABAJADOR -->
          <div class="row justify-content-center" v-if="personalTrabajador.length > 0" >
            <div class="card col-3 ml-2" v-for="(trabajador,index) in personalTrabajador" v-if="personalTrabajador[index].materiales.length>0">
              <!-- <img src="http://salasituacional.vtelca.gob.ve/img/cedulas/23586157.jpg" class="card-img-top" height="300" width="250" alt="img" style="width:100%"> -->
              <!-- <img src="http://salasituacional.vtelca.gob.ve/img/masculino.jpg" class="card-img-top" height="300" width="250" alt="img" style="width:100%"> -->
              <img @error="onImageLoadFailure($event)" :src="'http://salasituacional.vtelca.gob.ve/img/cedulas/'+trabajador.cedula+'.jpg'" class="card-img-top" height="300" width="250" alt="img" style="width:100%">
              <div class="text-center">
                <strong>
                  @{{trabajador.nombre}}
                </strong> <br>
                @{{trabajador.gerencia}}<br>
                @{{trabajador.cargo}} <br>
                <p>
                  <strong>Fecha de Solicitud:</strong>@{{ trabajador.fecha_creacion }} <br>
                </p>
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead class="bg-primary text-white">
                      <th>Material</th>
                      <th>Cantidad</th>
                      <th>Acción</th>
                    </thead>
                    <tbody v-if="tipoOficio=='trabajador'">
                      <tr v-for="(material,indexMaterial) in personalTrabajador[index].materiales" v-if="personalTrabajador[index].materiales.length>0">
                        <td>
                          <select class="form-control" v-model="material.materiales_id">
                            <option v-for="materialdb in materiales" v-bind:value="materialdb.id">@{{materialdb.nombre}}</option>
                          </select>
                        </td>
                        <td>
                          <input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="material.cantidad" >
                        </td>
                        <td>
                          <button type="button" title="Eliminar" v-show="personalTrabajador[index].materiales.length>1" class="btn btn-danger" name="button" @click="personalTrabajador[index].materiales.splice(indexMaterial,1)"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                      <tr >
                        <td colspan="2"></td>
                        <td class="text-center"><button type="button" @click="personalTrabajador[index].materiales.push({'cantidad':1,'registro_oficio_id':personalTrabajador[index].oficios_id,'created_at':'2018-10-18 08:50:40','despachado':false,'id':0,'materiales_id':38,'updated_at':'2018-10-18 08:50:40','nombre_material':'goma espuma blanca'})" class="btn btn-info" name="button"><i class="fa fa-plus"></i></button></td>
                      </tr>
                    </tbody>
                  </table>
                </div> <!-- div class table-responsive-->
              </div>
              <p style="text-align:right">
                <button type="button" name="button" title="Aprobar" @click="aprobarPersonalTrabajador(personalTrabajador[index])" class="btn btn-success">
                  <i class="fa fa-check"></i>
                </button>
                <button type="button" class="btn btn-danger" name="button" data-toggle="modal" data-target="#rechazarOficio" @click="oficio_id=personalTrabajador[index].oficios_id"><i class="fa fa-close"></i></button>
                <!-- <button type="button" name="button" title="Rechazar" @click="rechazar(personalTrabajador[index].oficios_id)" class="btn btn-danger">
                  <i class="fa fa-remove"></i>
                </button> -->
              </p>
            </div>
          </div>
          <div v-else class="row justify-content-center">
            <h3>No existe solicitudes pendientes por aprobar.</h3>
            <br><br><br><br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    oficio_id:0,
    personalTrabajador:{!! $personalTrabajador ? $personalTrabajador : "''" !!},
    personalJuridico:{!! $personalJuridico ? $personalJuridico : "''" !!},
    personalNatural:{!! $personalNatural ? $personalNatural : "''" !!},
    picked:'',
    materiales:{!! $materiales ? $materiales : "''" !!},
    materialesDonar:'',
    indexOficio:'',
    nombreSolicitante:'',
    tipoOficio:'',
    validated:false,
    index:0

  },
  computed:{

  },
  methods:{
    rechazarOficio(){
      var oficios=[];
      if(this.tipoOficio=="natural"){
        oficios=this.personalNatural;
      }else if(this.tipoOficio=="juridico"){
        oficios=this.personalJuridico;
      }else{
        oficios=this.personalTrabajador;
      }
      axios.post("{{ route('GestionSolicitudesDonativosRechazar') }}", {id: this.oficio_id}).then(reponse => {
        for(var i=0;i<oficios.length;i++){
          if(oficios[i].oficios_id==this.oficio_id){
            if(this.tipoOficio=="natural"){
              this.personalNatural.splice(i,1);
              alertify.success('Oficio rechazado satisfactoriamente');
            }else if(this.tipoOficio=="juridico"){
              this.personalJuridico.splice(i,1);
              alertify.success('Oficio rechazado satisfactoriamente');
            }else{
              this.personalTrabajador.splice(i,1);
              alertify.success('Oficio rechazado satisfactoriamente');
            }
          }
        }//for
        // window.location.reload();
      })
    },
    onlyNumber ($event) {
       //console.log($event.keyCode); //keyCodes value
       let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
       if (keyCode > 31 && (keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
          $event.preventDefault();
       }
    },
    onImageLoadFailure (event) {
        event.target.src = 'http://salasituacional.vtelca.gob.ve/img/masculino.jpg';
    },
    obtenerOficio(id, tipo, index){

      this.indexOficio = id;
      this.tipoOficio = tipo
      this.index = index

      if(tipo == 'trabajador')
        this.nombreSolicitante = this.personalTrabajador[id].nombre
      else if(tipo == 'juridico')
        this.nombreSolicitante = this.personalJuridico[id].nombre
      else
        this.nombreSolicitante = this.personalNatural[id].nombre
    },
    eliminarMaterial(indexMaterial){
      var material_id=this.personalJuridico[this.indexOficio].materiales[indexMaterial].id;
      axios.post("{{ route('GestionSolicitudesDonativosEliminar') }}", {id: material_id}).then(response => {

        if(response.data.error == 0){

          alertify.success(response.data.msg);
          this.personalJuridico[this.indexOficio].materiales.splice(indexMaterial,1);
          // if(this.tipoOficio == "trabajador")
          //   this.obtenerPersonalTrabajador()
          // else if(this.tipoOficio == 'natural')
          //   this.obtenerPersonalNatural()

        }else{

          alertify.error(response.data.msg)

        }

      })

    },
    aprobarPersonalTrabajador(oficio){
      // console.log(oficio);
      axios.post("{{ route('GestionSolicitudesDonativosAprobacionv2') }}", {oficio: oficio}).then(response => {

        if(response.data.error == 0){

          alertify.success('Oficio procesado correctamente')
          this.personalTrabajador = response.data.personalTrabajador
          this.personalJuridico = response.data.personalJuridico
          this.personalNatural = response.data.personalNatural
        }else{
          alertify.error(response.data.msg);
        }

      })
      //route GestionSolicitudesDonativosAprobacionv2
    },
    aprobarSolicitud(){

      // $("#card"+this.index).css('display', 'none')

        if(this.tipoOficio == 'trabajador')
          oficio = this.personalTrabajador[this.indexOficio]
        else if(this.tipoOficio == 'natural')
          oficio = this.personalNatural[this.indexOficio]
        else
          oficio = this.personalJuridico[this.indexOficio]

        axios.post("{{ route('GestionSolicitudesDonativosAprobacion') }}", {oficio: oficio, aprobacion: this.picked}).then(response => {

          if(response.data.error == 0){
            alertify.success('Oficio procesado correctamente');
            if(this.tipoOficio=="juridico")
              this.personalJuridico.splice(this.indexOficio,1);
            else if(this.tipoOficio == 'natural')
              this.personalNatural.splice(this.indexOficio,1);
            this.personalJuridico = response.data.personalJuridico;
            this.personalNatural = response.data.personalNatural;
            // this.personalTrabajador = response.data.personalTrabajador;
          }else
            alertify.error(response.data.msg);
        })
    },
    obtenerPersonalTrabajador(){

      this.obtenerPersonalTrabajador = []

      axios.get("{{ route('GestionSolicitudesDonativosObtenerPersonalTrabajador') }}").then(response => {

        this.personalTrabajador = response.data

      })

    },
    obtenerPersonalNatural(){

      this.obtenerPersonalNatural = []

      axios.get("{{ route('GestionSolicitudesDonativosObtenerPersonalNatural') }}").then(response => {

        this.personalNatural = response.data

      })

    },
    rechazar(id){
      if(confirm("¿Desea rechazar este oficio?")){

        axios.post("{{ route('GestionSolicitudesDonativosRechazar') }}", {id: id}).then(reponse => {

          window.location.reload()

        })

      }
    }//rechazar()
  },
  mounted(){

  }
});//const app= new Vue
</script>
@endpush
