@inject('detalle_evento','App\Modelos\ControlOficios\Control_oficio_detalle_evento') {{-- inject before foreach --}}
<style media="screen">
  td{
    text-align:center;
  }
</style>
<table>
  <tr style="background-color:#C40D13; color:#ffffff">
    <!-- <td>Nº</td> -->
    <td style="text-align:center;">MOTIVO DE SOLICITUD</td>
    <td>N° DE OFICIO</td>
    <td>FECHA DE OFICIO</td>
    <td>ESTADO</td>
    <td>TIPO DE SOLICITANTE</td>
    <td>CÉDULA</td>
    <td>NOMBRES Y APELLIDOS</td>
    <td>CÓDIGO DEL CARNET DE LA PATRIA</td>
    <td>TELÉFONO DE CONTACTO</td>
    <td>CORREO ELECTRÓNICO</td>
    <td>CANTIDAD DE TELÉFONOS SOLICITADA</td>
    <td>DESCRIPCIÓN DEL OFICIO</td>
    <td>FECHA DEL EVENTO</td>
    <td>HORA DEL EVENTO</td>
    <td>LUGAR DEL EVENTO</td>
  </tr>
  @foreach($oficios as $oficio)
  @if($oficio->personal_juridico==null)
  <tr>
    <td>{{ strtoupper($oficio->motivo_solicitud->nombre) }}</td>
    <td>{{ strtoupper($oficio->numero_oficio) }}</td>
    <td>{{ strtoupper(voltearFecha($oficio->fecha_emision)) }}</td>
    <td>{{ strtoupper($oficio->estado->estado) }}</td>
    @if( $oficio->personal_juridico==null)
    <td>PERSONA NATURAL</td>
    @else
    <td>PERSONA JURÍDICA</td>
    @endif
    <td>{{ strtoupper($oficio->personal_natural->cedula) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->nombre) }} {{ strtoupper($oficio->personal_natural->apellido) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->codigo_carnet_patria) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->telefono) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->correo_electronico) }}</td>
    @php
    $detalle=$detalle_evento->where('registro_oficio_id',$oficio->id)->first();
    @endphp
    @if($detalle==null)
    <td>
      @if($oficio->motivo_solicitud_id==1)
        @if(!$oficio->solicitud_productos)
          Sin solicitud de productos
        @else
          @foreach($oficio->solicitud_productos as $producto)
            {{$producto->cantidad}} {{$producto->producto->descripcion}},
          @endforeach
        @endif
      @elseif($oficio->motivo_solicitud_id==3)
        @if(!$oficio->solicitud_materiales)
          Sin solicitud de materiales
        @else
          @foreach($oficio->solicitud_materiales as $producto)
          {{$producto->cantidad}} {{$producto->material->nombre}},
          @endforeach
        @endif
      @endif
    </td>
    <td>{{$oficio->descripcion}}</td>
    <td></td>
    <td></td>
    <td></td>
    @else
    <td></td>
    <td></td>
    <td>{{$detalle->fecha}}</td>
    <td>{{$detalle->hora}}</td>
    <td>{{ strtoupper($detalle->lugar) }}</td>
    @endif
  </tr>
  @endif
  @endforeach
</table>
