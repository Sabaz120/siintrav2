<style media="screen">
td{
  text-align:center;
}
</style>
<table>
  <tr style="background-color:#757577; color:#ffffff; text-transform: uppercase;">
    <td colspan="2">ESTADÍSTICA POR MES</td>
  </tr>
  @foreach($meses as $mes)
    @php
      $cantidad=0;
    @endphp
    <tr style="background-color:#C40D13; color:#ffffff; text-transform: uppercase;">
      <td style="text-align:center;">{{ strtoupper($mes->mes) }}</td>
    </tr>
    @foreach($mes->oficios as $oficio)

    <tr>
      <td style="text-transform: uppercase;">{{ strtoupper($oficio->ente) }}</td>
      <td style="text-transform: uppercase;">{{ strtoupper($oficio->cantidad) }}</td>
    </tr>
    @php
       $cantidad = $cantidad + $oficio->cantidad;
    @endphp

    @endforeach
  <tr>
    <td><strong>Total:</strong></td>
    <td style="background-color:#C40D13; text-transform: uppercase;"><h4>{{$cantidad}}</h4></td>
  </tr>
  @endforeach
</table>
