@inject('detalle_evento','App\Modelos\ControlOficios\Control_oficio_detalle_evento') {{-- inject before foreach --}}
<style media="screen">
  td{
    text-align:center;
  }
</style>
<table>
  <tr style="background-color:#C40D13; color:#ffffff">
    <!-- <td>Nº</td> -->
    <td style="text-align:center;">CLASIFICACIÓN DE ENTE</td>
    <td style="text-align:center;">ENTE</td>
    <td>NIVEL DE ATENCIÓN</td>
    <td>RIF</td>
    <td>INSTITUCIÓN</td>
    <td>CÉDULA</td>
    <td>NOMBRES Y APELLIDOS</td>
    <td>TELÉFONO DE CONTACTO</td>
    <td>CORREO ELECTRÓNICO</td>
    <td>FECHA DE INICIO</td>
    <td>ESTADO</td>
    <td>DESCRIPCIÓN DEL OFICIO</td>
    <td>MODELO</td>
    <td>CANTIDAD TOTAL SOLICITADA</td>
  </tr>
  @foreach($oficios as $oficio)
  @php
    $modelos="";
    $cantidad_total=0;
  @endphp
  @if($oficio->personal_juridico!=null)
  <tr>
    <td>{{ strtoupper($oficio->personal_juridico->ente->clasificacion_ente->nombre) }}</td>
    <td>{{ strtoupper($oficio->personal_juridico->ente->nombre) }}</td>
    <td>{{ strtoupper($oficio->personal_juridico->ente->ponderacion) }}</td>
    <td>{{ strtoupper($oficio->personal_juridico->rif_cedula_situr) }}</td>
    <td>{{ strtoupper($oficio->personal_juridico->nombre_institucion) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->cedula) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->nombre) }} {{ strtoupper($oficio->personal_natural->apellido) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->telefono) }}</td>
    <td>{{ strtoupper($oficio->personal_natural->correo_electronico) }}</td>
    <td>{{ strtoupper(voltearFecha($oficio->fecha_emision)) }}</td>
    <td>{{ strtoupper($oficio->estado->estado) }}</td>
    <td>{{ strtoupper($oficio->descripcion) }}</td>
    <td>
      @if($oficio->motivo_solicitud_id==1)
        @if(!$oficio->solicitud_productos)
          Sin solicitud de productos
        @else
          @foreach($oficio->solicitud_productos as $producto)
            @php
            $cantidad_total=$cantidad_total+$producto->cantidad;
            $modelos=$modelos.' '.$producto->producto->descripcion.' / ';
            @endphp
          @endforeach
        {{$modelos}}
        @endif
      @elseif($oficio->motivo_solicitud_id==3)
        @if(!$oficio->solicitud_materiales)
          Sin solicitud de materiales
        @else
          @foreach($oficio->solicitud_materiales as $producto)
          $cantidad_total=$cantidad_total+$producto->cantidad;
          $modelos=$modelos.' '.$producto->material->nombre.' / ';
          @endforeach
        @endif
        {{$modelos}}
      @endif
    </td>
    <td>{{$cantidad_total}}</td>
  </tr>
  @endif
  @endforeach
</table>
