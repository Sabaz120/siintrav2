@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <div class="card-header">
      <h5 class="text-center header-title pt-1">REPORTE DE SEGUIMIENTO</h5>
    </div>
    <div class="card-body">
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">PARÁMETROS DE BUSQUEDA </h3>
      <div class="row">
        <div class="col-md-3 text-center">
          <strong>Fecha inicio:</strong>
          <input type="text" class="form-control datepicker" data-date-format="dd-mm-yyyy" id="fecha_inicio" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
          <!-- <input id="ente" class="form-control" v-bind:class="{ 'is-invalid': enteRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="data.ente"  placeholder="Ente" required> -->
        </div>
        <div class="col-md-3 text-center">
          <strong>Fecha fin:</strong>
          <input type="text" class="form-control datepicker" data-date-format="dd-mm-yyyy" id="fecha_fin" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
          <!-- <input id="ente" class="form-control" v-bind:class="{ 'is-invalid': enteRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="data.ente"  placeholder="Ente" required> -->
        </div>
        <div class="col-md-3 text-center ">
          <strong>Estado:</strong>
          <select class="form-control" name="estado_id" v-model="estado_id" id="estado_id">
            <option value="0">Selecciona un estado</option>
            <option v-for="estado in estados" v-bind:value="estado.idEstado">
              @{{ estado.estado }}
            </option>
          </select>
          <!-- <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)" minlength="3" maxlength="100"  type="text" v-model="data.descripcion"  placeholder="Descripción" required> -->
        </div>
        <div class="col-md-3 text-center">
          <strong>Motivo de solicitud:</strong>
          <select class="form-control" v-model="motivo_solicitud_id" name="motivo_solicitud_id">
            <option value="0">Seleccione un motivo de solicitud</option>
            <option v-for="motivo in motivos_solicitud" v-bind:value="motivo.id">
              @{{ motivo.nombre }}
            </option>
          </select>
        </div>
        <div class="col-md-3 text-center">
          <strong>Tipo de solicitante:</strong>
          <select class="form-control" v-model="tipo_solicitante">
            <option value="">Seleccione Tipo de Solicitante</option>
            <option value="natural">Personal Natural</option>
            <option value="juridico">Personal Jurídico</option>
            <option value="trabajador">Personal Trabajador</option>
          </select>
        </div>
        <div class="col-md-3 text-center" v-if="tipo_solicitante=='natural'">
          <strong>Solicitante:</strong>
          <input type="text" class="form-control" v-model="nombre_solicitante">
        </div>
        <div class="col-md-3 text-center" v-show="tipo_solicitante=='juridico'">
          <strong>Clasificación de ente:</strong>
          <select class="form-control" v-on:change="cargarEntes()" name="clasificacion_ente" v-model="clasificacion_ente_id" id="clasificacion_ente_id">
            <option value="0">Selecciona una clasificación de ente</option>
            <option v-for="clasificacion in clasificacion_entes" v-bind:value="clasificacion.id">
              @{{ clasificacion.nombre }}
            </option>
          </select>
        </div>
        <div class="col-md-3 text-center" v-show="tipo_solicitante=='juridico' && entes.length>0">
          <strong>Ente:</strong>
          <select class="form-control" v-model="ente_id" name="ente_id">
            <option value="0">Seleccione un ente</option>
            <option v-for="ente in entes" v-bind:value="ente.id">
              @{{ ente.nombre }}
            </option>
          </select>
        </div>
        <div class="col-md-3 text-center ">
          <strong>Estado de solicitud:</strong>
          <select class="form-control" name="estado_oficio_id" v-model="estado_oficio_id" id="estado_oficio_id">
            <option value="0">Selecciona un estado</option>
            <option value="Pendientes">Pendientes</option>
            <option value="Atendidas">Atendidas</option>
            <option value="EnProceso">En proceso</option>
          </select>
          <!-- <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)" minlength="3" maxlength="100"  type="text" v-model="data.descripcion"  placeholder="Descripción" required> -->
        </div>
      </div>
      <div class="row">
        <div class="mx-auto">
          <br>
          <button type="button" class="btn btn-info" name="button" @click="buscarOficios()">Buscar</button>
          <button type="button" class="btn btn-warning" name="button" data-toggle="modal" data-target="#limpiarForm">Limpiar</button>
        </div>
      </div> <!-- Fin row botones -->
      <br>
      <highcharts :options="options" v-if="oficios.length>0"></highcharts>
      <div class="row d-flex justify-content-center">
        <div class="card-deck ">
          <div style="width:250px;height:230px" v-for="oficio in oficios">
            <div class="card h-100" v-bind:class="oficio.class">
              <div class="card-header text-center">
                <h3>
                  <i v-bind:class="oficio.icon_card" class="fa-2x text-white"></i>
                </h3>
              </div>
              <div class="card-body text-center text-white d-inline-block">
                <h3>@{{solicitudesTotal}}</h3>
                <br>
                <h4>@{{oficio.nombre}}</h4>
              </div>
            </div> <!-- card -->
          </div>
        </div>
      </div>
      <br>
      <div class="table-responsive align-content-center text-center" v-if="oficios.length>0">
        <table class="table table-bordered">
          <thead class="bg-primary text-white">
            <th>MOTIVO DEL OFICIO</th>
            <th>NÚMERO DE OFICIOS</th>
            <th>ACCIÓN</th>
          </thead>
          <tbody>
            <tr v-for="(oficio,index) in oficios">
              <td>@{{oficio.nombre}}</td>
              <td>@{{oficio.oficios.length}}</td>
              <td>
                <div class="row">
                  <div class="col-md-6 text-right">
                    <form action="{{url('reportes/oficio_pdf')}}" method="post">
                      @csrf
                      <input type="hidden" name="tipo_reporte" value="1">
                      <input type="hidden" name="fecha_inicio" v-model="fecha_inicio">
                      <input type="hidden" name="fecha_fin" v-model="fecha_fin">
                      <input type="hidden" name="motivo_solicitud_id" :value="oficio.motivo_solicitud_id" >
                      <input type="hidden" name="estado_id" :value="estado_id_reporte">
                      <input type="hidden" name="ente_id" :value="ente_id_reporte">
                      <input type="hidden" name="tipo_solicitante" :value="tipo_solicitante">
                      <input type="hidden" name="nombre_solicitante" :value="nombre_solicitante">
                      <button type="submit" name="button" class="btn btn-warning btn-sm" title="Generar PDF"><i class="fa fa-file-pdf-o"></i></button>
                    </form>
                  </div>
                  <div class="col-md-6 text-left">
                    <form action="{{url('reportes/oficio_pdf')}}" method="post">
                      @csrf
                      <input type="hidden" name="tipo_reporte" value="2">
                      <input type="hidden" name="fecha_inicio" v-model="fecha_inicio">
                      <input type="hidden" name="fecha_fin" v-model="fecha_fin">
                      <input type="hidden" name="motivo_solicitud_id" :value="oficio.motivo_solicitud_id" >
                      <input type="hidden" name="estado_id" :value="estado_id_reporte">
                      <input type="hidden" name="ente_id" :value="ente_id_reporte">
                      <input type="hidden" name="tipo_solicitante" :value="tipo_solicitante">
                      <input type="hidden" name="nombre_solicitante" :value="nombre_solicitante">
                      <button type="submit" name="button" class="btn btn-success btn-sm" title="Generar Excel"><i class="fa fa-file-excel-o"></i></button>
                    </form>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="table-responsive align-content-center text-center" v-else>
        <h3>No hay oficios que mostrar</h3>
      </div>
    </div>
  </div>
  @include('controlOficios.recepcionOficio.partials.modal_limpiar')
</section>
@endsection
@push('scripts')
<script type="text/javascript">
function formatDate(value)  {
  // console.log('recibido: '+value);
  var fecha=value.split("-");
  value=fecha[2]+"-"+fecha[1]+"-"+fecha[0];
  return value;
}
var d=new Date();
var ano= d.getFullYear();
var mes= d.getMonth()+1;
var dia=d.getDate();
if(dia<10)
dia='0'+dia;
if(mes<10)
mes='0'+mes;
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  solicitudesTotal:0,
  solicitudesPendientes:0,
  solicitudesAtendidas:0,
  solicitudesProceso:0,
  data:{

    //gráfica
    options : null,

    // Variables controller
    estados: {!! $estados ? $estados : "''"!!},
    motivos_solicitud: {!! $motivos_solicitud ? $motivos_solicitud : "''"!!},
    clasificacion_entes: {!! $clasificacion_entes ? $clasificacion_entes : "''"!!},
    entes:[],
    ente_id:0,
    estado_id:0,
    motivo_solicitud_id:0,
    ente_id_reporte:0,
    estado_id_reporte:0,
    clasificacion_ente_id:0,
    // Variables controller
    //Variables traídas por axios
    oficios:[],
    //Variables traídas por axios
    fecha_inicio:'',
    fecha_fin:'',
    tipo_solicitante:'',
    nombre_solicitante:'',
    estado_oficio_id:'0'
  },
  methods:{
    limpiar(){
      this.motivo_solicitud_id=0,
      this.estado_id=0,
      $('#fecha_inicio').val(dia+'-'+mes+'-'+ano);
      $('#fecha_fin').val(dia+'-'+mes+'-'+ano);
      this.oficios=[];
      this.motivo_solicitud_id=0;
      this.estado_id=0;
      this.ente_id=0;
      this.entes=[];
      this.clasificacion_ente_id=0;
      this.nombre_solicitante="";
      this.tipo_solicitante='';
    },
    buscarOficios(){
      if(this.clasificacion_ente_id!=0 && this.ente_id==0){
        alertify.error('Debes seleccionar un ente');
      }else if(!this.validate_fechaMayorQue($('#fecha_inicio').val(),$('#fecha_fin').val())){
        alertify.error("La fecha "+$('#fecha_inicio').val()+" no puede ser superior a la fecha "+$('#fecha_fin').val());
      }else{
        axios.post('{{ url("reportes/recepcion_oficios") }}', {
          ente_id:this.ente_id,
          estado_id:this.estado_id,
          motivo_solicitud_id:this.motivo_solicitud_id,
          tipo_solicitante:this.tipo_solicitante,
          fecha_inicio:formatDate($('#fecha_inicio').val()),
          fecha_fin:formatDate($('#fecha_fin').val()),
          nombre_solicitante:this.nombre_solicitante,
          estado_oficio_id:this.estado_oficio_id
        }).then(response => {
          //console.log(response.data.solicitudesTotal)
          if(response.data.error==0){
            //Sin errores
            this.fecha_inicio=$('#fecha_inicio').val();
            this.fecha_fin=$('#fecha_fin').val();
            this.ente_id_reporte=this.ente_id;
            this.estado_id_reporte=this.estado_id;
            // alertify.success(response.data.msg);
            this.oficios=response.data.oficios;

            this.solicitudesTotal = response.data.solicitudesTotal
            this.solicitudesPendientes = response.data.solicitudesPendientes
            this.solicitudesAtendidas = response.data.solicitudesAtendidas
            this.solicitudesProceso = response.data.solicitudesProceso

            this.options = {
                            colors : [ '#7cb5ec','#7cb5ec','#7cb5ec','#7cb5ec'],
                            chart: {
                              type: 'bar',
                            },
                            title: {
                              text: 'ESTADOS DE LOS OFICIOS'
                            },
                            xAxis: {
                              categories: ['Total Solicitudes', 'Pendientes', 'Atendidas', 'En Proceso'],
                              title: {
                                  text: null
                              }
                            },
                            yAxis: {
                                  min: 0,
                                  title: {
                                      align: 'high'
                                  },
                                  labels: {
                                      overflow: 'justify'
                                  }
                              },
                            credits:false,
                            series: [{
                              name: 'Total Solicitudes',
                              data:[this.solicitudesTotal, this.solicitudesPendientes, this.solicitudesAtendidas, this.solicitudesProceso]
                            }]
                          }

            //console.log(this.options);

          }else if(response.data.error==1){
            this.limpiar();
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      }
    },
    cargarEntes(){
      this.ente_id=0;
      this.entes=[];
      axios.post('{{ route("api.controloficio.obtenerentes") }}', {clasificacion_ente_id:this.clasificacion_ente_id}).then(response => {
        if(response.data.error==0){
          this.entes=response.data.entes
        }else if(response.data.error==1){
          // this.limpiar();
          this.clasificacion_ente_id=0;
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        this.clasificacion_ente_id=0;
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    validate_fechaMayorQue(fechaInicial,fechaFinal)
    {
      valuesStart=fechaInicial.split("-");
      valuesEnd=fechaFinal.split("-");

      // Verificamos que la fecha no sea posterior a la actual
      var dateStart=new Date(valuesStart[2],valuesStart[1],valuesStart[0]);
      var dateEnd=new Date(valuesEnd[2],valuesEnd[1],valuesEnd[0]);
      // console.log(dateStart,dateEnd);


      if(dateStart>dateEnd)
      {
        return 0;
      }
      return 1;
    }
  },
  mounted(){
    $('#fecha_inicio').val(dia+'-'+mes+'-'+ano);
    $('#fecha_fin').val(dia+'-'+mes+'-'+ano);
  }
});//const app= new Vue
</script>
@endpush
