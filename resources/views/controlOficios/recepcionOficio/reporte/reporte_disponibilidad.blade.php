@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <div class="card-header">
      <h5 class="text-center header-title pt-1">REPORTE DE DISPONIBILIDAD</h5>
    </div>
    <div class="card-body">
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">PARÁMETROS DE BUSQUEDA </h3>
      <form class="" action="{{route('api.sistemaproduccionv1.disponibilidad.productos.recepcion.oficio')}}" method="post" target="_blank">
        @csrf
      <div class="row">
          <div class="col-md-6 text-center">
            <strong>Modelo:</strong>
            <select class="form-control" name="producto_id" v-model="producto_id" @change="cargarModalidad()">
              <option value="0">Todos</option>
              <option v-for="producto in productos" :value="producto.id">@{{producto.descripcion}}</option>
            </select>
          </div>
          <div class="col-md-6 text-center">
            <strong>Modalidad:</strong>
            <select class="form-control" name="modalidad_id" v-model="modalidad_id" @change="cargarColor()">
              <option value="0">Todos</option>
              <option v-for="modalidad in modalidades" :value="modalidad.modalidad">@{{modalidad.modalidad}}</option>
            </select>
          </div>
          <input type="text" name="type" style="display: none" id="type">
          <!-- <div class="col-md-3 text-center">
            <strong>Color:</strong>
            <select class="form-control" v-model="color_id">
              <option value="0">Todos</option>
              <option v-for="color in colores" :value="color.modeloColor">@{{color.modeloColor}}</option>
            </select>
          </div> -->
      </div>
      <div class="row">
        <div class="mx-auto">
          <br>
          <button type="submit" class="btn btn-warning" name="button" onclick="setType('PDF')"> <i class="fa fa-file-pdf-o"></i> PDF</button>
          <button type="submit" class="btn btn-success" name="button" onclick="setType('EXCEL')"> <i class="fa fa-file-excel-o"></i> Excel</button>
          <!-- <button type="button" class="btn btn-warning" name="button" data-toggle="modal" data-target="#limpiarForm">Limpiar</button> -->
        </div>
      </div> <!-- Fin row botones -->
    </form>
      <br>
      <div class="table-responsive align-content-center text-center" v-if="productos_encontrados.length>0">
        <table class="table table-bordered">
          <thead class="bg-primary text-white">
            <th>PRODUCTO</th>
            <th>DISPONIBLE</th>
            <th>COMPROMETIDO</th>
            <th>ACCIÓN</th>
          </thead>
          <tbody>
            <tr v-for="(producto,index) in productos_encontrados">
              <td>@{{producto.descripcion}}</td>
              <td>@{{producto.disponible}}</td>
              <td>@{{producto.comprometido}}</td>
              <td>
                <!-- <div class="row">
                  <div class="col-md-6 text-right">
                    <form action="{{url('reportes/oficio_pdf')}}" method="post">
                      @csrf
                      <input type="hidden" name="modelo" v-model="producto_id">
                      <input type="hidden" name="modalidad" v-model="modalidad_id">
                      <input type="hidden" name="color" v-model="color_id">
                      <button type="submit" name="button" class="btn btn-warning btn-sm" title="Generar PDF"><i class="fa fa-file-pdf-o"></i></button>
                    </form>
                  </div>
                </div> -->
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- <div class="table-responsive align-content-center text-center" v-else>
        <h3>No hay productos que mostrar</h3>
      </div> -->
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
function setType(type){

  $("#type").val(type)

}
function formatDate(value)  {
  // console.log('recibido: '+value);
  var fecha=value.split("-");
  value=fecha[2]+"-"+fecha[1]+"-"+fecha[0];
  return value;
}
var d=new Date();
var ano= d.getFullYear();
var mes= d.getMonth()+1;
var dia=d.getDate();
if(dia<10)
dia='0'+dia;
if(mes<10)
mes='0'+mes;
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    //Variables traídas por axios
    productos: {!! $productos ? $productos : "''"!!},
    productos_encontrados:[],
    modalidades:[],
    colores:[],
    modalidad_id:0,
    producto_id:0,
    color_id:0,
    //Variables traídas por axios
  },
  methods:{
    limpiar(){
      // this.productos=[];
      this.modalidades=[];
      this.colores=[];
      this.color_id=0;
      this.modalidad_id=0;
      this.producto_id=0;
    },
    buscar(){
      axios.post('{{ route("api.sistemaproduccionv1.disponibilidad.productos.recepcion.oficio") }}', {
        producto_id:this.producto_id,
        color_id:this.color_id,
        modalidad_id:this.modalidad_id
      }).then(response => {
        console.log(response.data);
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    cargarEntes(){
      this.ente_id=0;
      this.entes=[];
      axios.post('{{ route("api.controloficio.obtenerentes") }}', {clasificacion_ente_id:this.clasificacion_ente_id}).then(response => {
        if(response.data.error==0){
          this.entes=response.data.entes
        }else if(response.data.error==1){
          // this.limpiar();
          this.clasificacion_ente_id=0;
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        this.clasificacion_ente_id=0;
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    cargarModalidad(){
      axios.get('{{ route("api.sistemaproduccionv1.obtenermodalidad") }}', {
        params:{
          producto_id:this.producto_id
        }
      }).then(response => {
        console.log(response.data);
        this.modalidades=response.data.modalidades;
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    cargarColor(){
      axios.get('{{ route("api.sistemaproduccionv1.obtenercolor") }}', {
        params:{
          producto_id:this.producto_id,
          modalidad:this.modalidad_id
        }
      }).then(response => {
        this.colores=response.data.colores;
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
  },
  mounted(){
    console.log('asdad');
    // this.obtenerModalidad();
  }
});//const app= new Vue
</script>
@endpush
