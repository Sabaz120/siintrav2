@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <div class="card-header">
      <h5 class="text-center header-title pt-1">EXPEDIENTE DE SOLICITUDES</h5>
    </div>
    <div class="card-body">
      <div class="table-responsive align-content-center text-center" v-if="oficios.length>0">
        <div class="row">
          <div class="col-12 col-md-9 col-lg-9">
            <br>
            <div class="mt-2 form-inline font-weight-bold">
              Mostrar
              <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                <option v-for="option in optionspageSize" v-bind:value="option.value">
                  @{{ option.text }}
                </option>
              </select>
              registros
            </div>
          </div>
          <div class="col-12 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="Buscar" class="font-weight-bold">Buscar:</label>
              <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
            </div>
          </div>
        </div>
        <table class="table table-bordered">
          <thead class="bg-primary text-white">
            <th>N° de Registro</th>
            <th>N° Requisición</th>
            <th>Ente / Cliente</th>
            <th>Creado El</th>
            <th>Última actualización</th>
            <th>Acción</th>
          </thead>
          <tbody>
            <tr v-for="(oficio,index) in listado_oficios">
              <td>@{{index+1}}</td>
              <td>@{{oficio.numero_requisicion}}</td>
              <td>
                <label class="text-capitalize">@{{oficio.solicitante}}</label>
              </td>
              <td>@{{oficio.createdAt}}</td>
              <td>@{{oficio.updatedAt}}</td>
              <td>
                <div class="d-flex">
                  <div class="align-self-center">
                    <form action="{{route('generarPDFOferta')}}" method="post" target="_balnk">
                      {{ csrf_field() }}
                      <input type="hidden" name="oficio_id" v-model="oficio.id">
                      <button title="PDF de Oferta Económica" type="submit" name="button" class="btn btn-primary text-white mr-2" title="Generar PDF de Cotización"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                    </form>
                  </div>
                  <div class="align-self-center">
                    <form action="{{route('api.requisicion.pdf')}}" method="post" target="_balnk">
                      @csrf
                      <input type="hidden" name="oficio_id" :value="oficio.id">
                      <button type="submit" class="btn btn-info" title="Generar PDF de requisición" name="button">
                        <i class="fa fa-file-pdf"></i>
                      </button>
                    </form>
                  </div>
                  <div class="align-self-center">
                    <form action="{{route('gestion.solicitudes.pagos.pdf')}}" method="post" target="_balnk">
                      @csrf
                      <input type="hidden" name="oficio_id" :value="oficio.id">
                      <button type="submit" class="btn btn-info" title="Generar PDF de Pagos" name="button">
                        <i class="fa fa-file-pdf"></i>
                      </button>
                    </form>
                  </div>
                  <div class="align-self-center" v-if="oficio.documento">
                    <a class="btn btn-info" title="Descargar adjunto" :href="oficio.documento" download>
                      <i class="fa fa-file-pdf"></i>
                    </a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div class="">
          <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{oficios.length}}</strong>
          <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
          <div style="float:right">
            <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
            <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            <div class="row ml-2">
              <strong>Página:  @{{currentPage}}</strong>
            </div>
          </div>
        </div>
      </div>
      <div class="table-responsive align-content-center text-center" v-else>
        <h3>No hay oficios que mostrar</h3>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">

alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    // Variables controller
    oficios: {!! $oficios ? json_encode($oficios) : "''"!!},
    search:'',
    currentSort:'id',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0,
  },
  methods:{
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.oficios){
          if(this.oficios[i].solicitante.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
            this.statusFiltro=1;
            filtrardo.push(this.oficios[i]);
          }//
        }//
        if(filtrardo.length)
        this.oficios=filtrardo;
        else{
          this.statusFiltro=0;
          this.oficios={!! $oficios ? json_encode($oficios) : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.oficios={!! $oficios ? json_encode($oficios) : "''"!!};
      }//if(this.search$materiales
    },
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
  },
  computed:{
    listado_oficios:function() {
      this.rows=0;
      if(this.oficios.length>0){
        return this.oficios.sort((a,b) => {
          let modifier = 1;
          if(this.currentSortDir === 'desc')
          modifier = -1;
          if(a[this.currentSort] < b[this.currentSort])
          return -1 * modifier;
          if(a[this.currentSort] > b[this.currentSort])
          return 1 * modifier;
          return 0;
        }).filter((row, index) => {
          let start = (this.currentPage-1)*this.pageSize;
          let end = this.currentPage*this.pageSize;
          if(index >= start && index < end){
            this.rows+=1;
            return true;
          }
        });
      }else{
        return [];
      }
    },
  },
  mounted(){

  }
});//const app= new Vue
</script>
@endpush
