<table>
  <tr></tr>
  <tr>
    <td height="25">
      {{--<h3 style="text-align: center;">Agente Autorizado</h3>--}}
      @php

        $dateInicio = \Carbon\Carbon::parse($fechaInicio);
        $fechaInicio = $dateInicio->format('d/m/Y');

        $dateFin = \Carbon\Carbon::parse($fechaFin);
        $fechaFin = $dateFin->format('d/m/Y');

      @endphp
      <h3 colspan="4">Oficios atendidos desde: {{ $fechaInicio }} hasta: {{ $fechaFin }}</h3>
    </td>
  </tr>
  <tr></tr>
</table>

<table>
  <thead>
    <tr>
      <th height="25">Fecha</th>
      <th height="25">Nombre de institución</th>
      <th height="25">Nombre de autoridad</th>
      <th height="25">Descripción</th>
      <th height="25">Estado</th>
      <th height="25">Sumatoria</th>
    </tr>
  </thead>
  <tbody>
    @foreach($result as $key)

        @php
        $date = \Carbon\Carbon::parse($key->created_at);
        $fecha = $date->format('d/m/Y');
        @endphp

        <tr>
        
        <td height="25">{{ $fecha }}</td>
        <td height="25">{{ $key->nombre_institucion }}</td>
        <td height="25">{{ $key->nombre_autoridad }}</td>
        <td height="25">{{ $key->descripcion }}</td>
        <td height="25">{{ $key->estado }}</td>
        <td height="25">{{ $key->sum }}</td>
        
        </tr>
     
    @endforeach
  </tbody>
</table>



