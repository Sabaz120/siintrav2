@extends('layouts.pdf.pdf')
@section('contenido')
@inject('detalle_evento','App\Modelos\ControlOficios\Control_oficio_detalle_evento') {{-- inject before foreach --}}
<style media="screen">
  h2,h3,h4,h5{
    text-align:center;
  }
</style>
  <h2 class="text-center">{{$titulo}}</h2>
  <div class="text-center">
    <div style="float:left">
      <strong>MOTIVO DE SOLICITUD: </strong>{{$oficios[0]->motivo_solicitud->nombre}}
    </div>
    <div style="float:right">
      <strong>CANTIDAD PROCESADA: </strong>{{count($oficios)}}
    </div>
  </div>
  <br>
  @foreach($oficios as $oficio)
    <h3 style="text-align:center;">OFICIO #{{$loop->iteration}}</h3>
    <table style="text-align:center;width:100%;table-layout:fixed!important;">
      <thead>
        <tr>
          <th colspan="3"><strong>Fecha de Registro: </strong>{{$oficio->created_at->format('d-m-Y h:m:s')}}</th>
          <th colspan="3"><strong>Recibido por: </strong>{{$oficio->usuario_creador->name}}</th>
        </tr>
        <tr>
          <th colspan="6"><strong>DATOS GENERALES DEL OFICIO</strong></th>
        </tr>
        <tr>
          <th><strong>N° de Oficio</strong></th>
          <th colspan="2"><strong>Fecha de Oficio</strong></th>
          <th colspan="3"><strong>Estado</strong></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{$oficio->numero_oficio}}</td>
          <td colspan="2">{{voltearFecha($oficio->fecha_emision)}}</td>
          <td colspan="3">{{$oficio->estado->estado}}</td>
        </tr>
        <tr>
          <th>Tipo de solicitante</th>
          <th>Cédula</th>
          <th>Nombres y Apellidos</th>
          <th>Código Carnet de la Patria</th>
          <th>Teléfono de Contacto</th>
          <th>Correo Electrónico</th>
        </tr>
        <tr>
          @if($oficio->personal_juridico==null)
          <td>Persona Natural</td>
          @else
          <td>Persona Jurídica</td>
          @endif
          <td>{{$oficio->personal_natural->cedula}}</td>
          <td>{{$oficio->personal_natural->nombre}} {{$oficio->personal_natural->apellido}}</td>
          <td>{{$oficio->personal_natural->codigo_carnet_patria}}</td>
          <td>{{$oficio->personal_natural->telefono}}</td>
          <!-- <td style="text-align: justify!important;overflow:hidden!important"> -->
          <td style="text-align: justify!important;word-wrap: break-word!important;">
            <!-- sdsdsdsdsdsdsdsdsdsdsdsdsdsddasdadadaddadds -->
              {{$oficio->personal_natural->correo_electronico}}
          </td>
        </tr>
        @php
          $detalle=$detalle_evento->where('registro_oficio_id',$oficio->id)->first();
        @endphp
        @if($detalle==null)
          <!-- Jornada de equipos  -->
          @if($oficio->motivo_solicitud_id==1)
          <tr>
            <th colspan="2">Equipos solicitados</th>
            <th colspan="4">Descripción</th>
          </tr>
          <tr>
            <td colspan="2" style="text-align: center!important;word-wrap: break-word!important;">
              @if(!$oficio->solicitud_productos)
                <p>Sin solicitud de productos</p>
              @else
                @foreach($oficio->solicitud_productos as $producto)
                  {{$producto->cantidad}} {{$producto->producto->descripcion}} <br>
                @endforeach
              @endif
            </td>
            <td colspan="4">{{$oficio->descripcion}}</td>
          </tr>
          <!-- Jornada de equipos  -->
          <!-- Donativos -->
          @elseif($oficio->motivo_solicitud_id==3)
          <tr>
            <th colspan="2">Materiales solicitados</th>
            <th colspan="4">Descripción</th>
          </tr>
          <tr>
            <td colspan="2" style="text-align: center!important;word-wrap: break-word!important;">
              @if(!$oficio->solicitud_materiales)
                <p>Sin solicitud de materiales</p>
              @else
                @foreach($oficio->solicitud_materiales as $producto)
                {{$producto->cantidad}} {{$producto->material->nombre}} <br>
                @endforeach
              @endif
            </td>
            <td colspan="4">{{$oficio->descripcion}}</td>
          </tr>
          <!--  Donativos -->
          @endif
        @else
          <tr>
            <th>Fecha</th>
            <th>Hora</th>
            <th colspan="4">Lugar del evento</th>
          </tr>
          <tr>
            <td>{{voltearFecha($detalle->fecha)}}</td>
            <td>{{$detalle->hora}}</td>
            <td colspan="4">{{$detalle->lugar}}</td>
          </tr>
        @endif
      </tbody>
    </table>
    <br>

  @endforeach

@endsection
