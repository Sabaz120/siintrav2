@extends('layouts.pdf.pdf')
@section('contenido')
<style media="screen">
h2,h3,h4,h5{
  text-align:center;
}
</style>
<h2 class="text-center">{{$titulo}}</h2>
<br>
<table style="text-align:center;width:100%;table-layout:fixed!important;">
  <thead>
    <tr>
      <th>Modelo</th>
      <th>Modalidad</th>
      <th>Disponible</th>
      <th>Comprometido</th>
      <th>Total disponible</th>
    </tr>
  </thead>
  <tbody>
    @php
    //$total=0;
    @endphp
    @foreach($productos_disponibilidad as $producto)
    <tr>
      <td>{{$producto->descripcion}}</td>
      <td>{{$producto->modalidad}}</td>
      <td>{{$producto->disponible}}</td>
      <td>{{$producto->comprometido}}</td>
      <td>{{$producto->total_disponible}}</td>
    </tr>
    @php
      //$total+=$producto->total_disponible;
    @endphp
    @endforeach

  </tbody>
</table>

@endsection
