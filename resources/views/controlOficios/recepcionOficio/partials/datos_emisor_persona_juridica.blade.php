<div class="col-md-3 text-center" v-show="tipo_solicitante_id==2">
  <strong><i class="fas fa-asterisk"></i> R.I.F:</strong>
  <div class="input-group mb-4">
    <input type="text" name="" class="form-control" v-model="rif_empresa" v-on:keyup="keyrif" v-mask="'A-NNNNNNNNNNNN'" value="">
    <div class="input-group-append">
      <button class="btn btn-info" @click="buscarEmpresa" type="button"><i class="fa fa-search"></i></button>
    </div>
  </div>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==2">
  <strong>Nombres de la Entidad:</strong>
  <input type="text" class="form-control text-capitalize" v-model="empresa_nombres" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==2">
  <strong>Departamento:</strong>
  <input type="text" class="form-control text-capitalize" v-model="empresa_departamento" readonly>
</div>
<div class="col-md-2 text-center" v-show="tipo_solicitante_id==2">
  <strong>Cédula del Responsable:</strong>
  <input type="text" class="form-control" v-model="empresa_responsable_cedula" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==2">
  <strong>Nombres y Apellidos:</strong>
  <input type="text" class="form-control text-capitalize" v-model="empresa_responsable_nombres" readonly>
</div>
<div class="col-md-2 text-center" v-show="tipo_solicitante_id==2">
  <strong>Código Carnet de la Patria:</strong>
  <input type="text" class="form-control" v-model="empresa_responsable_carnet" readonly>
</div>
<div class="col-md-2 text-center" v-show="tipo_solicitante_id==2">
  <strong>Teléfono de Contacto:</strong>
  <input type="text" class="form-control" v-model="empresa_responsable_telefono" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==2">
  <strong>Correo Electrónico:</strong>
  <input type="text" class="form-control" v-model="empresa_responsable_correo" readonly>
</div>
