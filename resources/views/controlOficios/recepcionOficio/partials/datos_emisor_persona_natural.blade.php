<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1">
  <strong><i class="fas fa-asterisk"></i> Cédula:</strong>
  <div class="input-group mb-4">
    <input type="text" name="" v-model="cedula_persona_natural" v-mask="'########'" class="form-control" value="">
    <div class="input-group-append">
      <button class="btn btn-info" @click="buscarPersonaNatural()" type="button"><i class="fa fa-search"></i></button>
    </div>
  </div>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == false">
  <strong>Nombres y Apellidos:</strong>
  <input type="text" class="form-control text-capitalize" name="" v-model="pn_nombres" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == false">
  <strong>Código Carnet de la Patria:</strong>
  <input type="text" class="form-control" name="" v-model="pn_cod_carnet" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == false">
  <strong>Teléfono de contacto:</strong>
  <input type="text" class="form-control" name="" v-model="pn_telefono" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == false">
  <strong>Correo Electrónico:</strong>
  <input type="text" class="form-control" name="" v-model="pn_correo" readonly>
</div>
