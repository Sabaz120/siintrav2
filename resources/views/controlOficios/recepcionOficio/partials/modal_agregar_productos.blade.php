<!-- Trigger the modal with a button -->
 <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#eliminarEquipo">Open Modal</button> -->
 <!-- Modal -->
 <div class="modal fade" id="agregarProductos" role="dialog">
   <div class="modal-dialog modal-md">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header bg-primary">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
         <h4 class="modal-title mx-auto text-white">ASIGNACIÓN DE CANTIDADES</h4>
       </div>
       <div class="modal-body text-center">
         <div class="row">
           <div class="col-md-6">
             <strong>Modelo:</strong>
             <select class="form-control" v-model="producto_id">
               <option value="0">Seleccione un modelo</option>
               <option v-for="producto in productos" v-bind:value="producto.id">
                 @{{producto.descripcion}}
               </option>
             </select>
           </div>
           <div class="col-md-3">
             <strong>Cantidad:</strong>
             <input type="text" style="text-align:center;" v-mask="'###'" v-model="cantidad_solicitada" class="form-control" name="cantidad_solicitada" value="0">
           </div>
           <div class="col-md-3 mx-auto">
             <br>
              <button type="button" class="btn btn-success" @click="seleccionarProducto" name="button">Agregar</button>
           </div>
           <div class="col-md-12">
             <h5 class="text-center header-title m-t-20 m-b-20 pt-2">LISTADO PRODUCTO</h5>
             <div class="table-responsive">
               <table class="table table-bordered table-shape table-striped" style="width:97%">
                 <thead class="bg-primary text-white">
                   <tr>
                     <th style="text-align:center">Modelo</th>
                     <th style="text-align:center">Cantidad</th>
                     <th style="text-align:center">Acción</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr v-for="(producto,index) in productosSeleccionados">
                     <td>@{{producto.descripcion}}</td>
                     <td>@{{producto.cantidad}}</td>
                     <td>
                       <span title="Eliminar" @click="eliminarTlf(index)" data-toggle="modal" data-target="#eliminarTlf" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true" ></i></span>
                     </td>
                   </tr>
               </tbody>
             </table>
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-success" @click="registrarCantidadTlf()" data-dismiss="modal">Registrar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal" @click="limpiar">Cerrar</button>
       </div>
     </div>

   </div>
 </div>
