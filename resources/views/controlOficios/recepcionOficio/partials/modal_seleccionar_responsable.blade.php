<!-- Trigger the modal with a button -->
 <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#eliminarEquipo">Open Modal</button> -->

 <!-- Modal -->
 <div class="modal fade" id="responsablesEmpresa" role="dialog">
   <div class="modal-dialog modal-lg">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header bg-primary">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
         <h4 class="modal-title mx-auto text-white">PERSONAL RESPONSABLE</h4>
       </div>
       <div class="modal-body text-center">
         <div class="table-responsive">
           <table class="table table-bordered table-hover">
             <thead class="bg-primary text-white">
               <tr>
                 <th>Departamento</th>
                 <th>Cédula</th>
                 <th>Nombres Y Apellidos</th>
                 <th>Acción</th>
               </tr>
             </thead>
             <tbody>
               <tr v-for="(responsable,index) in responsables_empresa">
                 <td class="text-capitalize">@{{responsable.departamento.nombre}}</td>
                 <td>@{{responsable.personal_natural.cedula}}</td>
                 <td class="text-capitalize">@{{responsable.personal_natural.nombre}} @{{responsable.personal_natural.apellido}}</td>
                 <td><button type="button" class="btn btn-success" @click="seleccionarResponsable(index)" data-dismiss="modal">Seleccionar</button></td>
               </tr>
             </tbody>
           </table>
         </div>
       </div>
       <div class="modal-footer">
         <!-- <button type="button" class="btn btn-success" @click="resturarEquipo()" data-dismiss="modal">Sí, quiero restaurarlo.</button> -->
         <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> -->
       </div>
     </div>

   </div>
 </div>
