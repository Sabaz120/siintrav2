<!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#eliminarEquipo">Open Modal</button> -->
<!-- Modal -->

<div class="modal fade" id="verOficio" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title mx-auto text-white">Ver Solicitud</h4>
      </div>
      <div class="modal-body text-center row">
        <div class="col-md-12">
          <p style="float:left">
            <strong>N° de Registro: </strong>@{{oficio_editar.id}}
          </p>
          <strong>Fecha de Registro: </strong>@{{oficio_editar.f_registro}}
          <p style="float:right;">
            <strong>Recibido por: </strong>@{{oficio_editar.usuario_creador}}
          </p>
        </div>
        <div class="col-md-12">
          <h3 class="text-center header-title m-t-0 m-b-20 pt-2">DATOS GENERALES DEL OFICIO</h3>
        </div>
        <div class="col-md-4">
          <strong>N° de Oficio</strong>
          <input type="text" class="form-control" style="text-align:center;" readonly v-bind:value="oficio_editar.numero_oficio">
        </div>
        <div class="col-md-4">
          <strong>Fecha de Oficio</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.fecha_emision">
        </div>
        <div class="col-md-4">
          <strong>Estado</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.estado">
        </div>
        <div class="col-md-12">
          <h3 class="text-center header-title m-t-20 m-b-10 pt-2">DATOS DEL EMISOR</h3>
        </div>
        <div class="col-md-4">
          <strong>Tipo de solicitante</strong>
          <input type="text" style="text-align:center;" class="form-control" v-if="oficio_editar.personal_juridico_id!=null" readonly value="Personal Jurídico">
          <input type="text" style="text-align:center;" class="form-control" v-if="oficio_editar.personal_juridico_id==null" readonly value="Personal Natural">
        </div>
        <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
          <strong>Clasificación del Ente</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.personal_juridico_clasificacion_ente">
        </div>
        <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
          <strong>Ente</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.personal_juridico_ente">
        </div>
        <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
          <strong>R.I.F</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.personal_juridico_rif">
        </div>
        <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
          <strong>Nombres de la Entidad</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.personal_juridico_nombre">
        </div>
        <div class="col-md-4">
          <strong>Cédula</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.personal_natural_cedula">
        </div>
        <div class="col-md-4">
          <strong>Nombres y Apellidos</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.personal_natural_nombres">
        </div>
        <div class="col-md-4">
          <strong>Código Carnet de la Patria</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.personal_natural_carnet">
        </div>
        <div class="col-md-4">
          <strong>Teléfono de Contacto</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.personal_natural_telefono">
        </div>
        <div class="col-md-4">
          <strong>Correo Electrónico</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.personal_natural_correo">
        </div>
        <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
          <strong>Departamento</strong>
          <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-bind:value="oficio_editar.personal_natural_departamento">
        </div>
        <div class="col-md-12">
          <h3 class="text-center header-title m-t-20 m-b-20 pt-2">DETALLE DEL OFICIO</h3>
        </div>
        <div class="col-md-4">
          <strong>Motivo de Solicitud</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.motivo_solicitud">
          <br>
          <div v-show="oficio_editar.productos.length>0">
            <strong>Teléfonos solicitados:</strong>
            <div class="table-responsive">
              <table class="table table-bordered table-shape">
                <thead class="bg-primary text-white">
                  <th>Modelo</th>
                  <th>Cantidad</th>
                </thead>
                <tbody>
                  <tr v-for="producto in oficio_editar.productos">
                    <td>@{{producto.producto.descripcion}}</td>
                    <td>@{{producto.cantidad}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- <div class="" v-show="oficio_editar.detalle_evento_id==0">
            <i class="fas fa-asterisk"></i>
            <strong>Cantidad Solicitada</strong>
            <input type="text" style="text-align:center;" readonly class="form-control"  v-model="oficio_editar.cantidad">
          </div> -->
        </div>
        <div class="col-md-4" v-show="oficio_editar.motivo_solicitud_id == 2">
          <strong>Fecha del Evento</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.fecha_evento">
        </div>
        <div class="col-md-4" v-show="oficio_editar.motivo_solicitud_id == 2">
          <strong>Hora del Evento</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.hora_evento">
        </div>
        <div class="col-md-8" v-show="oficio_editar.motivo_solicitud_id == 2">
          <strong>Lugar del Evento</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.lugar_evento">
        </div>
        <div class="col-md-8" v-show="oficio_editar.motivo_solicitud_id != 2">
          <strong>Breve descripción</strong>
          <!-- <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.descripcion"> -->
          <textarea name="name" readonly v-model="oficio_editar.descripcion" class="form-control" rows="5" cols="80"></textarea>
        </div>
        <div class="col-md-4">

          <table class="table table-bordered" v-show="oficio_editar.motivo_solicitud_id == 3" style="margin-top: -5rem;">
            <thead style="background-color: #7cb5ec !important; color: #fff;">
              <tr>
                <th scope="col">Material</th>
                <th scope="col">Cantidad</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="materiales in oficio_editar.materiales">
                <td>@{{ materiales.nombre_material }}</td>
                <td>@{{ materiales.cantidad }}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- <div class="col-md-4" v-show="oficio_editar.detalle_evento_id==0">
          <strong>Cantidad Solicitada</strong>
          <input type="text" style="text-align:center;" class="form-control" readonly v-bind:value="oficio_editar.cantidad">
        </div> -->
        <div class="col-md-12" v-show="oficio_editar.documento!=null">
          <h3 class="text-center header-title m-t-20 m-b-20 pt-2">DOCUMENTO ANEXADO</h3>
          <div class="card">
            <div class="card-body">
              <a class="btn btn-success" :href="oficio_editar.documento" download>
                Descargar Adjunto
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>

</div>
