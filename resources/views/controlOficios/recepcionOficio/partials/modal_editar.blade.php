<!-- Trigger the modal with a button -->
 <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#eliminarEquipo">Open Modal</button> -->
 <!-- Modal -->

 <div class="modal fade" id="editarOficio" role="dialog">
   <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header bg-primary">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
         <h4 class="modal-title mx-auto text-white">Editar Solicitud</h4>
       </div>
       <div class="modal-body text-center row">
         <div class="col-md-12">
           <p style="float:left">
             <strong>N° de Registro: </strong>@{{oficio_editar.id}}
           </p>
           <strong>Fecha de Registro: </strong>@{{oficio_editar.f_registro}}
           <p style="float:right;">
             <strong>Recibido por: </strong>@{{oficio_editar.usuario_creador}}
           </p>
         </div>
         <div class="col-md-12">
           <h3 class="text-center header-title m-t-0 m-b-20 pt-2">DATOS GENERALES DEL OFICIO</h3>
         </div>
         <div class="col-md-4">
           <strong>N° de Oficio</strong>
           <input type="text" class="form-control" v-mask="'########'" style="text-align:center;" v-model="oficio_editar.numero_oficio">
         </div>
         <div class="col-md-4">
           <i class="fas fa-asterisk"></i>
           <strong>Fecha de Oficio</strong>
           <input type="text" class="form-control datepicker" data-date-format="dd-mm-yyyy" id="oficio_editar_fecha_emision" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
         </div>
         <div class="col-md-4">
           <i class="fas fa-asterisk"></i>
           <strong>Estado</strong>
           <select class="form-control" name="estado_id" v-model="oficio_editar.estado_id" id="estado_id">
             <option value="0">Selecciona un estado</option>
             <option v-for="estado in estados" v-bind:value="estado.idEstado">
               @{{ estado.estado }}
             </option>
           </select>
           <!-- <input type="text" style="text-align:center;" class="form-control" readonly v-model="oficio_editar.estado"> -->
         </div>
         <div class="col-md-6">
           <br>
           <strong>Anexar Nuevo Documento Digital:</strong>
           <input type="file" id="file2" accept="application/pdf" v-on:change="showLogo2" class="btn btn-primary" style="width:auto" name="documento_anexo">
         </div>
         <div class="col-md-6" v-show="oficio_editar.documento!=null">
           <br>
           <strong>Documento Digital:</strong>
           <br>
           <a class="btn btn-success form-control" :href="oficio_editar.documento" download>
             Descargar Adjunto
           </a>
         </div>
         <div class="col-md-12">
           <h3 class="text-center header-title m-t-20 m-b-10 pt-2">DATOS DEL EMISOR</h3>
         </div>
         <div class="col-md-4">
           <strong>Tipo de solicitante</strong>
           <input type="text" style="text-align:center;" class="form-control" v-if="oficio_editar.personal_juridico_id!=null" readonly value="Personal Jurídico">
           <input type="text" style="text-align:center;" class="form-control" v-if="oficio_editar.personal_juridico_id==null" readonly value="Personal Natural">
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
           <strong><i class="fas fa-asterisk"></i> R.I.F:</strong>
           <div class="input-group mb-4">
             <input type="text" name="" class="form-control" v-model="oficio_editar.personal_juridico_rif" v-mask="'A-#########'" value="" readonly>
             <div class="input-group-append">
               <button class="btn btn-info" @click="buscarEmpresaEditar" type="button"><i class="fa fa-search"></i></button>
             </div>
           </div>
           <!-- <strong>R.I.F</strong> -->
           <!-- <input type="text" style="text-align:center;" class="form-control"  v-model="oficio_editar.personal_natural_cedula"> -->
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
           <strong>Clasificación del Ente</strong>
           <input type="text" style="text-align:center" readonly class="form-control text-capitalize"  v-model="oficio_editar.personal_juridico_clasificacion_ente">
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
           <strong>Ente</strong>
           <input type="text" style="text-align:center" readonly class="form-control text-capitalize"  v-model="oficio_editar.personal_juridico_ente">
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
           <strong>Nombres de la Entidad</strong>
           <input type="text" style="text-align:center" readonly class="form-control text-capitalize"  v-model="oficio_editar.personal_juridico_nombre">
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
           <strong>Cédula del Responsable</strong>
           <input type="text" style="text-align:center;" readonly class="form-control"  v-model="oficio_editar.personal_natural_cedula">
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id==null">
           <!-- <strong>Cédula</strong>
           <input type="text" style="text-align:center;" class="form-control"  v-model="oficio_editar.personal_natural_cedula"> -->
           <strong><i class="fas fa-asterisk"></i> Cédula:</strong>
           <div class="input-group mb-4">
             <input type="text" name="" v-model="oficio_editar.personal_natural_cedula" v-mask="'########'" readonly class="form-control" value="">
             <div class="input-group-append">
               <button class="btn btn-info" @click="buscarPersonaNaturalEditar()" type="button"><i class="fa fa-search"></i></button>
             </div>
           </div>
         </div>
         <div class="col-md-4">
           <strong>Nombres y Apellidos</strong>
           <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-model="oficio_editar.personal_natural_nombres">
         </div>
         <div class="col-md-4">
           <strong>Código Carnet de la Patria</strong>
           <input type="text" style="text-align:center;" class="form-control" readonly v-model="oficio_editar.personal_natural_carnet">
         </div>
         <div class="col-md-4">
           <strong>Teléfono de Contacto</strong>
           <input type="text" style="text-align:center;" class="form-control" readonly v-model="oficio_editar.personal_natural_telefono">
         </div>
         <div class="col-md-4">
           <strong>Correo Electrónico</strong>
           <input type="text" style="text-align:center;text-capitalize" class="form-control" readonly v-model="oficio_editar.personal_natural_correo">
         </div>
         <div class="col-md-4" v-show="oficio_editar.personal_juridico_id!=null">
           <strong>Departamento</strong>
           <input type="text" style="text-align:center" class="form-control text-capitalize" readonly v-model="oficio_editar.personal_natural_correo">
         </div>
         <div class="col-md-12">
           <h3 class="text-center header-title m-t-20 m-b-20 pt-2">DETALLE DEL OFICIO</h3>
         </div>
         <div class="col-md-4">
           <i class="fas fa-asterisk"></i>
           <strong>Motivo de solicitud:</strong>
           <select class="form-control" disabled v-model="oficio_editar.motivo_solicitud_id" >
              <option v-for="motivo in motivos_solicitud" v-bind:value="motivo.id">
               @{{ motivo.nombre }}
             </option>
           </select>
           <br>
           <!-- <div class="" v-show="oficio_editar.motivo_solicitud_id!=2">
             <i class="fas fa-asterisk"></i>
             <strong>Cantidad Solicitada</strong>
             <input type="text" style="text-align:center;" v-mask="'###'" class="form-control"  v-model="oficio_editar.cantidad">
           </div> -->
         </div>
         <div class="col-md-4" v-show="oficio_editar.motivo_solicitud_id==2">
           <i class="fas fa-asterisk"></i>
           <strong>Fecha del Evento</strong>
           <input type="text" class="form-control datepicker" data-date-format="dd-mm-yyyy" id="oficio_editar_fecha_evento" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
         </div>
         <div class="col-md-4" v-show="oficio_editar.motivo_solicitud_id==2">
           <i class="fas fa-asterisk"></i>
           <strong>Hora del Evento</strong>
           <input type="text" class="form-control" name="" id="oficio_editar_hora_evento" value="">
         </div>
         <div class="col-md-8" v-show="oficio_editar.motivo_solicitud_id==2">
           <i class="fas fa-asterisk"></i>
           <strong>Lugar del Evento</strong>
           <input type="text" style="text-align:center;" class="form-control"  v-model="oficio_editar.lugar_evento">
         </div>
         <div class="col-md-8" v-show="oficio_editar.motivo_solicitud_id!=2">
           <i class="fas fa-asterisk"></i>
           <strong>Breve descripción</strong>
           <textarea name="name" v-model="oficio_editar.descripcion" class="form-control" rows="2" cols="80"></textarea>
         </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-success" @click="actualizarDatosOficio()" data-dismiss="modal">Actualizar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>
