
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == true">
  <strong>Nombres y Apellidos:</strong>
  <input type="text" class="form-control" name="" v-model="pt_nombres" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == true">
  <strong>Gerencia:</strong>
  <input type="text" class="form-control" name="" v-model="pt_gerencia" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == true">
  <strong>Cargo:</strong>
  <input type="text" class="form-control" name="" v-model="pt_cargo" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == true">
  <strong>Teléfono de contacto:</strong>
  <input type="text" class="form-control" name="" v-model="pt_telefono" readonly>
</div>
<div class="col-md-3 text-center" v-show="tipo_solicitante_id==1 && es_trabajador == true">
  <strong>Correo Electrónico:</strong>
  <input type="text" class="form-control" name="" v-model="pt_correo" readonly>
</div>
