<!-- Trigger the modal with a button -->
 <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#eliminarEquipo">Open Modal</button> -->
 <!-- Modal -->
 <div class="modal fade" id="agregarMateriales" role="dialog">
   <div class="modal-dialog modal-lg">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header bg-primary">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
         <h4 class="modal-title mx-auto text-white">Materiales y Cantidades</h4>
       </div>
       <div class="modal-body text-center">
         
          <div class="container">
            <div class="row">
                
            <div class="col-lg-5">
              <label>Materiales</label>
              <select class="form-control" v-model="material_donativo">
                <option v-bind:value="0">Seleccione Material</option>
                <option v-for="(material,index) in materiales_donativo" v-bind:value="index+1">@{{ material.nombre }}</option>
              </select>
            </div>
            <div class="col-lg-5">
              <label>Cantidad</label>
              <input type="text" v-model="cantidad_donativo" class="form-control" maxlength="3" data-previous-value="1" style="text-align: center;" @keypress="onlyNumber">
            </div>
            <div class="col-lg-2">
              <button style="margin-top: 30px;" class="btn btn-success" @click="agregarMaterialDonativo()">Agregar</button>
            </div>

            </div>
            <div class="row">
              <div class="col-12">
                <h5 class="text-center header-title m-t-20 m-b-20 pt-2">LISTADO MATERIALES</h5>
                <table class="table table-bordered table-shape table-striped" style="width:97%">
                   <thead class="bg-primary text-white">
                     <tr>
                       <th style="text-align:center">Materiales</th>
                       <th style="text-align:center">Cantidad</th>
                       <th style="text-align:center">Acción</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr v-for="(material,index) in materiales_donar">
                       <td>@{{material.nombre}}</td>
                       <td>@{{material.cantidad}}</td>
                       <td>
                         <span title="Eliminar" @click="eliminarMaterialDonado(material.id)" data-toggle="modal" data-target="#eliminarTlf" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true" ></i></span>
                       </td>
                     </tr>
                 </tbody>
               </table>
              </div>
            </div>
          </div>

       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-success" @click="registrarMaterialesOficios()">Registrar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal" >Cerrar</button>
       </div>
     </div>

   </div>
 </div>
