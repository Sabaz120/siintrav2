<div class="col-md-2" v-show="motivo_solicitud_id==2">
  <i class="fas fa-asterisk"></i>
  <strong>Fecha del evento:</strong>
  <input type="text" class="form-control datepicker" data-date-format="dd-mm-yyyy" id="fecha_evento" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
</div>
<div class="col-md-2" v-show="motivo_solicitud_id==2">
  <i class="fas fa-asterisk"></i>
  <strong>Hora del evento:</strong>
  <input type="text" class="form-control" name="" id="hora_evento" value="">
</div>
<div class="col-md-4" v-show="motivo_solicitud_id==2">
  <i class="fas fa-asterisk"></i>
  <strong>Lugar del evento:</strong>
  <input type="text" class="form-control" v-model="lugar_evento" name="" value="">
</div>
