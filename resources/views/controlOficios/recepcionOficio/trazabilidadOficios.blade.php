@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <h3 class="text-center header-title m-t-0 m-b-10 pt-2" v-show="oficios.length>0">LISTADO DE OFICIOS</h3>
    <div class="row col-md-12" v-show="oficiosTemp.length>0">
      <div class="table-responsive">
        <div class=" ml-5">
          <!-- <div class="row">
            <div class="col-12 col-md-9 col-lg-9">
              <br>
              <div class="mt-2 form-inline font-weight-bold">
                Mostrar
                <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                  <option v-for="option in optionspageSize" v-bind:value="option.value">
                    @{{ option.text }}
                  </option>
                </select>
                registros
              </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <label for="Buscar" class="font-weight-bold">Buscar:</label>
                <input id="search" class="form-control form-control-sm" type="text" v-model="search" v-on:keyup="filtrar" required>
              </div>
            </div>
          </div> -->
          <table class="table table-bordered table-shape table-striped" style="width:97%">
            <thead class="bg-primary text-white">
              <tr>
                <th style="text-align:center">ID</th>
                <th style="text-align:center">N° oferta económica</th>
                <th style="text-align:center">Cliente</th>
                <th style="text-align:center">Oferta Ecónomica</th>
                <th style="text-align:center">Requisición</th>
                <th style="text-align:center">Despachado</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(oficio,index) in oficios_registrados" v-show="statusFiltro==1">

                <td style="text-align:center" class="text-capitalize">@{{oficio.id}}</td>
                <td style="text-align:center" class="text-capitalize" v-if="oficio.oferta_economica!=null">
                  @{{oficio.oferta_economica.id}}
                </td>
                <td v-else>
                  No posee
                </td>
                <td style="text-align:center" class="text-capitalize" v-if="oficio.personal_juridico">@{{oficio.personal_juridico.nombre_institucion}} / @{{oficio.personal_natural.nombre}} @{{oficio.personal_natural.apellido}} </td>
                <td style="text-align:center" class="text-capitalize" v-else>@{{oficio.personal_natural.nombre}} @{{oficio.personal_natural.apellido}}</td>
                <td style="text-align:center" class="text-capitalize" v-if="oficio.oferta_economica!=null">
                  <i class="fa fa-check"></i>
                </td>
                <td style="text-align:center" class="text-capitalize" v-else>
                  <i class="fa fa-window-close"></i>
                </td>
                <td style="text-align:center" class="text-capitalize" v-if="oficio.oferta_economica!=null">
                  <i class="fa fa-check" v-if="oficio.oferta_economica.requisicion"></i>
                  <i class="fa fa-window-close" v-else></i>
                </td>
                <td style="text-align:center" class="text-capitalize" v-else>
                  <i class="fa fa-window-close"></i>
                </td>
                <td style="text-align:center" class="text-capitalize" v-if="oficio.oferta_economica">
                  <span v-if="oficio.oferta_economica.requisicion">
                    <span v-if="oficio.oferta_economica.requisicion.despacho">
                      <i class="fa fa-check" v-if="oferta_economica.requisicion.despacho.despachado"></i>
                      <i class="fa fa-window-close" v-else></i>
                    </span>
                    <span v-else>
                      <i class="fa fa-window-close"></i>
                    </span>
                  </span>
                  <span v-else>
                    <i class="fa fa-window-close"></i>
                  </span>
                </td>
                <td style="text-align:center" class="text-capitalize" v-else>
                  <i class="fa fa-window-close"></i>
                </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="4">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{oficios.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    // Variables DB
    oficios: {!! $oficios ? $oficios : "''"!!},
    oficiosTemp: {!! $oficios ? $oficios : "''"!!},
    search:'',
    currentSort:'id',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'200',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    productos:[],
    producto_id:0,
    productosSeleccionados:[]
  },
  computed:{
    oficios_registrados:function() {
      this.rows=0;
      return this.oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort].nombre < b[this.currentSort].nombre)
        return -1 * modifier;
        if(a[this.currentSort].nombre > b[this.currentSort].nombre)
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{

    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        // console.log('entre');
        // console.log(this.search);
        for(let i in this.oficios){
          if(this.oficios[i].personal_natural.nombre.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].personal_natural.apellido.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.oficios[i]);
          }//if(this.NivelAtencion[i].nivel.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.NivelAtencion)
        //Si filtrardo.length >0, oficios se iguala a filtrardo
        //Sino mostrar mensaje que no se encontraron resultados
        if(filtrardo.length>0){
          this.oficios=filtrardo;
          this.statusFiltro=1;
        }
        else{
          this.statusFiltro=0;
          this.oficios=this.oficiosTemp;
        }// if(filtrardo.length)
      }else{
        // console.log('Search vacio');
        this.statusFiltro=1;
        this.oficios=this.oficiosTemp;
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      // console.log(s);
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },

    limpiar(){
    },

  },
  mounted(){

  }
});//const app= new Vue

</script>
@endpush
