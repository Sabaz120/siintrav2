@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <div class="card-header">
      <h5 class="text-center header-title pt-1">REGISTRO DE OFICIOS</h5>
      <strong>Fecha de Registro: </strong>{{date('d-m-Y')}}
      <p style="float:right;">
        <strong>Recibido por: </strong>{{Auth::user()->name}}
      </p>
    </div>
    <div class="card-body">
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">DATOS GENERALES DEL OFICIO</h3>
      <div class="row">
        <div class="col-md-3 text-center">
          <strong>N° de Oficio:</strong>
          <input type="text" style="text-align:center" v-mask="'########'" class="form-control" name="n_oficio" v-model="n_oficio" value="" >
          <!-- <input id="ente" class="form-control" v-bind:class="{ 'is-invalid': enteRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="data.ente"  placeholder="Ente" required> -->
        </div>
        <div class="col-md-3 text-center">
          <i class="fas fa-asterisk"></i>
          <strong>Fecha de Oficio:</strong>
          <input type="text" class="form-control datepicker" data-date-format="dd-mm-yyyy" id="fecha_recepcion" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
          <!-- <input id="ente" class="form-control" v-bind:class="{ 'is-invalid': enteRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="data.ente"  placeholder="Ente" required> -->
        </div>
        <div class="col-md-3 text-center ">
          <i class="fas fa-asterisk"></i>
          <strong>Estado:</strong>
          <select class="form-control" name="estado_id" v-model="estado_id" id="estado_id">
            <option value="0">Selecciona un estado</option>
            <option v-for="estado in estados" v-bind:value="estado.idEstado">
              @{{ estado.estado }}
            </option>
          </select>
          <!-- <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)" minlength="3" maxlength="100"  type="text" v-model="data.descripcion"  placeholder="Descripción" required> -->
        </div>
        <div class="col-md-3 text-center ">
          <strong>Anexar Documento Digital:</strong>
          <br>
          <input type="file" id="file" style="width:98%" accept="application/pdf" v-on:change="showLogo" class="btn btn-primary" name="documento_anexo">
        </div>
      </div>
      <h5 class="text-center header-title m-t-20 m-b-20 pt-2">DATOS DEL EMISOR</h5>
      <div class="row">
        <div class="col-md-3 text-center">
          <i class="fas fa-asterisk"></i>
          <strong>Tipo de solicitante:</strong>
          <select class="form-control" name="tipo_solicitante_id" v-model="tipo_solicitante_id">
            <option value="1">Persona Natural</option>
            <option value="2">Persona Jurídica</option>
          </select>
          <!-- <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)" minlength="3" maxlength="100"  type="text" v-model="data.descripcion"  placeholder="Descripción" required> -->
        </div>
        @include('controlOficios.recepcionOficio.partials.datos_emisor_persona_natural')
        @include('controlOficios.recepcionOficio.partials.datos_emisor_persona_juridica')
        @include('controlOficios.recepcionOficio.partials.datos_emisor_trabajador')
      </div> <!-- Fin row datos de la solicitud -->
      <h5 class="font-weight-bold text-center header-title m-t-20 m-b-20 pt-2">DETALLE DEL OFICIO</h5>
      <div class="row">
        <div class="col-md-4 text-center ">
          <i class="fas fa-asterisk"></i>
          <strong>Motivo de solicitud:</strong>
          <select class="form-control" v-model="motivo_solicitud_id" name="motivo_solicitud_id">
            <option value="0">Seleccione un motivo de solicitud</option>
            <option v-for="motivo in motivos_solicitud" v-bind:value="motivo.id">
              @{{ motivo.nombre }}
            </option>
          </select>
          <!-- <br>
          <div v-show="motivo_solicitud_id!=2">
          <i class="fas fa-asterisk"></i>
          <strong>Cantidad solicitada:</strong>
          <input type="text" style="text-align:center;" v-mask="'###'" v-model="cantidad_solicitada" class="form-control" name="cantidad_solicitada" value="0">
        </div> -->
      </div>
      <div class="col-md-8 text-center" v-show="motivo_solicitud_id!=2">
        <i class="fas fa-asterisk"></i>
        <strong>Breve descripción:</strong>
        <textarea name="name" v-model="descripcion" class="form-control" rows="2" cols="80"></textarea>
      </div>
      @include('controlOficios.recepcionOficio.partials.oficio_detalle_evento')
    </div> <!-- FIN row Resumen de la solicitud -->
    <div class="row">
      <div class="mx-auto">
        <br>
        <button type="button" class="btn btn-success" name="button" @click="RegistrarOficio()">Registrar</button>
        <button type="button" class="btn btn-warning" name="button" data-toggle="modal" data-target="#limpiarForm">Limpiar</button>
      </div>
    </div> <!-- Fin row botones -->
  </div>
  @include('controlOficios.recepcionOficio.partials.modal_seleccionar_responsable')
  @include('controlOficios.recepcionOficio.partials.modal_agregar_productos')
  @include('controlOficios.recepcionOficio.partials.modal_seleccionar_responsable_editaroficio')
  @include('controlOficios.recepcionOficio.partials.modal_eliminar')
  @include('controlOficios.recepcionOficio.partials.modal_limpiar')
  @include('controlOficios.recepcionOficio.partials.modal_ver_oficio')
  @include('controlOficios.recepcionOficio.partials.modal_editar')
  @include('controlOficios.recepcionOficio.partials.modal_materiales')

  <hr>
  <h3 class="text-center header-title m-t-0 m-b-10 pt-2" v-show="oficios.length>0">LISTADO DE OFICIOS</h3>
  <div class="row col-md-12" v-show="oficiosTemp.length>0">
    <div class="table-responsive">
      <div class=" ml-5">
        <div class="row">
          <div class="col-12 col-md-9 col-lg-9">
            <br>
            <div class="mt-2 form-inline font-weight-bold">
              Mostrar
              <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                <option v-for="option in optionspageSize" v-bind:value="option.value">
                  @{{ option.text }}
                </option>
              </select>
              registros
            </div>
          </div>
          <div class="col-12 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="Buscar" class="font-weight-bold">Buscar:</label>
              <input id="search" class="form-control form-control-sm" type="text" v-model="search" v-on:keyup="filtrar" required>
            </div>
          </div>
        </div>
        <table class="table table-bordered table-shape table-striped" style="width:97%">
          <thead class="bg-primary text-white">
            <tr>
              <th style="text-align:center">Cliente</th>
              <th style="text-align:center" @click="sort('motivo_solicitud')">Motivo De Solicitud</th>
              <th style="text-align:center" @click="sort('descripcion')">Detalle</th>
              <!-- <th style="text-align:center">CANTIDAD SOLICITADA</th> -->
              <th style="text-align:center">Acción</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(oficio,index) in oficios_registrados" v-show="statusFiltro==1">

              <td style="text-align:center" class="text-capitalize" v-if="oficio.personal_juridico">@{{oficio.personal_juridico.nombre_institucion}} / @{{oficio.personal_natural.nombre}} @{{oficio.personal_natural.apellido}} </td>
              <td style="text-align:center" class="text-capitalize" v-else>@{{oficio.personal_natural.nombre}} @{{oficio.personal_natural.apellido}}</td>
              <td style="text-align:center" class="text-capitalize">@{{oficio.motivo_solicitud.nombre}}</td>
              <td style="text-align:center" class="text-capitalize">
                @{{oficio.descripcion}}
              </td>
              <!-- <td style="text-align:center" v-if="oficio.motivo_solicitud_id!=2">@{{oficio.cantidad}}</td> -->
              <!-- <td style="text-align:center" v-else>0</td> -->

              <td class="text-center">
                 <span title="Asociar materiales a solicitud" @click="capturarOficioDonativo(oficio.id)" v-show="oficio.motivo_solicitud_id==3" data-toggle="modal" data-target="#agregarMateriales" class="btn btn-primary btn-sm"><i class="fas fa-hand-holding-heart"></i></span>
                <span title="Agregar solicitud de equipos" v-show="oficio.motivo_solicitud_id==1" @click="capturarOficioEditar(oficio.id)" data-toggle="modal" data-target="#agregarProductos" class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></span>
                <span title="Ver oficio" @click="capturarOficioEditar(oficio.id)" data-toggle="modal" data-target="#verOficio" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></span>
                <span title="Editar" @click="capturarOficioEditar(oficio.id)" data-toggle="modal" data-target="#editarOficio" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                <span title="Eliminar" @click="capturarOficioEliminar(oficio.id)" data-toggle="modal" data-target="#eliminarOficio" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true" ></i></span>
              </td>
            </tr>
            <tr v-show='statusFiltro==0'>
              <td class="text-center font-weight-bold" colspan="5">No se encontraron registros</td>
            </tr>
        </tbody>
      </table>
      <div class="">
        <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{oficios.length}}</strong>
        <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
        <div style="float:right">
          <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
          <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
          <div class="row ml-2">
            <strong>Página:  @{{currentPage}}</strong>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    // Variables DB
    estados: {!! $estados ? $estados : "''"!!},
    motivos_solicitud: {!! $motivos_solicitud ? $motivos_solicitud : "''"!!},
    oficios: {!! $oficios ? $oficios : "''"!!},
    oficiosTemp: {!! $oficios ? $oficios : "''"!!},
    materiales_donativo: {!! $materiales ? $materiales: "''" !!},
    materiales_donados:[],
    // Variables registro
    es_trabajador:false,
    material_donativo:0,
    cantidad_donativo:1,
    materiales_donar:[],
    es_trabajador:false,
    n_oficio:'',
    oficio_donativo:'',
    fecha_recepcion:'',
    estado_id:0,
    documento_anexo:'',
    documento_anexo2:'',
    tipo_solicitante_id:'1',
    cedula_persona_natural:'',
    errorCedula:1,
    motivo_solicitud_id:0,
    cantidad_solicitada:1,
    descripcion:'',
    pn_nombres:'',
    pn_cod_carnet:'',
    pn_telefono:'',
    pn_correo:'',
    cedula_trabajador:'',
    pt_nombres:'',
    pt_gerencia:'',
    pt_cargo:'',
    pt_telefono:'',
    pt_correo:'',
    lugar_evento:'',
    empresa_nombres:'',
    empresa_departamento:'',
    empresa_responsable_cedula:'',
    empresa_responsable_correo:'',
    empresa_responsable_carnet:'',
    empresa_responsable_nombres:'',
    empresa_responsable_telefono:'',
    rif_empresa:'',
    responsables_empresa:[],
    index_oficio_eliminar:0,
    oficio_editar:{
      id:0,
      user_id:0,
      numero_oficio:0,
      fecha_emision:'',
      estado_id:0,
      estado:'',
      documento:'',
      cantidad:0,
      descripcion:'',
      motivo_solicitud_id:0,
      motivo_solicitud:'',
      personal_natural_id:0,
      personal_juridico_id:null,
      detalle_evento_id:0,
      fecha_evento:'',
      hora_evento:'',
      lugar_evento:'',
      personal_natural_nombres:'',
      personal_natural_cedula:'',
      personal_natural_carnet:'',
      personal_natural_telefono:'',
      personal_natural_correo:'',
      personal_natural_departamento:'',
      personal_juridico_nombre:'',
      personal_juridico_ente:'',
      personal_juridico_clasificacion_ente:'',
      personal_juridico_departamento:'',
      n_registro:0,
      f_registro:'',
      usuario_creador:'',
      personal_juridico_rif:'',
      productos:[]
    },
    search:'',
    currentSort:'id',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    productos:[],
    producto_id:0,
    productosSeleccionados:[]
  },
  computed:{
    oficios_registrados:function() {
      this.rows=0;
      return this.oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort].nombre < b[this.currentSort].nombre)
        return -1 * modifier;
        if(a[this.currentSort].nombre > b[this.currentSort].nombre)
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    keyrif(event){
      // console.log(event.key);
      // console.log(this.rif_empresa.length);
      if(this.rif_empresa.length<=2){
        if(event.key=="j"){
          this.rif_empresa="J-";
        }else if(event.key=="c"){
          this.rif_empresa="C-";
        }else if(event.key=="C"){
          this.rif_empresa="C-";
        }else if(event.key=="g"){
          this.rif_empresa="G-";
        }else if(event.key=="J"){
          this.rif_empresa="J-";
        }else if(event.key=="G"){
          this.rif_empresa="G-";
        }else if (event.key=="V" || event.key=="v") {
          this.rif_empresa="V-";
        }
        else{
          this.rif_empresa="";
        }
      }//lengt<1
    },
    eliminarTlf:function(index){
      axios.post('{{ url("recepcion_oficios/borrarTlf") }}', {telefono:this.productosSeleccionados[index],oficio_id:this.oficio_editar.id}).then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          this.productosSeleccionados.splice(index,1);
        }else if(response.data.error==1){
          this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
        }else{
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    registrarCantidadTlf:function(){
      //this.oficio_editar.id
      if(this.productosSeleccionados.length<1){
        alertify.error('Debes al menos agregar un equipo');
      }else{
        axios.post('{{ url("recepcion_oficios/asignarTlf") }}', {telefonos:this.productosSeleccionados,oficio_id:this.oficio_editar.id}).then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
          }else if(response.data.error==1){
            this.limpiar();
            alertify.error(response.data.msg);
          }else if(response.data.error==500){
            alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
            console.log(response.data.msg);
          }else{
            console.log('Error sin codigo:'+response.data.msg);
          }
        }).catch(error => {
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      }//else no errors
    },
    seleccionarProducto:function(){
      var index=0;
      for(var i=0;i<this.productos.length;i++){
        if(this.productos[i].id==this.producto_id){
          index=i;
          break;
        }
      }//
      if(this.producto_id==0){
        alertify.error('Debes seleccionar un modelo');
      }else if(this.cantidad_solicitada<1){
        alertify.error('La cantidad mínima debe ser 1');
      }else{
        var b=0;//Si ya el modelo fue seleccionado se agrega la nueva cantidad
        for(var i=0;i<this.productosSeleccionados.length;i++){
          if(this.productosSeleccionados[i].modelo==this.productos[index].nombre){
            b=1;
            this.productosSeleccionados[i].cantidad+=parseInt(this.cantidad_solicitada);
            alertify.warning('Este modelo ya se encontraba agregado, se ha sumado la cantidad.');
            break;
          }
        }//for productos seleccionados
        if(b==0)
        this.productosSeleccionados.push({'id':this.productos[index].id,'modelo':this.productos[index].nombre,'descripcion':this.productos[index].descripcion,'cantidad':parseInt(this.cantidad_solicitada)});
        this.producto_id=0;
        this.cantidad_solicitada=1;
      }//else
    },
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        // console.log('entre');
        // console.log(this.search);
        for(let i in this.oficios){
          if(this.oficios[i].personal_natural.nombre.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].personal_natural.apellido.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.oficios[i]);
          }//if(this.NivelAtencion[i].nivel.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.NivelAtencion)
        //Si filtrardo.length >0, oficios se iguala a filtrardo
        //Sino mostrar mensaje que no se encontraron resultados
        if(filtrardo.length>0){
          this.oficios=filtrardo;
          this.statusFiltro=1;
        }
        else{
          this.statusFiltro=0;
          this.oficios=this.oficiosTemp;
        }// if(filtrardo.length)
      }else{
        // console.log('Search vacio');
        this.statusFiltro=1;
        this.oficios=this.oficiosTemp;
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      // console.log(s);
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
    showLogo:function(event){
      let _this=this;
      // Reference to the DOM input element
      var input = event.target;
      // Ensure that you have a file before attempting to read it
      if (input.files && input.files[0]) {
        // create a new FileReader to read this image and convert to base64 format
        var reader = new FileReader();
        // Define a callback function to run, when FileReader finishes its job
        reader.onload = (e) => {
          // Note: arrow function used here, so that "this.imgLogo" refers to the imgLogo of Vue component
          // Read image as base64 and set to imgLogo
          _this.documento_anexo = e.target.result;
        }
        // Start the reader job - read file as a data url (base64 format)
        reader.readAsDataURL(input.files[0]);
      }
    },//showLogo:function(event)
    showLogo2:function(event){
      let _this=this;
      // Reference to the DOM input element
      var input = event.target;
      // Ensure that you have a file before attempting to read it
      if (input.files && input.files[0]) {
        // create a new FileReader to read this image and convert to base64 format
        var reader = new FileReader();
        // Define a callback function to run, when FileReader finishes its job
        reader.onload = (e) => {
          // Note: arrow function used here, so that "this.imgLogo" refers to the imgLogo of Vue component
          // Read image as base64 and set to imgLogo
          _this.documento_anexo2 = e.target.result;
        }
        // Start the reader job - read file as a data url (base64 format)
        reader.readAsDataURL(input.files[0]);
      }
    },//showLogo2:function(event)
    limpiar(){

      this.cantidad_donativo=1
      this.material_donativo=0
      this.pt_nombres='',
      this.pt_gerencia='',
      this.pt_cargo='',
      this.pt_telefono='',
      this.pt_correo='',
      this.pn_nombres='',
      this.pn_cod_carnet='',
      this.pn_telefono='',
      this.pn_correo='',
      this.descripcion='',
      this.cantidad_solicitada=1,
      this.cedula_persona_natural='',
      this.cedula_trabajador='',
      this.motivo_solicitud_id=0,
      this.tipo_solicitante_id=1,
      this.documento_anexo='',
      this.estado_id=0,
      this.fecha_recepcion='',
      this.n_oficio='',
      this.errorCedula=1,
      this.lugar_evento='',
      this.empresa_nombres='',
      this.empresa_departamento='',
      this.empresa_responsable_cedula='',
      this.empresa_responsable_correo='',
      this.empresa_responsable_carnet='',
      this.empresa_responsable_nombres='',
      this.empresa_responsable_telefono='',
      this.rif_empresa='',
      this.es_trabajador=false,
      $('#file').val(""),
      $('#file2').val(""),
      this.documento_anexo='',
      this.documento_anexo2='',
      producto_id=0,
      productosSeleccionados=[]
    },
    previewFiles(event) {
      console.log(event.target.files);
      this.documento_anexo = event.target.files;
    },
    buscarPersonaNatural(){
      axios.post('{{ route("api.controloficio.datos_persona_natural") }}', {cedper:this.cedula_persona_natural}).then(response => {

        if(response.data.error==0){

          if(response.data.es_trabajador == true){

            //this.tipo_solicitante_id = 3
            this.es_trabajador = true
            this.pt_nombres = response.data.datos_trabajador[0].nombres
            this.pt_gerencia = response.data.datos_trabajador[0].departamento
            this.pt_cargo = response.data.datos_trabajador[0].cargo
            this.pt_telefono = response.data.datos_trabajador[0].telefonomovil
            this.pt_correo = response.data.datos_trabajador[0].coreleper
            this.errorCedula=0;

          }else{

            this.es_trabajador = false
            this.pn_nombres=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
            this.pn_cod_carnet=response.data.persona_natural.codigo_carnet_patria;
            this.pn_telefono=response.data.persona_natural.telefono;
            this.pn_correo=response.data.persona_natural.correo_electronico;
            this.es_trabajador = response.data.persona_natural.trabajador_vtelca;
            this.errorCedula=0;

            if(this.es_trabajador){

              this.pt_nombres = response.data.datos_trabajador[0].nombres
              this.pt_gerencia = response.data.datos_trabajador[0].departamento
              this.pt_cargo = response.data.datos_trabajador[0].cargo
              this.pt_telefono = response.data.datos_trabajador[0].telefonomovil
              this.pt_correo = response.data.datos_trabajador[0].coreleper
              this.errorCedula=0;

            }

          }


        }else if(response.data.error==1){
          this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    buscarPersonaNaturalEditar(){
      // console.log(this.oficio_editar.personal_natural_cedula);
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:this.oficio_editar.personal_natural_cedula}).then(response => {
        if(response.data.error==0){
          this.oficio_editar.personal_natural_id=response.data.persona_natural.id;
          this.oficio_editar.personal_natural_nombres=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
          this.oficio_editar.personal_natural_carnet=response.data.persona_natural.codigo_carnet_patria;
          this.oficio_editar.personal_natural_telefono=response.data.persona_natural.telefono;
          this.oficio_editar.personal_natural_correo=response.data.persona_natural.correo_electronico;
          this.errorCedula=0;
        }else if(response.data.error==1){
          this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    RegistrarOficio(){
      if($('#fecha_recepcion').val()==""){
        alertify.error('Debe seleccionar la fecha de recepción del oficio');
      }else if(this.estado_id==0){
        alertify.error('Debe seleccionar el estado de donde proviene el oficio.');
        // }else if(this.cantidad_solicitada=="" || this.cantidad_solicitada=="0"){
        //   alertify.error('Debe ingresar una cantidad de teléfonos válida');
      }else if(this.motivo_solicitud_id=="0"){
        alertify.error('Debe seleccionar un motivo de solicitud');
      }else if(this.descripcion=="" && this.motivo_solicitud_id!="2"){
        alertify.error('Debe ingresar una descripción');
      }else if((this.descripcion.length<10 || this.descripcion.length>100) && this.motivo_solicitud_id!="2"){
        alertify.error('El campo descripción debe tener mínimo 10 dígitos y máximo 100.');
      }else{
        var b=0;
        if(this.tipo_solicitante_id=="1"){
          if(this.cedula_persona_natural=="" || this.errorCedula==1){
            alertify.error('Debe ingresar una cédula válida');
          }else{
            b=1;
          }
        }else if(this.tipo_solicitante_id=="2"){
          if(this.rif_empresa==""){
            alertify.error('Debe ingresar un RIF de empresa.');
          }else if(this.empresa_responsable_nombres=""){
            alertify.error('Posterior a buscar la empresa, debe seleccionar un responsable.');
          }else{
            b=1;
          }
        }//tipo_solicitante_id
        if(this.motivo_solicitud_id=="2"){
          if($('#fecha_evento').val()==""){
            alertify.error('Debes seleccionar la fecha de inicio del evento');
          }else if($('#hora_evento').val()=="" || $('#hora_evento').val()==null){
            alertify.error('Debes seleccionar la hora de inicio del evento.');
          }else if(this.lugar_evento==""){
            alertify.error('Debes ingresar el lugar del evento');
          }else if(this.lugar_evento.length<3){
            alertify.error('La información del lugar del evento es muy corta.');
          }else{
            b=1;
          }
        }//motivo_solicitud_id
        if(this.tipo_solicitante_id == "3"){
          b=1
        }

        if(b==1){

          if(this.cedula_trabajador)
            this.cedula_persona_natural = this.cedula_trabajador

          axios.post('{{ url("recepcion_oficios") }}',
          {n_oficio:this.n_oficio,
            fecha_recepcion:this.invertirFecha($('#fecha_recepcion').val()),
            estado_id:this.estado_id,
            documento_anexo:this.documento_anexo,
            tipo_solicitante_id:this.tipo_solicitante_id,
            cedper:this.cedula_persona_natural,
            motivo_solicitud_id:this.motivo_solicitud_id,
            cantidad_solicitada:this.cantidad_solicitada,
            descripcion:this.descripcion,
            fecha_evento:$('#fecha_evento').val(),
            hora_evento:$('#hora_evento').val(),
            lugar_evento:this.lugar_evento,
            rif_empresa:this.rif_empresa
          })
          .then(response => {
            if(response.data.error==0){
              alertify.success(response.data.msg);
              this.oficios=response.data.oficios;
              this.oficiosTemp=response.data.oficios;
              this.limpiar();
            }else if(response.data.error==1){
              alertify.error(response.data.msg);
            }else if(response.data.error==500){
              alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
              console.log(response.data.msg);
              this.limpiar();
            }else{
              this.limpiar();
              console.log('Error sin codigo:'+response.data.msg);
            }
          }).catch(error => {
            // this.limpiar();
            alertify.error('Error en el servidor.');
            console.log('Axios api datos persona natural catch');
            console.log(error);
          });
        }
      }//else
    },
    agregarMaterialDonativo(){

      if(this.cantidad_donativo == 0){
        alertify.error('La cantidad mínima debe ser 1');
      }else if(this.material_donativo == 0){

        alertify.error('Debe seleccionar un material')

      }else{

        var b = 0

        for(var i=0;i<this.materiales_donar.length;i++){

          if(this.materiales_donar[i].id == this.materiales_donativo[this.material_donativo-1].id){
            b=1;
            this.materiales_donar[i].cantidad+=parseInt(this.cantidad_donativo);
            alertify.warning('Este material ya se encontraba agregado, se ha sumado la cantidad.');
            break;
          }
        }//for productos seleccionados

        if(b == 0){

          var nombre = this.materiales_donativo[this.material_donativo-1].nombre
          var id = this.materiales_donativo[this.material_donativo-1].id
          var cantidad = parseInt(this.cantidad_donativo)

          this.materiales_donar.push({nombre, id, cantidad})
          this.material_donativo = 0
          this.cantidad_donativo = 1


        }

      }

    },
    eliminarMaterialDonado(id){

      axios.post("{{ url('eliminar_materiales_donados') }}", {id: id, oficio_id: this.oficio_donativo}).then(response => {

        for(var i=0;i<this.materiales_donar.length;i++){

            if(this.materiales_donar[i].id == id){

              this.materiales_donar.splice(i, 1)
              //this.obtenerMaterialesRegistrados()
            }
          }

      })

    },
    registrarMaterialesOficios(){

      if(this.materiales_donar.length > 0){
        axios.post("{{ url('agregar_materiales_donativos') }}", {materiales: this.materiales_donar, oficio_id: this.oficio_donativo}).then(response => {
          if(response.data.error == 0){
            $('#agregarMateriales').modal('hide');
            alertify.success(response.data.msg)
          }else
            alertify.error(response.data.msg)
        })
      }else
        alertify.error('Debe agregar materiales a la solicitud')
    },
    capturarOficioDonativo(id){

      this.oficio_donativo = id
      this.obtenerMaterialesRegistrados()

    },
    obtenerMaterialesRegistrados(){

      axios.post("{{ url('obtener_materiales_donativos') }}", {oficio_id: this.oficio_donativo}).then( response => {

        this.materiales_donar = response.data

      })

    },
    onlyNumber ($event) {
       //console.log($event.keyCode); //keyCodes value
       let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
       if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
          $event.preventDefault();
       }
    },
    invertirFecha(fecha){
      var fecha=fecha.split("-");
      if(fecha[0].length==4)
      fecha=fecha[2]+"-"+fecha[1]+"-"+fecha[0];
      else
      fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
      return fecha;
    },
    invertirFechaCreatedAt(fecha){
      var fecha=fecha.split("-");
      var ano=fecha[2].split(" ");
      fecha=ano[0]+"-"+fecha[1]+"-"+fecha[0];
      return fecha;
    },
    buscarEmpresa(){
      axios.post('{{ route("api.controloficio.datos_personal_juridico") }}', {rif:this.rif_empresa}).then(response => {
        if(response.data.error==0){
          this.responsables_empresa=response.data.responsables;
          this.empresa_nombres=response.data.empresa.nombre_institucion;
          $('#responsablesEmpresa').show().modal({backdrop: 'static', keyboard: false})
        }else if(response.data.error==1){
          // this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos personal juridico catch');
        console.log(error);
      });
    },
    buscarEmpresaEditar(){
      axios.post('{{ route("api.controloficio.datos_personal_juridico") }}', {rif:this.oficio_editar.personal_juridico_rif}).then(response => {
        if(response.data.error==0){
          this.responsables_empresa=response.data.responsables;
          this.personal_juridico_nombre=response.data.empresa.nombre_institucion;
          $('#editarOficio').modal('toggle');
          $('#responsablesEmpresaEditarOficio').show().modal({backdrop: 'static', keyboard: false});
        }else if(response.data.error==1){
          // this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos personal juridico catch');
        console.log(error);
      });
    },
    seleccionarResponsable(index){
      this.cedula_persona_natural=this.responsables_empresa[index].personal_natural.cedula;
      this.empresa_departamento=this.responsables_empresa[index].departamento.nombre;
      this.empresa_responsable_cedula=this.responsables_empresa[index].personal_natural.cedula;
      this.empresa_responsable_nombres=this.responsables_empresa[index].personal_natural.nombre+' '+this.responsables_empresa[index].personal_natural.apellido;
      this.empresa_responsable_carnet=this.responsables_empresa[index].personal_natural.codigo_carnet_patria;
      this.empresa_responsable_telefono=this.responsables_empresa[index].personal_natural.telefono;
      this.empresa_responsable_correo=this.responsables_empresa[index].personal_natural.correo_electronico;
    },
    seleccionarResponsableEditarOficio(index){
      $('#editarOficio').modal('toggle');
      this.oficio_editar.personal_natural_id=this.responsables_empresa[index].personal_natural.id;
      this.oficio_editar.personal_natural_cedula=this.responsables_empresa[index].personal_natural.cedula;
      this.oficio_editar.personal_juridico_departamento=this.responsables_empresa[index].departamento.nombre;
      this.oficio_editar.personal_natural_nombres=this.responsables_empresa[index].personal_natural.nombre+' '+this.responsables_empresa[index].personal_natural.apellido;
      this.oficio_editar.personal_natural_carnet=this.responsables_empresa[index].personal_natural.codigo_carnet_patria;
      this.oficio_editar.personal_natural_telefono=this.responsables_empresa[index].personal_natural.telefono;
      this.oficio_editar.personal_natural_correo=this.responsables_empresa[index].personal_natural.correo_electronico;
    },
    capturarOficioEliminar(oficio_id){
      for(var i=0;i<this.oficios.length;i++){
        if(this.oficios[i].id==oficio_id){
          this.index_oficio_eliminar=i;
        }
      }//for
    },
    eliminarOficio(){
      //this.index_oficio_eliminar
      axios.post('{{ url("recepcion_oficios/borrar") }}', {oficio_id:this.oficios[this.index_oficio_eliminar].id}).then(response => {
        if(response.data.error==0){
          //Sin errores
          alertify.success(response.data.msg);
          this.oficios.splice(this.index_oficio_eliminar,1);
        }else if(response.data.error==1){
          // this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos personal juridico catch');
        console.log(error);
      });
    },
    capturarOficioEditar(oficio_id){
      //this.oficios[this.index_oficio_eliminar].id
      axios.post('{{ route("api.controloficio.obtenerdatosoficio") }}', {oficio_id:oficio_id}).then(response => {

        if(response.data.error==0){
          //Sin errores

          //console.log(response.data);
          this.oficio_editar.personal_natural_id=response.data.oficio.personal_natural_id;
          this.oficio_editar.personal_juridico_id=response.data.oficio.personal_juridico_id;
          this.oficio_editar.cantidad=response.data.oficio.cantidad;
          this.oficio_editar.descripcion=response.data.oficio.descripcion;
          if(response.data.detalle_oficio!=null){
            console.log(response.data.detalle_oficio.fecha)
            this.oficio_editar.detalle_evento_id=response.data.detalle_oficio.id;
            this.oficio_editar.fecha_evento=response.data.detalle_oficio.fecha;
            //$('#oficio_editar_fecha_evento').val(this.invertirFecha(this.oficio_editar.fecha_evento));
            this.oficio_editar.hora_evento=response.data.detalle_oficio.hora;
            $('#oficio_editar_hora_evento').val(this.oficio_editar.hora_evento);
            this.oficio_editar.lugar_evento=response.data.detalle_oficio.lugar;
          }
          else{
            this.oficio_editar.detalle_evento_id=0;
            this.oficio_editar.fecha_evento=null;
            this.oficio_editar.hora_evento=null;
            this.oficio_editar.lugar_evento=null;
          }
          this.oficio_editar.materiales = response.data.materiales
          this.oficio_editar.fecha_emision=this.invertirFecha(response.data.oficio.fecha_emision);

          this.oficio_editar.numero_oficio=response.data.oficio.numero_oficio;
          this.oficio_editar.personal_natural_nombres=response.data.oficio.personal_natural.nombre+" "+response.data.oficio.personal_natural.apellido;
          this.oficio_editar.documento=response.data.oficio.documento;
          this.oficio_editar.personal_natural_carnet=response.data.oficio.personal_natural.codigo_carnet_patria;
          this.oficio_editar.personal_natural_cedula=response.data.oficio.personal_natural.cedula;
          this.oficio_editar.personal_natural_telefono=response.data.oficio.personal_natural.telefono;
          this.oficio_editar.personal_natural_correo=response.data.oficio.personal_natural.correo_electronico;
          if(response.data.responsable!=null)
          this.oficio_editar.personal_natural_departamento=response.data.responsable.departamento.nombre;
          this.oficio_editar.usuario_creador=response.data.oficio.usuario_creador.name;
          this.oficio_editar.n_registro=response.data.oficio.id;
          this.oficio_editar.id=response.data.oficio.id;
          this.oficio_editar.f_registro=this.invertirFechaCreatedAt(response.data.oficio.created_at);
          this.oficio_editar.estado=response.data.estado.estado;
          this.oficio_editar.estado_id=response.data.estado.idEstado;
          this.oficio_editar.motivo_solicitud=response.data.oficio.motivo_solicitud.nombre;
          this.oficio_editar.motivo_solicitud_id=response.data.oficio.motivo_solicitud.id;
          if(response.data.oficio.personal_juridico!=null){
            this.oficio_editar.personal_juridico_nombre=response.data.oficio.personal_juridico.nombre_institucion;
            this.oficio_editar.personal_juridico_ente=response.data.oficio.personal_juridico.ente.nombre;
            this.oficio_editar.personal_juridico_clasificacion_ente=response.data.oficio.personal_juridico.ente.clasificacion_ente.nombre;
            this.oficio_editar.personal_juridico_rif=response.data.oficio.personal_juridico.rif_cedula_situr;
            this.oficio_editar.departamento=response.data.departamento.nombre;
          }else{
            this.oficio_editar.personal_juridico_nombre=null;
            this.oficio_editar.personal_juridico_ente=null;
            this.oficio_editar.personal_juridico_clasificacion_ente=null;
            this.oficio_editar.personal_juridico_rif=null;
            this.oficio_editar.departamento=null;
          }//else
          this.oficio_editar.productos=response.data.oficio.solicitud_productos;
          this.errorCedula=0;

          $('#oficio_editar_fecha_emision').val(this.invertirFecha(this.oficio_editar.fecha_emision));
          this.productosSeleccionados=[];
          for(var i=0;i<response.data.oficio.solicitud_productos.length;i++){
            this.productosSeleccionados.push({'id':response.data.oficio.solicitud_productos[i].producto_id,'modelo':response.data.oficio.solicitud_productos[i].producto.nombre,'descripcion':response.data.oficio.solicitud_productos[i].producto.descripcion,'cantidad':response.data.oficio.solicitud_productos[i].cantidad});
          }//for
        }else if(response.data.error==1){
          // this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }//else
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos oficio catch');
        console.log(error);
      });
    },//capturarOficioEditar()
    actualizarDatosOficio(){
      if($('#oficio_editar_fecha_emision').val()==""){
        alertify.error('Debe seleccionar la fecha de recepción del oficio');
      }else if(this.oficio_editar.estado_id==0){
        alertify.error('Debe seleccionar el estado de donde proviene el oficio.');
        // }else if(this.oficio_editar.cantidad=="" || this.oficio_editar.cantidad=="0"){
        //   alertify.error('Debe ingresar una cantidad de teléfonos válida');
      }else if(this.oficio_editar.motivo_solicitud_id=="0"){
        alertify.error('Debe seleccionar un motivo de solicitud');
      }else{
        var b=0;
        if(this.oficio_editar.personal_juridico_id==null){
          if(this.oficio_editar.cedula_persona_natural=="" || this.errorCedula==1){
            alertify.error('Debe ingresar una cédula válida');
          }else{
            b=1;
          }
        }else if(this.oficio_editar.personal_juridico_id!=null){
          if(this.oficio_editar.personal_juridico_rif==""){
            alertify.error('Debe ingresar un RIF de empresa.');
          }else if(this.oficio_editar.personal_juridico_nombre=""){
            alertify.error('Posterior a buscar la empresa, debe seleccionar un responsable.');
          }else{
            b=1;
          }
        }//tipo_solicitante_id
        if(this.oficio_editar.motivo_solicitud_id=="2"){
          b=0;
          //motivo solicitud==evento
          if($('#oficio_editar_fecha_evento').val()==""){
            alertify.error('Debes seleccionar la fecha de inicio del evento');
          }else if($('#oficio_editar_hora_evento').val()=="" || $('#hora_evento').val()==null){
            alertify.error('Debes seleccionar la hora de inicio del evento.');
          }else if(this.oficio_editar.lugar_evento==""){
            alertify.error('Debes ingresar el lugar del evento');
          }else if(this.oficio_editar.lugar_evento.length<3){
            alertify.error('La información del lugar del evento es muy corta.');
          }else if(this.oficio_editar.lugar_evento.length>100){
            alertify.error('La información del lugar del evento es muy larga.');
          }else{
            b=1;
          }
        }//motivo_solicitud_id
        if(b==1){
          this.oficio_editar.fecha_evento=this.invertirFecha($('#oficio_editar_fecha_evento').val());
          this.oficio_editar.hora_evento=$('#oficio_editar_hora_evento').val();
          this.oficio_editar.fecha_emision=this.invertirFecha($('#oficio_editar_fecha_emision').val());
          axios.post('{{ url("recepcion_oficios/actualizar") }}', {oficio:this.oficio_editar,documento_anexo:this.documento_anexo2}).then(response => {

            //console.log(response.data)

            if(response.data.error==0){
              //Sin errores
              alertify.success(response.data.msg);
              this.oficios=response.data.oficios;
              this.oficiosTemp=response.data.oficios;
            }else if(response.data.error==1){
              // this.limpiar();
              alertify.error(response.data.msg);
            }else if(response.data.error==500){
              alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
              console.log(response.data.msg);
              this.limpiar();
            }else{
              this.limpiar();
              console.log('Error sin codigo:'+response.data.msg);
            }
          }).catch(error => {
            this.limpiar();
            alertify.error('Error en el servidor.');
            console.log('Axios actualizar oficio catch');
            console.log(error);
          });

        }//b==1
      }
    },//actualizarDatosOficio()
  },
  mounted(){

    _this = this;

    $('#agregarMateriales').on('hidden.bs.modal', function () {

      _this.material_donativo = 0
      _this.cantidad_donativo = 1
      _this.materiales_donar = []

    })

    var d=new Date();
    var ano= d.getFullYear();
    var mes= d.getMonth()+1;
    var dia=d.getDate();
    if(dia<10)
    dia='0'+dia;
    if(mes<10)
    mes='0'+mes;
    $('#fecha_recepcion').val(dia+'-'+mes+'-'+ano);
    $('#fecha_evento').val(dia+'-'+mes+'-'+ano);
    $('#oficio_editar_fecha_evento').val(dia+'-'+mes+'-'+ano);
    $('#hora_evento').timepicker({
      'minTime': '8:00am',
      'maxTime': '11:30pm',
    });
    $('#oficio_editar_hora_evento').timepicker({
      'minTime': '8:00am',
      'maxTime': '11:30pm',
    });
    $('#hora_evento').val('8:00am');
    $('#oficio_editar_hora_evento').val('8:00am');
    axios.post('{{ route("api.sistemaproduccionv1.obtenerproductos") }}', {}).then(response => {
      if(response.data.error==0){
        // console.log(response.data.productos);
        this.productos=response.data.productos;
      }else if(response.data.error==1){
        alertify.error(response.data.msg);
      }else if(response.data.error==500){
        alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
        console.log(response.data.msg);
      }else{
        this.limpiar();
        console.log('Error sin codigo:'+response.data.msg);
      }
    }).catch(error => {
      alertify.error('Error en el servidor.');
    });
  }
});//const app= new Vue

</script>
@endpush
