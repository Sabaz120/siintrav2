@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">Ofertas Económicas para Facturar</h5>
      <div class="row justify-content-center">
        <div class="col-12 table-responsive" v-if="Oficios.length>0" >
          <div class="row">
            <div class="col-12 col-md-9 col-lg-9">
              <br>
              <div class="mt-2 form-inline font-weight-bold">
                Mostrar
                <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                  <option v-for="option in optionspageSize" v-bind:value="option.value">
                    @{{ option.text }}
                  </option>
                </select>
                registros
              </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <label for="Buscar" class="font-weight-bold">Buscar:</label>
                <input id="search" class="form-control form-control-sm" type="text" v-model="search" v-on:keyup="filtrar" required>
              </div>
            </div>
          </div>
          <table  class="table table-bordered text-center" >
            <thead class="bg-primary text-white">
              <tr>
                <td>N° de Oferta</td>
                <td>Solicitante/Ente</td>
                <td>Cantidad Total</td>
                <td>Monto Total</td>
                <td>Acción</td>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(oficio,index) in oficios_registrados" v-show="statusFiltro==1">
                <td>@{{oficio.oferta_economica_id}}</td>
                <td>@{{oficio.solicitante_ente}}</td>
                <td>@{{oficio.cantidad_total}}</td>
                <td>@{{oficio.monto_total}}</td>
                <td>
                  <button v-if="oficio.estado_aprobacion_id==6" type="button" class="btn btn-success text-white text-center mr-2" data-toggle="modal" data-target="#modal_plan_despacho" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Plan de despacho" @click="capturarOficio(oficio.id)"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                  <button v-else="" type="button" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#modal_despacho" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Cerrar despacho" @click="capturarOficio(oficio.id)"><i class="fa fa-user-check" aria-hidden="true"></i></button>
                </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="5">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Oficios.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 text-center jumbotron" v-else>
            <h5>Sin ofertas económicas por facturar</h5>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL PLAN DE DESPACHO -->
  <div class="modal" id="modal_plan_despacho" v-if="Oficios.length>0">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header bg-primary text-center">
          <h4 class="modal-title mx-auto text-white">PLAN DE DESPACHO</h4>
          <button type="button" title="Cerrar" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body row justify-content-center">
          <div class="col-md-4 text-center">
            <!-- <i class="fa fa-asterisk"></i> -->
            <strong>Número de Factura:</strong>
            <input type="text" style="text-align:center;" class="form-control" :maxlength="20" v-mask="'####################'" v-model="num_factura" >
          </div>
          <div class="col-md-4 text-center">
            <!-- <i class="fa fa-asterisk"></i> -->
            <strong>Fecha:</strong>
            <!-- <input type="text" style="text-align:center;" class="form-control" v-model="fecha_despacho" > -->
            <input type="text" class="form-control datepicker" onchange="obtenerDespachos()" data-date-format="dd-mm-yyyy" id="fecha_despacho" data-provide="datepicker" style="text-align:center;" placeholder="dd-mm-yyyy" v-mask="'##-##-####'" name="" value="">
          </div>
          <div class="col-12 text-center table-responsive" v-if="despachos_pendientes.length>0">
            <br>
            <table class="table table-bordered text-center">
              <thead class="bg-primary text-white">
                <th>Agente de venta</th>
                <th>Solicitante</th>
                <th>Modelos</th>
                <th>Cantidad Total</th>
              </thead>
              <tbody>
                <tr v-for="despacho in despachos_pendientes">
                  <td v-if="despacho.gerencia_creador_oficio=='GERENCIA DE ASUNTOS PUBLICOS'">Agente Autorizado</td>
                  <td v-else-if="despacho.gerencia_creador_oficio=='UNIDAD DE MECADEO Y COMERCIALIZACION'">Comercialización</td>
                  <td v-else>Indefinido</td>
                  <td>@{{despacho.solicitante_ente}}</td>
                  <td>@{{despacho.modelos}}</td>
                  <td>@{{despacho.cantidad_total}}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-12 text-center">
            <br>
            <button type="button" class="btn btn-success" name="button" @click="despacho()" >Registrar</button>
          </div>
        </div>
        <!-- Modal footers -->
        <!-- <div class="modal-footer mx-auto">
          <button type="button" class="btn btn-success" name="button" >Registrar.</button>
        </div> -->
      </div>
    </div>
  </div>
  <!-- MODAL PLAN DE DESPACHO -->
  <!-- MODAL DESPACHO -->
  <div class="modal" id="modal_despacho" v-if="Oficios.length>0">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header bg-primary text-center">
          <h4 class="modal-title mx-auto text-white">Confirmar Despacho</h4>
          <button type="button" title="Cerrar" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body row justify-content-center">
          <div class="col-md-12 text-center">
            <strong>Observación:</strong>
            <textarea name="name" style="text-align:center;" :maxlength="180" placeholder="Observaciones del despacho"  class="form-control" v-model="observacion_despacho"  rows="5" cols="80"></textarea>
            <!-- <input type="text" style="text-align:center;" placeholder="Observaciones del despacho" class="form-control" v-model="observacion_despacho" > -->
          </div>
          <div class="col-12 text-center">
            <br>
            <button type="button" class="btn btn-success" name="button" @click="confirmarDespacho()" >Culminar despacho</button>
          </div>
        </div>
        <!-- Modal footers -->
        <!-- <div class="modal-footer mx-auto">
          <button type="button" class="btn btn-success" name="button" >Registrar.</button>
        </div> -->
      </div>
    </div>
  </div>
  <!-- MODAL DESPACHO -->
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    Oficios:{!! json_encode($oficios) ? json_encode($oficios) : "''"!!},
    indexOficio:0,
    num_factura:'',
    observacion_despacho:'',
    despachos_pendientes:[],
    // Paginación
    search:'',
    currentSort:'solicitante_ente',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'25',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0,
    // Paginación
  },
  computed:{
    oficios_registrados:function() {
      this.rows=0;
      return this.Oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    // Paginación
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        // console.log('entre');
        // console.log(this.search);
        for(let i in this.Oficios){
          if(this.Oficios[i].solicitante_ente.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.Oficios[i]);
          }//if(this.NivelAtencion[i].nivel.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.NivelAtencion)
        //Si filtrardo.length >0, oficios se iguala a filtrardo
        //Sino mostrar mensaje que no se encontraron resultados
        if(filtrardo.length>0){
          this.Oficios=filtrardo;
          this.statusFiltro=1;
        }
        else{
          this.statusFiltro=0;
          this.Oficios={!! json_encode($oficios) ? json_encode($oficios) : "''"!!};
        }// if(filtrardo.length)
      }else{
        // console.log('Search vacio');
        this.statusFiltro=1;
        this.Oficios={!! json_encode($oficios) ? json_encode($oficios) : "''"!!};
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.Oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      // console.log(s);
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
    // Paginación
    limpiar(){
      this.num_factura='';
      //INput fecha despacho
      var d=new Date();
      var ano= d.getFullYear();
      var mes= d.getMonth()+1;
      var dia=d.getDate();
      if(dia<10)
      dia='0'+dia;
      if(mes<10)
      mes='0'+mes;
      $('#fecha_despacho').val(dia+'-'+mes+'-'+ano);
      //INput fecha despacho
    },
    capturarOficio:function(id,tipo_operacion=null){
      for(var i=0;i<this.Oficios.length;i++){
        if(this.Oficios[i].id==id){
          this.indexOficio=i;
        }//if this.oficio
      }//for
    },
    despacho(){
      var fecha_despacho=$('#fecha_despacho').val();
      if(this.num_factura==""){
        alertify.error('Debes ingresar un número de factura');
      }else if(fecha_despacho==""){
        alertify.error("Debe seleccionar una fecha de despacho");
      }else{
        axios.post('{{ route("guardarDatosDespacho") }}', {
          oferta_economica_id:this.Oficios[this.indexOficio].oferta_economica_id,
          oficio_id:this.Oficios[this.indexOficio].id,
          fecha_despacho:fecha_despacho,
          num_factura:this.num_factura
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Oficios.splice(this.indexOficio,1);
            this.Oficios=response.data.oficios;//Esto por sí se han agregado nuevos oficios con estado de pendiente por facturación
            $('#modal_plan_despacho').modal('toggle');
            this.limpiar();
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    },
    confirmarDespacho(){
      axios.post('{{ route("confirmarDespacho") }}', {
        oferta_economica_id:this.Oficios[this.indexOficio].oferta_economica_id,
        oficio_id:this.Oficios[this.indexOficio].id,
        observacion:this.observacion_despacho
      })
      .then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          this.Oficios.splice(this.indexOficio,1);
          this.Oficios=response.data.oficios;//Esto por sí se han agregado nuevos oficios con estado de pendiente por facturación
          $('#modal_despacho').modal('toggle');
          this.limpiar();
        }//if(response.data.status=="success")
        else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    obtenerDespachosPendientesPorFecha(){
      var fecha_despacho=$('#fecha_despacho').val();
      axios.post('{{ route("api.controloficio.obtener.despachos.pendientes.fecha") }}', {
        fecha_despacho:fecha_despacho
      })
      .then(response => {
        if(response.data.error==0){
          this.despachos_pendientes=response.data.despachos;
        }//if(response.data.status=="success")
        else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    onlyNumber ($event) {
      //console.log($event.keyCode); //keyCodes value
      let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if (keyCode > 31 && (keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
        $event.preventDefault();
      }
    },
  },//methods
  mounted(){
    var d=new Date();
    var ano= d.getFullYear();
    var mes= d.getMonth()+1;
    var dia=d.getDate();
    if(dia<10)
    dia='0'+dia;
    if(mes<10)
    mes='0'+mes;
    $('#fecha_despacho').val(dia+'-'+mes+'-'+ano);
    this.obtenerDespachosPendientesPorFecha();
  }
});//const app= new Vue

function obtenerDespachos(){
  app.obtenerDespachosPendientesPorFecha();
}//
</script>
@endpush
