@extends('layouts.app')
@section('contenido')
<section id="register">
  @include('controlOficios.recepcionOficio.partials.modal_limpiar')
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 pt-5">REGISTRO DE PERSONAL NATURAL</h5>
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30">O FIRMA PERSONAL</h5>
      <div class="row justify-content-justify">
        <div class="col-12 col-md-2 col-lg-2">
          <div class="form-group text-center">
            <strong>¿Es firma personal?</strong>
            <div class="oye-checkbox oye-happy">
              <label>
                <input type="checkbox"  v-bind:id="data.firmaPersonal" v-model="data.firmaPersonal">Si
                <span class="oye-check"></span>
              </label>
            </div>
          </div>
        </div>
        <div class="col-11 col-md-2 col-lg-2">
          <div class="form-group text-center">

            <strong><i class="fas fa-asterisk"></i> Cédula:</strong>
            <div class="input-group mb-4">
              <input type="text" name="" v-model="data.cedula" v-mask="'########'" class="form-control" v-model="data.cedula"  onkeypress="return soloNumeros(event)">
              <div class="input-group-append">
                <button class="btn btn-info" @click="verificarPersona()" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>

          </div>
        </div>

        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Nombres:</strong>
            <input id="nombre" class="form-control text-uppercase text-uppercase input-disabled" v-bind:class="{ 'is-invalid': nombreRequerido }" minlength="3" maxlength="50"  type="text" v-model="data.nombre"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese nombre" readonly required >
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Apellidos:</strong>
            <input id="apellido" class="form-control text-uppercase input-disabled" v-bind:class="{ 'is-invalid': apellidoRequerido }" minlength="3" maxlength="50"  type="text" v-model="data.apellido"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese apellido" required readonly>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <!-- <i class="fas fa-asterisk"></i> -->
            <strong>Código Carnet de la Patria:</strong>
            <input id="codigoCarnetPatria" class="form-control input-disabled text-uppercase" v-bind:class="{ 'is-invalid': codigoCarnetPatriaRequerido }" minlength="10" maxlength="10"  type="text" v-model="data.codigo_carnet_patria"  onkeypress="return soloNumeros(event)" placeholder="Ingrese código carnet de la patria" required readonly>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4" v-show="data.gerencia">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Gerencia:</strong>
            <input class="form-control input-disabled text-uppercase" minlength="10" maxlength="10"  type="text" v-model="data.gerencia" required readonly>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4" v-show="data.cargo">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Cargo:</strong>
            <input class="form-control input-disabled text-uppercase" minlength="10" maxlength="10"  type="text" v-model="data.cargo" required readonly>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Teléfono de Contacto:</strong>
            <input id="telefono" class="form-control input-disabled text-uppercase" v-bind:class="{ 'is-invalid': telefonoRequerido }" minlength="10" maxlength="11"  type="tel" v-model="data.telefono"  onkeypress="return soloNumeros(event)" pattern="[0-9]{4}[0-9]{3}[0-9]{4}" placeholder="Ingrese télefono" required readonly>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Correo Electrónico:</strong>
            <input id="correoElectronico" class="form-control input-disabled text-uppercase" v-bind:class="{ 'is-invalid': correoElectronicoRequerido }" minlength="3" maxlength="50"  type="email" v-model="data.correoElectronico"   pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Ingrese correo" required readonly>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
        <button type="button" class="btn btn-success" v-on:click="registrar">Registrar</button>
        <button type="button" class="btn btn-warning" name="button" data-toggle="modal" data-target="#limpiarForm">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show='PersonalNatural.length'>
          <div class="row">
            <div class="header mx-auto">
              <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE PERSONAL NATURAL</h4>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <table  class="table table-bordered">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th scope="col" style="cursor:pointer; width:33%;" class="text-center" @click="sort('cedula')">Cédula</th>
                      <th scope="col" style="cursor:pointer; width:33%;" class="text-center" @click="sort('nombre')">Nombre y Apellido</th>
                      <th scope="col" class="text-center width:33%;">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="(personalNatural,index) in personal_natural_registradas" v-show='statusFiltro'>
                      <td class="text-center">
                        <p class="anchoparrafo">@{{personalNatural.cedula}}</p>
                      </td>
                      <td  class="text-center text-uppercase">@{{personalNatural.nombre +' '+ personalNatural.apellido}}</td>
                      <td class="text-center align-middle">
                        <button type="button" class="btn btn-info text-white text-center" data-toggle="modal" data-target="#myModalDetalle" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Detalle" @click="capturarData(personalNatural)"><i class="fas fa-eye fa-1x" aria-hidden="true"></i></button>
                        <button v-show="personalNatural.trabajador_vtelca == false" type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarData(personalNatural)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                        <!-- <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="datoBorrar(personalNatural.id)"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button> -->
                      </td>
                    </tr>
                    <tr v-show='!statusFiltro'>
                      <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{PersonalNatural.length}}</strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- The Modal Detalle -->
<div class="modal" id="myModalDetalle">
  <div class="modal-dialog modal-lg" style="max-width:80%;">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">DETALLES DE PERSONAL NATURAL</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-md-2 col-lg-">
            <div class="form-group text-center">
              <label for="firmaPersonal">¿Es firma personal?</label>
              <div class="oye-checkbox oye-happy">
                <label>
                  <input type="checkbox"  v-bind:id="firmaPersonal" v-model="firmaPersonal" disabled>Si
                  <span class="oye-check"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-2 col-lg-2">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="cedula">Cédula:</label>
              <input id="cedula" class="form-control" v-bind:class="{ 'is-invalid': cedulaRequerido }" minlength="7" maxlength="8"  type="text" v-model="cedula"  onkeypress="return soloNumeros(event)" placeholder="Ingrese cédula" disabled>
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="nombre">Nombres</label>
              <input id="nombre" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nombreRequerido }" minlength="3" maxlength="50"  type="text" v-model="nombre"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese nombre" disabled >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="apellido">Apellidos</label>
              <input id="apellido" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': apellidoRequerido }" minlength="3" maxlength="50"  type="text" v-model="apellido"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese apellido" disabled >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="codigoCarnetPatria">Código Carnet de la Patria:</label>
              <input id="codigoCarnetPatria" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': codigoCarnetPatriaRequerido }" minlength="10" maxlength="10"  type="text" v-model="codigoCarnetPatria"  onkeypress="return soloNumeros(event)" placeholder="Ingrese código carnet de la patria" disabled >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="telefono">Teléfono de Contacto:</label>
              <input id="telefono" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': telefonoRequerido }" minlength="11" maxlength="11"  type="tel" v-model="telefono"  onkeypress="return soloNumeros(event)" pattern="[0-9]{4}[0-9]{3}[0-9]{4}" placeholder="Ingrese télefono" disabled >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="correoElectronico">Correo Electrónico:</label>
              <input id="correoElectronico" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': correoElectronicoRequerido }" minlength="3" maxlength="50"  type="email" v-model="correoElectronico"   pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Ingrese correo" disabled>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- The Modal Editar -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg" style="max-width:80%;">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">EDICIÓN DE PERSONAL NATURAL</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-md-2 col-lg-">
            <div class="form-group text-center">
              <label for="firmaPersonal">¿Es firma personal?</label>
              <div class="oye-checkbox oye-happy">
                <label>
                  <input type="checkbox"  v-bind:id="firmaPersonal" v-model="firmaPersonal">Si
                  <span class="oye-check"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-2 col-lg-2">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="cedula">Cédula:</label>
              <input id="cedula" class="form-control" v-bind:class="{ 'is-invalid': cedulaRequerido }" minlength="7" maxlength="8"  type="text" v-model="cedula"  onkeypress="return soloNumeros(event)" placeholder="Ingrese cédula"required >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="nombre">Nombres</label>
              <input id="nombre" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nombreRequerido }" minlength="3" maxlength="50"  type="text" v-model="nombre"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese nombre"required >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="apellido">Apellidos</label>
              <input id="apellido" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': apellidoRequerido }" minlength="3" maxlength="50"  type="text" v-model="apellido"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese apellido"required >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="codigoCarnetPatria">Código Carnet de la Patria:</label>
              <input id="codigoCarnetPatria" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': codigoCarnetPatriaRequerido }" minlength="10" maxlength="10"  type="text" v-model="codigoCarnetPatria"  onkeypress="return soloNumeros(event)" placeholder="Ingrese código carnet de la patria"required >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="telefono">Teléfono de Contacto:</label>
              <input id="telefono" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': telefonoRequerido }" minlength="11" maxlength="11"  type="tel" v-model="telefono"  onkeypress="return soloNumeros(event)" pattern="[0-9]{4}[0-9]{3}[0-9]{4}" placeholder="Ingrese télefono" required >
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <label for="correoElectronico">Correo Electrónico:</label>
              <input id="correoElectronico" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': correoElectronicoRequerido }" minlength="3" maxlength="50"  type="email" v-model="correoElectronico"   pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Ingrese correo" required>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success text-white font-weight-bold" @click="editar()">Actualizar</button>
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- The Modal Eliminar -->
<div class="modal" id="myModalEliminar">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h5 class="modal-title mx-auto  text-white">
          ELIMINAR PERSONAL NATURAL</h4>
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mx-auto text-center">
            ¿Esta seguro de eliminar este registro?
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminar()">Sí, estoy seguro.</button>
          <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    errors: [],
    data:{
      cedula:'',
      nombre:'',
      apellido:'',
      codigo_carnet_patria:'',
      telefono:'',
      correoElectronico:'',
      firmaPersonal:false,
      cargo:'',
      gerencia:'',
      trabajador_vtelca:false
    },

    id:'',
    cedula:'',
    nombre:'',
    apellido:'',
    codigoCarnetPatria:'',
    telefono:'',
    correoElectronico:'',
    cargo:'',
    firmaPersonal:false,
    cedulaRequerido:false,
    nombreRequerido:false,
    apellidoRequerido:false,
    codigoCarnetPatriaRequerido:false,
    telefonoRequerido:false,
    correoElectronicoRequerido:false,
    firmaPersonalRequerido:false,
    PersonalNatural:{!! $PersonalNatural ? $PersonalNatural : "''"!!},
    search:'',
    rows:0,
    seleccione:'Seleccione',//opcion por defecto
    currentSort:'nombre',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1
  },


  computed:{

    personal_natural_registradas:function() {
      this.rows=0;
      return this.PersonalNatural.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },

  methods:{
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.PersonalNatural){
          if(this.PersonalNatural[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.PersonalNatural[i].apellido.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.PersonalNatural[i].cedula.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.PersonalNatural[i]);
          }//if(this.PersonalNatural[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.PersonalNatural[i].ponderacion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.PersonalNatural)
        if(filtrardo.length)
        this.PersonalNatural=filtrardo;
        else{
          this.statusFiltro=0;
          this.PersonalNatural={!! $PersonalNatural ? $PersonalNatural : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.PersonalNatural={!! $PersonalNatural ? $PersonalNatural : "''"!!};
      }//if(this.search)
    },// filtrar:function()

    limpiar:function(){
      this.errors= [];
      this.data={
        cedula:'',
        nombre:'',
        apellido:'',
        codigoCarnetPatria:'',
        telefono:'',
        correoElectronico:'',
        firmaPersonal:false,
      };
      this.id='';
      this.cedula='';
      this.nombre='';
      this.apellido='';
      this.codigoCarnetPatria='';
      this.telefono='';
      this.correoElectronico='';
      this.firmaPersonal=false;
      this.cedulaRequerido=false;
      this.nombreRequerido=false;
      this.apellidoRequerido=false;
      this.codigoCarnetPatriaRequerido=false;
      this.telefonoRequerido=false;
      this.correoElectronicoRequerido=false;
      this.firmaPersonalRequerido=false;
      this.search='';
      this.statusFiltro=1;
    },//limpiar:function()

    buscar:function(){
      for (let i in this.PersonalNatural){
        if(this.PersonalNatural[i].cedula==this.data.cedula){
          return(1);
        }//if(this.PersonalNatural[i].cedula==this.data.cedula)
      }//for (let i in this.PersonalNatural)
      return(0);
    },//buscar:function()

    buscarCodigo:function(){
      for (let i in this.PersonalNatural){
        if(this.PersonalNatural[i].codigo_carnet_patria==this.data.codigoCarnetPatria){
          return(1);
        }//  if(this.PersonalNatural[i].codigo_carnet_patria==this.data.codigoCarnetPatria)
      }//for (let i in this.PersonalNatural)
      return(0);
    },//buscar:function()

    validEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },//validEmail

    registrar:function(){
      this.errors=[];
      this.cedulaRequerido=false;
      this.nombreRequerido=false;
      this.apellidoRequerido=false;
      this.codigoCarnetPatriaRequerido=false;
      this.telefonoRequerido=false;
      this.correoElectronicoRequerido=false;
      this.firmaPersonalRequerido=false;

      if(this.data.trabajador_vtelca == false){

        if (!this.data.cedula) {
          this.errors.push("Existen campos por llenar:<br>*Cédula.");
          this.cedulaRequerido=true;
        }else if (this.data.cedula.toString().length<7){
          this.errors.push("El campo cédula acepta un minimo de 7 caracteres y un máximo de 8 caracteres.");
          this.cedulaRequerido=true;
        }else if (this.buscar()==1) {
          this.errors.push('No se puede duplicar el campo cédula.');
          this.nombreRequerido=true;
        }//else if (!this.data.cedula)

        if (!this.data.nombre) {
          this.errors.push("Existen campos por llenar:<br>*Nombre.");
          this.nombreRequerido=true;
        } else if (this.data.nombre.length<3 || this.data.nombre.length>50) {
          this.errors.push('El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
          this.nombreRequerido=true;
        }// if (!this.data.nombre)

        if (!this.data.apellido) {
          this.errors.push("Existen campos por llenar:<br>*Apellido.");
          this.apellidoRequerido=true;
        } else if (this.data.apellido.length<3 || this.data.apellido.length>50) {
          this.errors.push('El campo apellido acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
          this.apellidoRequerido=true;
        }// if (!this.data.apellido)


        // if (!this.data.codigo_carnet_patria) {
        //   this.errors.push("Existen campos por llenar:<br>*Código carnet de la patria");
        //   this.codigoCarnetPatriaRequerido=true;
        // } else if (this.data.codigo_carnet_patria.length<10 || this.data.codigo_carnet_patria.length>10) {
        //   this.errors.push('El campo código carnet de la patria debe contener 10 caracteres incluyendo los ceros.');
        //   this.codigoCarnetPatriaRequerido=true;
        // }else
        // if (this.data.codigo_carnet_patria && this.buscarCodigo()==1) {
        //   this.errors.push('No se puede duplicar el campo código carnet de la patria.');
        //   this.codigoCarnetPatriaRequerido=true;
        // }// if (!this.data.codigoCarnetPatria)


        if (!this.data.telefono) {
          this.errors.push("Existen campos por llenar:<br>*Teléfono");
          this.telefonoRequerido=true;
        } else if (this.data.telefono.length<10 || this.data.telefono.length>11) {
          this.errors.push('El campo teléfono debe contener al menos 10 caracteres incluyendo el código de area.');
          this.telefonoRequerido=true;
        }// if (!this.data.codigoCarnetPatria)

        if (!this.data.correoElectronico) {
          this.errors.push("Existen campos por llenar:<br>*Correo electrónico");
          this.correoElectronicoRequerido=true;
        } else if (this.data.correoElectronico.length<10 || this.data.correoElectronico.length>50) {
          this.errors.push('El campo correo electrónico acepta un minimo de 10 caracteres y un máximo de 50 caracteres.');
          this.correoElectronicoRequerido=true;
        } else if (!this.validEmail(this.data.correoElectronico)) {
          this.errors.push('El campo correo electrónico es Invalido.');
          this.correoElectronicoRequerido=true;
        } //if (!this.data.correo)


      }

      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
      }else{
        //Envio de datos a la base de datos*/
        let self = this;
        let cedula=this.data.cedula;
        let nombre=this.data.nombre.toLowerCase();
        let apellido=this.data.apellido.toLowerCase();
        let codigo_carnet_patria=this.data.codigo_carnet_patria;
        let telefono=this.data.telefono;
        let correo_electronico=this.data.correoElectronico.toLowerCase();
        let firma_personal=this.data.firmaPersonal;
        let trabajador_vtelca=this.data.trabajador_vtelca;

        axios.post('{{ url("registrar_personal_natural") }}', {
          cedula:cedula,
          nombre: nombre,
          apellido: apellido,
          codigo_carnet_patria:codigo_carnet_patria,
          telefono:telefono,
          correo_electronico:correo_electronico,
          firma_personal:firma_personal,
          trabajador_vtelca: trabajador_vtelca
        })
        .then(function (response) {
          if(response.data.status=="success"){
            self.PersonalNatural=response.data.PersonalNatural;
            self.limpiar();
            alertify.set('notifier','position', 'top-right');
            alertify.success('Registro Satisfactorio.');
          }//if(response.data.status=="success")
          if(response.data.status=="error"){
            alertify.set('notifier','position', 'top-right');
            $.each(response.data.mensaje, function( key, value ) {
              alertify.error(""+value);
            });
          }//if(response.data.status=="error")

        })
        .catch(function (error) {
          console.log(error);
        });
        /*Fin envio de datos a la base de datos*/

      }//if(this.error.length)
    },
    capturarData: function(personalNatural){

      this.id=personalNatural.id;
      this.cedula=personalNatural.cedula;
      this.nombre=personalNatural.nombre.toLowerCase();
      this.apellido=personalNatural.apellido.toLowerCase();
      this.codigoCarnetPatria=personalNatural.codigo_carnet_patria;
      this.telefono=personalNatural.telefono;
      this.correoElectronico=personalNatural.correo_electronico;
      this.firmaPersonal=personalNatural.firma_personal;
      this.cedulaRequerido=false;
      this.nombreRequerido=false;
      this.apellidoRequerido=false;
      this.codigoCarnetPatriaRequerido=false;
      this.telefonoRequerido=false;
      this.correoElectronicoRequerido=false;
      this.firmaPersonalRequerido=false;

    },
    editar:function (){
      this.errors=[];
      this.cedulaRequerido=false;
      this.nombreRequerido=false;
      this.apellidoRequerido=false;
      this.codigoCarnetPatriaRequerido=false;
      this.telefonoRequerido=false;
      this.correoElectronicoRequerido=false;
      this.firmaPersonalRequerido=false;

      if (!this.cedula) {
        this.errors.push("Existen campos por llenar:<br>*Cédula.");
        this.cedulaRequerido=true;
      }else if (this.cedula.toString().length<7){
        this.errors.push("El campo cédula acepta un minimo de 7 caracteres y un máximo de 8 caracteres.");
        this.cedulaRequerido=true;
      }//else if (!this.data.cedula)

      if (!this.nombre) {
        this.errors.push("Existen campos por llenar:<br>*Nombre.");
        this.nombreRequerido=true;
      } else if (this.nombre.length<3 || this.nombre.length>50) {
        this.errors.push('El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
        this.nombreRequerido=true;
      }// if (!this.data.nombre)

      if (!this.apellido) {
        this.errors.push("Existen campos por llenar:<br>*Apellido.");
        this.apellidoRequerido=true;
      } else if (this.apellido.length<3 || this.apellido.length>50) {
        this.errors.push('El campo apellido acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
        this.apellidoRequerido=true;
      }// if (!this.data.apellido)

      if (!this.codigoCarnetPatria) {
        this.errors.push("Existen campos por llenar:<br>*Código carnet de la patria");
        this.codigoCarnetPatriaRequerido=true;
      } else if (this.codigoCarnetPatria.length<10 || this.codigoCarnetPatria.length>10) {
        this.errors.push('El campo código carnet de la patria debe contener 10 caracteres incluyendo los ceros.');
        this.codigoCarnetPatriaRequerido=true;
      }// if (!this.data.codigoCarnetPatria)


      if (!this.telefono) {
        this.errors.push("Existen campos por llenar:<br>*Teléfono");
        this.telefonoRequerido=true;
      } else if (this.telefono.length<11 || this.telefono.length>11) {
        this.errors.push('El campo teléfono debe contener 11 caracteres incluyendo el código de area.');
        this.telefonoRequerido=true;
      }// if (!this.data.codigoCarnetPatria)

      if (!this.correoElectronico) {
        this.errors.push("Existen campos por llenar:<br>*Correo electrónico");
        this.correoElectronicoRequerido=true;
      } else if (this.correoElectronico.length<10 || this.correoElectronico.length>50) {
        this.errors.push('El campo correo electrónico acepta un minimo de 10 caracteres y un máximo de 50 caracteres.');
        this.correoElectronicoRequerido=true;
      } else if (!this.validEmail(this.correoElectronico)) {
        this.errors.push('El campo correo electrónico es Invalido.');
        this.correoElectronicoRequerido=true;
      } //if (!this.data.correo)

      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
      }else{
        /*Envio de datos a la base de datos*/
        let self = this;
        let id=this.id;
        let cedula=this.cedula;
        let nombre=this.nombre.toLowerCase();
        let apellido=this.apellido.toLowerCase();
        let codigo_carnet_patria=this.codigoCarnetPatria;
        let telefono=this.telefono;
        let correo_electronico=this.correoElectronico.toLowerCase();
        let firma_personal=this.firmaPersonal;
        axios.post('{{ url("modificar_personal_natural") }}', {
          id:id,
          cedula:cedula,
          nombre: nombre,
          apellido: apellido,
          codigo_carnet_patria:codigo_carnet_patria,
          telefono:telefono,
          correo_electronico:correo_electronico,
          firma_personal:firma_personal,
        }).then(response => {
          if(response.data.status=="success"){
            alertify.set('notifier','position', 'top-right');
            alertify.success(response.data.mensaje);
            $('#myModal').modal('toggle');
            self.PersonalNatural=response.data.PersonalNatural;
          }//if(response.data.status=="success")
          if(response.data.status=="error"){
            alertify.set('notifier','position', 'top-right');
            $.each(response.data.mensaje, function( key, value ) {
              alertify.error(""+value);
            });
          }//if(response.data.status=="error")
        }).catch(error => {
          alertify.error(""+error);
        });
      }//if(this.errors.length>0)
    },
    datoBorrar:function(id){
      this.id=id;
    },
    verificarPersona:function(){

        axios.post("{{ url('/buscar_personal') }}", {'cedula': this.data.cedula}).then(response => {

          if(response.data.personal != null ){

            this.data.nombre = response.data.personal.nombre
            this.data.apellido = response.data.personal.apellido
            this.data.codigo_carnet_patria = response.data.personal.codigoCarnetPatria
            this.data.telefono = response.data.personal.telefono
            this.data.correoElectronico = response.data.personal.correo_electronico
            this.data.cargo = response.data.personal.cargo
            this.data.gerencia = response.data.personal.gerencia
            this.data.trabajador_vtelca = true

            if(this.data.correoElectronico.length <= 0){
              this.data.correoElectronico = "SININFORMACION@gmail.com"
            }

            $(".input-disabled").attr('readonly', true);

          }else{

            this.data.nombre = ''
            this.data.apellido = ''
            this.data.codigo_carnet_patria = ''
            this.data.telefono = ''
            this.data.correoElectronico = ''
            this.data.cargo = ''
            this.data.gerencia = ''
            this.data.trabajador_vtelca = false

            $(".input-disabled").attr('readonly', false);


          }

        })

    },
    eliminar: function(){
      let self = this;
      axios.post('{{ url("eliminar_personal_natural") }}', {id:this.id}).then(response => {
        if(response.data.status=="success"){
          self.limpiar();
          alertify.set('notifier','position', 'top-right');
          alertify.success(response.data.mensaje);
          $('#myModalEliminar').modal('toggle');
          self.PersonalNatural=response.data.PersonalNatural;
        }//if(response.data.status=="success")
        if(response.data.status=="error"){
          self.limpiar();
          $('#myModalEliminar').modal('toggle');
          alertify.set('notifier','position', 'top-right');
          $.each(response.data.mensaje, function( key, value ) {
            alertify.error(""+value);
          });
        }//if(response.data.status=="error")
      }).catch(error => {
        console.log(error);
      });
    },
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.PersonalNatural.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    }

  },//methods
});//const app= new Vue
</script>
@endpush
