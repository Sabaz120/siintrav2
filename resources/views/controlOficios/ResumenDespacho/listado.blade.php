@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">RESUMEN DE DESPACHO</h5>
      <div class="row justify-content-center">
        <div class="col-12 table-responsive" v-if="Oficios.length>0" >
          <div class="row">
            <div class="col-12 col-md-9 col-lg-9">
              <br>
              <div class="mt-2 form-inline font-weight-bold">
                Mostrar
                <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                  <option v-for="option in optionspageSize" v-bind:value="option.value">
                    @{{ option.text }}
                  </option>
                </select>
                registros
              </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <label for="Buscar" class="font-weight-bold">Buscar:</label>
                <input id="search" class="form-control form-control-sm" type="text" v-model="search" v-on:keyup="filtrar" required>
              </div>
            </div>
          </div>
          <table  class="table table-bordered text-center" >
            <thead class="bg-primary text-white">
              <tr>
                <td>N° Requesición</td>
                <td>Ente / Cliente</td>
                <td>Nombre del Chofer</td>
                <td>Cédula</td>
                <td>Vehículo</td>
                <td>Placa del Vehículo</td>
                <td>Fecha de Salida</td>
                <td>Acción</td>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(oficio, index) in oficios_registrados" v-show="statusFiltro==1">
                <td>@{{oficio.oferta_economica_id}}</td>
                <td class="text-uppercase">@{{oficio.solicitante_ente}}</td>
                <td v-if="oficio.despacho!=null">@{{oficio.despacho.chofer_nombre}}</td>
                <td v-else>En espera</td>
                <td v-if="oficio.despacho!=null">@{{oficio.despacho.chofer_cedula}}</td>
                <td v-else>En espera</td>
                <td v-if="oficio.despacho!=null">@{{oficio.despacho.vehiculo_modelo}}</td>
                <td v-else>En espera</td>
                <td v-if="oficio.despacho!=null">@{{oficio.despacho.vehiculo_placa}}</td>
                <td v-else>En espera</td>
                <td v-if="oficio.despacho!=null && oficio.despacho.fecha_despacho!=null">@{{oficio.despacho.fecha_despacho}}</td>
                <td v-else>En espera</td>
                <td><button type="button" class="btn btn-info text-white text-center"data-toggle="modal" data-target="#detalle" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Visualizar" @click="obtenerDetalle(oficio.oferta_economica_id)"><i class="fas fa-search fa-1x" aria-hidden="true"></i></button></td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="10">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Oficios.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 text-center jumbotron" v-else>
            <h5>Sin despachos que mostrar</h5>
        </div>
      </div>
    </div>
  </div>
<!-- Modal Detalle-->
<div class="modal fade" id="detalle" tabindex="-1" role="dialog" aria-labelledby="detalle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center" style="display: block;">
        <h5 class="modal-title text-center" >Productos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table  class="table table-bordered text-center" >
            <thead class="bg-primary text-white">
              <tr class="text-center">
                <td>Modelo</td>
                <td>Modalidad</td>
                <td>Cantidad</td>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(producto, index) in detalle">
                <td>@{{producto.producto.nombre}}</td>
                <td>@{{producto.modalidad}}</td>
                <td>@{{producto.cantidad}}</td>
              </tr>
            </tbody>
       </table>      
      </div>
    </div>
  </div>
</div>
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    Oficios:[],
    OficiosTemp:[],
    detalle:'',
    // Paginación
    search:'',
    currentSort:'solicitante_ente',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'25',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0,
    // Paginación
  },
  computed:{
    oficios_registrados:function() {
      this.rows=0;
      return this.Oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    // Paginación
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        // console.log('entre');
        // console.log(this.search);
        for(let i in this.Oficios){
          if(this.Oficios[i].solicitante_ente.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.Oficios[i]);
          }//if(this.NivelAtencion[i].nivel.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.NivelAtencion)
        // console.log(filtrardo);
        if(filtrardo.length>0){
          this.Oficios=filtrardo;
          this.statusFiltro=1;
        }
        else{
          this.statusFiltro=0;
          this.Oficios=this.OficiosTemp;
        }// if(filtrardo.length)
      }else{
        // console.log('Search vacio');
        this.statusFiltro=1;
        this.Oficios=this.OficiosTemp;
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.Oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      // console.log(s);
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
    // Paginación
    obtenerResumenDespachos(){
      axios.get('{{ route("api.resumen.despacho") }}', {

      }).then(response => {
        if(response.data.error==0){
          this.Oficios=response.data.oficios;
          this.OficiosTemp=response.data.oficios;
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        // this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    obtenerDetalle(id){
      for(let i in this.Oficios){
        if(this.Oficios[i].oferta_economica_id==id){
          this.detalle=this.Oficios[i]['productos'];
          break;
        }//if(this.Oficios[i].oferta_economica_id==id)
      }//for(let i in this.Oficios)
    },
  },//methods
  mounted(){
    this.obtenerResumenDespachos();
  }
});//const app= new Vue

</script>
@endpush
