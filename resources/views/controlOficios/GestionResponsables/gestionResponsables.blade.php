@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">REGISTRO DE RESPONSABLE</h5>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Empresa:</strong>
            <select class="form-control text-uppercase" v-model="empresa_id">
              <option value="0">Seleccione una empresa</option>
              <option v-for="empresa in Empresas" :value="empresa.id" class="text-uppercase">@{{empresa.nombre_institucion}}</option>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Departamento:</strong>
            <select class="form-control text-uppercase" v-model="departamento_id">
              <option value="0">Seleccione una empresa</option>
              <option v-for="departamento in Departamentos" :value="departamento.id" class="text-uppercase">@{{departamento.nombre}}</option>
            </select>
          </div>
        </div>
        <div class="col-md-4 text-center">
          <strong><i class="fas fa-asterisk"></i> Cédula:</strong>
          <div class="form-group input-group mb-4">
            <input type="text" name="" v-model="cedula" v-mask="'########'" class="form-control text-uppercase" value="">
            <div class="input-group-append">
              <button class="btn btn-info" @click="buscarPersonaNatural()" type="button"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
        <div v-show="pn_cedula!=''" class="col-md-4 form-group text-center">
          <strong> Nombres y Apellidos:</strong>
          <input type="text" readonly class="form-control text-uppercase" v-model="pn_nombres" name="" value="">
        </div>
        <div v-show="pn_cedula!=''" class="col-md-4 form-group text-center">
          <strong>Código Carnet de la Patria:</strong>
          <input type="text" readonly class="form-control text-uppercase" v-model="pn_cod_carnet" name="" value="">
        </div>
        <div v-show="pn_cedula!=''" class="col-md-4 form-group text-center">
          <strong>Teléfono de Contacto:</strong>
          <input type="text" readonly class="form-control text-uppercase" v-model="pn_telefono" name="" value="">
        </div>
        <div v-show="pn_cedula!=''" class="col-md-4 form-group text-center">
          <strong> Correo Electrónico:</strong>
          <input type="text" readonly class="form-control text-uppercase" v-model="pn_correo" name="" value="">
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
        <br>
        <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
        <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show="Responsables.length>0">
          <div class="row">
            <div class="header mx-auto">
              <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE EMPRESAS</h4>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group text-center">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm text-uppercase"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <table  class="table table-bordered text-center">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th scope="col" class="text-center text-uppercase" @click="sort('nombre')">Empresa</th>
                      <th scope="col" class="text-center text-uppercase" @click="sort('descripcion')">Departamento</th>
                      <th scope="col" class="text-center text-uppercase" @click="sort('descripcion')">Responsable</th>
                      <th scope="col" >Acción</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr v-for="(responsable,index) in responsables_registrados" v-show='statusFiltro' >
                      <td class="text-uppercase">@{{responsable.empresa}}</td>
                      <td class="text-uppercase">@{{responsable.departamento}}</td>
                      <td class="text-uppercase">@{{responsable.responsable}}</td>
                      <td class="text-center align-middle">
                        <button type="button" class="btn btn-info text-white text-center" data-toggle="modal" data-target="#myModalDetalle" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Detalle" @click="capturarResponsable(responsable.id)"><i class="fas fa-eye fa-1x" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarResponsable(responsable.id)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                        <!-- <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="capturarResponsable(index)"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button> -->
                      </td>
                    </tr>
                    <tr v-show='!statusFiltro'>
                      <td class="text-center font-weight-bold" colspan="4">No se encontraron registros</td>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Responsables.length}}</strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="myModalDetalle">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">DETALLE DE RESPONSABLE</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-md-2 col-lg-4">
            <div class="form-group text-center">
              <strong>Empresa:</strong>
              <input type="text" readonly v-model="empresa" class="form-control text-center text-uppercase" value="">
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <strong>Departamento:</strong>
              <input type="text" readonly v-model="departamento" class="form-control text-center text-uppercase" value="">
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <strong>Cédula:</strong>
              <input type="text" readonly v-model="cedula_edit" class="form-control text-center text-uppercase" value="">
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <strong>Nombres y Apellidos:</strong>
              <input type="text" readonly class="form-control text-center text-uppercase" v-model="pn_nombres_edit" name="" value="">
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <strong>Código Carnet de la Patria:</strong>
              <input type="text" readonly class="form-control text-center text-uppercase" v-model="pn_cod_carnet_edit" name="" value="">
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <strong>Teléfono de Contacto:</strong>
              <input type="text" readonly class="form-control text-center text-uppercase" v-model="pn_telefono_edit">
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
              <strong>Correo Electrónico:</strong>
              <input type="text" readonly class="form-control text-center text-uppercase" v-model="pn_correo_edit" name="" value="">
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">EDICIÓN DE RESPONSABLE</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <strong>Empresa:</strong>
              <select class="form-control text-uppercase" v-model="empresa_id_edit">
                <option value="0">Seleccione una empresa</option>
                <option v-for="empresa in Empresas" :value="empresa.id" class="text-uppercase">@{{empresa.nombre_institucion}}</option>
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group text-center">
              <i class="fas fa-asterisk"></i>
              <strong>Departamento:</strong>
              <select class="form-control text-uppercase" v-model="departamento_id_edit">
                <option value="0">Seleccione una empresa</option>
                <option v-for="departamento in Departamentos" :value="departamento.id" class="text-uppercase">@{{departamento.nombre}}</option>
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group text-center">
              <strong><i class="fas fa-asterisk"></i> Cédula:</strong>
              <div class="input-group mb-4">
                <input type="text" name="" v-model="cedula_edit" v-mask="'########'" class="form-control text-uppercase" value="">
                <div class="input-group-append">
                  <button class="btn btn-info" @click="buscarPersonaNaturalEdit()" type="button"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>
          <div v-show="pn_cedula_edit!=''" class="col-12 col-md-4 col-lg-4 text-center">
            <strong> Nombres y Apellidos:</strong>
            <input type="text" readonly class="form-control text-uppercase" v-model="pn_nombres_edit" name="" value="">
          </div>
          <div v-show="pn_cedula_edit!=''" class="col-12 col-md-4 col-lg-4 text-center text-uppercase">
            <strong>Código Carnet de la Patria:</strong>
            <input type="text" readonly class="form-control text-uppercase" v-model="pn_cod_carnet_edit" name="" value="">
          </div>
          <div v-show="pn_cedula_edit!=''" class="col-12 col-md-4 col-lg-4 text-center">
            <strong>Teléfono de Contacto:</strong>
            <input type="text" readonly class="form-control text-uppercase" v-model="pn_telefono_edit" name="" value="">
          </div>
          <div v-show="pn_cedula_edit!=''" class="col-12 col-md-4 col-lg-4 text-center">
            <strong> Correo Electrónico:</strong>
            <input type="text" readonly class="form-control text-uppercase" v-model="pn_correo_edit" name="" value="">
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success text-white font-weight-bold" @click="actualizar()">Actualizar</button>
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- Eliminar -->
<div class="modal" id="myModalEliminar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title mx-auto  text-white">
          ELIMINAR RESPONSABLE</h4>
          <!-- <button type="button" class="close" data-dismiss="modal" >&times;</button> -->
        </div>
        <div class="modal-body">
          <div class="mx-auto text-center">
            ¿Esta seguro de eliminar este responsable?
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminarResponsable">Sí, estoy seguro.</button>
          <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    Responsables:{!! $Responsables ? $Responsables : "''"!!},
    Departamentos:{!! $Departamentos ? $Departamentos : "''"!!},
    Empresas:{!! $Empresas ? $Empresas : "''"!!},
    empresa_id:0,
    departamento_id:0,
    cedula:'',
    pn_nombres:'',
    pn_cedula:'',
    pn_cod_carnet:'',
    pn_telefono:'',
    pn_correo:'',
    pn_nombres_edit:'',
    pn_cedula_edit:'',
    pn_cod_carnet_edit:'',
    pn_telefono_edit:'',
    pn_correo_edit:'',
    departamento:'',
    empresa:'',
    search:'',
    indexResponsable:0,
    departamento_id_edit:0,
    empresa_id_edit:0,
    cedula_edit:0,
    ///////////////////////PAGINADOR
    currentSort:'nombre',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0
    //////////////////////Paginador
  },
  computed:{
    responsables_registrados:function() {
      this.rows=0;
      return this.Responsables.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    limpiar(){
      this.pn_nombres='';
      this.pn_cod_carnet='';
      this.pn_telefono='';
      this.pn_correo='';
      this.pn_cedula='';
      this.pn_nombres_edit='';
      this.pn_cod_carnet_edit='';
      this.pn_telefono_edit='';
      this.pn_correo_edit='';
      this.pn_cedula_edit='';
      this.cedula='';
      this.empresa_id=0;
      this.departamento_id=0;
      this.indexResponsable=0;
      this.departamento_id_edit=0,
      this.empresa_id_edit=0,
      this.cedula_edit=0,
      this.departamento='';
      this.empresa='';
    },
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.Responsables){
          if(this.Responsables[i].empresa.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Responsables[i].departamento.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Responsables[i].responsable.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
            this.statusFiltro=1;
            filtrardo.push(this.Responsables[i]);
          }//if(this.Materiales[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.MotivosSolicitud)
        if(filtrardo.length)
        this.Responsables=filtrardo;
        else{
          this.statusFiltro=0;
          this.Responsables={!! $Responsables ? $Responsables : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.Responsables={!! $Responsables ? $Responsables : "''"!!};
      }//if(this.search$materiales
    },// filtrar:function()
    capturarResponsable(id){
      var index=0;
      for(var i=0;i<this.Responsables.length;i++){
        if(this.Responsables[i].id==id){
          index=i;
          break;
        }//if
      }//for
      this.indexResponsable=index;
      this.empresa_id_edit=this.Responsables[index].personal_juridico_id;
      this.departamento_id_edit=this.Responsables[index].departamento_id;
      this.cedula_edit=this.Responsables[index].cedula;
      for(var i=0;i<this.Empresas.length;i++){
        if(this.Empresas[i].id==this.empresa_id_edit){
          this.empresa=this.Empresas[i].nombre_institucion;
          break;
        }
      }//for empresas
      for(var i=0;i<this.Departamentos.length;i++){
        if(this.Departamentos[i].id==this.departamento_id_edit){
          this.departamento=this.Departamentos[i].nombre;
          break;
        }
      }//for empresas
      this.buscarPersonaNaturalEdit();
    },
    buscarPersonaNatural(){
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:this.cedula}).then(response => {
        if(response.data.error==0){
          this.pn_nombres=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
          this.pn_cod_carnet=response.data.persona_natural.codigo_carnet_patria;
          this.pn_telefono=response.data.persona_natural.telefono;
          this.pn_correo=response.data.persona_natural.correo_electronico;
          this.pn_cedula=this.cedula;
        }else if(response.data.error==1){
          this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    buscarPersonaNaturalEdit(){
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:this.cedula_edit}).then(response => {
        if(response.data.error==0){
          this.pn_nombres_edit=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
          this.pn_cod_carnet_edit=response.data.persona_natural.codigo_carnet_patria;
          this.pn_telefono_edit=response.data.persona_natural.telefono;
          this.pn_correo_edit=response.data.persona_natural.correo_electronico;
          this.pn_cedula_edit=this.cedula_edit;
        }else if(response.data.error==1){
          this.limpiar();
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    registrar() {
      if(this.empresa_id==0){
        alertify.error('Debes seleccionar una empresa');
      }else if(this.departamento_id==0){
        alertify.error('Debes seleccionar un departamento');
      }else if(this.pn_cedula==""){
        alertify.error('Debes ingresar una cédula válida');
      }else{
        axios.post('{{ route("AsignacionResponsable") }}',
        {
          departamento_id:this.departamento_id,
          empresa_id:this.empresa_id,
          cedula:this.pn_cedula
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Responsables=response.data.Responsables;
            this.limpiar();
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }else if(response.data.error==500){
            alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
            console.log(response.data.msg);
            this.limpiar();
          }else{
            this.limpiar();
            console.log('Error sin codigo:'+response.data.msg);
          }
        }).catch(error => {
          // this.limpiar();
          alertify.error('Error en el servidor.');
          console.log('Axios api datos persona natural catch');
          console.log(error);
        });
      }
    },
    actualizar() {
      if(this.empresa_id_edit==0){
        alertify.error('Debes seleccionar una empresa');
      }else if(this.departamento_id_edit==0){
        alertify.error('Debes seleccionar un departamento');
      }else if(this.pn_cedula_edit==""){
        alertify.error('Debes ingresar una cédula válida');
      }else{
        axios.post('{{ route("ActualizarResponsable") }}',
        {
          departamento_id:this.departamento_id_edit,
          empresa_id:this.empresa_id_edit,
          cedula:this.pn_cedula_edit,
          id:this.Responsables[this.indexResponsable].id
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Responsables=response.data.Responsables;
            this.limpiar();
            $('#myModal').modal('toggle');
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }else if(response.data.error==500){
            alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
            console.log(response.data.msg);
            this.limpiar();
          }else{
            this.limpiar();
            console.log('Error sin codigo:'+response.data.msg);
          }
        }).catch(error => {
          // this.limpiar();
          alertify.error('Error en el servidor.');
          console.log('Axios api datos persona natural catch');
          console.log(error);
        });
      }
    },
    eliminarResponsable(){
      axios.post('{{ route("borrarResponsables") }}',
      {
        id:this.Responsables[this.indexResponsable].id
      })
      .then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          this.Responsables=response.data.Responsables;
          this.limpiar();
          $('#myModalEliminar').modal('toggle');
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        // this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },//eliminarMaterial
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.Responsables.length) this.currentPage++;

    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;

    },
    sort:function(s) {
      //if s == current sort, reverse
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    }
  },//methods
});//const app= new Vue
</script>
@endpush
