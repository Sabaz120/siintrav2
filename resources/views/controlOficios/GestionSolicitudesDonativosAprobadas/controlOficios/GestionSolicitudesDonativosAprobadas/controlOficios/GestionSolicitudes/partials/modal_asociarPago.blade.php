<div class="modal" id="asociarPago" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">ASOCIACIÓN DE PAGO</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3 text-center">
            <h4 style="color:blue;">TOTAL OFERTA ECONÓMICA</h4>
            <br>
            <h4>@{{calcular_total}} Bs.S</h4>
            <br>
            <h4 style="color:blue;">TOTAL CANCELADO</h4>
            <br>
            <h4>@{{calcular_cancelado}} Bs.S</h4>
            <br>
            <h4 style="color:blue;">SALDO DEUDOR</h4>
            <br>
            <h4>@{{calcular_restante}} Bs.S</h4>
            <br>
          </div>
          <div class="col-md-9 text-center" v-if="pago_index==null">
            <h3>DETALLE DEL PAGO</h3>
            <strong>
              <i class="fa fa-asterisk"></i>
              Tipo de Pago:
            </strong>
            <select class="form-control" v-model="tipo_pago">
              <option value="0">Seleccione el tipo de pago</option>
              <option value="transferencia">Transferencia</option>
              <option value="punto_venta">Punto de Venta</option>
            </select>
            <strong>
              <i class="fa fa-asterisk"></i>
              Referencia de Pago:
            </strong>
            <input type="text" @keypress="onlyNumberWithOutDecimalPoint" :maxlength="12" v-model="referencia_pago" class="form-control" >
            <strong>
              <i class="fa fa-asterisk"></i>
              Monto:
            </strong>
            <input type="text" @keypress="onlyNumber" v-model="monto_pago" :maxlength="15" class="form-control" >
            <strong v-show="tipo_pago=='transferencia'">
              <i class="fa fa-asterisk"></i>
              Soporte de Pago:
            </strong>
            <strong v-show="tipo_pago=='punto_venta'">
              <i class="fa fa-asterisk"></i>
              Número de Lote:
            </strong>
            <input v-if="tipo_pago=='transferencia'" type="file" @change="convertirBase64SoportePago" accept="image/*" class="form-control" >
            <input v-else-if="tipo_pago=='punto_venta'" type="text" @keypress="onlyNumber" :maxlength="10" v-model="soporte_pago" class="form-control" >
            <!-- BOTONES -->
            <div class="mx-auto">
              <br>
              <button type="button" name="button" class="btn btn-success" @click="asociarPago()">Registrar</button>
              <!-- <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal" @click="limpiar()">Cancelar</button> -->
            </div>
            <!-- BOTONES -->
          </div>
          <div class="col-md-9 text-center" v-else>
            <h3>DETALLE DEL PAGO</h3>
            <strong>
              <i class="fa fa-asterisk"></i>
              Tipo de Pago:
            </strong>
            <select class="form-control" v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago">
              <option value="0">Seleccione el tipo de pago</option>
              <option value="transferencia">Transferencia</option>
              <option value="punto_venta">Punto de Venta</option>
            </select>
            <strong>
              <i class="fa fa-asterisk"></i>
              Referencia de Pago:
            </strong>
            <input type="text" @keypress="onlyNumber" :maxlength="20" v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].referencia" class="form-control" >
            <strong>
              <i class="fa fa-asterisk"></i>
              Monto:
            </strong>
            <input type="text" @keypress="onlyNumber" v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].monto" :maxlength="15" class="form-control" >
            <strong v-show="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='transferencia'">
              <i class="fa fa-asterisk"></i>
              Soporte de Pago:
            </strong>
            <strong v-show="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='punto_venta'">
              <i class="fa fa-asterisk"></i>
              Número de lote:
            </strong>
            <input v-show="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='punto_venta'" type="text" v-model="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" @keypress="onlyNumber" :maxlength="8" class="form-control" >
            <input v-show="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='transferencia'" type="file" @change="convertirBase64SoportePagoEditar" accept="image/*" class="form-control" >
            <img v-if="Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='transferencia' && Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote!=null" :src="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" class="img-fluid img-responsive">
            <!-- BOTONES -->
            <div class="mx-auto">
              <br>
              <button type="button" name="button" class="btn btn-success" @click="actualizarPago()">Actualizar</button>
              <button type="button" class="btn btn-danger font-weight-bold" @click="limpiar()">Cancelar</button>
            </div>
            <!-- BOTONES -->
          </div>
        </div>
        <!-- LISTADO PAGOS -->
        <div class="row table-responsive">
          <table class="table table-bordered text-center">
            <thead class="bg-primary text-white">
              <th>N° de Pago</th>
              <th>Referencia</th>
              <th>Monto</th>
              <th>Validado</th>
              <th>Acción</th>
            </thead>
            <tbody>
              <tr v-for="(pago,index) in this.Oficios[indexOficio].oferta_economica_pagos">
                <td>@{{index+1}}</td>
                <td>@{{pago.referencia}}</td>
                <td>@{{pago.monto}}</td>
                <td v-if="pago.validacion">
                  <i title="Pago confirmado" class="bg-success text-white fa fa-check"></i>
                </td>
                <td v-else>
                  <i title="Pago sin confirmar" class="bg-danger text-white fa fa-clock-o"></i>
                </td>
                <td class="text-center">
                  <button type="button" title="Editar pago" @click="pago_index=index" class="btn btn-info" name="button"> <i class="fa fa-pencil-square-o"></i></button>
                  <button type="button" @click="eliminarPago(pago.id)" class="btn btn-danger" name="button"> <i class="fa fa-trash"></i> </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- LISTADO PAGOS -->
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">

      </div>
    </div>
  </div>
</div>
