@extends('layouts.pdf.pdf')
@section('contenido')
<style media="screen">
  h2,h3,h4,h5{
    text-align:center;
  }
  th{
    height: 50px;
    background-color: #dee2e6;
  }
  td{
    height: 45px;
  }
  .header{
 width: 100%;
 text-align: center;
 position: fixed;
}
</style>
 <h2 class="text-center">{{$titulo}}</h2>
 <table style="width:100%">
 <thead>
  <tr>
    <th>Cédula</th>
    <th>Responsable</th>
    <th>Cédula Chofer</th>
    <th>Chofer</th>
    <th>Vehículo</th>
    <th>Placa</th>
    <th>Material</th>
    <th>Cantidad</th>
    <th>Estatus</th>
  </tr>
  </thead>
  @foreach($oficio as $campo)
  <tbody>
  <tr>
    <td>{{$campo['cedulaResponsable']}}</td>
    <td>{{$campo['nombresResponsableDespacho']}}</td>
    <td>{{$campo['cedulaChofer']}}</td>
    <td>{{$campo['nombresChofer']}}</td>
    <td>{{$campo['modelo']}}</td>
    <td>{{$campo['placa']}}</td>
    <td>{{$campo['nombres_materiales']}}</td>
    <td>{{$campo['materiales_cantidad']}}</td>
    <td>{{$campo['nombre_estado']}}</td>
  </tr>
  </tbody>
  @endforeach
</table>
@endsection
