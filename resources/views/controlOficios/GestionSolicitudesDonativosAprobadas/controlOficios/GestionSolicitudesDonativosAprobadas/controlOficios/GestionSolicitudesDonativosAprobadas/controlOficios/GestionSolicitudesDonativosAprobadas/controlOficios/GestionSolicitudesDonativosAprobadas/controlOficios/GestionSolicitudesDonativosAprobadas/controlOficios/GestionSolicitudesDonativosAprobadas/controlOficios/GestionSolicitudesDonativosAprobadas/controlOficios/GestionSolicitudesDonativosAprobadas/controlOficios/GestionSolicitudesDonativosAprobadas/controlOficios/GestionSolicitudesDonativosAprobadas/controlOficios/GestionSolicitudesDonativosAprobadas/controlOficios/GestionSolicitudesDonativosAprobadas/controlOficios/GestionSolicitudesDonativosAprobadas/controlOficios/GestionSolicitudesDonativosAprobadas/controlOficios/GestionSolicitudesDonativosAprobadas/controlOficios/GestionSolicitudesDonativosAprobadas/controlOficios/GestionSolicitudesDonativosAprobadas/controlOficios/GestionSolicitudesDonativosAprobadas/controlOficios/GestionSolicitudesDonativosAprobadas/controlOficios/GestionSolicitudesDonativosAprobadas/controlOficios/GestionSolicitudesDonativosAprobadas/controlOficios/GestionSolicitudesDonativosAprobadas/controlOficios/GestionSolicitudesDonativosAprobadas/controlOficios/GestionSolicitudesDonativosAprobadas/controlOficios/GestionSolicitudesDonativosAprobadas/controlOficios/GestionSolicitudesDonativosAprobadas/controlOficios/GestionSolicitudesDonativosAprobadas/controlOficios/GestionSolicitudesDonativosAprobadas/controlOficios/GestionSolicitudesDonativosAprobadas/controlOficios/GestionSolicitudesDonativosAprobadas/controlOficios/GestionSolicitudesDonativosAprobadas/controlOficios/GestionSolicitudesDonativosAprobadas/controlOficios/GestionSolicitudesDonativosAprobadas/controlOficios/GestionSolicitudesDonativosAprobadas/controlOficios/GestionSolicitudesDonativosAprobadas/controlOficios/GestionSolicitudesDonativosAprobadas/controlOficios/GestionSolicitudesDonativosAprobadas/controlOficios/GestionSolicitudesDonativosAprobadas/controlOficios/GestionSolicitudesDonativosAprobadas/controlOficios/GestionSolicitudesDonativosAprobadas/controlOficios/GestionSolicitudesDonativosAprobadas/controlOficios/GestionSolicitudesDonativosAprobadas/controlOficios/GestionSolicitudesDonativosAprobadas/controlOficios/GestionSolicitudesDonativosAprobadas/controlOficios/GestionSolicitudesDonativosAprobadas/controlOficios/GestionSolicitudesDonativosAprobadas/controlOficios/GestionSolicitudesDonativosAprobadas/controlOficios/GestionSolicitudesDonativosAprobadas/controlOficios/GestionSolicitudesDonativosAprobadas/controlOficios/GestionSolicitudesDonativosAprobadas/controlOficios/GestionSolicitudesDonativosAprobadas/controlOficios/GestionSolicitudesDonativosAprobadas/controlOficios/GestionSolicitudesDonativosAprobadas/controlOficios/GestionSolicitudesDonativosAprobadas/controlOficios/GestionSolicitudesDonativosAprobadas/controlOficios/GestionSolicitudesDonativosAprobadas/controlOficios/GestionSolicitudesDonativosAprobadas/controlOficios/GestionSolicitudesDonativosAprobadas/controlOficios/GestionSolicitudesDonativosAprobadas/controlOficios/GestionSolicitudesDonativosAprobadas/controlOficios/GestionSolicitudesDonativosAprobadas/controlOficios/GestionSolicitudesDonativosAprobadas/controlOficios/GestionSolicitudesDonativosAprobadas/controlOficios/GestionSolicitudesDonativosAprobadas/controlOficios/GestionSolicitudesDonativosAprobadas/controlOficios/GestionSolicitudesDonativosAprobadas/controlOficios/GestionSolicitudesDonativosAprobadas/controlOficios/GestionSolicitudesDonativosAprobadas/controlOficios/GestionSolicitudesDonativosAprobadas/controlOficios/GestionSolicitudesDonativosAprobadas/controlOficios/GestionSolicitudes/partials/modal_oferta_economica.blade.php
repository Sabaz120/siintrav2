<div class="modal" id="myModal" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">PREVISUALIZAR OFERTA ECONÓMICA</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="container-fluid table-responsive">
            <table  class="table table-bordered text-center">
              <thead class="bg-primary text-white">
                <tr>
                  <th scope="col" class="text-center" >Descripción</th>
                  <th scope="col" class="text-center" >Modelo</th>
                  <th scope="col" class="text-center" >Cantidad</th>
                  <th scope="col" class="text-center" >Costo Unitario</th>
                  <th scope="col" class="text-center" >Costo Total</th>
                  <th scope="col" class="text-center" v-if="Oficios[indexOficio].oferta_economica!=null">Acción</th>
                </tr>
              </thead>
              <tbody >
                <tr v-for="(producto,index) in Oficios[indexOficio].productos">
                  <td class="text-center text-capitalize">@{{producto.descripcion}}</td>
                  <td class="text-center text-capitalize">@{{producto.modelo}}</td>
                  <td class="text-center text-capitalize"><input type="number" class="form-control" v-bind:max="producto.cantidad_max" v-model="producto.cantidad"></td>
                  <td class="text-center text-capitalize">
                    <span v-if="'precio_desactualizado' in producto && producto.precio_desactualizado==1" class="badge badge-danger">Precio desactualizado,<br>selecciona el nivel de precio y actualiza.</span>
                    <select class="form-control" v-model="producto.nivel_precio" v-on:click="cambiarPrecio(index)">
                      <option value="0">Seleccione Precio</option>
                      <option value="1">P1-@{{producto.precios.precio1}}</option>
                      <option value="2">P2-@{{producto.precios.precio2}}</option>
                      <option value="3">P3-@{{producto.precios.precio3}}</option>
                    </select>
                  </td>
                  <td class="text-center text-capitalize">Bs.S @{{parseFloat(producto.precio*producto.cantidad).toFixed(2)}}</td>
                  <td class="text-center align-middle" v-if="Oficios[indexOficio].oferta_economica!=null">
                      <button type="button" class="btn btn-success text-white text-center" title="Actualizar" @click="guardarCambiosProducto(index)"><i class="fa fa-save fa-1x" aria-hidden="true"></i></button>
                  </td>
                </tr>
                <tr>
                  <td colspan="5" v-if="Oficios[indexOficio].oferta_economica!=null" class="text-right"><strong>Sub-Total</strong></td>
                  <td colspan="4" v-else class="text-right"><strong>Sub-Total</strong></td>
                  <td class="text-center"><strong>@{{calcular_subtotal}} Bs.S</strong></td>
                </tr>
                <tr>
                  <td colspan="5" v-if="Oficios[indexOficio].oferta_economica!=null" class="text-right"><strong>I.V.A</strong></td>
                  <td colspan="4" v-else class="text-right"><strong>I.V.A</strong></td>
                  <td class="text-center"><strong>@{{iva}} %</strong></td>
                </tr>
                <tr>
                  <td colspan="5" v-if="Oficios[indexOficio].oferta_economica!=null" class="text-right"><strong>Total</strong></td>
                  <td colspan="4" v-else class="text-right"><strong>Total</strong></td>
                  <td class="text-center"><strong>@{{calcular_total}} BS.S</strong></td>
                </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto">
        <button type="button" v-if="Oficios[indexOficio].oferta_economica==null" v-show="!generandoOferta" class="btn btn-success text-white font-weight-bold" @click="GenerarOfertaEconomica()">Guardar</button>
        <button type="button" v-if="Oficios[indexOficio].oferta_economica==null" class="btn btn-success text-white font-weight-bold rounded-0 border-r-g" v-show="generandoOferta">
          <i class="fa fa-spinner fa-pulse"></i> Generando Oferta Económica..
        </button>
        <!-- <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button> -->
      </div>
    </div>
  </div>
</div>
