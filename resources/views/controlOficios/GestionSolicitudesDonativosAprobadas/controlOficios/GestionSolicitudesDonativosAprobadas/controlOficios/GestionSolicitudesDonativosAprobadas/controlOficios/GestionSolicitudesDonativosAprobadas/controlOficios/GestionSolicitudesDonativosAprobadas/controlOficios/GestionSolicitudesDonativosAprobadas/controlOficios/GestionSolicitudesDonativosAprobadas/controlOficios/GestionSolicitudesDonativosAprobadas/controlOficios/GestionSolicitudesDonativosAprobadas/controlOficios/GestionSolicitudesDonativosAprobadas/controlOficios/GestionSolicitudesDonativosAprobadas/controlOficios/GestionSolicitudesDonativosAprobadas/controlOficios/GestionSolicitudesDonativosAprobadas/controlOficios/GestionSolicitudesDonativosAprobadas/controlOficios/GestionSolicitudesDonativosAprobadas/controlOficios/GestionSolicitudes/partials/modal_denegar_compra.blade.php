<div class="modal" id="denegarCompra" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">DATOS DE CUENTA A REINTEGRAR</h4>
        <button type="button" title="Cerrar" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body row justify-content-center" v-if="reintegro_reintegrar==1">
        <div class="col-md-12 bg-secondary text-center text-white" >
          <p>Este oficio contiene pagos validados, por lo tanto es necesario re-integrarle su dinero.</p>
        </div>
        <div class="col-md-3 text-center">
          <i class="fa fa-asterisk"></i>
          <strong>Tipo de solicitante:</strong>
          <input type="text" style="text-align:center;" readonly v-if="Oficios[indexOficio].personal_juridico==null" value="Personal Natural" class="form-control">
          <input type="text" style="text-align:center;" readonly v-else value="Personal Jurídico" class="form-control">
        </div>
        <div class="col-md-3 text-center" v-if="Oficios[indexOficio].personal_juridico==null">
          <i class="fa fa-asterisk"></i>
          <strong>Cédula:</strong>
          <input type="text" class="form-control" v-model="reintegro_cedula" @keypress="onlyNumber" maxlength="8">
        </div>
        <div class="col-md-3 text-center" v-else>
          <i class="fa fa-asterisk"></i>
          <strong>R.I.F:</strong>
          <input type="text" class="form-control" v-model="reintegro_rif" v-mask="'A-#########'" maxlength="8">
        </div>
        <div class="col-md-3 text-center">
          <i class="fa fa-asterisk"></i>
          <strong>Banco:</strong>
          <input type="text" class="form-control" v-model="reintegro_banco" maxlength="20">
        </div>
        <div class="col-md-3 text-center">
          <i class="fa fa-asterisk"></i>
          <strong>Tipo de cuenta:</strong>
          <select class="form-control" v-model="reintegro_tipo_cuenta">
            <option value="1">Corriente</option>
            <option value="2">Ahorro</option>
          </select>
        </div>
        <div class="col-md-4 text-center">
          <i class="fa fa-asterisk"></i>
          <strong>Número de cuenta:</strong>
          <input type="text" class="form-control" v-model="reintegro_num_cuenta" v-mask="'####-####-##-##-########'">
        </div>
        <div class="col-md-4 text-center">
          <i class="fa fa-asterisk"></i>
          <strong>Nombre:</strong>
          <input type="text" class="form-control" v-model="reintegro_nombre" maxlength="40">
        </div>
        <div class="col-md-4 text-center">
          <i class="fa fa-asterisk"></i>
          <strong>Correo electrónico:</strong>
          <input type="email" class="form-control" v-model="reintegro_correo" maxlength="40">
        </div>
      </div>
      <div class="modal-body row justify-content-center" v-else>
        <h5> <strong>¿Está seguro de denegar esta compra?</strong> </h5>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto">
        <button v-if="reintegro_reintegrar==1" type="button" class="btn btn-success" name="button" @click="denegarCompra()">Registrar datos de re-integro y denegar la compra.</button>
        <button v-else type="button" class="btn btn-success" name="button" @click="denegarCompra()">Denegar la compra.</button>
      </div>
    </div>
  </div>
</div>
