<div class="modal" id="validarPago" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">VALIDACIÓN DE PAGO</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body row" v-if="this.Oficios[indexOficio].oferta_economica_pagos.length>0">
        <!-- LISTADO PAGOS -->
        <div class="col-md-8 table-responsive">
          <table class="table table-bordered text-center">
            <thead class="bg-primary text-white">
              <th>Referencia</th>
              <th>Monto</th>
              <th>Acción</th>
            </thead>
            <tbody>
              <tr v-for="(pago,index) in this.Oficios[indexOficio].oferta_economica_pagos" v-if="pago.tipo_pago=='transferencia'">
                <td>@{{pago.referencia}}</td>
                <td>@{{pago.monto}} Bs.S</td>
                <td v-if="pago.validacion" class="text-center">
                  <button type="button" class="btn btn-primary" name="button" @click="pago_index=index"> <i class="fa fa-search"></i> </button>
                  <!-- <i title="Pago confirmado" class="bg-success text-white fa fa-check fa-4x"></i> -->
                  <button type="button" class="btn btn-success" title="Pago confirmado" name="button"><i title="Pago confirmado" class="bg-success text-white fa fa-check"></i></button>
                </td>
                <td v-else class="text-center">
                  <button type="button" class="btn btn-primary" name="button" @click="pago_index=index"> <i class="fa fa-search"></i> </button>
                  <button type="button" name="button" title="Confirmar pago" class="btn bg-secondary" @click="confirmarPago(index)"><i title="Confirmar pago" class="fa fa-circle"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-4 text-center" v-if="pago_index!=null && Oficios[indexOficio].oferta_economica_pagos[pago_index].tipo_pago=='transferencia'">
          <h3><strong>Soporte de Pago (Referencia: @{{Oficios[indexOficio].oferta_economica_pagos[pago_index].referencia}}):</strong></h3>
          <a :href="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" download>
            <img class="img-fluid img-responsive" :src="Oficios[indexOficio].oferta_economica_pagos[pago_index].soporte_lote" alt="">
          </a>
        </div>
        <div class="col-md-4 text-center" v-else>
          <h3><strong>Selecciona un pago para ver su soporte</strong></h3>
          <img src="http://rec.vtelca.gob.ve/img/loader-128.gif" class="img-fluid img-responsive" alt="">
        </div>
        <!-- LISTADO PAGOS -->
      </div>
      <div class="modal-body text-center" v-else>
        <h5>No hay pagos por validar</h5>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">

      </div>
    </div>
  </div>
</div>
