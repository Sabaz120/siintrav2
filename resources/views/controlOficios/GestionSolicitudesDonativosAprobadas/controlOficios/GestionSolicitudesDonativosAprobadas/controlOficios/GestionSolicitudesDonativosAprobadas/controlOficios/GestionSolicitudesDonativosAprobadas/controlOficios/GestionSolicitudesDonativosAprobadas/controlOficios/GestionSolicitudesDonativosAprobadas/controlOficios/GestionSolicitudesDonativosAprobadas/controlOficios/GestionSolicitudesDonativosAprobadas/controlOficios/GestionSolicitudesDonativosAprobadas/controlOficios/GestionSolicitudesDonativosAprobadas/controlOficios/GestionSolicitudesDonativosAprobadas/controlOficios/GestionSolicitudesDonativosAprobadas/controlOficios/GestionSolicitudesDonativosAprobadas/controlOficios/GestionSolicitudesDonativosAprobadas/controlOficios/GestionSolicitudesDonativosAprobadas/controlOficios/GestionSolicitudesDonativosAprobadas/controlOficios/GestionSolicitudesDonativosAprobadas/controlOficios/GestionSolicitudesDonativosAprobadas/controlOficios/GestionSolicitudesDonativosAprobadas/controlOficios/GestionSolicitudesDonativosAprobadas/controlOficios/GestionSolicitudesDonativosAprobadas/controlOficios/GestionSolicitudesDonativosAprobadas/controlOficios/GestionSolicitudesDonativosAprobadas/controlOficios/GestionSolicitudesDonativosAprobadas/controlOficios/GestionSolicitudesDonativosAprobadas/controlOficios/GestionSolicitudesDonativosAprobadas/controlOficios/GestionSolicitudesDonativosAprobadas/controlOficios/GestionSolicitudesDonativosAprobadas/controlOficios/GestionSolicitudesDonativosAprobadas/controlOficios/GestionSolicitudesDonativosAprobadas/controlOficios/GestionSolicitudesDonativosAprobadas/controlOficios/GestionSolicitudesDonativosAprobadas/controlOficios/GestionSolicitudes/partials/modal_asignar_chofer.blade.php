<div class="modal" id="asignarChofer" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">RESPONSABLE DE RETIRO</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row text-center" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho==null">
          <div class="col-md-12 form-group">
            <h3>DATOS DEL CHOFER</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4">
              <input type="text" v-model="cedula_chofer" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(cedula_chofer,'chofer','registrar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Nombre y Apellido:</strong>
            <input type="text" readonly v-model="chofer_nombres_apellidos" class="form-control" >
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Número de Contacto:</strong>
            <input type="text" readonly v-model="chofer_numero" class="form-control" >
          </div>
          <div class="col-md-12 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Destino del Despacho:</strong>
            <input type="text" v-model="destino_despacho" class="form-control" maxlength="80">
          </div>
          <div class="col-md-12 form-group">
            <h3>DATOS DEL VEHÍCULO</h3>
          </div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Placa del Vehículo:</strong>
            <div class="input-group mb-4">
              <input type="text" name="" v-model="vehiculo_placa" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarVehiculo(vehiculo_placa,'registrar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Modelo de Vehículo:</strong>
            <input type="text" readonly v-model="vehiculo_modelo" class="form-control" >
          </div>
          <div class="col-md-12 form-group">
            <h3>DATOS DEL RESPONSABLE</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4">
              <input type="text" name="" v-model="cedula_responsable" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(cedula_responsable,'responsable','registrar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Nombre y Apellido:</strong>
            <input type="text" readonly v-model="responsable_nombres_apellidos" class="form-control" >
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Número de Contacto:</strong>
            <input type="text" readonly v-model="responsable_numero" class="form-control" >
          </div>
          <div class="col-md-12 form-group">
            <h3>BENEFICIARIOS</h3>
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Listado de Beneficiarios:</strong>
            <input type="file" name="file1" id="1" @change="convertirBase64Beneficiarios" accept="application/pdf" class="form-control" >
          </div>
          <div class="col-md-3"></div>
          <div class="mx-auto">
            <br>
            <button type="button" name="button" class="btn btn-success" @click="registrarChofer()">Registrar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
        <div class="row text-center" v-else>
          <div class="col-md-12 form-group">
            <h3>DATOS DEL CHOFER</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
              <input type="text" name="" v-model="Oficios[indexOficio].oferta_economica.despacho.chofer.cedula" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(Oficios[indexOficio].oferta_economica.despacho.chofer.cedula,'chofer','editar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-4 form-group" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <i class="fa fa-asterisk"></i>
            <strong>Nombre y Apellido:</strong>
            <input type="text" readonly v-model="Oficios[indexOficio].oferta_economica.despacho.chofer.nombres_apellidos" class="form-control" >
          </div>
          <div class="col-md-4 form-group" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <i class="fa fa-asterisk"></i>
            <strong>Número de Contacto:</strong>
            <input type="text" readonly v-model="Oficios[indexOficio].oferta_economica.despacho.chofer.telefono" class="form-control" >
          </div>
          <div class="col-md-12 form-group" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <i class="fa fa-asterisk"></i>
            <strong>Destino del Despacho:</strong>
            <input type="text" v-model="Oficios[indexOficio].oferta_economica.despacho.destino" class="form-control" maxlength="80">
          </div>
          <div class="col-md-12 form-group">
            <h3>DATOS DEL VEHÍCULO</h3>
          </div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Placa del Vehículo:</strong>
            <div class="input-group mb-4" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
              <input type="text" name="" v-model="Oficios[indexOficio].oferta_economica.despacho.vehiculo.placa" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarVehiculo(Oficios[indexOficio].oferta_economica.despacho.vehiculo.placa,'editar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-6 form-group" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <i class="fa fa-asterisk"></i>
            <strong>Modelo de Vehículo:</strong>
            <input type="text" readonly v-model="Oficios[indexOficio].oferta_economica.despacho.vehiculo.modelo" class="form-control" >
          </div>
          <div class="col-md-12 form-group">
            <h3>DATOS DEL RESPONSABLE</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
              <input type="text" name="" v-model="Oficios[indexOficio].oferta_economica.despacho.responsable.cedula" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(Oficios[indexOficio].oferta_economica.despacho.responsable.cedula,'responsable','editar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-4 form-group" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <i class="fa fa-asterisk"></i>
            <strong>Nombre y Apellido:</strong>
            <input type="text" readonly v-model="Oficios[indexOficio].oferta_economica.despacho.responsable.nombres_apellidos" class="form-control" >
          </div>
          <div class="col-md-4 form-group" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <i class="fa fa-asterisk"></i>
            <strong>Número de Contacto:</strong>
            <input type="text" readonly v-model="Oficios[indexOficio].oferta_economica.despacho.responsable.telefono" class="form-control" >
          </div>
          <div class="col-md-12 form-group">
            <h3>BENEFICIARIOS</h3>
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Listado de Beneficiarios:</strong>
            <input type="file" name="file2" id="2" @change="convertirBase64BeneficiariosEditar" accept="application/pdf" class="form-control" >
          </div>
          <div class="col-md-3" v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
            <strong>Adjunto</strong>
            <br>
            <a :href="Oficios[indexOficio].oferta_economica.despacho.beneficiario" download class="btn btn-success">Descargar</a>
          </div>
          <div class="mx-auto">
            <br>
            <button type="button" name="button" class="btn btn-success" @click="actualizarChofer()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">
        {{--<table class="table table-bordered text-center">
          <thead>
            <th>Cédula</th>
            <th>Nombre y Apellido</th>
            <th>Número de Contacto</th>
            <th>Destino de Despacho</th>
            <th>Vehículo</th>
            <th>Placa</th>
            <th>Acción</th>
          </thead>
          <tbody>
            <tr v-if="Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho!=null">
              <td>@{{Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho.chofer.cedula}}</td>
              <td>@{{Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho.chofer.nombres_apellidos}}</td>
              <td>@{{Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho.chofer.telefono}}</td>
              <td>@{{Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho.destino}}</td>
              <td>@{{Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho.vehiculo.modelo}}</td>
              <td>@{{Oficios[indexOficio].oferta_economica!=null && Oficios[indexOficio].oferta_economica.despacho.vehiculo.placa}}</td>
              <td class="text-center">
                <button type="button" class="btn btn-info" name="button" title="Editar"><i class="fa fa-pencil-square-o"></i></button>
              </td>
            </tr>
          </tbody>
        </table>--}}
      </div>
    </div>
  </div>
</div>
