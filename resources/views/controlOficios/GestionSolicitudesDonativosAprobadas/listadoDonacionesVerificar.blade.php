@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE DONACIONES POR VERIFICAR</h5>
      <div class="row justify-content-center mb-5 text-center">
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespacho" clas=""><strong>Fecha Inicio:</strong></label>
            <input class="form-control text-uppercase text-center"    type="date" placeholder="" v-model="fechaDespacho" required>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespachoFin" clas=""><strong>Fecha Fin:</strong></label>
            <input class="form-control text-uppercase text-center"    type="date" placeholder="" v-model="fechaDespachoFin" required>
          </div>
        </div>
        <div class="col-md-1 text-center">
          <br>
          <button type="button" class="btn btn-info mt-2" @click="buscarOficioPorFecha" name="button">Buscar</button>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-if='oficios.length'>
          <div class="row">
            <div class="header mx-auto">
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <div id="no-more-tables">
                  <table  class="table table-bordered">
                    <thead class="bg-primary text-white">
                      <tr>
                        <th scope="col" style="cursor:pointer" class="text-center align-middle" @click="sort('cedula')">Cédula</th>
                        <th scope="col" style="cursor:pointer" class="text-center align-middle" @click="sort('responsable')"> Responsable</th>
                        <th scope="col" class="text-center align-middle" >Material - Cantidad</th>
                        <th scope="col" class="text-center align-middle" >Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(fila,index) in orderBy(registros, currentSort,currentSortDir)" v-show='statusFiltro'>
                        <td class="text-center align-middle" data-title="Cédula">@{{fila.cedula | capitalize }}</td>
                        <td  class="text-center align-middle" data-title="Responsable">@{{fila.responsable | capitalize}}</td>
                        <td  class="text-center align-middle" data-title="Materiales">
                          <p v-for="material in fila.materiales">
                            @{{material.nombre | capitalize}} - @{{material.cantidad_solicitada}}
                          </p>
                        </td>
                        <td class="text-center align-middle">
                          <button type="button" :disabled="!fila.verificar" title="Confirmar salida de materiales" class="btn btn-primary text-white text-center" @click="Verificar(index)" name="button">
                            <i class="fa fa-check fa-1x"></i>
                          </button>
                          <button  v-if="fila.condicional_material == 1"  @click="ObtenerId(fila)" type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#ChoferModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Salida" >
                            <i class="fa fa-car-alt fa-1x" aria-hidden="true"></i>
                          </button>
                        </td>
                      </tr>
                      <tr v-show='!statusFiltro'>
                        <td class="text-justify font-weight-bold" colspan="4">No se encontraron registros</td>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-12">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{oficios.length}} </strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 text-center">
              <!--<button type="button" class="btn btn-warning mt-2" @click="generarPdf" name="button">Generar PDF</button>-->
            </div>
          </div>
        </div>
      </div>
      <div class="text-center" v-else>
        <h3>No existe solicitudes por Verificar</h3>
      </div>


    </div>
  </div>

  <!-- The Modal Chofer -->
  <div class="modal" id="ChoferModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary text-center">
          <h4 class="modal-title mx-auto  text-white">CHOFER RESPONSABLE DE SALIDA DE MATERIAL</h4>
          <button type="button" class="close" data-dismiss="modal" v-on:click="limpiar">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row justify-content-center text-center">
            <div class="col-12 col-md-12 cl-lg-12 text-center">
              <h4 class="modal-title mx-auto font-weight-bold mb-2">DATOS DEL CHOFER</h4>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="cedulaChofer"><strong>Cédula:</strong></label>
                <div class="input-group mb-4">

                  <input type="text" name="" v-model="cedulaChofer"  onkeypress="return soloNumeros(event)"  class="form-control">
                  <div class="input-group-append">
                    <button class="btn btn-info" @click="buscarChofer" type="button"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-5 col-lg-5">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="nombresChofer"><strong>Nombre y Apellido:</strong></label>
                <input  class="form-control" v-bind:class="{ 'is-invalid': nombresChoferRequerido }"   minlength="3" maxlength="45"  type="text" placeholder="" v-model="nombresChofer" required readonly>
              </div>
            </div>
            <div class="col-12 col-md-4 col-lg-4">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="telefonoChofer"><strong>Número de Contacto:</strong></label>
                <input  class="form-control" v-bind:class="{ 'is-invalid': telefonoChoferRequerido }"   minlength="11" maxlength="11"  type="text" placeholder="" v-model="telefonoChofer" required readonly>
              </div>
            </div>
            {{--Datos Vehículo--}}
            <div class="col-12 col-md-12 cl-lg-12 text-center">
              <h4 class="modal-title mx-auto font-weight-bold mb-2">DATOS DEL VEHÍCULO</h4>
            </div>


            <div class="col-12 col-md-5 col-lg-5">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="placaVehiculo"><strong>Placa de Vehículo:</strong></label>
                <div class="input-group mb-4">
                  <input class="form-control text-uppercase" type="text" name="" v-model="placaVehiculo" v-bind:class="{ 'is-invalid': placaVehiculoRequerido }" onkeypress="return soloLetrasNumerosSinEspacios(event)"  minlength="6" maxlength="8"  type="text" placeholder="" v-model="placaVehiculo" required>
                  <div class="input-group-append">
                    <button class="btn btn-info" @click="buscarVehiculo" type="button"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-1 col-lg-1">
            </div>
            <div class="col-12 col-md-5 col-lg-5">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="modeloVehiculo"><strong>Modelo de Vehículo:</strong></label>
                <input class="form-control" v-bind:class="{ 'is-invalid': modeloVehiculoRequerido }"  minlength="3" maxlength="45"  type="text" placeholder="" v-model="modeloVehiculo" required readonly>
              </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2">
            </div>

            {{--Datos Responsable--}}
            <div class="col-12 col-md-12 cl-lg-12 text-center">
              <h4 class="modal-title mx-auto font-weight-bold mb-2">DATOS DEL RESPONSABLE</h4>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="cedulaResponsable"><strong>Cédula:</strong></label>
                <div class="input-group mb-4">
                  <input type="text" name="" v-model="cedulaResponsable" v-mask="'########'"  v-bind:class="{ 'is-invalid': cedulaResponsableRequerido }" onkeypress="return soloNumeros(event)"  class="form-control" value="">
                  <div class="input-group-append">
                    <button class="btn btn-info" @click="buscarResponsable" type="button"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-5 col-lg-5">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="nombresResponsable"><strong>Nombre y Apellido:</strong></label>
                <input  class="form-control" v-bind:class="{ 'is-invalid': nombresResponsableRequerido }"   minlength="3" maxlength="200"  type="text" placeholder="" v-model="nombresResponsable" required readonly>
              </div>
            </div>
            <div class="col-12 col-md-4 col-lg-4">
              <div class="form-group">
                <i class="fas fa-asterisk"></i>
                <label for="telefonoResponsable"><strong>Número de Contacto:</strong></label>
                <input  class="form-control" v-bind:class="{ 'is-invalid': telefonoResponsableRequerido }"   minlength="3" maxlength="200"  type="text" placeholder="" v-model="telefonoResponsable" required readonly>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-12 text-center">
              <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar" v-show="actualizar">Registrar</button>
              <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar" v-show="actualizar">Limpiar</button>
              <button type="button" class="btn btn-success font-weight-bold" v-on:click="actualizarRegistro" v-show="!actualizar">Actualizar</button>
              <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar" v-show="!actualizar"  data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
</section>
@endsection
@push('scripts')
<script>
function process(date){
  var parts = date.split("-");
  return new Date(parts[2], parts[1] - 1, parts[0]);
}
const app= new Vue({
  el:'#register',
  data:{
    errors: [],
    oficios:{!! $oficios ? $oficios : "''"!!},
    fechaDespacho:'',
    fechaDespachoFin:'',
    search:'',
    materiales:'',
    items:'',
    idChofer:'',
    actualizar:true,

    cedulaChofer:'',
    nombresChofer:'',
    telefonoChofer:'',
    modeloVehiculo:'',
    idVehiculo:'',
    placaVehiculo:'',
    cedulaResponsable:'',
    idResponsable:'',
    nombresResponsable:'',
    telefonoResponsable:'',
    despachos:'',
    cedulaChoferRequerido:false,
    nombresChoferRequerido:false,
    telefonoChoferRequerido:false,
    modeloVehiculoRequerido:false,
    placaVehiculoRequerido:false,
    cedulaResponsableRequerido:false,
    nombresResponsableRequerido:false,
    telefonoResponsableRequerido:false,
    idSolicitud:'',

    DatosChofer:'',

    currentSort:'cedula',//campo por defecto que tomara para ordenar
    currentSortDir:0,//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
  },
  computed:{
    registros:function() {
      this.rows=0;
      return this.oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    actualizarRegistro:function(){
      this.errors=[];
      this.cedulaChoferRequerido=false;
      this.nombresChoferRequerido=false;
      this.telefonoChoferRequerido=false;
      this.modeloVehiculoRequerido=false;
      this.placaVehiculoRequerido=false;
      this.cedulaResponsableRequerido=false;
      this.nombresResponsableRequerido=false;
      this.telefonoResponsableRequerido=false;
      this.fechaDespachoRequerido=false;

      if (!this.idChofer) {
        this.errors.push("Existen campos por llenar:<br>*Datos del Chofer.");
        this.cedulaChoferRequerido=true;
        this.nombresChoferRequerido=true;
        this.telefonoChoferRequerido=true;
      }else if(this.cedulaChofer.length<7){
        this.errors.push("El número de cédula del chofer es invalido.");
        this.cedulaChoferRequerido=true;
        this.nombresChoferRequerido=true;
        this.telefonoChoferRequerido=true;
      }//if (!this.idChofer)
      else if (!this.idVehiculo){
        this.errors.push("Existen campos por llenar:<br>*Datos del Vehículo");
        this.modeloVehiculoRequerido=true;
        this.placaVehiculoRequerido=true;
      }//if (!this.idVehiculo)
      else if (this.placaVehiculo.length<6){
        this.errors.push("La placa es invalidad");
        this.modeloVehiculoRequerido=true;
        this.placaVehiculoRequerido=true;
      }//if (!this.idVehiculo)
      else if (!this.idResponsable){
        this.errors.push("Existen campos por llenar:<br>*Datos del Responsable");
        this.cedulaResponsableRequerido=true;
        this.nombresResponsableRequerido=true;
        this.telefonoResponsableRequerido=true;
      }else if (this.cedulaResponsable.length<7){
        this.errors.push("El número de cédula del responsable  es invalido.");
        this.cedulaResponsableRequerido=true;
        this.nombresResponsableRequerido=true;
        this.telefonoResponsableRequerido=true;
      }//else if (!this.idResponsable)
      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
      }//if(this.errors.length>0)
      else{
        let self = this;
        axios.post('{{ url("actualizar_datos_chofer") }}', {

          chofer_id:this.idChofer,
          vehiculo_id: this.idVehiculo,
          responsable_id: this.idResponsable,
          registro_oficio_id:this.idSolicitud,
        })
        .then(function (response) {
          if(response.data.status=="success"){
            alertify.set('notifier','position', 'top-right');
            alertify.success('Registro Satisfactorio.');
            $('#ChoferModal').modal('toggle');
            self.despachos=response.data.despachos;
            self.oficios=response.data.oficios;
            // self.limpiar();

          }//if(response.data.status=="success")
          if(response.data.status=="error"){
            alertify.set('notifier','position', 'top-right');
            alertify.error(response.data.mensaje);
          }//if(response.data.status=="error")

        })
        .catch(function (error) {
          console.log(error);
        });
      }//else if(this.errors.length>0)
    },//actualizar:function()
    registrar:function(){
      this.errors=[];
      this.cedulaChoferRequerido=false;
      this.nombresChoferRequerido=false;
      this.telefonoChoferRequerido=false;
      this.modeloVehiculoRequerido=false;
      this.placaVehiculoRequerido=false;
      this.cedulaResponsableRequerido=false;
      this.nombresResponsableRequerido=false;
      this.telefonoResponsableRequerido=false;
      if (!this.idChofer) {
        this.errors.push("Existen campos por llenar:<br>*Datos del Chofer.");
        this.cedulaChoferRequerido=true;
        this.nombresChoferRequerido=true;
        this.telefonoChoferRequerido=true;
      }else if(this.cedulaChofer.length<7){
        this.errors.push("El número de cédula del chofer es invalido.");
        this.cedulaChoferRequerido=true;
        this.nombresChoferRequerido=true;
        this.telefonoChoferRequerido=true;
      }//if (!this.idChofer)
      else if (!this.idVehiculo){
        this.errors.push("Existen campos por llenar:<br>*Datos del Vehículo");
        this.modeloVehiculoRequerido=true;
        this.placaVehiculoRequerido=true;
      }//if (!this.idVehiculo)
      else if (this.placaVehiculo.length<6){
        this.errors.push("La placa es invalidad");
        this.modeloVehiculoRequerido=true;
        this.placaVehiculoRequerido=true;
      }//if (!this.idVehiculo)
      else if (!this.idResponsable){
        this.errors.push("Existen campos por llenar:<br>*Datos del Responsable");
        this.cedulaResponsableRequerido=true;
        this.nombresResponsableRequerido=true;
        this.telefonoResponsableRequerido=true;
      }else if (this.cedulaResponsable.length<7){
        this.errors.push("El número de cédula del responsable  es invalido.");
        this.cedulaResponsableRequerido=true;
        this.nombresResponsableRequerido=true;
        this.telefonoResponsableRequerido=true;
      }//else if (!this.idResponsable)
      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
      }//if(this.errors.length>0)
      else{
        axios.post('{{ url("registrar_datos_chofer") }}', {
          chofer_id:this.idChofer,
          vehiculo_id: this.idVehiculo,
          responsable_id: this.idResponsable,
          registro_oficio_id:this.idSolicitud,
        })
        .then(response => {
          if(response.data.status=="success"){
            alertify.set('notifier','position', 'top-right');
            alertify.success('Registro Satisfactorio.');
            $('#ChoferModal').modal('toggle');
            this.despachos=response.data.despachos;
            // self.limpiar();
            var index=0;
            for(var i=0;i<this.oficios.length;i++){
              if(this.oficios[i].id==this.idSolicitud){
                index=i;
                break;
              }//this.oficios[i].id
            }//for()
            this.oficios[index].chofer={
              registrado:0
            };
            console.log('Resultado exitoso');
          }//if(response.data.status=="success")
          if(response.data.status=="error"){
            alertify.set('notifier','position', 'top-right');
            alertify.error(response.data.mensaje);
          }//if(response.data.status=="error")
        })
        .catch(function (error) {
          console.log(error);
        });
      }//else if(this.errors.length>0)
    },//registrar:function()
    buscarResponsable:function(){
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:this.cedulaResponsable}).then(response => {
        if(response.data.error==0){
          this.idResponsable=response.data.persona_natural.id;
          this.nombresResponsable=response.data.persona_natural.apellido+" "+response.data.persona_natural.nombre;
          this.telefonoResponsable=response.data.persona_natural.telefono;
        }//if(response.data.status=="success")
        if(response.data.error==1){
          alertify.error(response.data.msg);
        }//if(response.data.status=="error")
      }).catch(error => {
        alertify.error(""+error);
      });
    },//buscarResponsable:function()
    buscarVehiculo:function(){
      axios.post('{{ route("api.controloficio.datos.vehiculo") }}', {placa:this.placaVehiculo.toUpperCase()}).then(response => {
        if(response.data.error==0){
          this.idVehiculo=response.data.vehiculo.id;
          this.modeloVehiculo=response.data.vehiculo.modelo;
        }//if(response.data.status=="success")
        if(response.data.error==1){
          alertify.error(response.data.msg);
        }//if(response.data.status=="error")
      }).catch(error => {
        alertify.error(""+error);
      });
    },//buscarResponsable:function()
    buscarChofer:function(){
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:this.cedulaChofer}).then(response => {
        if(response.data.error==0){
          this.idChofer=response.data.persona_natural.id;
          this.nombresChofer=response.data.persona_natural.apellido+" "+response.data.persona_natural.nombre;
          this.telefonoChofer=response.data.persona_natural.telefono;
        }//if(response.data.status=="success")
        if(response.data.error==1){
          alertify.error(response.data.msg);
        }//if(response.data.status=="error")
      }).catch(error => {
        alertify.error(""+error);
      });
    },//buscarChofer:function()
    limpiar:function(){
      this.idChofer='';
      this.cedulaChofer='';
      this.nombresChofer='';
      this.telefonoChofer='';
      this.idVehiculo='';
      this.modeloVehiculo='';
      this.placaVehiculo='';
      this.cedulaResponsable='';
      this.idResponsable='';
      this.nombresResponsable='';
      this.telefonoResponsable='';
      this.actualizar=true;
      this.cedulaChoferRequerido=false;
      this.nombresChoferRequerido=false;
      this.telefonoChoferRequerido=false;
      this.modeloVehiculoRequerido=false;
      this.placaVehiculoRequerido=false;
      this.cedulaResponsableRequerido=false;
      this.nombresResponsableRequerido=false;
      this.telefonoResponsableRequerido=false;
      this.items='';
      this.fechaDespacho='';
      this.fechaDespachoRequerido=false;
    },// limpiar:function()
    cambiar:function(){
      this.actualizar=false;
    },//cambiar:function()
    ObtenerId:function(items){
      this.fechaDespacho='';
      this.idSolicitud=items.id;
      this.items=items;
      this.buscarDespacho();
    },//ObtenerId
    buscarDespacho:function(){
      let self = this;
      // console.log(this.idSolicitud);
      // axios.post('{{ url("buscar_datos_despacho") }}', {
      axios.post('{{ url("buscar_datos_chofer") }}', {
        registro_oficio_id:this.idSolicitud,
      })
      .then(function (response) {
        if(response.data.status=="success"){
          // console.log(response.data.datosChofer);
          self.cedulaChofer = response.data.datosChofer.cedulaChofer;
          self.nombresChofer = response.data.datosChofer.nombresChofer;
          self.telefonoChofer = response.data.datosChofer.telefono;
          self.placaVehiculo = response.data.datosChofer.placa;
          self.modeloVehiculo = response.data.datosChofer.modelo;
          self.cedulaResponsable = response.data.datosChofer.cedulaChofer;
          self.nombresResponsable = response.data.datosChofer.nombresChofer;
          self.telefonoResponsable = response.data.datosChofer.telefono;
          self.actualizar=false;
        }//if(response.data.status=="success")
        else{
          self.actualizar=true;
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    },//buscarDespacho:function()
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.oficios){
          if(this.oficios[i].responsable.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.oficios[i].cedula.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.oficios[i]);
          }// if(this.oficios[i].cedulaResponsable.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.oficios[i].nombresResponsableDespacho.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].cedulaChofer.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombresChofer.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].modelo.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].placa.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombres_materiales.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].materiales_cantidad.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombre_estado.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.oficios)
        if(filtrardo.length)
        this.oficios=filtrardo;
        else{
          this.statusFiltro=0;
          this.oficios={!! $oficios ? $oficios : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.oficios={!! $oficios ? $oficios : "''"!!};
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      this.currentSort=s;
      if(this.currentSortDir==0)
      this.currentSortDir=-1;
      else
      this.currentSortDir=0;
    },
    buscarOficioPorFecha(){

      if(this.fechaDespacho=="" && this.fechaDespachoFin==""){
        alertify.error('Debe seleccionar un rango de fechas válido.');
      }else if(this.fechaDespacho && this.fechaDespachoFin==""){
        alertify.error('Debe una fecha de fin válida');
      }else if(this.fechaDespacho=="" && this.fechaDespachoFin){
        alertify.error('Debe una fecha de inicio válida');
      }else if(process(this.fechaDespacho)>process(this.fechaDespachoFin)){
        alertify.error('El intervalo de fechas es erróneo.');
      }else{
        axios.post('{{ url("listado_donaciones_por_verificar") }}', {
          fecha_inicio:this.fechaDespacho,
          fecha_fin:this.fechaDespachoFin
        }).then(response => {
          if(response.data.error==0){
            this.oficios=response.data.oficios;
          }else  {
            alertify.error('No existe solicitudes por Verificar');
          }

        }).catch(error => {
          alertify.error(""+error);
        });
      }
    },
    Verificar(indexOficio){
      var b=0;//Para saber que tiene madera como material y debe tener un chofer registrado.
      for(var i=0;i<this.oficios[indexOficio].materiales.length;i++){
        if(this.oficios[indexOficio].materiales[i].nombre=="madera" || this.oficios[indexOficio].materiales[i].nombre=="maderas"){
          b=1;//id 37 es el material maderas.
        }
      }//for
      if(b==1 && this.oficios[indexOficio].chofer==null)
        alertify.error('No puedes verificar el oficio, si no tiene chofer asignado.');//Solo si tiene en sus materiales,madera.
      else
        b=0;
      if(b==0){
        axios.post('{{ url("listado_donaciones_verificar") }}', {
          oficio_id:this.oficios[indexOficio].id
        }).then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.oficios=response.data.oficios;
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      }//b==0
    }
  },//methods
  mounted(){
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output =  d.getFullYear() +'-'+ (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day ;

    this.fechaDespacho = output;
    this.fechaDespachoFin = output;

  }
});//const app= new Vue
</script>
@endpush
