

@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE DONACIONES</h5>
      <div class="row justify-content-center mb-5 text-center">
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespacho" clas=""><strong>Estado del oficio:</strong></label>
            <select class="form-control" name="estatus_oficio" v-model="estatus_oficio">
              <option value="0">Seleccione un estado</option>
              <option value="2">En proceso de despacho</option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespacho" clas=""><strong>Fecha Inicio:</strong></label>
            <input class="form-control text-uppercase text-center"    type="date" placeholder="" v-model="fechaDespacho" required>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespachoFin" clas=""><strong>Fecha Fin:</strong></label>
            <input class="form-control text-uppercase text-center"    type="date" placeholder="" v-model="fechaDespachoFin" required>
          </div>
        </div>
        <div class="col-md-1 text-center">
          <br>
          <button type="button" class="btn btn-info mt-2" @click="buscarOficioPorFecha" name="button">Buscar</button>
        </div>

      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show='oficios.length'>
          <div class="row">
            <div class="header mx-auto">
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <div id="no-more-tables">
                  <table  class="table table-bordered table-condensed cf table-striped">
                    <thead class="bg-primary text-white">
                      <tr>
                        <th scope="col" style="cursor:pointer; width:12%;" class="text-center align-middle" @click="sort('cedulaResponsable')">Cédula</th>
                        <th scope="col" style="cursor:pointer; width:12%;" class="text-center align-middle" @click="sort('nombresResponsableDespacho')"> Responsable</th>
                        <th scope="col" style="cursor:pointer; width:12%;" class="text-center align-middle" @click="sort('cedulaChofer')">Cédula de Chofer</th>
                        <th scope="col" style="cursor:pointer; width:12%;" class="text-center align-middle"@click="sort('nombresChofer')">Chofer</th>
                        <th scope="col" style="cursor:pointer; width:12%;" class="text-center align-middle"@click="sort('modeloVehiculo')">Vehículo</th>
                        <th scope="col" style="width:12%;" class="text-center align-middle" @click="sort('placaVehiculo')">Placa</th>
                        <th scope="col" style="width:12%;" class="text-center align-middle" @click="sort('nombres_materiales')">Material</th>
                        <th scope="col" class="text-center align-middle"  style="width:12%;" @click="sort('materiales_cantidad')">Cantidad</th>
                        <th scope="col" class="text-center align-middle"  style="width:12%;" @click="sort('nombre_estado')">Estatus</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(fila,index) in orderBy(registros, currentSort,currentSortDir)" v-show='statusFiltro'>
                        <td class="text-center align-middle" data-title="Cédula">@{{fila.cedulaResponsable | capitalize  | placeholder('Sin información')}}</td>
                        <td  class="text-justify align-middle" data-title="Responsable">@{{fila.nombre_responsable | capitalize  | placeholder('Sin información')}}</td>
                        <td class="text-center align-middle" data-title="Cédula de Chofer">@{{fila.cedulaChofer | capitalize  | placeholder('Sin información')}}</td>
                        <td  class="text-justify text-capitalize align-middle" data-title="Chofer">@{{fila.nombresChofer | capitalize  | placeholder('Sin información')}}</td>
                        <td class="text-justify text-capitalize align-middle" data-title="Vehículo">@{{fila.modelo | capitalize  | placeholder('Sin información')}}</td>
                        <td  class="text-center align-middle" data-title="Placa">@{{fila.placa | capitalize  | placeholder('Sin información')}}</td>
                        <td  class="text-justify align-middle" data-title="Material">@{{fila.nombres_materiales | capitalize  | placeholder('Sin información')}}</td>
                        <td class="text-center align-middle" data-title="Cantidad">@{{fila.materiales_cantidad | capitalize  | placeholder('Sin información')}}</td>
                        <td class="text-center align-middle" data-title="Estatus">@{{fila.nombre_estado | capitalize  | placeholder('Sin información')}}</td>
                      </tr>
                      <tr v-show='!statusFiltro'>
                        <td class="text-justify font-weight-bold" colspan="8">No se encontraron registros</td>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-12">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{oficios.length}} </strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 text-center">
              <button type="button" class="btn btn-warning mt-2" @click="generarPdf" name="button">Generar PDF</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
@endsection
@push('scripts')
<script>
function process(date){
  var parts = date.split("-");
  return new Date(parts[2], parts[1] - 1, parts[0]);
}
const app= new Vue({
  el:'#register',
  data:{
    errors: [],
    oficios:{!! $oficios ? $oficios : "''"!!},
    fechaDespacho:'',
    fechaDespachoFin:'',
    estatus_oficio:0,
    search:'',
    currentSort:'fecha_emision',//campo por defecto que tomara para ordenar
    currentSortDir:0,//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,

  },
  computed:{
    registros:function() {
      this.rows=0;
      return this.oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.oficios){
          if(this.oficios[i].cedulaResponsable.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.oficios[i].nombresResponsableDespacho.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].cedulaChofer.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombresChofer.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].modelo.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].placa.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombres_materiales.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].materiales_cantidad.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombre_estado.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.oficios[i]);
          }// if(this.oficios[i].cedulaResponsable.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.oficios[i].nombresResponsableDespacho.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].cedulaChofer.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombresChofer.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].modelo.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].placa.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombres_materiales.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].materiales_cantidad.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.oficios[i].nombre_estado.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.oficios)
        if(filtrardo.length)
        this.oficios=filtrardo;
        else{
          this.statusFiltro=0;
          this.oficios={!! $oficios ? $oficios : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.oficios={!! $oficios ? $oficios : "''"!!};
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      this.currentSort=s;
      if(this.currentSortDir==0)
      this.currentSortDir=-1;
      else
      this.currentSortDir=0;
    },
    buscarOficioPorFecha(){
      if(this.fechaDespacho=="" && this.fechaDespachoFin==""){
        this.errors.push("Debe seleccionar un rango de fechas válido.");
      }else if(this.fechaDespacho && this.fechaDespachoFin==""){
        this.errors.push("Debe una fecha de fin válida");
      }else if(this.fechaDespacho=="" && this.fechaDespachoFin){
        this.errors.push("Debe una fecha de inicio válida");
      }else if(process(this.fechaDespacho)>process(this.fechaDespachoFin)){
        this.errors.push("El intervalo de fechas es erróneo.");
      }
      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
        this.errors=[];
      }//if(this.errors.length>0)
      else{
        axios.post('{{ url("listado_donaciones_por_fecha") }}', {
          fecha_despacho:this.fechaDespacho,
          estatus_oficio:this.estatus_oficio,
          fechaDespachoFin:this.fechaDespachoFin
        }).then(response => {

          if(response.data.error==0){
            this.oficios=response.data.oficios;
          }//if(response.data.status=="success")
          if(response.data.error==1){
            alertify.error(response.data.msg);
          }//if(response.data.status=="error")
        }).catch(error => {
          alertify.error(""+error);
        });
      }
    },//buscarResponsable:function()
    generarPdf(){
      if(this.oficios.length==0){
        alertify.set('notifier','position', 'top-right');
        alertify.error('No hay información para la generación del archivo pdf');
      }else{
        if(this.fechaDespacho && this.fechaDespachoFin==""){
          alertify.error("Debe una fecha de fin válida");
        }else if(this.fechaDespacho=="" && this.fechaDespachoFin){
          alertify.error("Debe una fecha de inicio válida");
        }else if(process(this.fechaDespacho)>process(this.fechaDespachoFin)){
          alertify.error("El intervalo de fechas es erróneo.");
        }else{
          var fechaDespacho = JSON.stringify(this.fechaDespacho);
          var fechaDespachoFin = JSON.stringify(this.fechaDespachoFin);
          window.open("{{ url('listado_donaciones_pdf') }}"+"?fechaDespacho="+fechaDespacho+"&fechaDespachoFin="+fechaDespachoFin, 'Download');
        }
      }//if(this.oficios.length==0)
    }//generarPdf:function()
  },//methods
});//const app= new Vue
</script>
@endpush
