@extends('layouts.app')
@section('contenido')
<!-- https://developer.snapappointments.com/bootstrap-select/examples/  -->
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE DONACIONES</h5>
      <div class="row justify-content-center mb-5 text-center">
        {{--<div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespacho" ><strong>Estado del oficio:</strong></label>
            <select class="form-control selectpicker" multiple data-live-search="true" name="estatus_oficio" v-model="estatus_oficio">
              <option value="0">Seleccione un estado</option>
              <option value="2">En proceso de despacho</option>
            </select>
          </div>
        </div>--}}
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespacho"><strong>Fecha Inicio:</strong></label>
            <input class="form-control text-uppercase text-center"    type="date" placeholder="" v-model="fechaDespacho" required>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespachoFin"><strong>Fecha Fin:</strong></label>
            <input class="form-control text-uppercase text-center"    type="date" placeholder="" v-model="fechaDespachoFin" required>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="fechaDespachoFin" ><strong>Material:</strong></label>
            <select class="form-control" multiple v-model="materiales_id">
              <option value="0" disabled>Seleccione material</option>
              @foreach($materiales as $material)
              <option class="text-capitalize" value="{{$material->id}}">{{$material->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="Tipo solicitante"><strong>Tipo de Solicitante:</strong></label>
            <select class="form-control" v-model="tipo_solicitante">
              <option value="0">Seleccione de Solicitante</option>
              <option value="1">Personal Natural</option>
              <option value="2">Personal Jurídico</option>
              <option value="3">Personal Trabajador</option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 ">
          <div class="form-group">
            <label for="Nombre solicitante"><strong>Solicitante:</strong></label>
            <input type="text" class="form-control" v-model="nombre_solicitante">
          </div>
        </div>
        <div class="col-md-1 text-center">
          <br>
          <button type="button" class="btn btn-info mt-2" @click="buscarOficioPorFecha" name="button">Buscar</button>
        </div>
      </div>
      <!-- <div id="container" v-show="false" style="height: 400px; min-width: 320px; max-width: 600px; margin: 0 auto"></div> -->
      <highcharts v-if="donut" :options="options"></highcharts>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-if='datosTablaFiltrado.length'>
          <div class="row">
            <div class="header mx-auto">
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <div id="no-more-tables">
                  <table  class="table table-bordered table-condensed cf table-striped">
                    <thead class="bg-primary text-white">
                      <tr>
                        <th scope="col" style="cursor:pointer; width:20%;" class="text-center align-middle" @click="sort('nombre')">Estado</th>
                        <th scope="col" style="cursor:pointer; width:20%;" class="text-center align-middle">Tipo de Solicitante</th>
                        <th scope="col" style="cursor:pointer; width:20%;" class="text-center align-middle" >Materiales</th>
                        <th scope="col" style="cursor:pointer; width:20%;" class="text-center align-middle" @click="sort('cantidadTOtal')">Cantidad Total</th>
                        <th scope="col" style="cursor:pointer; width:20%;" class="text-center align-middle">Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(fila,index) in orderBy(registros, currentSort,currentSortDir)" v-show='statusFiltro'>
                        <td class="text-center align-middle" data-title="Estado">@{{fila.nombre | capitalize  | placeholder('Sin información')}}</td>
                        <td  class="text-center align-middle" data-title="Tipo Solicitante">@{{tipoSolicitanteFiltrado}}</td>
                        <td class="text-center align-middle" data-title="Materiales">@{{nombresMaterialesConsultados}}</td>
                        <td  class="text-center text-capitalize align-middle" data-title="CantidadTotal">@{{fila.cantidadTotal | capitalize  | placeholder('Sin información')}}</td>
                        <td  class="text-center text-capitalize align-middle" data-title="Accion">
                          <form class="" action="{{url('listado_donaciones_pdf_individual')}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="tipoSolicitante" v-model="valorTipoSolicitanteFiltrado">
                            <input type="hidden" name="fechaInicio" v-model="fechasConsultadas.inicio">
                            <input type="hidden" name="fechaFin" v-model="fechasConsultadas.fin">
                            <input type="hidden" name="fechaFin" v-model="fechasConsultadas.fin">
                            <input type="hidden" name="estado_aprobacion_id" v-model="fila.id">
                            <input type="hidden" name="materiales_id" v-model="materialesConsultados">
                            <input type="hidden" name="nombreSolicitante" v-model="nombreSolicitanteConsultado">
                            <button title="Generar PDF" type="submit" class="btn btn-warning" name="button">
                              <i class="fa fa-file-pdf-o"></i>
                            </button>
                          </form>
                        </td>
                      </tr>
                      <tr v-show='!statusFiltro'>
                        <td class="text-justify font-weight-bold" colspan="4">No se encontraron registros</td>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-12">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{datosTablaFiltrado.length}} </strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 text-center">
              <form class="" action="{{url('listado_donaciones_pdf_global')}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="tipoSolicitante" v-model="valorTipoSolicitanteFiltrado">
                <input type="hidden" name="fechaInicio" v-model="fechasConsultadas.inicio">
                <input type="hidden" name="fechaFin" v-model="fechasConsultadas.fin">
                <input type="hidden" name="fechaFin" v-model="fechasConsultadas.fin">
                <input type="hidden" name="materiales_id" v-model="materialesConsultados">
                <input type="hidden" name="nombreSolicitante" v-model="nombreSolicitanteConsultado">
                <button type="submit" class="btn btn-warning mt-2" name="button">Generar PDF</button>
                <!-- <button title="Generar PDF" type="submit" class="btn btn-warning" name="button">
                <i class="fa fa-file-pdf-o"></i>
              </button> -->
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="text-center" v-else>
      <h3>No hay nada que mostrar</h3>
    </div>
  </div> <!-- tabla registros-->
</div>
</div>
</section>
@endsection
@push('scripts')

<script>
function process(date){
  var parts = date.split("-");
  return new Date(parts[2], parts[1] - 1, parts[0]);
}
const app= new Vue({
  el:'#register',
  data:{
    options : {
      colors : [ '#7cb5ec','#434348','#434348','#434348','#434348','#434348','#434348'],
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45
        }
      },
      title: {
        text: 'ESTADOS DE LOS OFICIOS'
      },
      subtitle: {
        text: 'Cantidad por estado'
      },
      credits:false,
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45
        }
      },
      series: [{
        name: 'Delivered amount',
        data:[]
        // data: [
        //   ['Pendiente', 1],
        //   ['Procesados', 3],
        //   ['En proceso de despacho', 1],
        //   ['En proceso de verificación', 6],
        //   ['Completados', 8]
        // ]
      }]
    },
    donut:false,
    errors: [],
    oficios:{!! $oficios ? $oficios : "''"!!},
    fechaDespacho:'',
    fechaDespachoFin:'',
    nombre_solicitante:'',
    materiales_id:[],
    fechasConsultadas:[],
    nombresMaterialesConsultados:'Todos',
    materialesConsultados:[],
    nombreSolicitanteConsultado:"",
    tipo_solicitante:0,
    tipoSolicitanteFiltrado:'',
    valorTipoSolicitanteFiltrado:0,
    datosTablaFiltrado:[],
    datosTablaFiltradoTemp:[],
    estatus_oficio:0,
    search:'',
    currentSort:'fecha_emision',//campo por defecto que tomara para ordenar
    currentSortDir:0,//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,

  },
  computed:{
    registros:function() {
      this.rows=0;
      return this.datosTablaFiltrado.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.datosTablaFiltrado){
          if(this.datosTablaFiltrado[i].nombre.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.datosTablaFiltrado[i]);
          }//if
        }//for(let i in this.oficios)
        if(filtrardo.length)
        this.datosTablaFiltrado=filtrardo;
        else{
          this.statusFiltro=0;
          this.datosTablaFiltrado=this.datosTablaFiltradoTemp;
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.datosTablaFiltrado=this.datosTablaFiltradoTemp;
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.datosTablaFiltrado.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      this.currentSort=s;
      if(this.currentSortDir==0)
      this.currentSortDir=-1;
      else
      this.currentSortDir=0;
    },
    buscarOficioPorFecha(){
      if(this.fechaDespacho=="" && this.fechaDespachoFin==""){
        this.errors.push("Debe seleccionar un rango de fechas válido.");
      }else if(this.fechaDespacho && this.fechaDespachoFin==""){
        this.errors.push("Debe una fecha de fin válida");
      }else if(this.fechaDespacho=="" && this.fechaDespachoFin){
        this.errors.push("Debe una fecha de inicio válida");
      }else if(process(this.fechaDespacho)>process(this.fechaDespachoFin)){
        this.errors.push("El intervalo de fechas es erróneo.");
      }
      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
        this.errors=[];
      }//if(this.errors.length>0)
      else{
        axios.post('{{ url("listado_donaciones_por_fecha") }}', {
          fechaInicio:this.fechaDespacho,
          fechaFin:this.fechaDespachoFin,
          tipoSolicitante:this.tipo_solicitante,
          materiales_id:this.materiales_id,
          nombreSolicitante:this.nombre_solicitante
        }).then(response => {

          if(response.data.error==0){
            this.oficios=response.data.oficios;
            this.datosTablaFiltrado=response.data.contenidoTabla;
            this.datosTablaFiltradoTemp=response.data.contenidoTabla;
            this.valorTipoSolicitanteFiltrado=response.data.ValortipoSolicitante;
            this.tipoSolicitanteFiltrado=response.data.tipoSolicitante;
            this.fechasConsultadas=response.data.fechasConsultadas;
            this.materialesConsultados=response.data.materialesConsultados;
            this.nombresMaterialesConsultados=response.data.nombresMaterialesConsultados;
            this.nombreSolicitanteConsultado=response.data.nombreSolicitante;
          }//if(response.data.status=="success")
          if(response.data.error==1){
            alertify.error(response.data.msg);
            this.oficios=response.data.oficios;
            this.datosTablaFiltrado=response.data.contenidoTabla;
            this.datosTablaFiltradoTemp=response.data.contenidoTabla;
            this.valorTipoSolicitanteFiltrado=response.data.ValortipoSolicitante;
            this.tipoSolicitanteFiltrado=response.data.tipoSolicitante;
            this.fechasConsultadas=response.data.fechasConsultadas;
            this.materialesConsultados=response.data.materialesConsultados;
            this.nombresMaterialesConsultados=response.data.nombresMaterialesConsultados;
            this.nombreSolicitanteConsultado=response.data.nombreSolicitante;
          }//if(response.data.status=="error")
        }).catch(error => {
          alertify.error(""+error);
        });
      }//else
    },//buscarResponsable:function()
    generarPdf(){
      if(this.oficios.length==0){
        alertify.set('notifier','position', 'top-right');
        alertify.error('No hay información para la generación del archivo pdf');
      }else{
        if(this.fechaDespacho && this.fechaDespachoFin==""){
          alertify.error("Debe una fecha de fin válida");
        }else if(this.fechaDespacho=="" && this.fechaDespachoFin){
          alertify.error("Debe una fecha de inicio válida");
        }else if(process(this.fechaDespacho)>process(this.fechaDespachoFin)){
          alertify.error("El intervalo de fechas es erróneo.");
        }else{
          var fechaDespacho = JSON.stringify(this.fechaDespacho);
          var fechaDespachoFin = JSON.stringify(this.fechaDespachoFin);
          window.open("{{ url('listado_donaciones_pdf') }}"+"?fechaDespacho="+fechaDespacho+"&fechaDespachoFin="+fechaDespachoFin, 'Download');
        }
      }//if(this.oficios.length==0)
    }//generarPdf:function()
  },//methods
  mounted(){
    var d=new Date();
    var ano= d.getFullYear();
    var mes= d.getMonth()+1;
    var dia=d.getDate();
    if(dia<10)
    dia='0'+dia;
    if(mes<10)
    mes='0'+mes;
    this.fechaDespacho=ano+'-'+mes+'-'+dia;
    this.fechaDespachoFin=ano+'-'+mes+'-'+dia;
    this.buscarOficioPorFecha();
    $('select').selectpicker();
    this.options.series[0].data=[
      ['Pendiente', 1],
      ['Procesados', 3],
      ['En proceso de despacho', 1],
      ['En proceso de verificación', 6],
      ['Completados', 8]
    ];
    // this.donut=true;
  }
});//const app= new Vue
</script>
@endpush
