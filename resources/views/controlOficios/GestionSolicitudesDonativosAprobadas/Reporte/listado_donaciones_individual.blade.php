@extends('layouts.pdf.pdf')
@section('intervaloFecha')
<br>
<div class="text-left" style="float:left">
  <strong >Período Consultado:</strong> Desde {{$fechaInicio}} Hasta {{$fechaFin}}
</div>
@endsection
@section('contenido')
<style media="screen">
  h2,h3,h4,h5{
    text-align:center;
  }
  th{
    height: 50px;
    background-color: #dee2e6;
  }
  td{
    height: 45px;
  }
  .header{
    width: 100%;
    text-align: center;
    position: fixed;
  }
  #header {
    margin: 20px!important;//Esto para que no se monte la tabla de la segunda página sobre el texto de período consultado
  }
</style>
 <h2 class="text-center">{{$titulo}}</h2>
 <table style="width:100%">
 <thead>
  <tr>
    <th>Fecha</th>
    <th>Estado</th>
    <th>Responsable</th>
    <th>Cédula</th>
    <th>Chofer</th>
    <th>Cédula del Chofer</th>
    <th>Vehículo</th>
    <th>Placa</th>
    <th>Materiales / Cantidad</th>
  </tr>
  </thead>
  <tbody>
    @foreach($oficios as $oficio)
    <tr>
      <td>{{$oficio->fecha_emision}}</td>
      <td>{{$oficio->estado_aprobacion->nombre}}</td>
      <td>{{$oficio->personal_natural->nombre}} {{$oficio->personal_natural->apellido}}</td>
      <td>{{$oficio->personal_natural->cedula}}</td>
      @if($oficio->despacho && $oficio->despacho->chofer)
      <td>{{$oficio->despacho->chofer->nombre}} {{$oficio->despacho->chofer->apellido}}</td>
      <td>{{$oficio->despacho->chofer->cedula}}</td>
      <td>{{$oficio->despacho->vehiculo->modelo}}</td>
      <td>{{$oficio->despacho->vehiculo->placa}}</td>
      @else
      <td>No aplica</td>
      <td>No aplica</td>
      <td>No aplica</td>
      <td>No aplica</td>
      @endif
      <td>
        @php
        $materiales="";
        @endphp
        @if($oficio->aprobacion_parcial_materiales!=null && count($oficio->aprobacion_parcial_materiales)>0)
          @foreach($oficio->aprobacion_parcial_materiales as $material)
            @php
            if(isset($oficio->aprobacion_parcial_materiales[$loop->index+1]))
            $materiales.=$material->material->nombre.' / '.$material->cantidad.', ';
            else
            $materiales.=$material->material->nombre.' / '.$material->cantidad;
            @endphp
          @endforeach
          @php
          //if(count($oficio->aprobacion_parcial_materiales)>2)
          //  $materiales=substr($materiales, 0, -1);
          @endphp
        @elseif($oficio->solicitud_materiales!=null && count($oficio->solicitud_materiales)>0)
          @foreach($oficio->solicitud_materiales as $material)
            @php
            if(isset($oficio->solicitud_materiales[$loop->index+1]))
            $materiales.=$material->material->nombre.' / '.$material->cantidad.', ';
            else
            $materiales.=$material->material->nombre.' / '.$material->cantidad;
            @endphp
          @endforeach
          @php
            //if(count($oficio->solicitud_materiales)>2)
            //  $materiales=substr($materiales, 0, -1);
          @endphp
        @endif
        {{$materiales}}
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
@endsection
