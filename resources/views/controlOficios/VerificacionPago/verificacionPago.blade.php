@extends('layouts.app')
@section('contenido')
<section id="verificacion">
	<div class="card" >
	<div class="card-body">
	<h3 class="text-center header-title m-t-0 m-b-20 pt-2">VERIFICACIÓN DE PAGO</h3>
	<div class="row">
		<div class="col-md-2 text-center ml-5">
			<i class="fas fa-asterisk"></i>
			<strong>Fecha inicio:</strong>
			<input type="date" class="form-control"  id="fecha_inicio" v-model="fecha_inicio"  style="text-align:center;">
		</div>
		<div class="col-md-2 text-center">
			<i class="fas fa-asterisk"></i>
			<strong>Fecha fin:</strong>
			<input type="date" class="form-control"  id="fecha_fin"  v-model="fecha_fin" style="text-align:center;">
		</div>
		<div class="col-md-2 text-center ">
			<i class="fas fa-asterisk"></i>
			<strong>Tipo de Solicitante:</strong>
			<select class="form-control" name="tipo_solicitante" v-model="tipo_solicitante" id="tipo_solicitante" @change="tipoSolicitante">
				<option value="0">Selecciona un solicitante</option>
				<option value="1">Persona Natural</option>
				<option value="2">Persona Jurídica</option>
			</select>
		</div>
		<div class="col-12 col-md-2 col-lg-2  text-center" v-if="buscar">
			<i class="fas fa-asterisk"></i>
			<strong>@{{titulo}}:</strong>
			<input type="text" class="form-control text-uppercase" v-model="solicitante" v-mask="mascara" maxlength="12">
		</div>
		<div class="col-md-1 text-center">
			<button type="button" class="btn btn-info mt-4" @click="verificarPagos" name="button">Buscar</button>
		</div>
    <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-if='pagos_registrados.length' class="mt-3">
          <div class="row">
            <div class="header mx-auto">
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">

              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <div id="no-more-tables" v-if='pagos_registrados'>
                  <table  class="table table-bordered table-condensed cf table-striped">
                    <thead class="bg-primary text-white">
                      <tr>
                        <th scope="col" style="cursor:pointer; width:5%;" class="text-center align-middle" @click="sort('id')">N° De Registro</th>
                        <th scope="col" style="cursor:pointer; width:8%;" class="text-center align-middle" @click="sort('fecha_emision')">Fecha de Solicitud</th>
                        <th scope="col" style="cursor:pointer; width:8%;" class="text-center align-middle" @click="sort('fecha_aprobacion')">Fecha de Aprobación</th>
                        <th scope="col" style="cursor:pointer; width:16%;" class="text-center align-middle"@click="sort('solicitante')">Solicitante</th>
                        <th scope="col" style="cursor:pointer; width:10%;" class="text-center align-middle"@click="sort('agente_retencion')">Agente de Retención</th>
                        <th scope="col" style="cursor:pointer; width:10%;" class="text-center align-middle"@click="sort('retencion')">Retención</th>
                        <th scope="col" style="width:20%;" class="text-center align-middle" @click="sort('modelos_solicitados')">Modelos Solicitados</th>
                        <th scope="col" style="width:10%;" class="text-center align-middle" @click="sort('cantidad_total_solicitada')">Cantidad Total Solicitada</th>
                        <th scope="col" class="text-center align-middle"  style="width:15%;">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(fila,index) in orderBy(pagos_registrados, currentSort,currentSortDir)" v-show='statusFiltro'>
                        <td class="text-center align-middle" data-title="N° De Registro">@{{fila.id}}</td>
                        <td  class="text-center align-middle" data-title="Fecha de Solicitud">@{{fila.fecha_emision}}</td>
                        <td class="text-center align-middle" data-title="Fecha de Aprobación">@{{fila.fecha_aprobacion}}</td>
                        <td  class="text-justify text-uppercase align-middle" data-title="Solicitante">@{{fila.solicitante}}</td>
                        <td class="text-center text-uppercase align-middle" data-title="Agente de Retencion">@{{fila.agente_retencion}}</td>
                        <td class="text-center text-uppercase align-middle" data-title="Retención">@{{fila.retencion}}</td>
                        <td  class="text-justify align-middle text-uppercase" data-title="Modelos Solicitados">@{{fila.modelos_solicitados}}</td>
                        <td  class="text-center align-middle" data-title="Cantidad Total Solicitada">@{{fila.cantidad_total_solicitada}}</td>
                        <td class="text-center align-middle" data-title="Acciones">
                      
                        <a v-bind:href="url +fila.id"  class="btn btn-primary text-white text-center"   title="Pdf" target="_blank"><i class="fa fa-file-pdf fa-1x" aria-hidden="true"></i></a>
                            
                        </td>
                      </tr>
                      <tr v-show='!statusFiltro'>
                        <td class="text-justify font-weight-bold" colspan="8">No se encontraron registros</td>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <div class="col-12">
                  <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{pagos_registrados.length}} </strong>
                  <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                  <div style="float:right">
                    <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                    <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                    <div class="row ml-2">
                      <strong>Página:  @{{currentPage}}</strong>
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- mx auto -->

          </div>
        </div>
      </div>
      <div class="container" v-else>
        <div class="row">
          <div class="mx-auto mt-5">
            <h5>No existen registros para la consulta seleccionada</h5>
          </div>
        </div>
      </div>

    </div>
  </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
	alertify.set('notifier','position', 'top-right');
	const appVue = new Vue({
	  el:'#verificacion',
	  data:{
      url:'{{url('verificar_pagos_pdf')}}'+'/',
      fecha_inicio:'<?php echo date("Y-m-d");?>',
      fecha_fin:'<?php echo date("Y-m-d");?>',
	    tipo_solicitante:0,
	    buscar:false,
	    titulo:'',
	    solicitante:'',
      mascara:'',
      pagos:{!! $pagos ? $pagos : "''"!!},
	    errors:[],
	    search:'',
	    seleccione:'Seleccione',//opcion por defecto
	    currentSort:'id',//campo por defecto que tomara para ordenar
	    currentSortDir:'asc',//order asc
	    pageSize:'5',//Registros por pagina
	    optionspageSize: [
	      { text: '5', value: 5 },
	      { text: '10', value: 10 },
	      { text: '25', value: 25 },
	      { text: '50', value: 50 },
	      { text: '100', value: 100 }
	    ],//Registros por pagina
	    currentPage:1,//Pagina 1
	    statusFiltro:1,
	  },
 
    computed:{

pagos_registrados:function() {

     this.rows=0;
     return this.pagos.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
           modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
           return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
           return 1 * modifier;
    return 0;
 }).filter((row, index) => {
    let start = (this.currentPage-1)*this.pageSize;
    let end = this.currentPage*this.pageSize;
    if(index >= start && index < end){
        this.rows+=1;
        return true;
    }
});
},
},
	  methods:{
     filtrar:function(){
                   let filtrardo=[];
                   if(this.search){
                     for(let i in this.pagos){
                       if(this.pagos[i].id.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.pagos[i].fecha_emision.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].fecha_aprobacion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].solicitante.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].agente_retencion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.pagos[i].modelos_solicitados.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].cantidad_total_solicitada.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                        this.statusFiltro=1;
                        filtrardo.push(this.pagos[i]);
                       }//if(this.pagos[i].id.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.pagos[i].fecha_emision.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].fecha_aprobacion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].solicitante.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].agente_retencion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.pagos[i].modelos_solicitados.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.pagos[i].cantidad_total_solicitada.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
                     }//for(let i in this.pagos)
                     if(filtrardo.length)
                      this.pagos=filtrardo;
                     else{
                        this.statusFiltro=0;
                        this.pagos={!! $pagos ? $pagos : "''"!!};
                      }// if(filtrardo.length)
                   }else{
                     this.statusFiltro=1;
                     this.pagos={!! $pagos ? $pagos : "''"!!};
                   }//if(this.search)
                 },// filtrar:function()
                 nextPage:function() {
                    if((this.currentPage*this.pageSize) < this.pagos.length) this.currentPage++;
                 },
                 prevPage:function() {
                     if(this.currentPage > 1) this.currentPage--;
                 },
                 sort:function(s) {
                  //if s == current sort, reverse
                     if(s === this.currentSort) {
                        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
                     }
                  this.currentSort = s;
                 },
	    tipoSolicitante:function(){
        this.solicitante='';
        this.mascara='';

	        if(this.tipo_solicitante==0){
	            this.buscar=0;
	            this.titulo='';
	        }//if(this.tipo_solicitante==0)
	        else if(this.tipo_solicitante==1){
	            this.buscar=1;
	            this.titulo='Cédula';
              this.mascara='########';
	        }//else if(his.tipo_solicitante==1)
	        else{
	            this.buscar=1;
	            this.titulo='Rif';
              this.mascara='A-#########';
             

	        }//else
	    },//tipoSolicitante
	    verificarPagos:function(){
       
	        var fecha_inicio=this.fecha_inicio;
	        var fecha_fin=this.fecha_fin;
	        var tipo_solicitante=this.tipo_solicitante;
	        var solicitante=this.solicitante;
	        let self = this;
	      if(fecha_inicio==""){
	        this.errors.push("Se requiere la fecha de inicio.");
	      }//if(fecha_inicio=="")
	      else if(fecha_fin==""){
	        this.errors.push("Se requiere la fecha Fin.");
	      }// else if(fecha_fin="")
	      else if(tipo_solicitante==0){
	        this.errors.push("Se requiere el tipo de solicitante.");
	      }// else if(tipo_solicitante==0)
	      else if(solicitante==""){
	        this.errors.push("Se requiere el número de cédula o rif del solicitante.");
	      }//else if(solicitante=="")
	      if(this.errors.length>0){
	        for(var i=0;i<this.errors.length;i++){
	          alertify.set('notifier','position', 'top-right');
	          alertify.error(this.errors[i]);
	        }// for(var i=0;i<this.errors.length;i++)
	        this.errors=[];
	      }//else if(this.errors.length>0)
	      else{
          if(tipo_solicitante==2){
            solicitante=solicitante.toUpperCase();
          }//if(tipo_solicitante==2)
	        axios.post('{{ route("verificar_pagos") }}', {
	          fecha_inicio:fecha_inicio,
	          fecha_fin:fecha_fin,
	          tipo_solicitante:tipo_solicitante,
	          solicitante:solicitante,
	        }).then(response => {
	          self.pagos=response.data;
	        }).catch(error => {
	        alertify.error('Error en el servidor.');
	        console.log(error);
	      });
	      }//else
	    },//verificarPagos
      frontEndDateFormat: function(date) {
        		return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
      },
	  },
   
	});//const app= new Vue
</script>
@endpush