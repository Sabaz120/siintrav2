@extends('layouts.app')
@section('contenido')
<style media="screen">
input[type="radio"] {
position: relative;
display: inline-block;
width: 30px;
height: 30px;
border-radius: 100%;
outline: none !important;
-webkit-appearance: none;
}
input[type="radio"]::before {
position: relative;
top: -1px;
left: -1px;
display: block;
content: '';
background: white;
border: 1px solid rgba(128, 128, 128, 0.4);
border-radius: 100%;
box-shadow: inset 0 0.1em 1px -0.1em rgba(0, 0, 0, 0.3);
width: 32px;
height: 32px;
}
input[type="radio"]:active::before {
box-shadow: inset 0 0.1em 1px -0.1em rgba(0, 0, 0, 0.3), inset 0 0 2px 3px rgba(0, 0, 0, 0.1);
}
input[type="radio"]:focus::before {
box-shadow: inset 0 0.1em 1px -0.1em rgba(0, 0, 0, 0.3), 0 0 0 2px rgba(30, 144, 255, 0.5);
}
input[type="radio"]:checked::before {
background: #a5d3ff;
border-color: dodgerblue;
}
input[type="radio"]:disabled::before {
cursor: not-allowed;
background-color: #eaeaea;
border-color: rgba(128, 128, 128, 0.2);
}



input[type="radio"]::after {
position: relative;
top: -17px;
left: 15px;
display: block;
content: '';
background: dodgerblue;
border-radius: 100%;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
width: 0;
height: 0;
}
input[type="radio"]:checked::after {
transition: all ease-in-out 100ms 0;
top: -27px;
left: 7px;
width: 18px;
height: 18px;
}
input[type="radio"]:disabled::after {
background: #7cb5ec;
}


input + input {
margin-left: .5em;
}

</style>
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 pt-5">REGISTRO DE PERSONAL JURÍDICO</h5>
      <div class="row justify-content-justify">
        <div class="col-12 col-md-3 col-lg-3">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="cedula"><strong>Clasificación de ente:</strong></label>
            <select class="form-control text-uppercase" v-on:change="cargarEntes(clasificacion_ente_id)" name="clasificacion_ente" v-model="clasificacion_ente_id" id="clasificacion_ente_id">
              <option value="0">Selecciona una clasificación de ente</option>
              <option v-for="clasificacion in clasificacion_entes" v-bind:value="clasificacion.id" class="text-uppercase">
                @{{ clasificacion.nombre }}
              </option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2" v-show="entes.length>0">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="nombre"><strong>Ente:</strong></label>
            <select class="form-control text-uppercase" v-model="ente_id" name="ente_id">
              <option value="0">Seleccione un ente</option>
              <option v-for="ente in entes" v-bind:value="ente.id" class="text-uppercase">
                @{{ ente.nombre }}
              </option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="apellido"><strong>R.I.F / C.I. SITUR:</strong></label>
            <input type="text" name="" class="form-control text-uppercase" v-model="rif_empresa" v-on:keyup="keyrif" v-mask="'A-###########'" >
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="apellido"><strong>Nombre Completo de la Institución:</strong></label>
            <input type="text" name="" class="form-control text-uppercase" v-model="nombre_empresa" minlength="3" maxlength="200"  value="">
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <!-- <i class="fas fa-asterisk"></i> -->
            <label for="apellido"><strong>Nombre Completo de la Máxima Autoridad:</strong></label>
            <input type="text" name="" class="form-control text-uppercase" v-model="nombre_autoridad"  maxlength="200"  value="">
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <!-- <i class="fas fa-asterisk"></i> -->
            <label for="apellido"><strong>Dirección Fiscal:</strong></label>
            <input type="text" name="" class="form-control text-uppercase" v-model="direccion_fiscal" minlength="3" maxlength="200" value="">
          </div>
        </div>
        <div class="">
          <div class="form-group text-center ml-5">
            <i class="fas fa-asterisk"></i>
            <label for="apellido"><strong>Agente de Retención:</strong></label><br>
            <label><input name="q1" type="radio" v-model="agente_retencion" :value="true"/> Si</label>
            <label class="ml-5"><input name="q1" type="radio"/ v-model="agente_retencion" :value="false"> No</label>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
        <button type="button" class="btn btn-success" v-on:click="registrar">Registrar</button>
        <button type="button" class="btn btn-warning" name="button" data-toggle="modal" data-target="#limpiarForm">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show='personalJuridico.length'>
          <div class="row">
            <div class="header mx-auto">
              <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE PERSONAL JURÍDICO</h4>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2 text-uppercase">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <table  class="table table-bordered">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th scope="col" style="cursor:pointer;" class="text-center text-uppercase" @click="sort('nombre_empresa')" >Nombre de la Empresa</th>
                      <th scope="col" style="cursor:pointer;" class="text-center text-uppercase" @click="sort('nombre_autoridad')" >Nombre de la Máxima Autoridad</th>
                      <th scope="col" style="cursor:pointer;" class="text-center text-uppercase" @click="sort('ente')">Ente</th>
                      <th scope="col" class="text-center width:33%;">Acción</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="(personal,index) in personal_juridico_registrado" v-show='statusFiltro'>
                      <td class="text-center text-uppercase">
                        @{{personal.nombre_empresa}}
                      </td>
                      <td  class="text-center text-uppercase">@{{personal.nombre_autoridad}}</td>
                      <td class="text-center text-uppercase">
                        @{{personal.ente}}
                      </td>
                      <td class="text-center">
                        <button type="button" class="btn btn-info text-white text-center" data-toggle="modal" data-target="#myModalDetalle" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Visualizar" @click="verPersonalJuridico(personal.rif_empresa)"><i class="fas fa-eye fa-1x" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="verPersonalJuridico(personal.rif_empresa)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                        <!-- <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="empresa_id=personal.id"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button> -->
                      </td>
                    </tr>
                    <tr v-show='!statusFiltro'>
                      <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                    </td>
                  </tbody>
                </table>
                <div id="footerTable">
                  <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{personalJuridico.length}}</strong>
                  <strong class="pt-5" v-show='!statusFiltro'>Mostrando 0 registros de 0 </strong>
                  <div style="float:right">
                    <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                    <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                    <div class="row ml-2">
                      <strong>Página:  @{{currentPage}}</strong>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="myModalDetalle">
      <div class="modal-dialog modal-lg" style="max-width:80%;">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">Detalle de Empresa</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body justify-content-center text-center">
            <div class="row">
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <label for="firmaPersonal"><strong>Ente</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase"  v-model="empresa.ente_nombre" readonly name="" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <label for="firmaPersonal"><strong>R.I.F. / C.I. SITUR:</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase"  v-model="empresa.rif_empresa" readonly name="" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <label for="firmaPersonal"><strong>Nombre Completo de la Institución:</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase"  v-model="empresa.nombreEmpresa" readonly name="" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <label for="firmaPersonal"><strong>Nombre de la Máxima Autoridad:</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase"  v-model="empresa.nombre_autoridad" readonly name="" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <label for="firmaPersonal"><strong>Dirección Fiscal:</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase"  v-model="empresa.direccion_fiscal" readonly name="" value="">
                </div>
              </div>

              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <label for="firmaPersonal"><strong>Agente de Retención:</strong></label><br>
                  <label><input name="q1" type="radio" v-model="empresa.agente_retencion" :value="true" disabled/> Si</label>
                  <label class="ml-5"><input name="q1" type="radio"/ v-model="empresa.agente_retencion" :value="false" disabled> No</label>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg" style="max-width:80%;">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">Edición de Empresa </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body justify-content-center text-center">
            <div class="row">
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for="cedula"><strong>Clasificación de ente:</strong></label>
                  <select class="form-control text-uppercase" v-on:change="cargarEntes(empresa.clasificacion_ente_id)" name="clasificacion_ente" v-model="empresa.clasificacion_ente_id" id="clasificacion_ente_id">
                    <option value="0">Selecciona una clasificación de ente</option>
                    <option v-for="clasificacion in clasificacion_entes" v-bind:value="clasificacion.id" class="text-uppercase">
                      @{{ clasificacion.nombre }}
                    </option>
                  </select>
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4" v-show="entes.length>0">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for="nombre"><strong>Ente:</strong></label>
                  <select class="form-control text-uppercase" v-model="empresa.ente_id" name="ente_id">
                    <!-- <option value="0">Seleccione un ente</option> -->
                    <option v-for="ente in entes" v-bind:value="ente.id" class="text-uppercase">
                      @{{ ente.nombre }}
                    </option>
                  </select>
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for=""><strong>R.I.F. / C.I. SITUR:</strong></label>
                  <input type="text" name="" class="form-control text-uppercase" v-model="empresa.rif_empresa" v-on:keyup="keyrifedit" v-mask="'A-###########'" maxlength="12" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for=""><strong>Nombre Completo de la Institución:</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase" maxlength="200"  v-model="empresa.nombreEmpresa" name="" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for=""><strong>Nombre de la Máxima Autoridad:</strong></label>

                  <input type="text" style="text-align:center;" class="form-control text-uppercase" maxlength="200" v-model="empresa.nombre_autoridad" name="" value="">
                </div>
              </div>
              <div class="col-12 col-md-2 col-lg-4">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for=""><strong>Dirección Fiscal:</strong></label>
                  <input type="text" style="text-align:center;" class="form-control text-uppercase" maxlength="200"  v-model="empresa.direccion_fiscal" name="" value="">
                </div>
              </div>

              <div class="col-12">
                <div class="form-group text-center ml-5">
                  <i class="fas fa-asterisk"></i>
                  <label for="apellido"><strong>Agente de Retención:</strong></label><br>
                  <label><input name="q2" type="radio" v-model="empresa.agente_retencion" :value="true"/> Si</label>
                  <label class="ml-5"><input name="q2" type="radio"/ v-model="empresa.agente_retencion" :value="false"> No</label>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" data-dismiss="modal" @click="actualizarPersonalJuridico()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('controlOficios.recepcionOficio.partials.modal_limpiar')

  <!-- <div class="modal" id="myModalEliminar">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header bg-primary">
  <h5 class="modal-title mx-auto  text-white">
  ELIMINAR PERSONAL NATURAL</h4>
  <button type="button" class="close" data-dismiss="modal" >&times;</button>
</div>
<div class="modal-body">
<div class="mx-auto text-center">
¿Esta seguro de eliminar este registro?
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminar()">Sí, estoy seguro.</button>
<button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
</div>
</div>
</div>
</div> -->
</section>

@endsection
@push('scripts')
<script>
alertify.set('notifier','position', 'top-right');
const app= new Vue({
  el:'#register',
  data:{
    personalJuridico:{!! $personalJuridico ? $personalJuridico : "''"!!},
    clasificacion_entes:{!! $clasificacion_entes ? $clasificacion_entes : "''"!!},
    entes:[],
    clasificacion_ente_id:0,
    ente_id:0,
    rif_empresa:'',
    nombre_empresa:'',
    nombre_autoridad:'',
    direccion_fiscal:'',
    empresa_id:0,
    agente_retencion:false,
    agente_retencion_editar:false,
    empresa:{
      id:0,
      empresa_id:0,
      clasificacion_ente_id:0,
      ente_id:0,
      ente_nombre:'',
      nombreEmpresa:'',
      nombre_autoridad:'',
      direccion_fiscal:'',
      rif_empresa:''
    },
    search:'',
    rows:0,
    seleccione:'Seleccione',//opcion por defecto
    currentSort:'nombre_empresa',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1
  },
  computed:{
    personal_juridico_registrado:function() {
      this.rows=0;
      return this.personalJuridico.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    validarNoNumeros(string){
      var filtro = '0123456789';//Caracteres validos
      for (var i=0; i<string.length; i++){
        if (filtro.indexOf(string.charAt(i)) != -1){
          return true;
        }
      }//for
      return false;
    },
    keyrif(event){
      // console.log(event.key);
      // console.log(this.rif_empresa.length);
      if(this.rif_empresa.length<=2){
        if(event.key=="j"){
          this.rif_empresa="J-";
        }else if(event.key=="g"){
          this.rif_empresa="G-";
        }else if(event.key=="c"){
          this.rif_empresa="C-";
        }else if(event.key=="C"){
          this.rif_empresa="C-";
        }else if(event.key=="J"){
          this.rif_empresa="J-";
        }else if(event.key=="G"){
          this.rif_empresa="G-";
        }else if (event.key=="V" || event.key =="v") {
          this.rif_empresa="V-";
        }else{
          this.rif_empresa="";
        }
      }//lengt<1
    },
    keyrifedit(event){
      // console.log(event.key);
      // console.log(this.empresa.rif_empresa.length);
      if(this.empresa.rif_empresa.length<=2){
        if(event.key=="j"){
          this.empresa.rif_empresa="J-";
        }else if(event.key=="c"){
          this.empresa.rif_empresa="C-";
        }else if(event.key=="C"){
          this.empresa.rif_empresa="C-";
        }else if(event.key=="g"){
          this.empresa.rif_empresa="G-";
        }else if(event.key=="J"){
          this.empresa.rif_empresa="J-";
        }else if(event.key=="G"){
          this.empresa.rif_empresa="G-";
        }else if (event.key=="V" || event.key =="v") {
          this.rif_empresa="V-";
        }else{
          this.empresa.rif_empresa="";
        }
      }//lengt<=2
    },
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.personalJuridico){
          if(this.personalJuridico[i].nombre_autoridad != null){
              if(this.personalJuridico[i].nombre_empresa.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.personalJuridico[i].nombre_autoridad.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.personalJuridico[i].rif_empresa.toString().trim().search(this.search.toLowerCase())!=-1 || this.personalJuridico[i].ente.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                this.statusFiltro=1;
                filtrardo.push(this.personalJuridico[i]);
              }//if(this.PersonalNatural[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.PersonalNatural[i].ponderacion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
          }
        }//for(let i in this.PersonalNatural)
        if(filtrardo.length)
        this.personalJuridico=filtrardo;
        else{
          this.statusFiltro=0;
          this.personalJuridico={!! $personalJuridico ? $personalJuridico : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.personalJuridico={!! $personalJuridico ? $personalJuridico : "''"!!};
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.personalJuridico.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
    registrar(){
      if(this.clasificacion_ente_id==0){
        alertify.error('Debes seleccionar una clasificación de ente');
      }else if(this.ente_id==0){
        alertify.error('Debes seleccionar un ente');
      }else if(this.rif_empresa==""){
        alertify.error('Debe ingresar un RIF');
      }else if(this.nombre_empresa==""){
        alertify.error('Debe ingresar un nombre de institución');
      // }else if(this.validarNoNumeros(this.nombre_empresa)){
      //   alertify.error('El nombre de la empresa no puede contener números');
      }else if(this.nombre_empresa.length<5){
        alertify.error('El nombre de la institución no puede ser menor a 5 caracteres');
      // }else if(this.nombre_autoridad==""){
      //   alertify.error('Debe ingresar el nombre completo de la máxima autoridad');
      // }else if(this.validarNoNumeros(this.nombre_autoridad)){
      //   alertify.error('El nombre de la máxima autoridad no puede contener números');
      // }else if(this.nombre_autoridad.length<5){
      //   alertify.error('El nombre de la máxima autoridad no puede ser menor a 5 caracteres');
      // }else if(this.direccion_fiscal==""){
      //   alertify.error('Debe ingresar la dirección fiscal');
      // }else if(this.direccion_fiscal.length<5){
      //   alertify.error('La dirección fiscal no puede ser menor a 5 caracteres');
      } else{
        //registrar
        axios.post('{{ url("registrar_personal_juridico") }}', {
          ente_id:this.ente_id,
          rif_empresa: this.rif_empresa,
          nombre_empresa: this.nombre_empresa,
          nombre_autoridad:this.nombre_autoridad,
          direccion_fiscal:this.direccion_fiscal,
          agente_retencion:this.agente_retencion,
        })
        .then(response => {
          if(response.data.error==0){
            this.PersonalNatural=response.data.PersonalNatural;
            this.personalJuridico=response.data.personalJuridico;
            this.limpiar();
            alertify.success('Registro Satisfactorio.');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }//else
    },
    limpiar(){
      //console.log('entre limpiar');
      this.entes=[],
      this.clasificacion_ente_id=0,
      this.ente_id=0,
      this.rif_empresa='',
      this.nombre_empresa='',
      this.nombre_autoridad='',
      this.direccion_fiscal='',
      this.empresa.empresa_id=0,
      this.empresa.id=0,
      this.empresa.clasificacion_ente_id=0,
      this.empresa.ente_id=0,
      this.empresa.ente_nombre='',
      this.empresa.nombreEmpresa='',
      this.empresa.nombre_autoridad='',
      this.empresa.direccion_fiscal='',
      this.empresa.rif_empresa=''
      this.empresa.agente_retencion=''

    },
    verPersonalJuridico:function(rif_empresa){
      axios.post('{{ route("api.controloficio.datos_personal_juridico_sin_responsable") }}', {
        rif:rif_empresa,
      }).then(response=> {
        if(response.data.error==0){
          this.empresa.id=response.data.empresa.id;
          this.empresa.nombreEmpresa=response.data.empresa.nombre_institucion;
          this.empresa.empresa_id=response.data.empresa.id;
          this.empresa.ente_id=response.data.empresa.ente_id;
          this.empresa.clasificacion_ente_id=response.data.empresa.ente.clasificacion_ente_id;
          this.empresa.ente_nombre=response.data.empresa.ente.nombre;
          this.empresa.nombre_autoridad=response.data.empresa.nombre_autoridad;
          this.empresa.direccion_fiscal=response.data.empresa.direccion_fiscal;
          this.empresa.rif_empresa=response.data.empresa.rif_cedula_situr;
          this.empresa.agente_retencion=response.data.empresa.agente_retencion;
          this.cargarEntes(response.data.empresa.ente.clasificacion_ente_id);
        }//error==0
      })
      .catch(function (error) {
        alertify.error('Error en el servidor');
        console.log(error);
      });
    },
    actualizarPersonalJuridico(){
      if(this.empresa.clasificacion_ente_id==0){
        alertify.error('Debes seleccionar una clasificación de ente');
      }else if(this.empresa.ente_id==0){
        alertify.error('Debes seleccionar un ente');
      }else if(this.empresa.rif_empresa==""){
        alertify.error('Debe ingresar un RIF');
      }else if(this.empresa.nombreEmpresa==""){
        alertify.error('Debe ingresar un nombre de institución');
      // }else if(this.validarNoNumeros(this.empresa.nombreEmpresa)){
      //   alertify.error('El nombre de la institución no puede contener números');
      }else if(this.empresa.nombreEmpresa.length<5){
        alertify.error('El nombre de la institución no puede ser menor a 5 caracteres');
      // }else if(this.empresa.nombre_autoridad==""){
      //   alertify.error('Debe ingresar el nombre completo de la máxima autoridad');
      // }else if(this.validarNoNumeros(this.empresa.nombre_autoridad)){
      //   alertify.error('El nombre de la máxima autoridad no puede contener números');
      // }else if(this.empresa.nombre_autoridad.length<5){
      //   alertify.error('El nombre de la máxima autoridad no puede ser menor a 5 caracteres');
      // }else if(this.empresa.direccion_fiscal==""){
      //   alertify.error('Debe ingresar la dirección fiscal');
      // }else if(this.empresa.direccion_fiscal.length<5){
      //   alertify.error('La dirección fiscal no puede ser menor a 5 caracteres');
      }else{
        axios.post('{{ url("modificar_personal_juridico") }}', {
          empresa:this.empresa,
        }).then(response=> {
          // console.log(response.data);
          if(response.data.error==0){
            this.personalJuridico=response.data.personalJuridico;
            this.limpiar();
            alertify.set('notifier','position', 'top-right');
            alertify.success('Actualización Satisfactoria.');
          }//error==0
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          alertify.error('Error en el servidor');
          console.log(error);
        });
      }
    },
    cargarEntes(clasificacion_ente_id){
      this.ente_id=0;
      this.entes=[];
      axios.post('{{ route("api.controloficio.obtenerentes") }}', {clasificacion_ente_id:clasificacion_ente_id}).then(response => {
        if(response.data.error==0){
          this.entes=response.data.entes
        }else if(response.data.error==1){
          // this.limpiar();
          this.clasificacion_ente_id=0;
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        this.clasificacion_ente_id=0;
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    }
  },//methods
});//const app= new Vue
</script>
@endpush
