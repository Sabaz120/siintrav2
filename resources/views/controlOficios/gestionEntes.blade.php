@extends('layouts.app')
@section('contenido')
<section id="register">
<div class="card" >
   <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE ENTES</h5>
      <div class="row justify-content-justify">
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="clasificacionEnte">Clasificación de Ente:</label>
               <select v-model="data.clasificacionEnte" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': clasificacionEnteRequerido }">
                           <option value="0">Seleccione una clasificación de ente</option>
                           <option v-for="option in ClasificacionEnte" v-bind:value="option.id">
                             @{{ option.nombre }}
                           </option>
                         </select>
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="nombre">Nombre:</label>
               <input id="nombre" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nombreRequerido }" minlength="3" maxlength="50"  type="text" v-model="data.nombre"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese nombre"required >
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
               <label for="descripcion">Descripción:</label>
               <input id="descripcion" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': descripcionRequerido }" minlength="3" maxlength="200"  type="text" v-model="data.descripcion" onkeypress="return soloLetras(event)"  pattern="[a-zA-Z]*" placeholder="Ingrese descripción" required>
            </div>
         </div>
         <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="nivelAtencion">Nivel de Atención:</label>
               <select v-model="data.nivelAtencion" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nivelAtencionRequerido }">
                           <option v-for="option in data.niveles" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
         <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
         <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
         <div id="tabla" v-show='Entes.length'>
            <div class="row">
               <div class="header mx-auto">
                  <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE ENTES</h4>
               </div>
            </div>
            <div class="row">
               <div class="container-fluid">
                  <div  class="mx-auto">
                    <div class="row">
                      <div class="col-12 col-md-9 col-lg-9">
                      <br>
                      <div class="mt-2 form-inline font-weight-bold">
                        Mostrar
                        <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2 text-uppercase">
                           <option v-for="option in optionspageSize" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
                         registros
                         </div>
                      </div>
                      <div class="col-12 col-md-3 col-lg-3">

                        <div class="form-group">
                         <label for="Buscar" class="font-weight-bold">Buscar:</label>
                         <input id="search" class="form-control form-control-sm text-uppercase"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                       </div>
                      </div>
                    </div>
                     <table  class="table table-bordered">
                        <thead class="bg-primary text-white">
                           <tr>
                              <th scope="col" style="cursor:pointer; width:33%;" class="text-center text-uppercase" @click="sort('nombre')">Ente</th>
                              <th scope="col" style="cursor:pointer; width:33%;" class="text-center text-uppercase" @click="sort('ponderacion')">Niveles de Atención</th>
                              <th scope="col" class="text-center width:33%;">Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr v-for="(entes,index) in entes_registradas" v-show='statusFiltro'>
                              <td class="text-justify text-uppercase"><p class="anchoparrafo">@{{entes.nombre}}</p></td>
                              <td  class="text-center text-uppercase">@{{entes.ponderacion}}</td>
                              <td class="text-center align-middle">
                              <button type="button" class="btn btn-info text-white text-center" data-toggle="modal" data-target="#myModalDetalle" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Detalle" @click="capturarData(entes)"><i class="fas fa-eye fa-1x" aria-hidden="true"></i></button>
                                 <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarData(entes)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                                 <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="datoBorrar(entes.id)"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button>
                              </td>
                           </tr>
                           <tr v-show='!statusFiltro'>
                              <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="">
                        <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Entes.length}}</strong>
                        <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                        <div style="float:right">
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                           <div class="row ml-2">
                              <strong>Página:  @{{currentPage}}</strong>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
 <!-- The Modal Detalle -->
 <div class="modal" id="myModalDetalle">
      <div class="modal-dialog modal-lg" style="max-width:80%;">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">DETALLES DE ENTES</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">

              <div class="row">
              <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">

               <label for="clasificacionEnte">Clasificación de Ente:</label>
               <select v-model="clasificacionEnte" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': clasificacionEnteRequerido }" disabled>
                           <option value="0">Seleccione una clasificación de ente</option>
                           <option v-for="option in ClasificacionEnte" v-bind:value="option.id">
                             @{{ option.nombre }}
                           </option>
                         </select>
            </div>
         </div>
                <div class="col-12 col-md-4 col-lg-4">
                <div class="form-group text-center">

               <label for="nombre">Nombre:</label>
               <input class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nombreRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" placeholder="Ingrese nombre" v-model="nombre" required disabled>
            </div>
                </div>
                <div class="col-12 col-md-4 col-lg-4">
                <div class="form-group text-center">
               <label for="descripcion">Descripción:</label>
               <input  class="form-control text-uppercase" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="200"  type="text" placeholder="Ingrese descripción" v-model="descripcion" required disabled>
            </div>
                </div>
                <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">

               <label for="nivelAtencion">Nivel de Atención:</label>
               <select v-model="nivelAtencion" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nivelAtencionRequerido }" disabled>
                           <option v-for="option in data.niveles" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
            </div>
         </div>
              </div>
          </div>
          <!-- Modal footers -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>

        </div>
      </div>
    </div>
 <!-- The Modal Editar -->
 <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg" style="max-width:80%;">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">EDICIÓN DE ENTES</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">

              <div class="row">
              <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="clasificacionEnte">Clasificación de Ente:</label>
               <select v-model="clasificacionEnte" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': clasificacionEnteRequerido }">
                           <option value="0">Seleccione una clasificación de ente</option>
                           <option v-for="option in ClasificacionEnte" v-bind:value="option.id">
                             @{{ option.nombre }}
                           </option>
                         </select>
            </div>
         </div>
                <div class="col-12 col-md-4 col-lg-4">
                <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="nombre">Nombre:</label>
               <input class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nombreRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" placeholder="Ingrese nombre" v-model="nombre" required>
            </div>
                </div>
                <div class="col-12 col-md-4 col-lg-4">
                <div class="form-group text-center">
               <label for="descripcion">Descripción:</label>
               <input  class="form-control text-uppercase" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="200"  type="text" placeholder="Ingrese descripción" v-model="descripcion" required>
            </div>
                </div>
                <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="nivelAtencion">Nivel de Atención:</label>
               <select v-model="nivelAtencion" class="form-control text-uppercase" v-bind:class="{ 'is-invalid': nivelAtencionRequerido }">
                           <option v-for="option in data.niveles" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
            </div>
         </div>
              </div>
          </div>
          <!-- Modal footers -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="editarMotivo()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>

        </div>
      </div>
    </div>

<!-- The Modal Eliminar -->
<div class="modal" id="myModalEliminar">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header bg-primary">
            <h5 class="modal-title mx-auto  text-white">
            ELIMINAR MOTIVO</h4>
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <div class="mx-auto text-center">
               ¿Esta seguro de eliminar este registro?
            </div>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminarEnte()">Sí, estoy seguro.</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </div>
</div>
</section>

@endsection
@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              errors: [],
              data:{
                  clasificacionEnte:0,
                  nombre:'',
                  descripcion:'',
                  nivelAtencion:0,//Registros por pagina
                  niveles: [
                   { text: 'Seleccione una nivel de atención', value: 0 },
                   { text: '1', value: 1 },
                   { text: '2', value: 2 },
                   { text: '3', value: 3 },
                   { text: '4', value: 4 },
                   { text: '5', value: 5 },
                   { text: '6', value: 6 },
                   { text: '7', value: 7 },
                   { text: '8', value: 8 },
                   { text: '9', value: 9 },
                   { text: '10', value: 10 },
                   { text: '11', value: 11 },
                   { text: '12', value: 12 },
                   { text: '13', value: 13 },
                   { text: '14', value: 14 },
                   { text: '15', value: 15 },
                  ],//Niveles de Atención
              },

              id:'',
              clasificacionEnte:0,
              nombre:'',
              descripcion:'',
              nivelAtencion:0,
              nombreRequerido:false,
              descripcionRequerido:false,
              nivelAtencionRequerido:false,
              clasificacionEnteRequerido:false,
              Entes:{!! $Entes ? $Entes : "''"!!},
              ClasificacionEnte:{!! $ClasificacionEnte ? $ClasificacionEnte : "''"!!},
              search:'',
              seleccione:'Seleccione',//opcion por defecto
              currentSort:'nombre',//campo por defecto que tomara para ordenar
              currentSortDir:'asc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,

        },


        computed:{

                entes_registradas:function() {
                     this.rows=0;
                     return this.Entes.sort((a,b) => {
                        let modifier = 1;
                        if(this.currentSortDir === 'desc')
                           modifier = -1;
                        if(a[this.currentSort] < b[this.currentSort])
                           return -1 * modifier;
                        if(a[this.currentSort] > b[this.currentSort])
                           return 1 * modifier;
                    return 0;
                 }).filter((row, index) => {
                    let start = (this.currentPage-1)*this.pageSize;
                    let end = this.currentPage*this.pageSize;
                    if(index >= start && index < end){
                        this.rows+=1;
                        return true;
                    }
                });
                },
        },
        methods:{
                 filtrar:function(){
                   let filtrardo=[];
                   if(this.search){
                     for(let i in this.Entes){
                       if(this.Entes[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Entes[i].ponderacion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                        this.statusFiltro=1;
                        filtrardo.push(this.Entes[i]);
                       }//if(this.Entes[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Entes[i].ponderacion.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
                     }//for(let i in this.Entes)
                     if(filtrardo.length)
                      this.Entes=filtrardo;
                     else{
                        this.statusFiltro=0;
                        this.Entes={!! $Entes ? $Entes : "''"!!};
                      }// if(filtrardo.length)
                   }else{
                     this.statusFiltro=1;
                     this.Entes={!! $Entes ? $Entes : "''"!!};
                   }//if(this.search)
                 },// filtrar:function()

                 limpiar:function(){
                     this.error=[];
                     this.data={
                  clasificacionEnte:0,
                  nombre:'',
                  descripcion:'',
                  nivelAtencion:0,//Registros por pagina
                  niveles: [
                   { text: 'Seleccione una nivel de atención', value: 0 },
                   { text: '1', value: 1 },
                   { text: '2', value: 2 },
                   { text: '3', value: 3 },
                   { text: '4', value: 4 },
                   { text: '5', value: 5 },
                   { text: '6', value: 6 },
                   { text: '7', value: 7 },
                   { text: '8', value: 8 },
                   { text: '9', value: 9 },
                   { text: '10', value: 10 },
                   { text: '11', value: 11 },
                   { text: '12', value: 12 },
                   { text: '13', value: 13 },
                   { text: '14', value: 14 },
                   { text: '15', value: 15 },
                  ],//Niveles de Atención
                  };
                  this.id='';
                  this.clasificacionEnte=0;
                  this.nombre='';
                  this.descripcion='';
                  this.nivelAtencion=0;
                  this.nombreRequerido=false;
                  this.descripcionRequerido=false;
                  this.nivelAtencionRequerido=false;
                  this.clasificacionEnteRequerido=false;
                  this.search='';
                  this.statusFiltro=1;
                 },//limpiar:function()

                 buscar:function(){
                     for (let i in this.Entes){
                         if(this.Entes[i].nombre==this.data.nombre){
                            return(1);
                         }//if(this.Entes[i].nombre==this.data.nombre)
                     }//for (let i in this.Entes)
                     return(0);
                 },//buscar:function()

                 registrar:function(){
                     let descripcionM;
                     this.errors=[];
                     this.nombreRequerido=false;
                     this.descripcionRequerido=false;
                     this.nivelAtencionRequerido=false;
                     this.clasificacionEnteRequerido=false;


                     if (!this.data.clasificacionEnte) {
                         this.errors.push("Existen campos por llenar:<br>*Clasificación de Ente.");
                         this.clasificacionEnteRequerido=true;
                     }//if (!this.data.clasificacionEnte)

                     if (!this.data.nombre) {
                         this.errors.push("Existen campos por llenar:<br>*Nombre.");
                         this.nombreRequerido=true;
                     } else if (this.buscar()==1) {
                         this.errors.push('No se puede duplicar el campo nombre.');
                         this.nombreRequerido=true;
                     } else if (this.data.nombre.length<3 || this.data.nombre>50) {
                         this.errors.push('El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
                         this.nombreRequerido=true;
                     }// if (!this.data.nombre)

                    /* if (!this.data.descripcion) {
                         this.errors.push("Existen campos por llenar:<br>*Descripción");
                         this.descripcionRequerido=true;
                     } else if (this.data.descripcion.length<3 || this.data.descripcion>200) {
                         this.descripcionRequerido=true;
                         this.errors.push('El campo descripcion acepta un minimo de 3 caracteres y un máximo de 200 caracteres.');
                     }// if (!this.data.descripcion)*/

                     if (this.data.nivelAtencion==0) {
                         this.errors.push("Existen campos por llenar:<br>*Nivel de Atención.");
                         this.nivelAtencionRequerido=true;
                     }//if (!this.data.nivelAtencion)

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       /*Envio de datos a la base de datos*/
                       let self = this;
                       let clasificacion_ente_id=this.data.clasificacionEnte;
                       let nombre=this.data.nombre.toLowerCase();
                       if (!this.data.descripcion) {
                          descripcionM=null;
                       }else{
                           descripcionM=this.data.descripcion.toLowerCase();
                       }//if (!this.data.descripcion) 
                      let ponderacion=this.data.nivelAtencion;

                       axios.post('{{ url("registrar_entes") }}', {
                        clasificacion_ente_id:clasificacion_ente_id,
                        nombre: nombre,
                        descripcion: descripcionM,
                        ponderacion:ponderacion,
                       })
                       .then(function (response) {
                         if(response.data.status=="success"){
                            self.Entes=response.data.Entes;
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success('Registro Satisfactorio.');
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            alertify.error(response.data.mensaje);
                         }//if(response.data.status=="error")

                       })
                       .catch(function (error) {
                         console.log(error);
                       });
                       /*Fin envio de datos a la base de datos*/


                     }//if(this.error.length)
                 },
                 capturarData: function(ente){
                    this.id=ente.id;
                    this.clasificacionEnte=ente.clasificacion_ente_id;
                    this.nombre=ente.nombre;
                    this.descripcion=ente.descripcion;
                    this.nivelAtencion=ente.ponderacion;
                    this.nombreRequerido=false;
                    this.descripcionRequerido=false;
                    this.nivelAtencionRequerido=false;
                    this.clasificacionEnteRequerido=false;

                 },
                 editarMotivo:function (){
                    let self = this;
                    let nombre=this.nombre.toLowerCase();
                    let descripcionM;
                    this.errors=[];
                    this.nombreRequerido=false;
                    this.descripcionRequerido=false;
                    this.nivelAtencionRequerido=false;
                    this.clasificacionEnteRequerido=false;


                    if (this.clasificacionEnte==0) {
                         this.errors.push("Existen campos por llenar:<br>*Clasificación de Ente.");
                         this.clasificacionEnteRequerido=true;
                     }//if (!this.clasificacionEnte)

                     if (!nombre) {
                         this.errors.push("Existen campos por llenar:<br>*Nombre.");
                         this.nombreRequerido=true;
                     } else if (nombre.length<3 || nombre>50) {
                      this.nombreRequerido=true;
                         this.errors.push('El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
                     }// if (!nombre)

                     /*if (!descripcion) {
                         this.errors.push("Existen campos por llenar:<br>*Descripción");
                         this.descripcionRequerido=true;
                     } else if (descripcion.length<3 || descripcion>200) {
                         this.descripcionRequerido=true;
                         this.errors.push('El campo descripcion acepta un minimo de 3 caracteres y un máximo de 200 caracteres.');
                     }// if (!descripcion)*/

                     if (this.nivelAtencion==0) {
                         this.errors.push("Existen campos por llenar:<br>*Nivel de Atención.");
                         this.nivelAtencionRequerido=true;
                     }//if (!this.nivelAtencion)

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                      if (!this.descripcion) {
                            descripcionM=null;
                         }else{
                         descripcionM=this.descripcion.toLowerCase();
                         }//if if (!this.descripcion)
                       axios.post('{{ url("modificar_entes") }}', {clasificacion_ente_id:this.clasificacionEnte,nombre:nombre,descripcion:descripcionM,id:this.id,ponderacion:this.nivelAtencion}).then(response => {
                        if(response.data.status=="success"){
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModal').modal('toggle');
                            self.Entes=response.data.Entes;
                        }//if(response.data.status=="success")
                        if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                        }//if(response.data.status=="error")
                       }).catch(error => {
                        alertify.error(""+error);
                       });
                     }//if(this.errors.length>0)
                 },
                 datoBorrar:function(id){
                    this.id=id;
                 },
                 eliminarEnte: function(){
                    let self = this;
                      axios.post('{{ url("eliminar_entes") }}', {id:this.id}).then(response => {
                        if(response.data.status=="success"){
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModalEliminar').modal('toggle');
                            self.Entes=response.data.Entes;
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.error(response.data.mensaje);
                         }//if(response.data.status=="error")
                      }).catch(error => {
                         console.log(error);
                      });
                 },
                 nextPage:function() {
                    if((this.currentPage*this.pageSize) < this.Entes.length) this.currentPage++;
                 },
                 prevPage:function() {
                     if(this.currentPage > 1) this.currentPage--;
                 },
                 sort:function(s) {
                  //if s == current sort, reverse
                     if(s === this.currentSort) {
                        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
                     }
                  this.currentSort = s;
                 }

        },//methods
   });//const app= new Vue
</script>
@endpush
