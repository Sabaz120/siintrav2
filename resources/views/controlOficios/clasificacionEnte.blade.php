@extends('layouts.app')
@section('contenido')
<section id="register">
<div class="card" >
   <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE CLASIFICACIÓN DE ENTE</h5>
      <div class="row justify-content-center">
         <div class="col-12 col-md-6 col-lg-6">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="nombre">Nombre:</label>
               <input id="nombre" class="form-control" v-bind:class="{ 'is-invalid': nombreRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="data.nombre"  placeholder="Ingrese nombre" required>
            </div>
         </div>
         <div class="col-12 col-md-6 col-lg-6">
            <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="descripcion">Descripción:</label>
               <input id="descripcion" class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)" minlength="3" maxlength="200"  type="text" v-model="data.descripcion"  placeholder="Ingrese descripción" required>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
         <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
         <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
         <div id="tabla" v-show='ClasificacionEnte.length'>
            <div class="row">
               <div class="header mx-auto">
                  <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO TIPO ENTES</h4>
               </div>
            </div>
            <div class="row">
               <div class="container-fluid">
                  <div  class="mx-auto">
                    <div class="row">
                      <div class="col-12 col-md-9 col-lg-9">
                      <br>
                      <div class="mt-2 form-inline font-weight-bold">
                        Mostrar
                        <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                           <option v-for="option in optionspageSize" v-bind:value="option.value">
                             @{{ option.text }}
                           </option>
                         </select>
                         registros
                         </div>
                      </div>
                      <div class="col-12 col-md-3 col-lg-3">

                        <div class="form-group">
                         <label for="Buscar" class="font-weight-bold">Buscar:</label>
                         <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                       </div>
                      </div>
                    </div>
                     <table  class="table table-bordered">
                        <thead class="bg-primary text-white">
                           <tr>
                              <th scope="col" style="cursor:pointer; width:33%;" class="text-center" @click="sort('nombre')">Ente</th>
                              <th scope="col" style="cursor:pointer; width:33%;" class="text-center" @click="sort('descripcion')">Descripción</th>
                              <th scope="col" class="text-center width:33%;">Acciones</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr v-for="(clasificaciones,index) in clasificaciones_registradas" v-show='statusFiltro'>
                              <td class="text-justify"><p class="anchoparrafo">@{{clasificaciones.nombre}}</p></td>
                              <td  class="text-justify"><p class="anchoparrafo">@{{clasificaciones.descripcion}}</p></td>
                              <td class="text-center align-middle">
                                 <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarData(clasificaciones)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                                 <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="datoBorrar(clasificaciones.id)"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button>
                              </td>
                           </tr>
                           <tr v-show='!statusFiltro'>
                              <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="">
                        <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{ClasificacionEnte.length}}</strong>
                        <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                        <div style="float:right">
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                           <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                           <div class="row ml-2">
                              <strong>Página:  @{{currentPage}}</strong>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

 <!-- The Modal Editar -->
 <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary">
            <h4 class="modal-title mx-auto  text-white">EDICIÓN DE CLASIFICACIÓN DE ENTE</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">

              <div class="row">
                <div class="col">
                <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="nombre">Nombre:</label>
               <input class="form-control" v-bind:class="{ 'is-invalid': nombreRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="nombre" placeholder="Ingrese nombre" required>
            </div>
                </div>
                <div class="col">
                <div class="form-group text-center">
               <i class="fas fa-asterisk"></i>
               <label for="descripcion">Descripción:</label>
               <input  class="form-control" v-bind:class="{ 'is-invalid': descripcionRequerido }" onkeypress="return soloLetras(event)"  minlength="3" maxlength="200"  type="text" v-model="descripcion" placeholder="Ingrese descripción" required>
            </div>
                </div>

              </div>
          </div>
          <!-- Modal footers -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="editarClasificacion()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>

        </div>
      </div>
    </div>

<!-- The Modal Eliminar -->
<div class="modal" id="myModalEliminar">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header bg-primary">
            <h5 class="modal-title mx-auto  text-white">
            ELIMINAR CLASIFICACIÓN</h4>
            <button type="button" class="close" data-dismiss="modal" >&times;</button>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <div class="mx-auto text-center">
               ¿Esta seguro de eliminar este registro?
            </div>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminarClasificacion()">Sí, estoy seguro.</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </div>
</div>
</section>

@endsection

@push('scripts')
<script>
   const app= new Vue({
        el:'#register',
        data:{
              errors: [],
              data:{
                  nombre:'',
                  descripcion:'',
              },
              id:'',
              nombre:'',
              descripcion:'',
              nombreRequerido:false,
              descripcionRequerido:false,
              ClasificacionEnte:{!! $ClasificacionEnte ? $ClasificacionEnte : "''"!!},
              search:'',
              currentSort:'ente',//campo por defecto que tomara para ordenar
              currentSortDir:'asc',//order asc
              pageSize:'5',//Registros por pagina
              optionspageSize: [
               { text: '5', value: 5 },
               { text: '10', value: 10 },
               { text: '25', value: 25 },
               { text: '50', value: 50 },
               { text: '100', value: 100 }
              ],//Registros por pagina
              currentPage:1,//Pagina 1
              statusFiltro:1,

        },
        computed:{

                clasificaciones_registradas:function() {
                     this.rows=0;
                     return this.ClasificacionEnte.sort((a,b) => {
                        let modifier = 1;
                        if(this.currentSortDir === 'desc')
                           modifier = -1;
                        if(a[this.currentSort] < b[this.currentSort])
                           return -1 * modifier;
                        if(a[this.currentSort] > b[this.currentSort])
                           return 1 * modifier;
                    return 0;
                 }).filter((row, index) => {
                    let start = (this.currentPage-1)*this.pageSize;
                    let end = this.currentPage*this.pageSize;
                    if(index >= start && index < end){
                        this.rows+=1;
                        return true;
                    }
                });
                },
        },
        methods:{
                 filtrar:function(){
                   let filtrardo=[];
                   if(this.search){

                     for(let i in this.ClasificacionEnte){
                       if(this.ClasificacionEnte[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.ClasificacionEnte[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
                        this.statusFiltro=1;
                        filtrardo.push(this.ClasificacionEnte[i]);
                       }//if(this.ClasificacionEnte[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.ClasificacionEnte[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
                     }//for(let i in this.ClasificacionEnte)

                     if(filtrardo.length)
                      this.ClasificacionEnte=filtrardo;
                     else{
                      this.statusFiltro=0;
                      this.ClasificacionEnte={!! $ClasificacionEnte ? $ClasificacionEnte : "''"!!};
                     }//if(filtrardo.length)

                   }else{
                     this.statusFiltro=1;
                     this.ClasificacionEnte={!! $ClasificacionEnte ? $ClasificacionEnte : "''"!!};
                   }//if(this.search)
                 },//filtrar:function()

                 limpiar:function(){
                     this.error=[];
                     this.data={
                      nombre:'',
                      descripcion:'',
                    };
                    this.nombreRequerido=false;
                    this.descripcionRequerido=false;
                 },//limpiar:function()

                 buscar:function(){
                     for (let i in this.ClasificacionEnte){
                         if(this.ClasificacionEnte[i].nombre==this.data.nombre){
                            return(1);
                         }//if(this.ClasificacionEnte[i].nombre==this.data.nombre)
                     }//for (let i in this.ClasificacionEnte)
                     return(0);
                 },//buscar:function()

                 registrar:function(){
                     this.errors=[];
                     this.nombreRequerido=false;
                     this.descripcionRequerido=false;
                     if (!this.data.nombre) {
                         this.errors.push("Existen campos por llenar:<br>*Nombre.");
                         this.nombreRequerido=true;
                     } else if (this.buscar()==1) {
                         this.errors.push('No se puede duplicar el campo nombre.');
                         this.nombreRequerido=true;

                     } else if (this.data.nombre.length<3 || this.data.nombre>50) {
                         this.errors.push('El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
                     }// if (!this.data.nombre)

                      if (!this.data.descripcion) {
                         this.errors.push("Existen campos por llenar:<br>*Descripción.");
                         this.descripcionRequerido=true;
                     } else if (this.data.descripcion.length<3 || this.data.descripcion>200) {
                         this.descripcionRequerido=true;
                         this.errors.push('El campo descripcion acepta un minimo de 3 caracteres y un máximo de 200 caracteres.');
                     }// if (!this.data.descripcion)

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       /*Envio de datos a la base de datos*/
                       let self = this;
                       let nombre=this.data.nombre.toLowerCase();
                       nombre=nombre.charAt(0).toUpperCase()+nombre.slice(1);
                       let descripcion=this.data.descripcion.toLowerCase();
                       descripcion=descripcion.charAt(0).toUpperCase()+descripcion.slice(1);
                       axios.post('{{ url("registrar_clasificacion_ente") }}', {
                        nombre: nombre,
                        descripcion: descripcion,
                       })
                       .then(function (response) {
                         if(response.data.status=="success"){
                            self.ClasificacionEnte=response.data.ClasificacionEnte;
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success('Registro Satisfactorio.');
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                         }//if(response.data.status=="error")

                       })
                       .catch(function (error) {
                         console.log(error);
                       });
                       /*Fin envio de datos a la base de datos*/


                     }//if(this.error.length)
                 },
                 capturarData: function(clasificacion){
                    this.id=clasificacion.id;
                    this.nombre=clasificacion.nombre;
                    this.descripcion=clasificacion.descripcion;
                    this.nombreRequerido=false;
                    this.descripcionRequerido=false;
                 },
                 editarClasificacion:function (){
                    let self = this;
                    let nombre=this.nombre.toLowerCase();
                    nombre=nombre.charAt(0).toUpperCase()+nombre.slice(1);
                    let descripcion=this.descripcion.toLowerCase();
                    descripcion=descripcion.charAt(0).toUpperCase()+descripcion.slice(1);
                    this.errors=[];
                    this.nombreRequerido=false;
                    this.descripcionRequerido=false;
                     if (!nombre) {
                         this.errors.push("Existen campos por llenar:<br>*Nombre.");
                         this.nombreRequerido=true;
                     } else if (nombre.length<3 || nombre>50) {
                         this.errors.push('El campo nombre acepta un minimo de 3 caracteres y un máximo de 50 caracteres.');
                     }// if (!nombre)

                      if (!descripcion) {
                         this.errors.push("Existen campos por llenar:<br>*Descripción");
                         this.descripcionRequerido=true;
                     } else if (descripcion.length<3 || descripcion>200) {
                         this.errors.push('El campo descripcion acepta un minimo de 3 caracteres y un máximo de 200 caracteres.');
                     }// if (!descripcion)

                     if(this.errors.length>0){
                       for(var i=0;i<this.errors.length;i++){
                        alertify.set('notifier','position', 'top-right');
                        alertify.error(this.errors[i]);
                       }// for(var i=0;i<this.errors.length;i++)
                     }else{
                       axios.post('{{ url("modificar_clasificacion_ente") }}', {nombre:nombre,descripcion:descripcion,id:this.id}).then(response => {
                        if(response.data.status=="success"){
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModal').modal('toggle');
                            self.ClasificacionEnte=response.data.ClasificacionEnte;
                        }//if(response.data.status=="success")
                        if(response.data.status=="error"){
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                        }//if(response.data.status=="error")
                       }).catch(error => {
                        alertify.error(""+error);
                       });
                     }// if(this.errors.length>0)
                 },
                 datoBorrar:function(id){
                    this.id=id;
                 },
                 eliminarClasificacion: function(){
                    let self = this;
                      axios.post('{{ url("eliminar_clasificacion_ente") }}', {id:this.id}).then(response => {
                        if(response.data.status=="success"){
                            self.limpiar();
                            alertify.set('notifier','position', 'top-right');
                            alertify.success(response.data.mensaje);
                            $('#myModalEliminar').modal('toggle');
                            self.ClasificacionEnte=response.data.ClasificacionEnte;
                         }//if(response.data.status=="success")
                         if(response.data.status=="error"){
                            self.limpiar();
                            $('#myModalEliminar').modal('toggle');
                            alertify.set('notifier','position', 'top-right');
                            $.each(response.data.mensaje, function( key, value ) {
                                alertify.error(""+value);
                            });
                         }//if(response.data.status=="error")
                      }).catch(error => {
                         console.log(error);
                      });
                 },
                 nextPage:function() {
                    if((this.currentPage*this.pageSize) < this.ClasificacionEnte.length) this.currentPage++;
                 },
                 prevPage:function() {
                     if(this.currentPage > 1) this.currentPage--;
                 },
                 sort:function(s) {
                  //if s == current sort, reverse
                     if(s === this.currentSort) {
                        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
                     }
                  this.currentSort = s;
                 }

        },//methods
   });//const app= new Vue
</script>
@endpush
