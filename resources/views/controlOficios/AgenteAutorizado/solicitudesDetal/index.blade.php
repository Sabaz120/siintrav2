@extends('layouts.app')
@section('contenido')

<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">Gestión de Solicitudes</h5>

      <div class="row justify-content-center">

        <div class="col-12 col-md-6 col-lg-6" >
          <div class="form-group text-center">
            <strong>Fecha Solicitud: </strong>
            <input type="text" readonly class="form-control" value="{{date('d-m-Y')}}">
          </div>
        </div>

        <!-- <div class="col-12 col-md-3 col-lg-3" >
        <div class="form-group text-center">
        <strong>Código Solicitud:</strong>
        <input type="text" readonly class="form-control" value="">
      </div>
    </div> -->

    <div class="col-12 col-md-6 col-lg-6" >
      <div class="form-group text-center">
        <strong>Solicitante:</strong>
        <input type="text" readonly class="form-control text-capitalize" value="{{Auth::user()->name}} {{Auth::user()->last_name}}">
      </div>
    </div>

    <div class="col-12 col-md-6 col-lg-6" >
      <div class="form-group text-center">
        <strong>Cliente: </strong>
        <input type="text" readonly class="form-control text-capitalize" value="VENEZOLANA DE TELECOMUNICACIONES C.A. VTELCA">
      </div>
    </div>

    <div class="col-12 col-md-6 col-lg-6" >
      <div class="form-group text-center">
        <strong>Responsable Jurídico: </strong>
        <input type="text" readonly class="form-control text-capitalize" :value="pn.nombre">
        {{--
        <input type="text" readonly class="form-control text-capitalize" :value="pn.nombre+' '+pn.apellido">
        --}}
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-12 text-center">
      <h4>Asociar equipos a solicitud</h4>
    </div>

    <div class="col-12 col-md-3">
      <div class="form-group text-center">
        <strong>Modelo: </strong>
        <select class="form-control" v-model="form.productIndex" v-on:click="cargarModalidad()">
          <option value="99999">Seleccione un modelo</option>
          <option :value="indexP" v-for="(product,indexP) in productos">@{{product.descripcion}}</option>
        </select>
      </div>
    </div>

    <div class="col-12 col-md-3">
      <div class="form-group text-center">
        <strong>Modalidad: </strong>
        <select class="form-control" v-model="form.modalitiesIndex" v-on:click="ajustarDisponibilidad()">
          <option value="99999">Seleccione una modalidad</option>
          <option :value="indexM" v-for="(modalitie,indexM) in form.modalities">@{{modalitie.modalidad}}</option>
        </select>
      </div>
    </div>
    <div class="col-12 col-md-3">
      <div class="form-group text-center">
        <strong>Disponible en almacén: </strong>
        <input type="number" class="form-control" :value="form.disponibility" readonly>
      </div>
    </div>

    <div class="col-12 col-md-3">
      <div class="form-group text-center">
        <strong>Cantidad a Solicitar: </strong>
        <input type="number" class="form-control" v-model="form.quantity" :max="form.disponibility" >
      </div>
    </div>

    <div class="col-12 col-md-3">
      <div class="form-group text-center">
        <button type="button" class="btn btn-success" name="button" @click="addProduct()">Agregar</button>
      </div>
    </div>

    <div class="col-12 col-md-12 col-lg-12 text-center my-4">
      <h4>Lista de equipos asociados</h4>
    </div>

    <div class="col-12 col-md-12 col-lg-12 text-center">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td>Modelo</td>
            <td>Modalidad</td>
            <td>Cantidad</td>
          </tr>
        </thead>
        <tbody>
          <tr  v-for="(prod,indexProd) in form.products">
            <td>@{{prod.modelo}}</td>
            <td>@{{prod.modalidad}}</td>
            <td>@{{prod.cantidad}}</td>
          </tr>
          <tr v-if="form.products.length==0">
            <td colspan="3">No hay productos asociados</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-12">
      <div class="form-group text-center">
        <button type="button" class="btn btn-success" name="button" @click="save()">Guardar</button>
        <button type="button" class="btn btn-warning" name="button" @click="clearVars()">Limpiar</button>
      </div>
    </div>


  </div>

</div>
</div>

</section>
@endsection
@push('scripts')
<script>

const app= new Vue({
  el:'#register',
  data:{
    pn:{!! $pn ? $pn : "''"!!},
    Solicitudes:{!! $solicitudes ? $solicitudes : "''"!!},
    productosSistProd:{!! $productosSistProd ? json_encode($productosSistProd) : "''"!!},
    productos:{!! $productos ? json_encode($productos) : "''"!!},
    indexProducto:0,
    indexOficio:0,
    iva:16,
    total:0,
    search:'',
    form:{
      productIndex:'99999',
      modalitiesIndex:'99999',
      quantity:0,
      modalities:[],
      disponibility:0,
      products:[

      ]
    }


  },
  methods:{
    addProduct(){
      if(this.form.productIndex!='99999' && this.form.modalitiesIndex!='99999'){
        if(this.form.quantity==0 || this.form.quantity==""){
          alertify.error("Debes ingresar la cantidad a solicitar");
        }else if(this.form.quantity>this.form.disponibility){
          alertify.error("No puedes seleccionar una cantidad mayor a la disponible");
        }else{
          var b=0;
          for(var i=0;i<this.form.products.length;i++){
            if(this.form.products[i].product_id==this.productos[this.form.productIndex].id){
              b=1;
              break;
            }
          }//for
          if(b==1){
            alertify.error("Ya has agregado este producto a la lista.");
          }else{
            this.form.products.push({
              product_id:this.productos[this.form.productIndex].id,
              nombre:this.productos[this.form.productIndex].nombre,
              modelo:this.productos[this.form.productIndex].descripcion,
              modalidad:this.productosSistProd[this.string_to_slug(this.productos[this.form.productIndex].nombre)][this.form.modalitiesIndex].modalidad,
              cantidad:this.form.quantity
            });
            this.form.modalitiesIndex='99999';
            this.form.productIndex='99999';
            this.form.quantity=0;
            this.form.disponibility=0;
            this.form.modalities=[];
          }
        }
      }else{
        alertify.error("Debes seleccionar un modelo y modalidad");
      }

    },
    ajustarDisponibilidad(){
      if(this.form.modalitiesIndex!='99999'){
        let disponibility=this.productosSistProd[this.string_to_slug(this.productos[this.form.productIndex].nombre)][this.form.modalitiesIndex].disponibilidad;
        this.form.disponibility=disponibility;
      }else{
        alertify.error("Debes seleccionar una modalidad");
      }
    },
    cargarModalidad(){
      if(this.form.productIndex!='99999'){
        this.form.modalitiesIndex='99999';
        this.form.modalities=[];
        var modalities=this.productosSistProd[this.string_to_slug(this.productos[this.form.productIndex].nombre)];
        this.form.modalities=modalities;
        this.form.quantity=0;
      }else{
        alertify.warning("Debes seleccionar un modelo");
      }
    },

    string_to_slug(str) {

      //replace all special characters | symbols with a space
      str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();

      // trim spaces at start and end of string
      str = str.replace(/^\s+|\s+$/gm,'');

      // replace space with dash/hyphen
      str = str.replace(/\s+/g, '-');
      return str;
    },
    save(){
      if(this.form.products.length==0){
        alertify.error("Debes asociar al menos un equipo, para procesar la solicitud.");
      }else{
        axios.post('{{ route("api.agente.autorizado.solicitudes.store") }}', {
          products:this.form.products,
        }).then(response => {
          alertify.success(response.data.msg);
          this.clearVars();
        }).catch(error => {
          this.clearVars();
          alertify.error('Error en el servidor.');
          console.log(error);
        });

      }
    },
    clearVars(){
      this.form.modalitiesIndex='99999';
      this.form.productIndex='99999';
      this.form.quantity=0;
      this.form.disponibility=0;
      this.form.modalities=[];
      this.form.products=[];
    }

  },//methods

});//const app= new Vue
</script>
@endpush
