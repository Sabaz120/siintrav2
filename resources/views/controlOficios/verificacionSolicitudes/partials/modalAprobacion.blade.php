<!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#aprobarOficio">Open Modal</button> -->
<!-- Modal -->
<div class="modal fade" id="aprobarOficio" role="dialog" v-if="oficios.personalNatural.length>0 || oficios.personalJuridico.length>0">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mx-auto text-white">Productos</h4>
      </div>
      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead class="bg-primary text-white">
                  <th>Modelo</th>
                  <th>Modalidad</th>
                  <th>Cantidad</th>
                  <th>Cantidad Almacen</th>
                  <th>Acción</th>
                </thead>
                <tbody v-if="indexOficioTipo=='natural' && oficios.personalNatural.length>0">
                  <tr v-for="(producto,indexProducto) in oficios.personalNatural[indexOficio].detalle_productos">
                    <td data-title="Modelo">
                      <select class="form-control" v-model="producto.producto_id" name="" v-on:change="consultarDisponibilidad(indexOficio,indexProducto)">
                        <option v-for="productoSistema in productos" v-bind:value="productoSistema.id">@{{productoSistema.descripcion}}</option>
                      </select>
                    </td>
                    <td>
                      <select class="form-control" v-model="producto.modalidad_id" v-on:change="actualizarDisponibilidad(indexOficio,indexProducto)">
                        <option v-for="modalidad in producto.modalidades" v-bind:value="modalidad.modalidad">@{{modalidad.modalidad}}</option>
                      </select>
                    </td>
                    <td data-title="Cantidad">
                      <input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="producto.cantidad" v-bind:max="producto.disponibilidad">
                    </td>
                    <td data-title="Cantidad Almacen" class="text-dark text-center" style="font-size:16px;">@{{producto.disponibilidad}}</td>
                    <td><button type="button" title="Eliminar" v-show="oficios.personalNatural[indexOficio].detalle_productos.length>1" @click="oficios.personalNatural[indexOficio].detalle_productos.splice(indexProducto,1)" class="btn btn-danger" name="button"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr >
                    <!-- <td colspan="5" class="text-center"><button type="button" @click="oficios.personalNatural[indexOficio].detalle_productos.push({'cantidad':0,'control_oficio_registro_oficio_id':oficios.personalNatural[indexOficio].oficio_id,'created_at':'2018-10-18 08:50:40','disponibilidad':0,'id':0,'producto_id':1,'updated_at':'2018-10-18 08:50:40','producto':[]})" class="btn btn-success" name="button">Agregar<i class="fa fa-plus"></i></button></td> -->
                    <td colspan="5" class="text-center"><button type="button" @click="agregarProducto()" class="btn btn-success" name="button">Agregar<i class="fa fa-plus"></i></button></td>
                  </tr>
                </tbody>
                <tbody v-if="indexOficioTipo=='juridico' && oficios.personalJuridico.length>0">
                  <tr v-for="(producto,indexProducto) in oficios.personalJuridico[indexOficio].detalle_productos">
                    <td data-title="Modelo">
                      <select class="form-control" v-model="producto.producto_id" name="" v-on:change="consultarDisponibilidad(indexOficio,indexProducto)">
                        <option v-for="productoSistema in productos" v-bind:value="productoSistema.id">@{{productoSistema.descripcion}}</option>
                      </select>
                    </td>
                    <td>
                      <select class="form-control" v-model="producto.modalidad_id" v-on:change="actualizarDisponibilidad(indexOficio,indexProducto)">
                        <option v-for="modalidad in producto.modalidades" v-bind:value="modalidad.modalidad">@{{modalidad.modalidad}}</option>
                      </select>
                    </td>
                    <td data-title="Cantidad">
                      <input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="producto.cantidad" v-bind:max="producto.disponibilidad">
                    </td>
                    <td data-title="Cantidad Almacen" class="text-dark text-center" style="font-size:16px;">@{{producto.disponibilidad}}</td>
                    <!-- <td v-else>Si</td> -->
                    <td><button type="button" title="Eliminar" v-show="oficios.personalJuridico[indexOficio].detalle_productos.length>1" @click="oficios.personalJuridico[indexOficio].detalle_productos.splice(indexProducto,1)" class="btn btn-danger" name="button"><i class="fa fa-trash"></i></button></td>



                  </tr>
                  <tr>
                    <td colspan="5" class="text-center"><button type="button" @click="agregarProducto()" class="btn btn-success" name="button">Agregar<i class="fa fa-plus"></i></button></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <table class="table table-bordered">
            <tr v-if="oficios.personalJuridico.length>0 ">
              <td class="font-weight-bold">
                <strong>
                  <br>
                  Verificación de Pago:
                </strong>
              </td>
              <td  >
                <label v-if=" oficios.personalJuridico[indexOficio].agente_retencion ===false">
                  <label><input name="q1" type="radio" v-model="oficios.personalJuridico[indexOficio].verificacion_pago" :value="true" /> Si</label>
                  <label ><input name="q1" type="radio"/ v-model="oficios.personalJuridico[indexOficio].verificacion_pago" :value="false" > No</label>
                </label>
                <label v-else>
                  <label><input name="q1" type="radio" v-model="oficios.personalJuridico[indexOficio].verificacion_pago" disabled :value="true" /> Si</label>
                  <label ><input name="q1" type="radio"/ v-model="oficios.personalJuridico[indexOficio].verificacion_pago" disabled :value="false" > No</label>
                </label>
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">
                <strong>
                  <br>
                  Plan de ventas:
                </strong>
              </td>
              <td>
                <input type="date" class="form-control" v-model="plan_venta" min="{{date('Y-m-d')}}">
              </td>
            </tr>
          </table>


        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-success" @click="aprobarOficio()" data-dismiss="modal">Enviar.</button> -->
        <button title="Aprobar" type="button"  @click="aprobarOficio(indexOficio)" name="button" class="btn btn-success mr-4">
          <i class="fa fa-check"></i>
        </button>
        <button type="button" data-dismiss="modal" class="btn btn-danger mr-4"
         name="button" data-toggle="modal" title="Rechazar" data-target="#rechazarOficio"
         @click="capturarIndexOficio(indexOficio)"><i class="fa fa-close"></i>
        </button>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> -->
      </div>
    </div>
  </div>
</div>




{{--                       Modal aprobacion antiguo                               --}}
{{--<div class="modal fade" id="aprobarOficio" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title mx-auto text-white">Aprobación</h4>
      </div>
      <div class="modal-body text-center">
        <strong>Aprobación Total</strong> <input type="checkbox" v-model="aprobacion" name="" id="total" value="total">
        <br>
        <strong>Aprobación Parcial</strong> <input type="checkbox" v-model="aprobacion" name="" id="parcial" value="parcial">
        <br>
        <div class="row" v-if="aprobacion=='parcial'">
          <div class="col-md-12 text-center">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead class="bg-primary text-white">
                  <th>Solicitante</th>
                  <th>Modelo</th>
                  <th>Cantidad de Solicitud</th>
                  <th>Cantidad en Almacen</th>
                  <th>Acción</th>
                </thead>
                <tbody v-if="indexOficioTipo=='natural'">
                  <tr v-for="(producto,index) in oficios.personalNatural[indexOficio].detalle_productos">
                    <td>@{{indexSolicitante}}</td>
                    <td>
                      <select class="form-control" v-model="producto.producto_id" name="" v-on:change="consultarDisponibilidad(index)">
                        <option v-for="productoSistema in productos" v-bind:value="productoSistema.id">@{{productoSistema.nombre}}</option>
                      </select>
                      <td><input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="producto.cantidad" v-bind:max="producto.disponibilidad"></td>
                      <td>@{{producto.disponibilidad}}</td>
                      <td><button type="button" title="Eliminar" v-show="oficios.personalNatural[indexOficio].detalle_productos.length>1" @click="oficios.personalNatural[indexOficio].detalle_productos.splice(index,1)" class="btn btn-danger" name="button"><i class="fa fa-trash"></i></button></td>
                    </tr>
                    <tr >
                      <td colspan="5" class="text-center"><button type="button" @click="oficios.personalNatural[indexOficio].detalle_productos.push({'cantidad':0,'control_oficio_registro_oficio_id':oficios.personalNatural[indexOficio].oficio_id,'created_at':'2018-10-18 08:50:40','disponibilidad':0,'id':0,'producto_id':1,'updated_at':'2018-10-18 08:50:40','producto':[]})" class="btn btn-success" name="button">Agregar<i class="fa fa-plus"></i></button></td>
                    </tr>

                  </tbody>
                  <tbody v-if="indexOficioTipo=='juridico'">

                    <tr v-for="(producto,index) in oficios.personalJuridico[indexOficio].detalle_productos">
                      <td>@{{indexSolicitante}}</td>
                      <td>
                        <select class="form-control" v-model="producto.producto_id" name="" v-on:change="consultarDisponibilidad(index)">
                          <option v-for="productoSistema in productos" v-bind:value="productoSistema.id">@{{productoSistema.nombre}}</option>
                        </select>
                        <td><input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="producto.cantidad" v-bind:max="producto.disponibilidad"></td>
                        <td>@{{producto.disponibilidad}}</td>
                        <td><button type="button" title="Eliminar" v-show="oficios.personalJuridico[indexOficio].detalle_productos.length>1" @click="oficios.personalJuridico[indexOficio].detalle_productos.splice(index,1)" class="btn btn-danger" name="button"><i class="fa fa-trash"></i></button></td>
                      </tr>
                      <tr>
                        <td colspan="5" class="text-center"><button type="button" @click="oficios.personalJuridico[indexOficio].detalle_productos.push({'cantidad':0,'control_oficio_registro_oficio_id':oficios.personalJuridico[indexOficio].oficio_id,'created_at':'2018-10-18 08:50:40','disponibilidad':0,'id':0,'producto_id':1,'updated_at':'2018-10-18 08:50:40','producto':[]})" class="btn btn-success" name="button">Agregar<i class="fa fa-plus"></i></button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" @click="aprobarOficio()" data-dismiss="modal">Enviar.</button>
            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> -->
          </div>
        </div>
      </div>
    </div>--}}
