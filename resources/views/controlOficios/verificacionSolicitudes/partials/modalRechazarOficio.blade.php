<!-- Trigger the modal with a button -->
 <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#rechazarOficio">Open Modal</button> -->
 <!-- Modal -->
 <div class="modal fade" id="rechazarOficio" role="dialog">
   <div class="modal-dialog modal-md">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header bg-primary">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
         <h4 class="modal-title mx-auto text-white">Rechazar Oficio</h4>
       </div>
       <div class="modal-body text-center">
         <p>¿Estás seguro de rechazar este oficio?</p>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-success" @click="rechazarOficio()" data-dismiss="modal">Sí, estoy seguro.</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>
