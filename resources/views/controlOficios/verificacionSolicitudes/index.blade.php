@extends('layouts.app')
@section('contenido')
<style media="screen">
input[type="radio"] {
position: relative;
display: inline-block;
width: 30px;
height: 30px;
border-radius: 100%;
outline: none !important;
-webkit-appearance: none;
}
input[type="radio"]::before {
position: relative;
top: -1px;
left: -1px;
display: block;
content: '';
background: white;
border: 1px solid rgba(128, 128, 128, 0.4);
border-radius: 100%;
box-shadow: inset 0 0.1em 1px -0.1em rgba(0, 0, 0, 0.3);
width: 32px;
height: 32px;
}
input[type="radio"]:active::before {
box-shadow: inset 0 0.1em 1px -0.1em rgba(0, 0, 0, 0.3), inset 0 0 2px 3px rgba(0, 0, 0, 0.1);
}
input[type="radio"]:focus::before {
box-shadow: inset 0 0.1em 1px -0.1em rgba(0, 0, 0, 0.3), 0 0 0 2px rgba(30, 144, 255, 0.5);
}
input[type="radio"]:checked::before {
background: #a5d3ff;
border-color: dodgerblue;
}
input[type="radio"]:disabled::before {
cursor: not-allowed;
background-color: #eaeaea;
border-color: rgba(128, 128, 128, 0.2);
}



input[type="radio"]::after {
position: relative;
top: -17px;
left: 15px;
display: block;
content: '';
background: dodgerblue;
border-radius: 100%;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
width: 0;
height: 0;
}
input[type="radio"]:checked::after {
transition: all ease-in-out 100ms 0;
top: -27px;
left: 7px;
width: 18px;
height: 18px;
}
input[type="radio"]:disabled::after {
background: #7cb5ec;
}


input + input {
margin-left: .5em;
}

</style>
<link rel="stylesheet" href="{{ URL::asset('css/tabpanel.css') }}">
<section id="gestion">
  @include('controlOficios.verificacionSolicitudes.partials.modalAprobacion')
  @include('controlOficios.verificacionSolicitudes.partials.modalRechazarOficio')
  <div class="card" >
    <div class="card-header text-white">
      <ul class="tablist" role="tablist">
        <li class="tab" role="tab"><a href="#panel1">Personal natural</a></li>
        <li class="tab" role="tab"><a href="#panel2">Personal juridica</a></li>
        <li class="tab-menu">
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
        </li>
      </ul>

      <div class="tabpanel" id="panel1" role="tabpanel" @click="indexOficioTipo='natural'">
        <section class="container-fluid">
          <!-- Filtros de búsqueda -->
          <div class="row p-2" >
            <div class="col-md-2 text-center">
              <strong class="text-dark font-weight-bold">Fecha inicio:</strong>
              <input type="date" class="form-control" v-model="fecha_inicio">
            </div>
            <div class="col-md-2 text-center">
              <strong class="text-dark font-weight-bold">Fecha fin:</strong>
              <input type="date" class="form-control" v-model="fecha_fin">
            </div>
            <div class="col-md-3 text-center">
              <strong class="text-dark font-weight-bold">Atenciones:</strong>
              <select class="form-control" v-model="num_atenciones">
                <option value="">Seleccione un número de atención</option>
                <option value="0">Ninguna</option>
                <option value="1y5">Entre 1 y 5</option>
                <option value="M5">Mayor a 5</option>
              </select>
            </div>
            <!-- <div class="col-md-3 text-center">
              <strong class="text-dark font-weight-bold">Clasificación del Ente:</strong>
              <select class="form-control"  v-on:change="cargarEntes()" v-model="clasificacion_ente_id">
                <option value="0">Seleccione una clasificación</option>
                <option v-for="clasificacion in clasificacion_entes" v-bind:value="clasificacion.id">@{{clasificacion.nombre}}</option>
              </select>
            </div>
            <div class="col-md-3 text-center"  v-show="entes.length>0">
              <strong class="text-dark font-weight-bold">Ente:</strong>
              <select class="form-control" v-model="ente_id">
                <option value="0">Seleccione un ente</option>
                <option v-for="ente in entes" v-bind:value="ente.id">@{{ente.nombre}}</option>
              </select>
            </div> -->
            <div class="col-md-2 text-center">
              <strong class="text-dark font-weight-bold">Estado:</strong>
              <select class="form-control" v-model="estado_id">
                <option value="0">Seleccione un estado</option>
                <option v-for="estado in estados" v-bind:value="estado.idEstado">@{{estado.estado}}</option>
              </select>
            </div>
            <div class="col-md-2 text-center">
              <strong class="text-dark font-weight-bold">Solicitante:</strong>
              <input type="text" class="form-control" v-model="solicitante" name="" value="">
            </div>
            <div class="col-md-1 text-center align-self-center">
              <br>
              <button type="button" class="btn btn-info" @click="obtenerOficiosConFiltro()" name="button">Buscar</button>
            </div>
          </div>
          <!-- Filtros de búsqueda -->
          <div class="row active-with-click" v-if="oficios.personalNatural.length>0">
            <div class="col-md-4 col-sm-6 col-xs-12" v-for="(oficio,index) in oficios.personalNatural" v-if="oficio.detalle_productos.length>0" >
              <article class="material-card Blue text-capitalize">
                <h2>
                  <span> @{{oficio.solicitante}}</span>
                  <strong>
                    <!-- <i class="fa fa-fw fa-star"></i>
                    The Deer Hunter -->
                  </strong>
                </h2>
                <div class="mc-content">
                  <div class="img-container " style="background-color:#85C1E9">
                    <img id="cart" class="img-responsive" src="{{asset('images/cart.png')}}">
                  </div>
                  <div id="no-more-tables" class="mc-description">
                    <table class="table table-bordered cf">
                      <thead class="cf">
                        <tr class="bg-primary">
                          <th>Modelo</th>
                          <th>Modalidad</th>
                          <th>Cantidad</th>
                          <th>Cantidad Almacen</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody >
{{--                        <tr v-for="(producto,indexProducto) in oficio.detalle_productos">
                          <td data-title="Modelo">
                            <select class="form-control" v-model="producto.producto_id" name="" v-on:change="consultarDisponibilidad(index,indexProducto)">
                              <option v-for="productoSistema in productos" v-bind:value="productoSistema.id">@{{productoSistema.descripcion}}</option>
                            </select>
                          </td>
                          <td>
                            <select class="form-control" v-model="producto.modalidad_id" v-on:change="actualizarDisponibilidad(index,indexProducto)">
                              <option v-for="modalidad in producto.modalidades" v-bind:value="modalidad.modalidad">@{{modalidad.modalidad}}</option>
                            </select>
                          </td>
                          <td data-title="Cantidad">
                            <input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="producto.cantidad" v-bind:max="producto.disponibilidad">
                          </td>
                          <td data-title="Cantidad Almacen" class="text-dark text-center" style="font-size:16px;">@{{producto.disponibilidad}}</td>
                          <td data-title="Acción" >
                            <button v-if="oficio.detalle_productos.length>1" type="button" title="Eliminar"  @click="oficio.detalle_productos.splice(indexProducto,1)" class="btn btn-danger btn-sm" name="button"><i class="fa fa-trash"></i></button>
                          </td>
                        </tr>--}}
                        <tr >
                          <td colspan="4"></td>
                          <td colspan="1" class="text-center"><button type="button" @click="oficio.detalle_productos.push({'cantidad':0,'control_oficio_registro_oficio_id':oficio.oficio_id,'created_at':'2018-10-18 08:50:40','disponibilidad':0,'id':0,'producto_id':1,'updated_at':'2018-10-18 08:50:40','producto':[]})" class="btn btn-primary" name="button"><i class="fa fa-plus"></i></button></td>
                        </tr>
                      </tbody>
                    </table>

                    <!-- <div class="container">
                      <div class="row">
                          <button type="button" name="button" class="btn btn-primary">
                            <i class="fa fa-plus"></i>
                          </button>
                      </div>
                    </div> -->
                  </div>
                </div>
                <a class="mc-btn-action" data-toggle="modal" data-target="#aprobarOficio" @click="capturarIndexOficio(index,'natural')">
                  <i class="fa fa-bars"></i>
                </a>
                <div class="mc-footer text-right">
                    <button title="Aprobar" type="button" @click="aprobarOficio(index)" name="button" class="btn btn-success mr-4">
                      <i class="fa fa-check"></i>
                    </button>
                    <button type="button" class="btn btn-danger mr-4"
                     name="button" data-toggle="modal" title="Rechazar" data-target="#rechazarOficio"
                     @click="capturarIndexOficio(index)"><i class="fa fa-close"></i></button>
                </div>
              </article>
            </div>
          </div>
          <div v-else class="text-center text-dark">
            <h3>Sin oficios pendientes por aprobar</h3>
          </div>
        </section>
      </div>
    <div class="tabpanel" id="panel2" role="tabpanel" @click="indexOficioTipo='juridico'">
       <div class="container-fluid">
         <div class="row">
           <section class="container-fluid">
             <!-- Filtros de búsqueda -->
             <div class="row p-4" >
               <div class="col-md-2 text-center">
                 <strong class="text-dark font-weight-bold">Fecha inicio:</strong>
                 <input type="date" class="form-control" v-model="fecha_inicio_juridico">
               </div>
               <div class="col-md-2 text-center">
                 <strong class="text-dark font-weight-bold">Fecha fin:</strong>
                 <input type="date" class="form-control" v-model="fecha_fin_juridico">
               </div>
               <div class="col-md-2 text-center">
                 <strong class="text-dark font-weight-bold">Atenciones:</strong>
                 <select class="form-control" v-model="num_atenciones_juridico">
                   <option value="">Seleccione un número de atención</option>
                   <option value="0">Ninguna</option>
                   <option value="1y5">Entre 1 y 5</option>
                   <option value="M5">Mayor a 5</option>
                 </select>
               </div>
               <div class="col-md-3 text-center">
                 <strong class="text-dark font-weight-bold">Clasificación del Ente:</strong>
                 <select class="form-control"  v-on:change="cargarEntes()" v-model="clasificacion_ente_id">
                   <option value="0">Seleccione una clasificación</option>
                   <option v-for="clasificacion in clasificacion_entes" v-bind:value="clasificacion.id">@{{clasificacion.nombre}}</option>
                 </select>
               </div>
               <div class="col-md-2 text-center"  v-show="entes.length>0">
                 <strong class="text-dark font-weight-bold">Ente:</strong>
                 <select class="form-control" v-model="ente_id">
                   <option value="0">Seleccione un ente</option>
                   <option v-for="ente in entes" v-bind:value="ente.id">@{{ente.nombre}}</option>
                 </select>
               </div>
               <div class="col-md-2 text-center">
                 <strong class="text-dark font-weight-bold">Estado:</strong>
                 <select class="form-control" v-model="estado_id_juridico">
                   <option value="0">Seleccione un estado</option>
                   <option v-for="estado in estados" v-bind:value="estado.idEstado">@{{estado.estado}}</option>
                 </select>
               </div>
               <div class="col-md-1 text-center align-self-center">
                 <br>
                 <button type="button" class="btn btn-info" @click="obtenerOficiosConFiltro()" name="button">Buscar</button>
               </div>
             </div>
             <!-- Filtros de búsqueda -->
             <div class="row active-with-click" v-if="oficios.personalJuridico.length>0">
               <div class="col-md-4 col-sm-6 col-xs-12" v-for="(oficio,index) in oficios.personalJuridico" v-if="oficio.detalle_productos.length>0" >
                 <article class="material-card Blue text-capitalize">
                   <h2>
                     <span>   @{{ oficio.empresa }}/@{{oficio.ente}} </span>
                     <strong>
                       <i class="fa fa-check-circle" aria-hidden="true"></i>
                       @{{oficio.solicitante}}
                        <span v-if="oficio.agente_retencion ===true">Agente de retención</span>
                        <span v-else>No es Agente de Retención</span>
                     </strong>
                   </h2>
                   <div class="mc-content">
                     <div class="img-container" style="background-color:#85C1E9">
                       <img id="cart" class="img-responsive" src="{{asset('images/cart.png')}}">
                     </div>
                     <div class="mc-description">
                       <table class="table table-bordered">
                         <thead >
                           <tr class="bg-primary">
                             <th>Modelo</th>
                             <th>Cantidad</th>
                             <th>Cantidad Almacen</th>
                             <th>Acción</th>
                           </tr>
                         </thead>
                         <tbody >
{{--                           <tr v-for="(producto,indexProducto) in oficio.detalle_productos">
                             <td>
                               <select class="form-control" v-model="producto.producto_id" name="" v-on:change="consultarDisponibilidad(index,indexProducto)">
                                 <option v-for="productoSistema in productos" v-bind:value="productoSistema.id">@{{productoSistema.descripcion}}</option>
                               </select>
                             </td>
                             <td>
                               <input type="number" min="1" class="form-control" @keypress="onlyNumber" v-model="producto.cantidad" v-bind:max="producto.disponibilidad">
                             </td>
                             <td class="text-dark text-center" style="font-size:16px;">@{{producto.disponibilidad}}</td>
                             <td>
                               <button type="button" title="Eliminar"  @click="oficio.detalle_productos.splice(indexProducto,1)" class="btn btn-danger" name="button"><i class="fa fa-trash"></i></button>
                             </td>
                           </tr>--}}
                           <tr >
                             <td colspan="4"></td>
                             <td colspan="1" class="text-center"><button type="button" @click="oficio.detalle_productos.push({'cantidad':0,'control_oficio_registro_oficio_id':oficio.oficio_id,'created_at':'2018-10-18 08:50:40','disponibilidad':0,'id':0,'producto_id':1,'updated_at':'2018-10-18 08:50:40','producto':[]})" class="btn btn-primary" name="button"><i class="fa fa-plus"></i></button></td>
                           </tr>
                         </tbody>
                       </table>
                     </div>
                   </div>
                   <a class="mc-btn-action" data-toggle="modal" data-target="#aprobarOficio" @click="capturarIndexOficio(index,'juridico')">
                     <i class="fa fa-bars"></i>
                   </a>
                   <div class="mc-footer text-right">
                       <button title="Aprobar" type="button" @click="aprobarOficio(index)" name="button" class="btn btn-success mr-4">
                         <i class="fa fa-check"></i>
                       </button>
                       <button type="button" class="btn btn-danger mr-4"
                        name="button" data-toggle="modal" title="Rechazar" data-target="#rechazarOficio"
                        @click="capturarIndexOficio(index)"><i class="fa fa-close"></i></button>
                   </div>
                 </article>
               </div>
             </div>
             <div v-else class="text-center text-dark">
               <h3>Sin oficios pendientes por aprobar</h3>
             </div>
           </section>
         </div>
       </div>
    </div>
  </div>

  </div>
  <!-- <div class="card-body">
  <label for=""> hey</label>
</div> -->
</div>
</section>
@endsection
@push('scripts')


<script type="text/javascript">
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    // Variables DB
      estados: {!! $estados ? $estados : "''"!!},
      motivos_solicitud: {!! $motivos_solicitud ? $motivos_solicitud : "''"!!},
      clasificacion_entes: {!! $clasificacion_entes ? $clasificacion_entes : "''"!!},
      oficios: {!! $oficios ? $oficios : "''"!!},
      productos: {!! $productos ? $productos : "''"!!},
      entes:[],
      verificacion:1,
      ente_id:0,
      agente_retencion:false,
      clasificacion_ente_id:0,
      motivo_solicitud_id:0,
      aprobacion:[],
      indexOficio:0,
      indexOficioTipo:'natural',
      indexSolicitante:'',
      /*Filtros*/
      num_atenciones:"",
      solicitante:'',
      estado_id:0,
      fecha_inicio:'',
      fecha_fin:'',
      fecha_inicio_juridico:'',
      fecha_fin_juridico:'',
      plan_venta:'',
      estado_id_juridico:0,
      num_atenciones_juridico:"",
      /*FIltros*/
      filtrosQuery:[]
  },
  computed:{
    validarAprobacion(){
      //Metodo para que no permita seleccionar mas de un checkbox en el modal de aprobacion (total o parcial)
      if(this.aprobacion.length>1)
      this.aprobacion.splice(1,1);
    },
  },
  methods:{
    limpiar(){
    this.aprobacion=[],
    this.verificacion=1;
  },

  onlyNumber ($event) {
     //console.log($event.keyCode); //keyCodes value
     let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
     if (keyCode > 31 && (keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
        $event.preventDefault();
     }
  },
  capturarIndexOficio(index,tipo){
      // if(this.oficios.personalNatural.length==0)
      // location.reload();
      // else if(this.oficios.personalJuridico.length==0)
      // location.reload();
      //natural o juridico
      this.indexOficio=index;
      this.indexOficioTipo=tipo;
      if(this.indexOficioTipo=='natural')
      this.indexSolicitante=this.oficios.personalNatural[index].solicitante;
      else{
        this.indexSolicitante=this.oficios.personalJuridico[index].solicitante;
        if (this.oficios.personalJuridico[index].agente_retencion === false) {
          this.oficios.personalJuridico[index].verificacion_pago = true;
        }else {
          this.oficios.personalJuridico[index].verificacion_pago = false;

        }
      }
      // console.log(this.indexOficio);
      // console.log(this.indexOficioTipo);
      this.limpiar();
    },
    rechazarOficio(){
    var oficio=null;
    if(this.indexOficioTipo=='natural')
    oficio=this.oficios.personalNatural[this.indexOficio];
    else
    oficio=this.oficios.personalJuridico[this.indexOficio];
    // console.log(oficio);
    axios.post('{{ route("rechazarOficio") }}', {oficio:oficio}).then(response => {
      if(response.data.error==0){
        // console.log(response.data);
        this.limpiar();
        alertify.success('Oficio rechazado exitosamente');
        this.oficios=response.data.oficios;
        // if(this.oficios.personalNatural.length==0)
        // location.reload();
        // else if(this.oficios.personalJuridico.length==0)
        // location.reload();
      }else if(response.data.error==1){
        // this.limpiar();
        alertify.error(response.data.msg);
      }
    }).catch(error => {
      this.limpiar();
      alertify.error('Error en el servidor.');
      console.log(error);
    });
  },
  aprobarOficio(indexOficio){
    var oficio=null;
    if(this.indexOficioTipo=='natural')
    oficio=this.oficios.personalNatural[indexOficio];
    else
    oficio=this.oficios.personalJuridico[indexOficio];
    var b=0;//variable bandera si no existe disponiblidad de un producto al momento de aprobar
    for(var i=0;i<oficio.detalle_productos.length;i++){
      if(parseInt(oficio.detalle_productos[i].cantidad)==0){
        b=1;
        break;
      }else if(parseInt(oficio.detalle_productos[i].cantidad)>parseInt(oficio.detalle_productos[i].disponibilidad)){
        b=2;
        break;
      }//if
    }//for
    if(b==1)
    alertify.error('Tienes la cantidad de un producto como 0, no es una cantidad valida para solicitar.');
    else if(b==2)
    alertify.error('Estas aprobando la cantidad de un producto en el cual no tienes disponibilidad.');
    else{
      var data={
        oficio:oficio,
        plan_venta:this.plan_venta
      };
      if(!this.indexOficioTipo=='natural')
      data.verificacion=this.oficios.personalJuridico[indexOficio].verificacion_pago;
      axios.post('{{ route("aprobarOficio") }}', data).then(response => {
        if(response.data.error==0){
          // console.log(response.data);
          alertify.success('Oficio aprobado exitosamente.');
          this.limpiar();
          // this.oficios=response.data.oficios;
          if(this.indexOficioTipo=='natural')
            this.oficios.personalNatural.splice(indexOficio,1);
          else
            this.oficios.personalJuridico.splice(indexOficio,1);
          $('#aprobarOficio').modal('toggle');
          // if(this.oficios.personalNatural.length==0 && this.indexOficioTipo=='natural')
          // location.reload();
          // else if(this.oficios.personalJuridico.length==0 && this.indexOficioTipo=='juridico')
          // location.reload();
        }else if(response.data.error==1){
          // this.limpiar();
          this.oficios=response.data.oficios;
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    }//else b==0, no errors
  },
  verDetalle(index,tipo){
    if(tipo=="natural"){
      if(this.oficios.personalNatural[index].ver_detalle==0)
      this.oficios.personalNatural[index].ver_detalle=1;
      else
      this.oficios.personalNatural[index].ver_detalle=0;
    }//natural
    else if(tipo=='juridico'){
      if(this.oficios.personalJuridico[index].ver_detalle==0)
      this.oficios.personalJuridico[index].ver_detalle=1;
      else
      this.oficios.personalJuridico[index].ver_detalle=0;
    }
  },
  cargarEntes(){
    this.ente_id=0;
    this.entes=[];
    axios.post('{{ route("api.controloficio.obtenerentes") }}', {clasificacion_ente_id:this.clasificacion_ente_id}).then(response => {
      if(response.data.error==0){
        this.entes=response.data.entes
      }else if(response.data.error==1){
        // this.limpiar();
        this.clasificacion_ente_id=0;
        alertify.error(response.data.msg);
      }
    }).catch(error => {
      this.limpiar();
      this.clasificacion_ente_id=0;
      alertify.error('Error en el servidor.');
      console.log(error);
    });
  },
  invertirFecha(fecha){
    var fecha=fecha.split("-");
    if(fecha[0].length==4)
    fecha=fecha[2]+"-"+fecha[1]+"-"+fecha[0];
    else
    fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
    return fecha;
  },
  invertirFechaCreatedAt(fecha){
    var fecha=fecha.split("-");
    var ano=fecha[2].split(" ");
    fecha=ano[0]+"-"+fecha[1]+"-"+fecha[0];
    return fecha;
  },
  agregarProducto(){
    //Variables Globales: indexOficio,indexOficioTipo
    if(this.indexOficioTipo=='natural'){
      this.oficios.personalNatural[this.indexOficio].detalle_productos.push({
        'cantidad':1,
        'control_oficio_registro_oficio_id':this.oficios.personalNatural[this.indexOficio].oficio_id,
        'created_at':'2018-10-18 08:50:40',
        'disponibilidad':0,
        'id':0,
        'producto_id':this.productos[0].id,
        'producto_nombre':this.productos[0].nombre,
        'producto_descripcion':this.productos[0].descripcion,
        'updated_at':'2018-10-18 08:50:40',
        'producto':[]
      });
      this.consultarDisponibilidad(this.indexOficio,this.oficios.personalNatural[this.indexOficio].detalle_productos.length-1);
    }else{
      this.oficios.personalJuridico[this.indexOficio].detalle_productos.push({
        'cantidad':0,'control_oficio_registro_oficio_id':this.oficios.personalJuridico[this.indexOficio].oficio_id,
        'created_at':'2018-10-18 08:50:40',
        'disponibilidad':0,
        'id':0,
        'producto_id':this.productos[0].id,
        'producto_nombre':this.productos[0].nombre,
        'producto_descripcion':this.productos[0].descripcion,
        'updated_at':'2018-10-18 08:50:40','producto':[]})
      this.consultarDisponibilidad(this.indexOficio,this.oficios.personalJuridico[this.indexOficio].detalle_productos.length-1);
    }
  },
  obtenerOficiosConFiltro(){
    var error=0;
    var filtros={
      test:'test'
    };
    if(this.indexOficioTipo=='natural'){
      if((this.fecha_inicio && this.fecha_inicio!="") && (this.fecha_fin && this.fecha_fin!="")){
        if(!this.validate_fechaMayorQue(this.fecha_inicio,this.fecha_fin)){
          alertify.error("La fecha "+this.fecha_inicio+" no puede ser superior a la fecha "+this.fecha_fin);
          error=1;
        }
        filtros.fecha_inicio=this.fecha_inicio;
        filtros.fecha_fin=this.fecha_fin;
      }//fecha_inicio!="" && fecha_fin!=""
      if(this.estado_id!=0)
        filtros.estado_id=this.estado_id;
        if(this.num_atenciones && this.num_atenciones!="")
        filtros.num_atenciones=this.num_atenciones;
    }else{
      //Juridico
      if((this.fecha_inicio_juridico && this.fecha_inicio_juridico!="") && (this.fecha_fin_juridico && this.fecha_fin_juridico!="")){
        if(!this.validate_fechaMayorQue(this.fecha_inicio_juridico,this.fecha_fin_juridico)){
          alertify.error("La fecha "+this.fecha_inicio_juridico+" no puede ser superior a la fecha "+this.fecha_fin_juridico);
          error=1;
        }
        filtros.fecha_inicio=this.fecha_inicio_juridico;
        filtros.fecha_fin=this.fecha_fin_juridico;
      }//fecha_inicio!="" && fecha_fin!=""
      if(this.estado_id_juridico!=0)
        filtros.estado_id=this.estado_id_juridico;
      if(this.num_atenciones_juridico && this.num_atenciones_juridico!="")
        filtros.num_atenciones=this.num_atenciones_juridico;
    }//else
    if(this.solicitante!="")
      filtros.solicitante=this.solicitante;
    if(this.clasificacion_ente_id!=0 && this.ente_id==0)
      alertify.error('Debes seleccionar un ente, de lo contrario no se tomará en cuenta este filtro de búsqueda.');
    if(this.ente_id!=0)
      filtros.ente_id=this.ente_id;
    filtros.tipoOficio=this.indexOficioTipo;
    console.log(filtros);
    if(error==0){
      axios.post('{{ route("obtenerOficiosFiltro") }}', {
        filtros:filtros
      }).then(response => {
        if(response.data.error==0){
          // this.oficios=response.data.oficios;
          if(this.indexOficioTipo=='natural')
          this.oficios.personalNatural=response.data.oficios.personalNatural;
          else if(this.indexOficioTipo=='juridico')
          this.oficios.personalJuridico=response.data.oficios.personalJuridico;
        }else if(response.data.error==1){
          // this.limpiar();
          this.clasificacion_ente_id=0;
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        this.clasificacion_ente_id=0;
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    }//error==1
  },
  consultarDisponibilidad(indexOficio,indexProducto){
    if(this.indexOficioTipo=="natural")
    var producto_id=this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].producto_id;
    else if(this.indexOficioTipo=="juridico")
    var producto_id=this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].producto_id;
    // console.log(producto_id);
    var producto_nombre='';
    var producto_descripcion='';
    for(var i=0;i<this.productos.length;i++){
      if(this.productos[i].id==producto_id){
        producto_nombre=this.productos[i].nombre;
        producto_descripcion=this.productos[i].descripcion;
        break;
      }//if
    }//for
    axios.post('{{ route("obtenerModalidadesYDisponibilidadProducto") }}', {producto_id:producto_id}).then(response => {
      if(this.indexOficioTipo=="natural"){
        this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].disponibilidad=response.data.disponibilidad;
        this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].producto_nombre=producto_nombre;
        this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].producto_descripcion=producto_descripcion;
        this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].modalidad_id=response.data.modalidad_id;
        this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].modalidades=response.data.modalidades;
      }else if(this.indexOficioTipo=="juridico"){
        this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].disponibilidad=response.data.disponibilidad;
        this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].producto_nombre=producto_nombre;
        this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].producto_descripcion=producto_descripcion;
        this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].modalidad_id=response.data.modalidad_id;
        this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].modalidades=response.data.modalidades;
      }//juridico
    }).catch(error => {
      this.limpiar();
      this.clasificacion_ente_id=0;
      alertify.error('Ocurrió un error en el servidor al intentar consultar la disponibilidad del producto');
      console.log(error);
    });
  },
  actualizarDisponibilidad(indexOficio,indexProducto){
    if(this.indexOficioTipo=="natural"){
      for(var i=0;i<this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].modalidades.length;i++){
        if(this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].modalidades[i].modalidad==this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].modalidad_id){
          this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].disponibilidad=this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].modalidades[i].disponibilidad;
        }
      }//for
    }//if natural
    else if(this.indexOficioTipo=="juridico"){
      for(var i=0;i<this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].modalidades.length;i++){
        if(this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].modalidades[i].modalidad==this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].modalidad_id){
          this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].disponibilidad=this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].modalidades[i].disponibilidad;
        }
      }//for
    }//else if juridico
  },
  validate_fechaMayorQue(fechaInicial,fechaFinal)
  {
    valuesStart=fechaInicial.split("-");
    valuesEnd=fechaFinal.split("-");
    // Verificamos que la fecha no sea posterior a la actual
    var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
    var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
    // console.log(dateStart,dateEnd);
    if(dateStart>dateEnd)
    {
      return 0;
    }
    return 1;
  },
  // consultarDisponibilidad(indexOficio,indexProducto){
  //   if(this.indexOficioTipo=="natural")
  //   var producto_id=this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].producto_id;
  //   else if(this.indexOficioTipo=="juridico")
  //   var producto_id=this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].producto_id;
  //   // console.log(producto_id);
  //   axios.post('{{ route("obtenerDisponibilidadProducto") }}', {producto_id:producto_id}).then(response => {
  //     if(this.indexOficioTipo=="natural")
  //     this.oficios.personalNatural[indexOficio].detalle_productos[indexProducto].disponibilidad=response.data.disponibilidad;
  //     else if(this.indexOficioTipo=="juridico")
  //     this.oficios.personalJuridico[indexOficio].detalle_productos[indexProducto].disponibilidad=response.data.disponibilidad;
  //   }).catch(error => {
  //     this.limpiar();
  //     this.clasificacion_ente_id=0;
  //     alertify.error('Ocurrió un error en el servidor al intentar consultar la disponibilidad del producto');
  //     console.log(error);
  //   });
  // }
  },
  mounted(){
    var d=new Date();
    var ano= d.getFullYear();
    var mes= d.getMonth()+1;
    var dia=d.getDate();
    if(dia<10)
    dia='0'+dia;
    if(mes<10)
    mes='0'+mes;
    console.log('oficios');
    console.log(this.oficios);

    for (var i = 0; i < this.oficios.personalJuridico.length; i++) {
      this.agente_retencion_var = this.oficios.personalJuridico[i].agente_retencion;

      // if (this.oficios.personalJuridico[i].agente_retencion === true) {
      //   console.log(this.oficios.personalJuridico[i].agente_retencion);
      //   this.agente_retencion_var =false;
      //   this.oficios.personalJuridico[i].agente_retencion = false;
      // }else if (this.oficios.personalJuridico[i].agente_retencion ===false) {
      //   console.log(this.oficios.personalJuridico[i].agente_retencion);
      //   this.agente_retencion_var =true;
      // }
      //
    }


  }
});//const app= new Vue
/***TABS PANEL***/
(function() {

  function activateTab() {
    if(activeTab) {
      resetTab.call(activeTab);
    }
    this.parentNode.className = 'tab tab-active';
    activeTab = this;
    activePanel = document.getElementById(activeTab.getAttribute('href').substring(1));
    activePanel.className = 'tabpanel show';
    activePanel.setAttribute('aria-expanded', true);
  }

  function resetTab() {
    activeTab.parentNode.className = 'tab';
    if(activePanel) {
      activePanel.className = 'tabpanel hide';
      activePanel.setAttribute('aria-expanded', false);
    }
  }

  var doc = document,
  tabs = doc.querySelectorAll('.tab a'),
  panels = doc.querySelectorAll('.tabpanel'),
  activeTab = tabs[0],
  activePanel;

  activateTab.call(activeTab);

  for(var i = tabs.length - 1; i >= 0; i--) {
    tabs[i].addEventListener('click', activateTab, false);
  }

})();

/***PROPUESTA ORIANA***/
// $(function() {
//   $('.material-card > .mc-btn-action').click(function () {
//     var card = $(this).parent('.material-card');
//     var icon = $(this).children('i');
//     icon.addClass('fa-spin-fast');
//
//     if (card.hasClass('mc-active')) {
//       card.removeClass('mc-active');
//
//       window.setTimeout(function() {
//         icon
//         .removeClass('fa-arrow-left')
//         .removeClass('fa-spin-fast')
//         .addClass('fa-bars');
//
//       }, 800);
//     } else {
//       card.addClass('mc-active');
//
//       window.setTimeout(function() {
//         icon
//         .removeClass('fa-bars')
//         .removeClass('fa-spin-fast')
//         .addClass('fa-arrow-left');
//
//       }, 800);
//     }
//   });
// });
</script>
@endpush
