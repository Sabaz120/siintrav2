@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE DEDUCCIONES</h5>
      <div class="row justify-content-center">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <strong>Nombre:</strong>
            <input id="nombre" class="form-control" minlength="3" maxlength="255"  type="text" v-model="nombreMaterial"  pattern="[a-zA-Z]*" placeholder="Ingrese nombre de la deducción" required >
            <!-- <input id="nombre" class="form-control" minlength="3" maxlength="255"  type="text" v-model="nombreMaterial"  pattern="[a-zA-Z]*" onkeypress="return soloLetras(event)" placeholder="Ingrese nombre de la deducción" required > -->
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <strong>Cantidad:</strong>
            <input type="number" class="form-control" v-model="cantidad">
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-44">
          <div class="form-group text-center">
            <strong>Tipo de deducción:</strong>
            <select class="form-control" v-model="tipo_deduccion">
              <option value="base imponible">Base imponible</option>
              <option value="iva">I.V.A</option>
            </select>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
        <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
        <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show='Deducciones.length'>
          <div class="row">
            <div class="header mx-auto">
              <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">LISTADO DE DEDUCCIONES</h4>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <table  class="table table-bordered text-center">
                  <thead class="bg-primary text-white">
                    <tr>
                      <th scope="col" style="cursor:pointer" class="text-center" @click="sort('nombre')">Nombre</th>
                      <th scope="col" style="cursor:pointer" class="text-center" @click="sort('descripcion')">Cantidad</th>
                      <th scope="col" style="cursor:pointer" class="text-center" @click="sort('tipo_deduccion')">Tipo de deducción</th>
                      <th scope="col" class="text-center">Acción</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr v-for="(material,index) in materiales_registrados" v-show='statusFiltro' >
                      <td class="text-center text-capitalize">@{{material.nombre}}</td>
                      <td  class="text-center text-capitalize">@{{material.cantidad}}</td>
                      <td  class="text-center text-capitalize">@{{material.tipo_deduccion}}</td>
                      <td class="text-center align-middle">
                        <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarMaterial(index)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-danger text-white text-center" data-toggle="modal"  data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Eliminar" @click="indexMaterial=index"><i class="fa fa-trash-alt fa-1x" aria-hidden="true"></i></button>
                      </td>
                    </tr>
                    <tr v-show='!statusFiltro'>
                      <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Deducciones.length}}</strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">EDICIÓN DE DEDUCCIÓN</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col">
            <div class="form-group">
              <i class="fas fa-asterisk"></i>
              <label for="nombre">Nombre:</label>
              <input class="form-control" minlength="3" maxlength="50"  type="text" v-model="nombreEdit" required>
              <!-- <input class="form-control" onkeypress="return soloLetras(event)"  minlength="3" maxlength="50"  type="text" v-model="nombreEdit" required> -->
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="descripcion">Cantidad:</label>
              <input type="number" class="form-control" v-model="cantidadEdit">
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="descripcion">Tipo de deducción:</label>
              <select class="form-control" v-model="tipoDeduccionEdit">
                <option value="base imponible">Base imponible</option>
                <option value="iva">I.V.A</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success text-white font-weight-bold" @click="editarMaterial()">Actualizar</button>
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- Eliminar -->
<div class="modal" id="myModalEliminar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title mx-auto  text-white">
          ELIMINAR DEDUCCIÓN</h4>
          <!-- <button type="button" class="close" data-dismiss="modal" >&times;</button> -->
        </div>
        <div class="modal-body">
          <div class="mx-auto text-center">
            ¿Esta seguro de eliminar esta deducción?
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminarMaterial">Sí, estoy seguro.</button>
          <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    Deducciones:{!! $deducciones ? $deducciones : "''"!!},
    indexMaterial:0,
    tipo_deduccion:'base imponible',
    cantidad:0,
    nombreEdit:'',
    descripcionEdit:'',
    cantidadEdit:0,
    tipoDeduccionEdit:'base imponible',
    nombreMaterial:'',
    descripcion:'',
    search:'',
    currentSort:'nombre',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0
  },
  computed:{
    materiales_registrados:function() {
      this.rows=0;
      return this.Deducciones.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    limpiar(){
      this.nombreMaterial='',
      this.cantidad='',
      this.tipo_deduccion='base imponible',
      this.indexMaterial=0
    },
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.Deducciones){
          if(this.Deducciones[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
            this.statusFiltro=1;
            filtrardo.push(this.Deducciones[i]);
          }//if(this.Materiales[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.MotivosSolicitud)
        if(filtrardo.length)
        this.Deducciones=filtrardo;
        else{
          this.statusFiltro=0;
          this.Deducciones={!! $deducciones ? $deducciones : "''"!!};
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.Deducciones={!! $deducciones ? $deducciones : "''"!!};
      }//if(this.search$materiales
      },// filtrar:function()
      registrar() {
        if(this.nombreMaterial==""){
          alertify.error('Debes ingresar un nombre de deducción');
        }else if(this.cantidad=="" ){
          alertify.error('Debe ingresar una cantidad correcta');
        }else{
          axios.post('{{ route("RegistrarDeduccion") }}', {
            nombre: this.nombreMaterial,
            cantidad: this.cantidad,
            tipo_deduccion: this.tipo_deduccion,
          })
          .then(response => {
            if(response.data.error==0){
              this.limpiar();
              this.Deducciones=response.data.deducciones;
              alertify.success(response.data.msg);
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
        }//else no errors
      },
      capturarMaterial(index){
        this.indexMaterial=index;
        this.nombreEdit=this.Deducciones[index].nombre;
        this.cantidadEdit=this.Deducciones[index].cantidad;
        this.tipoDeduccionEdit=this.Deducciones[index].tipo_deduccion;
      },
      editarMaterial(){
        if(this.nombreEdit==""){
          alertify.error('Debes ingresar un nombre de deducción');
        }else if(this.cantidadEdit==""){
          alertify.error('Debe ingresar una cantidad válida.');
        }else{
          axios.post('{{ route("ActualizarDeduccion") }}', {
            id:this.Deducciones[this.indexMaterial].id,
            nombre: this.nombreEdit,
            cantidad: this.cantidadEdit,
            tipo_deduccion: this.tipoDeduccionEdit,
          })
          .then(response => {
            if(response.data.error==0){
              this.limpiar();
              this.Deducciones=response.data.deducciones;
              alertify.success(response.data.msg);
              $('#myModal').modal('toggle');
            }//if(response.data.status=="success")
            else if(response.data.error==1){
              alertify.error(response.data.msg);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
        }//else no errors
      },
      eliminarMaterial(){
        axios.post('{{ route("EliminarDeduccion") }}', {
          id:this.Deducciones[this.indexMaterial].id
        })
        .then(response => {
          if(response.data.error==0){
            this.limpiar();
            this.Deducciones=response.data.deducciones;
            alertify.success(response.data.msg);
            $('#myModalEliminar').modal('toggle');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      },//eliminarMaterial
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.Deducciones.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      }
    },//methods
  });//const app= new Vue
</script>
@endpush
