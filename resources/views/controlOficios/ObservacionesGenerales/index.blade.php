@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">OBSERVACIONES Y CONDICIONES GENERALES DE COMPRA</h5>
      <div class="row justify-content-center text-center">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group">
            <i class="fas fa-asterisk"></i>
            <label for="posicion">Posición:</label>
            <select class="form-control" name="posicion" v-model="posicion">
              <option value="">Seleccione orden</option>
              <option v-for="(observacion,index) in Observaciones" :value="index+1">@{{index+1}}</option>
              <option :value="Observaciones.length+1">@{{Observaciones.length+1}}</option>
            </select>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group">
            <i class="fas fa-asterisk"></i>
            <label for="descripcion">Descripción:</label>
            <input class="form-control"  type="text" v-model="descripcion" placeholder="Ingrese descripción" required >
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-12 text-center">
        <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
        <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
      </div>
      <div class="col-12 col-md-12 col-lg-12">
        <div id="tabla" v-show='Observaciones.length'>
          <div class="row">
            <div class="header mx-auto">
              <h4 class="text-dark  header-title m-t-0 m-b-30 pt-5">OBSERVACIONES Y CONDICIONES</h4>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <div  class="mx-auto">
                <div class="row">
                  <div class="col-12 col-md-9 col-lg-9">
                    <br>
                    <div class="mt-2 form-inline font-weight-bold">
                      Mostrar
                      <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                        <option v-for="option in optionspageSize" v-bind:value="option.value">
                          @{{ option.text }}
                        </option>
                      </select>
                      registros
                    </div>
                  </div>
                  <div class="col-12 col-md-3 col-lg-3">

                    <div class="form-group">
                      <label for="Buscar" class="font-weight-bold">Buscar:</label>
                      <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
                    </div>
                  </div>
                </div>
                <div id="no-more-tables">
                  <table  class="table table-bordered table-condensed cf table-striped">
                    <thead class="bg-primary text-white">
                      <tr>
                        <th scope="col" style="cursor:pointer; width:25%;" class="text-center" >Posición</th>
                        <th scope="col" style="cursor:pointer; width:25%;" class="text-center" >Descripción</th>
                        <th scope="col" class="text-center width:25%;">Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(registro,index) in registros" v-show='statusFiltro'>
                        <td class="text-justify"  data-title="Posicion"><p class="anchoparrafo">@{{registro.posicion}}</p></td>
                        <td  class="text-justify" data-title="Descripción"><p class="anchoparrafo">@{{registro.descripcion | placeholder('Sin información') }}</p></td>
                        <td class="text-center align-middle" data-title="Acción">
                          <button v-if="!registro.deleted_at" type="button" class="btn btn-info text-white text-center" data-toggle="modal" data-target="#myModalEliminar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Deshabilitar" @click="capturarData(registro)"><i class="fa fa-close fa-1x" aria-hidden="true"></i></button>
                          <button v-else type="button" class="btn btn-success text-white text-center" data-toggle="modal" data-target="#myModalRestaurar" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Habilitar" @click="capturarData(registro)"><i class="fa fa-check fa-1x" aria-hidden="true"></i></button>
                          <button type="button" class="btn btn-primary text-white text-center" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Editar" @click="capturarData(registro)"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                        </td>
                      </tr>
                      <tr v-show='!statusFiltro'>
                        <td class="text-justify font-weight-bold" colspan="3">No se encontraron registros</td>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="">
                <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Observaciones.length}}</strong>
                <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de: 0 </strong>
                <div style="float:right">
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                  <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  <div class="row ml-2">
                    <strong>Página:  @{{currentPage}}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- The Modal Editar -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h4 class="modal-title mx-auto  text-white">EDICIÓN DE OBSERVACIONES Y CONDICIONES</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">

        <div class="row justify-content-center text-center">
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
              <i class="fas fa-asterisk"></i>
              <label for="posicion">Posición:</label>
              <select class="form-control" v-model="posicion_editar" name="">
                <option v-for="(observacion,index) in Observaciones" :value="index+1">@{{index+1}}</option>
                <!-- <option :value="Observaciones.length+1">@{{Observaciones.length+1}}</option> -->
              </select>
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-4">
            <div class="form-group">
              <i class="fas fa-asterisk"></i>
              <label for="descripción">Descripción:</label>
              <input class="form-control" type="text" v-model="descripcion_editar" required >
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success text-white font-weight-bold" @click="editar()">Actualizar</button>
        <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>

<!-- The Modal Deshabilitar -->
<div class="modal" id="myModalEliminar">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h5 class="modal-title mx-auto  text-white">
          DESHABILITAR/HABILITAR OBSERVACIÓN</h4>
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mx-auto text-center">
            ¿Esta seguro de deshabilitar este registro?
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success text-white font-weight-bold" @click="eliminar()">Sí, estoy seguro.</button>
          <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<!-- The Modal Habilitar -->
<div class="modal" id="myModalRestaurar">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary">
        <h5 class="modal-title mx-auto  text-white">
          DESHABILITAR/HABILITAR OBSERVACIÓN</h4>
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mx-auto text-center">
            ¿Esta seguro de habilitar este registro?
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success text-white font-weight-bold" @click="habilitar()">Sí, estoy seguro.</button>
          <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    errors: [],
    posicion:'',
    descripcion:'',
    posicion_editar:'',
    descripcion_editar:'',
    Observaciones:{!! $Observaciones ? $Observaciones : "''"!!},
    ObservacionesTemp:{!! $Observaciones ? $Observaciones : "''"!!},
    search:'',
    currentSort:'deleted_at',//campo por defecto que tomara para ordenar
    currentSortDir:'desc',//order asc
    pageSize:'5',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0

  },
  computed:{

    registros:function() {
      this.rows=0;
      return this.Observaciones.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
  },
  methods:{
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        for(let i in this.Observaciones){
          if(this.Observaciones[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.Observaciones[i]);
          }//  if(this.Vehiculos[i].modelo.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Vehiculos[i].placa.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.Vehiculos[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.Vehiculos)
        if(filtrardo.length)
        this.Observaciones=filtrardo;
        else{
          this.statusFiltro=0;
          this.Observaciones=this.ObservacionesTemp;
        }// if(filtrardo.length)
      }else{
        this.statusFiltro=1;
        this.Observaciones=this.ObservacionesTemp;
      }//if(this.search)
    },// filtrar:function()

    limpiar:function(){
      this.error=[];
      this.posicion="";
      this.descripcion="";
    },//limpiar:function()

    registrar:function(){
      this.errors=[];

      if (!this.descripcion) {
        this.errors.push("Existen campos por llenar:<br>*Descripción.");
      } else if (this.descripcion.length<3 ) {
        this.errors.push('El campo descripción acepta un mínimo de 3 caracteres.');
      }// if (!this.data.modelo)
      if(!this.posicion)
        this.errors.push('Debes seleccionar una posición.');

      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
      }else{
        /*Envio de datos a la base de datos*/
        axios.post('{{ url("observaciones_condiciones") }}', {
          posicion: this.posicion,
          descripcion: this.descripcion,
        })
        .then(response => {
          if(response.data.error==0){
            this.Observaciones=response.data.Observaciones;
            this.ObservacionesTemp=response.data.Observaciones;
            this.limpiar();
            alertify.set('notifier','position', 'top-right');
            alertify.success(response.data.msg);
          }//if(response.data.status=="success")
          if(response.data.error==1){
            alertify.set('notifier','position', 'top-right');
            alertify.error(response.data.msg);
          }//if(response.data.status=="error")

        })
        .catch(function (error) {
          console.log(error);
        });
        /*Fin envio de datos a la base de datos*/
      }//if(this.error.length)
    },
    editar(){
      this.errors=[];

      if (!this.descripcion_editar) {
        this.errors.push("Existen campos por llenar:<br>*Descripción.");
      } else if (this.descripcion_editar.length<3 ) {
        this.errors.push('El campo descripción acepta un mínimo de 3 caracteres.');
      }// if (!this.data.modelo)
      if(!this.posicion_editar)
        this.errors.push('Debes seleccionar una posición.');

      if(this.errors.length>0){
        for(var i=0;i<this.errors.length;i++){
          alertify.set('notifier','position', 'top-right');
          alertify.error(this.errors[i]);
        }// for(var i=0;i<this.errors.length;i++)
      }else{
        /*Envio de datos a la base de datos*/
        axios.put('{{ url("observaciones_condiciones") }}', {
          id:this.id,
          posicion: this.posicion_editar,
          descripcion: this.descripcion_editar,
        })
        .then(response => {
          if(response.data.error==0){
            this.Observaciones=response.data.Observaciones;
            this.ObservacionesTemp=response.data.Observaciones;
            this.limpiar();
            alertify.set('notifier','position', 'top-right');
            alertify.success(response.data.msg);
            $('#myModal').modal('toggle');
          }//if(response.data.status=="success")
          if(response.data.error==1){
            alertify.set('notifier','position', 'top-right');
            alertify.error(response.data.msg);

          }//if(response.data.status=="error")

        })
        .catch(function (error) {
          console.log(error);
        });
        /*Fin envio de datos a la base de datos*/
      }//if(this.error.length)
    },
    eliminar(){
      axios.post('{{ url("observaciones_condiciones/deshabilitar") }}', {
        id:this.id
      })
      .then(response => {
        if(response.data.error==0){
          this.Observaciones=response.data.Observaciones;
          this.ObservacionesTemp=response.data.Observaciones;
          this.limpiar();
          alertify.set('notifier','position', 'top-right');
          alertify.success(response.data.msg);
          $('#myModalEliminar').modal('toggle');
        }//if(response.data.status=="success")
        if(response.data.error==1){
          alertify.set('notifier','position', 'top-right');
          alertify.error(response.data.msg);
        }//if(response.data.status=="error")

      })
      .catch(function (error) {
        console.log(error);
      });
      /*Fin envio de datos a la base de datos*/
    },
    habilitar(){
      axios.post('{{ url("observaciones_condiciones/habilitar") }}', {
        id:this.id
      })
      .then(response => {
        if(response.data.error==0){
          this.Observaciones=response.data.Observaciones;
          this.ObservacionesTemp=response.data.Observaciones;
          this.limpiar();
          alertify.set('notifier','position', 'top-right');
          alertify.success(response.data.msg);
          $('#myModalRestaurar').modal('toggle');
        }//if(response.data.status=="success")
        if(response.data.error==1){
          alertify.set('notifier','position', 'top-right');
          alertify.error(response.data.msg);
        }//if(response.data.status=="error")

      })
      .catch(function (error) {
        console.log(error);
      });
      /*Fin envio de datos a la base de datos*/
    },
    capturarData: function(registro){
      this.id=registro.id;
      this.posicion_editar=registro.posicion;
      this.descripcion_editar=registro.descripcion;
    },
    datoBorrar:function(id){
      this.id=id;
    },
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.Observaciones.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    }

  },//methods
});//const app= new Vue
</script>
@endpush
