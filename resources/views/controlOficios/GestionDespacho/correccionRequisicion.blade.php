@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">CORRECCIÓN DE REQUISICIÓN</h5>
      <div class="row text-center">
        <div class="col-lg-5 col-md-5">
          <label class="font-weight-bold">N° de requisición:</label>
          <input type="number" class="form-control" v-model="num_requisicion" onKeyPress="return soloNumeros(event);" maxlength="15">
        </div>
        <div class="col-lg-7 col-md-7">
          <br>
          <button type="button" class="btn btn-info" @click="buscar()" name="button">Buscar</button>
        </div>
      </div>
      <div class="row col-12 justify-content-center mt-4" v-if="oficio!=null">
        <h3>Datos del oficio</h3>
      </div>
      <div class="row text-center mt-4" v-if="oficio!=null">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Número de identificación</th>
              <th>Cliente</th>
              <th>Tipo de cliente</th>
              <th>Número de oferta económica</th>
              <th>Número de requisición</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>@{{oficio.num_identificacion}}</td>
              <td>@{{oficio.cliente}}</td>
              <td v-if="oficio.es_personal_juridico">Jurídico</td>
              <td v-else="oficio.es_personal_juridico">Natural</td>
              <td>@{{oficio.oferta_economica_id}}</td>
              <td>@{{oficio.num_requisicion}}</td>
              <td>
                <button type="button" class="btn btn-success" @click="corregir(oficio.num_requisicion)" name="button">Corregir</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="row col-12 justify-content-center mt-4" v-if="oficio!=null">
        <h4>Productos</h4>
      </div>
      <div class="row text-center mt-4" v-if="oficio!=null">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Modelo</th>
              <th>Modalidad</th>
              <th>Cantidad</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="equipo in oficio.equipos">
              <td>@{{equipo.producto.descripcion}}</td>
              <td>@{{equipo.modalidad}}</td>
              <td>@{{equipo.cantidad}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    num_requisicion:'',
    oficio:null
  },
  methods:{
    buscar(){
      if(this.num_requisicion=="")
      {
        alertify.error("El número de requisición es requerido para poder realizar la busqueda");
      }//if(this.num_requisicion=="")
      else if(this.num_requisicion.toString().length<8){
        alertify.error("El número de requisición ingresado no es valido");
      }// else if(this.num_requisicion.length>8)
      else{
        axios.get('{{ route("api.gestion.despacho") }}', {
           params:{
            num_requisicion:this.num_requisicion,
           }
        }).then(response => {
           if(response.data.oficios.length==0)
             alertify.error("No se ha encontrado ningún oficio con el número de requisición: "+this.num_requisicion);
           else
             this.oficio=response.data.oficios[0];
        }).catch(error => {
           // this.limpiar();
           alertify.error('Error en el servidor.');
           console.log(error);
         });
      }//else if(this.num_requisicion=="")
    },
    corregir(num_requisicion){
      axios.post('{{ route("api.correccion.requisicion") }}', {
        num_requisicion:num_requisicion,
      }).then(response => {
        if(response.data.status==200){
          alertify.success(response.data.msg);
          this.oficio=null;
        }else{
          alertify.error('Detalle técnico del error: '+response.data.msgSystem);
        }
      }).catch(error => {
        console.log(error.response);
        alertify.error('Se ha producido un error en el servidor.');
      });
    }
  },//methods
  mounted(){

  }
});//const app= new Vue

function soloNumeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = "0123456789";
    especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if(letras.indexOf(tecla) == -1 && !tecla_especial){
        return false;
      }
}
</script>
@endpush
