@extends('layouts.pdf.pdf')
@section('contenido')
<style media="screen">
h2,h3,h4,h5{
  text-align:center;
}
</style>
<h2 class="text-center">REQUISICIÓN DE DESPACHO</h2>
<label style="float:right" ><strong>N° REQUISICIÓN: {{$oficio->num_requisicion}}</strong></label>
<br>
<div class="grid-container">
  <div style="grid-item">
    <strong>
      <label>De: </label> <u>Gerencia de Comercialización</u>
    </strong>
    <br>
    <strong>
      <label>Para: </label> <u>GERENCIA DE ADMINISTRACIÓN Y LOGÍSTICA / DEPARTAMENTO DE ALMACEN Y LOGÍSTICA</u>
    </strong>
    <p>
      <strong>
      <span style="font-size: 14px;">SIRVA REALIZAR EL DESPACHO DE LA PRESENTE REQUISICIÓN PARA DAR ATENCIÓN AL CLIENTE:<span> <u style="text-transform: uppercase;">{{ $nombre_institucion }}</u>
    </strong>
    </p>
  </div>
  <br>
  <div class="grid-item">
    <table class="table table-bordered">
      <thead>
        <tr >
          <th style="text-align:center;" colspan="3">FECHA DE CREACIÓN DE REQUISICIÓN</th>
        </tr>
        <tr class="text-center bg-muted">
          <th>DÍA</th>
          <th>MES</th>
          <th>AÑO</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{\Carbon\Carbon::parse($oficio->requisicion->created_at)->format('d')}}</td>
          <td>{{\Carbon\Carbon::parse($oficio->requisicion->created_at)->format('m')}}</td>
          <td>{{\Carbon\Carbon::parse($oficio->requisicion->created_at)->format('Y')}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<br>
<label><strong>TIPO DE DESPACHO: VENTA</strong></label>
<br>
<table class="table table-bordered">
  <thead>
    <tr class="text-center bg-muted">
      <th>MODELO</th>
      <th>MODALIDAD</th>
      <th>CANTIDAD</th>
    </tr>
  </thead>
  <tbody>
    @foreach($oficio->equipos as $equipo)
    <tr>
        <td>{{$equipo->producto->nombre}}</td>
        <td>{{$equipo->modalidad}}</td>
        <td>{{$equipo->cantidad}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
<table class="table table-bordered">
  <thead>
    <tr>
      <th colspan="2">USO DE LA GERENCIA DE COMERCIALIZACIÓN</th>
      <th>USO DEL DEPARTAMENTO DE ALMACEN Y LOGÍSTICA</th>
    </tr>
    <tr class="text-center bg-muted">
      <th>ELABORADO POR:</th>
      <th>APROBADO POR:</th>
      <th>RECIBIDO POR:</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">
        <br>
        <br>
        ________________
        <br>
        ANALISTA DE VENTAS
      </td>
      <td class="text-center">
        <br>
        <br>
        ________________
        <br>
        GERENTE DE COMERCIALIZACIÓN
      </td>
      <td class="text-center">
        <br>
        <br>
        _____________________
        <br>
        ANALISTA DE INVENTARIO
      </td>
    </tr>
  </tbody>
</table>

</div>


@endsection
