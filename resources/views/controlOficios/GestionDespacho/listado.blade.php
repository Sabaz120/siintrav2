@extends('layouts.app')
@section('contenido')
<section id="register">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE REQUISICIÓN</h5>
      <div class="row text-center">
        <div class="col-lg-3 col-md-3">
          <label class="font-weight-bold">Fecha Inicio:</label>
          <input type="date" class="form-control" v-model="fecha_inicio" value="">
        </div>
        <div class="col-lg-3 col-md-3">
          <label class="font-weight-bold">Fecha Fin:</label>
          <input type="date" class="form-control" v-model="fecha_fin" value="">
        </div>
        <div class="col-lg-3 col-md-3">
          <label class="font-weight-bold">Verificación de Pago:</label>
          <select class="form-control" v-model="verificacion_pago">
            <option value="">Todos</option>
            <option value="1">Requiere</option>
            <option value="0">No requiere</option>
          </select>
        </div>
        <div class="col-lg-3 col-md-3">
          <br>
          <button type="button" class="btn btn-info" @click="buscar()" name="button">Buscar</button>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-12 table-responsive" v-if="Oficios.length>0" >
          <div class="row">
            <div class="col-12 col-md-9 col-lg-9">
              <br>
              <div class="mt-2 form-inline font-weight-bold">
                Mostrar
                <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                  <option v-for="option in optionspageSize" v-bind:value="option.value">
                    @{{ option.text }}
                  </option>
                </select>
                registros
              </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <label for="Buscar" class="font-weight-bold">Buscar:</label>
                <input id="search" class="form-control form-control-sm" type="text" v-model="search" v-on:keyup="filtrar" required>
              </div>
            </div>
          </div>
          <table  class="table table-bordered text-center" >
            <thead class="bg-primary text-white">
              <tr>
                <td>N° de Cotización</td>
                <td>RIF/CI</td>
                <td>Cliente/Ente</td>
                <td>Cantidades</td>
                <td>Acción</td>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(oficio,index) in oficios_registrados" v-show="statusFiltro==1">
                <td>@{{oficio.num_cotizacion}}</td>
                <td>@{{oficio.num_identificacion}}</td>
                <td>@{{oficio.cliente}}</td>
                <td>@{{oficio.cantidad_equipos}}</td>
                <td class="row justify-content-center">
                  <div class="col-2">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#previsualizar" data-backdrop="static" data-keyboard="false" @click="capturarIndexOficio(oficio.id)" title="Visualizar requisición" name="button">
                      <i class="fa fa-search"></i>
                    </button>
                  </div>
                  <div class="col-2" v-if="oficio.requisicion">
                    <form action="{{route('api.requisicion.pdf')}}" method="post">
                      @csrf
                      <input type="hidden" name="oficio_id" :value="oficio.id">
                      <button type="submit" class="btn btn-info" title="Generar PDF de requisición" name="button">
                        <i class="fa fa-file-pdf"></i>
                      </button>
                    </form>
                  </div>
                  <!--<div class="col-1" v-if="oficio.requisicion">
                    <button type="button"   data-toggle="modal" @click="capturarIndexOficio(oficio.id)" data-target="#asignarChofer" data-backdrop="static" data-keyboard="false" class="btn btn-info" title="Datos de chofer" name="button">
                      <i class="fa fa-car"></i>
                    </button>
                  </div>-->
                  <!--<div class="col-1" v-if="oficio.requisicion">
                    <button type="button"   data-toggle="modal" @click="capturarIndexOficio(oficio.id)" data-target="#recaudos" data-backdrop="static" data-keyboard="false" class="btn btn-info" title="Lista de recaudos" name="button">
                      <i class="fa fa-list"></i>
                    </button>
                  </div>-->
                  <div class="col-2" v-if="oficio.oferta_economica.requisicion">
                    <button v-if="oficio.oferta_economica.requisicion.despachado == true" type="button"  data-toggle="modal" data-target="#cerrarRecaudo" data-backdrop="static" data-keyboard="false"   class="btn btn-info" title="Cerrar oferta" name="button" @click="abrirModalCerrarOferta(oficio.oferta_economica.control_oficio_registro_oficio_id, oficio.despachado)">
                      <i class="fa fa-lock"></i>
                    </button>
                  </div>
                  <div class="col-2" v-if="oficio.requisicion">
                    <button type="button" v-if="oficio.oferta_economica.requisicion.despachado == false"  data-toggle="modal" data-target="#eliminarRecaudo" data-backdrop="static" data-keyboard="false"   class="btn btn-danger" title="Eliminar" name="button" @click="abrirModalEliminarRequisicion(oficio.num_requisicion)">
                      <i class="fa fa-trash"></i>
                    </button>
                  </div>
                  <div class="col-2" v-if="oficio.requisicion && oficio.despacho">
                    <button type="button"   data-toggle="modal" @click="capturarIndexOficio(oficio.id)" data-target="#despacho" data-backdrop="static" data-keyboard="false"  class="btn btn-success" title="Fecha despacho" name="button">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </div>
                  <div class="col-2" v-if="oficio.requisicion && oficio.agente_retencion==true">
                    <button type="button"  class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#asociarPago" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Asociar pagos" @click="capturarIndexOficio(oficio.id)"><i class="fa fa-money-bill-wave" aria-hidden="true"></i></button>
                  </div>
                  <div class="col-2" v-if="oficio.requisicion && oficio.despacho && oficio.despacho.fecha_despacho">
                    <button type="button" class="btn btn-primary text-white text-center mr-2" data-toggle="modal" data-target="#cierreRequisicion" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" title="Cierre de requisición" @click="capturarIndexOficio(oficio.id)">
                      <i class="fa fa-lock" aria-hidden="true"></i>
                    </button>
                  </div>

                </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="10">No se encontraron registros</td>
              </tr>

            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{Oficios.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 text-center jumbotron" v-else>
          <h5>Sin registros que mostrar</h5>
        </div>
      </div>
    </div>
  </div>

  <!-- The Modal Confirm-->
  <div class="modal" id="eliminarRecaudo">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <div class="mx-auto">
            <h4 class="modal-title text-center">¡Advertencia!</h4>
          </div>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="mx-auto">
                <h3>¿Esta seguro de eliminar este registro?</h3>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" @click="eliminarRequisicion">Sí, estoy seguro</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>

      </div>
    </div>
  </div>

  <div class="modal" id="cerrarRecaudo">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <div class="mx-auto">
            <h4 class="modal-title text-center">Cierre de oferta económica</h4>
          </div>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="container">
            <div class="row">
              <div class="mx-auto">
                <h3>¿Está seguro que desea cerrar la oferta económica?</h3>
              </div>

            </div>
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" @click="cerrarOferta()">Sí</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        </div>

      </div>
    </div>
  </div>


  @include('controlOficios.GestionDespacho.partials.modal_asignar_chofer')
  @include('controlOficios.GestionDespacho.partials.modal_asociarPago')
  @include('controlOficios.GestionDespacho.partials.modal_verPago')
  @include('controlOficios.GestionDespacho.partials.modal_fecha_despacho')
  @include('controlOficios.GestionDespacho.partials.modal_previsualizar_requisicion')
  @include('controlOficios.GestionDespacho.partials.modal_recaudos')
  @include('controlOficios.GestionDespacho.partials.modal_cierre_requisicion')
</section>
@endsection
@push('scripts')
<script>
const app= new Vue({
  el:'#register',
  data:{
    btnRegistroChofer:false,
    Oficios:[],
    OficiosTemp:[],
    deducciones:{!! $deducciones ? $deducciones : "''"  !!},
    relacion_deducciones:[],
    /* Variables modal asociar pago*/
    tipo_pago:0,
    referencia_pago:'',
    monto_pago:0,
    soporte_pago:'',
    pago_id:0,//Id del pago a editar
    pago_index:null,
    pago_editar:false,
    iva:16,
    total:0,
    numero_requisicion:0,
    /* Variables modal asociar pago*/
    cedula_chofer:"",
    chofer_nombres_apellidos:"",
    chofer_numero:"",
    destino_despacho:"",
    vehiculo_placa:"",
    vehiculo_modelo:"",
    cedula_responsable:"",
    responsable_nombres_apellidos:"",
    responsable_numero:"",
    beneficiarios:"",
    fechaDespacho:"",
    indexOficio:0,
    // Paginación
    search:'',
    currentSort:'num_cotizacion',//campo por defecto que tomara para ordenar
    currentSortDir:'asc',//order asc
    pageSize:'25',//Registros por pagina
    optionspageSize: [
      { text: '5', value: 5 },
      { text: '10', value: 10 },
      { text: '25', value: 25 },
      { text: '50', value: 50 },
      { text: '100', value: 100 }
    ],//Registros por pagina
    currentPage:1,//Pagina 1
    statusFiltro:1,
    rows:0,
    // Paginación
    //FIltros de busqueda
    verificacion_pago:"",
    fecha_inicio:"",
    fecha_fin:"",


    /***new***/
    chofer_correo:'',
    chofer_nombres:'',
    chofer_apellidos:'',
    readChofer:true,
    readVehiculo:true,
    vehiculo_descripcion:'',
    responsable_id:'',
    oficio_id:'',

    //tipos de Ventas

    tipoVentas:{!! $tipoVentas ? $tipoVentas : "''"  !!},
    tipoVenta:"0",
    seedStock:false,
    seedStockPorcentaje:"2",
    observacionFinal:'',
    cerrarOfertaEconomica:"",
    despachoOfertaEconomica:""

  },
  computed:{
    oficios_registrados:function() {
      this.rows=0;
      return this.Oficios.sort((a,b) => {
        let modifier = 1;
        if(this.currentSortDir === 'desc')
        modifier = -1;
        if(a[this.currentSort] < b[this.currentSort])
        return -1 * modifier;
        if(a[this.currentSort] > b[this.currentSort])
        return 1 * modifier;
        return 0;
      }).filter((row, index) => {
        let start = (this.currentPage-1)*this.pageSize;
        let end = this.currentPage*this.pageSize;
        if(index >= start && index < end){
          this.rows+=1;
          return true;
        }
      });
    },
    calcular_subtotal:function(){
      if(this.Oficios.length>0){
        var subtotal=0;
        for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.detalle_oferta.length;i++)
        subtotal=subtotal+this.Oficios[this.indexOficio].oferta_economica.detalle_oferta[i].cantidad*this.Oficios[this.indexOficio].oferta_economica.detalle_oferta[i].precio_unitario;
        if(this.Oficios[this.indexOficio].requisicion){
          for(var i=0;i<this.Oficios[this.indexOficio].requisicion.deducciones.length;i++){
            if(this.Oficios[this.indexOficio].requisicion.deducciones[i].deduccion.tipo_deduccion=="base imponible"){
              subtotal=subtotal-parseFloat(this.Oficios[this.indexOficio].requisicion.deducciones[i].deduccion.cantidad);
              // console.log('resta base imponible'+subtotal);
            }
          }//for deducciones
        }
        return parseFloat(subtotal).toFixed(2);
      }else{
        return 0;
      }
    },
    calcular_total:function(){
      if(this.Oficios.length>0){
        var total=0;
        var subtotal=0;
        var iva=0;
        for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.detalle_oferta.length;i++){
          subtotal=subtotal+this.Oficios[this.indexOficio].oferta_economica.detalle_oferta[i].cantidad*this.Oficios[this.indexOficio].oferta_economica.detalle_oferta[i].precio_unitario;
        }
        if(this.Oficios[this.indexOficio].requisicion){
          for(var i=0;i<this.Oficios[this.indexOficio].requisicion.deducciones.length;i++){
            if(this.Oficios[this.indexOficio].requisicion.deducciones[i].deduccion.tipo_deduccion=="base imponible"){
              subtotal=subtotal-parseFloat(this.Oficios[this.indexOficio].requisicion.deducciones[i].deduccion.cantidad);
              // console.log('resta base imponible'+subtotal);
            }
          }//for deducciones
        }
        iva=((this.iva*subtotal)/100);
        if(this.Oficios[this.indexOficio].requisicion){
          for(var i=0;i<this.Oficios[this.indexOficio].requisicion.deducciones.length;i++){
            if(this.Oficios[this.indexOficio].requisicion.deducciones[i].deduccion.tipo_deduccion=="iva"){
              iva=iva*parseFloat(this.Oficios[this.indexOficio].requisicion.deducciones[i].deduccion.cantidad);
              console.log('resta iva'+iva);
              break;
            }
          }//for deducciones
        }
        total=subtotal+iva;
        total=parseFloat(total).toFixed(2);
        this.total=total;
        return total;
      }else{
        return 0;
      }
    },
    calcular_restante:function(){
      if(this.Oficios.length>0){
        var cancelado=0;
        for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.pagos.length;i++){
          cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica.pagos[i].monto);
        }
        cancelado=this.total-cancelado;
        cancelado=parseFloat(cancelado).toFixed(2);
        return cancelado;//restante
      }else{
        return 0;
      }
    },
    calcular_cancelado:function(){
      if(this.Oficios.length>0){
        var cancelado=0;
        for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.pagos.length;i++){
          cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica.pagos[i].monto);
        }
        cancelado=parseFloat(cancelado).toFixed(2);
        return cancelado;
      }else{
        return 0;
      }
    }
  },
  methods:{
    abrirModalEliminarRequisicion(numeroRequisicion){
      this.numero_requisicion=numeroRequisicion;
    },
    abrirModalCerrarOferta(numeroRequisicion, despacho){
      this.cerrarOfertaEconomica = numeroRequisicion
      this.despachoOfertaEconomica = despacho
      //alert(this.cerrarOfertaEconomica)
    },
    cerrarOferta(){

          axios.post("{{ url('api/requisicion/cerrar_oferta_economica') }}", {cerrarOfertaEconomica: this.cerrarOfertaEconomica}).then(res => {

            if(res.data.success == true){
              window.location.reload()
            }

          })

    },
    eliminarRequisicion(){
      axios.post('{{ route("api.requisicion.eliminarRequisicion") }}', {
        'numero_requisicion':this.numero_requisicion
      })
      .then(response => {
        console.log(response.data);
        if(response.data.error ==0){
          alertify.success(response.data.mensaje);
          $('#eliminarRecaudo').modal('toggle');
          this.obtenerResumenDespachos();
        }else if (response.data.error==1) {
          alertify.error(response.data.mensaje);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    },

    cierreRequisicion(){
      axios.post('{{ route("api.requisicion.cierre") }}', {
        observacion:this.observacionFinal,
        requisicion_id: this.Oficios[this.indexOficio].requisicion.id,
        oficio_id:this.Oficios[this.indexOficio].id
      }).then(response => {
        // console.log(response.data);
        if(response.data.error ==0){
          this.indexOficio=0;
          alertify.success(response.data.mensaje);
          $('#cierreRequisicion').modal("toggle");
          this.obtenerResumenDespachos();
        }
        else if (response.data.error==1)
          alertify.error(response.data.mensaje);
      }).catch(function (error) {
        console.log(error);
      });
    },

    capturarIndexOficio(id){

      for(var i=0;i<this.Oficios.length;i++){
        if(this.Oficios[i].id==id){
          this.indexOficio=i;
          this.responsable_id=this.Oficios[i].personal_natural_id;
          this.oficio_id=this.Oficios[i].id;

          break;
        }
      }//for
      var arr=[];
      if(this.Oficios[this.indexOficio].oferta_economica.requisicion){
        for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.requisicion.deducciones.length;i++)
        arr.push(this.Oficios[this.indexOficio].oferta_economica.requisicion.deducciones[i].deduccion.id);
      }
      this.relacion_deducciones=arr;
    },
    actualizarPago(){
      var b=0;
      if(this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index].tipo_pago==0){
        b++;
        alertify.error('Debes seleccionar un tipo de pago');
      }else if(this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index].referencia==""){
        b++;
        alertify.error('Debes ingresar el número de referencia del pago');
      }else if(this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index].monto==""){
        b++;
        alertify.error('Debes ingresar el monto del pago realizado');
      }else if(this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index].tipo_pago=="transferencia" && this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index].soporte==""){
        b++;
        alertify.error('Debes ingresar el soporte de pago.');
      }
      if(b==0){
        axios.post('{{ route("actualizarPago") }}', {
          pago:this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index]
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.limpiar();
            // $('#myModal').modal('toggle');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }//b==0 no errors
    },
    eliminarPago(pago_id){
      axios.post('{{ route("borrarPago") }}', {
        pago_id:pago_id
      })
      .then(response => {
        if(response.data.error==0){
          alertify.success(response.data.msg);
          for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.pagos.length;i++){
            if(this.Oficios[this.indexOficio].oferta_economica.pagos[i].id==pago_id){
              this.Oficios[this.indexOficio].oferta_economica.pagos.splice(i,1);
              break;
            }//pago==id
          }//for
          // $('#myModal').modal('toggle');
        }//if(response.data.status=="success")
        else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    limpiar(){
      this.cedula_chofer='';
      this.cedula_responsable='';
      this.chofer_nombres_apellidos='';
      this.chofer_numero='';
      this.chofer_nombres='';
      this.chofer_apellidos='';
      this.chofer_correo='';
      this.responsable_nombres_apellidos='';
      this.responsable_numero='';
      this.vehiculo_placa='';
      this.vehiculo_modelo='';
      this.vehiculo_descripcion='';
      this.destino_despacho='';
      this.beneficiarios='';
      this.fechaDespacho='';
      this.pago_id=0;
      this.soporte_pago=0;
      this.tipo_pago=0;
      this.referencia_pago="";
      this.monto_pago="";
      this.pago_index=null;
      this.pago_editar=false;
      this.readChofer=true;
      this.readVehiculo=true;
    },
    slugify(string) {
      return string
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, "-")
      .replace(/[^\w\-]+/g, "")
      .replace(/\-\-+/g, "-")
      .replace(/^-+/, "")
      .replace(/-+$/, "");
    },
    convertirBase64SoportePago: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.soporte_pago = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    formatearNumero:function(nStr) {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
    },
    convertirBase64SoportePagoEditar: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.Oficios[this.indexOficio].oferta_economica.pagos[this.pago_index].soporte_lote = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    buscar(){
      axios.get('{{ route("api.gestion.despacho") }}', {
        params:{
          verificacion_pago:this.verificacion_pago,
          fecha_inicio:this.fecha_inicio,
          fecha_fin:this.fecha_fin
        }
      }).then(response => {
        if(response.data.error==0){
          this.Oficios=response.data.oficios;
          this.OficiosTemp=response.data.oficios;
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        // this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    actualizarRecaudos(){
      axios.post('{{ route("api.requisicion.update.recaudos") }}', {
        recaudos: this.Oficios[this.indexOficio].requisicion.recaudos,
      })
      .then(response => {
        this.limpiar();
        alertify.success(response.data.msg);
        $('#recaudos').modal("toggle");
        this.Oficios=response.data.oficios;
      })
      .catch(function (error) {
        alertify.error("Whoops, ha ocurrido un error en el servidor.");
        console.log(error);
      });
    },
    asociarDeducciones(){
      axios.post('{{ route("api.requisicion.sync.deducciones") }}', {
        requisicion_id: this.Oficios[this.indexOficio].requisicion.id,
        oficio_id:this.Oficios[this.indexOficio].id,
        deducciones:this.relacion_deducciones
      })
      .then(response => {
        this.limpiar();
        alertify.success(response.data.msg);
        this.indexOficio=0;
        setTimeout(function(){
          window.location.reload(1);
        }, 2500);
        // this.Oficios[this.indexOficio].requisicion.deducciones=response.data.deducciones;
        $('#asociarPago').modal("toggle");
      })
      .catch(function (error) {
        alertify.error("Whoops, ha ocurrido un error en el servidor.");
        console.log(error);
      });
    },
    registrarDespacho(){
      var oficio=this.Oficios[this.indexOficio];
      if(this.fechaDespacho!=""){
        axios.post('{{ route("api.requisicion.store.despacho") }}', {
          oferta_economica_id: oficio.oferta_economica_id,
          oficio_id: oficio.id,
          requisicion_id: oficio.requisicion.id,
          num_factura: oficio.num_cotizacion,
          fecha_despacho: this.fechaDespacho,
        })
        .then(response => {
          this.limpiar();
          alertify.success(response.data.msg);
          $('#despacho').modal("toggle");
          this.Oficios=response.data.oficios;
        })
        .catch(function (error) {
          alertify.error("Whoops, ha ocurrido un error en el servidor.");
          console.log(error);
        });
      }else
      alertify.error("Debes seleccionar una fecha de despacho");

    },
    actualizarDespacho(){
      var oficio=this.Oficios[this.indexOficio];
      if(this.Oficios[this.indexOficio].despacho.fecha_despacho!=""){
        axios.post('{{ route("api.requisicion.update.despacho") }}', {
          requisicion_id: oficio.requisicion.id,
          fecha_despacho: oficio.despacho.fecha_despacho,
        })
        .then(response => {
          this.limpiar();
          alertify.success(response.data.msg);
          $('#despacho').modal("toggle");
          this.Oficios=response.data.oficios;
        })
        .catch(function (error) {
          alertify.error("Whoops, ha ocurrido un error en el servidor.");
          console.log(error);
        });
      }else
      alertify.error("Debes seleccionar una fecha de despacho");

    },
    actualizarChofer(){
      // console.log(this.Oficios[this.indexOficio].oferta_economica.despacho);
      var b=0;
      if(this.Oficios[this.indexOficio].despacho.chofer.cedula==""){
        b=1;
        alertify.error('Debe ingresar una cédula de chofer');
      }else if(this.Oficios[this.indexOficio].despacho.vehiculo.placa==""){
        b=1;
        alertify.error('Debe ingresar una placa de vehículo');
      }else if(this.Oficios[this.indexOficio].despacho.destino==""){
        b=1;
        alertify.error('Debe ingresar el destino del despacho');
      }else if(this.Oficios[this.indexOficio].despacho.responsable.cedula==""){
        b=1;
        alertify.error('Debe ingresar una cédula de responsable');
      }else if(this.Oficios[this.indexOficio].despacho.beneficiario==""){
        b=1;
        alertify.error('Debe adjuntar un pdf de beneficiarios');
      }
      if(b==0){
        //no errors
        axios.post('{{ route("actualizarDatosChofer2") }}', {
          oficio_id:this.Oficios[this.indexOficio].id,
          requisicion_id:this.Oficios[this.indexOficio].oferta_economica.requisicion.id,
          cedula_chofer:this.Oficios[this.indexOficio].despacho.chofer.cedula,
          nombre_chofer:this.Oficios[this.indexOficio].despacho.chofer.nombres,
          apellidos_chofer:this.Oficios[this.indexOficio].despacho.chofer.apellidos,
          telefono_chofer:this.Oficios[this.indexOficio].despacho.chofer.telefono,
          correo_chofer:this.Oficios[this.indexOficio].despacho.chofer.correo_electronico,
          vehiculo_placa:this.Oficios[this.indexOficio].despacho.vehiculo.placa,
          vehiculo_id:this.Oficios[this.indexOficio].despacho.vehiculo.id,
          vehiculo_modelo:this.Oficios[this.indexOficio].despacho.vehiculo.modelo,
          vehiculo_descripcion:this.Oficios[this.indexOficio].despacho.vehiculo.descripcion,
          despacho_id:this.Oficios[this.indexOficio].despacho.id
        }).then(response => {
          if(response.data.error==0){
            this.obtenerResumenDespachos();
            this.limpiar();
            $('#asignarChofer').modal('toggle');
            alertify.success(response.data.msg);
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          this.limpiar();
          alertify.error('Error en el servidor.');
          console.log('Axios api datos persona natural catch');
          console.log(error);
        });
      }//b==0 no errors
    },
    convertirBase64BeneficiariosEditar: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.Oficios[this.indexOficio].despacho.beneficiario = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },

    // registrarChofer(){
    //   var b=0;
    //   if(this.cedula_chofer==""){
    //     b=1;
    //     alertify.error('Debe ingresar una cédula de chofer');
    //   }else if(this.destino_despacho==""){
    //     b=1;
    //     alertify.error('Debe ingresar el destino del despacho');
    //   }else if(this.vehiculo_placa==""){
    //     b=1;
    //     alertify.error('Debe ingresar una placa de vehículo');
    //   }else if(this.cedula_responsable==""){
    //     b=1;
    //     alertify.error('Debe ingresar una cédula de responsable');
    //   }else if(this.beneficiarios==""){
    //     b=1;
    //     alertify.error('Debe adjuntar un pdf de beneficiarios');
    //   }
    //   if(b==0){
    //     //no errors
    //     axios.post('{{ route("registrarChofer") }}', {
    //       cedula_chofer:this.cedula_chofer,
    //       cedula_responsable:this.cedula_responsable,
    //       chofer_nombres_apellidos:this.chofer_nombres_apellidos,
    //       chofer_numero:this.chofer_numero,
    //       responsable_nombres_apellidos:this.responsable_nombres_apellidos,
    //       responsable_numero:this.responsable_numero,
    //       vehiculo_placa:this.vehiculo_placa,
    //       vehiculo_modelo:this.vehiculo_modelo,
    //       destino_despacho:this.destino_despacho,
    //       beneficiarios:this.beneficiarios,
    //       oficio_id:this.Oficios[this.indexOficio].id
    //     }).then(response => {
    //       if(response.data.error==0){
    //         this.Oficios[this.indexOficio].despacho={'prueba':1234};
    //         this.Oficios[this.indexOficio].despacho.beneficiario=this.beneficiarios;
    //         this.Oficios[this.indexOficio].despacho.destino=this.destino_despacho;
    //         this.Oficios[this.indexOficio].despacho.fecha_despacho=null;
    //         this.Oficios[this.indexOficio].despacho.id=response.data.despacho_id;
    //         this.Oficios[this.indexOficio].despacho.chofer={'cedula':this.cedula_chofer,'nombres_apellidos':this.chofer_nombres_apellidos,'telefono':this.chofer_numero};
    //         this.Oficios[this.indexOficio].despacho.responsable={'cedula':this.cedula_responsable,'nombres_apellidos':this.responsable_nombres_apellidos,'telefono':this.responsable_numero};
    //         this.Oficios[this.indexOficio].despacho.vehiculo={'placa':this.vehiculo_placa,'modelo':this.vehiculo_modelo};
    //         $('#asignarChofer').modal('toggle');
    //         alertify.success(response.data.msg);
    //         this.limpiar();
    //       }else if(response.data.error==1){
    //         alertify.error(response.data.msg);
    //       }
    //     }).catch(error => {
    //       this.limpiar();
    //       alertify.error('Error en el servidor.');
    //       console.log(error);
    //     });
    //   }//b==0 no errors
    // },

    registrarDatosChofer(){
      this.btnRegistroChofer=false;
      var b=0;
      if(this.cedula_chofer==""){
        b=1;
        alertify.error('Debe ingresar una cédula de chofer');
      }else if(this.chofer_nombres==""){
        b=1;
        alertify.error('Debe ingresar los nombres del chofer');
      }else if(this.chofer_apellidos==""){
        b=1;
        alertify.error('Debe ingresar los apellidos del Chofer');
      }else if(this.chofer_numero==""){
        b=1;
        alertify.error('Debe ingresar un número de teléfono de contacto');
      }else if(this.chofer_correo==""){
        b=1;
        alertify.error('Debe ingresar un correo electrónico');
      }else if(this.vehiculo_placa==""){
        b=1;
        alertify.error('Debe ingresar el número de placa del vehículo');
      }else if(this.vehiculo_modelo==""){
        b=1;
        alertify.error('Debe ingresar el modelo del vehículo');
      }
      else if(this.vehiculo_descripcion==""){
        b=1;
        alertify.error('Debe ingresar una descripción del vehículo');
      }
      if(b==1){
        this.btnRegistroChofer=true;

      }else{
        axios.post('{{ route("api.controloficio.registrarDatosChofer") }}', {
        cedula_chofer:this.cedula_chofer,
        chofer_nombres:this.chofer_nombres,
        chofer_apellidos:this.chofer_apellidos,
        chofer_numero:this.chofer_numero,
        chofer_correo:this.chofer_correo,
        vehiculo_placa:this.vehiculo_placa,
        vehiculo_modelo:this.vehiculo_modelo,
        vehiculo_descripcion:this.vehiculo_descripcion,
        responsable_id:this.responsable_id,
        oficio_id:this.oficio_id,
        requisicion_id:this.Oficios[this.indexOficio].oferta_economica.requisicion.id,
        codigo_carnet_patria:'0000000000'
      }).then(response => {
        this.obtenerResumenDespachos();
        this.btnRegistroChofer=false

        $('#asignarChofer').modal('toggle');
        alertify.success(response.data.msg);
        this.limpiar();
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
      }//else if (b==1)

    },

    buscarPersonaNatural(cedula,tipo_persona,tipo_operacion){
      //tipo_persona == chofer || responsable
      // console.log(cedula,tipo_persona);
      if(cedula==""){
       alertify.error('Debe ingresar un número de cédula para la busqueda.');
       return -1;
      }//if(cedula=="")
      axios.post('{{ route("api.controloficio.datos_persona_natural2") }}', {cedper:cedula}).then(response => {
        if(response.data.error==0){
          if(tipo_persona=="chofer"){
            //Si es chofer
            if(tipo_operacion=='registrar'){
              console.log('registrar 0');
              this.chofer_nombres = response.data.persona_natural.nombre;
              this.chofer_apellidos =response.data.persona_natural.apellido;
              // this.chofer_nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.chofer_numero=response.data.persona_natural.telefono;
              this.chofer_correo=response.data.persona_natural.correo_electronico;
            }else{
              this.Oficios[this.indexOficio].despacho.chofer.nombres=response.data.persona_natural.nombre;
              this.Oficios[this.indexOficio].despacho.chofer.apellidos=response.data.persona_natural.apellido;
              this.Oficios[this.indexOficio].despacho.chofer.telefono=response.data.persona_natural.telefono;
              this.Oficios[this.indexOficio].despacho.chofer.correo_electronico=response.data.persona_natural.correo_electronico;
            }
          }else{
            //Si es responsable
            if(tipo_operacion=='registrar'){
              this.responsable_nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.responsable_numero=response.data.persona_natural.telefono;
            }else{
              this.Oficios[this.indexOficio].despacho.responsable.nombres_apellidos=response.data.persona_natural.nombre+' '+response.data.persona_natural.apellido;
              this.Oficios[this.indexOficio].despacho.responsable.telefono=response.data.persona_natural.telefono;
            }
          }
        }else if(response.data.error==1){
          console.log('falso');
          this.readChofer=false;
          if (this.Oficios[this.indexOficio].despacho !=null) {
            this.Oficios[this.indexOficio].despacho.chofer.nombres='';
            this.Oficios[this.indexOficio].despacho.chofer.apellidos='';
            this.Oficios[this.indexOficio].despacho.chofer.telefono='';
            this.Oficios[this.indexOficio].despacho.chofer.correo_electronico='';
          }
          alertify.error(response.data.msg);
        }else if(response.data.error==500){
          alertify.error('Ha ocurrido un error en el servidor, por favor notifique al administrador.');
          // console.log(response.data.msg);
          this.limpiar();
        }else{
          this.limpiar();
          console.log('Error sin codigo:'+response.data.msg);
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');
        console.log('Axios api datos persona natural catch');
        console.log(error);
      });
    },
    convertirBase64Beneficiarios: function(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.beneficiarios = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
    },
    buscarVehiculo(placa,tipo_operacion){
      if(placa==""){
        alertify.error("Debe ingresar un número de placa para la busqueda");
        return -1;
      }//if(placa=="")
      axios.post('{{ route("api.controloficio.datos.vehiculo") }}', {placa:placa}).then(response => {
        if(response.data.error==0){
          if(tipo_operacion=='registrar'){
            this.vehiculo_modelo=response.data.vehiculo.modelo;
            this.vehiculo_descripcion=response.data.vehiculo.descripcion;
          }//if(tipo_operacion=='registrar')

          else if(tipo_operacion=='editar'){
            this.Oficios[this.indexOficio].despacho.vehiculo.modelo=response.data.vehiculo.modelo;
            this.Oficios[this.indexOficio].despacho.vehiculo.descripcion=response.data.vehiculo.descripcion;
          }

          this.readVehiculo=true;
        }else if(response.data.error==1){
          this.vehiculo_modelo="";
          this.vehiculo_descripcion="";
          if(tipo_operacion!='registrar'){
            this.Oficios[this.indexOficio].despacho.vehiculo.modelo='';
            this.Oficios[this.indexOficio].despacho.vehiculo.descripcion='';
          }//if(tipo_operacion!='registrar')

          //this.readVehiculo=false;
          alertify.error(response.data.msg);
          this.readVehiculo=false;
        }
      }).catch(error => {
        this.limpiar();
        alertify.error('Error en el servidor.');

      });
    },
    // Paginación
    filtrar:function(){
      let filtrardo=[];
      if(this.search){
        // console.log('entre');
        // console.log(this.search);
        for(let i in this.Oficios){
          if(this.Oficios[i].cliente.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1){
            this.statusFiltro=1;
            filtrardo.push(this.Oficios[i]);
          }//if(this.NivelAtencion[i].nivel.toString().toLowerCase().trim().search(this.search.toLowerCase())!=-1)
        }//for(let i in this.NivelAtencion)
        // console.log(filtrardo);
        if(filtrardo.length>0){
          this.Oficios=filtrardo;
          this.statusFiltro=1;
        }
        else{
          this.statusFiltro=0;
          this.Oficios=this.OficiosTemp;
        }// if(filtrardo.length)
      }else{
        // console.log('Search vacio');
        this.statusFiltro=1;
        this.Oficios=this.OficiosTemp;
      }//if(this.search)
    },// filtrar:function()
    nextPage:function() {
      if((this.currentPage*this.pageSize) < this.Oficios.length) this.currentPage++;
    },
    prevPage:function() {
      if(this.currentPage > 1) this.currentPage--;
    },
    sort:function(s) {
      //if s == current sort, reverse
      // console.log(s);
      if(s === this.currentSort) {
        this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
      }
      this.currentSort = s;
    },
    // Paginación
    //
    obtenerResumenDespachos(){
      axios.get('{{ route("api.gestion.despacho") }}', {
      }).then(response => {
        if(response.data.error==0){
          this.Oficios=response.data.oficios;
          this.OficiosTemp=response.data.oficios;
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        // this.limpiar();
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },
    crearRequisicion(){
      var oficio=this.Oficios[this.indexOficio];

      if(this.tipoVenta==0 || this.tipoVenta=="0"){
        alertify.error("Debes seleccionar un tipo de venta.");
      }else{
        axios.post('{{ route("api.requisicion.store") }}', {
          oferta_economica_id: oficio.oferta_economica_id,
          numero_requisicion: oficio.num_requisicion,
          // tipo_venta_id: 1,
          tipo_venta_id: this.tipoVenta,
          seedStock: this.seedStock,
          seedStockPorcentaje: this.seedStockPorcentaje
        })
        .then(response => {
          // this.limpiar();
          alertify.success(response.data.msg);
          $('#previsualizar').modal("toggle");
          this.Oficios=response.data.oficios;
        })
        .catch(function (error) {
          alertify.error("Whoops, ha ocurrido un error en el servidor.");
          console.log(error);
        });
      }

    },
    onlyNumber ($event) {
      //console.log($event.keyCode); //keyCodes value
      let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if (keyCode > 31 && (keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
        $event.preventDefault();
      }
    },
    onlyNumberWithOutDecimalPoint ($event) {
      //console.log($event.keyCode); //keyCodes value
      let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if (keyCode > 31 && (keyCode < 48 || keyCode > 57)) { // 46 is dot (,)
        $event.preventDefault();
      }
    },
    asociarPago(){
      var b=0;
      if(this.tipo_pago==0){
        b++;
        alertify.error('Debes seleccionar un tipo de pago');
      }else if(this.referencia_pago==""){
        b++;
        alertify.error('Debes ingresar el número de referencia del pago');
      }else if(this.monto==""){
        b++;
        alertify.error('Debes ingresar el monto del pago realizado');
      }else if(this.tipo_pago=="transferencia" && this.soporte_pago==""){
        b++;
        alertify.error('Debes ingresar el soporte de pago.');
      }
      if(b==0){
        axios.post('{{ route("asociarPago") }}', {
          oficio_id:this.Oficios[this.indexOficio].id,
          tipo_pago:this.tipo_pago,
          referencia_pago:this.referencia_pago,
          monto_pago:this.monto_pago,
          soporte_pago:this.soporte_pago
        })
        .then(response => {
          if(response.data.error==0){
            alertify.success(response.data.msg);
            this.Oficios[this.indexOficio].oferta_economica.pagos.push(response.data.pago);
            var cancelado=0;
            for(var i=0;i<this.Oficios[this.indexOficio].oferta_economica.pagos.length;i++){
              if(this.Oficios[this.indexOficio].oferta_economica.pagos[i].validacion)
              cancelado=cancelado+parseFloat(this.Oficios[this.indexOficio].oferta_economica.pagos[i].monto);
            }
            cancelado=parseFloat(cancelado);
            if(parseFloat(cancelado)==this.total){
              //Muestra alerta que ya todo el dinero fue cancelado,
              //si aun no ha cargado datos de chofer, muestra alerta que debe cargar dichos datos.
              //sino, lo quita del listado.
              $('#asociarPago').modal('toggle');
              if(this.Oficios[this.indexOficio].despacho==null){
                alertify.warning('Ya se pagó el monto total de la cotización. Debes ingresar los datos del chofer,para proceder al despacho.');
                $('#asignarChofer').modal('toggle');
              }
            }//Si todo fue cancelado
            this.limpiar();
            // $('#myModal').modal('toggle');
          }//if(response.data.status=="success")
          else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }//b==0 no errors
    },
  },//methods
  mounted(){
    this.obtenerResumenDespachos();

  }
});//const app= new Vue

</script>
@endpush
