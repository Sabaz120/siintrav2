<div class="modal" id="despacho" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg" v-if="Oficios[indexOficio].despacho!=null">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">PLAN DE DESPACHO</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">

          <div class="col-6 form-group text-center">
              <label class="font-weight-bold">Número de Requisición</label>
              <input type="text" class="form-control" readonly v-model="Oficios[indexOficio].num_requisicion">
          </div>

          <div class="col-6 form-group text-center">
              <label class="font-weight-bold">Cliente</label>
              <input type="text" class="form-control" readonly v-model="Oficios[indexOficio].cliente">
          </div>

          <div class="col-12 form-group text-center table-responsive">
            <table class="table table-bordered table-shape">
              <thead>
                <tr>
                  <td colspan="3" class="text-white" style="background-color:gray;">MODELOS SOLICITADOS</td>
                </tr>
                <tr>
                  <td>MODELO</td>
                  <td>MODALIDAD</td>
                  <td>CANTIDAD</td>
                </tr>
              </thead>
              <tbody>
                <tr v-for="equipo in Oficios[indexOficio].equipos">
                  <td>@{{equipo.producto.descripcion}}</td>
                  <td>@{{equipo.modalidad}}</td>
                  <td>@{{equipo.cantidad}}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="col-12 form-group text-center">
              <label class="font-weight-bold">Fecha de retiro de los equipos:</label>
              <input type="date" class="form-control" v-model="fechaDespacho" v-if="!Oficios[indexOficio].despacho.fecha_despacho">
              <input type="date" class="form-control" v-model="Oficios[indexOficio].despacho.fecha_despacho" v-else>
          </div>

        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer justify-content-center table-responsive">
        <button type="button" class="btn btn-success" v-if="!Oficios[indexOficio].despacho.fecha_despacho" @click="registrarDespacho" name="button">Registrar</button>
        <button type="button" class="btn btn-success" v-else @click="actualizarDespacho" name="button">Actualizar</button>
      </div>
    </div>
  </div>
</div>
