<div class="modal" id="previsualizar" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">PREVISUALIZACIÓN DE REQUISICIÓN</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-12" >
            <div class="form-group text-center">
              <label class="font-weight-bold">REQUISICIÓN DE DESPACHO</label>
                <label style="float:right" class="font-weight-bold">N° REQUISICIÓN: @{{Oficios[indexOficio].num_requisicion}}</label>
            </div>
          </div>
          <div class="col-6">
            <label class="font-weight-bold">De: </label> <u>Gerencia de Comercialización</u>
            <br>
            <label class="font-weight-bold">Para: </label> <u>GERENCIA DE ADMINISTRACIÓN Y LOGÍSTICA / DEPARTAMENTO DE ALMACEN Y LOGÍSTICA</u>
            <br>
            <label>
              <strong>Se genera la siguiente requisición para dar atención al siguiente Cliente:
                <u style="text-transform: uppercase;" v-if="Oficios[indexOficio].personal_juridico">
                  @{{ Oficios[indexOficio].personal_juridico.nombre_institucion }}
                </u>
                <u style="text-transform: uppercase;" v-else>
                  @{{ Oficios[indexOficio].personal_natural.nombre }} @{{ Oficios[indexOficio].personal_natural.apellido }}
                </u>
              </strong>
            </label>
          </div>
          <div class="col-6">
            <table class="table table-bordered">
              <thead>
                <tr >
                  <th style="text-align:center;" colspan="3">FECHA</th>
                </tr>
                <tr class="text-center bg-muted">
                  <th>DÍA</th>
                  <th>MES</th>
                  <th>AÑO</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>05</td>
                  <td>04</td>
                  <td>2019</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-12">
            <!--<label class="font-weight-bold">TIPO DE DESPACHO:</label> VENTA-->
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">TIPO DE VENTA</label>
              <div class="col-sm-6">
                <select class="form-control" v-model="tipoVenta">
                  <option :value="0">Seleccione el tipo de venta</option>
                  <option v-for="(venta, index) in tipoVentas" :value="venta.id">@{{ venta.nombre }}</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6">
                <div class="form-check">
                  <label class="form-check-label" for="defaultCheck1">
                    ¿APLICA SEED STOCK (%)?
                  </label>
                  <input class="form-check-input" type="checkbox" v-model="seedStock" id="defaultCheck1" style="margin-left: 10px;">
                </div>
              </div>
{{--              <div class="col-sm-6" v-if="seedStock == true">
                <select class="form-control" v-model="seedStockPorcentaje">
                  <!-- <option :value="0">Seleccione %</option> -->
                  <!-- <option :value="1">1%</option> -->
                  <option :value="2">2%</option>
                  <option :value="3">3%</option>
                  <option :value="4">4%</option>
                  <option :value="5">5%</option>
                </select>
              </div>--}}
            </div>
            <table class="table table-bordered">
              <thead>
                <tr class="text-center bg-muted">
                  <th>MODELO</th>
                  <th>MODALIDAD</th>
                  <th>CANTIDAD</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="equipo in Oficios[indexOficio].equipos">
                  <td>@{{equipo.producto.nombre}}</td>
                  <td>@{{equipo.modalidad}}</td>
                  <td>@{{equipo.cantidad}}</td>
                </tr>
              </tbody>
            </table>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th colspan="2">USO DE LA GERENCIA DE COMERCIALIZACIÓN</th>
                  <th>USO DEL DEPARTAMENTO DE ALMACEN Y LOGÍSTICA</th>
                </tr>
                <tr class="text-center bg-muted">
                  <th>ELABORADO POR:</th>
                  <th>APROBADO POR:</th>
                  <th>RECIBIDO POR:</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center">
                    <br>
                    <br>
                    ________________
                    <br>
                    ANALISTA DE VENTAS
                  </td>
                  <td class="text-center">
                    <br>
                    <br>
                    ________________
                    <br>
                    GERENTE DE COMERCIALIZACIÓN
                  </td>
                  <td class="text-center">
                    <br>
                    <br>
                    _____________________
                    <br>
                    ANALISTA DE INVENTARIO
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">
        <button type="button" class="btn btn-success" v-show="!Oficios[indexOficio].requisicion" @click="crearRequisicion" name="button">Guardar</button>
      </div>
    </div>
  </div>
</div>
