<div class="modal modalasociarPago" id="asociarPago" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">ASOCIACIÓN DE PAGO</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6 text-center">
            <h5 style="color:blue;">TOTAL A PAGAR</h5>
            <br>
            <h5>@{{formatearNumero(calcular_total)}} Bs</h5>
            <br>
            <h5 style="color:blue;">TOTAL CANCELADO</h5>
            <br>
            <h5>@{{formatearNumero(calcular_cancelado)}} Bs</h5>
            <hr style="background-color:blue;">
            <h5 style="color:blue;">SALDO DEUDOR</h5>
            <br>
            <h5>@{{formatearNumero(calcular_restante)}} Bs</h5>
            <br>
          </div>
          <div class="col-md-6 text-center">
            <h5 class="font-weight-bold">Configuración de deducciones</h5>
            <br>
            <div class="custom-control custom-checkbox justify-content-center" v-for="deduccion in deducciones">
              <input type="checkbox" v-model="relacion_deducciones" :value="deduccion.id" class="custom-control-input" :id="deduccion.id+slugify(deduccion.nombre)">
              <label class="custom-control-label" :for="deduccion.id+slugify(deduccion.nombre)">
                @{{deduccion.nombre}}
              </label>
            </div>
            <br>
            <div class="text-center">
              <button type="button" @click="asociarDeducciones" class="btn btn-success" name="button">
                Guardar configuración de deducciones
              </button>
            </div>
          </div>
          <div class="col-md-12 text-center" v-if="pago_index==null && pago_editar==false">
            <h3>DETALLE DEL PAGO</h3>
            <div class="row">
              <div class="col-md-6 mb-4">
                <strong>
                  <i class="fa fa-asterisk"></i>
                  Tipo de Pago:
                </strong>
                <select class="form-control" v-model="tipo_pago">
                  <option value="0">Seleccione el tipo de pago</option>
                  <option value="transferencia">Transferencia</option>
                  <option value="punto_venta">Punto de Venta</option>
                </select>
              </div>
              <div class="col-md-6 mb-4" v-if="tipo_pago=='punto_venta'">
                <strong >
                  <i class="fa fa-asterisk"></i>
                  Número de Lote:
                </strong>
                <input type="text" @keypress="onlyNumber" :maxlength="10" v-model="soporte_pago" class="form-control" >
              </div>
              <div class="col-md-6 mb-4">
                <strong>
                  <i class="fa fa-asterisk"></i>
                  Referencia de Pago:
                </strong>
                <input type="text" @keypress="onlyNumberWithOutDecimalPoint" :maxlength="12" v-model="referencia_pago" class="form-control" >
              </div>
              <div class="col-md-6 mb-4">
                <strong>
                  <i class="fa fa-asterisk"></i>
                  Monto:
                </strong>
                <money v-model="monto_pago" class="form-control"></money>
                <!-- <input type="text" @keypress="onlyNumber" v-model="monto_pago" :maxlength="15" class="form-control" > -->
              </div>
              <div class="col-md-6" v-if="tipo_pago=='transferencia'">
                <strong >
                  <i class="fa fa-asterisk"></i>
                  Soporte de Pago:
                </strong>
                <input type="file" @change="convertirBase64SoportePago" accept="image/jpeg" class="form-control" >
              </div>
            </div>
            <!-- BOTONES -->
            <div class="mx-auto mb-2">
              <br>
              <button type="button" name="button" class="btn btn-success" @click="asociarPago()">Registrar</button>
              <!-- <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal" @click="limpiar()">Cancelar</button> -->
            </div>
            <!-- BOTONES -->
          </div>
          <div class="col-md-12 text-center" v-else-if="pago_index!=null && pago_editar">
            <h3>DETALLE DEL PAGO</h3>
            <div class="row">
              <div class="col-md-6 mb-4">
                <strong>
                  <i class="fa fa-asterisk"></i>
                  Tipo de Pago:
                </strong>
                <select class="form-control" v-model="Oficios[indexOficio].oferta_economica.pagos[pago_index].tipo_pago">
                  <option value="0">Seleccione el tipo de pago</option>
                  <option value="transferencia">Transferencia</option>
                  <option value="punto_venta">Punto de Venta</option>
                </select>
              </div>
              <div class="col-md-6 mb-4" v-show="Oficios[indexOficio].oferta_economica.pagos[pago_index].tipo_pago=='punto_venta'">
                <strong >
                  <i class="fa fa-asterisk"></i>
                  Número de lote:
                </strong>
                <input type="text" v-model="Oficios[indexOficio].oferta_economica.pagos[pago_index].soporte_lote" @keypress="onlyNumber" :maxlength="8" class="form-control" >
              </div>
              <div class="col-md-6 mb-4">
                <strong>
                  <i class="fa fa-asterisk"></i>
                  Referencia de Pago:
                </strong>
                <input type="text" @keypress="onlyNumber" :maxlength="20" v-model="Oficios[indexOficio].oferta_economica.pagos[pago_index].referencia" class="form-control" >
              </div>
              <div class="col-md-6 mb-4">
                <strong>
                  <i class="fa fa-asterisk"></i>
                  Monto:
                </strong>
                <money v-model="Oficios[indexOficio].oferta_economica.pagos[pago_index].monto" class="form-control"></money>
                <!-- <input type="text" @keypress="onlyNumber" v-model="Oficios[indexOficio].oferta_economica.pagos[pago_index].monto" :maxlength="15" class="form-control" > -->
              </div>
              <div class="col-md-6" v-show="Oficios[indexOficio].oferta_economica.pagos[pago_index].tipo_pago=='transferencia'">
                <strong >
                  <i class="fa fa-asterisk"></i>
                  Soporte de Pago:
                </strong>
                <input  type="file" @change="convertirBase64SoportePagoEditar" accept="image/*" class="form-control" >
              </div>
              <div class="col-md-12 mb-4 text-center" v-if="Oficios[indexOficio].oferta_economica.pagos[pago_index].tipo_pago=='transferencia' && Oficios[indexOficio].oferta_economica.pagos[pago_index].soporte_lote!=null">
                <img :src="Oficios[indexOficio].oferta_economica.pagos[pago_index].soporte_lote" style="max-heigth:300px!important;max-width:100%" >
              </div>
            </div>
            <!-- BOTONES -->
            <div class="mx-auto pt-2 pb-5">
              <br>
              <button type="button" name="button" class="btn btn-success" @click="actualizarPago()">Actualizar</button>
              <button type="button" class="btn btn-danger font-weight-bold" @click="limpiar()">Cancelar</button>
            </div>
            <!-- BOTONES -->
          </div>
        </div>
        <!-- LISTADO PAGOS -->
        <div class="row table-responsive">
          <table class="table table-bordered text-center">
            <thead class="bg-primary text-white">
              <th>N° de Pago</th>
              <th>Referencia</th>
              <th>Monto</th>
              <th>Validado</th>
              <th>Acción</th>
            </thead>
            <tbody>
              <tr v-for="(pago,index) in this.Oficios[indexOficio].oferta_economica.pagos">
                <td>@{{index+1}}</td>
                <td>@{{pago.referencia}}</td>
                <td>@{{formatearNumero(pago.monto)}}</td>
                <td v-if="pago.validacion">
                  <i title="Pago confirmado" class="bg-success text-white fa fa-check"></i>
                </td>
                <td v-else>
                  <i title="Pago sin confirmar" class="bg-danger text-white fa fa-clock-o"></i>
                </td>
                <td class="text-center">
                  <button type="button" v-if="!pago.validacion" title="Editar pago" @click="pago_index=index;pago_editar=true" class="btn btn-info" name="button"> <i class="fa fa-pencil-square-o"></i></button>
                  {{--
                    <button type="button" v-if="!pago.validacion" title="Agregar mas soportes" @click="pago_index=index;pago_editar=true;agregarMasSoportes()" data-toggle="modal" data-target="#masSoportes" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" class="btn btn-info" name="button"> <i class="fa fa-plus"></i> </button> -->
                  --}}
                  <button type="button" v-if="!pago.validacion" title="Eliminar pago" @click="eliminarPago(pago.id)" class="btn btn-danger" name="button"> <i class="fa fa-trash"></i> </button>
                  <button type="button" v-if="pago.validacion"  title="Ver pago" @click="pago_index=index;pago_editar=false;" data-toggle="modal" data-target="#verPago" data-backdrop="static" data-keyboard="false"  data-toggle="tooltip" class="btn btn-info" name="button"> <i class="fa fa-search"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- LISTADO PAGOS -->
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">

      </div>
    </div>
  </div>
</div>
