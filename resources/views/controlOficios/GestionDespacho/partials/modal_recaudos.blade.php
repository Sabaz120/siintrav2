<div class="modal" id="recaudos" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">LISTADO DE RECAUDOS</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row" v-if="Oficios[indexOficio].requisicion">
          <div class="col-12" >
            <table class="table table-bordered text-justify">
              <thead>
                <tr>
                  <td>N°</td>
                  <!-- <td>Descripción</td> -->
                  <td>Acción</td>
                </tr>
              </thead>
              <tbody>
                <tr v-for="recaudo in Oficios[indexOficio].requisicion.recaudos">
                  <td>@{{recaudo.recaudos_id}}</td>
                  <!-- <td>@{{recaudo.recaudo.nombre}}</td> -->
                  <td>
                    <div class="custom-control custom-checkbox">
                    <input type="checkbox" v-model="recaudo.entregado" class="custom-control-input" :id="recaudo.recaudo.id">
                    <label class="custom-control-label" :for="recaudo.recaudo.id">@{{recaudo.recaudo.nombre}}</label>
                  </div>
                  <!-- <input type="checkbox" v-model="recaudo.entregado" value=""> -->
                </td>
              </tr>
            </tbody>
          </table>
        </div>

      </div>
    </div>
    <!-- Modal footers -->
    <div class="modal-footer mx-auto table-responsive">
      <button type="button" class="btn btn-success" @click="actualizarRecaudos" name="button">Guardar</button>
    </div>
  </div>
</div>
</div>
