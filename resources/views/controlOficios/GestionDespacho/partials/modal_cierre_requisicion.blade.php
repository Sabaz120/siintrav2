<div class="modal" id="cierreRequisicion" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg" v-if="Oficios[indexOficio].despacho!=null">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">CIERRE DE REQUISICIÓN</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">

          <div class="col-12 form-group text-center">
            <h4>DATOS DE LA REQUISICIÓN</h4>
          </div>

          <div class="col-6 form-group text-center">
              <label class="font-weight-bold">Número de Requisición</label>
              <input type="text" class="form-control" readonly v-model="Oficios[indexOficio].num_requisicion">
          </div>

          <div class="col-6 form-group text-center">
              <label class="font-weight-bold">Cliente</label>
              <input type="text" class="form-control" readonly v-model="Oficios[indexOficio].cliente">
          </div>

          <div class="col-12 form-group text-center">
            <h4>DATOS DE LA FACTURA</h4>
          </div>

          <div class="col-6 form-group text-center">
            <label class="font-weight-bold">Número de Factura</label>
            <input type="text" class="form-control" readonly v-if="Oficios[indexOficio].num_factura" v-model="Oficios[indexOficio].num_factura">
            <input type="text" class="form-control" readonly v-else value="No posee">
          </div>

          <div class="col-6 form-group text-center">
              <label class="font-weight-bold">Cliente</label>
              <input type="text" class="form-control" readonly v-model="Oficios[indexOficio].cliente">
          </div>

          <div class="col-6 form-group text-center">
              <label class="font-weight-bold">Monto Total a Cancelar</label>
              <input type="text" class="form-control" readonly v-model="Oficios[indexOficio].totalCancelar">
          </div>

          <div class="col-12 form-group text-center">
            <hr>
            <strong>Observación Final</strong>
            <textarea name="name" v-model="observacionFinal" rows="4" cols="80"></textarea>
          </div>

        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer justify-content-center table-responsive">
        <button type="button" class="btn btn-success" v-if="Oficios[indexOficio].despacho.fecha_despacho" @click="cierreRequisicion" name="button">Cierre de Evento</button>
      </div>
    </div>
  </div>
</div>
