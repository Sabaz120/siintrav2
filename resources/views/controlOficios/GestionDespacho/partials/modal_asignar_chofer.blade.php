<div class="modal" id="asignarChofer" v-if="Oficios.length>0">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header bg-primary text-center">
        <h4 class="modal-title mx-auto text-white">RESPONSABLE DE RETIRO</h4>
        <button @click="limpiar" type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row text-center" v-if="Oficios[indexOficio].despacho==null">
          <div class="col-md-12 form-group">
            <h3>DATOS DEL CHOFER</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4">
              <input type="text" v-model="cedula_chofer" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(cedula_chofer,'chofer','registrar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Nombres:</strong>
            <input type="text" :readonly="readChofer == true" v-model="chofer_nombres" class="form-control" >
          </div>

          <div class="col-md-4 from-group">
            <i class="fa fa-asterisk"></i>
            <strong>Apellidos:</strong>
            <input type="text" :readonly="readChofer == true" v-model="chofer_apellidos" class="form-control" >
          </div>

          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Teléfono de Contacto:</strong>
            <input type="text" :readonly="readChofer == true" v-model="chofer_numero" class="form-control" maxlength="11">
          </div>

          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Correo Electónico:</strong>
            <input type="text" :readonly="readChofer == true" v-model="chofer_correo" class="form-control" >
          </div>
          <!-- <div class="col-md-12 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Destino del Despacho:</strong>
            <input type="text" v-model="destino_despacho" class="form-control" maxlength="80">
          </div> -->
          <div class="col-md-12 form-group">
            <h3>DATOS DEL VEHÍCULO</h3>
          </div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Placa del Vehículo:</strong>
            <div class="input-group mb-4">
              <input type="text" name="" v-model="vehiculo_placa" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarVehiculo(vehiculo_placa,'registrar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Modelo de Vehículo:</strong>
            <input type="text" :readonly="readVehiculo == true"  v-model="vehiculo_modelo" class="form-control" >
          </div>
          <div class="col-md-12 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Descripción</strong>
            <input type="text" :readonly="readVehiculo == true"  v-model="vehiculo_descripcion" class="form-control" >
          </div>
          <!-- <div class="col-md-12 form-group">
            <h3>DATOS DEL RESPONSABLE</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4">
              <input type="text" name="" v-model="cedula_responsable" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(cedula_responsable,'responsable','registrar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Nombre y Apellido:</strong>
            <input type="text" readonly v-model="responsable_nombres_apellidos" class="form-control" >
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Número de Contacto:</strong>
            <input type="text" readonly v-model="responsable_numero" class="form-control" >
          </div> -->
          <!-- <div class="col-md-12 form-group">
            <h3>BENEFICIARIOS</h3>
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Listado de Beneficiarios:</strong>
            <input type="file" name="file1" id="1" @change="convertirBase64Beneficiarios" accept="application/pdf" class="form-control" >
          </div> -->
          <div class="mx-auto">
            <br>
            <!-- <button type="button" name="button" class="btn btn-success" @click="registrarChofer()">Registrar</button> -->
            <button type="button" name="button" class="btn btn-success" :disabled="btnRegistroChofer" @click="registrarDatosChofer()">Registrar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal" @click="limpiar">Cancelar</button>
          </div>
        </div>

        <div class="row text-center" v-else>
          <div class="col-md-12 form-group">
            <h3>DATOS DEL CHOFER</h3>
          </div>
          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Cédula:</strong>
            <div class="input-group mb-4">
              <input type="text" name="" v-model="Oficios[indexOficio].despacho.chofer.cedula" v-mask="'########'" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarPersonaNatural(Oficios[indexOficio].despacho.chofer.cedula,'chofer','editar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>

          <div class="col-md-4 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Nombres:</strong>
            <input type="text"  v-model="Oficios[indexOficio].despacho.chofer.nombres" class="form-control" >
          </div>

          <div class="col-md-4 from-group">
            <i class="fa fa-asterisk"></i>
            <strong>Apellidos:</strong>
            <input type="text"  v-model="Oficios[indexOficio].despacho.chofer.apellidos" class="form-control" >
          </div>


          <div class="col-md-6 form-group" >
            <i class="fa fa-asterisk"></i>
            <strong>Teléfono de Contacto:</strong>
            <input type="text"v-model="Oficios[indexOficio].despacho.chofer.telefono" class="form-control" maxlength="11">
          </div>

          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Correo Electónico::</strong>
            <input type="text" v-model="Oficios[indexOficio].despacho.chofer.correo_electronico" class="form-control" >
          </div>
          <!-- <div class="col-md-12 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Destino del Despacho:</strong>
            <input type="text" v-model="Oficios[indexOficio].despacho.destino" class="form-control" maxlength="80">
          </div> -->
          <div class="col-md-12 form-group">
            <h3>DATOS DEL VEHÍCULO</h3>
          </div>
          <div class="col-md-6 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Placa del Vehículo:</strong>
            <div class="input-group mb-4" >
              <input type="text" name="" v-model="Oficios[indexOficio].despacho.vehiculo.placa" class="form-control" value="">
              <div class="input-group-append">
                <button class="btn btn-info" @click="buscarVehiculo(Oficios[indexOficio].despacho.vehiculo.placa,'editar')" type="button"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-md-6 form-group" >
            <i class="fa fa-asterisk"></i>
            <strong>Modelo de Vehículo:</strong>
            <input type="text"   v-model="Oficios[indexOficio].despacho.vehiculo.modelo" class="form-control" >
          </div>

          <div class="col-md-12 form-group">
            <i class="fa fa-asterisk"></i>
            <strong>Descripción</strong>
            <input type="text"  v-model="Oficios[indexOficio].despacho.vehiculo.descripcion" class="form-control" >
          </div>
          
          <div class="mx-auto">
            <br>
            <button type="button" name="button" class="btn btn-success" @click="actualizarChofer()">Actualizar</button>
            <button type="button" class="btn btn-danger font-weight-bold" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
      <!-- Modal footers -->
      <div class="modal-footer mx-auto table-responsive">

      </div>
    </div>
  </div>
</div>
