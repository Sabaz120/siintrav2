@extends('layouts.app')
@section('contenido')
<div class="wrapper mt-5 mb-5">
  <div class="container-fluid">
    <!-- Set up your HTML -->
    <!-- <div class="owl-carousel text-center">
      <div style="background:#4DC7A0">
        asdas
       </div>
      <div> Your Content </div>
      <div> Your Content </div>
      <div> Your Content </div>
      <div> Your Content </div>
      <div> Your Content </div>
      <div> Your Content </div>
    </div> -->

  </div> <!-- end container -->
</div>
<!-- end wrapper -->
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
      0:{
        items:1,
        nav:true
      },
      600:{
        items:3,
        nav:false
      },
      1000:{
        items:5,
        nav:true,
        loop:false
      }
    }
  });
});
</script>
@endpush
