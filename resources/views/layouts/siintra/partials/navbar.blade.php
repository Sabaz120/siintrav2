@php
if(Auth::guest()){
  //redireccionar();
  //dd('testxx');
}
@endphp
<style media="screen">
.modal-footer .bg-danger{
  background-color: rgba(239, 83, 80, 0.75) !important;
}
@media (max-width: 767px) {
  .navbar-toggleable-sm {
    clear: both;
  }
}
.dropdown-submenu {
  position: relative;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -1px;
}

</style>
<nav class="navbar navbar-expand-sm bg-primary navbar-light bg-light ">
  <a class="navbar-brand" href="{{('/home')}}" data-toggle="tooltip" data-placement="top" title="Ir al inicio">
    <i class="fa fa-home fa-2x text-white" aria-hidden="true"></i>
  </a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar1">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbar1">
    <ul class="navbar-nav">
      @can('siintra_menu_control_oficios')
      <li class="nav-item dropdown">
        <a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle text-white font-weight-bold">Control de Oficios</a>
        <ul class="dropdown-menu">
          <!-- MÓDULOS BÁSICOS CONTROL OFICIO -->
          @if(Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_motivo_solicitud')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_clasificacion_ente')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_entes')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_registrar_personal_natural')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_registrar_personal_juridico')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_departamentos')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_asignar_responsables')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_registrar_vehiculos')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_materiales')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_observaciones_generales') )
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Módulos Administrativos</a>
            <ul class="dropdown-menu">
              @can('siintra_menu_control_oficios_gestion_motivo_solicitud')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/gestionar_motivos')}}">Gestionar Motivo de Solicitud</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_clasificacion_ente')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/clasificacion_ente')}}">Gestionar Clasificación de Ente</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_entes')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/gestionar_entes')}}">Gestionar Ente</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_registrar_personal_natural')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('gestionar_personal_natural')}}">Gestionar Personal Natural</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_registrar_personal_juridico')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('registrar_personal_juridico')}}">Gestionar Empresa</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_departamentos')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/gestionar_departamentos')}}">Gestión de Departamentos</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_asignar_responsables')
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('GestionResponsables')}}">Gestionar Responsables</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_registrar_vehiculos')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('gestionar_vehiculos')}}">Gestionar Vehículos</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_materiales')
              <li class="dropdown-item">
                <a  href="{{route('GestionarMateriales')}}" class="text-dark">Gestión de Materiales</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_observaciones_generales')
              <li class="dropdown-item">
                <a  href="{{route('ObservacionesGenerales')}}" class="text-dark">Observaciones y Condiciones Generales de Compra</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_recaudos')
              <li class="dropdown-item">
                <a  href="{{route('GestionarRecaudos')}}" class="text-dark">Gestión de Recaudos</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_deducciones')
              <li class="dropdown-item">
                <a  href="{{route('GestionarDeducciones')}}" class="text-dark">Gestión de Deducciones</a>
              </li>
              @endcan
            </ul>
          </li>
          @endif
          <!-- MÓDULOS BÁSICOS CONTROL OFICIO -->
          <!-- MÓDULO RECEPCIÓN DE OFICIO CONTROL OFICIO -->
          @can('siintra_menu_control_oficios_recepcion_de_oficios')
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Recepción de Oficios</a>
            <ul class="dropdown-menu">
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('recepcion_oficios')}}">Gestionar Recepción de Oficios</a>
              </li>
            </ul>
          </li>
          @endcan
          <!-- MÓDULO RECEPCIÓN DE OFICIO CONTROL OFICIO -->
          @if(Auth::user()->hasPermissionTo('siintra_menu_control_oficios_verificacion_solicitudes')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_solicitudes')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestionar_solicitud_de_donativos_aprobados')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_verificacion_solicitudes_donativos')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_verificacion_listado_donaciones_despachar')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_verificacion_listado_donaciones_verificar')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_listado_reintegro')
          || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_ofertas_economicas_facturar') )
          <!-- MÓDULOS SOLICITUDES RECEPCIÓN DE OFICIOS-->
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Procesar Solicitudes</a>
            <ul class="dropdown-menu">
              @can('siintra_menu_control_oficios_verificacion_solicitudes')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('verificacion_solicitudes')}}">Configuración de Plan de Venta.</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_solicitudes')
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('GestionSolicitudes')}}">Gestión de Solicitudes (Jornadas de Equipos)</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestionar_solicitud_de_donativos_aprobados')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('gestion_solicitudes_donativos_aprobadas')}}">Gestionar Donativos</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_verificacion_solicitudes_donativos')
              <li class="dropdown-item">
                <a class="text-dark" href="{{ route('GestionSolicitudesDonativos') }}">Verificación de Donativos</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_verificacion_listado_donaciones_despachar')
              <li class="dropdown-item">
                <a class="text-dark" href="{{ route('listado_donaciones_a_despachar') }}">Listado de Donaciones Por Despachar</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_verificacion_listado_donaciones_verificar')
              <li class="dropdown-item">
                <a class="text-dark" href="{{ route('listado_donaciones_verificar') }}">Listado de Donaciones Por Verificar</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_listado_reintegro')
              <li class="dropdown-item">
                <a class="text-dark" href="{{ route('listadoReintegro') }}">Listado de Reintegro</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_ofertas_economicas_facturar')
              <li class="dropdown-item">
                <a class="text-dark" href="{{ route('listaFacturacion') }}">Ofertas Económicas para Facturar</a>
              </li>
              @endcan
            </ul>
          </li>
          @endif
          <!-- MÓDULOS SOLICITUDES RECEPCIÓN DE OFICIOS-->
          <!-- MÓDULO DESPACHO DE SOLICITUDES -->
          @if( Auth::user()->hasPermissionTo('siintra_menu_control_oficios_resumen_despacho') || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_gestion_requisicion')
           || Auth::user()->hasPermissionTo('siintra_menu_control_oficios_correccion_requisicion'))
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Despacho de Solicitudes</a>
            <ul class="dropdown-menu">
              @can('siintra_menu_control_oficios_resumen_despacho')
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('resumenDespacho')}}">Resumen de Despacho</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_gestion_requisicion')
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('gestionDespacho')}}">Gestión de Requisición</a>
              </li>
              @endcan
              @can('siintra_menu_control_oficios_correccion_requisicion')
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('correccionRequisicion')}}">Corrección de Requisición</a>
              </li>
              @endcan
            </ul>
          </li>
          @endif
          <!-- MÓDULOS SOLICITUDES RECEPCIÓN DE OFICIOS-->
          @can('siintra_menu_control_oficios_reporte_oficios')
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Reportes</a>
            <ul class="dropdown-menu">
             @can('siintra_menu_control_oficios_reporte_seguimiento_recepcion_oficios')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('reportes/recepcion_oficios')}}">Seguimiento de Recepción de Oficios</a>
              </li>
             @endcan
             
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('reportes/recepcion_oficios_atendidos')}}">Reporte de listado de oficios atendidos</a>
              </li>
             
             @can('siintra_menu_control_oficios_reporte_disponibilidad_productos')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('reportes/disponibilidad_productos')}}">Disponibilidad de Productos</a>
              </li>
             @endcan
             @can('siintra_menu_control_oficios_listado_de_donaciones')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('listado_donaciones')}}">Listado de Donaciones</a>
              </li>
             @endcan
             @can('siintra_menu_control_oficios_reporte_verificacion_de_pago')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('verificacion_pago')}}">Verificación de Pago</a>
              </li>
             @endcan
             @can('siintra_menu_control_oficios_reporte_expediente_solicitudes')
             <li class="dropdown-item">
               <a class="text-dark" href="{{route('reporte.expediente.solicitudes')}}">Expediente de Solicitudes</a>
             </li>
             @endcan
            </ul>
          </li>
          @endcan
        </ul>
      </li>
      @endcan

      <!-- 2 menu VIDAMED-->
      @can('siintra_menu_vidamed')
      <li class="nav-item dropdown">
        <a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle text-white font-weight-bold">VIDAMED</a>
        <ul class="dropdown-menu">
          @can('siintra_menu_vidamed_sincronizar')
          <li class="dropdown-item">
            <a class="text-dark" href="{{url('/vidamed/sincronizar_trabajador')}}">Sincronizar Trabajadores SIGESP con VIDAMED</a>
          </li>
          @endcan
          @can('siintra_menu_vidamed_sincronizar')
          <li class="dropdown-item">
            <a class="text-dark" href="{{url('/vidamed/sincronizar_familiares')}}">Sincronizar Familiares de Trabajadores con VIDAMED</a>
          </li>
          @endcan
          <!-- <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Comercialización</a>
            <ul class="dropdown-menu">
              <li class="dropdown-item">
                <a  href="{{url('comercializacion')}}" class="text-dark">Recepción de equipos</a>
              </li>
            </ul>
          </li> -->
        </ul>
      </li>
      @endcan
      @can('siintra_menu_gestion_social')
      <li class="nav-item dropdown">
        <a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle text-white font-weight-bold">Gestion social</a>
        <ul class="dropdown-menu">
          @if(Auth::user()->hasPermissionTo('siintra_menu_gestion_social_clasificacion_productos')
          || Auth::user()->hasPermissionTo('siintra_menu_gestion_social_gestion_productos')
          || Auth::user()->hasPermissionTo('siintra_menu_gestion_social_registro_jornada')
          || Auth::user()->hasPermissionTo('siintra_menu_gestion_social_entrega_beneficios') )
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Modulo Administrativo</a>
            <ul class="dropdown-menu">
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/gestionarBiometricos')}}">Gestionar Biometricos</a>
              </li>
              @can('siintra_menu_gestion_social_clasificacion_productos')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/clasificacionProducto')}}">Clasificación de productos</a>
              </li>
              @endcan

              @can('siintra_menu_gestion_social_gestion_productos')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/gestionarProductos')}}">Gestión de productos</a>
              </li>
              @endcan
              @can('siintra_menu_gestion_social_registro_jornada')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/registroJornada')}}">Registro de jornada</a>
              </li>
              @endcan
              @can('siintra_menu_gestion_social_entrega_beneficios')
              <li class="dropdown-item">
                <a class="text-dark" href="{{url('/entregaBeneficios')}}" target="_blank">Entrega de beneficios</a>
              </li>
              @endcan
              @can('siintra_menu_gestion_social_entrega_beneficios')
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('gestionSocial.index')}}" target="_blank">Reporte</a>
              </li>
              @endcan
            </ul>
          </li>
          @endif
        </ul>
      </li>
      @endcan
      @can('siintra_menu_premium')
      
      <li class="nav-item dropdown">
        
        <a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle text-white font-weight-bold">Premium</a>
        <ul class="dropdown-menu">
            <li class="nav-item">
                <a class="text-dark" href="{{route('premium.partepieza')}}">Buscar precio PP</a>
              </li>
          @if(Auth::user()->hasPermissionTo('siintra_menu_premium'))
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Reportes</a>
            <ul class="dropdown-menu">
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('premium.libro_ventas')}}">Libro de Ventas</a>
              </li>
            </ul>
          </li>
          @endif
          <li class="dropdown-item dropdown-submenu">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-dark">Petro</a>
            <ul class="dropdown-menu">
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('premium.factorpp')}}">Actualización factor PP</a>
              </li>
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('premium.petro')}}">Actualización equipo</a>
              </li>
              <li class="dropdown-item">
                <a class="text-dark" href="{{route('premium.gestion.precio')}}">Gestión Precio Petro</a>
              </li>
              
            </ul>
          </li>
        </ul>

      </li>
      @endcan


      @can('siintra_menu_agente_autorizado')
      <li class="nav-item dropdown">
        <a href="#" id="menu" data-toggle="dropdown" class="nav-link dropdown-toggle text-white font-weight-bold">Agente autorizado</a>
        <ul class="dropdown-menu">

          @if(Auth::user()->hasPermissionTo('siintra_menu_agente_autorizado_solicitud_almacen'))
          <li class="dropdown-item">
            <a class="text-dark" href="{{url('/solicitudes_agente_autorizado')}}">Solicitud Almacén</a>
          </li>
          @endif

        </ul>
      </li>
      @endcan


    </ul>





    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link text-white dropdown-toggle font-weight-bold" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>  {{Auth::user()->email}}</a>
        <ul class="dropdown-menu">
          <li class="dropdown-item">
            <a class="text-dark" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Cerrar sesión
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </li>
  </ul>
</div>
</nav>


<br>
