<style media="screen">
.modal-footer .bg-danger{
  background-color: rgba(239, 83, 80, 0.75) !important;
}
@media (max-width: 767px) {
  .navbar-toggleable-sm {
    clear: both;
  }
}
</style>

<nav class="navbar navbar-expand-sm bg-primary navbar-dark sticky-top mb-5">
  <a class="navbar-brand font-weight-normal font-italicfont-italic" href="#"> <i class="fas fa-home fa-2x"></i></a>
  <ul class="navbar-nav font-weight-bold">
    <!-- <li class="nav-item">
    <a class="nav-link  text-white" href="#">Configuración básica</a>
  </li> -->

  @can('siintra_menu_control_oficios')
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle text-white" href="#" id="navbardrop" data-toggle="dropdown">
      Control de Oficios
    </a>
    <div class="dropdown-menu">
      @can('siintra_menu_control_oficios_gestion_materiales')
      <a class="dropdown-item" href="{{route('GestionarMateriales')}}">Gestión de Materiales</a>
      @endcan
      @can('siintra_menu_control_oficios_gestion_motivo_solicitud')
      <a class="dropdown-item" href="{{url('/gestionar_motivos')}}">Gestión de Motivo de Solicitud</a>
      @endcan
      @can('siintra_menu_control_oficios_gestion_clasificacion_ente')
      <a class="dropdown-item" href="{{url('/clasificacion_ente')}}">Gestión de Clasificación de Ente</a>
      @endcan
      @can('siintra_menu_control_oficios_gestion_entes')
      <a class="dropdown-item" href="{{url('/gestionar_entes')}}">Gestión de Entes</a>
      @endcan
      @can('siintra_menu_control_oficios_gestion_departamentos')
      <a class="dropdown-item" href="{{url('/gestionar_departamentos')}}">Gestión de Departamentos</a>
      @endcan
      @can('siintra_menu_control_oficios_registrar_personal_natural')
      <a class="dropdown-item" href="{{url('gestionar_personal_natural')}}">Registrar Personal Natural</a>
      @endcan
      @can('siintra_menu_control_oficios_registrar_personal_juridico')
      <a class="dropdown-item" href="{{url('registrar_personal_juridico')}}">Registrar Personal Jurídico</a>
      @endcan
      @can('siintra_menu_control_oficios_registrar_vehiculos')
      <a class="dropdown-item" href="{{url('gestionar_vehiculos')}}">Registrar Vehículos</a>
      @endcan
      @can('siintra_menu_control_oficios_asignar_responsables')
      <a class="dropdown-item" href="{{route('GestionResponsables')}}">Asignar Responsables por Empresa</a>
      @endcan
      @can('siintra_menu_control_oficios_recepcion_de_oficios')
      <a class="dropdown-item" href="{{url('recepcion_oficios')}}">Recepción de Oficios</a>
      @endcan
      @can('siintra_menu_control_oficios_verificacion_solicitudes')
      <a class="dropdown-item" href="{{url('verificacion_solicitudes')}}">Verificacion de Solicitudes (Aprobación/Rechazo)</a>
      @endcan
      @can('siintra_menu_control_oficios_gestion_solicitudes')
      <a class="dropdown-item" href="{{route('GestionSolicitudes')}}">Gestión de Solicitudes (Jornadas de Equipos)</a>
      @endcan
      @can('siintra_menu_control_oficios_verificacion_solicitudes_donativos')
      <a class="dropdown-item" href="{{ route('GestionSolicitudesDonativos') }}">Verificación de Solicitudes de Donativos</a>
      @endcan
      @can('siintra_menu_control_oficios_reporte_oficios')
      <a class="dropdown-item" href="{{url('reportes/recepcion_oficios')}}">Reporte de Oficios</a>
      @endcan
     <!-- <a class="dropdown-item" href="{{url('gestion_solicitudes_donativos_aprobadas')}}">Gestión de Solicitudes de Donativos Aprobadas</a>-->
    </div>
  </li>
  @endcan
  @can('siintra_menu_vidamed')
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle text-white" href="#" id="navbardrop" data-toggle="dropdown">
      VIDAMED
    </a>
    <div class="dropdown-menu">
      @can('siintra_menu_vidamed_sincronizar')
      <a class="dropdown-item" href="{{url('/vidamed/sincronizar_trabajador')}}">Sincronizar Trabajadores SIGESP con VIDAMED</a>
      <a class="dropdown-item" href="{{url('/vidamed/sincronizar_familiares')}}">Sincronizar Familiares de Trabajadores con VIDAMED</a>
      @endcan
    </div>
  </li>
  @endcan
</ul>
<ul class="navbar-nav ml-auto">
  <li class="nav-item dropdown">
    <a class="nav-link text-white dropdown-toggle font-weight-bold" data-toggle="dropdown" href="#"><i class="fa fa-user"></i>  {{Auth::user()->email}}</a>
    <ul class="dropdown-menu">
      <li class="dropdown-item">
        <a class="text-dark" href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        Cerrar sesión
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
    </li>
  </ul>
</li>
</ul>
</nav>
<br>
