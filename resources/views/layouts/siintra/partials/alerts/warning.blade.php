@if(Session::has('error'))
<div class="alert alert-danger " id="errorMessage" role="alert " style="z-index: 100000000000000;position: fixed;">
  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('error')}}
</div>
@endif


<div class="alert-2 alert-danger-2 " id="mensajeGenerico" role="alert " style="z-index: 100000000000000;position: fixed;display:none">
  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <div id="mensajeMostrar">asasasas</div>
</div>
@if (count($errors) > 0)
  <div class="alert alert-danger" style="z-index: 100000000000000;position: fixed;" >
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

<div id="error-client">

</div>
