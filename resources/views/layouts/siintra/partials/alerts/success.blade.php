@if(Session::has('mensaje'))
<div class="alert alert-success" role="alert" style="z-index: 100000000000000;position: fixed;">
<a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('mensaje')}}
</div>
@endif
