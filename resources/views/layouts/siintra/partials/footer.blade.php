<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-right">
                {{date('Y')}} © Venezolana de Telecomunicaciones C.A
            </div>
        </div>
    </div>
</footer>
