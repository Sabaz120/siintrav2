<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
  <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Panel Administrativo</title>
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/icons.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
  <link href="{{ asset('plugins/font-awesome/css/all.min.css') }}" rel="stylesheet">
  <!-- Styles alertify-->
  <link href="{{ asset('plugins/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
  <!-- Styles alertify-->
  <link href="{{ asset('plugins/alertifyjs/css/themes/default.min.css') }}" rel="stylesheet">
  <!-- Styles alertify-->
  <link href="{{ asset('plugins/alertifyjs/css/themes/semantic.min.css') }}" rel="stylesheet">
  <!-- Styles alertify-->
  <link href="{{ asset('plugins/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ URL::asset('css/datepicker.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/jquery.timepicker.css') }}">
  <!-- <script src="{{ asset('highchartsCronos/highcharts.js') }}"></script> -->
  <!-- <script src="{{ asset('highchartsCronos/exporting.js') }}"></script> -->


  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-select.css') }}">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" /> -->

  <link rel="stylesheet" href="{{ URL::asset('css/owlCarousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/owlCarousel/owl.theme.default.min.css') }}">

  <style media="screen">
  .btn-circle {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
  }

  .modal-xl{
    max-width: 1140px!important;
  }

  </style>
  @stack('styles')
</head>
@include('layouts.siintra.partials.header')
<body class="skin-default-dark fixed-layout pt-5 pb-5">
  <div id="main-wrapper">
    <div class="container-fluid pb-5 pt-5">
      @include('layouts.siintra.partials.navbar')
      @include('alerts.warning')
      @yield('contenido')
    </div>
  </div>
  @include('layouts.siintra.partials.footer')
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/funciones.js') }}"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script> -->
  <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> -->

  <!-- <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script> -->

  <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('datepicker/locales/bootstrap-datepicker.es.min.js') }}"></script>
  <script src="{{ asset('js/jquery.timepicker.js') }}"></script>
  <script src="{{ asset('plugins/vueMask/v-mask.min.js') }}"></script>
  <script src="{{ asset('plugins/alertifyjs/js/alertify.min.js') }}"></script>
  <script src="{{ asset('plugins/vue2-filters/vue2-filters.min.js') }}"></script>
  <script src="{{ asset('js/vue-h-zoom.min.js') }}"></script>
  <script src="{{ asset('js/owlCarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/jquery.mask.min.js') }}"></script>

  <script>
  $('.dropdown-submenu > a').on("click", function(e) {
    var submenu = $(this);
    $('.dropdown-submenu .dropdown-menu').removeClass('show');
    submenu.next('.dropdown-menu').addClass('show');
    e.stopPropagation();
  });

  $('.dropdown').on("hidden.bs.dropdown", function() {
    // hide any open menus when parent closes
    $('.dropdown-menu.show').removeClass('show');
  });



  alertify.set('notifier','position', 'top-right');
  Vue.use(VueMask.VueMaskPlugin);
  $.fn.datepicker.defaults.format = "dd-mm-yyyy";
  // $.fn.datepicker.defaults.endDate = "+0d";
  // comentada 22 06 2021 inicio de 90 dias antes
  // $.fn.datepicker.defaults.startDate = "-90d";
  $.fn.datepicker.defaults.language = "es";
  // $.fn.datepicker.defaults.orientation = "bottom left";
  $('.datepicker').datepicker({
    orientation: "bottom auto"
  });
  function isObject (value) {
    return value && typeof value === 'object' && value.constructor === Object;
  }//isObject
  </script>
  @stack('scripts')
</body>
</html>
