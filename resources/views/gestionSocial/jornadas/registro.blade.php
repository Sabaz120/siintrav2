@extends('layouts.app')
@section('contenido')
<section id="regristroJornada">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">REGISTRO DE JORNADAS</h5>
      <div class="row justify-content-justify">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="clasificacionEnte">Fecha Inicio:</label>
            <input type="date" class="form-control" v-model="fechaInicio">
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="nombre">Fecha Fin:</label>
            <input type="date" class="form-control" v-model="fechaFin">
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="descripcion">Nombre de la jornada:</label>
            <input type="text" class="form-control" v-model="nombreJornada">
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="nivelAtencion">Lugar de la Jornada:</label>
            <input type="text" class="form-control" v-model="lugarJornada">
          </div>
        </div>

        <div class="col-12 col-md-6 col-lg-6">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="nivelAtencion">Hora de la Jornada:</label>
            <input type="time" class="form-control" v-model="horaJornada">
          </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
          <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrarJornada">Registrar</button>
          <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
        </div>
      </div>

      <div class="row pt-5">
        <div class="mx-auto">
          <h3 class="text-dark">Listado de jornadas</h3>
        </div>

        <div class="table">
          <div class="row pt-5">
            <div class="col-12 col-md-9 col-lg-9">
              <br>
              <div class="mt-2 form-inline font-weight-bold">
                Mostrar
                <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                  <option v-for="option in optionspageSize" v-bind:value="option.value">
                    @{{ option.text }}
                  </option>
                </select>
                registros
              </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
              <div class="form-group">
                <label for="Buscar" class="font-weight-bold">Buscar:</label>
                <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
              </div>
            </div>
          </div>
          <table class="table table-bordered  text-center">
            <thead class=" bg-primary text-white ">
              <tr>
                <th>Fecha inicio</th>
                <th>Fecha Fin</th>
                <th>Nombre</th>
                <th>Lugar</th>
                <th>Hora</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(items,index) in listado_jornadas"  v-show="statusFiltro==1">
                <td>@{{items.fecha_inicio}}</td>
                <td>@{{items.fecha_fin}}</td>
                <td>@{{items.nombre}}</td>
                <td>@{{items.lugar}}</td>
                <td>@{{items.hora}}</td>
                <td>
                  <button type="button" name="button" class="btn btn-primary" title="Editar Jornada" data-toggle="modal" data-target="#myModalEditarJornada" @click="verJornada(items,index)">
                    <i class="fa fa-edit"></i>
                  </button>

                  <button type="button" name="button" class="btn btn-success" title="Asociar Productos" data-toggle="modal" data-target="#myModal" @click="obtenerProductosRegistrados(items)">
                    <i class="fa fa-plus"></i>
                  </button>
                </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="6">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{jornadas.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal eliminar -->
    <div class="modal fade" id="eliminarProducto" role="dialog">
     <div class="modal-dialog modal-md">
       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header bg-primary">
           <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
           <h4 class="modal-title mx-auto text-white">Eliminar detalle de jornada</h4>
         </div>
         <div class="modal-body text-center">
           <p>¿Estás seguro de eliminar este detalle?</p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-success" @click="eliminarDetalle()" data-dismiss="modal">Sí, estoy seguro.</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         </div>
       </div>

     </div>
    </div>

        <!-- The Modal EDITAR JORNADA-->
        <div class="modal" id="myModalEditarJornada">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header text-center">
                <h4 class="modal-title text-center">Actualizar jornada</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="row justify-content-justify">
                  <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group text-center">
                      <i class="fas fa-asterisk"></i>
                      <label for="nivelAtencion">Fecha inicio:</label>
                      <input type="date" class="form-control" v-model="fechaInicioEditar">
                    </div>
                  </div>

                  <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group text-center">
                      <i class="fas fa-asterisk"></i>

                      <label for="nivelAtencion">Fecha fin:</label>
                      <input type="date" class="form-control" v-model="fechaFinEditar">
                    </div>
                  </div>

                  <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group text-center">
                      <i class="fas fa-asterisk"></i>
                      <label for="nivelAtencion">Nombre:</label>
                      <input type="text" class="form-control" v-model="nombreJornadaEditar">
                    </div>
                  </div>

                  <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group text-center">
                      <label for="nivelAtencion">Lugar:</label>
                      <input type="text" class="form-control" v-model="lugarJornadaEditar" >
                    </div>
                  </div>

                  <div class="col-12 col-md-6 col-lg-12">
                    <div class="form-group text-center">
                      <label for="nivelAtencion">Hora:</label>
                      <input type="time" class="form-control" v-model="horaJornadaEditar">
                    </div>
                  </div>

                  <div class="mx-auto">
                    <button type="button" name="button" class="btn btn-success" @click="actualizarJornada">Actualizar</button>
                    <button type="button" name="button" class="btn btn-warning" @click="limpiar">Limpiar</button>
                  </div>
                </div>

                <div class="row" v-show="productosRegistrados.length>0">
                    <div class="mx-auto pt-5">
                      <h4 class="text-dark">LISTADO DE PRODUCTOS ASOCIADOS</h4>
                    </div>

                    <div class="table">
                      <table class="table table-bordered">
                          <thead class="bg-primary text-white text-center">
                            <tr>
                              <th>Producto</th>
                              <th>Modalidad</th>
                              <th>Monto</th>
                              <th>Descripción</th>
                              <th>Acción</th>
                            </tr>
                          </thead>

                          <tbody>
                            <tr v-for="(items,index) in productosRegistrados">
                              <td>@{{items.producto.nombre}}</td>
                              <td>@{{items.modalidad}}</td>
                              <td>@{{items.monto}}</td>
                              <td>@{{items.description}}</td>
                              <td>
                                <button type="button" name="button" class="btn btn-primary" title="Actualizar" @click="mostrarDatosActualizar(items)">
                                  <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                </button>
                                <!--<button type="button" name="button" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#eliminarProducto" @click="eliminarProducto(items.id)">
                                  <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>-->
                              </td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                </div>

              </div>



            </div>
          </div>
        </div>


    <!-- The Modal ASOCIAR PRODUCTOS -->
    <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header text-center">
            <h4 class="modal-title text-center">ASOCIACIÓN DE PRODUCTOS</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div class="row justify-content-justify">
              <div class="col-12 col-md-6 col-lg-6">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for="nivelAtencion">Clasificación:</label>
                  <select class="form-control" v-model="clasificacion_id" @change="obtenerProductosAsociados">
                    <option value="0">Seleccione una opcion</option>
                    <option v-for="items in clasificacion" v-bind:value="items.id">@{{items.nombre}}</option>
                  </select>
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>

                  <label for="nivelAtencion">Productos:</label>
                  <select class="form-control" v-model="producto_id">
                    <option value="0">Seleccione una opcion</option>
                    <option v-for="items in productos" v-bind:value="items.id">@{{items.nombre}}</option>

                  </select>
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for="nivelAtencion">Modalidad:</label>
                  <select class="form-control" v-model="modalidad_id" @change="modalidad()">
                    <option value="0">Seleccione una opcion</option>
                    <option value="Venta">Venta</option>
                    <option value="Libre de pago">Libre de pago</option>
                  </select>
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="form-group text-center">
                  <label for="nivelAtencion">Monto:</label>
                  <input type="text" class="form-control" v-model="monto" id="detalleMonto" v-money="money">

                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-12">
                <div class="form-group text-center">
                  <label for="nivelAtencion">Descripción:</label>
                  <input type="text" class="form-control" v-model="descripcion">
                </div>
              </div>

              <div class="mx-auto">
                <button type="button" name="button" class="btn btn-success" @click="asociarProductos" v-show="buttonHide ==1">Registrar</button>
                <button type="button" name="button" class="btn btn-success" @click="actualizarProductosAsociados" v-show="buttonHide !=1">Actualizar</button>
                <button type="button" name="button" class="btn btn-warning" @click="LimpiarModal">Limpiar</button>
              </div>
            </div>

            <div class="row" v-show="productosRegistrados.length>0">
                <div class="mx-auto pt-5">
                  <h4 class="text-dark">LISTADO DE PRODUCTOS ASOCIADOS</h4>
                </div>

                <div class="table">
                  <table class="table table-bordered">
                      <thead class="bg-primary text-white text-center">
                        <tr>
                          <th>Producto</th>
                          <th>Modalidad</th>
                          <th>Monto</th>
                          <th>Descripción</th>
                          <th>Acción</th>
                        </tr>
                      </thead>

                      <tbody>
                        <tr v-for="(items,index) in productosRegistrados">
                          <td>@{{items.producto.nombre}}</td>
                          <td>@{{items.modalidad}}</td>
                          <td>
                            Bs @{{formatearNumero(items.monto)}}
                          </td>
                          <td>@{{items.description}}</td>
                          <td>
                            <button type="button" name="button" class="btn btn-primary" title="Actualizar" @click="mostrarDatosActualizar(items)">
                              <i class="fa fa-pencil-square" aria-hidden="true"></i>
                            </button>
                            <!--<button type="button" name="button" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#eliminarProducto" @click="eliminarProducto(items.id)">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>-->
                          </td>
                        </tr>
                      </tbody>
                  </table>
                </div>
            </div>

          </div>



        </div>
      </div>
    </div>


  </section>

  @endsection
  @push('scripts')
  <script>
  const app= new Vue({
    el:'#regristroJornada',
    data:{
      money: {
         decimal: '.',
         thousands: '',
         prefix: '',
         suffix: '',
         precision: 2,
       },
      fechaInicio:'',
      fechaFin:'',
      nombreJornada:'',
      lugarJornada:'',
      horaJornada:'',
      jornadas:[],
      jornadas2:[],
      productosRegistrados:[],
      modalidad_id:0,
      clasificacion_id:0,
      monto:'',
      detalleJornadaId:'',
      descripcion:'',
      producto_id:0,
      productos:[],
      clasificacion:[],
      jornada_id:'',
      buttonHide:1,
      fechaInicioEditar:'',
      fechaFinEditar:'',
      nombreJornadaEditar:'',
      horaJornadaEditar:'',
      lugarJornadaEditar:'',
      idJornada:'',
      currentSort:'',//campo por defecto que tomara para ordenar
      currentSortDir:'asc',//order asc
      pageSize:'5',//Registros por pagina
      search:'',
      optionspageSize: [
        { text: '5', value: 5 },
        { text: '10', value: 10 },
        { text: '25', value: 25 },
        { text: '50', value: 50 },
        { text: '100', value: 100 }
      ],//Registros por pagina
      currentPage:1,//Pagina 1
      statusFiltro:1,
      rows:0,
    },
    computed:{
      listado_jornadas:function() {
        this.rows=0;
        return this.jornadas.sort((a,b) => {
          let modifier = 1;
          if(this.currentSortDir === 'desc')
          modifier = -1;
          if(a[this.currentSort] < b[this.currentSort])
          return -1 * modifier;
          if(a[this.currentSort] > b[this.currentSort])
          return 1 * modifier;
          return 0;
        }).filter((row, index) => {
          let start = (this.currentPage-1)*this.pageSize;
          let end = this.currentPage*this.pageSize;
          if(index >= start && index < end){
            this.rows+=1;
            return true;
          }
        });
      },
    },
    methods:{
      filtrar:function(){
        let filtrardo=[];
        if(this.search){
          for(let i in this.jornadas){
            if(this.jornadas[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  ){
              this.statusFiltro=1;
              filtrardo.push(this.jornadas[i]);
            }//if(this.Materiales[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
          }//for(let i in this.MotivosSolicitud)
          if(filtrardo.length)
          this.jornadas=filtrardo;
          else{
            this.statusFiltro=0;
            this.jornadas=this.jornadas2;
          }// if(filtrardo.length)
        }else{
          this.statusFiltro=1;
          this.jornadas=this.jornadas2;
        }//if(this.search$materiales
        },// filtrar:function()
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.jornadas.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        // console.log(s);
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      },
      verJornada(items,index){
        this.idJornada=items.id;
        this.fechaInicioEditar= items.fecha_inicio;
        this.fechaFinEditar= items.fecha_fin;
        this.nombreJornadaEditar= items.nombre;
        this.horaJornadaEditar= items.hora;
        this.lugarJornadaEditar= items.lugar;
      },
      limpiar:function(){
        this.fechaInicio='';
        this.fechaFin='';
        this.nombreJornada='';
        this.lugarJornada='';
        this.horaJornada='';
        this.fechaInicioEditar='';
        this.fechaFinEditar='';
        this.nombreJornadaEditar='';
        this.lugarJornadaEditar='';
        this.horaJornadaEditar='';
      },
      LimpiarModal(){
        this.buttonHide=1;
        this.clasificacion_id=0;
        this.producto_id=0;
        this.modalidad_id=0;
        this.monto='';
        this.descripcion='';
      },
      mostrarDatosActualizar(items){
        this.detalleJornadaId = items.id;
        this.buttonHide =0;
        this.modalidad_id= items.modalidad;
        this.monto = items.monto;
        this.descripcion = items.description;
        this.clasificacion_id = items.producto.clasificacion.id;
        this.producto_id = items.producto_id;
        this.obtenerProductosAsociados();
        // this.clasificacion_id =
      },

      obtenerJornadas(){
        axios.get('{{ route("obtenerJornadas") }}', {
        }).then(response => {

          if(response.data.error==0){
            this.jornadas=response.data.jornadas;
            this.jornadas2=response.data.jornadas;
          }else if(response.data.error==1){
            alertify.error(response.data.msg);
          }
        }).catch(error => {
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      },

      registrarJornada: function(){

        let existeError = false;

        if(!this.fechaInicio){
          alertify.error("Fecha de inicio requerida");
          existeError = true
        }

        if(!this.fechaFin){
          alertify.error("Fecha de fin requerida");
          existeError = true
        }

        if(!this.nombreJornada){
          alertify.error("Nombre de la jornada es requerida");
          existeError = true
        }

        if(!this.lugarJornada){
          alertify.error("Lugar de la jornada es requerida");
          existeError = true
        }

        if(!this.horaJornada){
          alertify.error("Hora de la jornada es requerida");
          existeError = true
        }

        if(existeError == false){
          axios.post("{{ route('registroJornada')}}",
          {
            'fechaInicio':this.fechaInicio,'fechaFin':this.fechaFin,'nombreJornada':this.nombreJornada,'lugarJornada':this.lugarJornada,'horaJornada':this.horaJornada
          }).then(response => {
            if (response.data.error ==0) {

              alertify.success(response.data.mensaje);
              this.limpiar();
              this.obtenerJornadas();

            }else if (response.data.error == 1) {

              $.each(response.data.mensaje, function( index, value ) {
                alertify.error(value);
              });

            }
          }).catch(error => {
            console.log(error);
          });
        }

      }, //registrarJornada

      actualizarJornada: function(){
        let existeError = false;

        if(!this.fechaInicioEditar){
          alertify.error("Fecha de inicio requerida");
          existeError = true
        }

        if(!this.fechaFinEditar){
          alertify.error("Fecha de fin requerida");
          existeError = true
        }

        if(!this.nombreJornadaEditar){
          alertify.error("Nombre de la jornada es requerida");
          existeError = true
        }

        if(!this.lugarJornadaEditar){
          alertify.error("Lugar de la jornada es requerida");
          existeError = true
        }

        if(!this.horaJornadaEditar){
          alertify.error("Hora de la jornada es requerida");
          existeError = true
        }

        if(existeError == false){
          axios.post("{{ route('actualizarJornada')}}",
          {
            'jornada_id':this.idJornada,'fechaInicio':this.fechaInicioEditar,'fechaFin':this.fechaFinEditar,'nombreJornada':this.nombreJornadaEditar,'lugarJornada':this.lugarJornadaEditar,'horaJornada':this.horaJornadaEditar
          }).then(response => {
            if (response.data.error ==0) {
              alertify.success(response.data.mensaje);
              this.limpiar();
              this.obtenerJornadas();
              $('#myModalEditarJornada').modal('toggle');
            }else if (response.data.error == 1) {
              $.each(response.data.mensaje, function( index, value ) {
                alertify.error(value);
              });

            }
          }).catch(error => {
            console.log(error);
          });
        }

      }, //registrarJornada
      modalidad(){

        if(this.modalidad_id == "Libre de pago"){
          this.monto = 0
          $("#detalleMonto").attr('readonly', true)
        }else{
          $("#detalleMonto").attr('readonly', false)
        }

      },

      obtenerProductosAsociados: function(){
        axios.post("{{ route('obtenerProductosAsociados')}}",
        {
          'clasificacion_id':this.clasificacion_id
        }).then(response => {
          if (response.data.error ==0) {
            this.producto_id=0;
            this.productos= response.data.productos;
          }else if (response.data.error == 1) {
            $.each(response.data.mensaje, function( index, value ) {
              alertify.error(value);
            });
          }
        }).catch(error => {
          console.log(error);
        });
      }, //registrarJornada

      mostrarDatosJornada(items){
        this.jornada_id = items.id;
      },
      obtenerClasificacionProducto(){
        axios.get('{{ route("api.index.clasificacion.producto") }}', {
        }).then(response => {
            this.clasificacion=response.data.data;
        }).catch(error => {
          console.log(error);
        });
      },

      asociarProductos: function(){
        axios.post("{{ route('asociarProductos')}}",
        {
          'clasificacion_id':this.clasificacion_id,'producto_id':this.producto_id,'modalidad_id':this.modalidad_id,'monto':this.monto,'descripcion':this.descripcion,'jornada_id':this.jornada_id
        }).then(response => {
          if (response.data.error ==0) {
            alertify.success(response.data.mensaje);
            this.limpiar();
            this.obtenerJornadas();
            $('#myModal').modal('toggle');

          }else if (response.data.error == 1) {
            $.each(response.data.mensaje, function( index, value ) {
              alertify.error(value);
            });
          }
        }).catch(error => {
          console.log(error);
        });
      }, //registrarJornada
      actualizarProductosAsociados(){
        axios.post("{{ url('actualizarDetalleJornada') }}", {modalidad_id: this.modalidad_id, monto: this.monto, descripcion: this.descripcion, clasficacion_id: this.clasificacion_id, detalleJornadaId: this.detalleJornadaId, jornada_id: this.jornada_id, producto_id: this.producto_id}).then(response => {
          alertify.success(response.data.mensaje);
          this.obtenerProductosRegistrados({id:this.jornada_id});
        });

      },
      formatearNumero:function(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
      },
      obtenerProductosRegistrados(items){
        this.jornada_id = items.id;

        axios.post("{{ route('obtenerProductosRegistrados')}}",
        {
          'jornada_id':this.jornada_id
        }).then(response => {
          if (response.data.error ==0) {
            this.productosRegistrados= response.data.productos;
          }else if (response.data.error == 1) {
            $.each(response.data.mensaje, function( index, value ) {
              alertify.error(value);
            });
          }
        }).catch(error => {
          console.log(error);
        });
      },

      eliminarProducto(id){
        this.detalleJornadaId = id;
      }

    },//methods
    mounted(){
      this.obtenerJornadas();
      this.obtenerClasificacionProducto();
      // this.obtenerProductosRegistrados();
    }
  });//const app= new Vue
</script>
@endpush
