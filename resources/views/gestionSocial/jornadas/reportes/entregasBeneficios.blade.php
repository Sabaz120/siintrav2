<table>
  <thead>
    <tr>
      <th colspan="4">{{ $data['jornada']->nombre }}: {{ $data['detalleJornada']->description }}</th>
    </tr>
    <tr>
      <th>Cedula</th>
      <th>Nombre</th>
      <th>Cargo</th>
      <th>Departamento</th>
      <th>Marcaje</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data['entregas'] as $key)
    <tr>
      <td>{{$key->cedula}}</td>
      <td>{{$key->trabajador->nombre}} {{$key->trabajador->apellido}}</td>
      <td>{{$key->trabajador->cargo}}</td>
      <td>{{$key->trabajador->departamento}}</td>
      <td>{{$key->marcaje}}</td>
    </tr>
    @endforeach

  </tbody>
</table>
