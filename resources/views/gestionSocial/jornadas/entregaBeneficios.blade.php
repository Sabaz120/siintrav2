<!DOCTYPE html>
<html lang="en" dir="ltr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}" />
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Gestion Social</title>
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/icons.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
<link href="{{ asset('plugins/font-awesome/css/all.min.css') }}" rel="stylesheet">
<!-- Styles alertify-->
<link href="{{ asset('plugins/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
<!-- Styles alertify-->
<link href="{{ asset('plugins/alertifyjs/css/themes/default.min.css') }}" rel="stylesheet">
<!-- Styles alertify-->
<link href="{{ asset('plugins/alertifyjs/css/themes/semantic.min.css') }}" rel="stylesheet">
<!-- Styles alertify-->
<link href="{{ asset('plugins/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('css/datepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/jquery.timepicker.css') }}">
<!-- <script src="{{ asset('highchartsCronos/highcharts.js') }}"></script> -->
<!-- <script src="{{ asset('highchartsCronos/exporting.js') }}"></script> -->


<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-select.css') }}">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" /> -->

<link rel="stylesheet" href="{{ URL::asset('css/owlCarousel/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/owlCarousel/owl.theme.default.min.css') }}">
<body>

  <style media="screen">
  .social-card-header{
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: center;
    justify-content: center;
    height: 96px;
  }
  .social-card-header i {
    font-size: 32px;
    color:#FFF;
  }
  .bg-facebook {
    background-color:#3b5998;
  }
  .text-facebook {
    color:#3b5998;
  }
  .bg-google-plus{
    background-color:#dd4b39;
  }
  .text-google-plus {
    color:#dd4b39;
  }
  .bg-twitter {
    background-color:#1da1f2;
  }
  .text-twitter {
    color:#1da1f2;
  }
  .bg-pinterest {
    background-color:#bd081c;
  }
  .text-pinterest {
    color:#bd081c;
  }
  .share:hover {
    text-decoration: none;
    opacity: 0.8;
  }
  </style>

  <section id="gestionSocial">
    <div class="card" >
      <div class="card-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6 col-xl-12">
              <div class="widget-simple-chart text-center text-white card-box bg-primary">
                <h1> <span class="font-weight-bold">@{{nombreJornada}} :</span> <br> <span>@{{descripcion_jornada}}</span> <br>
                  <span>Costo: @{{costo | formatPrice}} Bs</span></h1>

                </div>
              </div>
            </div>
            <!-- end row -->

            <div class="container-fluid">
              <div class="form-row">
                <div class="form-group col-3">
                  <label for="">Seleccione la jornada</label>
                  <select class="form-control" name="" v-model="jornada_id" @change="obtenerProductosAsociadosJornada">
                    <option value="0">Seleccione</option>
                    <option v-for="items in jornadas" v-bind:value="items.id">@{{items.nombre}}</option>
                  </select>
                </div>

                <div class="form-group col-3">
                  <label for="">Seleccione el producto</label>

                  <select class="form-control" name="" v-model="producto_id" >
                    <option value="0">Seleccione</option>
                    <option v-for="items in productosJornadas" v-bind:value="items.producto.id">@{{items.producto.nombre}}</option>

                  </select>
                </div>


                <div class="form-group col-3">
                  <label for="">Seleccione el biometrico</label>

                  <select class="form-control" name="" v-model="biometrico_id" @change="obtenerTrabajadores()" >
                    <option v-for="items in biometricos" v-bind:value="items.ip">@{{items.ip}}</option>

                  </select>
                </div>

                <div class="form-group col-3">
                  <label for="">Estado del sistema</label>
                  <select class="form-control" name="" v-model="estado">
                    <option value="0">En proceso</option>
                    <option value="1">Terminado</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-3" v-for="items in users.marcas" >
                <div class="card mb-4 border-dark">
                  <img class="" :src="'http://salasituacional.vtelca.gob.ve/img/cedulas/'+items.cedula+'.jpg'" style="width:100%;height:350px">

                  <div class="card-body">
                    <h5 class="card-title text-center ">@{{items.nombres}}</h5>
                    <h6 class="text-center text-capitalize">@{{items.area}}</h6>
                    <h4 class="text-center font-weight-bold pt-2">Marcaje: @{{items.hora}}</h4>

                  </div>
                </div>
              </div>

              <div class="container-fluid">
                <div class="row">
                  <!-- <div class="mr-auto">
                  <div class="pb-3">
                  <a href="{{url('reporteEntregaBeneficio')}}" class="btn btn-primary">Imprimir reporte</a>
                </div>
              </div> -->
              <div class="ml-auto">
                <div class="pb-3">
                  <button type="button" @click="registrarParticipantes()"  name="button" class="btn btn-primary btn-lg">Finalizar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header bg-danger">
          <div class="row">
            <div class="mx-auto">
              <h3 class="modal-title text-center">Usted ya ha sido beneficiado en esta jornada!</h3>
            </div>
          </div>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <!-- <h3 class="text-center">Usted ya ha sido beneficiado en esta jornada!</h3> -->
          <img class="card-img-top" :src="'http://salasituacional.vtelca.gob.ve/img/cedulas/'+users.cedula_duplicado+'.jpg'">
          <h4 class="card-title text-center pt-5 ">@{{users.cedula_duplicado}}</h4>
          <h4 class="card-title text-center">@{{users.nombre_duplicado}}</h4>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> -->
        </div>

      </div>
    </div>
  </div>

</section>
<script src="{{ asset('js/app.js') }}"></script>
<script>
const app= new Vue({
  el:'#gestionSocial',
  data:{
    users:[],
    entregasBeneficios:[],
    biometricos:[],
    estado:0,
    users2:[],
    jornadas:[],
    productosJornadas:[],
    jornada_id:0,
    producto_id:0,
    biometrico_id:'192.168.100.208',
    detalle_jornada_id:'',
    nombreJornada:'',
    descripcion_jornada:'',
    costo:'',
  },
  filters:{
    formatPrice(nStr){
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
    }
  },
  methods:{
    //

    obtenerEntregas(){
      axios.get('{{ url("obtenerJornadas") }}', {
      }).then(response => {
        this.entregasBeneficios = response.data.entregas;
      }).catch(error => {
        console.log(error);
      });
    },
    obtenerJornadas(){
      axios.get('{{ route("obtenerJornadas") }}', {
      }).then(response => {
        if(response.data.error==0){
          this.jornadas=response.data.jornadas;
        }else if(response.data.error==1){
          alertify.error(response.data.msg);
        }
      }).catch(error => {
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },

    obtenerProductosAsociadosJornada: function(){
      axios.post("{{ route('obtenerProductosJornadas')}}",
      {
        'jornada_id':this.jornada_id
      }).then(response => {
        if (response.data.error ==0) {
          this.productosJornadas= response.data.detalle;
          for (var i = 0; i < this.productosJornadas.length; i++) {
            this.detalle_jornada_id = this.productosJornadas[i].id;
            this.descripcion_jornada = this.productosJornadas[i].description;
            this.costo = this.productosJornadas[i].monto;
            this.nombreJornada = this.productosJornadas[i].jornada.nombre;
          }
        }else if (response.data.error == 1) {
          $.each(response.data.mensaje, function( index, value ) {
            alertify.error(value);
          });
        }
      }).catch(error => {
        console.log(error);
      });
    }, //


    obtenerTrabajadores(){
      axios.get('{{ url("biometrico") }}'+'/'+this.biometrico_id, {
      }).then(response => {
        this.users = response.data.data;
        if (this.users.duplicado) {
          $('#myModal').modal('show');
          setTimeout(function() {$('#myModal').modal('hide');}, 5000);
        }
        var self = this;
        if (this.estado !=1) {
          setTimeout(function() {
            self.obtenerTrabajadores();
          }, 5000);
        }

      }).catch(error => {
        console.log(error);
      });
    },

    registrarParticipantes: function(){
      console.log(this.detalle_jornada_id);
      axios.post("{{ url('biometrico_jornada')}}",
      {
        'detalle_jornada_id':this.detalle_jornada_id,'biometrico_id':this.biometrico_id
      }).then(response => {
        alert("Participantes almacenados correctamente");
      }).catch(error => {
        console.log(error);
      });
    }, //registrarJornada

    obtenerBiometricos(){
      axios.get('{{ route("obtenerBiometricos") }}', {
      }).then(response => {
        this.biometricos=response.data.biometricos;
      }).catch(error => {
        alertify.error('Error en el servidor.');
        console.log(error);
      });
    },

  },//methods
  mounted(){
    this.obtenerTrabajadores();
    this.obtenerEntregas();
    this.obtenerJornadas();
    this.obtenerBiometricos();
    // this.obtenerTrabajadores2();

  }
});//const app= new Vue

</script>
</body>
</html>
