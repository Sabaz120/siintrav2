@extends('layouts.app')
@section('contenido')
<section id="biometricos">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIONAR BIOMETRICOS</h5>
      <div class="row justify-content-justify">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="form-group text-center">
            <label for="descripcion">IP:</label>
            <input type="text" class="form-control" v-model="ip">
          </div>
        </div>

        <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
          <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
          <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
        </div>
      </div>

      <div class="row pt-5">
        <div class="mx-auto">
          <h3 class="text-dark">Listado de biometricos</h3>
        </div>

        <div class="table">
          <table class="table table-bordered  text-center">
            <thead class=" bg-primary text-white ">
            <tr>
                <th>IP</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
          </thead>
            <tbody>
                <tr v-for="(items,index) in biometricos">
                  <td>@{{items.ip}}</td>
                  <td v-if="items.estado ==true">Activo</td>
                  <td v-else>Deshabilitado</td>
                  <td>
                    <button type="button" class="btn btn-info" title="Editar" @click="modalActualizar(index)"  name="button">
                      <i class="fa fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger" title="Borrar" @click="borrar(index)"  name="button">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>


    <div class="modal" id="editar" v-if="biometricos.length>0">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary text-center">
            <h4 class="modal-title mx-auto text-white">Clasificación de Producto</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body row">
            <div class="col-md-12">
              <label for="">IP</label>
              <input type="text" class="form-control" v-model="nombre_editar">
            </div>
          </div>

          <!-- Modal footers -->
          <div class="modal-footer mx-auto table-responsive">
            <button type="button" class="btn btn-success" @click="actualizar" name="button">Actualizar</button>
          </div>
        </div>
      </div>
    </div>

  </section>

  @endsection
  @push('scripts')
  <script>
  const app= new Vue({
    el:'#biometricos',
    data:{
      ip:'',
      biometricos:[],
      biometricos_index:"",
      nombre_editar:"",
      biometrico_id:""
    },
    methods:{
      limpiar:function(){
        this.ip='';
      },

      modalActualizar(index){
        console.log(index)
        this.nombre_editar=this.biometricos[index].ip;
        this.biometricos_index=index;
        $('#editar').modal('toggle');
      },
      actualizar(){
        if(this.nombre_editar){
          axios.put("{{ route('actualizarBiometricos')}}",
          {
            'ip':this.nombre_editar,
            'biometrico_id':this.biometricos[this.biometricos_index].id
          }).then(response => {
            this.obtenerBiometricos();
            alertify.success(response.data.mensaje);
            this.limpiar();
            $('#editar').modal('toggle');
          }).catch(error => {
            var response=error.response.data;
            $.each(response.msg, function( index, value ) {
              alertify.error(value);
            });
          });
        }else
          alertify.error("Debe escribir un nombre de clasificación");
      },
      borrar(index){
        var id=this.biometricos[index].id;
        var url="{{ route('borrarBiometricos',':id')}}";
        url = url.replace(':id', id);
        axios.delete(url).then(response => {
          this.obtenerBiometricos();
          alertify.success(response.data.mensaje);
          this.limpiar();
        }).catch(error => {
          var response=error.response.data;
          $.each(response.msg, function( index, value ) {
            alertify.error(value);
          });
        });
      },

      obtenerBiometricos(){
        axios.get('{{ route("obtenerBiometricos") }}', {
        }).then(response => {
          this.biometricos=response.data.biometricos;
        }).catch(error => {
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      },

      registrar: function(){
          axios.post("{{ route('registrarBiometricos')}}",
          {
            'ip':this.ip
          }).then(response => {
            alertify.success(response.data.mensaje);
            this.limpiar();
            this.obtenerBiometricos()
          }).catch(error => {
            var response=error.response.data;
            $.each(response.msg, function( index, value ) {
              alertify.error(value);
            });
          });
      }, //registrarJornada


    },//methods
    mounted(){
      this.obtenerBiometricos();
    }
  });//const app= new Vue
</script>
@endpush
