@extends('layouts.app')
@section('contenido')
<section id="registroProductos">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE PRODUCTOS</h5>
      <div class="row justify-content-justify">
        <div class="col-12 col-md-6 col-lg-6">
          <div class="form-group text-center">
            <i class="fas fa-asterisk"></i>
            <label for="nivelAtencion">Clasificación:</label>
            <select class="form-control" v-model="clasificacion_id">
              <option value="0">Seleccione una opcion</option>
              <option v-for="items in clasificacion" v-bind:value="items.id">@{{items.nombre}}</option>

            </select>
          </div>
        </div>

        <div class="col-12 col-md-6 col-lg-6">
          <div class="form-group text-center">
            <label for="nivelAtencion">Nombre del producto:</label>
            <input type="text" class="form-control" v-model="nombreProducto">
          </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
          <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrarProductos">Registrar</button>
          <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
        </div>
      </div>

      <div class="row">
        <div class="mx-auto">
          <h3 class="text-dark">Listado de de productos</h3>
        </div>
      </div>
      <div class="row pt-5">
          <div class="col-12 col-md-9 col-lg-9">
            <br>
            <div class="mt-2 form-inline font-weight-bold">
              Mostrar
              <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                <option v-for="option in optionspageSize" v-bind:value="option.value">
                  @{{ option.text }}
                </option>
              </select>
              registros
            </div>
          </div>
          <div class="col-12 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="Buscar" class="font-weight-bold">Buscar:</label>
              <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
            </div>
          </div>
        </div>

        <div class="table">
          <table class="table table-bordered  text-center" v-if="productos.length>0">
            <thead class=" bg-primary text-white ">
            <tr>
                <th>Clasificación</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
          </thead>
            <tbody>
                <tr v-for="(items,index) in listado_productos" v-show="statusFiltro==1">
                  <td v-if="items.clasificacion.nombre !=null || items.clasificacion.nombre !=''">@{{items.clasificacion.nombre}}</td>
                  <td v-if="items.nombre !=null || items.nombre !=''">@{{items.nombre}}</td>
                  <td>
                    <button type="button" name="button" title="Actualizar" class="btn btn-primary" data-toggle="modal" data-target="#myModal" @click="mostrarDatosModal(items)">
                      <i class="fa fa-pencil-square" aria-hidden="true"></i>
                    </button>
                    <button type="button" name="button" title="Eliminar" class="btn btn-danger" @click="eliminarProductos(items.id)">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="3">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{productos.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header text-center">
            <h4 class="modal-title text-center">Actualizar</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div class="row justify-content-justify">
              <div class="col-12 col-md-6 col-lg-6">
                <div class="form-group text-center">
                  <i class="fas fa-asterisk"></i>
                  <label for="nivelAtencion">Clasificación:</label>
                  <select class="form-control" v-model="clasificacion_id_editar">
                    <option value="0">Seleccione una opcion</option>
                    <option v-for="items in clasificacion" v-bind:value="items.id">@{{items.nombre}}</option>

                  </select>
                </div>
              </div>

              <div class="col-12 col-md-6 col-lg-6">
                <div class="form-group text-center">
                  <label for="nivelAtencion">Nombre del producto:</label>
                  <input type="text" class="form-control" v-model="nombreProductoEditar">
                </div>
              </div>
            </div>

          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" @click="actualizarProductos" >Actualizar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>

        </div>
      </div>
    </div>


  </section>

  @endsection
  @push('scripts')
  <script>
  const app= new Vue({
    el:'#registroProductos',
    data:{
      productos:[],
      productos2:[],
      clasificacion:[],
      clasificacion_id:0,
      nombreProducto:'',
      nombreProductoEditar:'',
      clasificacion_id_editar:'0',
      id:'',
      currentSort:'',//campo por defecto que tomara para ordenar
      currentSortDir:'asc',//order asc
      pageSize:'5',//Registros por pagina
      search:'',
      optionspageSize: [
        { text: '5', value: 5 },
        { text: '10', value: 10 },
        { text: '25', value: 25 },
        { text: '50', value: 50 },
        { text: '100', value: 100 }
      ],//Registros por pagina
      currentPage:1,//Pagina 1
      statusFiltro:1,
      rows:0,


    },
    computed:{
      listado_productos:function() {
        this.rows=0;
        return this.productos.sort((a,b) => {
          let modifier = 1;
          if(this.currentSortDir === 'desc')
          modifier = -1;
          if(a[this.currentSort] < b[this.currentSort])
          return -1 * modifier;
          if(a[this.currentSort] > b[this.currentSort])
          return 1 * modifier;
          return 0;
        }).filter((row, index) => {
          let start = (this.currentPage-1)*this.pageSize;
          let end = this.currentPage*this.pageSize;
          if(index >= start && index < end){
            this.rows+=1;
            return true;
          }
        });
      },
    },
    methods:{
      filtrar:function(){
        let filtrardo=[];
        if(this.search){
          for(let i in this.productos){
            if(this.productos[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.productos[i].clasificacion.nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
              this.statusFiltro=1;
              filtrardo.push(this.productos[i]);
            }//if(this.Materiales[i].productos.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
          }//for(let i in this.MotivosSolicitud)
          if(filtrardo.length)
          this.productos=filtrardo;
          else{
            this.statusFiltro=0;
            this.productos=this.productos2;
          }// if(filtrardo.length)
        }else{
          this.statusFiltro=1;
          this.productos=this.productos2;
        }//if(this.search$materiales
        },// filtrar:function()
      limpiar:function(){
        this.clasificacion_id=0;
        this.nombreProducto='';
      },
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.productos.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        // console.log(s);
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      },


      obtenerProductos(){
        axios.get('{{ route("obtenerProductos") }}', {
        }).then(response => {
            this.productos=response.data.productos;
            this.productos2=response.data.productos;
        }).catch(error => {
          console.log(error);
        });
      },

      obtenerClasificacionProducto(){
        axios.get('{{ route("api.index.clasificacion.producto") }}', {
        }).then(response => {
            this.clasificacion=response.data.data;
        }).catch(error => {
          console.log(error);
        });
      },

      mostrarDatosModal(items){

        console.log(items)

        this.nombreProductoEditar = items.nombre;
        this.clasificacion_id_editar =items.clasificacion_productos_id;
        this.id =items.id;
      },

      registrarProductos: function(){

        let existeError = false

        if(this.nombreProducto.length == 0){
          alertify.error("Debe seleccionar un nombre");
          existeError = true
        }

        if(this.clasificacion_id == 0){
          alertify.error("Debe seleccionar una clasificacion");
          existeError = true
        }

        if(existeError == false){
          axios.post("{{ route('registroProductos')}}",
          {
            'clasificacion_id':this.clasificacion_id,'nombreProducto':this.nombreProducto
          }).then(response => {
            if (response.data.error ==0) {
              alertify.success(response.data.mensaje);
              this.limpiar();
              this.obtenerProductos();
            }else if (response.data.error == 1) {
              $.each(response.data.mensaje, function( index, value ) {
                alertify.error(value);
              });
            }
          }).catch(error => {
            console.log(error);
          });
        }

      }, //registrarProductos

      actualizarProductos: function(){
        axios.post("{{ route('actualizarProductos')}}",
        {
          'id':this.id,'clasificacion_id':this.clasificacion_id_editar,'nombreProducto':this.nombreProductoEditar
        }).then(response => {
          if (response.data.error ==0) {
            console.log("he");
            alertify.success(response.data.mensaje);
            this.limpiar();
            this.obtenerProductos();
            $('#myModal').modal('toggle');

          }else if (response.data.error == 1) {
            $.each(response.data.mensaje, function( index, value ) {
              alertify.error(value);
            });
          }
        }).catch(error => {
          console.log(error);
        });
      }, //ActualizarProductos

      eliminarProductos: function(id){
        axios.post("{{ route('eliminarProductos')}}",
        {
          'id':id
        }).then(response => {
          if (response.data.error ==0) {
            console.log("he");
            alertify.success(response.data.mensaje);
            // this.limpiar();
            this.obtenerProductos();
            // $('#myModal').modal('toggle');
          }else if (response.data.error == 1) {
            $.each(response.data.mensaje, function( index, value ) {
              alertify.error(value);
            });
          }
        }).catch(error => {
          console.log(error);
        });
      }, //eliminarProductos



    },//methods
    mounted(){
      this.obtenerProductos();
      this.obtenerClasificacionProducto();
    }
  });//const app= new Vue
</script>
@endpush
