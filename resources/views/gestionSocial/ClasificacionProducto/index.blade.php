@extends('layouts.app')
@section('contenido')
<section id="regristroJornada">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">REGISTRO DE CLASIFICACIÓN DE PRODUCTO</h5>
      <div class="row justify-content-justify">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="form-group text-center">
            <label for="descripcion">Nombre:</label>
            <input type="text" class="form-control" v-model="nombre">
          </div>
        </div>

        <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
          <button type="button" class="btn btn-success font-weight-bold" v-on:click="registrar">Registrar</button>
          <button type="button" class="btn btn-warning font-weight-bold" v-on:click="limpiar">Limpiar</button>
        </div>
      </div>

      <div class="row pt-5">
        <div class="mx-auto">
          <h3 class="text-dark">Listado de clasificaciones</h3>
        </div>
      </div>
      <div class="row pt-5">
        <div class="col-12 col-md-9 col-lg-9">
          <br>
          <div class="mt-2 form-inline font-weight-bold">
            Mostrar
            <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
              <option v-for="option in optionspageSize" v-bind:value="option.value">
                @{{ option.text }}
              </option>
            </select>
            registros
          </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3">
          <div class="form-group">
            <label for="Buscar" class="font-weight-bold">Buscar:</label>
            <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
          </div>
        </div>
      </div>
        <div class="table">
          <table class="table table-bordered  text-center">
            <thead class=" bg-primary text-white ">
            <tr>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
          </thead>
            <tbody>
                <tr v-for="(items,index) in listado_clasificaciones" v-show="statusFiltro==1">
                  <td>@{{items.nombre}}</td>
                  <td>
                    <button type="button" class="btn btn-info" title="Editar" @click="modalActualizar(index)"  name="button">
                      <i class="fa fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger" title="Borrar" @click="borrar(index)"  name="button">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="2">No se encontraron registros</td>
              </tr>
            </tbody>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{clasificaciones.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal" id="editar" v-if="clasificaciones.length>0">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header bg-primary text-center">
            <h4 class="modal-title mx-auto text-white">Clasificación de Producto</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body row">
            <div class="col-md-12">
              <label for="">Nombre</label>
              <input type="text" class="form-control" v-model="nombre_editar">
            </div>
          </div>

          <!-- Modal footers -->
          <div class="modal-footer mx-auto table-responsive">
            <button type="button" class="btn btn-success" @click="actualizar" name="button">Actualizar</button>
          </div>
        </div>
      </div>
    </div>

  </section>

  @endsection
  @push('scripts')
  <script>
  const app= new Vue({
    el:'#regristroJornada',
    data:{
      nombre:'',
      clasificaciones:[],
      clasificaciones2:[],
      clasificacion_index:0,
      nombre_editar:'',
      currentSort:'',//campo por defecto que tomara para ordenar
      currentSortDir:'asc',//order asc
      pageSize:'5',//Registros por pagina
        search:'',
      optionspageSize: [
        { text: '5', value: 5 },
        { text: '10', value: 10 },
        { text: '25', value: 25 },
        { text: '50', value: 50 },
        { text: '100', value: 100 }
      ],//Registros por pagina
      currentPage:1,//Pagina 1
      statusFiltro:1,
      rows:0,
    },
    computed:{
      listado_clasificaciones:function() {
        this.rows=0;
        return this.clasificaciones.sort((a,b) => {
          let modifier = 1;
          if(this.currentSortDir === 'desc')
          modifier = -1;
          if(a[this.currentSort] < b[this.currentSort])
          return -1 * modifier;
          if(a[this.currentSort] > b[this.currentSort])
          return 1 * modifier;
          return 0;
        }).filter((row, index) => {
          let start = (this.currentPage-1)*this.pageSize;
          let end = this.currentPage*this.pageSize;
          if(index >= start && index < end){
            this.rows+=1;
            return true;
          }
        });
      },
    },
    methods:{
      filtrar:function(){
        let filtrardo=[];
        if(this.search){
          for(let i in this.clasificaciones){
            if(this.clasificaciones[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
              this.statusFiltro=1;
              filtrardo.push(this.clasificaciones[i]);
            }//if(this.Materiales[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
          }//for(let i in this.MotivosSolicitud)
          if(filtrardo.length)
          this.clasificaciones=filtrardo;
          else{
            this.statusFiltro=0;
            this.clasificaciones=this.clasificaciones2;
          }// if(filtrardo.length)
        }else{
          this.statusFiltro=1;
          this.clasificaciones=this.clasificaciones2;
        }//if(this.search$materiales
        },// filtrar:function()
      limpiar:function(){
        this.nombre='';
      },
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.clasificaciones.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        // console.log(s);
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      },

      modalActualizar(index){
        this.nombre_editar=this.clasificaciones[index].nombre;
        this.clasificacion_index=index;
        $('#editar').modal('toggle');
      },
      actualizar(){
        if(this.nombre_editar){
          axios.put("{{ route('api.update.clasificacion.producto')}}",
          {
            'nombre':this.nombre_editar,'clasificacion_id':this.clasificaciones[this.clasificacion_index].id
          }).then(response => {
            this.obtenerClasificaciones();
            alertify.success(response.data.msg);
            this.limpiar();
            $('#editar').modal('toggle');
          }).catch(error => {
            var response=error.response.data;
            $.each(response.msg, function( index, value ) {
              alertify.error(value);
            });
          });
        }else
          alertify.error("Debe escribir un nombre de clasificación");
      },
      borrar(index){
        var id=this.clasificaciones[index].id;
        var url="{{ route('api.destroy.clasificacion.producto',':id')}}";
        url = url.replace(':id', id);
        axios.delete(url).then(response => {
          this.obtenerClasificaciones();
          alertify.success(response.data.msg);
          this.limpiar();
        }).catch(error => {
          var response=error.response.data;
          $.each(response.msg, function( index, value ) {
            alertify.error(value);
          });
        });
      },

      obtenerClasificaciones(){
        axios.get('{{ route("api.index.clasificacion.producto") }}', {
        }).then(response => {
          this.clasificaciones=response.data.data;
          this.clasificaciones2=response.data.data;
        }).catch(error => {
          alertify.error('Error en el servidor.');
          console.log(error);
        });
      },

      registrar: function(){
        if(this.nombre){
          axios.post("{{ route('api.store.clasificacion.producto')}}",
          {
            'nombre':this.nombre
          }).then(response => {
            this.obtenerClasificaciones();
            alertify.success(response.data.msg);
            this.limpiar();
          }).catch(error => {
            var response=error.response.data;
            $.each(response.msg, function( index, value ) {
              alertify.error(value);
            });
          });
        }else
          alertify.error("Debe escribir un nombre de clasificación");
      }, //registrarJornada


    },//methods
    mounted(){
      this.obtenerClasificaciones();
    }
  });//const app= new Vue
</script>
@endpush
