@extends('layouts.app')
@section('contenido')
<section id="reporteJornadas">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">JORNADAS</h5>

      <div class="row">
        <div class="mx-auto">
          <h3 class="text-dark">Listado de jornadas</h3>
        </div>
      </div>

      <div class="row pt-5">
          <div class="col-12 col-md-9 col-lg-9">
            <br>
            <div class="mt-2 form-inline font-weight-bold">
              Mostrar
              <select v-model="pageSize" class="form-control form-control-sm col-2 col-md-1 col-lg-1 ml-2 mr-2">
                <option v-for="option in optionspageSize" v-bind:value="option.value">
                  @{{ option.text }}
                </option>
              </select>
              registros
            </div>
          </div>
          <div class="col-12 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="Buscar" class="font-weight-bold">Buscar:</label>
              <input id="search" class="form-control form-control-sm"  maxlength="200"  type="text" v-model="search" v-on:keyup="filtrar" required>
            </div>
          </div>

        <div class="table">
          <table class="table table-bordered  text-center">
            <thead class=" bg-primary text-white ">
            <tr>
                <th>Jornada</th>
                <th>Lugar</th>
                <th>Fecha</th>
                <th>Acciones</th>
            </tr>
          </thead>
            <tbody>
                <tr v-for="(items,index) in listado_jornadas" v-show="statusFiltro==1">
                  <td>@{{items.nombre}}</td>
                  <td>@{{items.lugar}}</td>
                  <td>@{{items.fecha_inicio}}</td>
                  <td>
                    <button type="button" name="button" title="Imprimir reporte" class="btn btn-success" data-toggle="modal" data-target="#myModal" @click="imprimir(items.id)">
                      <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    </button>
                    <button @click="estadisticas(items.id)" type="button" name="button" title="Estadisticas" class="btn btn-info">
                      <i class="fa fa-pie-chart" aria-hidden="true"></i>
                    </button>
                  </td>
              </tr>
              <tr v-show='statusFiltro==0'>
                <td class="text-center font-weight-bold" colspan="2">No se encontraron registros</td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <strong class="pt-5" v-show='statusFiltro'>Mostrando: @{{rows}} registros de: @{{jornadas.length}}</strong>
            <strong class="pt-5" v-show='!statusFiltro'>Mostrando: 0 registros de 0 </strong>
            <div style="float:right">
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="prevPage" data-toggle="tooltip" title="Anterior"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <button class="btn bg-primary text-white font-weight-bold" v-on:click="nextPage"data-toggle="tooltip" title="Siguiente"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
              <div class="row ml-2">
                <strong>Página:  @{{currentPage}}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </section>

  @endsection
  @push('scripts')
  <script>
  const app= new Vue({
    el:'#reporteJornadas',
    data:{
      jornadas:[],
      jornadas2:[],
      currentSort:'',//campo por defecto que tomara para ordenar
      currentSortDir:'asc',//order asc
      pageSize:'5',//Registros por pagina
        search:'',
      optionspageSize: [
        { text: '5', value: 5 },
        { text: '10', value: 10 },
        { text: '25', value: 25 },
        { text: '50', value: 50 },
        { text: '100', value: 100 }
      ],//Registros por pagina
      currentPage:1,//Pagina 1
      statusFiltro:1,
      rows:0,
    },
    computed:{
      listado_jornadas:function() {
        this.rows=0;
        return this.jornadas.sort((a,b) => {
          let modifier = 1;
          if(this.currentSortDir === 'desc')
          modifier = -1;
          if(a[this.currentSort] < b[this.currentSort])
          return -1 * modifier;
          if(a[this.currentSort] > b[this.currentSort])
          return 1 * modifier;
          return 0;
        }).filter((row, index) => {
          let start = (this.currentPage-1)*this.pageSize;
          let end = this.currentPage*this.pageSize;
          if(index >= start && index < end){
            this.rows+=1;
            return true;
          }
        });
      },
    },
    methods:{
      filtrar:function(){
        let filtrardo=[];
        if(this.search){
          for(let i in this.jornadas){
            if(this.jornadas[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1 || this.jornadas[i].lugar.toLowerCase().trim().search(this.search.toLowerCase())!=-1 ){
              this.statusFiltro=1;
              filtrardo.push(this.jornadas[i]);
            }//if(this.Materiales[i].nombre.toLowerCase().trim().search(this.search.toLowerCase())!=-1  || this.Materiales[i].descripcion.toLowerCase().trim().search(this.search.toLowerCase())!=-1)
          }//for(let i in this.MotivosSolicitud)
          if(filtrardo.length)
          this.jornadas=filtrardo;
          else{
            this.statusFiltro=0;
            this.jornadas=this.jornadas2;
          }// if(filtrardo.length)
        }else{
          this.statusFiltro=1;
          this.jornadas=this.jornadas2;
        }//if(this.search$materiales
        },// filtrar:function()
      nextPage:function() {
        if((this.currentPage*this.pageSize) < this.jornadas.length) this.currentPage++;
      },
      prevPage:function() {
        if(this.currentPage > 1) this.currentPage--;
      },
      sort:function(s) {
        //if s == current sort, reverse
        // console.log(s);
        if(s === this.currentSort) {
          this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
        }
        this.currentSort = s;
      },
      obtenerJornadas(){
        axios.get('{{ route("obtenerJornadas") }}', {
        }).then(response => {
            this.jornadas=response.data.jornadas;
            this.jornadas2=response.data.jornadas;
        }).catch(error => {
          console.log(error);
        });
      },

      imprimir(id){
        window.open("{{ url('/reporteEntregaBeneficio/') }}"+"/"+id, '_blank')
      },

      estadisticas(id){
        window.open("{{ url('/reportesJornada/') }}"+"/"+id+"/estadistica/", '_blank')
      }

    },//methods
    mounted(){
      this.obtenerJornadas();
    }
  });//const app= new Vue
</script>
@endpush
