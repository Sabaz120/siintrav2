<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="{{ asset('css/file.css') }}" rel="stylesheet">
  <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

  <title>Registro Masivo de Nomina</title>

<style media="screen">
h1 {
text-align: center;
}

.alert {
min-width: 150px;
padding: 15px;
margin-bottom: 20px;
border: 1px solid transparent;
border-radius: 3px;
}
.alert-success {
background-color: #91cf91;
border-color: #80c780;
color: #3d8b3d;
}
.alert-warning {
background-color: #ebc063;
border-color: #e8b64c;
color: #a07415;
}
.alert-danger {
background-color: #e27c79;
border-color: #dd6864;
color: #9f2723;
}
.alert p {
padding: 0;
margin: 0;
}
.alert i {
padding-right: 5px;
vertical-align: middle;
font-size: 24px;
}
.alert .close-alert {
-webkit-appearance: none;
position: relative;
float: right;
padding: 0;
border: 0;
cursor: pointer;
color: inherit;
background: 0 0;
font-size: 21px;
line-height: 1;
font-weight: bold;
text-shadow: 0 1px 0 rgba(255, 255, 255, 0.7);
filter: alpha(opacity=40);
opacity: .4;
}
.alert .close-alert:hover {
filter: alpha(opacity=70);
opacity: .7;
}

.shadow-1, .alert {
box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
}

.shadow-2, .alert:hover {
box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
}

</style>

</head>
<body>
  <div class="container" style="padding-top:10rem;">
    @include('alerts.success')
    @include('alerts.warning')


      <h2>Seleccione el archivo a cargar</h2>
      <p class="lead">Formatos permitidos: <b>CSV y XLS</b></p>

      <!-- Upload  -->
      <form id="file-upload-form" class="uploader" action="{{ route('CargaMasiva') }}"  method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input id="file-upload" type="file" name="file" />

        <label for="file-upload" id="file-drag">
          <img id="file-image" src="#" alt="Preview" class="hidden">
          <div id="start">
            <i class="fa fa-download" aria-hidden="true"></i>
            <div>Seleccione el archivo o Arrastrelo hasta aqui</div>
            <div id="notimage" class="hidden">Por favor seleccione el archivo</div>
            <span id="file-upload-btn" class="btn btn-primary">Seleccione un archivo</span>
          </div>
          <div id="response" class="hidden">
            <div id="messages"></div>
            <progress class="progress" id="file-progress" value="0">
              <span>0</span>%
            </progress>
          </div>
        </label>
        <input type="submit" class="btn btn-primary btn-lg" style="margin-top: 3%">

      </form>
  </div>
</body>
<!-- <script src="{{ asset('js/bootstrap.min.css') }}" charset="utf-8"></script> -->

<script type="text/javascript">
$(".close-alert").click(function(e){
$(this).parent().remove();
e.preventDefault();
});

</script>


</html>
