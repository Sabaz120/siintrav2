@extends('layouts.app')
@section('contenido')
	
	<section id="libro">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">LIBRO DE VENTAS</h5>
      <form target="_blank" action="{{ route('premium.reporte') }}" method="POST" id="form">
          {{ csrf_field() }}
      <div class="row justify-content-justify">
        
          <div class="col-3 col-md-3 col-lg-3">
            <div class="form-group text-center">
              
                <span style="font-size: 25px;  color:#8D001F">*</span><label for="descripcion">Fecha Inicio:</label>
              
              <input type="date" class="form-control" id="fecha_inicio" name="fecha_inicio">
            </div>
          </div>
          <div class="col-3 col-md-3 col-lg-3">
            <div class="form-group text-center">
              <span style="font-size: 25px; color: #8D001F">*</span> <label for="descripcion">Fecha Fin:</label>
              <input type="date" class="form-control" id="fecha_fin" name="fecha_fin">
            </div>
          </div>
          <div class="col-3 col-md-3 col-lg-3">
            <div class="form-group text-center">
              <span class="text-warning" style="font-size: 25px; visibility:hidden">*</span> <label for="descripcion">Tipo de Libro:</label>
              <select class="form-control" name="tipo" id="tipo">
                <option value="">Seleccione un tipo de libro</option>
                <option value="1">Agente Autorizado</option>
                <option value="2">Post Venta</option>
                <option value="3">Venta Directa</option>
              </select>
            </div>
          </div>

          <div class="col-3 col-md-3 col-lg-3">
            <div class="form-group text-center">
              <span class="text-warning" style="font-size: 25px; visibility:hidden">*</span><label for="descripcion">Tipo de reporte:</label>
              <select class="form-control" name="tipoReporte" id="tipo">
                <option value="1">Resumen</option>
                <!--<option value="2">Detallado</option>-->
              </select>
            </div>
          </div>

          {{--<div class="col-3 col-md-3 col-lg-3 ">
            <div class="form-group text-center">
              <p style="margin-bottom: 0px;"><label for="descripcion">Grupos:</label></p>
              <select class="selectpicker" style="width: 100% !important;" multiple data-live-search="true" data-header="Seleccione un grupo" name="grupo[]">
                @foreach($grupos as $grupo)
                  <option value="{{ $grupo->codigo }}">{{ $grupo->nombre }}</option>
                @endforeach
              </select>
            </div>
          </div>--}}

          <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
            <!--<button type="submit" class="btn btn-success font-weight-bold" v-on:click="reportes">Generar</button>-->
            <button type="button" onclick="submitForm()" class="btn btn-success font-weight-bold">Generar</button>
          </div>
        
      </div>
      </form>
    </div>

  </section>

@endsection

@push('scripts')
	
	<script>

    $('.selectpicker').selectpicker();

    function submitForm(){

      var error = false

        if($("#fecha_inicio").val() == ""){
          alertify.error('Debes ingresar una fecha de inicio');
          error = true
        }

        if($("#fecha_fin").val() == ""){
          alertify.error('Debes ingresar una fecha fin');
          error = true
        }

        /*if($("#tipo").val() == ""){
          alertify.error('Debes seleccionar un tipo');
          error = true
        }*/

        if(!error)
          $("#form").submit()
      }

  </script>

@endpush