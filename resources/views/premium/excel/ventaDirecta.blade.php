@php
  
  $base_imponible = 0;
  $impuesto = 0;
  $totalVentaConIva= 0;

@endphp

<table>
  <tr></tr>
  <tr>
    <td height="25" colspan="4">
      @php

        $dateInicio = \Carbon\Carbon::parse($fechaInicio);
        $fechaInicio = $dateInicio->format('d/m/Y');

        $dateFin = \Carbon\Carbon::parse($fechaFin);
        $fechaFin = $dateFin->format('d/m/Y');

      @endphp

      <h3>Libro de Venta desde: {{ $fechaInicio }} hasta: {{ $fechaFin }} - Venta directa</h3>
    </td>
  </tr>
  <tr></tr>
</table>

<table>
  <thead>
    <tr>
      <th height="25">N° de Oper.</th>
      <th height="25">Fecha del Documento</th>
      <th height="25">R.I.F</th>
      <th height="25">Nombre o Razón Social</th>
      <th height="25">N° de Factura</th>
      <th height="25">N° Control de Factura</th>
      <th height="25">N° de Factura Asociada a N/C</th>
      <th height="25">Precio Unitario</th>
      <th height="25">Cantidad</th>
      <th height="25">Modelo</th>
      <th height="25">Tipo de Transacc.</th>
      
      <th height="25">Base Imponible</th>
      <th height="25">% Alicuota</th>
      @if($tipoReporte == 2)
      <th height="25">Tipo moneda</th>
      <th height="25">Referencia de pago</th>
      <th height="25">Identidad bancaria</th>
      <th height="25">Tasa de pago</th>
      @endif
      <th height="25">Impuesto IVA</th>
      <th height="25">Total Venta incluyendo IVA</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $key)

    @php
      $date = \Carbon\Carbon::parse($key->fecha_de_documento);
      $fecha = $date->format('d/m/Y');
    @endphp

    <tr>
      <td height="25">{{ $loop->index + 1 }}</td>
      <td height="25">{{ $fecha }}</td>
      <td height="25">{{ $key->rif }}</td>
      <td height="25">{{ $key->nombre_o_razon_Social }}</td>
      <td height="25">{{ $key->numero_factura }}</td>
      <td height="25">{{ $key->numero_control_factura }}</td>
      <td height="25">{{ $key->numero_referencia }}</td>
      <td height="25">{{ number_format($key->precio_unitario, 2, ',', '.') }}</td>
      <td height="25">{{ $key->cantidad }}</td>
      <td height="25">{{ $key->modelo_telefono }}</td>
      <td height="25">{{ $key->tipo_transaccion }}</td>
      
      <td height="25">{{ number_format($key->base_imponible, 2, ',', '.') }}</td>
      <td height="25">{{ $key->alicuota }}</td>
      @if($tipoReporte == 2)
      <td height="25">{{ $key->moneda }}</td>
      <td height="25">{{ $key->numero_referencia}}</td>
      <td height="25">{{ $key->banco}}</td>
      <td height="25">{{ $key->factorcamb}}</td>
      @endif
      <td height="25">{{ number_format($key->impuesto_iva, 2, ',', '.') }}</td>
      <th height="25">{{ number_format($key->base_imponible + $key->impuesto_iva, 2, ',', '.') }}</th>
    </tr>
      @php
        $base_imponible = $base_imponible + floatval($key->base_imponible);
        $impuesto = $impuesto + floatval($key->impuesto_iva);
        $totalVentaConIva = $totalVentaConIva + floatval($key->base_imponible + $key->impuesto_iva);
      @endphp
    @endforeach
  </tbody>
</table>

<table>
  <tbody>
    <tr>
        <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25"></td>
      <td height="25">{{ number_format($base_imponible, 2, ',', '.') }}</td>
      <td height="25"></td>

      
      

      @if($tipoReporte == 2)
        <td height="25"></td>
        <td height="25"></td>
        <td height="25"></td>
        <td height="25"></td>
      @endif

      <td height="25">{{ number_format($impuesto, 2, ',', '.') }}</td>
      <th height="25">{{ number_format($totalVentaConIva, 2, ',', '.') }}</th>  


    </tr>
  </tbody>
</table>

