@extends('layouts.app')



@section('contenido')
  
<section id="petro">


    <div style="position:fixed; top: 0; left: 0; width: 100%; height: 100vh; z-index: 999; background-color: rgba(0, 0, 0, 0.5);" v-if="loading">
        <div class="container">
          <div class="row">
            <div class="col-4">
      
            </div>
            <div class="col-4">
                <div class="loader-petro" style="margin-top: 40vh;"></div>
            </div>
          </div>
        </div>
      </div>

  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">GESTIÓN DE PRECIOS EN PETRO CON IVA</h5>
      <form method="POST" id="form">
          {{ csrf_field() }}
        <div class="row justify-content-justify">

            <div class="col-12 col-md-4 offset-md-4 col-lg-4 offset-lg-4 text-center pt-5">
                <div class="form-group">
                    <label for="exampleInputEmail1">Modelo:</label>
                    <div class="row">
                        <div class="col-10">
                            <select class="form-control" v-model="modelo">
                                <option :value="modelo.codigo" v-for="modelo in modelos">@{{ modelo.codigo }}</option>
                            </select>
                                    
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-info font-weight-bold" @click="buscar()">buscar</button>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
        <div class="row">

            <div class="offset-md-3 col-md-2">
                <label for="exampleInputEmail1">Precio 1:</label>
                <input class="form-control money" id="precio1">
            </div>
            <div class="col-md-2">
                <label for="exampleInputEmail1">Precio 2:</label>
                <input class="form-control money" id="precio2">
            </div>
            <div class="col-md-2">
                <label for="exampleInputEmail1">Precio 3:</label>
                <input class="form-control money" id="precio3">
            </div>


            <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
                <!--<button type="submit" class="btn btn-success font-weight-bold" v-on:click="reportes">Generar</button>-->
                <button type="button" @click="update()" class="btn btn-success font-weight-bold">Actualizar</button>
                <button type="button" @click="limpiar()" class="btn btn-warning font-weight-bold">Limpiar</button>
            </div>

        </div>
      </form>
    </div>

  </section>

@endsection

@push('scripts')
  
<script type="text/javascript">
  $(document).ready(function(){
    alertify.set('notifier','position', 'top-right');
    $(".money").val("0")
    $('.money').mask("00,0000", {
  reverse: true
});
  })
    const appVue = new Vue({
      el:'#petro',
      data:{
        // Variables DB
        modelos: [],
        modelo:"",
        loading:false
        // Variables DB
    
      },
      methods:{

        getModelos(){

          this.loading = true

          axios.get("{{ url('/petro/modelos/obtener') }}").then(response => {
             
              this.modelos = response.data
              this.loading = false

            }).catch((err) => {
              this.loading = false;
            })

        },
        
        update(){
          let precio1 = $("#precio1").val()
          let precio1Parsed = precio1.replace(".", "")
          precio1Parsed = precio1Parsed.replace(",", ".")

          let precio2 = $("#precio2").val()
          let precio2Parsed = precio2.replace(".", "")
          precio2Parsed = precio2Parsed.replace(",", ".")

          let precio3 = $("#precio3").val()
          let precio3Parsed = precio3.replace(".", "")
          precio3Parsed = precio3Parsed.replace(",", ".")
          
          this.loading = true

          if(!this.formHasErrors()){

            console.log(precio1Parsed, precio2Parsed, precio3Parsed)

            axios.post("{{ url('/petro/precios/actualizar') }}", {"precio1": precio1Parsed, "precio2": precio2Parsed, "precio3": precio3Parsed, "codigo": this.modelo}).then(response => {
              this.loading = false
                if(response.data.success == true){
                  
                  alertify.success(response.data.msg)

                }else{
                    alertify.error(response.data.msg)
                }
            })
          }else{
            this.loading = false
          }
          

        },
        formHasErrors(){

          let error = false

          /*console.log(this.precio1, this.precio2, this.precio3)

          if(parseFloat(this.precio1) === ""){
            console.log(true)
          }*/

          if($("#precio1").val() === "" || $("#precio2").val() === '' || $("#precio3").val() === '' || this.modelo === ""){
            alertify.error("Todos los campos son obligatorios")
            error = true
          }

          return error

        },
        buscar(){
          if(this.modelo != ""){
            this.loading = true
            axios.post("{{ url('/petro/precios/buscar') }}", {"codigo": this.modelo}).then(response => {
              this.loading = false
                if(response.data.success == true){
                    
                    alertify.success(response.data.msg)

                    if(response.data.data.length > 0){
                      if(response.data.data[0].preciofin1 != null){
                        let precio1 = response.data.data[0].preciofin1+""
                        let zeroamount1 = (precio1.indexOf(".") +1) - precio1.length

                        if(zeroamount1 < 0){

                          //precio1.padEnd("0000",(zeroamount1 * -1) + precio1.length)
                          var zeroString1 = ""
                          var length1 = 4 - (zeroamount1 * -1)
                          let  i = 0;

                          while( i < length1){
                            zeroString1 += "0"
                            i++
                          }
                          
                          
                        }

                        precio1.replace(".", ",")
                        $("#precio1").val(precio1 + zeroString1)
                        var precioParsed1 = $("#precio1").val()
                        $("#precio1").val(precioParsed1.replace(".", ","))
                      
                      }else{
                        $("#precio1").val("0")
                      }

                      if(response.data.data[0].preciofin2 != null){
                        

                        let precio2 = response.data.data[0].preciofin2+""
                        let zeroamount2 = (precio2.indexOf(".") +1) - precio2.length
                        if(zeroamount2 < 0){


                          var zeroString2 = ""
                          var length2 = 4 - (zeroamount2 * -1)
                          let  i = 0;

                          while( i < length2){
                            zeroString2 += "0"
                            i++
                          }

                          
                        }

                        precio2.replace(".", ",")
                        $("#precio2").val(precio2 + zeroString2)
                        var precioParsed2 = $("#precio2").val()
                        $("#precio2").val(precioParsed2.replace(".", ","))
                      }else{
                        $("#precio2").val("0")
                      }
                        
                      if(response.data.data[0].preciofin3 != null){
                        let precio3 = response.data.data[0].preciofin3+""
                        let zeroamount3 = (precio3.indexOf(".") +1) - precio3.length
                        if(zeroamount3 < 0){
                          var zeroString3 = ""
                          var length3 = 4 - (zeroamount3 * -1)
                          let  i = 0;

                          while( i < length3){
                            zeroString3 += "0"
                            i++
                          }

                        }

                        $("#precio3").val(precio3 + zeroString3)
                        var precioParsed3 = $("#precio3").val()
                        $("#precio3").val(precioParsed3.replace(".", ","))
                      }else{
                        $("#precio3").val("0")
                      }
                        
                        

                        //$("#precio1").val()
                        //$("#precio2").val(response.data.data[0].preciofin2) 
                        //$("#precio3").val(response.data.data[0].preciofin3) 
                    }else{
                      $("#precio1").val(0)
                      $("#precio2").val(0)
                      $("#precio3").val(0)
                    }

                }else{
                  
                    alertify.error(response.data.msg)
                }
            })
          }else{
            alertify.error("Debe elegir un modelo")
          }

        },
        pad_with_zeroes(number, length) {

          
          while (my_string.length < length) {
              my_string = '0' + my_string;
          }

          return my_string;

        },
        limpiar(){
          $("#precio1").val(0)
          $("#precio2").val(0)
          $("#precio3").val(0)
            this.modelo = ""
        }

      },
      mounted(){
        this.getModelos()
      }
    });//const app= new Vue
    </script>

@endpush