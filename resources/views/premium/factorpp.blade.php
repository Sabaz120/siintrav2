@extends('layouts.app')
@section('contenido')
	
<section id="petro">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">Precio PETRO</h5>
      <form method="POST" id="form">
          {{ csrf_field() }}
        <div class="row justify-content-justify">

            <div class="col-12 col-md-3 offset-md-3 col-lg-3 offset-lg-3 text-center pt-5">
                <div class="form-group">
                    <label for="exampleInputEmail1">Precio Actual</label>
                    <input readonly type="email" class="form-control" id="exampleInputEmail1" v-model="precio" aria-describedby="emailHelp" placeholder="Cotización del petro" >
                </div>
            </div>

            <div class="col-12 col-md-3 col-lg-3 text-center pt-5">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nueva cotización</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" v-model="nuevoPrecio" aria-describedby="emailHelp" placeholder="Cotización del petro" @keypress="isNumber($event)">
                </div>
            </div>

          <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
            <!--<button type="submit" class="btn btn-success font-weight-bold" v-on:click="reportes">Generar</button>-->
            <button type="button" @click="update()" class="btn btn-success font-weight-bold">Actualizar</button>
          </div>
        
        </div>
      </form>
    </div>

  </section>

@endsection

@push('scripts')
	
<script type="text/javascript">
    alertify.set('notifier','position', 'top-right');
    const appVue = new Vue({
      el:'#petro',
      data:{
        // Variables DB
        precio: {{ $petro }},
        nuevoPrecio:0
        // Variables DB
    
      },
      methods:{
        
        update(){
          
          if(!this.formHasErrors()){
            axios.post("{{ url('/petro/actualizacion-factor/update') }}", {"precio": this.nuevoPrecio}).then(response => {
              
                if(response.data.success == true){

                  alertify.success(response.data.msg)
                  
                  this.precio = this.nuevoPrecio
                  this.nuevoPrecio = 0

                }else{
                    alertify.error(response.data.msg)
                }
            })
          }
          

        },
        formHasErrors(){

          let error = false

          if(this.nuevoPrecio == "" || this.nuevoPrecio == 0 || this.nuevoPrecio == null){
            alertify.error("La nueva cotización no puede estar vacía o ser igual a 0")
            error = true
          }

          return error

        },
        isNumber: function(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();
          } else {
            return true;
          }
        }

      },
      mounted(){
        
      }
    });//const app= new Vue
    </script>

@endpush