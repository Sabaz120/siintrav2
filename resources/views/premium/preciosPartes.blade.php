@extends('layouts.app')
@section('contenido')
	
<section id="precioPartes">
  <div class="card" >
    <div class="card-body">
      <h5 class="font-weight-bold text-center text-dark  header-title m-t-0 m-b-30 pt-5">Precios Partes y Piezas</h5>
      <form method="POST" id="form">
          {{ csrf_field() }}
        <div class="row justify-content-justify">

            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Código</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" v-model="codigo" aria-describedby="emailHelp" placeholder="codigo partes piezas" >
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Costo</label>
                    <input readonly  class="form-control" id="exampleInputEmail1" v-model="costo">
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Factor</label>
                    <input readonly  class="form-control" id="exampleInputEmail1" v-model="factor">
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Precio sin impuesto</label>
                    <input readonly  class="form-control" id="exampleInputEmail1" v-model="precio1">
                </div>
            </div>

            <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Precio con impueesto</label>
                        <input readonly  class="form-control" id="exampleInputEmail1" v-model="preciofin1">
                    </div>
                </div>
            
                <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">CFOB</label>
                            <input readonly  class="form-control" id="exampleInputEmail1" v-model="cfob">
                        </div>
                    </div>

            {{--<div class="col-12 col-md-3 col-lg-3 text-center pt-5">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nueva cotización</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" v-model="nuevoPrecio" aria-describedby="emailHelp" placeholder="Cotización del petro" @keypress="isNumber($event)">
                </div>
            </div>--}}

          <div class="col-12 col-md-12 col-lg-12 text-center pt-5">
            <!--<button type="submit" class="btn btn-success font-weight-bold" v-on:click="reportes">Generar</button>-->
            <button type="button" @click="search()" class="btn btn-success font-weight-bold">Buscar</button>
          </div>
        
        </div>
      </form>
    </div>

  </section>

@endsection

@push('scripts')
	
<script type="text/javascript">
    alertify.set('notifier','position', 'top-right');
    const appVue = new Vue({
      el:'#precioPartes',
      data:{
        // Variables D
        codigo:"",
        costo:"",
        factor:"",
        precio1:"",
        preciofin1:"",
        cfob:""
        // Variables DB
    
      },
      methods:{
        
        search(){
          
          if(!this.formHasErrors()){
            axios.post("{{ url('/premium/precio-partes-piezas/search') }}", {"codigo": this.codigo}).then(response => {
                console.log(response)
                if(response.data.success == true){

                  alertify.success(response.data.msg)
                  
                    this.costo = response.data.response[0].COSTO
                    this.factor  = response.data.response[0].FACTOR
                    this.precio1 =     response.data.response[0].PRECIO1
                    this.preciofin1 = response.data.response[0].PRECIOFIN1
                    this.cfob = response.data.response[0].CFOB

                }else{
                    alertify.error(response.data.msg)
                }
            })
          }
          

        },
        formHasErrors(){

          let error = false

          if(this.codigo == "" ){
            alertify.error("Código es requerido")
            error = true
          }

          return error

        },
        isNumber: function(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();
          } else {
            return true;
          }
        }

      },
      mounted(){
        
      }
    });//const app= new Vue
    </script>

@endpush