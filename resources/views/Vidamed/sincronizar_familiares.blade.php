@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <!-- <div class="card-header"> -->
      <!-- <h5 class="text-center header-title pt-1">REGISTRO DE OFICIOS</h5> -->
    <!-- </div> -->
    <div class="card-body" v-if="personalFamiliar.length>0">
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">FAMILIARES DESACTUALIZADOS EN HCM (@{{personalFamiliar.length}})</h3>
      <div class="row justify-content-center">
        <div class="col-md-12 text-center" v-for="(familiares,index) in personalFamiliar">
          <h4>Personal #@{{index+1}}</h4>
          <div class="row">
            <div class="col-md-6">
              <h5 class="badge badge-primary">SIGESP</h5>
              <table class="table table-bordered table-shape table-striped">
                <thead class="bg-primary text-white font-weight-bold">
                  <tr>
                    <th>CÉDULA EMPLEADO</th>
                    <th>APELLIDOS Y NOMBRES</th>
                    <th>CÉDULA FAMILIAR</th>
                    <th>FECHA NACIMIENTO FAMILIAR</th>
                    <th>NEXO FAMILIAR</th>
                    <th>SEXO FAMILIAR</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="familiar in familiares.sigesp">
                    <td>@{{familiar.cedula}}</td>
                    <td>@{{familiar.nombre_familiar}}</td>
                    <td>@{{familiar.cedfam}}</td>
                    <td>@{{familiar.fecnacfam}}</td>
                    <td>@{{familiar.nexfam}}</td>
                    <td>@{{familiar.sexfam}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-6">
              <h5 class="badge badge-primary">VIDAMED</h5>
              <table class="table table-bordered table-shape table-striped">
                <thead class="bg-primary text-white font-weight-bold">
                  <tr>
                    <th>CÉDULA EMPLEADO</th>
                    <th>APELLIDOS Y NOMBRES</th>
                    <th>CÉDULA FAMILIAR</th>
                    <th>FECHA NACIMIENTO FAMILIAR</th>
                    <th>NEXO FAMILIAR</th>
                    <th>SEXO FAMILIAR</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="familiar in familiares.vidamed">
                    <td>@{{familiar.cedu_titu}}</td>
                    <td>@{{familiar.nombre_fami}}</td>
                    <td>@{{familiar.cedu_fami}}</td>
                    <td>@{{familiar.fecha_nac}}</td>
                    <td>@{{familiar.parentesco}}</td>
                    <td>@{{familiar.sexo}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-body" v-else>
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">El servicio de Vidamed esta sincronizado con los activos en SIGESP</h3>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    // Variables DB
    personalFamiliar: {!! json_encode($personalFamiliar) ? json_encode($personalFamiliar) : "''"!!},
    // Variables DB

  },
  methods:{

  },
  mounted(){
    var d=new Date();
    var ano= d.getFullYear();
    var mes= d.getMonth()+1;
    var dia=d.getDate();
    if(dia<10)
    dia='0'+dia;
    if(mes<10)
    mes='0'+mes;
    console.log('asdada');
  }
});//const app= new Vue
</script>
@endpush
