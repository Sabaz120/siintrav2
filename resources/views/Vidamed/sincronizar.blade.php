@extends('layouts.app')
@section('contenido')
<section id="gestion">
  <div class="card" >
    <!-- <div class="card-header"> -->
      <!-- <h5 class="text-center header-title pt-1">REGISTRO DE OFICIOS</h5> -->
    <!-- </div> -->
    <div class="card-body" v-if="personasAfiliadas.length>0">
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">PERSONAS NO REGISTRADAS EN HCM (@{{personasAfiliadas.length}})</h3>
      <div class="row justify-content-center">
        <div class="col-md-12 text-center">
          <table class="table table-bordered table-shape table-striped">
            <thead class="bg-primary text-white font-weight-bold">
              <tr>
                <td>APELLIDOS Y NOMBRES</td>
                <td>CARGO</td>
                <td>ACCIÓN</td>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(persona,index) in personasAfiliadas">
                <td>@{{persona.nombres}}</td>
                <td>@{{persona.cargo}}</td>
                <td>
                  <button type="button" class="btn btn-success btn-sm" @click="afiliarEmpleado(index)" name="button">
                    <i class="fa fa-plus"></i>
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card-body" v-else>
      <h3 class="text-center header-title m-t-0 m-b-20 pt-2">El servicio de Vidamed esta sincronizado con los activos en SIGESP</h3>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
alertify.set('notifier','position', 'top-right');
const appVue = new Vue({
  el:'#gestion',
  data:{
    // Variables DB
    personasAfiliadas: {!! json_encode($PersonasNoAfiliadas) ? json_encode($PersonasNoAfiliadas) : "''"!!},
    // Variables DB

  },
  methods:{
    afiliarEmpleado(index){
      // console.log(this.personasAfiliadas[index]);
      axios.post('{{ url("vidamed/sincronizar_trabajador") }}', {personal:this.personasAfiliadas[index]}).then(response => {
        console.log(response.data);
        if(response.data.error==0){
          alertify.success(this.personasAfiliadas[index].nombres)+" afiliado al seguro HCM correctamente.";
          this.personasAfiliadas.splice(index,1);
        }else{
          alertify.error('Se ha producido un error en el sistema, notifique al administrador.');
          console.log('Error:');
          console.log(response.data);
        }
      }).catch(error => {
        console.log(error);
      });
    }
  },
  mounted(){
    var d=new Date();
    var ano= d.getFullYear();
    var mes= d.getMonth()+1;
    var dia=d.getDate();
    if(dia<10)
    dia='0'+dia;
    if(mes<10)
    mes='0'+mes;
    console.log('asdada');
  }
});//const app= new Vue
</script>
@endpush
