
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Highcharts from 'highcharts'
import HighchartsVue from 'highcharts-vue'
import money from 'v-money'
import VOwlCarousel from 'vue-owl-carousel'
import StockChart from './components/StockChart'
Vue.use(HighchartsVue);
Vue.use(VOwlCarousel);
Vue.use(money,{
  precision: 2,
  decimal: ',',
  thousands: '.',
  masked:true
});
