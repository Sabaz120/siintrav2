<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//ENtidades para hacer respaldos de datos en un json. - Sabas was here
// use App\Modelos\Sigesp\sno_personal;
// use App\Modelos\Sigesp\sno_cargo;
// use App\Modelos\Sigesp\sno_dedicacion;
// use App\Modelos\Sigesp\sno_personalnomina;
// use App\Modelos\Sigesp\sno_profesion;
// use App\Modelos\Sigesp\sno_ubicacionfisica;
// use Storage;
// Route::get('/test', function () {
//   $personal=sno_ubicacionfisica::all();
//   $title="sno_ubicacionfisica.json";
//   Storage::disk('local')->put($title, json_encode($personal));
// });
Route::get('/', function () {
  return view('welcome');
});
Route::get('biometrico/{ip}','Api\BiometricosController@obtenerMarcaje');
Route::post('biometrico_jornada/','Api\BiometricosController@insertar');
Auth::routes();

Route::get('/limpiarCache',function(){
  Artisan::call('config:cache');
  Artisan::call('cache:clear');
  return redirect('/');
  // Artisan::call('cache:forget spatie.permission.cache');//php artisan cache:forget spatie.permission.cache limpiar cache permisologia
});//limpiarCache

Route::get('/biometrico/prueba/{ip}', 'Api\BiometricosController@obtenerMarcajePrueba');

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware'=>'auth'], function () {


  /**************GESTIÓN SOCIAL***********************/
  //GESTIONAR PRODUCTOS
  Route::get('/gestionarProductos', function () {return view('gestionSocial.productos.registroProductos');});
  Route::post('registroProductos','GestionSocial\GestionProductosController@registrarProductos')->name('registroProductos');
  Route::get('obtenerProductos','GestionSocial\GestionProductosController@obtenerProductos')->name('obtenerProductos');
  Route::post('actualizarProductos','GestionSocial\GestionProductosController@actualizacionProductos')->name('actualizarProductos');
  Route::post('eliminarProductos','GestionSocial\GestionProductosController@deshabilitarProductos')->name('eliminarProductos');
  Route::post('obtenerProductosAsociados','GestionSocial\GestionProductosController@obtenerProductosAsociados')->name('obtenerProductosAsociados');
  Route::post('obtenerProductosRegistrados','GestionSocial\GestionProductosController@obtenerProductosRegistrados')->name('obtenerProductosRegistrados');

  //JORNADAS
  Route::get('/registroJornada', function () {return view('gestionSocial.jornadas.registro');});
  Route::post('registroJornada','GestionSocial\JornadasController@registroJornada')->name('registroJornada');
  Route::post('actualizarJornada','GestionSocial\JornadasController@actualizarJornada')->name('actualizarJornada');
  Route::get('obtenerJornadas','GestionSocial\JornadasController@obtenerJornadas')->name('obtenerJornadas');
  Route::get('/entregaBeneficios', function () {return view('gestionSocial.jornadas.entregaBeneficios');});
  Route::post('asociarProductos','GestionSocial\JornadasController@asociacionProductos')->name('asociarProductos');
  Route::post('obtenerProductosJornadas','GestionSocial\JornadasController@obtenerDetalleJornada')->name('obtenerProductosJornadas');
  Route::post('actualizarDetalleJornada','GestionSocial\JornadasController@actualizarDetalleJornada')->name('actualizarDetalleJornada');

  //BIOMETRICOS
  Route::get('/gestionarBiometricos', function () {return view('gestionSocial.biometricos.gestionarBiometricos');});
  Route::post('registrarBiometricos','GestionSocial\GestionarBiometricosController@registrarBiometricos')->name('registrarBiometricos');
  Route::get('obtenerBiometricos','GestionSocial\GestionarBiometricosController@obtenerBiometricos')->name('obtenerBiometricos');
  Route::put('actualizarBiometricos','GestionSocial\GestionarBiometricosController@actualizarBiometricos')->name('actualizarBiometricos');
  Route::delete('borrarBiometricos/{id}','GestionSocial\GestionarBiometricosController@borrarBiometricos')->name('borrarBiometricos');

  //Clasificacion productos
  Route::get('/clasificacionProducto', function () {return view('gestionSocial.ClasificacionProducto.index');});
  Route::get('clasificacionProductos','GestionSocial\ClasificacionProductoController@index')->name('api.index.clasificacion.producto');
  Route::post('clasificacionProducto','GestionSocial\ClasificacionProductoController@store')->name('api.store.clasificacion.producto');
  Route::put('clasificacionProducto','GestionSocial\ClasificacionProductoController@update')->name('api.update.clasificacion.producto');
  Route::delete('clasificacionProducto/{id}','GestionSocial\ClasificacionProductoController@destroy')->name('api.destroy.clasificacion.producto');

  //Reporte
  Route::get('/reportesJornada', 'GestionSocial\Reportes\entregasBeneficiosController@index')->name('gestionSocial.index');
  Route::get('/reportesJornada/{id}/estadistica', 'GestionSocial\Reportes\entregasBeneficiosController@estadisticas')->name('gestionSocial.estadisticas');
  Route::get('reporteEntregaBeneficio/{id}', 'GestionSocial\Reportes\entregasBeneficiosController@entregasBeneficios')->name('gestionSocial.reporte');


  /**************FIN GESTIÓN SOCIAL***********************/
  //Gestion solicitudes - Oficio - Oferta economica
  Route::get('gestionSolicitudes','controlOficios\GestionSolicitudesController@index')->name('GestionSolicitudes');
  Route::post('guardarOfertaEconomica','controlOficios\GestionSolicitudesController@crearOfertaEconomica')->name('GuardarOfertaEconomica');
  Route::post('ActualizarProductoOfertaEconomica','controlOficios\GestionSolicitudesController@actualizarProductoOfertaEconomica')->name('ActualizarProductoOfertaEconomica');
  Route::post('ActualizarProductosOfertaEconomica','controlOficios\GestionSolicitudesController@ActualizarProductosOfertaEconomica')->name('ActualizarProductosOfertaEconomica');
  Route::post('generarPDFOferta','controlOficios\GestionSolicitudesController@generarPDFOferta')->name('generarPDFOferta');
  Route::post('generarPDFOfertaPetro','controlOficios\GestionSolicitudesController@generarPDFOfertaPetro')->name('generarPDFOfertaPetro');
  Route::post('generarPDFOfertaHibrido','controlOficios\GestionSolicitudesController@generarPDFOfertaHibrido')->name('generarPDFOfertaHibrido');
  Route::post('obtenerOficiosOferta','controlOficios\GestionSolicitudesController@obtenerOficiosFiltro')->name('FiltroObtenerOficios');
  Route::post('asociarPagoOferta','controlOficios\GestionSolicitudesController@asociarPago')->name('asociarPago');
  Route::post('agregarReferenciaPagoAdicional','controlOficios\GestionSolicitudesController@asociarReferenciaAdicionalPago')->name('asociarReferenciaAdicionalPago');
  Route::post('actualizarPagoOferta','controlOficios\GestionSolicitudesController@actualizarPago')->name('actualizarPago');
  Route::post('borrarPagoOferta','controlOficios\GestionSolicitudesController@borrarPago')->name('borrarPago');
  Route::post('registrarChofer','controlOficios\GestionSolicitudesController@registrarChofer')->name('registrarChofer');
  Route::post('actualizarChofer','controlOficios\GestionSolicitudesController@actualizarChofer')->name('actualizarChofer');
  Route::post('actualizarDatosChofer2','controlOficios\GestionSolicitudesController@actualizarDatosChofer2')->name('actualizarDatosChofer2');
  Route::post('confirmarPago','controlOficios\GestionSolicitudesController@confirmarPago')->name('confirmarPago');
  Route::post('revertirPago','controlOficios\GestionSolicitudesController@revertirPago')->name('revertirValidacionPago');
  Route::post('denegarCompra','controlOficios\GestionSolicitudesController@denegarCompra')->name('denegarCompra');
  Route::post('actualizarEstadoOficio','controlOficios\GestionSolicitudesController@actualizarEstadoOficio')->name('actualizarEstadoOficio');
  Route::post('agregarProductoOfertaEconomica','controlOficios\GestionSolicitudesController@agregarProductoOfertaEconomica')->name('agregarProductoOfertaEconomica');
  Route::post('eliminarProductoOfertaEconomica','controlOficios\GestionSolicitudesController@eliminarProductoOfertaEconomica')->name('eliminarProductoOfertaEconomica');

  /***RUTAS LUIS***/
  Route::post('registrarDatosChofer','controlOficios\GestionSolicitudesController@registrarDatosChofer')->name('api.controloficio.registrarDatosChofer');

  //rE INTEGRO
  Route::get('listadoReintegro','controlOficios\ListadoReintegrosController@index')->name('listadoReintegro');
  Route::post('guardarReembolso','controlOficios\ListadoReintegrosController@guardarReembolso')->name('guardarReembolso');
  Route::get('listaFacturacion','controlOficios\OfertasEconomicasFacturacionController@listaFacturacion')->name('listaFacturacion');
  Route::post('guardarDatosDespacho','controlOficios\OfertasEconomicasFacturacionController@guardarDatosDespacho')->name('guardarDatosDespacho');
  Route::post('confirmarDespacho','controlOficios\OfertasEconomicasFacturacionController@confirmarDespacho')->name('confirmarDespacho');
  Route::get('resumenDespacho',function(){
    return view('controlOficios/ResumenDespacho/listado');
  })->name('resumenDespacho');
  ////////Gestion DEspacho - Requisicion
  Route::get('gestionDespacho',function(){
    return view('controlOficios.GestionDespacho.listado',['deducciones'=>\App\Modelos\ControlOficios\Deduccion::all(), 'tipoVentas' => App\Modelos\ControlOficios\TipoVenta::all()]);
  })->name('gestionDespacho');
  ///////Corrección Requisición
  Route::get('correccionRequisicion',function(){
    return view('controlOficios.GestionDespacho.correccionRequisicion');
  })->name('correccionRequisicion');

  Route::post('api/requisicion/cerrar_oferta_economica', "controlOficios\GestionOficioController@cerrarOferta");

  //Gestion solicitudes - Oficio - Oferta economica

  Route::get('CargaMasiva', function () {return view('CargaMasiva.CargaMasiva');});
  Route::post('CargaMasiva', 'CargaMasivaController@cargaMasivaNomina')->name('CargaMasiva');


  /**Control de Oficios**/
  /*---ASIGNACION RESPONSABLE---*/
  Route::get('/asignacion_responsable', 'controlOficios\AsignacionResponsableController@GestionResponsables')->name('GestionResponsables');
  Route::post('/asignacion_responsable', 'controlOficios\AsignacionResponsableController@AsignarResponsable')->name('AsignacionResponsable');
  Route::post('/actualizar_responsable', 'controlOficios\AsignacionResponsableController@actualizarResponsable')->name('ActualizarResponsable');
  Route::post('/borrar_responsable', 'controlOficios\AsignacionResponsableController@borrarResponsable')->name('borrarResponsables');

  /*---ASIGNACION RESPONSABLE---*/

  /*---Getion de Materiales---*/
  Route::get('/gestionar_materiales', 'controlOficios\GestionMaterialesController@gestionMaterial')->name('GestionarMateriales');
  Route::post('/gestionar_materiales', 'controlOficios\GestionMaterialesController@registrarMaterial')->name('RegistrarMaterial');
  Route::post('/actualizar_material', 'controlOficios\GestionMaterialesController@actualizarMaterial')->name('ActualizarMaterial');
  Route::post('/eliminar_material', 'controlOficios\GestionMaterialesController@eliminarMaterial')->name('EliminarMaterial');
  /*---Getion de Materiales---*/
  /*---Getion de Recaudos---*/
  Route::get('/gestionar_recaudos', 'controlOficios\GestionRecaudosController@index')->name('GestionarRecaudos');
  Route::post('/gestionar_recaudos', 'controlOficios\GestionRecaudosController@store')->name('RegistrarRecaudos');
  Route::post('/actualizar_recaudo', 'controlOficios\GestionRecaudosController@update')->name('ActualizarRecaudo');
  Route::post('/eliminar_recaudo', 'controlOficios\GestionRecaudosController@delete')->name('EliminarRecaudo');
  /*---Getion de Recaudos---*/
  /*---Getion de Deducciones---*/
  Route::get('/gestionar_deducciones', 'controlOficios\GestionDeduccionesController@index')->name('GestionarDeducciones');
  Route::post('/gestionar_deducciones', 'controlOficios\GestionDeduccionesController@store')->name('RegistrarDeduccion');
  Route::post('/actualizar_deducciones', 'controlOficios\GestionDeduccionesController@update')->name('ActualizarDeduccion');
  Route::post('/eliminar_deducciones', 'controlOficios\GestionDeduccionesController@delete')->name('EliminarDeduccion');
  /*---Getion de Deducciones---*/

  /*---Getion de Motivos---*/
  Route::get('/gestionar_motivos', 'controlOficios\motivoSolicitudController@mostrarMotivoSolicitud')->name('GestionarMotivos');
  Route::post('/registrar_motivo_solicitud', 'controlOficios\motivoSolicitudController@registrarMotivoSolicitud')->name('RegistrarMotivoSolicitud');
  Route::post('/modificar_motivo_solicitud', 'controlOficios\motivoSolicitudController@modificarMotivoSolicitud')->name('ModificarMotivoSolicitud');
  Route::post('/eliminar_motivo_solicitud', 'controlOficios\motivoSolicitudController@eliminarMotivoSolicitud')->name('EliminarMotivoSolicitud');
  /*---Clasificacion Ente---*/
  Route::get('/clasificacion_ente', 'controlOficios\clasificacionEnteController@mostrarClasificacionEnte')->name('ClasificacionEnte');
  Route::post('/registrar_clasificacion_ente', 'controlOficios\clasificacionEnteController@registrarClasificacionEnte')->name('RegistrarClasificacionEnte');
  Route::post('/modificar_clasificacion_ente', 'controlOficios\clasificacionEnteController@modificarClasificacionEnte')->name('ModificarClasificacionEnte');
  Route::post('/eliminar_clasificacion_ente', 'controlOficios\clasificacionEnteController@eliminarClasificacionEnte')->name('EliminarClasificacionEnte');
  /*---Nivel de Atención---*/
  Route::get('/nivel_atencion', 'controlOficios\nivelAtencionController@mostrarNivelAtencion')->name('NivelAtencion');
  Route::post('/registrar_nivel_atencion', 'controlOficios\nivelAtencionController@registrarNivelAtencion')->name('RegistrarNivelAtencion');
  Route::post('/modificar_nivel_atencion', 'controlOficios\nivelAtencionController@modificarNivelAtencion')->name('ModificarNivelAtencion');
  Route::post('/eliminar_nivel_atencion', 'controlOficios\nivelAtencionController@eliminarNivelAtencion')->name('EliminarNivelAtencion');
  /*---Tiempo de Atención---*/
  Route::get('/tiempo_atencion', 'controlOficios\tiempoAtencionController@mostrarTiempoAtencion')->name('TiempoAtencion');
  Route::post('/registrar_tiempo_atencion', 'controlOficios\tiempoAtencionController@registrarTiempoAtencion')->name('RegistrarTiempoAtencion');
  Route::post('/modificar_tiempo_atencion', 'controlOficios\tiempoAtencionController@modificarTiempoAtencion')->name('ModificarTiempoAtencion');
  Route::post('/eliminar_tiempo_atencion', 'controlOficios\tiempoAtencionController@eliminarTiempoAtencion')->name('EliminarTiempoAtencion');
  /*---Observaciones y condiciones---*/
  Route::get('/observaciones_condiciones', 'controlOficios\ObservacionesCondicionesController@index')->name('ObservacionesGenerales');
  Route::post('/observaciones_condiciones', 'controlOficios\ObservacionesCondicionesController@store');
  Route::put('/observaciones_condiciones', 'controlOficios\ObservacionesCondicionesController@update');
  Route::post('/observaciones_condiciones/deshabilitar', 'controlOficios\ObservacionesCondicionesController@destroy');
  Route::post('/observaciones_condiciones/habilitar', 'controlOficios\ObservacionesCondicionesController@restore');

  /*---Departamentos---*/
  Route::get('/gestionar_departamentos', 'controlOficios\departamentosController@mostrarDepartamentos')->name('Departamentos');
  Route::post('/registrar_departamentos', 'controlOficios\departamentosController@registrarDepartamentos')->name('RegistrarDepartamentos');
  Route::post('/modificar_departamentos', 'controlOficios\departamentosController@modificarDepartamentos')->name('ModificarDepartamentos');
  Route::post('/eliminar_departamentos', 'controlOficios\departamentosController@eliminarDepartamentos')->name('EliminarDepartamentos');
  /*---Gestión de Entes---*/
  Route::get('/gestionar_entes', 'controlOficios\enteController@mostrarEntes')->name('Entes');//
  Route::post('/registrar_entes', 'controlOficios\enteController@registrarEnte')->name('RegistrarEnte');
  Route::post('/modificar_entes', 'controlOficios\enteController@modificarEnte')->name('ModificarEnte');
  Route::post('/eliminar_entes', 'controlOficios\enteController@eliminarEnte')->name('EliminarEnte');
  /*---Personal Natural---*/

  Route::post('/buscar_personal', 'controlOficios\personalNaturalController@buscarPersona');

  Route::get('/gestionar_personal_natural', 'controlOficios\personalNaturalController@mostrarPersonalNatural')->name('PersonalNatural');
  Route::post('/registrar_personal_natural', 'controlOficios\personalNaturalController@registrarPersonalNatural')->name('RegistrarPersonalNatural');
  Route::post('/modificar_personal_natural', 'controlOficios\personalNaturalController@modificarPersonalNatural')->name('ModificarPersonalNatural');
  Route::post('/eliminar_personal_natural', 'controlOficios\personalNaturalController@eliminarPersonalNatural')->name('EliminarPersonalNatural');
  /*---Personal Juridica---*/
  Route::get('/registrar_personal_juridico', 'controlOficios\personalJuridicoController@mostrarPersonalJuridico')->name('PersonalJuridico');
  Route::post('/registrar_personal_juridico', 'controlOficios\personalJuridicoController@registrarPersonalJuridico')->name('RegistrarPersonalJuridico');
  Route::post('/modificar_personal_juridico', 'controlOficios\personalJuridicoController@modificarPersonalJuridico')->name('ModificarPersonalJuridico');
  /*---Getion de Vehiculos---*/
  Route::get('/gestionar_vehiculos', 'controlOficios\VehiculoController@mostrarVehiculo')->name('GestionarVehiculos');
  Route::post('/registrar_vehiculo', 'controlOficios\VehiculoController@registrarVehiculo')->name('RegistrarVehiculo');
  Route::post('/modificar_vehiculo', 'controlOficios\VehiculoController@modificarVehiculo')->name('ModificarVehiculo');
  Route::post('/eliminar_vehiculo', 'controlOficios\VehiculoController@eliminarVehiculo')->name('EliminarVehiculo');
  // Route::post('/eliminar_personal_juridico', 'controlOficios\personalJuridicoController@eliminarPersonalJuridico')->name('EliminarPersonalJuridico');
  /*---Recepción oficios---*/
  Route::get('trazabilidad_oficios','controlOficios\GestionOficioController@trazabilidad');
  Route::get('recepcion_oficios','controlOficios\GestionOficioController@registrar');
  Route::post('recepcion_oficios','controlOficios\GestionOficioController@registrarPost');
  Route::post('recepcion_oficios/borrar','controlOficios\GestionOficioController@borrarOficio');
  Route::post('recepcion_oficios/actualizar','controlOficios\GestionOficioController@actualizarOficio');
  Route::post('recepcion_oficios/asignarTlf','controlOficios\GestionOficioController@asignarTlf');
  Route::post('recepcion_oficios/borrarTlf','controlOficios\GestionOficioController@borrarTlf');
  /*---Recepción oficios---*/
  /*---Verificación solicitudes oficios---*/
  Route::get('verificacion_solicitudes','controlOficios\VerificacionSolicitudesController@index');
  Route::post('aprobar_oficio','controlOficios\VerificacionSolicitudesController@aprobarOficio')->name('aprobarOficio');
  Route::post('rechazar_oficio','controlOficios\VerificacionSolicitudesController@rechazarOficio')->name('rechazarOficio');
  Route::post('obtenerOficioFiltro','controlOficios\VerificacionSolicitudesController@obtenerOficiosConFiltro')->name('obtenerOficiosFiltro');
  Route::post('obtenerDisponibilidadProducto','controlOficios\VerificacionSolicitudesController@obtenerDisponibilidadProducto')->name('obtenerDisponibilidadProducto');
  Route::post('obtenerModalidadesYDisponibilidadProducto','controlOficios\VerificacionSolicitudesController@obtenerModalidadesYDisponibilidadProducto')->name('obtenerModalidadesYDisponibilidadProducto');
  /*---Verificación solicitudes oficios---*/
  /*--- Recepción de oficios donativos ---*/
  Route::post('agregar_materiales_donativos', 'controlOficios\GestionOficioController@registrarMateriales');
  Route::post('obtener_materiales_donativos', 'controlOficios\GestionOficioController@obtenerMateriales');
  Route::post('eliminar_materiales_donados', 'controlOficios\GestionOficioController@eliminarMaterial');
  /*--- Recepción de oficios donativos ---*/

  /*--- Verificación de solicitudes de donativos ---*/
  Route::get('gestion_solicitudes_donativos', 'controlOficios\VerificacionSolicitudesDonativos@index')->name('GestionSolicitudesDonativos');
  Route::get('gestion_solicitudes_donativos/obtenerPersonalTrabajador', 'controlOficios\VerificacionSolicitudesDonativos@obtenerOficiosPersonalTrabajador')->name('GestionSolicitudesDonativosObtenerPersonalTrabajador');
  Route::get('gestion_solicitudes_donativos/obtenerPersonalNatural', 'controlOficios\VerificacionSolicitudesDonativos@obtenerOficiosPersonalNatural')->name('GestionSolicitudesDonativosObtenerPersonalNatural');
  Route::post('gestion_solicitudes_donativos/elliminar', 'controlOficios\VerificacionSolicitudesDonativos@eliminarMaterial')->name('GestionSolicitudesDonativosEliminar');
  Route::post('gestion_solicitudes_donativos/aprobacion', 'controlOficios\VerificacionSolicitudesDonativos@aprobarSolicitud')->name('GestionSolicitudesDonativosAprobacion');
  Route::post('gestion_solicitudes_donativos/aprobacionv2', 'controlOficios\VerificacionSolicitudesDonativos@aprobarSolicitudv2')->name('GestionSolicitudesDonativosAprobacionv2');//Aprobar solicitud de donativo v2
  Route::post('gestion_solicitudes_donativos/rechazar', 'controlOficios\VerificacionSolicitudesDonativos@rechazarSolicitud')->name('GestionSolicitudesDonativosRechazar');
  /*--- Verificación de solicitudes de donativos ---*/

  /*---Gestion de Solicitudes de Donativos Aprobadas---*/
  Route::get('gestion_solicitudes_donativos_aprobadas','controlOficios\GestionSolicitudesDonativosAprobadasController@index');
  Route::post('buscar_solicitudes_donativos_con_filtro', 'controlOficios\GestionSolicitudesDonativosAprobadasController@buscarSolicitudesDonativosAprobadasConFiltro')->name('buscar_solicitudes_donativos_con_filtro');
  Route::post('registrar_datos_chofer', 'controlOficios\GestionSolicitudesDonativosAprobadasController@registrarDatosChofer')->name('registrar_datos_chofer');
  Route::post('buscar_datos_despacho', 'controlOficios\GestionSolicitudesDonativosAprobadasController@BuscarDatosDespacho')->name('buscar_datos_despacho');
  Route::post('buscar_datos_chofer', 'controlOficios\GestionSolicitudesDonativosAprobadasController@ObtenerDatosChoferDespacho')->name('buscar_datos_chofer');
  Route::post('actualizar_datos_chofer', 'controlOficios\GestionSolicitudesDonativosAprobadasController@actualizarDatosChofer')->name('actualizar_datos_chofer');
  Route::post('cambiar_estado_solicitud_donativo', 'controlOficios\GestionSolicitudesDonativosAprobadasController@cambiarEstado')->name('cambiar_estado_solicitud_donativo');

  Route::post('actualizar_fecha_desapacho', 'controlOficios\GestionSolicitudesDonativosAprobadasController@AsignarFechaDesapacho')->name('actualizar_fecha_desapacho');

  Route::get('listado_donaciones', 'controlOficios\ListadoDonacionesController@index')->name('listado_donaciones');
  Route::post('listado_donaciones_por_fecha_old', 'controlOficios\ListadoDonacionesController@BuscarDatosDespachoPorFechaOld')->name('listado_donaciones_por_fecha');
  Route::post('listado_donaciones_por_fecha', 'controlOficios\ListadoDonacionesController@BuscarDatosDespachoPorFecha')->name('listado_donaciones_por_fecha');
  Route::post('listado_donaciones_por_verificar', 'controlOficios\ListadoDonacionesController@buscarOficiosPorVerificar');
  Route::get('listado_donaciones_pdf', 'controlOficios\ListadoDonacionesController@pdf')->name('listado_donaciones_pdf');
  Route::post('listado_donaciones_pdf_individual', 'controlOficios\ListadoDonacionesController@pdfIndividual')->name('listado_donaciones_pdf_individual');
  Route::post('listado_donaciones_pdf_global', 'controlOficios\ListadoDonacionesController@pdfGlobal')->name('listado_donaciones_pdf_global');
  /*-----Listado de donaciones a desapachar----*/
  Route::post('obtener_datos_chofer', 'controlOficios\ListadoDonacionesController@obtenerDatosChofer')->name('obtener_datos_chofer');

  Route::get('listado_donaciones_a_despachar', 'controlOficios\ListadoDonacionesController@listadoDonacionesDespachar')->name('listado_donaciones_a_despachar');
  Route::post('listado_donaciones_a_despachar_buscar_material', 'controlOficios\ListadoDonacionesController@buscarMaterialesAsociados')->name('listado_donaciones_a_despachar_buscar_material');
  Route::post('listado_donaciones_entregar_material', 'controlOficios\ListadoDonacionesController@entregarMaterial')->name('listado_donaciones_entregar_material');
  Route::get('listado_donaciones_verificar', 'controlOficios\ListadoDonacionesController@listadoDonacionesVerificar')->name('listado_donaciones_verificar');
  Route::post('listado_donaciones_verificar', 'controlOficios\ListadoDonacionesController@listadoDonacionesVerificarPost')->name('listado_donaciones_verificar_post');


  /*---Solicitudes---*/
  Route::get('solicitudes_agente_autorizado','controlOficios\SolicitudesController@index');
  // Route::post('recepcion_oficios','controlOficios\GestionOficioController@registrarPost');
  // Route::post('recepcion_oficios/borrar','controlOficios\GestionOficioController@borrarOficio');
  // Route::post('recepcion_oficios/actualizar','controlOficios\GestionOficioController@actualizarOficio');
  // Route::post('recepcion_oficios/asignarTlf','controlOficios\GestionOficioController@asignarTlf');
  // Route::post('recepcion_oficios/borrarTlf','controlOficios\GestionOficioController@borrarTlf');


  /*---Gestion de Solicitudes de Donativos Aprobadas---*/

  /* Premium */
  Route::get("/premium/precio-partes-piezas", "premium\PartesController@index")->name("premium.partepieza");
  Route::post("/premium/precio-partes-piezas/search", "premium\PartesController@search");

  Route::get('/libroVentas', 'premium\Reportes@libroVentas')->name('premium.libro_ventas');
  Route::post('/libroVentas/reportes', 'premium\Reportes@reportes')->name('premium.reporte');
  Route::get('/petro', 'premium\PetroController@index')->name('premium.petro');
  Route::post('/petro/update', 'premium\PetroController@update');

  Route::get('/petro/precios', 'premium\PetroController@gestionPrecios')->name('premium.gestion.precio');
  Route::post('/petro/precios/buscar', 'premium\PetroController@buscarPrecios');
  Route::post('/petro/precios/actualizar', 'premium\PetroController@actualizarPrecios');
  Route::get('/petro/modelos/obtener', 'premium\PetroController@obtenerModelos');

  Route::get('/petro/actualizacion-factor', 'premium\PetroController@factorPP')->name('premium.factorpp');
  Route::post('/petro/actualizacion-factor/update', 'premium\PetroController@factorPPupdate');

  /* Premium */

  /**Fin Control de Oficios**/
  Route::prefix('vidamed')->group(function () {
    Route::get('sincronizar_trabajador','Vidamed\VidamedController@index');
    Route::post('sincronizar_trabajador','Vidamed\VidamedController@afiliarPersonalHcm');
    Route::get('sincronizar_familiares','Vidamed\VidamedController@sincronizarFamiliaresGet');
  });

  //////////////////////////////API CONSULTAS
  Route::prefix('api')->group(function () {
    //Recibe cedper -- modificada por wilian para saber si es trabajador o no (Validado con sigesp)
    Route::post('obtenerDatosPersonalNatural', ['as' => 'api.controloficio.datos_persona_natural',
    'uses' => 'Api\ControlOficioController@obtenerDatosPersonalNatural']);
    //Recibe cedper
    Route::post('obtenerDatosPersonalNatural2', ['as' => 'api.controloficio.datos_persona_natural2',
    'uses' => 'Api\ControlOficioController@obtenerDatosPersonalNatural2']);
    //Recibe fecha_despacho
    Route::post('obtenerDespachosPendientesPorFecha', ['as' => 'api.controloficio.obtener.despachos.pendientes.fecha',
    'uses' => 'Api\ControlOficioController@obtenerDespachosPendientesPorFecha']);
    //Recibe placa
    Route::post('obtenerDatosVehiculo', ['as' => 'api.controloficio.datos.vehiculo',
    'uses' => 'Api\ControlOficioController@obtenerDatosVehiculo']);
    //Recibe Rif
    Route::post('obtenerDatosPersonalJuridico', ['as' => 'api.controloficio.datos_personal_juridico',
    'uses' => 'Api\ControlOficioController@obtenerDatosPersonalJuridico']);
    //Recibe Rif
    Route::post('obtenerDatosPersonalJuridicoSinResponsables', ['as' => 'api.controloficio.datos_personal_juridico_sin_responsable',
    'uses' => 'Api\ControlOficioController@obtenerDatosPersonalJuridicoSinResponsables']);
    //Recibe id oficio
    Route::post('obtenerDatosOficio', ['as' => 'api.controloficio.obtenerdatosoficio',
    'uses' => 'Api\ControlOficioController@obtenerDatosOficio']);
    //Recibe id clasificacion de entes, para obtener entes asociados
    Route::post('obtenerEntes', ['as' => 'api.controloficio.obtenerentes',
    'uses' => 'Api\ControlOficioController@obtenerEntes']);



    //Sistema producción v1



    Route::post('obtenerProductos', [
      'as' => 'api.sistemaproduccionv1.obtenerproductos',
      'uses' => 'Api\ProductosController@obtenerProductos'
    ]);

    Route::get('obtenerModalidad', [
      'as' => 'api.sistemaproduccionv1.obtenermodalidad',
      'uses' => 'Api\ProductosController@obtenerModalidad'
    ]);

    Route::get('obtenerColor', [
      'as' => 'api.sistemaproduccionv1.obtenercolor',
      'uses' => 'Api\ProductosController@obtenerColor'
    ]);
    Route::post('disponibilidad_productos_recepcion_oficio', [
      'as' => 'api.sistemaproduccionv1.disponibilidad.productos.recepcion.oficio',
      'uses' => 'Api\ProductosController@obtenerDisponibilidadProductosRecepcionOficio'
    ]);

    //Sistema PRodución v1


    // Requisición
    Route::post('obtenerDatosTrabajador', ['as' => 'api.controloficio.datos_trabajador', 'uses' => 'Api\ControlOficioController@obtenerDatosTrabajador']);
    Route::get('obtenerResumenDespacho','Api\ControlOficioController@resumenDespacho')->name('api.resumen.despacho');
    Route::get('obtenerGestionDespacho','Api\RequisicionController@index')->name('api.gestion.despacho');
    Route::post('correccionRequisicion','Api\RequisicionController@correccionRequisicion')->name('api.correccion.requisicion');
    Route::post('requisicion','Api\RequisicionController@store')->name('api.requisicion.store');
    Route::post('requisicion_pdf','Api\RequisicionController@reportePdf')->name('api.requisicion.pdf');
    Route::post('requisicion_despacho','Api\RequisicionController@guardarDatosDespacho')->name('api.requisicion.store.despacho');
    Route::post('requisicion_despacho_actualizar','Api\RequisicionController@actualizarDatosDespacho')->name('api.requisicion.update.despacho');
    Route::post('requisicion_recaudos','Api\RequisicionController@actualizarRecaudos')->name('api.requisicion.update.recaudos');
    Route::post('requisicion_deducciones','Api\RequisicionController@asociarDeducciones')->name('api.requisicion.sync.deducciones');
    Route::post('eliminarRequisicion','Api\RequisicionController@eliminarRequisicion')->name('api.requisicion.eliminarRequisicion');
    Route::post('cierreRequisicion','Api\RequisicionController@cierreRequisicion')->name('api.requisicion.cierre');

    //Solicitudes - agente autorizado
    Route::post('solicitudes_agente_autorizado','Api\SolicitudDetalController@store')->name('api.agente.autorizado.solicitudes.store');

  });
  //////////////////////////////API CONSULTAS
  ///////////////////////////////////////////////REPORTES
  Route::prefix('reportes')->group(function () {
    Route::get('recepcion_oficios','controlOficios\GestionOficioController@reporteGet');
    Route::post('recepcion_oficios','controlOficios\GestionOficioController@reportePost');
    Route::post('oficio_pdf','controlOficios\GestionOficioController@generarPDF');
    Route::get('disponibilidad_productos','controlOficios\GestionOficioController@reporteDisponibilidadProduct');
    Route::get('expediente_solicitudes','controlOficios\GestionOficioController@expedienteSolicitudes')->name("reporte.expediente.solicitudes");
    Route::post('gestion_solicitudes_pagos','controlOficios\GestionOficioController@generarPdfPagos')->name("gestion.solicitudes.pagos.pdf");
    Route::get('recepcion_oficios_atendidos','controlOficios\GestionOficioController@reporteAtendidosGet');
    Route::post('recepcion_oficios_atendidos','controlOficios\GestionOficioController@reporteAtendidosPost');

  });
  ///////////////////////////////////////////////REPORTES
  Route::get('verificacion_pago','controlOficios\VerificacionPagoController@index')->name('verificacion_pago');
  Route::post('verificar_pagos','controlOficios\VerificacionPagoController@verificarPagos')->name('verificar_pagos');
  Route::get('verificar_pagos_pdf/{id}','controlOficios\VerificacionPagoController@verificarPagosPdf')->name('verificar_pagos_pdf');


});//Fin Middleware auth -> rutas
