<?php

use Illuminate\Database\Seeder;
use App\permissions;

class permisosSiintra extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos=[
          'siintra_menu_control_oficios',
          'siintra_menu_control_oficios_reporte_oficios',
          'siintra_menu_control_oficios_recepcion_de_oficios',
          'siintra_menu_control_oficios_reporte_seguimiento_recepcion_oficios',
          'siintra_menu_control_oficios_gestion_entes',
          'siintra_menu_control_oficios_gestion_departamentos',
          'siintra_menu_control_oficios_gestion_clasificacion_ente',
          'siintra_menu_control_oficios_gestion_motivo_solicitud',
          'siintra_menu_control_oficios_registrar_personal_juridico',
          'siintra_menu_control_oficios_registrar_personal_natural',
          'siintra_menu_vidamed',
          'siintra_menu_vidamed_sincronizar',
          'siintra_menu_control_oficios_gestion_materiales',
          'siintra_menu_control_oficios_asignar_responsables',
          'siintra_menu_control_oficios_verificacion_solicitudes',
          'siintra_menu_control_oficios_gestion_solicitudes',
          'siintra_menu_control_oficios_verificacion_solicitudes_donativos',
          'siintra_menu_control_oficios_gestion_solicitudes_validar_pago',
          'siintra_menu_control_oficios_listado_reintegro',
          'siintra_menu_control_oficios_registrar_vehiculos',
          'siintra_menu_control_oficios_ofertas_economicas_facturar',
          'siintra_menu_control_oficios_resumen_despacho',
          'siintra_menu_control_oficios_listado_de_donaciones',
          'siintra_menu_control_oficios_verificacion_listado_donaciones_despachar',
          'siintra_menu_control_oficios_verificacion_listado_donaciones_verificar',
          'siintra_menu_control_oficios_gestionar_solicitud_de_donativos_aprobados',
          'siintra_menu_control_oficios_observaciones_generales',
          'siintra_menu_control_oficios_gestion_requisicion',
          'siintra_menu_control_oficios_correccion_requisicion',
          'siintra_menu_control_oficios_gestion_recaudos',
          'siintra_menu_gestion_social',
          'siintra_menu_gestion_social_clasificacion_productos',
          'siintra_menu_gestion_social_gestion_productos',
          'siintra_menu_gestion_social_registro_jornada',
          'siintra_menu_gestion_social_entrega_beneficios',
          'siintra_menu_control_oficios_reporte_disponibilidad_productos',
          'siintra_menu_control_oficios_gestion_deducciones',
          'siintra_menu_control_oficios_reporte_verificacion_de_pago',
          'siintra_menu_control_oficios_reporte_expediente_solicitudes',
          'siintra_menu_premium',
          'siintra_menu_agente_autorizado',
          'siintra_menu_agente_autorizado_solicitud_almacen',
        ];
        foreach($permisos as $permiso){
          $p = permissions::firstOrNew(array('name' => $permiso));
          $p->name=$permiso;
          $p->guard_name="web";
          $p->save();
        }
    }
}
