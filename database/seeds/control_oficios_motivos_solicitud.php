<?php

use Illuminate\Database\Seeder;
use App\Modelos\ControlOficios\MotivoSolicitud;
class control_oficios_motivos_solicitud extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m = MotivoSolicitud::firstOrNew(array('id' => 1));
        $m->id=1;
        $m->nombre="Jornada de Venta de Telefonos";
        $m->descripcion="Sin descripcion";
        $m->save();
        $m = MotivoSolicitud::firstOrNew(array('id' => 2));
        $m->id=2;
        $m->nombre="Invitación a Eventos";
        $m->descripcion="Sin descripcion";
        $m->save();
        $m = MotivoSolicitud::firstOrNew(array('id' => 3));
        $m->id=3;
        $m->nombre="Donativos";
        $m->descripcion="Sin descripcion";
        $m->save();
        $m = MotivoSolicitud::firstOrNew(array('id' => 4));
        $m->id=4;
        $m->nombre="Intercambio de Bienes y Servicios";
        $m->descripcion="Sin descripcion";
        $m->save();
    }
}
