<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(control_oficios_motivos_solicitud::class);
        $this->call(permisosSiintra::class);
        $this->call(EstadosSeeder::class);
    }
}
