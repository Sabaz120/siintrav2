<?php

use Illuminate\Database\Seeder;
use App\Modelos\Estados;
class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $estados=[
        [
          'id'=>1,
          'nombre'=>'Pendiente',
          'descripcion'=>'-'
        ],
        [
          'id'=>2,
          'nombre'=>'Aprobado Totalmente',
          'descripcion'=>'-'
        ],
        [
          'id'=>3,
          'nombre'=>'Aprobado Parcialmente',
          'descripcion'=>'-'
        ],
        [
          'id'=>4,
          'nombre'=>'Rechazado',
          'descripcion'=>'Utilizado al momento de rechazar un oficio (aprobación parcial - completa)'
        ],
        [
          'id'=>5,
          'nombre'=>'Denegado',
          'descripcion'=>'Utilizado al momento de denegar una oferta economica'
        ],
        [
          'id'=>6,
          'nombre'=>'En Proceso de Facturación',
          'descripcion'=>'-'
        ],
        [
          'id'=>7,
          'nombre'=>'En Proceso de Despacho',
          'descripcion'=>'-'
        ],
        [
          'id'=>8,
          'nombre'=>'Procesado',
          'descripcion'=>'-'
        ],
        [
          'id'=>9,
          'nombre'=>'En proceso de verificación',
          'descripcion'=>'-'
        ],


      ];
      $estados=json_decode(json_encode($estados));//Convierto en colección de datos
      foreach($estados as $estado){
        $p=Estados::updateOrCreate(
          [
            'id'=>$estado->id
          ],
          [
            'id'=>$estado->id,
            'nombre'=>$estado->nombre,
            'descripcion'=>$estado->descripcion
          ]
        );

      }//foreach estados
    }
}
