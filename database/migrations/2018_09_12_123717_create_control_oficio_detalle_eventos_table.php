<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioDetalleEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_detalle_evento', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->time('hora');
            $table->string('lugar',100);
            $table->unsignedInteger('registro_oficio_id');
            $table->foreign('registro_oficio_id')->references('id')->on('control_oficio_registro_oficio')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_detalle_evento');
    }
}
