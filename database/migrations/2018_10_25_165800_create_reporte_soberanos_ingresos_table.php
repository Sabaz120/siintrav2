<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReporteSoberanosIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_soberanos_ingresos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cedula')->nullable();
            $table->string('conceptos')->nullable();
            $table->string('unidad')->nullable();
            $table->float('monto')->nullable();
            $table->integer('tipo_ingreso')->nullable();
            $table->integer('mes')->nullable();
            $table->integer('quincena')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporte_soberanos_ingresos');
    }
}
