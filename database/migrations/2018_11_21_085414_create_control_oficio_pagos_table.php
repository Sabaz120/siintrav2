<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_pago');
            $table->bigint('referencia');//Bigint, integer normal no sirve
            $table->float('monto',8,2);
            $table->longtext('soporte_lote');
            $table->boolean('validacion');
            $table->unsignedInteger('control_oficio_oferta_economica_id');
            $table->foreign('control_oficio_oferta_economica_id')->references('id')->on('control_oficio_oferta_economicas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_oficio_pagos');
    }
}
