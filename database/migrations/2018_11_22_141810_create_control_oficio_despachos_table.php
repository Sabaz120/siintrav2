<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioDespachosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_despachos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_despacho')->nullable();
            $table->integer('responsable_id')->unsigned();
            $table->foreign('responsable_id')->references('id')->on('control_oficio_personal_natural')->onDelete('cascade');
            $table->integer('chofer_id')->unsigned();
            $table->foreign('chofer_id')->references('id')->on('control_oficio_personal_natural')->onDelete('cascade');
            $table->integer('vehiculo_id')->unsigned();
            $table->foreign('vehiculo_id')->references('id')->on('control_oficio_vehiculos')->onDelete('cascade');
            $table->unsignedInteger('requisicion_id');
            // $table->unsignedInteger('registro_oficio_id');
            // $table->foreign('registro_oficio_id')->references('id')->on('control_oficio_registro_oficio')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_despachos');
    }
}
