<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioMaterialAprobacionParcial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_material_aprobacion_parcial', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->integer('registro_oficio_id')->unsigned();
            $table->integer('materiales_id')->unsigned();
            $table->foreign('registro_oficio_id')->references('id')->on('control_oficio_registro_oficio')->onDelete('cascade');
            $table->foreign('materiales_id')->references('id')->on('control_oficio_materiales')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_oficio_material_aprobacion_parcial');
    }
}
