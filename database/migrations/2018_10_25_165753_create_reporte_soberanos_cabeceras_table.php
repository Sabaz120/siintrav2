<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReporteSoberanosCabecerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_soberanos_cabeceras', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('cedula')->nullable();
          $table->string('nombre_concepto')->nullable();
          $table->string('departamento')->nullable();
          $table->string('tipo_nomina')->nullable();
          $table->string('cargo')->nullable();
          $table->float('sueldo_mensual')->nullable();
          $table->integer('mes')->nullable();
          $table->integer('quincena')->nullable();
          $table->string('total_asignacion')->nullable();
          $table->string('total_deducion')->nullable();
          $table->string('neto_cobrar')->nullable();
          $table->timestamps();
          $table->string('fecha_ingreso')->nullable();
          $table->string('fecha_corte_nomina')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporte_soberanos_cabeceras');
    }
}
