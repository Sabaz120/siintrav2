<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregadoCampoEstadoTablaOficios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_control_oficios')->table('control_oficio_registro_oficio', function (Blueprint $table) {
        $table->unsignedInteger('estado_aprobacion_id')->default(1);
        $table->foreign('estado_aprobacion_id')->references('id')->on('public.estados')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
