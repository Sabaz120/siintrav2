<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionSocialTrabajador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_gestion_social')->create('gestion_social_trabajador', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('cedula');
          $table->string('nombre');
          $table->string('apellido');
          $table->string('departamento')->nullable();
          $table->string('cargo')->nullable();
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_gestion_social')->dropIfExists('gestion_social_trabajador');
    }
}
