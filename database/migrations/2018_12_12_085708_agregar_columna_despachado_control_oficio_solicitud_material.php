<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarColumnaDespachadoControlOficioSolicitudMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->table('control_oficio_solicitud_material', function (Blueprint $table) {
            $table->boolean('despachado')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->table('control_oficio_solicitud_material', function (Blueprint $table) {
            $table->dropColumn('despachado');
        });
    }
}
