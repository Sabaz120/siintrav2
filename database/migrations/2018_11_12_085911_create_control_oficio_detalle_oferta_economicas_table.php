<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioDetalleOfertaEconomicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_detalle_oferta_economicas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('control_oficio_oferta_economica_id');
            $table->foreign('control_oficio_oferta_economica_id')->references('id')->on('control_oficio_oferta_economicas')->onDelete('cascade');
            $table->float('precio_unitario', 8, 2);
            $table->integer('producto_id');//Referencia a tabla products de sistema produccion antiguo (v1)
            $table->integer('cantidad')->default(1);
            $table->integer('nivel_precio')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_detalle_oferta_economicas');
    }
}
