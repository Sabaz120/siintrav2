<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioDespachoJornadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_despacho_jornadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('destino');
            $table->longtext('beneficiario');
            $table->unsignedInteger('despacho_id');
            $table->foreign('despacho_id')->references('id')->on('control_oficio_despachos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_despacho_jornadas');
    }
}
