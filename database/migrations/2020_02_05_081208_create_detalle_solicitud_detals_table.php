<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleSolicitudDetalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_detalle_solicitud_detal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('producto_id');//Referencia a tabla products de sistema produccion antiguo (v1)
            $table->integer('cantidad')->default(1);
            $table->string('modalidad');
            $table->integer('solicitud_detal_id')->unsigned();
            $table->foreign('solicitud_detal_id')->references('id')->on('control_oficio_solicitud_detal')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_detalle_solicitud_detal');
    }
}
