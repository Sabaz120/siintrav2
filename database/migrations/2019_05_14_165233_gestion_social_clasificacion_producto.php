<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionSocialClasificacionProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_gestion_social')->create('gestion_social_clasificacion_producto', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre', 100);
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::connection('siintra_gestion_social')->dropIfExists('gestion_social_clasificacion_producto');

    }
}
