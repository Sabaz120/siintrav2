<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiarCamposANullTablaControlOficioDespachos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::connection('siintra_control_oficios')->table('control_oficio_despachos', function (Blueprint $table) {
            $table->integer('responsable_id')->unsigned()->nullable()->change();
            $table->integer('chofer_id')->unsigned()->nullable()->change();
            $table->integer('vehiculo_id')->unsigned()->nullable()->change();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->table('control_oficio_despachos', function (Blueprint $table) {
            $table->integer('responsable_id')->unsigned()->change();
            $table->integer('chofer_id')->unsigned()->change();
            $table->integer('vehiculo_id')->unsigned()->change();
          });
     
    }
}
