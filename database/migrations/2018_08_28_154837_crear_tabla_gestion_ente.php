<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaGestionEnte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_ente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100)->unique();
            $table->string('descripcion', 200)->nullable();
            $table->integer('ponderacion');//1, 2, 3 ,4
            $table->unsignedInteger('clasificacion_ente_id');
            // $table->unsignedInteger('tiempo_atencion_id');
            // $table->unsignedInteger('nivel_atencion_id');
            $table->foreign('clasificacion_ente_id')->references('id')->on('control_oficio_clasificacion_ente')->onDelete('cascade');
            // $table->foreign('tiempo_atencion_id')->references('id')->on('control_oficios_tiempo_atencion');
            // $table->foreign('nivel_atencion_id')->references('id')->on('control_oficios_nivel_atencion');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_ente');
    }
}
