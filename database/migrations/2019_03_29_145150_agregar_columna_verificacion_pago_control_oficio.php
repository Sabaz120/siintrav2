<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarColumnaVerificacionPagoControlOficio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_control_oficios')->table('control_oficio_registro_oficio', function (Blueprint $table) {
          $table->boolean('verificacion_pago')->default(true);//Campo para validar si el oficio requiere pago al inicio o al final.
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
