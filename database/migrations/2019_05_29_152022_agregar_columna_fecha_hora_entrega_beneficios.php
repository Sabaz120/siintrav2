<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarColumnaFechaHoraEntregaBeneficios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::connection('siintra_gestion_social')->table('gestion_social_entrega_beneficios', function (Blueprint $table) {
          $table->timestamp('marcaje')->nullable();//fecha y hora de marcaje en el biometrico
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
