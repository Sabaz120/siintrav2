<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioSolicitudProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_solicitud_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('control_oficio_registro_oficio_id');
            $table->foreign('control_oficio_registro_oficio_id')->references('id')->on('control_oficio_registro_oficio')->onDelete('cascade');
            $table->integer('producto_id');//Referencia a tabla products de sistema produccion antiguo
            $table->integer('cantidad')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_oficio_solicitud_productos');
    }
}
