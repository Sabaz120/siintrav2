<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioRegistroOficiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_registro_oficio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');//Usuario autenticado en sistema que registra el oficio
            $table->integer('numero_oficio')->nullable();
            $table->date('fecha_emision');
            $table->integer('estado_id');//Estado de donde proviene la solicitud (oficio)
            $table->longtext('documento')->nullable();
            $table->integer('cantidad')->nullable();
            $table->text('descripcion')->nullable();
            $table->unsignedInteger('motivo_solicitud_id');
            $table->foreign('motivo_solicitud_id')->references('id')->on('control_oficio_motivo_solicitud')->onDelete('cascade');
            $table->unsignedInteger('personal_natural_id');
            $table->foreign('personal_natural_id')->references('id')->on('control_oficio_personal_natural')->onDelete('cascade');
            $table->unsignedInteger('personal_juridico_id')->nullable();
            $table->foreign('personal_juridico_id')->references('id')->on('control_oficio_personal_juridico')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_registro_oficio');
    }
}
