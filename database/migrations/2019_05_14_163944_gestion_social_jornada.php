<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionSocialJornada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_gestion_social')->create('gestion_social_jornada', function (Blueprint $table) {
          $table->increments('id');
          $table->text('nombre');
          $table->date('fecha_inicio');
          $table->date('fecha_fin');
          $table->text('lugar');
          $table->time('hora');
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_gestion_social')->dropIfExists('gestion_social_jornada');
    }
}
