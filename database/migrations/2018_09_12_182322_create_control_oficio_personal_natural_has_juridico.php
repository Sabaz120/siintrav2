<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioPersonalNaturalHasJuridico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_control_oficios')->create('control_oficio_personal_natural_has_personal_juridico', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('personal_natural_id');
          $table->foreign('personal_natural_id')->references('id')->on('control_oficio_personal_natural')->onDelete('cascade');
          $table->unsignedInteger('personal_juridico_id');
          // $table->foreign('personal_juridico_id')->references('id')->on('control_oficio_personal_juridico');
          $table->unsignedInteger('departamento_id');
          $table->foreign('departamento_id')->references('id')->on('control_oficio_departamento')->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_personal_natural_has_personal_juridico');
    }
}
