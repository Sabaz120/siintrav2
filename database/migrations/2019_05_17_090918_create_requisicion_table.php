<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisicionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_requisicion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');//Usuario que genera la requisición
            $table->string('numero_requisicion');//Compuesto por el id de la oferta economica
            $table->boolean('despachado')->default(0);
            $table->unsignedInteger('oferta_economica_id');
            $table->foreign('oferta_economica_id')->references('id')->on('control_oficio_oferta_economicas')->onDelete('cascade');
            $table->unsignedInteger('tipo_venta_id');
            $table->foreign('tipo_venta_id')->references('id')->on('control_oficio_tipo_venta')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_requisicion');
    }
}
