<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionSocialDetalleJornada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_gestion_social')->create('gestion_social_detalle_jornada', function (Blueprint $table) {
          $table->increments('id');
          $table->string('modalidad', 50);
          $table->float('monto');
          $table->text('description');
          $table->unsignedInteger('jornada_id');
          $table->foreign('jornada_id')->references('id')->on('gestion_social_jornada')->onDelete('cascade');
          $table->unsignedInteger('producto_id');
          $table->foreign('producto_id')->references('id')->on('gestion_social_producto')->onDelete('cascade');
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_gestion_social')->dropIfExists('gestion_social_detalle_jornada');
    }
}
