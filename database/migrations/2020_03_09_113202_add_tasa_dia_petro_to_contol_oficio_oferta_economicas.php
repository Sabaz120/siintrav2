<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTasaDiaPetroToContolOficioOfertaEconomicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->table('control_oficio_oferta_economicas', function (Blueprint $table) {
            $table->double('tasa_diaria_petro', 20, 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->table('control_oficio_oferta_economicas', function (Blueprint $table) {
            $table->dropColumn('tasa_diaria_petro');
        });
    }
}
