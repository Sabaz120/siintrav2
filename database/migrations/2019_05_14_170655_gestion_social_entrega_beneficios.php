<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionSocialEntregaBeneficios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_gestion_social')->create('gestion_social_entrega_beneficios', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('cedula');
          $table->boolean('entregado');
          $table->unsignedInteger('detalle_jornada_id');
          $table->foreign('detalle_jornada_id')->references('id')->on('gestion_social_detalle_jornada')->onDelete('cascade');
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_gestion_social')->dropIfExists('gestion_social_entrega_beneficios');

    }
}
