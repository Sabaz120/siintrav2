<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarColumnaModalidadTablaProductosAprobacionParcial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_control_oficios')->table('control_oficio_productos_aprobacion_parcial', function (Blueprint $table) {
          $table->string('modalidad');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_control_oficios')->table('control_oficio_productos_aprobacion_parcial', function (Blueprint $table) {
          $table->dropColumn('modalidad');
      });
    }
}
