<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarColumnaEstadoReintegroOfertaEconomica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('siintra_control_oficios')->table('control_oficio_oferta_economicas', function (Blueprint $table) {
        $table->unsignedInteger('estado_reintegro_id')->default(1);
        $table->foreign('estado_reintegro_id')->references('id')->on('public.estados')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
