<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlOficioReintegroCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_reintegro_cuenta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_cuenta',50);
            $table->string('tipo_cuenta',20);
            $table->string('banco',20);
            $table->string('identificacion',50);
            $table->string('nombre',40);
            $table->string('correo_electronico',45);
            $table->unsignedInteger('control_oficio_oferta_economica_id');
            $table->foreign('control_oficio_oferta_economica_id')->references('id')->on('control_oficio_oferta_economicas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_reintegro_cuenta');
    }
}
