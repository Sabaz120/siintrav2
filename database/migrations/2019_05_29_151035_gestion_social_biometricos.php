<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionSocialBiometricos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::connection('siintra_gestion_social')->create('gestion_social_biometricos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('ip');
          $table->boolean('estado');
          $table->softDeletes();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::connection('siintra_gestion_social')->dropIfExists('gestion_social_biometricos');
    }
}
