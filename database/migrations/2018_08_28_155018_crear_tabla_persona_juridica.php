<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPersonaJuridica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('siintra_control_oficios')->create('control_oficio_personal_juridico', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rif_cedula_situr', 50)->unique();
            $table->string('nombre_institucion', 50);
            $table->string('nombre_autoridad', 50)->nullable();
            $table->string('direccion_fiscal', 50)->nullable();
            // $table->string('correo', 45);
            // $table->string('estado', 45);
            $table->unsignedInteger('ente_id');
            $table->foreign('ente_id')->references('id')->on('control_oficio_ente')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('siintra_control_oficios')->dropIfExists('control_oficio_personal_juridico');
    }
}
